{
    "id": "0899970b-286d-4ed5-b945-76232c636318",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "GMLVideo",
    "IncludedResources": [
        "Sprites\\GMLVID\\sprPlaceholder",
        "Shaders\\shdVidDraw",
        "Objects\\GMLVID\\objPlayer",
        "Objects\\GMLVID\\GMLVIDEO_MANAGER",
        "Rooms\\rmGMLVideo",
        "Included Files\\GMLVideo_Support\\GMLVideo_V1.0.pdf",
        "Included Files\\GMLVideo_Support\\Video_Converter-1.0.exe",
        "Included Files\\GMLVideo_Support\\Video_Converter-1.0.1.exe",
        "Included Files\\GMLVideo_Support\\Video_Converter-1.0.2.exe",
        "Included Files\\three_laws.vid"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2017-17-27 06:01:49",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "fd33c3c5-7ce8-42ec-945b-14ef050a1066",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "bb3221c6-4083-4bfb-b1eb-93bb439b8111",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_NOBUFFER",
                    "hidden": false,
                    "value": "-1"
                },
                {
                    "id": "98b9763b-c8cb-4d79-afee-b780f58e1985",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_LOADINGBUFFER",
                    "hidden": false,
                    "value": "-2"
                },
                {
                    "id": "f14353af-51e2-422c-b52e-eec102b2d8f8",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_BUFFERLOADED",
                    "hidden": false,
                    "value": "-3"
                },
                {
                    "id": "fd2e8a29-a6b5-4dfa-a148-a5356e0aa7f3",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_DRAWDEBUG",
                    "hidden": false,
                    "value": "false"
                },
                {
                    "id": "990716f1-2502-419a-8f86-5bbdeeda14f3",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_NODATA",
                    "hidden": false,
                    "value": "-4"
                },
                {
                    "id": "508edd02-d3ed-4497-8a43-7524132b1229",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "GMLVIDEO_FILEFORMAT_VERSION",
                    "hidden": false,
                    "value": "1003"
                }
            ],
            "copyToTargets": 123146358329582,
            "filename": "GMLVideo.ext",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 4,
            "order": [
                "bb3221c6-4083-4bfb-b1eb-93bb439b8111",
                "98b9763b-c8cb-4d79-afee-b780f58e1985",
                "f14353af-51e2-422c-b52e-eec102b2d8f8",
                "fd2e8a29-a6b5-4dfa-a148-a5356e0aa7f3",
                "990716f1-2502-419a-8f86-5bbdeeda14f3",
                "508edd02-d3ed-4497-8a43-7524132b1229"
            ],
            "origname": "extensions\\GMLVideo.ext",
            "uncompress": false
        },
        {
            "id": "832438bd-41f7-4298-a359-a5e7770be7f4",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 123146358329550,
            "filename": "gmlvideo_core.gml",
            "final": "gmlvideo_end",
            "functions": [
                {
                    "id": "4e7278dc-36e5-40e4-8c53-6acc17ccd9e5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_load",
                    "help": "gmlvideo_load(filename[,optionsmap])",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_load",
                    "returnType": 2
                },
                {
                    "id": "9ae92889-6fc3-4979-b983-de26c3da4244",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_flushcache",
                    "help": "gmlvideo_flushcache()",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_flushcache",
                    "returnType": 2
                },
                {
                    "id": "b0872945-3da3-47d6-8b1c-7d5b7540a007",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_init",
                    "help": "gmlvideo_init()",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_init",
                    "returnType": 2
                },
                {
                    "id": "c3e74e41-2366-4655-9e7a-f9d318fb06b3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_end",
                    "help": "gmlvideo_end()",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_end",
                    "returnType": 2
                },
                {
                    "id": "9d9acfb7-9928-445b-b2b8-5052ec5b819e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_step",
                    "help": "gmlvideo_step()",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_step",
                    "returnType": 2
                },
                {
                    "id": "2c97bd57-f3db-4dd2-a691-90aa3add86d1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "gmlvideo_destroy",
                    "help": "gmlvideo_destroy(gmlvideo)",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_destroy",
                    "returnType": 2
                },
                {
                    "id": "e21b6a6c-5542-4a78-9f95-5bff12ac7318",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_video_framejump",
                    "help": "gmlvideo_video_framejump(gmlvideo[,frames,progress])",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_framejump",
                    "returnType": 2
                },
                {
                    "id": "93ca0f8e-b05f-4a6d-a92c-ac03bb54d4c6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "gmlvideo_video_stop",
                    "help": "gmlvideo_video_stop(gmlvideo)",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_stop",
                    "returnType": 2
                },
                {
                    "id": "f9fd64bc-2356-4d09-a712-1949d5aefabd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_queue_frame",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_queue_frame",
                    "returnType": 2
                },
                {
                    "id": "6a5437d2-9b85-456d-b496-e385d7c21986",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_dequeue_frame",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_dequeue_frame",
                    "returnType": 2
                },
                {
                    "id": "9258bd24-3632-4220-a9c5-e3cb450654c4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "gmlvideo_video_getsurface",
                    "help": "gmlvideo_video_getsurface(gmlvideo)",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_getsurface",
                    "returnType": 2
                },
                {
                    "id": "1a521d26-db41-4bf9-b7af-31a6041a5bee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_video_drawframe",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_video_drawframe",
                    "returnType": 2
                },
                {
                    "id": "d2c2bb36-55a9-4022-a5f7-fad2a88154a7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "frame_is_keyframe",
                    "help": "frame_is_keyframe(manifest,frame)",
                    "hidden": false,
                    "kind": 11,
                    "name": "frame_is_keyframe",
                    "returnType": 2
                },
                {
                    "id": "d3529b4b-5fbc-4106-bafb-089686cec195",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_drawVertexFrame",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_drawVertexFrame",
                    "returnType": 2
                },
                {
                    "id": "b3d56c3a-2316-4b07-acb3-9cf93c8e59d3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "frameVertexArrayClear",
                    "help": "frameVertexArrayClear(vertexArray)",
                    "hidden": false,
                    "kind": 11,
                    "name": "frameVertexArrayClear",
                    "returnType": 2
                },
                {
                    "id": "1f985277-d96e-4a72-83f8-6ace94e6be11",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_video_draw",
                    "help": "gmlvideo_video_draw(gmlvideo,x,y[w,h])",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_draw",
                    "returnType": 2
                },
                {
                    "id": "ca7d703a-3337-4e0c-b67f-aee7af8b3249",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_sync_audio",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_sync_audio",
                    "returnType": 2
                },
                {
                    "id": "8c4193ae-9430-46a5-82c5-80241234eedc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_video_play",
                    "help": "gmlvideo_video_play(gmlvideo[,play])",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_play",
                    "returnType": 2
                },
                {
                    "id": "47fd3c48-8ca2-4c2b-afeb-c76a4eff4f7e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_video_speed",
                    "help": "gmlvideo_video_speed(gmlvideo,speed)",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_speed",
                    "returnType": 2
                },
                {
                    "id": "8eb64846-284d-41f4-92e5-0d359ad1c1a0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmlvideo_video_volume",
                    "help": "gmlvideo_video_volume(gmlvideo[,volume])",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_video_volume",
                    "returnType": 2
                },
                {
                    "id": "97662c7d-88d8-4436-a619-2b3eac096381",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "_gmlvideo_video_framebuffer_to_vertexbuffer",
                    "help": "_gmlvideo_video_framebuffer_to_vertexbuffer(frameSizeList,framebuffer)",
                    "hidden": false,
                    "kind": 11,
                    "name": "_gmlvideo_video_framebuffer_to_vertexbuffer",
                    "returnType": 2
                },
                {
                    "id": "6d322523-6387-4635-8046-2819bd61e4f0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "gmlvideo_set_framecache",
                    "help": "gmlvideo_set_framecache(framesToBuffer)",
                    "hidden": false,
                    "kind": 11,
                    "name": "gmlvideo_set_framecache",
                    "returnType": 2
                }
            ],
            "init": "gmlvideo_init",
            "kind": 2,
            "order": [
                "4e7278dc-36e5-40e4-8c53-6acc17ccd9e5",
                "9ae92889-6fc3-4979-b983-de26c3da4244",
                "b0872945-3da3-47d6-8b1c-7d5b7540a007",
                "c3e74e41-2366-4655-9e7a-f9d318fb06b3",
                "9d9acfb7-9928-445b-b2b8-5052ec5b819e",
                "2c97bd57-f3db-4dd2-a691-90aa3add86d1",
                "e21b6a6c-5542-4a78-9f95-5bff12ac7318",
                "93ca0f8e-b05f-4a6d-a92c-ac03bb54d4c6",
                "f9fd64bc-2356-4d09-a712-1949d5aefabd",
                "6a5437d2-9b85-456d-b496-e385d7c21986",
                "9258bd24-3632-4220-a9c5-e3cb450654c4",
                "1a521d26-db41-4bf9-b7af-31a6041a5bee",
                "d2c2bb36-55a9-4022-a5f7-fad2a88154a7",
                "d3529b4b-5fbc-4106-bafb-089686cec195",
                "b3d56c3a-2316-4b07-acb3-9cf93c8e59d3",
                "1f985277-d96e-4a72-83f8-6ace94e6be11",
                "ca7d703a-3337-4e0c-b67f-aee7af8b3249",
                "8c4193ae-9430-46a5-82c5-80241234eedc",
                "47fd3c48-8ca2-4c2b-afeb-c76a4eff4f7e",
                "8eb64846-284d-41f4-92e5-0d359ad1c1a0",
                "97662c7d-88d8-4436-a619-2b3eac096381",
                "6d322523-6387-4635-8046-2819bd61e4f0"
            ],
            "origname": "extensions\\gmlvideo_core.gml",
            "uncompress": false
        },
        {
            "id": "663e6cd0-2b67-4c7d-a2d2-a0bce18034a0",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "datascripts.gml",
            "final": "",
            "functions": [
                {
                    "id": "562070c2-be36-4faa-8bbe-7fce572c1a51",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "dm",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "dm",
                    "returnType": 2
                },
                {
                    "id": "d7f6d3c6-0216-498c-b73c-2b2c164f78e0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "timer",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "timer",
                    "returnType": 2
                },
                {
                    "id": "b5b30bc6-c99a-40a4-81d2-37312bd0be4c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "filename_removeExt",
                    "help": "filename_removeExt(filename)",
                    "hidden": false,
                    "kind": 11,
                    "name": "filename_removeExt",
                    "returnType": 1
                },
                {
                    "id": "880bff35-01d3-41ad-b381-6944e7f06eca",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_get_embedded",
                    "help": "ds_get_embedded(struture,key\/index,...)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_get_embedded",
                    "returnType": 2
                },
                {
                    "id": "3d936fe4-5598-4f41-a739-bedd31211713",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_list_set_all",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_list_set_all",
                    "returnType": 2
                },
                {
                    "id": "4313b173-b5a4-48fc-b038-cb8da9bfe1d8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "file_text_read_all",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "file_text_read_all",
                    "returnType": 1
                },
                {
                    "id": "c4ccdc76-9ae6-41ec-9114-5e02900e469d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_set_embedded",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_set_embedded",
                    "returnType": 2
                },
                {
                    "id": "747a1958-c4a1-4bbc-b395-44ca211d9db9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_list_add_list",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_list_add_list",
                    "returnType": 2
                },
                {
                    "id": "7b167c7d-0091-473c-a650-a2dc9e86bec5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_list_set_map",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_list_set_map",
                    "returnType": 2
                },
                {
                    "id": "4ee49291-14c1-4482-b6c6-41b85d54fb25",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "isset_default",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "isset_default",
                    "returnType": 2
                },
                {
                    "id": "a1d74f71-a588-4a20-97ae-3336a89a992b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "isset_equality",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "isset_equality",
                    "returnType": 2
                },
                {
                    "id": "95d6f63b-e4c0-4709-87e1-1d45869983b7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_map_merge",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_map_merge",
                    "returnType": 2
                },
                {
                    "id": "6e548ea2-6a74-41fa-91d7-9bcae5ab78ed",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ds_map_merge_isset",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ds_map_merge_isset",
                    "returnType": 2
                },
                {
                    "id": "175094df-8c2a-4a0f-a62d-aee1452a85e6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "isset",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "isset",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "562070c2-be36-4faa-8bbe-7fce572c1a51",
                "d7f6d3c6-0216-498c-b73c-2b2c164f78e0",
                "b5b30bc6-c99a-40a4-81d2-37312bd0be4c",
                "880bff35-01d3-41ad-b381-6944e7f06eca",
                "3d936fe4-5598-4f41-a739-bedd31211713",
                "4313b173-b5a4-48fc-b038-cb8da9bfe1d8",
                "c4ccdc76-9ae6-41ec-9114-5e02900e469d",
                "747a1958-c4a1-4bbc-b395-44ca211d9db9",
                "7b167c7d-0091-473c-a650-a2dc9e86bec5",
                "4ee49291-14c1-4482-b6c6-41b85d54fb25",
                "a1d74f71-a588-4a20-97ae-3336a89a992b",
                "95d6f63b-e4c0-4709-87e1-1d45869983b7",
                "6e548ea2-6a74-41fa-91d7-9bcae5ab78ed",
                "175094df-8c2a-4a0f-a62d-aee1452a85e6"
            ],
            "origname": "extensions\\datascripts.gml",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "me.eladga.gmlvideo",
    "productID": "D1ADFEAE68D56A3B4626C9B858EE52B0",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "1.0.4"
}