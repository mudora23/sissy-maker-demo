#define GMLVIDEO_Data_Scripts
/*Title placeholder*/

#define dm
///dm(prop,value,...)
//creates and returns a map with the specified values
var a = ds_map_create();
for (var i = 0; i < argument_count; i += 2)
    a[? argument[i]] = argument[i + 1];
return a;

#define timer
if (argument_count >= 1) {
    global.timertime = current_time;
    global.timermessage = argument[0];
}
else  {
    show_debug_message(global.timermessage + ": " + string_format((current_time - global.timertime)/1000, 6, 5));
}

#define filename_removeExt
///filename_removeExt(filename)
var s = argument0, l = string_length(s), i = l, lastchar = "";
while (i > 0 && lastchar != ".") {
    lastchar = string_char_at(s, i);
    --i;
}
if (i == 1) {
    return "";
} else {
    return string_copy(s, 0, i);
}

#define ds_get_embedded
///ds_get_embedded(struture,key/index,...)
var _ds = argument[0], c = argument_count;
for (var i = 1; i < c; ++i) {
    if (is_undefined(_ds)) return undefined;
    if (is_string(argument[i])) {
        _ds = _ds[? argument[i]];
    } else {
        _ds = _ds[| argument[i]];
    }
}   
return _ds;

#define ds_list_set_all
///ds_list_set_all(list,value)
var i,l=argument0,s=ds_list_size(l);
for (i=0;i<s;++i)
    l[|i] = argument1;

#define file_text_read_all
///file_text_read_all(filename)
//reads a whole file and returns the string
if (!file_exists(argument0)) {
    show_debug_message("Could not load file: " + string(argument0));
    return "";
}
var f = file_text_open_read(argument0), s = "";
while (!file_text_eof(f)) {
    s += file_text_readln(f);
}
file_text_close(f);
return s;

#define ds_set_embedded
///ds_set_embedded(struture,value,key/index,...)
var _ds = argument[0], c = argument_count;
for (var i = 2; i < c; ++i) {
    if (is_string(argument[i])) {
        if (i == c - 1)
            _ds[? argument[i]] = argument[1];
        else
            _ds = _ds[? argument[i]];
    } else {
        if (i == c - 1)
            _ds[| argument[i]] = argument[1];
        else
            _ds = _ds[| argument[i]];
    }
}

#define ds_list_add_list
///ds_list_add_list(index,list);
ds_list_add(argument0,argument1);
ds_list_mark_as_list(argument0, ds_list_size(argument0) - 1);

#define ds_list_set_map
///ds_list_set_map(list,index,map);
argument0[| argument1] = argument2;
ds_list_mark_as_map(argument0, argument1);

#define isset_default
if (!is_undefined(argument0)) return argument0;
else return argument1;

#define isset_equality
///isset_equality(value,equality)
return (!is_undefined(argument0) && argument0 == argument1);
    

#define ds_map_merge
///ds_map_merge(map1, map2)
//merges map2 onto map1
var map1 = argument0,
    map2 = argument1;
    
var size = ds_map_size(map2);
var key = ds_map_find_first(map2);
for (var i = 0; i < size; i++;)
{
    map1[? key] = map2[? key];
    key = ds_map_find_next(map2, key);
}
return map1;

#define ds_map_merge_isset
///ds_map_merge_isset(map1, map2)
//merges map2 onto map1
var map1 = argument0,
    map2 = argument1;
    
var size = ds_map_size(map2);
var key = ds_map_find_first(map2), v;
for (var i = 0; i < size; i++;)
{
    v = map2[? key];
    if (isset(v))
        map1[? key] = map2[? key];
    key = ds_map_find_next(map2, key);
}
return map1;

#define isset
///isset(value)
return !is_undefined(argument0);

