#define gmlvideo_load
///gmlvideo_load(filename[,optionsmap])
//returns a play object
var fn = filename_removeExt(filename_name(argument[0])), ext = filename_ext(argument[0]),
    failed = true, temp_dir = "_GMLVID_", vid_dir = temp_dir + fn + "\\",
    manifest = undefined, manifestText, videoObject = undefined;
do if (ext == ".vid") {
    if (directory_exists(vid_dir) && file_exists(vid_dir + "video.dat")) {
        //exists
    } else {
        //if (!directory_exists(temp_dir))
        //    directory_create(temp_dir);
        var archiveName = filename_removeExt(filename_name(argument[0])) + ".zip";//argument[0];
        file_copy(argument[0], working_directory + archiveName);
        
        if (!directory_exists(vid_dir))
            directory_create(vid_dir);
        var r = zip_unzip(working_directory + archiveName, vid_dir);
        
        file_delete(working_directory + archiveName);
        if (r <= 0)
            break;
    }
    
    //Files exist, load data
    manifestText = file_text_read_all(vid_dir + "video.dat");
    if (manifestText == "") break;
    manifest = json_decode(manifestText);
    //check versioning
    if (isset(manifest[? "version"]) && manifest[? "version"] != GMLVIDEO_FILEFORMAT_VERSION)
        show_error("Video seems to have been created with a different version of the converter. Video will be attempted to be played but compatability is not guarenteed", false);
    failed = false;
} until true;
if (failed) {
    //error
    if (!is_undefined(manifest))
        ds_map_destroy(manifest);
    return -1;
} else {
    var opts = undefined;
    if (argument_count > 1) opts = argument[1];
    
    videoObject = dm(
        "frame",    0,
        "frame_progress", 0,
        "frame_lastinterval", current_time,
        "speed",    1,
        "frame_surface", -1,
        "playing",  true,
        "loop", true,
        "file_root", vid_dir,
        "autoplay", true
    );
    
    //add user options
    if (!is_undefined(opts)) {
        ds_map_merge(videoObject, opts);
        ds_map_destroy(opts);   
    }
    
    ds_map_add_map(videoObject, "manifest", manifest);
    
    //Create framebuffer
    ds_map_add_list(videoObject, "frame_buffer", ds_list_create());
    ds_set_embedded(videoObject, GMLVIDEO_NOBUFFER, "frame_buffer", manifest[? "frame_count"] - 1);
    ds_list_set_all(videoObject[? "frame_buffer"], GMLVIDEO_NOBUFFER);
    
    //Add audio
    var audioLo = videoObject[? "file_root"] + "output_audio.ogg";
    if (file_exists(audioLo)) {
        videoObject[? "audio"] = audio_create_stream(audioLo);
        videoObject[? "audio_instance"] = audio_play_sound(videoObject[? "audio"], 1, true);
        audio_pause_sound(videoObject[? "audio_instance"]);
    }
    
    //add to tracker
    ds_list_add(gmlvideo_instances, videoObject);
    
    gmlvideo_video_play(videoObject, videoObject[? "autoplay"]);
    
    return videoObject; 
}

#define gmlvideo_flushcache
///gmlvideo_flushcache()
//var temp_dir = working_directory + "_GMLVID\";
//directory_destroy(temp_dir);
var f = file_find_first("_GMLVID*", fa_directory);
while (f != "") {
    directory_destroy(f);
    f = file_find_next();
}
file_find_close();

#define gmlvideo_init
globalvar gmlvideo, gmlvideo_instances, gmlvideo_asyncAssoc;
var vertex_vid;
vertex_format_begin()
vertex_format_add_custom(vertex_type_colour, vertex_usage_colour);
vertex_vid = vertex_format_end();
gmlvideo = dm(
    "buffer_frames", 2,
    "vertex_vid", vertex_vid
);
gmlvideo_instances = ds_list_create();
gmlvideo_asyncAssoc = dm();

#define gmlvideo_end
vertex_format_delete(gmlvideo[? "vertex_vid"]);
ds_map_destroy(gmlvideo);
//clean all active videos
while (ds_list_size(gmlvideo_instances) > 0)
    gmlvideo_destroy(gmlvideo_instances[| 0]);
    
ds_list_destroy(gmlvideo_instances);

#define gmlvideo_step
if (!instance_exists(GMLVIDEO_MANAGER))
    instance_create_depth(0,0, 0, GMLVIDEO_MANAGER);

#define gmlvideo_destroy
///gmlvideo_destroy(gmlvideo)
var G = argument0;
if (surface_exists(G[? "frame_surface"])) {
    surface_free(G[? "frame_surface"]);
}
if (isset(G[? "audio"]))
    audio_destroy_stream(G[? "audio"]);
ds_map_destroy(G);
var pos = ds_list_find_index(gmlvideo_instances, G);
if (pos != -1)
    ds_list_delete(gmlvideo_instances, pos);
//IMPORTANT: DESTORY FRAMES
show_debug_message("DESTROY FRAMES MUST BE IMPLEMENTED");

#define gmlvideo_video_framejump
///gmlvideo_video_framejump(gmlvideo[,frames,progress])
//Jump 1?x frames and set progress to 0?y
var video = argument[0], frameinc = 1, progress = 0, index = video[? "frame"],
    framecount = ds_get_embedded(video, "manifest", "frame_count"), loop = !!video[? "loop"];
if (argument_count > 1) {
    frameinc = argument[1];
    if (argument_count > 2)
        progress = argument[2];
}
index += frameinc;
if (index >= framecount) {
    if (loop) {
        index = index mod framecount;
    } else {
        //video ended
        gmlvideo_video_stop(video);
    }
}
video[? "frame"] = index;
video[? "frame_progress"] = progress;
video[? "frame_redraw"] = true;

#define gmlvideo_video_stop
///gmlvideo_video_stop(gmlvideo)
//Stops a video
//probably unload all frames bar end one
var video = argument0;
video[? "playing"] = false;
video[? "frame"] = ds_get_embedded(video, "manifest", "frame_count");
video[? "frame_progress"] = 0;

#define _gmlvideo_queue_frame
///_gmlvideo_queue_frame(gmlvideo,index)
var index = argument[1], video = argument[0], framebuffer = video[? "frame_buffer"];
if (framebuffer[| index] == GMLVIDEO_NOBUFFER) {
    var manifest = video[? "manifest"], 
        frameSize = manifest[? "frameSizePrecalc"],
        start_frame = isset_default(manifest[? "start_frame"],0),
        frame_total_size = frameSize[| index];
        
    if (frame_total_size <= 0) {
        //framebuffer[| index] = GMLVIDEO_NODATA;
        return false;
    }
        
    var b = buffer_create(frame_total_size, buffer_fast, 1),
        frame_file = video[? "file_root"] + "frame_" + string(start_frame + index) + ".dat",
        uuid = buffer_load_async(b, frame_file, 0, frame_total_size);
        
    ds_map_add_map(gmlvideo_asyncAssoc, 
        uuid,
        dm("frame_buffer", framebuffer, "frame_index", index, "buffer_index", b)
    );
    
    ds_list_set_map(framebuffer, index,
        dm("status",GMLVIDEO_LOADINGBUFFER,"id",uuid)
    );
//    framebuffer[| index] = GMLVIDEO_LOADINGBUFFER;
}
//adaptation of loadFrame(folder,videManifest,frameIndex)
/*var vb, manifest = argument1, folder = argument0,
    start_frame = isset_default(manifest[? "start_frame"],0),
    frame_count = manifest[? "frame_count"],
    fi = argument2 mod frame_count,
    width = manifest[? "width"], height = manifest[? "height"],
    frameSize = ds_get_embedded(manifest, "frameSize", fi);
var frameName = "frame_"+string(start_frame + fi)+".dat",
    frameBuffer = buffer_load(folder + frameName), i, s = ds_list_size(frameSize), n = 0,
    bytesPerVertex = 4;*/

#define _gmlvideo_dequeue_frame
///_gmlvideo_dequeue_frame(gmlvideo,index)
var index = argument[1], video = argument[0], framebuffer = video[? "frame_buffer"];
if (framebuffer[| index] != GMLVIDEO_NOBUFFER) {
    
    if (ds_get_embedded(framebuffer[|index], "status") == GMLVIDEO_BUFFERLOADED) {
       // var manifest = video[? "manifest"];
        
        var b = ds_get_embedded(framebuffer, index, "buffer");
        if (!is_undefined(b))
            buffer_delete(b);    
        
        ds_map_destroy(framebuffer[|index]);
        framebuffer[| index] = GMLVIDEO_NOBUFFER;
        
    } else {
        ds_map_delete(gmlvideo_asyncAssoc, ds_get_embedded(framebuffer[| index], "id"));
        //possibly uuid may need to be added to a "discard" list if GM doesnt handle the deleted buffer very well?
        //rather thandeleting it straight away, let it load in anhd destroy if its on the blacklist (in save/load event)
    }
} else {
    //dont need to do anything
}

#define gmlvideo_video_getsurface
///gmlvideo_video_getsurface(gmlvideo)
var video = argument[0], manifest = video[? "manifest"], 
    xx = 0, yy = 0, ww = manifest[? "width"], hh = manifest[? "height"],
    redraw = isset_default(video[? "frame_redraw"], false),
    getSurface = true;
    
if (redraw) {
    _gmlvideo_video_drawframe(video, video[? "frame"]);
    video[? "frame_redraw"] = false;
}
if (getSurface) {
    return video[? "frame_surface"];
}/* else {
    if (surface_exists(video[? "frame_surface"])) {
        draw_surface_stretched(video[? "frame_surface"], xx, yy, room_width + 5, room_height + 5);
    }
}*/

#define _gmlvideo_video_drawframe
///_gmlvideo_video_drawframe(gmlvideo,frame)
var video = argument[0], manifest = video[? "manifest"], framebuffer = video[? "frame_buffer"],
    index = argument[1];
if (framebuffer[| index] != GMLVIDEO_NOBUFFER
    && isset_equality(ds_get_embedded(framebuffer, index, "status"), GMLVIDEO_BUFFERLOADED)) {
    //has data
    //convert to vertex buffers
    var vb = _gmlvideo_video_framebuffer_to_vertexbuffer(ds_get_embedded(manifest, "frameSize", index), 
        ds_get_embedded(framebuffer, index, "buffer"));
        
    if (!surface_exists(video[? "frame_surface"]))
        video[? "frame_surface"] = surface_create(manifest[? "width"], manifest[? "height"]);
    
    surface_set_target(video[? "frame_surface"]);
    
        if (GMLVIDEO_DRAWDEBUG)
            draw_clear(c_red);
            
        _gmlvideo_drawVertexFrame(manifest,vb);
    
    surface_reset_target();
    
    video[? "frame_lastdrawn"] = index;
    
    //Free memory
    frameVertexArrayClear(vb);
    //could cache after creation, but probably not much help bar interframe draws
}

#define _gmlvideo_video_framebuffer_to_vertexbuffer
///_gmlvideo_video_framebuffer_to_vertexbuffer(frameSizeList,framebuffer)
/*
    takes a infobuffer, and a framesize list and returns an array
    of vertex buffers
*/
// adopted from loadFrame(folder,videManifest,frameIndex)
var frameSize = argument[0], frameBuffer = argument[1],
    i, s = ds_list_size(frameSize), n = 0, vb,
    bytesPerVertex = 4, vertex_vid = gmlvideo[? "vertex_vid"];
//Seperate into vertex buffers
for (i = 0; i < s; ++i) {
    //for each block
    thisFrameSize = frameSize[| i];
    if (thisFrameSize > 0) {
        //data actually exists
        vb[i] = vertex_create_buffer_from_buffer_ext(frameBuffer, vertex_vid, n, thisFrameSize/bytesPerVertex);
        //vertex_freeze(vb[i]);
    } else {
        vb[i] = -1;
    }
    n += thisFrameSize;
}
return vb;

#define frame_is_keyframe
///frame_is_keyframe(manifest,frame)
var manifest = argument[0], i = argument[1];
return (i == 0) || (!is_undefined(manifest[? "keyframe_rate"]) && i mod manifest[? "keyframe_rate"] == 0);

#define _gmlvideo_drawVertexFrame
///_gmlvideo_drawVertexFrame(videManifest,vertexBufferArray)
var vb = argument1, manifest = argument0,
    width = manifest[? "width"], height = manifest[? "height"],
    blockSize = manifest[? "block_size"],
    blockCount = ds_list_size(ds_get_embedded(manifest, "frameSize", 0)),
    blockCount_W = ceil(width/blockSize),
    block_x, block_y, shdpos;
    
shader_set(shdVidDraw);
shdpos = shader_get_uniform(shdVidDraw, "offset_pos");
    
for (var i = 0; i < blockCount; ++i) {
    if (vb[@ i] == -1) continue;
    
    block_x = i mod blockCount_W;
    block_y = i div blockCount_W;
    
    shader_set_uniform_f(shdpos, block_x * blockSize, block_y * blockSize);
    
    vertex_submit(vb[@ i], pr_pointlist, -1);
}
shader_reset();

#define frameVertexArrayClear
///frameVertexArrayClear(vertexArray)
//cleans a vertex array
//set vb = 0; outside this script
var vb = argument0;
for (var i = 0; i < array_length_1d(vb); ++i) {
    if (vb[i] != -1)
        vertex_delete_buffer(vb[i]);
}   

#define gmlvideo_video_draw
///gmlvideo_video_draw(gmlvideo,x,y[w,h])
var video = argument[0], manifest = video[? "manifest"], 
    xx = argument[1], yy = argument[2], ww = manifest[? "width"], hh = manifest[? "height"], 
    s = gmlvideo_video_getsurface(video);
    
if (argument_count > 3) {
    ww = argument[3];
    if (argument_count > 4) {
        hh = argument[4];
    }
}
if (surface_exists(s)) {
    draw_surface_stretched(s, xx, yy, ww, hh);
}

#define _gmlvideo_sync_audio
///_gmlvideo_sync_audio(gmlvideo)
var video = argument0, manifest = video[? "manifest"];
if (!is_undefined(video[? "audio_instance"])) {
    audio_sound_set_track_position(video[? "audio_instance"], video[? "frame"] / manifest[? "target_fps"]);
    audio_sound_pitch(video[? "audio_instance"], video[? "speed"]);
}

#define gmlvideo_video_play
///gmlvideo_video_play(gmlvideo[,true/false])
var video = argument[0], play = !video[? "playing"];
if (argument_count > 1) play = argument[1];
video[? "playing"] = play;
_gmlvideo_sync_audio(video);
if (isset(video[? "audio_instance"])) {
    if (play) {
        audio_resume_sound(video[? "audio_instance"]);
    } else {
        audio_pause_sound(video[? "audio_instance"]);
    }
}

#define gmlvideo_video_speed
///gmlvideo_video_speed(gmlvideo,speed)
//changes the speed of the video. 1 = normal, 2 = double, 0.5 = half speed
var video = argument[0], s = argument[1];
video[? "speed"] = s;
_gmlvideo_sync_audio(video);

#define gmlvideo_video_volume
///gmlvideo_video_volume(gmlvideo[,volume])
var video = argument[0];//, manifest = video[? "manifest"];
if (!is_undefined(video[? "audio_instance"])) {
    if (argument_count == 1) {
        //mute/unmute
        if (audio_sound_get_gain(video[? "audio_instance"]) > 0)
            audio_sound_gain(video[? "audio_instance"], 0, 0);
        else
            audio_sound_gain(video[? "audio_instance"], 1, 0);
    } else {
        //volume
        audio_sound_gain(video[? "audio_instance"], argument[1], 0);
    }
}

#define gmlvideo_set_framecache
///gmlvideo_set_framecache(frames_to_buffer)
///sets the frame cache
gmlvideo[? "buffer_frames"] = argument0;

