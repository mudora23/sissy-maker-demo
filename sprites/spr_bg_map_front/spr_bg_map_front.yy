{
    "id": "d014d82d-ed08-47f1-99c9-87715a6ba6df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5b98993b-0111-479f-aab7-0ef411b94291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d014d82d-ed08-47f1-99c9-87715a6ba6df",
            "compositeImage": {
                "id": "ab05635b-38f1-4965-9f24-1a55f8d7b863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b98993b-0111-479f-aab7-0ef411b94291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aee51b9d-2223-48f2-8740-9f55885f2b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b98993b-0111-479f-aab7-0ef411b94291",
                    "LayerId": "61480338-f6c2-4e04-9463-91d736570d8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "61480338-f6c2-4e04-9463-91d736570d8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d014d82d-ed08-47f1-99c9-87715a6ba6df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}