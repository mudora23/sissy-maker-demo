{
    "id": "1fa0d732-f73b-4378-99f1-52964dc284bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c3ea139a-733e-4d98-b9b3-6ea2c249b5bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa0d732-f73b-4378-99f1-52964dc284bf",
            "compositeImage": {
                "id": "329a3d9d-56de-45d8-8b57-3fe39ec228c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ea139a-733e-4d98-b9b3-6ea2c249b5bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3421520c-dbdb-4c04-9707-69d99cc57b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ea139a-733e-4d98-b9b3-6ea2c249b5bf",
                    "LayerId": "89001cee-147a-4b58-bbf6-9b33b587fdd4"
                }
            ]
        },
        {
            "id": "a6e7e761-5549-4620-bd2e-2be3905414d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa0d732-f73b-4378-99f1-52964dc284bf",
            "compositeImage": {
                "id": "a0a9ab2f-1184-4725-971a-bf6d693e8fdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e7e761-5549-4620-bd2e-2be3905414d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df77f3a2-2220-46de-84d8-caaf5709422f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e7e761-5549-4620-bd2e-2be3905414d4",
                    "LayerId": "89001cee-147a-4b58-bbf6-9b33b587fdd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "89001cee-147a-4b58-bbf6-9b33b587fdd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fa0d732-f73b-4378-99f1-52964dc284bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}