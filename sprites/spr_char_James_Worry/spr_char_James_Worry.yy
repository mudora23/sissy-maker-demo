{
    "id": "70707906-7014-4cf6-bab3-9e009e2c25f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 308,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eacaedf0-970e-4f32-b9d1-d4bf65e9df76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70707906-7014-4cf6-bab3-9e009e2c25f9",
            "compositeImage": {
                "id": "78ee89ff-1b95-4b54-923b-a7ed4c55c6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eacaedf0-970e-4f32-b9d1-d4bf65e9df76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0fba4f-bd7a-4f02-9a9b-f42f235de845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eacaedf0-970e-4f32-b9d1-d4bf65e9df76",
                    "LayerId": "8824e82c-56ef-4916-b95c-2a062acfb2e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "8824e82c-56ef-4916-b95c-2a062acfb2e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70707906-7014-4cf6-bab3-9e009e2c25f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}