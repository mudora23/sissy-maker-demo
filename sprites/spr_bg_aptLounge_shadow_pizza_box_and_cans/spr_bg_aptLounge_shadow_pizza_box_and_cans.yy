{
    "id": "ff87dd6c-07c8-4a74-b7a7-52e87994262e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_pizza_box_and_cans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 572,
    "bbox_left": 301,
    "bbox_right": 568,
    "bbox_top": 405,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1239db16-834e-44c7-b3ab-a1c3280b16dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff87dd6c-07c8-4a74-b7a7-52e87994262e",
            "compositeImage": {
                "id": "1b5a9832-1364-4df7-b6f2-ddbd3ca6334f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1239db16-834e-44c7-b3ab-a1c3280b16dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3892715-2536-412d-a969-7e37c05ab95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1239db16-834e-44c7-b3ab-a1c3280b16dc",
                    "LayerId": "9a60a3fb-0b74-41e2-a464-8a03f95672b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9a60a3fb-0b74-41e2-a464-8a03f95672b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff87dd6c-07c8-4a74-b7a7-52e87994262e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}