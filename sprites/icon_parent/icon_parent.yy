{
    "id": "cbfd0b90-7862-48e5-8b92-838d1b12d038",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_parent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b3f7f006-29ee-43cd-bad3-101d117eb236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfd0b90-7862-48e5-8b92-838d1b12d038",
            "compositeImage": {
                "id": "4e6a6e71-0764-43b6-bf34-d801833b404c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f7f006-29ee-43cd-bad3-101d117eb236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb23340e-921e-4e46-b076-dafd81df9e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f7f006-29ee-43cd-bad3-101d117eb236",
                    "LayerId": "99527610-50be-4770-a97a-b7657cd34862"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "99527610-50be-4770-a97a-b7657cd34862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbfd0b90-7862-48e5-8b92-838d1b12d038",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}