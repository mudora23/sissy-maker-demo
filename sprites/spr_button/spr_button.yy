{
    "id": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "14ae5b9d-14b2-4848-a967-6b3103292155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
            "compositeImage": {
                "id": "e56e7b01-f36b-48a3-8b1b-c05dbce2778c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ae5b9d-14b2-4848-a967-6b3103292155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f4a39b-38e2-4ef5-9a21-41c772bd6f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ae5b9d-14b2-4848-a967-6b3103292155",
                    "LayerId": "11d93f0a-5312-4283-b7f0-1b21f59bd9eb"
                }
            ]
        },
        {
            "id": "d4e2a085-5193-4716-87d7-386bd53b7bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
            "compositeImage": {
                "id": "c059256a-db51-49a6-b047-928fd91d8c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e2a085-5193-4716-87d7-386bd53b7bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d1ccdf-437d-4edf-b528-06f3d5ec79c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e2a085-5193-4716-87d7-386bd53b7bc5",
                    "LayerId": "11d93f0a-5312-4283-b7f0-1b21f59bd9eb"
                }
            ]
        },
        {
            "id": "43ba99ac-589a-4796-809c-274eb95a6dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
            "compositeImage": {
                "id": "237bb6b3-a233-43b6-85a8-b1353a57bdbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ba99ac-589a-4796-809c-274eb95a6dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eff322f-e5f4-45fb-8599-c9424010d4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ba99ac-589a-4796-809c-274eb95a6dbd",
                    "LayerId": "11d93f0a-5312-4283-b7f0-1b21f59bd9eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "11d93f0a-5312-4283-b7f0-1b21f59bd9eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 91,
    "yorig": 21
}