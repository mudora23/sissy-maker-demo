{
    "id": "97428a6c-928e-45d9-a20f-b8cf453f832d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "953e83e5-e5c7-431f-83a7-d2c1c5dc8431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97428a6c-928e-45d9-a20f-b8cf453f832d",
            "compositeImage": {
                "id": "b1514d4f-f355-4bce-a80a-1e2655815b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "953e83e5-e5c7-431f-83a7-d2c1c5dc8431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3807a033-4379-4edc-89dc-400f51b3ddd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "953e83e5-e5c7-431f-83a7-d2c1c5dc8431",
                    "LayerId": "f34590b1-cd6f-488f-bb1d-591353caba35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f34590b1-cd6f-488f-bb1d-591353caba35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97428a6c-928e-45d9-a20f-b8cf453f832d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}