{
    "id": "b787ec4a-f8d4-4cd3-9db0-7cae325b616e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Sophia_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 859,
    "bbox_left": 35,
    "bbox_right": 282,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a3669a7b-28d8-49b1-aa8b-87e3baeefcb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b787ec4a-f8d4-4cd3-9db0-7cae325b616e",
            "compositeImage": {
                "id": "8fa07ef9-95a0-445e-9531-6310d787fce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3669a7b-28d8-49b1-aa8b-87e3baeefcb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445f1171-b31a-46cd-85aa-74bd86dbe387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3669a7b-28d8-49b1-aa8b-87e3baeefcb8",
                    "LayerId": "a4f93254-71cb-409a-a172-79c53bec5b95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 860,
    "layers": [
        {
            "id": "a4f93254-71cb-409a-a172-79c53bec5b95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b787ec4a-f8d4-4cd3-9db0-7cae325b616e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 283,
    "xorig": 0,
    "yorig": 0
}