{
    "id": "1f71d752-5398-4728-a6fe-28c4a6119538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "236cbc57-3fae-4c1c-aade-4d941c66042f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f71d752-5398-4728-a6fe-28c4a6119538",
            "compositeImage": {
                "id": "26b3a5c9-3e95-4421-a21a-b458020cf6bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "236cbc57-3fae-4c1c-aade-4d941c66042f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b6bdaeb-acca-41f9-8045-d7a6e1815bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "236cbc57-3fae-4c1c-aade-4d941c66042f",
                    "LayerId": "c908ce90-6fba-4c0b-b5d6-20d8d1064496"
                }
            ]
        },
        {
            "id": "5cf50453-2b56-4133-a9c6-1381e8fa2b7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f71d752-5398-4728-a6fe-28c4a6119538",
            "compositeImage": {
                "id": "bd193d65-f94a-4323-af0f-f7d7995fe97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf50453-2b56-4133-a9c6-1381e8fa2b7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26fffc2-2323-456c-8d22-412208003b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf50453-2b56-4133-a9c6-1381e8fa2b7a",
                    "LayerId": "c908ce90-6fba-4c0b-b5d6-20d8d1064496"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "c908ce90-6fba-4c0b-b5d6-20d8d1064496",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f71d752-5398-4728-a6fe-28c4a6119538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}