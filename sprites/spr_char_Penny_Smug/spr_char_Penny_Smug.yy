{
    "id": "50cdadde-39e4-418e-8aa6-ab209fb19b7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Penny_Smug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 32,
    "bbox_right": 286,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a09a5d3c-7ed1-473a-9db4-03bb5c99b741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50cdadde-39e4-418e-8aa6-ab209fb19b7c",
            "compositeImage": {
                "id": "52d38686-80b9-4a63-906e-c60d594b1059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a09a5d3c-7ed1-473a-9db4-03bb5c99b741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542140a7-482a-4636-baa7-d0104112bc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a09a5d3c-7ed1-473a-9db4-03bb5c99b741",
                    "LayerId": "ed123248-a0b9-43e4-9d46-7d3c56ecf677"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "ed123248-a0b9-43e4-9d46-7d3c56ecf677",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50cdadde-39e4-418e-8aa6-ab209fb19b7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}