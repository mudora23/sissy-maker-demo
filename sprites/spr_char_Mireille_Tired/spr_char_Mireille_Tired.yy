{
    "id": "ef7509a1-096c-46ca-8298-4d8d06ea3040",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Mireille_Tired",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "de7725eb-b1cc-485a-ae7e-1be06e389316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7509a1-096c-46ca-8298-4d8d06ea3040",
            "compositeImage": {
                "id": "f114ed8d-2a91-48a6-b365-c1baa54ec0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de7725eb-b1cc-485a-ae7e-1be06e389316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3faa9978-6e6c-4ce8-85aa-dfe8566296ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de7725eb-b1cc-485a-ae7e-1be06e389316",
                    "LayerId": "f600962f-7528-43e5-8cda-56c836f20247"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "f600962f-7528-43e5-8cda-56c836f20247",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef7509a1-096c-46ca-8298-4d8d06ea3040",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 390,
    "xorig": 0,
    "yorig": 0
}