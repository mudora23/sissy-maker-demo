{
    "id": "daa848cd-3204-416f-9d3b-1c58668299f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 8,
    "bbox_right": 341,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fe7a15ef-81e0-4791-a6cd-6856909b4135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daa848cd-3204-416f-9d3b-1c58668299f4",
            "compositeImage": {
                "id": "403ec79d-dd3e-42f5-b721-490d41a928cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe7a15ef-81e0-4791-a6cd-6856909b4135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2f6f1b-a3f2-4d9b-8e32-ca37c75b94b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe7a15ef-81e0-4791-a6cd-6856909b4135",
                    "LayerId": "033e75cd-abfa-45b7-be84-322e42096711"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "033e75cd-abfa-45b7-be84-322e42096711",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daa848cd-3204-416f-9d3b-1c58668299f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}