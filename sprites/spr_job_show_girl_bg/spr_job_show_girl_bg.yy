{
    "id": "fe71bb88-c5e1-4175-aa6b-f8dc7d2e65d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_show_girl_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "94687d7b-0a16-4637-b058-c0786f7044c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe71bb88-c5e1-4175-aa6b-f8dc7d2e65d8",
            "compositeImage": {
                "id": "15061dc1-beca-421f-8639-755f1db85a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94687d7b-0a16-4637-b058-c0786f7044c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaece107-8d38-4eda-a0ee-1ea326e404f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94687d7b-0a16-4637-b058-c0786f7044c9",
                    "LayerId": "6b20abf7-22a1-42f1-a5da-25900a73cb2c"
                },
                {
                    "id": "ed824ae1-af13-4e1a-b1c7-2380c011ecdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94687d7b-0a16-4637-b058-c0786f7044c9",
                    "LayerId": "5d4a1f0a-f0bf-4458-a40d-c36481c058e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5d4a1f0a-f0bf-4458-a40d-c36481c058e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe71bb88-c5e1-4175-aa6b-f8dc7d2e65d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 80,
            "visible": true
        },
        {
            "id": "6b20abf7-22a1-42f1-a5da-25900a73cb2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe71bb88-c5e1-4175-aa6b-f8dc7d2e65d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}