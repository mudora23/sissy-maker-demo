{
    "id": "c7f0d183-46eb-412c-b7ff-e5df2c29eff2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Johnny_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2e8ca674-79c1-4539-8348-6500e19e8422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7f0d183-46eb-412c-b7ff-e5df2c29eff2",
            "compositeImage": {
                "id": "c1c68066-665d-4c17-acbe-13e33aa1b922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8ca674-79c1-4539-8348-6500e19e8422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48706579-4f3d-4350-86d7-407d2f68f7bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8ca674-79c1-4539-8348-6500e19e8422",
                    "LayerId": "437579e4-9bc0-4a3a-96c4-3da0095f082e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 682,
    "layers": [
        {
            "id": "437579e4-9bc0-4a3a-96c4-3da0095f082e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7f0d183-46eb-412c-b7ff-e5df2c29eff2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 496,
    "xorig": 0,
    "yorig": 0
}