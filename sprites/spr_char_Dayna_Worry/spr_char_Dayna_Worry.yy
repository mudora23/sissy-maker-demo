{
    "id": "ecc47fef-19cb-47a3-af28-c823011f5953",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Dayna_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 663,
    "bbox_left": 54,
    "bbox_right": 270,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "74e88f89-5511-42a8-a458-2a823779a6c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecc47fef-19cb-47a3-af28-c823011f5953",
            "compositeImage": {
                "id": "a990af2f-3c71-44bf-aae9-44a00672218f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e88f89-5511-42a8-a458-2a823779a6c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8215019-c0c2-46e0-8db1-a5cc96642888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e88f89-5511-42a8-a458-2a823779a6c9",
                    "LayerId": "5b4b3580-f66a-4453-8b7f-e68cc36c8a9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 664,
    "layers": [
        {
            "id": "5b4b3580-f66a-4453-8b7f-e68cc36c8a9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecc47fef-19cb-47a3-af28-c823011f5953",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 0,
    "yorig": 0
}