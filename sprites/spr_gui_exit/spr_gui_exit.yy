{
    "id": "248741df-2284-43fa-9dc5-7708835df1dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 19,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f316e301-fc42-4e2b-bd2a-41d29ba9353d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "compositeImage": {
                "id": "f5516ce6-012f-4068-b6e5-506ad56a3339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f316e301-fc42-4e2b-bd2a-41d29ba9353d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d834c4-f4e7-450f-ad61-266f68f7b297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f316e301-fc42-4e2b-bd2a-41d29ba9353d",
                    "LayerId": "4e96b337-5487-44e3-beb6-87344ba06edd"
                },
                {
                    "id": "7b541fc6-7e9a-4bd4-9d58-2e9c263695fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f316e301-fc42-4e2b-bd2a-41d29ba9353d",
                    "LayerId": "28eda2e5-0205-4905-90f9-3083377c79de"
                },
                {
                    "id": "9e61acde-77b1-47f0-9f97-a85b74fc820e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f316e301-fc42-4e2b-bd2a-41d29ba9353d",
                    "LayerId": "113d1643-2f20-4599-9b92-73394d331b6a"
                }
            ]
        },
        {
            "id": "abd225e9-e1d3-48a6-9dd5-0b83162d2bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "compositeImage": {
                "id": "9449e29f-90d9-4069-8125-1c2881e3ba8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd225e9-e1d3-48a6-9dd5-0b83162d2bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f5a6e4-fa70-490c-a0f8-a5a3dcf6c35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd225e9-e1d3-48a6-9dd5-0b83162d2bf6",
                    "LayerId": "4e96b337-5487-44e3-beb6-87344ba06edd"
                },
                {
                    "id": "95efe498-e47c-4649-8ae4-d1bdabb71fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd225e9-e1d3-48a6-9dd5-0b83162d2bf6",
                    "LayerId": "28eda2e5-0205-4905-90f9-3083377c79de"
                },
                {
                    "id": "a1b2baac-fa40-4292-bc62-4837430ec472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd225e9-e1d3-48a6-9dd5-0b83162d2bf6",
                    "LayerId": "113d1643-2f20-4599-9b92-73394d331b6a"
                }
            ]
        },
        {
            "id": "a8a779a9-eef0-4aea-888d-4c9fa28ee911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "compositeImage": {
                "id": "e92ec9b7-bbc7-4591-9772-27b71f7f0e32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a779a9-eef0-4aea-888d-4c9fa28ee911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a752c6a-9662-467a-8d59-517ec7dc7e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a779a9-eef0-4aea-888d-4c9fa28ee911",
                    "LayerId": "4e96b337-5487-44e3-beb6-87344ba06edd"
                },
                {
                    "id": "8b9c2bf7-fb02-42d8-937b-9b777c07aa4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a779a9-eef0-4aea-888d-4c9fa28ee911",
                    "LayerId": "28eda2e5-0205-4905-90f9-3083377c79de"
                },
                {
                    "id": "f9ae0c2d-7922-4aea-9bb7-c09fcec8ffa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a779a9-eef0-4aea-888d-4c9fa28ee911",
                    "LayerId": "113d1643-2f20-4599-9b92-73394d331b6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "4e96b337-5487-44e3-beb6-87344ba06edd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 20,
            "visible": true
        },
        {
            "id": "28eda2e5-0205-4905-90f9-3083377c79de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 90,
            "visible": true
        },
        {
            "id": "113d1643-2f20-4599-9b92-73394d331b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "248741df-2284-43fa-9dc5-7708835df1dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}