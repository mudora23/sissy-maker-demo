{
    "id": "a918417f-7b17-4faa-9dcf-815283bd8559",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptBedroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ca8f02eb-532d-4adc-9de2-0c4cc6a945ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a918417f-7b17-4faa-9dcf-815283bd8559",
            "compositeImage": {
                "id": "03829a1c-f79e-46b8-8512-cdbc5338360f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8f02eb-532d-4adc-9de2-0c4cc6a945ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "187c364d-fe98-4015-a467-763c3735b6e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8f02eb-532d-4adc-9de2-0c4cc6a945ef",
                    "LayerId": "f6a0f932-387d-4970-bc50-6c678b469b84"
                }
            ]
        },
        {
            "id": "031a487e-ae2c-453d-b14d-3e871c6acc80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a918417f-7b17-4faa-9dcf-815283bd8559",
            "compositeImage": {
                "id": "93315819-682a-4d16-bc68-cf2a407c20ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031a487e-ae2c-453d-b14d-3e871c6acc80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2737fa82-aee7-459a-8db6-f40701c45771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031a487e-ae2c-453d-b14d-3e871c6acc80",
                    "LayerId": "f6a0f932-387d-4970-bc50-6c678b469b84"
                }
            ]
        },
        {
            "id": "51d30dda-2607-45a3-99ec-c4576ab74fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a918417f-7b17-4faa-9dcf-815283bd8559",
            "compositeImage": {
                "id": "f66c4fe3-1025-4947-9d74-90a8042e9a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d30dda-2607-45a3-99ec-c4576ab74fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e176c60-c74d-47df-a749-e2e2161fbe8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d30dda-2607-45a3-99ec-c4576ab74fd5",
                    "LayerId": "f6a0f932-387d-4970-bc50-6c678b469b84"
                }
            ]
        },
        {
            "id": "fd232812-2cd1-41e0-9fcc-88bf9007fcb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a918417f-7b17-4faa-9dcf-815283bd8559",
            "compositeImage": {
                "id": "c0d515b2-53dd-4785-af20-0e1895cf29e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd232812-2cd1-41e0-9fcc-88bf9007fcb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e44396-48a4-48a5-b1d8-e22a5faed104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd232812-2cd1-41e0-9fcc-88bf9007fcb4",
                    "LayerId": "f6a0f932-387d-4970-bc50-6c678b469b84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f6a0f932-387d-4970-bc50-6c678b469b84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a918417f-7b17-4faa-9dcf-815283bd8559",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}