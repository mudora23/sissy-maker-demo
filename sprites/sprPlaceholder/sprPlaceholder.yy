{
    "id": "325e6ce3-84b8-4787-8f03-4be350bc5e94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlaceholder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cd9d4810-aa54-46ba-977a-76ce3a449200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325e6ce3-84b8-4787-8f03-4be350bc5e94",
            "compositeImage": {
                "id": "885b1706-852b-446f-ab89-294400881133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9d4810-aa54-46ba-977a-76ce3a449200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdea9702-b43e-46b1-95a2-def64f920cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9d4810-aa54-46ba-977a-76ce3a449200",
                    "LayerId": "3103a284-b1ec-4127-802a-7cabe21e8cc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3103a284-b1ec-4127-802a-7cabe21e8cc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "325e6ce3-84b8-4787-8f03-4be350bc5e94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}