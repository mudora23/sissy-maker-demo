{
    "id": "7cf8081e-7109-43c6-9eff-b7aa5428d564",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_front_trans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "99f61ded-f600-4680-ae4e-9b43499d29b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cf8081e-7109-43c6-9eff-b7aa5428d564",
            "compositeImage": {
                "id": "9996fb22-4a32-4119-932f-33c494842f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f61ded-f600-4680-ae4e-9b43499d29b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee1b538-dcc3-4992-8c03-28b48965d17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f61ded-f600-4680-ae4e-9b43499d29b8",
                    "LayerId": "ecce07d9-7561-40a5-97aa-82ba73770e17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "ecce07d9-7561-40a5-97aa-82ba73770e17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cf8081e-7109-43c6-9eff-b7aa5428d564",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}