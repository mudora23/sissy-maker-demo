{
    "id": "33b42969-71e5-4bd2-b946-a6f55c8bf959",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_sozore_lotion_popup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 181,
    "bbox_left": 37,
    "bbox_right": 180,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a4c74c77-839b-4249-a3bd-8e7f6bd325a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33b42969-71e5-4bd2-b946-a6f55c8bf959",
            "compositeImage": {
                "id": "43219d7e-e341-4583-879f-ecb14d47aabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c74c77-839b-4249-a3bd-8e7f6bd325a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df734a1-8594-4a56-9f02-ec8995bca2b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c74c77-839b-4249-a3bd-8e7f6bd325a3",
                    "LayerId": "c9270dfa-8bfe-4fa4-811d-118e2127958a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "c9270dfa-8bfe-4fa4-811d-118e2127958a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33b42969-71e5-4bd2-b946-a6f55c8bf959",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 217,
    "xorig": 0,
    "yorig": 0
}