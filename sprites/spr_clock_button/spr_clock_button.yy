{
    "id": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clock_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5f3041fe-7715-4f44-9d49-909541f426ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
            "compositeImage": {
                "id": "db563403-0668-4799-9ee6-87242953340c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3041fe-7715-4f44-9d49-909541f426ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6836dcf6-e0fb-46df-9426-051b4bccaad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3041fe-7715-4f44-9d49-909541f426ac",
                    "LayerId": "c5a2a1a6-8eb2-4e03-b872-24eed835274b"
                }
            ]
        },
        {
            "id": "14fa8643-3176-46e1-9151-21af8e33e734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
            "compositeImage": {
                "id": "961be137-bf20-4ef4-b40f-64ef82192734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fa8643-3176-46e1-9151-21af8e33e734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3972093-2f8e-48d8-98e1-ea6ff72cf91d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fa8643-3176-46e1-9151-21af8e33e734",
                    "LayerId": "c5a2a1a6-8eb2-4e03-b872-24eed835274b"
                }
            ]
        },
        {
            "id": "eaed18a3-8eb6-46da-bb8c-7cc8984aa116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
            "compositeImage": {
                "id": "0f4abb2c-5d98-4cf1-b8f4-01b8b9da5365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaed18a3-8eb6-46da-bb8c-7cc8984aa116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d287b6b3-c649-4101-991a-01980a2650c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaed18a3-8eb6-46da-bb8c-7cc8984aa116",
                    "LayerId": "c5a2a1a6-8eb2-4e03-b872-24eed835274b"
                }
            ]
        },
        {
            "id": "b4550741-72dd-4fe3-9ac3-11a679457871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
            "compositeImage": {
                "id": "79c04aff-3c91-480c-956b-ae083f453a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4550741-72dd-4fe3-9ac3-11a679457871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "058e0740-2106-40e7-998b-b3068b212ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4550741-72dd-4fe3-9ac3-11a679457871",
                    "LayerId": "c5a2a1a6-8eb2-4e03-b872-24eed835274b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "c5a2a1a6-8eb2-4e03-b872-24eed835274b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b644854-52eb-4999-8c32-c0f5d4203ff6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 0
}