{
    "id": "d613ce57-4145-4681-8f3a-fbb22056c289",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_trans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3a43c4ac-0249-4475-8369-71b0c9513f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d613ce57-4145-4681-8f3a-fbb22056c289",
            "compositeImage": {
                "id": "5ee434f7-fe8d-4ead-8f71-526aeedd68d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a43c4ac-0249-4475-8369-71b0c9513f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4184c736-9a4b-4b57-a0ef-abdc622ca45b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a43c4ac-0249-4475-8369-71b0c9513f95",
                    "LayerId": "fffd20fd-184b-4923-9b89-5a968c0e7156"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "fffd20fd-184b-4923-9b89-5a968c0e7156",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d613ce57-4145-4681-8f3a-fbb22056c289",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}