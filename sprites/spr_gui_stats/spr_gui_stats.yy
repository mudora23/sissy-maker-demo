{
    "id": "ba913813-d91c-4ffa-9f37-7e034b0a00f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_stats",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 13,
    "bbox_right": 66,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "be93b838-99f8-460c-a8b0-47fea91812b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba913813-d91c-4ffa-9f37-7e034b0a00f7",
            "compositeImage": {
                "id": "223f5951-2651-48bc-b0c6-ef3ac40de53e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be93b838-99f8-460c-a8b0-47fea91812b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a295123-925b-4b7a-93e8-025a9e7986c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be93b838-99f8-460c-a8b0-47fea91812b2",
                    "LayerId": "ed71a08b-b3af-435c-83a1-1b7412e54290"
                }
            ]
        },
        {
            "id": "fabca0a3-bfe8-4608-92e4-c39cd91fc78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba913813-d91c-4ffa-9f37-7e034b0a00f7",
            "compositeImage": {
                "id": "6875d737-39d9-4a4d-939b-b400c025fa70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fabca0a3-bfe8-4608-92e4-c39cd91fc78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9dd377e-e46c-4023-ab56-65aa9210900e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fabca0a3-bfe8-4608-92e4-c39cd91fc78d",
                    "LayerId": "ed71a08b-b3af-435c-83a1-1b7412e54290"
                }
            ]
        },
        {
            "id": "ff6891a8-e81a-4787-90ca-7f874485ba7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba913813-d91c-4ffa-9f37-7e034b0a00f7",
            "compositeImage": {
                "id": "c61f595f-bd8c-4456-994a-fb7f08539dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6891a8-e81a-4787-90ca-7f874485ba7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce7a4f0-0a8e-4b32-8cac-a22e2e70687e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6891a8-e81a-4787-90ca-7f874485ba7d",
                    "LayerId": "ed71a08b-b3af-435c-83a1-1b7412e54290"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ed71a08b-b3af-435c-83a1-1b7412e54290",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba913813-d91c-4ffa-9f37-7e034b0a00f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}