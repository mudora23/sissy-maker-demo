{
    "id": "9ba2733f-6b36-4c65-84cd-08b76122c7d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_sp_ChrisBlanket01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 667,
    "bbox_left": 27,
    "bbox_right": 217,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "590b08e2-ca20-4b9b-8781-3f0ade835567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba2733f-6b36-4c65-84cd-08b76122c7d6",
            "compositeImage": {
                "id": "11c12407-ca08-430d-83b5-34bc4f6e882e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "590b08e2-ca20-4b9b-8781-3f0ade835567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ac8477-e1c9-4ce8-88ff-4cf8b2c70be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "590b08e2-ca20-4b9b-8781-3f0ade835567",
                    "LayerId": "8a5d4d5c-69b7-4491-9d48-f4f4bc61d9be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 680,
    "layers": [
        {
            "id": "8a5d4d5c-69b7-4491-9d48-f4f4bc61d9be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ba2733f-6b36-4c65-84cd-08b76122c7d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 247,
    "xorig": 0,
    "yorig": 0
}