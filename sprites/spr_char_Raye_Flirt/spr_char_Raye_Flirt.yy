{
    "id": "4d87e03d-1ffe-407a-9677-1bf69ceb4370",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Raye_Flirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "24d3dda0-fe70-4cc6-ad3b-ff08551941c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d87e03d-1ffe-407a-9677-1bf69ceb4370",
            "compositeImage": {
                "id": "a98d19d1-2fcb-4d00-aa94-0ce7e427b7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d3dda0-fe70-4cc6-ad3b-ff08551941c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0745554d-87f8-453a-8280-d3fd20717c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d3dda0-fe70-4cc6-ad3b-ff08551941c3",
                    "LayerId": "8f106500-47ea-4762-9e73-28a84ecee9f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 702,
    "layers": [
        {
            "id": "8f106500-47ea-4762-9e73-28a84ecee9f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d87e03d-1ffe-407a-9677-1bf69ceb4370",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 388,
    "xorig": 0,
    "yorig": 0
}