{
    "id": "3e87b1a8-f0af-4f94-8050-1c0608ac11c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Dayna_Party",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 663,
    "bbox_left": 16,
    "bbox_right": 305,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "91816fa3-0f4a-4b88-aa03-39f1bd44c55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e87b1a8-f0af-4f94-8050-1c0608ac11c4",
            "compositeImage": {
                "id": "8d47f289-fbff-4c1b-847a-5483b5b0375a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91816fa3-0f4a-4b88-aa03-39f1bd44c55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c74f087-3631-45dc-9471-9d3fe3479a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91816fa3-0f4a-4b88-aa03-39f1bd44c55a",
                    "LayerId": "470e91cd-d0e7-410c-b26c-64763c4c0eb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 664,
    "layers": [
        {
            "id": "470e91cd-d0e7-410c-b26c-64763c4c0eb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e87b1a8-f0af-4f94-8050-1c0608ac11c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 0,
    "yorig": 0
}