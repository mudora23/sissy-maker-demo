{
    "id": "fadb2f79-ed08-4c81-8fc6-ceaed66d5299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_load",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 19,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f37d1c89-86c3-4815-92e1-db86fabd0899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadb2f79-ed08-4c81-8fc6-ceaed66d5299",
            "compositeImage": {
                "id": "7a0b287a-bef7-4eec-b557-1d72421209d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37d1c89-86c3-4815-92e1-db86fabd0899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2469fb7e-2338-4f12-b1c2-33420e9abcda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37d1c89-86c3-4815-92e1-db86fabd0899",
                    "LayerId": "20763559-1e90-4bdb-bd04-1f418f2ff614"
                }
            ]
        },
        {
            "id": "45d31e9b-1712-4d4e-bc86-213590916f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadb2f79-ed08-4c81-8fc6-ceaed66d5299",
            "compositeImage": {
                "id": "6bd712db-92dd-49a9-b640-e23d84399068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d31e9b-1712-4d4e-bc86-213590916f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3b3887-b772-433f-a8ee-102952a26dc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d31e9b-1712-4d4e-bc86-213590916f5a",
                    "LayerId": "20763559-1e90-4bdb-bd04-1f418f2ff614"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "20763559-1e90-4bdb-bd04-1f418f2ff614",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fadb2f79-ed08-4c81-8fc6-ceaed66d5299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}