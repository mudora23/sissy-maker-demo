{
    "id": "59f23eb7-9d53-4ef5-af2a-83122fac438c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_LotusClub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "49ee6d3c-908a-472b-886c-9cc41680f3a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59f23eb7-9d53-4ef5-af2a-83122fac438c",
            "compositeImage": {
                "id": "43eccc6e-8016-441e-b35d-2bd9e2a8ed25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ee6d3c-908a-472b-886c-9cc41680f3a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed79ea98-9e6e-4b67-ae30-154647ee0a81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ee6d3c-908a-472b-886c-9cc41680f3a8",
                    "LayerId": "c75189d5-5fbe-464d-a912-e1dc7e6843ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "c75189d5-5fbe-464d-a912-e1dc7e6843ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59f23eb7-9d53-4ef5-af2a-83122fac438c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}