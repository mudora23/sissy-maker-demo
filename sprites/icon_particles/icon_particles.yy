{
    "id": "2b50787a-5eb7-4a3f-bf48-e7e1d716adb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_particles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1e654ef0-e58a-43da-915e-2519d79aeaa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b50787a-5eb7-4a3f-bf48-e7e1d716adb2",
            "compositeImage": {
                "id": "c90c5f56-e5fc-4872-8688-35a5467ce33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e654ef0-e58a-43da-915e-2519d79aeaa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b097fd-1034-4af9-890b-8752a4c3c0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e654ef0-e58a-43da-915e-2519d79aeaa2",
                    "LayerId": "3b26669c-4da6-408b-9dd2-baa99d811503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b26669c-4da6-408b-9dd2-baa99d811503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b50787a-5eb7-4a3f-bf48-e7e1d716adb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}