{
    "id": "fe155e8c-ce38-4f03-b0a0-0f97ab81e3cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_support_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "292d2fbb-0aee-41fe-945c-7ab6063096cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe155e8c-ce38-4f03-b0a0-0f97ab81e3cb",
            "compositeImage": {
                "id": "011ff0ac-0f24-4180-a2e8-e60bab97660d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292d2fbb-0aee-41fe-945c-7ab6063096cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e95b05-ef3e-4744-b21d-b819c5550145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292d2fbb-0aee-41fe-945c-7ab6063096cd",
                    "LayerId": "f9fc1b01-35c3-4e96-bd6d-4efaed7990f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "f9fc1b01-35c3-4e96-bd6d-4efaed7990f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe155e8c-ce38-4f03-b0a0-0f97ab81e3cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 65,
    "yorig": 14
}