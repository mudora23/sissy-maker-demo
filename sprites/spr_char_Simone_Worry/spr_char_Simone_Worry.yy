{
    "id": "2dee017e-387a-46ca-b013-f8fd6d4cc159",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Simone_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 772,
    "bbox_left": 75,
    "bbox_right": 268,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee39e288-622a-41e2-8cc8-ab9057e3707f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dee017e-387a-46ca-b013-f8fd6d4cc159",
            "compositeImage": {
                "id": "13c9cf6c-80d3-464d-81b7-e4a7e969cf59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee39e288-622a-41e2-8cc8-ab9057e3707f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74d95df-cba4-4f1a-809d-169b9710392b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee39e288-622a-41e2-8cc8-ab9057e3707f",
                    "LayerId": "3ac0a693-9314-4a9e-85b4-2335d4fb5e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "3ac0a693-9314-4a9e-85b4-2335d4fb5e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dee017e-387a-46ca-b013-f8fd6d4cc159",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}