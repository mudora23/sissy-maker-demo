{
    "id": "a3cd1054-6707-47f1-98ff-4116beb9291f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ronda_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 814,
    "bbox_left": 56,
    "bbox_right": 242,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "83b34fd3-b207-4879-95ca-25b82f04f157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd1054-6707-47f1-98ff-4116beb9291f",
            "compositeImage": {
                "id": "83248a3b-5775-4145-b518-1ad9300ea10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83b34fd3-b207-4879-95ca-25b82f04f157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8ebcef-6f73-4b8b-8204-ee0e6c65b3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83b34fd3-b207-4879-95ca-25b82f04f157",
                    "LayerId": "9fe4fbc3-db1f-4c0b-8363-83bd5cd7f82d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "9fe4fbc3-db1f-4c0b-8363-83bd5cd7f82d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3cd1054-6707-47f1-98ff-4116beb9291f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}