{
    "id": "a299948c-3737-433c-b0af-0bbd9cd88e7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gallery_table_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 0,
    "bbox_right": 173,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b35c8bf9-8207-43c9-9e9f-e998993b1160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a299948c-3737-433c-b0af-0bbd9cd88e7a",
            "compositeImage": {
                "id": "0649acda-63a4-466c-8f20-33496e0ba20b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35c8bf9-8207-43c9-9e9f-e998993b1160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e00c409-d006-499b-8fef-4c00b6a7b234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35c8bf9-8207-43c9-9e9f-e998993b1160",
                    "LayerId": "222e7ef3-7ede-468f-b24e-957aa1014f38"
                }
            ]
        },
        {
            "id": "72c30548-577e-4817-bed6-01fb62111ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a299948c-3737-433c-b0af-0bbd9cd88e7a",
            "compositeImage": {
                "id": "94fb2c1f-91b2-480a-9970-c373712f98d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c30548-577e-4817-bed6-01fb62111ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e76f13-bed3-4c8a-99a0-9d10838f83fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c30548-577e-4817-bed6-01fb62111ca7",
                    "LayerId": "222e7ef3-7ede-468f-b24e-957aa1014f38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 137,
    "layers": [
        {
            "id": "222e7ef3-7ede-468f-b24e-957aa1014f38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a299948c-3737-433c-b0af-0bbd9cd88e7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 174,
    "xorig": 3,
    "yorig": 3
}