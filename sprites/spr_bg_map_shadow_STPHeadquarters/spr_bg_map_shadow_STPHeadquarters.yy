{
    "id": "16dc7f9f-af5c-4d78-9c55-954b1b61bea4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_STPHeadquarters",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 522,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c0ee7487-fbec-4bc5-a85a-d987976db8ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dc7f9f-af5c-4d78-9c55-954b1b61bea4",
            "compositeImage": {
                "id": "32a6bf12-16a5-4357-9584-69222acead94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ee7487-fbec-4bc5-a85a-d987976db8ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae157f2-ba7f-4c41-9dad-ac4527b8e77e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ee7487-fbec-4bc5-a85a-d987976db8ef",
                    "LayerId": "549535d3-8aee-4914-b2a3-aa7e79545f92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "549535d3-8aee-4914-b2a3-aa7e79545f92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16dc7f9f-af5c-4d78-9c55-954b1b61bea4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}