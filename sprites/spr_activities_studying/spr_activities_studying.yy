{
    "id": "d4fc1e0a-7df3-40c9-9bf2-e377319dfabd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_activities_studying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 498,
    "bbox_left": 84,
    "bbox_right": 339,
    "bbox_top": 80,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ec07c819-7eae-4da8-bdb6-067ab865930c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4fc1e0a-7df3-40c9-9bf2-e377319dfabd",
            "compositeImage": {
                "id": "b77c3d37-507c-41ea-8ee8-e77cac63a2b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec07c819-7eae-4da8-bdb6-067ab865930c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "284dc39c-97d2-448d-8662-f44fac9d300e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec07c819-7eae-4da8-bdb6-067ab865930c",
                    "LayerId": "ed24fcc0-f834-4e9a-8df8-64b1f80a443f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "ed24fcc0-f834-4e9a-8df8-64b1f80a443f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4fc1e0a-7df3-40c9-9bf2-e377319dfabd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}