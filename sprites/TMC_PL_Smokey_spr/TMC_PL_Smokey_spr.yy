{
    "id": "3f394976-f390-4670-9497-eecb6bd61a80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TMC_PL_Smokey_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 5,
    "bbox_right": 92,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d8b722be-2c95-4069-a1e5-e0ca6ab9a848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f394976-f390-4670-9497-eecb6bd61a80",
            "compositeImage": {
                "id": "b3f794c3-2660-4ff4-9749-48e78cf17af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b722be-2c95-4069-a1e5-e0ca6ab9a848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d456fdf2-98c6-4fff-a0ad-b29c2b1e3c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b722be-2c95-4069-a1e5-e0ca6ab9a848",
                    "LayerId": "d85e6179-0bc6-4a57-876b-6993defbd884"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d85e6179-0bc6-4a57-876b-6993defbd884",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f394976-f390-4670-9497-eecb6bd61a80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}