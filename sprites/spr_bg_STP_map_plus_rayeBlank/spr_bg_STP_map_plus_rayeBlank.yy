{
    "id": "aac99991-0ee1-48b7-89f0-5969cf83a432",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_plus_rayeBlank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a5a1c644-b4bc-4a7b-8dd2-6da4f999d792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aac99991-0ee1-48b7-89f0-5969cf83a432",
            "compositeImage": {
                "id": "6ec0c629-e287-46f8-9f3e-d56d8b0e29bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a1c644-b4bc-4a7b-8dd2-6da4f999d792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027629fc-5e0e-4e78-98e1-5b01d29f025f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a1c644-b4bc-4a7b-8dd2-6da4f999d792",
                    "LayerId": "17593e8f-cc9a-4fd9-88e1-5461317a3fb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "17593e8f-cc9a-4fd9-88e1-5461317a3fb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aac99991-0ee1-48b7-89f0-5969cf83a432",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}