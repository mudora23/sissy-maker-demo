{
    "id": "d47313a3-56cd-4fd0-aa56-eaacba4e782b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 308,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6433c9e7-008f-4378-8ef7-fe6b78bea6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d47313a3-56cd-4fd0-aa56-eaacba4e782b",
            "compositeImage": {
                "id": "20d917ff-93ca-43c8-b88d-1dc961efa75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6433c9e7-008f-4378-8ef7-fe6b78bea6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4b9d77a-3b4e-4835-9d24-13770cd6c9ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6433c9e7-008f-4378-8ef7-fe6b78bea6b9",
                    "LayerId": "641630a1-7ed7-4f1a-99d3-ad3d3f59e575"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "641630a1-7ed7-4f1a-99d3-ad3d3f59e575",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d47313a3-56cd-4fd0-aa56-eaacba4e782b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}