{
    "id": "46e045a7-345a-41d6-aa54-5c03f24b7c62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_lotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12757456-2fab-4fc6-8dae-3fe95fba0cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46e045a7-345a-41d6-aa54-5c03f24b7c62",
            "compositeImage": {
                "id": "8d7f2092-48bc-47e7-b893-be496b45701a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12757456-2fab-4fc6-8dae-3fe95fba0cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d861380-57b4-4189-ac46-3449306dbf83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12757456-2fab-4fc6-8dae-3fe95fba0cfd",
                    "LayerId": "b105d835-8ea1-441a-9fd1-fa245f72da83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "b105d835-8ea1-441a-9fd1-fa245f72da83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46e045a7-345a-41d6-aa54-5c03f24b7c62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}