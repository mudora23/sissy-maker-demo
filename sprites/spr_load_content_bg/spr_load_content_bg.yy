{
    "id": "c7874ad6-2fa2-4b33-82d8-d329ae16368f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_load_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b79a072d-e60f-410f-a44c-883fb15dadc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7874ad6-2fa2-4b33-82d8-d329ae16368f",
            "compositeImage": {
                "id": "dde2fb92-5a4c-4f83-86e9-d22aebc92fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b79a072d-e60f-410f-a44c-883fb15dadc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5b47b8-bc20-4f53-980a-a3b3f49217f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b79a072d-e60f-410f-a44c-883fb15dadc1",
                    "LayerId": "9df6c5ab-e2e8-42cd-a4ac-26f600b58fca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9df6c5ab-e2e8-42cd-a4ac-26f600b58fca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7874ad6-2fa2-4b33-82d8-d329ae16368f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}