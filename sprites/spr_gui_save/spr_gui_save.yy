{
    "id": "c9db70a1-25f8-482a-b147-c0f65d1ad1d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_save",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 19,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1edcec5a-f68b-4177-93cd-779740557a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9db70a1-25f8-482a-b147-c0f65d1ad1d2",
            "compositeImage": {
                "id": "56044295-61b4-4717-aeae-1e91ebeffab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1edcec5a-f68b-4177-93cd-779740557a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aea4ac7-f572-4929-9213-4e907b0f042b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1edcec5a-f68b-4177-93cd-779740557a16",
                    "LayerId": "ed22c173-b16d-402d-a15d-d32a1202c49f"
                }
            ]
        },
        {
            "id": "a04f4805-6bb0-4212-970c-72c04602fbd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9db70a1-25f8-482a-b147-c0f65d1ad1d2",
            "compositeImage": {
                "id": "5d23dd4b-f7d3-42c0-8791-79f153d0b8a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04f4805-6bb0-4212-970c-72c04602fbd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af0769a-af1c-4e98-b1d9-5c2f09ea6823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04f4805-6bb0-4212-970c-72c04602fbd1",
                    "LayerId": "ed22c173-b16d-402d-a15d-d32a1202c49f"
                }
            ]
        },
        {
            "id": "275886ad-8000-4f47-aa9e-afb1ba913e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9db70a1-25f8-482a-b147-c0f65d1ad1d2",
            "compositeImage": {
                "id": "cc26ead9-2bf1-4941-afea-230a583287b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275886ad-8000-4f47-aa9e-afb1ba913e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a95f48-875d-4e6e-8b54-d76663c81807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275886ad-8000-4f47-aa9e-afb1ba913e0c",
                    "LayerId": "ed22c173-b16d-402d-a15d-d32a1202c49f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ed22c173-b16d-402d-a15d-d32a1202c49f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9db70a1-25f8-482a-b147-c0f65d1ad1d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}