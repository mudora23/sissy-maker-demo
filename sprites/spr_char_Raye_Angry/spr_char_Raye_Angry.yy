{
    "id": "5a2a3480-0ab6-4e5a-87c8-90b91b524fbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Raye_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2b4e38fe-75ef-4739-86d4-a6212e1fb84d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2a3480-0ab6-4e5a-87c8-90b91b524fbe",
            "compositeImage": {
                "id": "e44dcc80-559a-4a66-adde-baa60e7b26d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4e38fe-75ef-4739-86d4-a6212e1fb84d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54581da-d9fd-4d5a-b8ad-b82b1e6c138c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4e38fe-75ef-4739-86d4-a6212e1fb84d",
                    "LayerId": "2910aa00-ea39-4bf2-b6b5-3d79ac98595b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 702,
    "layers": [
        {
            "id": "2910aa00-ea39-4bf2-b6b5-3d79ac98595b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a2a3480-0ab6-4e5a-87c8-90b91b524fbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 388,
    "xorig": 0,
    "yorig": 0
}