{
    "id": "c2fe0c08-ea2c-429d-b4c1-50e18a73ad14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_questionnaire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 535,
    "bbox_left": 0,
    "bbox_right": 220,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c1215a67-c879-40c2-ba4c-670e7447434d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2fe0c08-ea2c-429d-b4c1-50e18a73ad14",
            "compositeImage": {
                "id": "82a620a4-3046-415b-b1c9-3280c33361a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1215a67-c879-40c2-ba4c-670e7447434d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12292a61-6b66-4b64-b802-67dc80254e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1215a67-c879-40c2-ba4c-670e7447434d",
                    "LayerId": "abfef8f9-3550-487e-bb79-78293c0ffe2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 536,
    "layers": [
        {
            "id": "abfef8f9-3550-487e-bb79-78293c0ffe2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2fe0c08-ea2c-429d-b4c1-50e18a73ad14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 221,
    "xorig": 0,
    "yorig": 0
}