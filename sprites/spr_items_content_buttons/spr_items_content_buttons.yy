{
    "id": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_buttons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 15,
    "bbox_right": 137,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b53814d5-9722-4ceb-9cd8-ad061166bb43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
            "compositeImage": {
                "id": "ae6990ae-ce06-4a53-9f95-2081ae2642c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b53814d5-9722-4ceb-9cd8-ad061166bb43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f813777-7c25-4904-86b2-c0b8ab70fcd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b53814d5-9722-4ceb-9cd8-ad061166bb43",
                    "LayerId": "c8b70c58-b426-4e52-b680-e42337226831"
                }
            ]
        },
        {
            "id": "c0ef415d-41c4-4072-94f9-13dd9f2c32a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
            "compositeImage": {
                "id": "720f2ec6-28e3-4c75-8c8c-3936190eaa81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ef415d-41c4-4072-94f9-13dd9f2c32a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "661aa50e-506a-4133-91c5-85e84de2b4a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ef415d-41c4-4072-94f9-13dd9f2c32a4",
                    "LayerId": "c8b70c58-b426-4e52-b680-e42337226831"
                }
            ]
        },
        {
            "id": "13b8c31d-7947-4b05-a105-052e65751682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
            "compositeImage": {
                "id": "4c0fc312-68e9-4fb7-9f19-a594b938a724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b8c31d-7947-4b05-a105-052e65751682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1d13c3-e1ae-40f0-b3e4-7d87a7c12e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b8c31d-7947-4b05-a105-052e65751682",
                    "LayerId": "c8b70c58-b426-4e52-b680-e42337226831"
                }
            ]
        },
        {
            "id": "c08b9df4-ea8d-4f3f-bc11-9838394f7126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
            "compositeImage": {
                "id": "e7e436c7-057d-49e3-9a2c-3662cf1773e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08b9df4-ea8d-4f3f-bc11-9838394f7126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b89e96-084b-4b9d-b78d-9976830a1f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08b9df4-ea8d-4f3f-bc11-9838394f7126",
                    "LayerId": "c8b70c58-b426-4e52-b680-e42337226831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "c8b70c58-b426-4e52-b680-e42337226831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a65169c4-5bbf-4e4f-814b-cd7db4391e95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}