{
    "id": "97daf235-057e-41d9-8a5c-45ec018466b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gallery_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 187,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a1f14cb3-ba4b-48aa-ab55-3b3b16e2d595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97daf235-057e-41d9-8a5c-45ec018466b7",
            "compositeImage": {
                "id": "55a30627-6e27-4c85-91ce-4ea54d227d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f14cb3-ba4b-48aa-ab55-3b3b16e2d595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e17afd6f-fb71-47f5-a248-6bae98a7707c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f14cb3-ba4b-48aa-ab55-3b3b16e2d595",
                    "LayerId": "bf493ef5-d66a-4792-952d-e59f638f2e9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "bf493ef5-d66a-4792-952d-e59f638f2e9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97daf235-057e-41d9-8a5c-45ec018466b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 188,
    "xorig": 0,
    "yorig": 0
}