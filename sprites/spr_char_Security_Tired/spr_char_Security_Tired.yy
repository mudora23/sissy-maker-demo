{
    "id": "f369f3c9-76b9-4aa5-a220-ddc416af22a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Security_Tired",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 827,
    "bbox_left": 23,
    "bbox_right": 223,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dab0dee9-6538-4f9a-bce7-fda9f3a648ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f369f3c9-76b9-4aa5-a220-ddc416af22a5",
            "compositeImage": {
                "id": "5401fd80-6fea-430c-a282-c2c0853a6661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dab0dee9-6538-4f9a-bce7-fda9f3a648ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a2a656-ab67-4d77-aecb-57cc6cd9c259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dab0dee9-6538-4f9a-bce7-fda9f3a648ee",
                    "LayerId": "e0cb5576-a2f8-40a4-a22c-d1a53f2491ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "e0cb5576-a2f8-40a4-a22c-d1a53f2491ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f369f3c9-76b9-4aa5-a220-ddc416af22a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}