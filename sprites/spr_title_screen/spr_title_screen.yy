{
    "id": "cbe59b03-d964-4067-962f-4189d2e33e6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6216aa9d-cc24-490d-a976-137d2483e8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe59b03-d964-4067-962f-4189d2e33e6e",
            "compositeImage": {
                "id": "b2a810ce-0f79-4675-bd27-28b2e7a00f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6216aa9d-cc24-490d-a976-137d2483e8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "093fb798-ea0f-45bb-b354-4551e08e400c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6216aa9d-cc24-490d-a976-137d2483e8ad",
                    "LayerId": "2ba997cd-b0b9-49a4-ac4e-e666b74c7ff0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "2ba997cd-b0b9-49a4-ac4e-e666b74c7ff0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe59b03-d964-4067-962f-4189d2e33e6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}