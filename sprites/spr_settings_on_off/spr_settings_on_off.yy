{
    "id": "cd503b05-add3-4148-b3d2-1ecdb50bb43b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_on_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 4,
    "bbox_right": 174,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7954e55b-cd2d-4a45-bde7-1701647bffe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd503b05-add3-4148-b3d2-1ecdb50bb43b",
            "compositeImage": {
                "id": "ca39d069-438b-4061-ac80-7773be433891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7954e55b-cd2d-4a45-bde7-1701647bffe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04003aa0-b4b2-4a67-8444-459fe8a4fd88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7954e55b-cd2d-4a45-bde7-1701647bffe5",
                    "LayerId": "81a47990-0bc7-4b68-bfe0-e94a996b65ea"
                }
            ]
        },
        {
            "id": "4145949e-52f0-4491-b0b0-e3d1a4643a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd503b05-add3-4148-b3d2-1ecdb50bb43b",
            "compositeImage": {
                "id": "99893f68-69f5-4222-881b-c785c805926b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4145949e-52f0-4491-b0b0-e3d1a4643a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7e671f-4546-4e8c-bc70-bbcc88da4bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4145949e-52f0-4491-b0b0-e3d1a4643a9b",
                    "LayerId": "81a47990-0bc7-4b68-bfe0-e94a996b65ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 67,
    "layers": [
        {
            "id": "81a47990-0bc7-4b68-bfe0-e94a996b65ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd503b05-add3-4148-b3d2-1ecdb50bb43b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 181,
    "xorig": 0,
    "yorig": 0
}