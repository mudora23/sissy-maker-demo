{
    "id": "bbc23771-19b0-40ca-83fb-53e2eb1909b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Sophia_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 859,
    "bbox_left": 35,
    "bbox_right": 282,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "462f3ffe-332f-4714-8d60-9ca0eedc370a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc23771-19b0-40ca-83fb-53e2eb1909b3",
            "compositeImage": {
                "id": "181f1685-c09b-4e51-9d2b-9d36425c02b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462f3ffe-332f-4714-8d60-9ca0eedc370a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b57349-77b3-40d1-a115-d679904398ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462f3ffe-332f-4714-8d60-9ca0eedc370a",
                    "LayerId": "218d2c7b-fa27-423e-8637-b8a50a65559f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 860,
    "layers": [
        {
            "id": "218d2c7b-fa27-423e-8637-b8a50a65559f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc23771-19b0-40ca-83fb-53e2eb1909b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 283,
    "xorig": 0,
    "yorig": 0
}