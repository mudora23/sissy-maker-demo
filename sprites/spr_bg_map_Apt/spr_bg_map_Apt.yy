{
    "id": "1b4313db-162c-4289-b190-09fdedc0f6cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_Apt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "68e07167-4a6a-4b88-bed4-41c975c9c93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b4313db-162c-4289-b190-09fdedc0f6cf",
            "compositeImage": {
                "id": "8c7fb251-e116-49c8-ba71-74b1e9409730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e07167-4a6a-4b88-bed4-41c975c9c93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9754739a-0b1a-444b-806d-d7295aceae37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e07167-4a6a-4b88-bed4-41c975c9c93c",
                    "LayerId": "739b3daf-65b1-46b9-92d2-1fda3958d9dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "739b3daf-65b1-46b9-92d2-1fda3958d9dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b4313db-162c-4289-b190-09fdedc0f6cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}