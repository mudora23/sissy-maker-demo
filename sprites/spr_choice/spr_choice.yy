{
    "id": "ccf0233a-4011-441e-9b63-eb72e5e1067f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_choice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee2ffaa8-2c79-4d58-94de-de01bcf47c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf0233a-4011-441e-9b63-eb72e5e1067f",
            "compositeImage": {
                "id": "d1498c03-5d4b-4116-b4c8-94b00de92412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2ffaa8-2c79-4d58-94de-de01bcf47c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b194007d-3b3d-4e73-a8e6-e0d9ca3e80aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2ffaa8-2c79-4d58-94de-de01bcf47c19",
                    "LayerId": "34516789-35c2-4aa9-8203-1887536d32af"
                }
            ]
        },
        {
            "id": "ed7dc054-9062-4f50-896b-34c0a6a28ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf0233a-4011-441e-9b63-eb72e5e1067f",
            "compositeImage": {
                "id": "8c634f18-e5f4-472c-93c8-cf973e52bc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7dc054-9062-4f50-896b-34c0a6a28ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d62712-1a5b-4d21-8c5c-b177799f6aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7dc054-9062-4f50-896b-34c0a6a28ea3",
                    "LayerId": "34516789-35c2-4aa9-8203-1887536d32af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "34516789-35c2-4aa9-8203-1887536d32af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccf0233a-4011-441e-9b63-eb72e5e1067f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 50
}