{
    "id": "4c6876cf-0fd7-4bdd-b486-8e925bdc1fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Security_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 849,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5229d2c8-bc64-48ed-a29b-b3dc9887678a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c6876cf-0fd7-4bdd-b486-8e925bdc1fe3",
            "compositeImage": {
                "id": "cd15fc5c-ee13-4083-8ab8-6f27762841a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5229d2c8-bc64-48ed-a29b-b3dc9887678a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0820ce2d-8aa3-4efd-8c44-8bc9cf040cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5229d2c8-bc64-48ed-a29b-b3dc9887678a",
                    "LayerId": "cc0d9de0-7b21-4b6d-98f4-f3496bfaae97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "cc0d9de0-7b21-4b6d-98f4-f3496bfaae97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c6876cf-0fd7-4bdd-b486-8e925bdc1fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}