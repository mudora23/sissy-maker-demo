{
    "id": "97946599-2fb2-4e0f-8db1-dfc621b78a96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clock_stay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 389,
    "bbox_left": 6,
    "bbox_right": 415,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a69a741a-af8f-41e2-af04-508f680d2091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97946599-2fb2-4e0f-8db1-dfc621b78a96",
            "compositeImage": {
                "id": "afd90106-39ca-478e-b041-6c43e2b9b396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69a741a-af8f-41e2-af04-508f680d2091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e86707-ae78-410c-9501-098b4fe8b12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69a741a-af8f-41e2-af04-508f680d2091",
                    "LayerId": "18518a15-f207-46ec-a281-7d495a9c9644"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 392,
    "layers": [
        {
            "id": "18518a15-f207-46ec-a281-7d495a9c9644",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97946599-2fb2-4e0f-8db1-dfc621b78a96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 422,
    "xorig": 0,
    "yorig": 0
}