{
    "id": "3c04768f-2886-42a0-a027-b66dc5077a4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_stats_prev",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2de6178e-2c60-40cb-892a-cbda815c18a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c04768f-2886-42a0-a027-b66dc5077a4b",
            "compositeImage": {
                "id": "70c2212f-6f20-49c8-a7f6-cc7ee21d9c2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de6178e-2c60-40cb-892a-cbda815c18a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39f044d-35da-4335-a594-4fff2bbb7b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de6178e-2c60-40cb-892a-cbda815c18a7",
                    "LayerId": "6b68d891-7e21-4a80-b26e-57ad6df0ecc2"
                }
            ]
        },
        {
            "id": "f5bd6a3d-c855-48ba-8bcf-3e2bc343baca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c04768f-2886-42a0-a027-b66dc5077a4b",
            "compositeImage": {
                "id": "b8dadbde-09b4-4d3d-b336-03e635423037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5bd6a3d-c855-48ba-8bcf-3e2bc343baca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b97fe14-0c43-4b81-ba53-778182180526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5bd6a3d-c855-48ba-8bcf-3e2bc343baca",
                    "LayerId": "6b68d891-7e21-4a80-b26e-57ad6df0ecc2"
                }
            ]
        },
        {
            "id": "34196b55-099a-494f-8be8-6b5915f62238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c04768f-2886-42a0-a027-b66dc5077a4b",
            "compositeImage": {
                "id": "8059f4e2-6340-4c0a-b517-c2986c65c58f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34196b55-099a-494f-8be8-6b5915f62238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0acc9e-0ec2-4d20-aa4e-360a3573fe71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34196b55-099a-494f-8be8-6b5915f62238",
                    "LayerId": "6b68d891-7e21-4a80-b26e-57ad6df0ecc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "6b68d891-7e21-4a80-b26e-57ad6df0ecc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c04768f-2886-42a0-a027-b66dc5077a4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}