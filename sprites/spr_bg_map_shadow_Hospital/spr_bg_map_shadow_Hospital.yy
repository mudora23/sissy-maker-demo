{
    "id": "5066d3a2-21c7-4f11-938b-ed5a4b47a3d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_Hospital",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 35,
    "bbox_right": 342,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "479cad48-fb3c-4e92-bf31-793ef230b5cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5066d3a2-21c7-4f11-938b-ed5a4b47a3d6",
            "compositeImage": {
                "id": "9f461e82-037e-4334-a3f8-0ccda268582f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "479cad48-fb3c-4e92-bf31-793ef230b5cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d303712-0559-4515-9c2a-ea373edcaea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479cad48-fb3c-4e92-bf31-793ef230b5cd",
                    "LayerId": "6c6e2f32-3fb6-4cf2-8e16-751cd5da191b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6c6e2f32-3fb6-4cf2-8e16-751cd5da191b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5066d3a2-21c7-4f11-938b-ed5a4b47a3d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}