{
    "id": "424a8387-d920-4313-bf5d-76a622afb983",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 504,
    "bbox_left": 22,
    "bbox_right": 397,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a9d7953a-3a12-4165-870f-866f47c51789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "424a8387-d920-4313-bf5d-76a622afb983",
            "compositeImage": {
                "id": "0065872d-44c0-4e42-a5fc-947c7c5a5617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9d7953a-3a12-4165-870f-866f47c51789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182ac776-b393-4c1b-9675-cf19a479d93c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9d7953a-3a12-4165-870f-866f47c51789",
                    "LayerId": "cfdb2f67-e65f-493f-a11e-42085abffdd9"
                }
            ]
        },
        {
            "id": "86e51dd6-d313-4989-ab7a-0edf24fb9d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "424a8387-d920-4313-bf5d-76a622afb983",
            "compositeImage": {
                "id": "1c1bde7f-e6fa-4d36-994d-8dd15e1ffd74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e51dd6-d313-4989-ab7a-0edf24fb9d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12390e48-850c-4f97-a3c5-43b22468ffdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e51dd6-d313-4989-ab7a-0edf24fb9d73",
                    "LayerId": "cfdb2f67-e65f-493f-a11e-42085abffdd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 527,
    "layers": [
        {
            "id": "cfdb2f67-e65f-493f-a11e-42085abffdd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "424a8387-d920-4313-bf5d-76a622afb983",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 410,
    "xorig": 0,
    "yorig": 0
}