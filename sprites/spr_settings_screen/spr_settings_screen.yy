{
    "id": "b873ff01-9b93-4d41-a590-10cae9a5c747",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 7,
    "bbox_right": 100,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "31f4d234-1769-487f-a34c-75d9241ac3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b873ff01-9b93-4d41-a590-10cae9a5c747",
            "compositeImage": {
                "id": "479f49dc-17e1-4407-920f-98e443181a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f4d234-1769-487f-a34c-75d9241ac3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2bd8d21-9280-4f2f-a57c-bb3eda0f2ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f4d234-1769-487f-a34c-75d9241ac3f2",
                    "LayerId": "75136ce5-c559-4069-8eff-2e06b99764ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "75136ce5-c559-4069-8eff-2e06b99764ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b873ff01-9b93-4d41-a590-10cae9a5c747",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 0,
    "yorig": 0
}