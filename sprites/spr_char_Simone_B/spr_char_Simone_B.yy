{
    "id": "10b7df1c-a731-4584-b109-ff382d996554",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Simone_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 764,
    "bbox_left": 23,
    "bbox_right": 274,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "06aa7dc4-5df7-42bf-9082-352fee74b846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b7df1c-a731-4584-b109-ff382d996554",
            "compositeImage": {
                "id": "e6024219-04be-4749-b0a8-eeebc1519368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06aa7dc4-5df7-42bf-9082-352fee74b846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "374572de-0790-4753-a50c-137b78f0c3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06aa7dc4-5df7-42bf-9082-352fee74b846",
                    "LayerId": "28ea6ceb-9f17-4865-9c0f-e579f94aee33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "28ea6ceb-9f17-4865-9c0f-e579f94aee33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10b7df1c-a731-4584-b109-ff382d996554",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}