{
    "id": "abd4862b-92c1-4afa-8f9c-01d919995a43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Happy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1e7d082c-d172-437e-b857-c55fe9e3fbfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abd4862b-92c1-4afa-8f9c-01d919995a43",
            "compositeImage": {
                "id": "b1abd7e2-a950-4b11-a4c2-3fe6980589ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e7d082c-d172-437e-b857-c55fe9e3fbfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a02fed-8e3d-4905-af6c-6b6c9b03d465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7d082c-d172-437e-b857-c55fe9e3fbfe",
                    "LayerId": "5be12911-1e73-401c-9f51-fbfac330f8db"
                }
            ]
        },
        {
            "id": "9c25b53d-89c7-4e65-a46f-450938c61038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abd4862b-92c1-4afa-8f9c-01d919995a43",
            "compositeImage": {
                "id": "6e5210f9-322c-4ff9-8a85-2e96b66fe416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c25b53d-89c7-4e65-a46f-450938c61038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13e67ae-ff5c-425d-8b8f-8358a8f314e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c25b53d-89c7-4e65-a46f-450938c61038",
                    "LayerId": "5be12911-1e73-401c-9f51-fbfac330f8db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "5be12911-1e73-401c-9f51-fbfac330f8db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abd4862b-92c1-4afa-8f9c-01d919995a43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}