{
    "id": "da749d30-3c44-463f-9023-fefe94c1e095",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPpennyOffice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ba6db89d-1eab-47ee-972e-bdc3135de7af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da749d30-3c44-463f-9023-fefe94c1e095",
            "compositeImage": {
                "id": "16ce42b1-3b69-47b4-9c9c-4d6b26aebcde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6db89d-1eab-47ee-972e-bdc3135de7af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99bf0f0-1e39-41b5-9c2a-ad195124b12e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6db89d-1eab-47ee-972e-bdc3135de7af",
                    "LayerId": "6f2b2eba-de89-4735-9fd4-82e5eb14d348"
                }
            ]
        },
        {
            "id": "b772d002-6580-42cd-8bce-2534384c4c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da749d30-3c44-463f-9023-fefe94c1e095",
            "compositeImage": {
                "id": "26857675-18d3-47e1-849c-07e78b29997c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b772d002-6580-42cd-8bce-2534384c4c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcfb179-54f9-42e9-9196-006c46f9b76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b772d002-6580-42cd-8bce-2534384c4c97",
                    "LayerId": "6f2b2eba-de89-4735-9fd4-82e5eb14d348"
                }
            ]
        },
        {
            "id": "2e82ace4-db25-43ed-81b1-85e9cb041a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da749d30-3c44-463f-9023-fefe94c1e095",
            "compositeImage": {
                "id": "2f3b6698-a346-455a-a123-116569b53241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e82ace4-db25-43ed-81b1-85e9cb041a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c2f87a-6134-426e-97b1-58af1284c2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e82ace4-db25-43ed-81b1-85e9cb041a8f",
                    "LayerId": "6f2b2eba-de89-4735-9fd4-82e5eb14d348"
                }
            ]
        },
        {
            "id": "f515bfef-ab52-49ea-bc3b-324417a76b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da749d30-3c44-463f-9023-fefe94c1e095",
            "compositeImage": {
                "id": "39da762f-68c0-4e17-8180-0bdeea0b8731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f515bfef-ab52-49ea-bc3b-324417a76b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9d58df9-1fef-4678-aab6-b3be991de9d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f515bfef-ab52-49ea-bc3b-324417a76b20",
                    "LayerId": "6f2b2eba-de89-4735-9fd4-82e5eb14d348"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6f2b2eba-de89-4735-9fd4-82e5eb14d348",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da749d30-3c44-463f-9023-fefe94c1e095",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}