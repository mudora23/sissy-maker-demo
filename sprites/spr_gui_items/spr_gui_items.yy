{
    "id": "303be1a1-ea02-4a5b-9c1e-e87c726cfc46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 16,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dfc6dcf5-1cb0-432d-a380-0cbb3cd266b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303be1a1-ea02-4a5b-9c1e-e87c726cfc46",
            "compositeImage": {
                "id": "c6185cf6-cc47-4152-a510-4a9d20af1e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc6dcf5-1cb0-432d-a380-0cbb3cd266b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee345d0-76f8-42a0-aa56-18a9b7f1b83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc6dcf5-1cb0-432d-a380-0cbb3cd266b8",
                    "LayerId": "08710045-aca7-4db0-83e5-534c30ca4d74"
                }
            ]
        },
        {
            "id": "66a4f12a-56e8-44a8-8e2d-b56950fd0fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303be1a1-ea02-4a5b-9c1e-e87c726cfc46",
            "compositeImage": {
                "id": "e8abef25-2f2c-45ba-8f79-17054126ae2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a4f12a-56e8-44a8-8e2d-b56950fd0fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87492b95-8a1e-400a-9402-89d988baed48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a4f12a-56e8-44a8-8e2d-b56950fd0fea",
                    "LayerId": "08710045-aca7-4db0-83e5-534c30ca4d74"
                }
            ]
        },
        {
            "id": "3f720972-a929-430b-8428-ab645f829787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303be1a1-ea02-4a5b-9c1e-e87c726cfc46",
            "compositeImage": {
                "id": "76b882d5-f350-41fe-bec2-6512c926fedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f720972-a929-430b-8428-ab645f829787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db450664-9014-46e9-882f-46112f16d683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f720972-a929-430b-8428-ab645f829787",
                    "LayerId": "08710045-aca7-4db0-83e5-534c30ca4d74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "08710045-aca7-4db0-83e5-534c30ca4d74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "303be1a1-ea02-4a5b-9c1e-e87c726cfc46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}