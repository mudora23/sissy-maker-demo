{
    "id": "d9ad78eb-cbcf-49b4-872c-64e006bceb1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 91,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "62474511-d8f1-4c04-b6a5-2ba9f2f30bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9ad78eb-cbcf-49b4-872c-64e006bceb1c",
            "compositeImage": {
                "id": "da9692c6-cf36-4c88-93fd-9cebf9a03636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62474511-d8f1-4c04-b6a5-2ba9f2f30bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a517036-9134-419a-832b-0440d2f2f437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62474511-d8f1-4c04-b6a5-2ba9f2f30bdd",
                    "LayerId": "eb13dfd5-dd04-4fd1-8919-bf4d6f7ff7b8"
                }
            ]
        },
        {
            "id": "dfbe8b39-d088-4661-9f50-cd991552e998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9ad78eb-cbcf-49b4-872c-64e006bceb1c",
            "compositeImage": {
                "id": "b84d6be1-fcc2-4e53-8e9e-fb62e0ebd09c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbe8b39-d088-4661-9f50-cd991552e998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333e121d-32bb-4609-adb0-e0beb45e92eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbe8b39-d088-4661-9f50-cd991552e998",
                    "LayerId": "eb13dfd5-dd04-4fd1-8919-bf4d6f7ff7b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "eb13dfd5-dd04-4fd1-8919-bf4d6f7ff7b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9ad78eb-cbcf-49b4-872c-64e006bceb1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 0,
    "yorig": 0
}