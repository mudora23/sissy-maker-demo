{
    "id": "c10fd75b-4b9f-454d-8a59-aae1fb52367d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_previous",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 91,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7be273a2-2cea-4e97-aa4b-891fe126aa2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c10fd75b-4b9f-454d-8a59-aae1fb52367d",
            "compositeImage": {
                "id": "f8c04331-d16d-4aca-8417-ed5fd0e02baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7be273a2-2cea-4e97-aa4b-891fe126aa2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bada2633-4dca-4940-a4b1-53458c3a611f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7be273a2-2cea-4e97-aa4b-891fe126aa2b",
                    "LayerId": "2307b6a7-0e3a-475e-a7f0-b08f8c80a5b6"
                }
            ]
        },
        {
            "id": "2d5633dd-6888-4314-99eb-d5f3dee5c566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c10fd75b-4b9f-454d-8a59-aae1fb52367d",
            "compositeImage": {
                "id": "b624ed89-aea7-422d-9f6f-c8f9f448365c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5633dd-6888-4314-99eb-d5f3dee5c566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22bf5775-42cd-43de-b814-e0f603a4fec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5633dd-6888-4314-99eb-d5f3dee5c566",
                    "LayerId": "2307b6a7-0e3a-475e-a7f0-b08f8c80a5b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "2307b6a7-0e3a-475e-a7f0-b08f8c80a5b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c10fd75b-4b9f-454d-8a59-aae1fb52367d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 0,
    "yorig": 0
}