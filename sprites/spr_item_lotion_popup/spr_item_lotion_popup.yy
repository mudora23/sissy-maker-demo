{
    "id": "91b36fbc-2111-4de5-b1ec-2b3c0befb149",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_lotion_popup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 165,
    "bbox_left": 49,
    "bbox_right": 170,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "14aae44d-c8c1-490c-a0b1-5743f53066b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91b36fbc-2111-4de5-b1ec-2b3c0befb149",
            "compositeImage": {
                "id": "b8b54990-3518-4e50-af5f-d662869c2bc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14aae44d-c8c1-490c-a0b1-5743f53066b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5972d2-14f0-46b8-a779-b04b930f238c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14aae44d-c8c1-490c-a0b1-5743f53066b3",
                    "LayerId": "bf9321c9-b3db-46ce-961a-a8c66b189c3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "bf9321c9-b3db-46ce-961a-a8c66b189c3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91b36fbc-2111-4de5-b1ec-2b3c0befb149",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 217,
    "xorig": 0,
    "yorig": 0
}