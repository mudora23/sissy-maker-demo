{
    "id": "e6124ed8-10f2-40bd-a5e8-8674b2f703b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Horny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 720,
    "bbox_left": 39,
    "bbox_right": 189,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f6be9abe-4f06-434e-979b-c97477b381eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6124ed8-10f2-40bd-a5e8-8674b2f703b3",
            "compositeImage": {
                "id": "e3b6f683-5107-4b3d-9043-d1bd83e2a958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6be9abe-4f06-434e-979b-c97477b381eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3baec929-815a-4d57-b0f4-af58e7cba440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6be9abe-4f06-434e-979b-c97477b381eb",
                    "LayerId": "58d4e0ab-e9b3-4be6-b5fc-03f41769af7a"
                }
            ]
        },
        {
            "id": "739ca17a-d727-4aee-b594-74a495c09cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6124ed8-10f2-40bd-a5e8-8674b2f703b3",
            "compositeImage": {
                "id": "7590fa64-af25-49bf-b9bb-04d80f23f7f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739ca17a-d727-4aee-b594-74a495c09cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a9f79e-0eca-4d73-a1a4-7271fd1a0f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739ca17a-d727-4aee-b594-74a495c09cc4",
                    "LayerId": "58d4e0ab-e9b3-4be6-b5fc-03f41769af7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "58d4e0ab-e9b3-4be6-b5fc-03f41769af7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6124ed8-10f2-40bd-a5e8-8674b2f703b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}