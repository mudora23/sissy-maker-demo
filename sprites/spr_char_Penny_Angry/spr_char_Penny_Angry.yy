{
    "id": "3d7f88d7-4ac9-4152-9616-6cf2c4bf4ce5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Penny_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 32,
    "bbox_right": 286,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b40dcb30-ce9a-4daf-a9a9-62c2ffb3c1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7f88d7-4ac9-4152-9616-6cf2c4bf4ce5",
            "compositeImage": {
                "id": "ead7c9cd-dee6-477a-a284-67f1556d70ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b40dcb30-ce9a-4daf-a9a9-62c2ffb3c1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e3ec96-7a11-43f7-bfef-9821e8af06a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b40dcb30-ce9a-4daf-a9a9-62c2ffb3c1e9",
                    "LayerId": "8e6f32ce-0c42-44d7-8887-808355473425"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "8e6f32ce-0c42-44d7-8887-808355473425",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d7f88d7-4ac9-4152-9616-6cf2c4bf4ce5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}