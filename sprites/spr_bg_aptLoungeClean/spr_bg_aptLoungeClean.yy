{
    "id": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLoungeClean",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e6a75c1b-8bec-484b-939e-3db32c3eb7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
            "compositeImage": {
                "id": "81014227-12cd-4833-8c39-5e0b5fe5a530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a75c1b-8bec-484b-939e-3db32c3eb7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d6021f-30aa-4c94-ab5c-6add320d3a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a75c1b-8bec-484b-939e-3db32c3eb7e8",
                    "LayerId": "cdd97cc1-fc0b-4f6f-be5a-085ba441568c"
                }
            ]
        },
        {
            "id": "890191c9-e847-4ea8-a3f2-ae406ab913cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
            "compositeImage": {
                "id": "221b029a-60df-4dbc-b938-945db06cc015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "890191c9-e847-4ea8-a3f2-ae406ab913cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d752386a-52b6-4e75-9528-4e6b4c75820d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890191c9-e847-4ea8-a3f2-ae406ab913cc",
                    "LayerId": "cdd97cc1-fc0b-4f6f-be5a-085ba441568c"
                }
            ]
        },
        {
            "id": "28d56d00-506e-4807-a5c7-e5c67eb6769d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
            "compositeImage": {
                "id": "d22754f6-2389-448d-8623-20abb020238b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d56d00-506e-4807-a5c7-e5c67eb6769d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77be9179-b4f1-4363-b2bf-ee64339900e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d56d00-506e-4807-a5c7-e5c67eb6769d",
                    "LayerId": "cdd97cc1-fc0b-4f6f-be5a-085ba441568c"
                }
            ]
        },
        {
            "id": "d6d581c0-0cc9-4ee3-9154-3cf8542ba70e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
            "compositeImage": {
                "id": "ed6b1b6f-05d0-4317-94cc-9432838075b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d581c0-0cc9-4ee3-9154-3cf8542ba70e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4353e79-a6c4-4a05-a957-2d8d4cd422e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d581c0-0cc9-4ee3-9154-3cf8542ba70e",
                    "LayerId": "cdd97cc1-fc0b-4f6f-be5a-085ba441568c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "cdd97cc1-fc0b-4f6f-be5a-085ba441568c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d97652d-6d5c-45c8-b724-8b189b61df2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}