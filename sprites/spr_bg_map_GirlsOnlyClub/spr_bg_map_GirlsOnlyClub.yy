{
    "id": "ea3a9db0-cb0e-462e-bdb5-d2fb11d9eac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_GirlsOnlyClub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 329,
    "bbox_right": 588,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5f1bbd0d-27a5-4718-b9ce-3293bba66ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea3a9db0-cb0e-462e-bdb5-d2fb11d9eac0",
            "compositeImage": {
                "id": "a42da2ac-3c3c-4185-a556-863496b8197c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f1bbd0d-27a5-4718-b9ce-3293bba66ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10dc1798-55c0-4567-bff3-09f1b6e69afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f1bbd0d-27a5-4718-b9ce-3293bba66ad7",
                    "LayerId": "56edde1e-3048-4362-850b-42cf1c25159a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "56edde1e-3048-4362-850b-42cf1c25159a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea3a9db0-cb0e-462e-bdb5-d2fb11d9eac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}