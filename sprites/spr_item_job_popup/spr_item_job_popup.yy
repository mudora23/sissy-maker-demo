{
    "id": "dd650d53-f92a-4f77-8926-c920550cc2a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_job_popup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 168,
    "bbox_left": 50,
    "bbox_right": 162,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "11a71c0b-647d-461a-90d5-b32304af3474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd650d53-f92a-4f77-8926-c920550cc2a0",
            "compositeImage": {
                "id": "f14cb541-2420-4c7e-a4e7-45635a27ca41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a71c0b-647d-461a-90d5-b32304af3474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca237d1e-2aa3-40df-a5b9-be7160f1cdaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a71c0b-647d-461a-90d5-b32304af3474",
                    "LayerId": "7cf416f6-b199-4873-a85a-6e1eb16edcb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "7cf416f6-b199-4873-a85a-6e1eb16edcb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd650d53-f92a-4f77-8926-c920550cc2a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 217,
    "xorig": 0,
    "yorig": 0
}