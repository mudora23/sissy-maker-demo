{
    "id": "fbab1443-ad57-4105-8a52-76bb39739108",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_gallery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 19,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a8166a69-9007-4a67-a4e6-5ce71b515560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab1443-ad57-4105-8a52-76bb39739108",
            "compositeImage": {
                "id": "2469031e-5763-432d-9983-fef1db541bc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8166a69-9007-4a67-a4e6-5ce71b515560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f1f261-1b26-4760-8a8c-d6f4cbc696e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8166a69-9007-4a67-a4e6-5ce71b515560",
                    "LayerId": "44d123de-5ff6-45fb-a67a-6c00de45424d"
                }
            ]
        },
        {
            "id": "946dfe19-3e72-4efc-9ac7-1f052e4e1f1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab1443-ad57-4105-8a52-76bb39739108",
            "compositeImage": {
                "id": "145d886a-5d45-42c2-9511-76049aaaba9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "946dfe19-3e72-4efc-9ac7-1f052e4e1f1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e6f7b3-81b3-4e99-a2cf-a4d3164f7999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "946dfe19-3e72-4efc-9ac7-1f052e4e1f1f",
                    "LayerId": "44d123de-5ff6-45fb-a67a-6c00de45424d"
                }
            ]
        },
        {
            "id": "f440442e-aee7-4e48-a80b-6c93e4686a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab1443-ad57-4105-8a52-76bb39739108",
            "compositeImage": {
                "id": "d5015d9b-92b2-483f-aaac-1d2dafccd6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f440442e-aee7-4e48-a80b-6c93e4686a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44d376f-1504-4707-b975-21829d16955d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f440442e-aee7-4e48-a80b-6c93e4686a16",
                    "LayerId": "44d123de-5ff6-45fb-a67a-6c00de45424d"
                }
            ]
        },
        {
            "id": "571410a2-f085-4407-bbe4-5675fb2192bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab1443-ad57-4105-8a52-76bb39739108",
            "compositeImage": {
                "id": "edadb379-29c6-4af7-baa1-62f15d75d770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "571410a2-f085-4407-bbe4-5675fb2192bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c393b69-02f8-4e95-9949-b2704f328eb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "571410a2-f085-4407-bbe4-5675fb2192bf",
                    "LayerId": "44d123de-5ff6-45fb-a67a-6c00de45424d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "44d123de-5ff6-45fb-a67a-6c00de45424d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbab1443-ad57-4105-8a52-76bb39739108",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}