{
    "id": "16429352-8ee0-457c-a4e1-f3518338a8fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_autosave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 7,
    "bbox_right": 144,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f55a0f32-b1cb-457a-ba89-7338814391e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16429352-8ee0-457c-a4e1-f3518338a8fa",
            "compositeImage": {
                "id": "4bd978ba-04c2-4cf0-a709-e483b238d6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55a0f32-b1cb-457a-ba89-7338814391e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e691c336-d1b2-4ea6-8274-f9bea3ba8ea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55a0f32-b1cb-457a-ba89-7338814391e0",
                    "LayerId": "2910e18e-16d8-4d45-89b6-de23857e5125"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "2910e18e-16d8-4d45-89b6-de23857e5125",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16429352-8ee0-457c-a4e1-f3518338a8fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 0,
    "yorig": 0
}