{
    "id": "b073216b-c75f-455a-99e6-a2de4e334ded",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Kane_Smiling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "95519dc8-cb0f-4c66-a1d3-6ce5fac897cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b073216b-c75f-455a-99e6-a2de4e334ded",
            "compositeImage": {
                "id": "7498f4b2-88b0-4a3f-b461-391155261b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95519dc8-cb0f-4c66-a1d3-6ce5fac897cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c2e0a90-f6f1-4570-a3ae-a8e96506e371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95519dc8-cb0f-4c66-a1d3-6ce5fac897cb",
                    "LayerId": "1fd71192-5395-4501-ac0a-3884a344e496"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 779,
    "layers": [
        {
            "id": "1fd71192-5395-4501-ac0a-3884a344e496",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b073216b-c75f-455a-99e6-a2de4e334ded",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 396,
    "xorig": 0,
    "yorig": 0
}