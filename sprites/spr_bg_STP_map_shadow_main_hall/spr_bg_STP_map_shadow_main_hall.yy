{
    "id": "9afedbb6-30e5-4716-9dea-28a0ef35cf07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_main_hall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 376,
    "bbox_left": 123,
    "bbox_right": 650,
    "bbox_top": 188,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9e228fb4-8516-4f67-b9b1-de083644e7af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afedbb6-30e5-4716-9dea-28a0ef35cf07",
            "compositeImage": {
                "id": "2cf1a33d-d1dc-45fe-86d3-21729632d817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e228fb4-8516-4f67-b9b1-de083644e7af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3b2df9-68b7-4244-8028-b63aef46b0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e228fb4-8516-4f67-b9b1-de083644e7af",
                    "LayerId": "e3f216b3-6dff-4263-a64f-4eb4f2011b1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e3f216b3-6dff-4263-a64f-4eb4f2011b1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9afedbb6-30e5-4716-9dea-28a0ef35cf07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}