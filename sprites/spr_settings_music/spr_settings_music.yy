{
    "id": "7be3e2cf-925d-4148-8b4a-3ea8c0355066",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_music",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 7,
    "bbox_right": 189,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "265ce52c-80b2-4ce6-824f-8200e334aabc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7be3e2cf-925d-4148-8b4a-3ea8c0355066",
            "compositeImage": {
                "id": "03e9726c-c0d4-4b7e-a9b7-430b2af55cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265ce52c-80b2-4ce6-824f-8200e334aabc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0340d062-fa64-4b6c-bcf7-51e92ba5a4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265ce52c-80b2-4ce6-824f-8200e334aabc",
                    "LayerId": "f8691bd3-a11d-42e7-9641-f4c95f2492cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "f8691bd3-a11d-42e7-9641-f4c95f2492cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7be3e2cf-925d-4148-8b4a-3ea8c0355066",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 195,
    "xorig": 0,
    "yorig": 0
}