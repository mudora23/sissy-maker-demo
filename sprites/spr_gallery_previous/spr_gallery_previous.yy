{
    "id": "93e787c3-e6e0-4a3e-a7d8-12e61e808206",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gallery_previous",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c21885e1-739e-4bfe-ac70-4f6c351b8d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93e787c3-e6e0-4a3e-a7d8-12e61e808206",
            "compositeImage": {
                "id": "a54de23b-b334-4799-8a18-42268e827428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21885e1-739e-4bfe-ac70-4f6c351b8d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45df192-9bc1-4e9c-90ca-1cef71bfa848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21885e1-739e-4bfe-ac70-4f6c351b8d17",
                    "LayerId": "d073a68a-9fd5-46ee-a9de-3249f9cde85d"
                }
            ]
        },
        {
            "id": "113e9776-7cf7-4b34-8456-6b749d2ca904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93e787c3-e6e0-4a3e-a7d8-12e61e808206",
            "compositeImage": {
                "id": "83cd3ff7-9156-4b05-b6e3-46765508d514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "113e9776-7cf7-4b34-8456-6b749d2ca904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f5b4fe7-f286-4c30-bcfc-75237d1beea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "113e9776-7cf7-4b34-8456-6b749d2ca904",
                    "LayerId": "d073a68a-9fd5-46ee-a9de-3249f9cde85d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "d073a68a-9fd5-46ee-a9de-3249f9cde85d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93e787c3-e6e0-4a3e-a7d8-12e61e808206",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 3,
    "yorig": 3
}