{
    "id": "1fab8cd0-e9d2-4916-b9f0-1c0ca1186a61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_Kanes_Residence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 506,
    "bbox_left": 492,
    "bbox_right": 711,
    "bbox_top": 330,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b50ad548-eabd-41f9-9983-d7118dea903b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fab8cd0-e9d2-4916-b9f0-1c0ca1186a61",
            "compositeImage": {
                "id": "f6ab1433-fae6-4494-851d-9d7fd6013216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50ad548-eabd-41f9-9983-d7118dea903b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4644d7c-9f96-43bf-ac7d-02d4f61a3bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50ad548-eabd-41f9-9983-d7118dea903b",
                    "LayerId": "fde72b68-83c0-46e4-b029-465c69ad0257"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "fde72b68-83c0-46e4-b029-465c69ad0257",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fab8cd0-e9d2-4916-b9f0-1c0ca1186a61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}