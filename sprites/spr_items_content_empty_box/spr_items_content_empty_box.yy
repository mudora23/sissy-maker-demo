{
    "id": "d131d406-4c61-413c-a23e-551820e15603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_empty_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 7,
    "bbox_right": 105,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8aa61f1d-0842-4140-b9a5-0b0752ecfad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d131d406-4c61-413c-a23e-551820e15603",
            "compositeImage": {
                "id": "bc23d7fb-1934-46ca-92d3-b7cc64e36d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa61f1d-0842-4140-b9a5-0b0752ecfad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa0dbb5-495b-4ffb-a293-9b85d57b511f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa61f1d-0842-4140-b9a5-0b0752ecfad5",
                    "LayerId": "72b678c1-fdac-4224-adb9-7af95dde8893"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "72b678c1-fdac-4224-adb9-7af95dde8893",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d131d406-4c61-413c-a23e-551820e15603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 10,
    "yorig": 14
}