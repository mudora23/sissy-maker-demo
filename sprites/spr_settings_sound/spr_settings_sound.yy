{
    "id": "ea3678f6-87e2-4a1c-b055-2439e2f6d955",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_sound",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 7,
    "bbox_right": 208,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a9e1d0cc-f305-4615-8596-393e094d0219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea3678f6-87e2-4a1c-b055-2439e2f6d955",
            "compositeImage": {
                "id": "2d5aab5d-7c11-4040-af5f-edf1214a31e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e1d0cc-f305-4615-8596-393e094d0219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d9a8de3-5287-475a-a36d-3ad21ecaa84d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e1d0cc-f305-4615-8596-393e094d0219",
                    "LayerId": "08dc098f-cbb1-4e37-9819-8357f9c1cc2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "08dc098f-cbb1-4e37-9819-8357f9c1cc2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea3678f6-87e2-4a1c-b055-2439e2f6d955",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 219,
    "xorig": 0,
    "yorig": 0
}