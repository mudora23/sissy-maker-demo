{
    "id": "00e7c22b-85e2-41aa-96d4-1fd8ca38b694",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_invitation_popup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 163,
    "bbox_left": 44,
    "bbox_right": 172,
    "bbox_top": 43,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "993805a0-4267-440d-9f37-7d484a864466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00e7c22b-85e2-41aa-96d4-1fd8ca38b694",
            "compositeImage": {
                "id": "3f326f6f-c700-4a46-be40-f5c51baea870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993805a0-4267-440d-9f37-7d484a864466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b077a8-8534-49cf-bf8c-1855f6385e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993805a0-4267-440d-9f37-7d484a864466",
                    "LayerId": "323d959b-31a4-45ee-b5d4-306b04398698"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "323d959b-31a4-45ee-b5d4-306b04398698",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00e7c22b-85e2-41aa-96d4-1fd8ca38b694",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 217,
    "xorig": 0,
    "yorig": 0
}