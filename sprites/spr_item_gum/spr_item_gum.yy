{
    "id": "2a735a19-8adb-4c78-a649-c84a3482b129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_gum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "981da60f-c30b-4c5b-8a6b-b51072ec8ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a735a19-8adb-4c78-a649-c84a3482b129",
            "compositeImage": {
                "id": "705b17c7-7c2b-4f27-8e0d-f2261cb5d78c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981da60f-c30b-4c5b-8a6b-b51072ec8ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d2489a-9d48-4c90-a5c7-251bf4e9d776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981da60f-c30b-4c5b-8a6b-b51072ec8ad9",
                    "LayerId": "8a2a49aa-a200-4345-ae6e-5529a0e57180"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8a2a49aa-a200-4345-ae6e-5529a0e57180",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a735a19-8adb-4c78-a649-c84a3482b129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}