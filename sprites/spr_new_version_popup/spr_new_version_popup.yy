{
    "id": "ee3eab28-1039-4442-ad76-66a3f076a1a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_new_version_popup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fcb654a4-f217-4bfa-97cc-7fe5dac1819a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee3eab28-1039-4442-ad76-66a3f076a1a3",
            "compositeImage": {
                "id": "34015603-08e5-4a5f-9776-966cfd50f19e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb654a4-f217-4bfa-97cc-7fe5dac1819a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427e928d-5a93-48eb-8079-87b8c2540a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb654a4-f217-4bfa-97cc-7fe5dac1819a",
                    "LayerId": "3de5e6f7-e225-4dc5-8a0e-016206434839"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "3de5e6f7-e225-4dc5-8a0e-016206434839",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee3eab28-1039-4442-ad76-66a3f076a1a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 0,
    "yorig": 0
}