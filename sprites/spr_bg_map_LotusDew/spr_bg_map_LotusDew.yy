{
    "id": "30f1b91b-ec22-4f31-b1d8-a65da5eb6438",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_LotusDew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 415,
    "bbox_left": 7,
    "bbox_right": 223,
    "bbox_top": 265,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "92c1e3ae-f52f-4718-9232-5ccb849ba528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30f1b91b-ec22-4f31-b1d8-a65da5eb6438",
            "compositeImage": {
                "id": "403d3ba3-7219-4eed-81f1-54d2b428572d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c1e3ae-f52f-4718-9232-5ccb849ba528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "341d47a3-e896-415a-a3a0-55fc5c818745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c1e3ae-f52f-4718-9232-5ccb849ba528",
                    "LayerId": "d2c359fd-da81-492a-88b1-e200732e0a75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "d2c359fd-da81-492a-88b1-e200732e0a75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30f1b91b-ec22-4f31-b1d8-a65da5eb6438",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}