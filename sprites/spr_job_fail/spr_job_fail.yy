{
    "id": "6f58d52a-7375-4f1e-9f0b-71232ca1a9ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_fail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 378,
    "bbox_left": 0,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "559464e5-576c-42d9-b150-9d4e4ac8a484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f58d52a-7375-4f1e-9f0b-71232ca1a9ba",
            "compositeImage": {
                "id": "d68d9c05-2b97-4793-a688-c11e9a34ae6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559464e5-576c-42d9-b150-9d4e4ac8a484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60899fb2-4261-4f49-a549-d34651979307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559464e5-576c-42d9-b150-9d4e4ac8a484",
                    "LayerId": "de95dcdd-94c8-4639-b188-2eec8765b759"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 379,
    "layers": [
        {
            "id": "de95dcdd-94c8-4639-b188-2eec8765b759",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f58d52a-7375-4f1e-9f0b-71232ca1a9ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 236,
    "xorig": 0,
    "yorig": 0
}