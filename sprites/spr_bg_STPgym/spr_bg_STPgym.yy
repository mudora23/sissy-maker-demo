{
    "id": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPgym",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "516bfe60-cebd-46be-a983-60fba8742be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
            "compositeImage": {
                "id": "7f20fa33-ed53-4f27-acb1-4225855c5fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516bfe60-cebd-46be-a983-60fba8742be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc4403c-2ecd-46ee-be8d-9120e79ba74a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516bfe60-cebd-46be-a983-60fba8742be2",
                    "LayerId": "27cb5ea1-8e9e-490c-a078-1b349fa79ba5"
                }
            ]
        },
        {
            "id": "d49caa58-1b26-45ec-983d-b4d93b405cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
            "compositeImage": {
                "id": "019f7cd5-146e-4d66-9ad9-ad4ae6093c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49caa58-1b26-45ec-983d-b4d93b405cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf49eda-94d3-4bb0-a1fa-30730def2d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49caa58-1b26-45ec-983d-b4d93b405cc3",
                    "LayerId": "27cb5ea1-8e9e-490c-a078-1b349fa79ba5"
                }
            ]
        },
        {
            "id": "f800f797-bd90-4125-8b91-af43a082e695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
            "compositeImage": {
                "id": "d0c7bd69-df0d-43ee-9e73-c78f223994af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f800f797-bd90-4125-8b91-af43a082e695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed99b87-9382-47b1-9546-b68390eb6705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f800f797-bd90-4125-8b91-af43a082e695",
                    "LayerId": "27cb5ea1-8e9e-490c-a078-1b349fa79ba5"
                }
            ]
        },
        {
            "id": "e7265187-da39-413d-9d21-a12bbd5c0b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
            "compositeImage": {
                "id": "8ae19d56-2abd-4960-a3be-6cfdb850a590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7265187-da39-413d-9d21-a12bbd5c0b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16425af6-7a6e-432f-867c-0c047adf5537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7265187-da39-413d-9d21-a12bbd5c0b54",
                    "LayerId": "27cb5ea1-8e9e-490c-a078-1b349fa79ba5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "27cb5ea1-8e9e-490c-a078-1b349fa79ba5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e736ee7-9724-4530-adfe-ba2514ec16bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}