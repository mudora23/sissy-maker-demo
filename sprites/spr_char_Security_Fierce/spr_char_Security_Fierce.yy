{
    "id": "12eb1e6b-2753-4a6f-adc4-71d148304575",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Security_Fierce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 827,
    "bbox_left": 23,
    "bbox_right": 223,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d23dade0-a232-4b95-8ff5-a38145e744ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12eb1e6b-2753-4a6f-adc4-71d148304575",
            "compositeImage": {
                "id": "752f5ea2-ccb0-4095-a02a-4be331ce56f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23dade0-a232-4b95-8ff5-a38145e744ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce260f56-31cf-4333-8685-8a32248bfe00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23dade0-a232-4b95-8ff5-a38145e744ce",
                    "LayerId": "2ecd2fa6-46aa-462e-8a72-fdc6459fbddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "2ecd2fa6-46aa-462e-8a72-fdc6459fbddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12eb1e6b-2753-4a6f-adc4-71d148304575",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}