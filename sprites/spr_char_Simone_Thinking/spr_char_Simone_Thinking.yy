{
    "id": "52bfb147-599b-466c-be5e-af6777acc274",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Simone_Thinking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 772,
    "bbox_left": 75,
    "bbox_right": 268,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cba32274-77d5-4ea2-b74b-04e699ed886f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52bfb147-599b-466c-be5e-af6777acc274",
            "compositeImage": {
                "id": "cf7f80c3-cd36-45fe-bd4f-9e1fbb3587af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba32274-77d5-4ea2-b74b-04e699ed886f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0069d583-1a2c-49c1-b955-2497db5143ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba32274-77d5-4ea2-b74b-04e699ed886f",
                    "LayerId": "5bb2d9ca-2905-4de3-a976-49967d9c7bee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "5bb2d9ca-2905-4de3-a976-49967d9c7bee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52bfb147-599b-466c-be5e-af6777acc274",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}