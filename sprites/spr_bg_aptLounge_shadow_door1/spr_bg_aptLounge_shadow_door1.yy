{
    "id": "d4857062-92ce-4ad7-b531-90f90c0eb5d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_door1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 411,
    "bbox_left": 151,
    "bbox_right": 221,
    "bbox_top": 103,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c50ca800-7441-4ec7-9077-df6d1a6e3c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4857062-92ce-4ad7-b531-90f90c0eb5d8",
            "compositeImage": {
                "id": "abf7cd28-e52b-4509-9503-4dc07915c18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50ca800-7441-4ec7-9077-df6d1a6e3c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8bb2a8-84b3-4ccb-8c74-3a241f2b8623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50ca800-7441-4ec7-9077-df6d1a6e3c54",
                    "LayerId": "cace7b0e-6183-40a8-b5e4-71d02776502d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "cace7b0e-6183-40a8-b5e4-71d02776502d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4857062-92ce-4ad7-b531-90f90c0eb5d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}