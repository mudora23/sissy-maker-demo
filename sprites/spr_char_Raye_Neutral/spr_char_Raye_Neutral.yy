{
    "id": "65658665-49fb-4ca4-8785-fd1ca1b146cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Raye_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9aa6dd41-9c77-48a0-97e8-ecd2b28ce251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65658665-49fb-4ca4-8785-fd1ca1b146cf",
            "compositeImage": {
                "id": "e2410135-af92-4877-a167-459a985bd90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa6dd41-9c77-48a0-97e8-ecd2b28ce251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d62ae-112c-49d5-bd41-eb65c06f45c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa6dd41-9c77-48a0-97e8-ecd2b28ce251",
                    "LayerId": "14a8b7b8-4309-4dfa-ac1d-61f5f0e9748d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 704,
    "layers": [
        {
            "id": "14a8b7b8-4309-4dfa-ac1d-61f5f0e9748d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65658665-49fb-4ca4-8785-fd1ca1b146cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 388,
    "xorig": 0,
    "yorig": 0
}