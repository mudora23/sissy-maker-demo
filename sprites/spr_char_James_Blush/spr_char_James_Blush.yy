{
    "id": "afa19752-eedc-4caa-a46a-6d74842c6fa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Blush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 326,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f44ade7f-73ed-49ac-b835-306e395c0fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa19752-eedc-4caa-a46a-6d74842c6fa1",
            "compositeImage": {
                "id": "95074493-0110-4832-a4ea-2e0024a8a052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44ade7f-73ed-49ac-b835-306e395c0fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c587df7-87fa-456b-a6e5-4bff1a142a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44ade7f-73ed-49ac-b835-306e395c0fcc",
                    "LayerId": "a0bad97b-c890-46c4-a300-82d8d27c34c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "a0bad97b-c890-46c4-a300-82d8d27c34c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afa19752-eedc-4caa-a46a-6d74842c6fa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}