{
    "id": "c6c25549-0a41-4172-94f6-7b54f36a9cee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_intro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "048e03dc-54a0-4ab6-8d09-e21a9d72e5fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6c25549-0a41-4172-94f6-7b54f36a9cee",
            "compositeImage": {
                "id": "a456a863-3cf6-46dc-96c9-1ecaf952899b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048e03dc-54a0-4ab6-8d09-e21a9d72e5fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "419cc4de-d650-46e0-8893-0bd1e01bf42e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048e03dc-54a0-4ab6-8d09-e21a9d72e5fe",
                    "LayerId": "557ca30a-a36e-43af-930d-5567e381fae6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "557ca30a-a36e-43af-930d-5567e381fae6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6c25549-0a41-4172-94f6-7b54f36a9cee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}