{
    "id": "b8cce064-0fb6-4b01-b0a6-bd2db4c7ea02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_lock_room2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 496,
    "bbox_left": 83,
    "bbox_right": 198,
    "bbox_top": 401,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eabe6c11-f429-4b2b-b0de-be16a0f8eb70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8cce064-0fb6-4b01-b0a6-bd2db4c7ea02",
            "compositeImage": {
                "id": "2e990588-a833-41d3-8165-9d898746c6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eabe6c11-f429-4b2b-b0de-be16a0f8eb70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dfc5b0c-3673-4f7e-bf18-916840271633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eabe6c11-f429-4b2b-b0de-be16a0f8eb70",
                    "LayerId": "b886394b-ad66-441b-b30b-e64d269e8e4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "b886394b-ad66-441b-b30b-e64d269e8e4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8cce064-0fb6-4b01-b0a6-bd2db4c7ea02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}