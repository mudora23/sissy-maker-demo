{
    "id": "011562cc-bd6f-4e9e-b79a-8615b542c697",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ronda_Smug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 814,
    "bbox_left": 56,
    "bbox_right": 242,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8bb3565e-4c76-42b6-b7e2-ee0dce5de1ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "011562cc-bd6f-4e9e-b79a-8615b542c697",
            "compositeImage": {
                "id": "03c99b0e-2421-41c9-937d-bfa7f1fc8b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb3565e-4c76-42b6-b7e2-ee0dce5de1ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d92ab963-cb51-42a7-9df3-f4b66cd6f04a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb3565e-4c76-42b6-b7e2-ee0dce5de1ca",
                    "LayerId": "75e55086-97ca-4355-97c6-0200f9feb773"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "75e55086-97ca-4355-97c6-0200f9feb773",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "011562cc-bd6f-4e9e-b79a-8615b542c697",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}