{
    "id": "1343b515-c477-4953-8e48-5fd0b2d187a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stats_empty_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "97fc1d9e-904d-4b72-bf90-bfd28860e88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1343b515-c477-4953-8e48-5fd0b2d187a9",
            "compositeImage": {
                "id": "4fa786cc-0d02-43fa-872c-dbb890fc38a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fc1d9e-904d-4b72-bf90-bfd28860e88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873d2924-2cea-4522-b130-db70eac8638a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fc1d9e-904d-4b72-bf90-bfd28860e88b",
                    "LayerId": "d2964489-a281-44b0-8cec-c0894be3e4c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "d2964489-a281-44b0-8cec-c0894be3e4c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1343b515-c477-4953-8e48-5fd0b2d187a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}