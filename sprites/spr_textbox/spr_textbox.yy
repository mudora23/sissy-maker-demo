{
    "id": "86ab34c0-cdfe-4199-bf5e-cc3b893073e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0bb025e1-386f-4a99-9179-0f27a35aac6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86ab34c0-cdfe-4199-bf5e-cc3b893073e6",
            "compositeImage": {
                "id": "101230b8-59c4-4c2f-8712-efe3920a8571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb025e1-386f-4a99-9179-0f27a35aac6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f2f5ab4-c5e4-4228-b21d-ae9d5418a5cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb025e1-386f-4a99-9179-0f27a35aac6e",
                    "LayerId": "9e3a8526-1377-443c-956e-4c9b5b1b5635"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "9e3a8526-1377-443c-956e-4c9b5b1b5635",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86ab34c0-cdfe-4199-bf5e-cc3b893073e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}