{
    "id": "45f1b3ce-c391-4290-9b9e-6476e8ec4502",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_parent_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bde19099-ea0f-438d-b1a4-b1e8b6b8914a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f1b3ce-c391-4290-9b9e-6476e8ec4502",
            "compositeImage": {
                "id": "94f6b899-d495-47fc-9bf1-b22b534a6cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde19099-ea0f-438d-b1a4-b1e8b6b8914a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126218b7-cf5c-4617-b932-b6789442a6ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde19099-ea0f-438d-b1a4-b1e8b6b8914a",
                    "LayerId": "4557a323-e94b-4853-b105-3f9dfffdaced"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4557a323-e94b-4853-b105-3f9dfffdaced",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45f1b3ce-c391-4290-9b9e-6476e8ec4502",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}