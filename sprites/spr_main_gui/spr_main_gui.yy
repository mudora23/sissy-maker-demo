{
    "id": "6b27a879-4e45-406c-95b5-793ab96e206b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_main_gui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "24986a4b-c365-43a0-b7ed-19d93ff16a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
            "compositeImage": {
                "id": "04ffa25f-0d82-470b-84be-744d219c6252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24986a4b-c365-43a0-b7ed-19d93ff16a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea12bc99-38d0-4b46-b5aa-dbd43769e5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24986a4b-c365-43a0-b7ed-19d93ff16a9f",
                    "LayerId": "970099c1-9303-491a-a2ec-283d34a10356"
                }
            ]
        },
        {
            "id": "e9a14e04-4261-4ad1-8c25-6f18b19b9ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
            "compositeImage": {
                "id": "af1b91bf-5b8c-4be2-b01a-2ec52064f873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a14e04-4261-4ad1-8c25-6f18b19b9ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d64c23-24fd-4715-91d0-b1ecc569ce03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a14e04-4261-4ad1-8c25-6f18b19b9ddf",
                    "LayerId": "970099c1-9303-491a-a2ec-283d34a10356"
                }
            ]
        },
        {
            "id": "fdacc915-a0fb-48e1-8abc-a40f9c7547ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
            "compositeImage": {
                "id": "ea408ca8-c670-4970-8ca0-1ff5c76726d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdacc915-a0fb-48e1-8abc-a40f9c7547ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb6c13b-88d9-422b-b0d5-09c6f35ba4af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdacc915-a0fb-48e1-8abc-a40f9c7547ee",
                    "LayerId": "970099c1-9303-491a-a2ec-283d34a10356"
                }
            ]
        },
        {
            "id": "d4dfd92e-3404-4ff9-a7b1-f2e14589ac8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
            "compositeImage": {
                "id": "6394c597-813f-4c26-967e-28ac10d2b3c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4dfd92e-3404-4ff9-a7b1-f2e14589ac8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e623986-2815-4e3c-bbe6-f3d62c9f3b46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4dfd92e-3404-4ff9-a7b1-f2e14589ac8f",
                    "LayerId": "970099c1-9303-491a-a2ec-283d34a10356"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "970099c1-9303-491a-a2ec-283d34a10356",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}