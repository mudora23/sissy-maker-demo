{
    "id": "10b84b8c-fd64-401e-ba07-4824e00898e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c23f0cdf-7203-46dc-adff-401c38736f7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b84b8c-fd64-401e-ba07-4824e00898e0",
            "compositeImage": {
                "id": "b7da0902-1f30-4398-9602-f3f6e44b65f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23f0cdf-7203-46dc-adff-401c38736f7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07456135-dfd9-415a-94e2-da5d397b0a6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23f0cdf-7203-46dc-adff-401c38736f7d",
                    "LayerId": "a29eaf74-8dfc-42c5-b767-b4b39c246826"
                }
            ]
        },
        {
            "id": "fbe97ab3-a4e6-4159-8c40-e5fe2ac4d309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b84b8c-fd64-401e-ba07-4824e00898e0",
            "compositeImage": {
                "id": "5a6a98d9-e129-401e-baf1-14b75d2392a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe97ab3-a4e6-4159-8c40-e5fe2ac4d309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cfede07-60f5-4037-845f-a30f184cd468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe97ab3-a4e6-4159-8c40-e5fe2ac4d309",
                    "LayerId": "a29eaf74-8dfc-42c5-b767-b4b39c246826"
                }
            ]
        },
        {
            "id": "2e31bbe6-ff80-4bcf-bc48-a0d4d6171a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b84b8c-fd64-401e-ba07-4824e00898e0",
            "compositeImage": {
                "id": "26963965-f662-4f59-af0a-70db8dc02ee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e31bbe6-ff80-4bcf-bc48-a0d4d6171a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de9514f3-40a0-4c32-913a-6b7cf6488c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e31bbe6-ff80-4bcf-bc48-a0d4d6171a18",
                    "LayerId": "a29eaf74-8dfc-42c5-b767-b4b39c246826"
                }
            ]
        },
        {
            "id": "d1d16442-19bc-489f-b573-dd28368c492d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b84b8c-fd64-401e-ba07-4824e00898e0",
            "compositeImage": {
                "id": "60bcccde-1037-4239-9729-e4e93b26b20d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1d16442-19bc-489f-b573-dd28368c492d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24aed11-ff3a-46ef-907f-a0c276e5ec6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1d16442-19bc-489f-b573-dd28368c492d",
                    "LayerId": "a29eaf74-8dfc-42c5-b767-b4b39c246826"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a29eaf74-8dfc-42c5-b767-b4b39c246826",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10b84b8c-fd64-401e-ba07-4824e00898e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}