{
    "id": "8d05b1a3-8138-4b1b-b7b4-0c0671bfdcb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_dice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 358,
    "bbox_right": 431,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fa908a6f-bd50-4435-bd8c-eb919fba76d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d05b1a3-8138-4b1b-b7b4-0c0671bfdcb6",
            "compositeImage": {
                "id": "d4a3e4d6-fd79-473c-93c2-d039bf8972f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa908a6f-bd50-4435-bd8c-eb919fba76d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da8a8b3-3888-402e-babd-c82c061ce7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa908a6f-bd50-4435-bd8c-eb919fba76d0",
                    "LayerId": "873c3d98-3b6b-45b9-ba30-f980ea3bc5ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "873c3d98-3b6b-45b9-ba30-f980ea3bc5ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d05b1a3-8138-4b1b-b7b4-0c0671bfdcb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}