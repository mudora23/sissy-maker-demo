{
    "id": "b517895f-1ef4-4676-bb2e-d48259c80e6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Pout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 720,
    "bbox_left": 38,
    "bbox_right": 225,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "860caaa3-00c8-4659-833b-7012b42eeb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b517895f-1ef4-4676-bb2e-d48259c80e6d",
            "compositeImage": {
                "id": "acf1323a-99c9-4010-9c8d-b8125bd8e22e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "860caaa3-00c8-4659-833b-7012b42eeb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af72d0f-a3b7-4b83-b5b1-0efd8c6524c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "860caaa3-00c8-4659-833b-7012b42eeb3e",
                    "LayerId": "94ef5fc5-3706-429e-aa7f-7994658766f7"
                }
            ]
        },
        {
            "id": "74f54be5-a975-4177-bfee-53e7f05ffc93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b517895f-1ef4-4676-bb2e-d48259c80e6d",
            "compositeImage": {
                "id": "e0273aba-db0e-4d30-ac6e-16f69bd7337d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f54be5-a975-4177-bfee-53e7f05ffc93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a61aff-3af9-41ad-baf0-b25a60b401ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f54be5-a975-4177-bfee-53e7f05ffc93",
                    "LayerId": "94ef5fc5-3706-429e-aa7f-7994658766f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "94ef5fc5-3706-429e-aa7f-7994658766f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b517895f-1ef4-4676-bb2e-d48259c80e6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}