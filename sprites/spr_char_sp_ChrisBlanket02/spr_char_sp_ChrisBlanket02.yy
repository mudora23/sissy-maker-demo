{
    "id": "c1dc95e0-6a71-43c9-a229-ce4c2c9c8416",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_sp_ChrisBlanket02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1218,
    "bbox_left": 29,
    "bbox_right": 377,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "77782347-fba9-43c4-97a8-f5697d514fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1dc95e0-6a71-43c9-a229-ce4c2c9c8416",
            "compositeImage": {
                "id": "a4432a04-abaf-4d6f-ac36-6a8dbccb57c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77782347-fba9-43c4-97a8-f5697d514fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "959d39db-a427-413c-8fcc-370f06387a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77782347-fba9-43c4-97a8-f5697d514fae",
                    "LayerId": "52a15547-fc0e-4583-87c7-2f2bb0b3b8cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1228,
    "layers": [
        {
            "id": "52a15547-fc0e-4583-87c7-2f2bb0b3b8cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1dc95e0-6a71-43c9-a229-ce4c2c9c8416",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 394,
    "xorig": 0,
    "yorig": 0
}