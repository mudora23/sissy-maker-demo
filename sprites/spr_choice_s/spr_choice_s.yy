{
    "id": "aa06e955-42b6-4adf-b358-9bb20363e916",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_choice_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fea78db7-5733-41a1-b8db-663843337497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa06e955-42b6-4adf-b358-9bb20363e916",
            "compositeImage": {
                "id": "cc2307df-29d4-4208-ab9b-db22d4af91d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea78db7-5733-41a1-b8db-663843337497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "617bd418-5361-400f-8aef-e99ea352903c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea78db7-5733-41a1-b8db-663843337497",
                    "LayerId": "cd8c5db7-6971-46c6-9232-ac70cf5f4f81"
                }
            ]
        },
        {
            "id": "f3f9c18c-1f5f-4c5b-aeb4-b38d92225979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa06e955-42b6-4adf-b358-9bb20363e916",
            "compositeImage": {
                "id": "d1dabd49-2418-4bc0-869a-e17747bf20b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f9c18c-1f5f-4c5b-aeb4-b38d92225979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dee97a9-f832-44c5-b043-16328cb5f9dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f9c18c-1f5f-4c5b-aeb4-b38d92225979",
                    "LayerId": "cd8c5db7-6971-46c6-9232-ac70cf5f4f81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "cd8c5db7-6971-46c6-9232-ac70cf5f4f81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa06e955-42b6-4adf-b358-9bb20363e916",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 50
}