{
    "id": "dab33521-1190-4e09-8134-35931454196d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_STPHeadquarters",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 522,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a7b0bfc3-82a3-43d3-a527-3f60246bd52b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab33521-1190-4e09-8134-35931454196d",
            "compositeImage": {
                "id": "91e30446-c719-4559-b43f-13e003bba41b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b0bfc3-82a3-43d3-a527-3f60246bd52b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde931f5-21d6-4ba8-8595-486adbb876ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b0bfc3-82a3-43d3-a527-3f60246bd52b",
                    "LayerId": "ac338391-28e1-4cc4-bdc4-3c88850e9e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "ac338391-28e1-4cc4-bdc4-3c88850e9e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dab33521-1190-4e09-8134-35931454196d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}