{
    "id": "cb11f0bb-7c12-46b4-b2e0-d35c93aa3859",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 366,
    "bbox_left": 0,
    "bbox_right": 122,
    "bbox_top": 198,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b703b5cb-b239-4a59-aabe-380c74e390b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb11f0bb-7c12-46b4-b2e0-d35c93aa3859",
            "compositeImage": {
                "id": "6c46d992-4f5a-4a71-9e74-ce78c3c35432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b703b5cb-b239-4a59-aabe-380c74e390b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93540090-9329-4e1b-a15e-3cadec71a0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b703b5cb-b239-4a59-aabe-380c74e390b4",
                    "LayerId": "86561c31-7458-46e8-b80d-927eab02a70a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "86561c31-7458-46e8-b80d-927eab02a70a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb11f0bb-7c12-46b4-b2e0-d35c93aa3859",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}