{
    "id": "b5c9ded6-bddb-419b-bc2e-5ad83aca8f7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1d105d10-95ab-46bd-b4d2-d6beaccb99a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5c9ded6-bddb-419b-bc2e-5ad83aca8f7a",
            "compositeImage": {
                "id": "46195d3a-b0e8-472e-a23f-3a61d0720dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d105d10-95ab-46bd-b4d2-d6beaccb99a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb0ae67-1474-4eef-89f2-38ac40c992fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d105d10-95ab-46bd-b4d2-d6beaccb99a1",
                    "LayerId": "abb980a8-b782-4524-aab6-6089df738da1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "abb980a8-b782-4524-aab6-6089df738da1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5c9ded6-bddb-419b-bc2e-5ad83aca8f7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}