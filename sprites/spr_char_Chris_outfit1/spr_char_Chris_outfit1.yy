{
    "id": "6a2fe97f-b433-458e-a64f-f971ea9e1d9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_outfit1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 442,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6818b367-74d6-4e75-9b6b-94663ef443ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2fe97f-b433-458e-a64f-f971ea9e1d9d",
            "compositeImage": {
                "id": "7845224c-bed4-4822-8e30-3bee615764bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6818b367-74d6-4e75-9b6b-94663ef443ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb9a9609-07b2-4c30-b939-858fee0531d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6818b367-74d6-4e75-9b6b-94663ef443ee",
                    "LayerId": "3b3f189b-8c27-4155-86ac-e40ed375e729"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 443,
    "layers": [
        {
            "id": "3b3f189b-8c27-4155-86ac-e40ed375e729",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a2fe97f-b433-458e-a64f-f971ea9e1d9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 58,
    "yorig": 3
}