{
    "id": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_buttons_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 133,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "60fbd611-9459-472f-ba69-c31395add406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "87f2be21-c509-4cd2-9985-b311508411f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60fbd611-9459-472f-ba69-c31395add406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc7ae3c-9745-4fb4-a6a2-05c8f8397a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60fbd611-9459-472f-ba69-c31395add406",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        },
        {
            "id": "bfdeaa6f-be34-45d8-80eb-df1f78178bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "bc699b66-b142-4f63-b944-e5ef1dfdd903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfdeaa6f-be34-45d8-80eb-df1f78178bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dda117f-4e4f-44fd-a50f-80ca94d695f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfdeaa6f-be34-45d8-80eb-df1f78178bd0",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        },
        {
            "id": "0f9f922e-9492-41d7-a768-7d556e7d95b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "e18892c8-f616-436e-8a14-713b749cd60b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9f922e-9492-41d7-a768-7d556e7d95b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b77445-7015-46f6-9410-7fb8e0dd2b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9f922e-9492-41d7-a768-7d556e7d95b8",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        },
        {
            "id": "41a6f052-8504-43eb-b828-dbd6832f45ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "60a05e72-d18e-4d4b-ba99-da6bde7fe2fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a6f052-8504-43eb-b828-dbd6832f45ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f5163e-a377-4cbf-b753-bd2eef62d307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a6f052-8504-43eb-b828-dbd6832f45ef",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        },
        {
            "id": "36ca0859-9151-43f7-92d1-de3fc8e930f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "01a1dbd8-6d55-4de9-9d4c-560a474b2d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ca0859-9151-43f7-92d1-de3fc8e930f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14eb342d-1146-4dbd-a4b5-242b67443de1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ca0859-9151-43f7-92d1-de3fc8e930f2",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        },
        {
            "id": "1f8c7895-dc59-4cb0-a518-1a71f673ec16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "compositeImage": {
                "id": "69b7466d-36d8-4366-8164-640df6b6e692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f8c7895-dc59-4cb0-a518-1a71f673ec16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8615e230-5582-4a7a-9614-39fdc7548bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f8c7895-dc59-4cb0-a518-1a71f673ec16",
                    "LayerId": "69667867-0364-4d52-96f4-ecccbb78438c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "69667867-0364-4d52-96f4-ecccbb78438c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cb13eb1-8737-4cd4-bd46-bdacd25d1759",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}