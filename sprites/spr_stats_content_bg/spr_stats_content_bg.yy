{
    "id": "5064efaa-5933-4dc6-bdaf-847cc0441082",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stats_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c8b6b468-ee8b-47ea-b685-8b305b8c88ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5064efaa-5933-4dc6-bdaf-847cc0441082",
            "compositeImage": {
                "id": "4687e8a4-ad24-4b04-a360-ad69ad79725a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b6b468-ee8b-47ea-b685-8b305b8c88ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f3badab-7a79-4887-aada-15ac28f30779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b6b468-ee8b-47ea-b685-8b305b8c88ab",
                    "LayerId": "ac628f36-2fd3-41c0-a781-be487f89fe68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "ac628f36-2fd3-41c0-a781-be487f89fe68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5064efaa-5933-4dc6-bdaf-847cc0441082",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}