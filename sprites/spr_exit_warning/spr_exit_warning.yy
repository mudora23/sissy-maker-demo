{
    "id": "546de844-7384-4c24-8ed8-c7d3b364f18e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit_warning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 516,
    "bbox_left": 167,
    "bbox_right": 645,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0a49ef9d-1760-48d0-a8a2-871892afddc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "546de844-7384-4c24-8ed8-c7d3b364f18e",
            "compositeImage": {
                "id": "c5225ebe-209e-4aab-8e4f-da73ac672e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a49ef9d-1760-48d0-a8a2-871892afddc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "379cce44-b518-40cb-bfc5-05b71a73a142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a49ef9d-1760-48d0-a8a2-871892afddc1",
                    "LayerId": "e7078bbf-cd09-4165-ac8b-1cb22edf560f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e7078bbf-cd09-4165-ac8b-1cb22edf560f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "546de844-7384-4c24-8ed8-c7d3b364f18e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}