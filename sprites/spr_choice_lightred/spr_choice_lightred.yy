{
    "id": "8cb3a384-78c2-4dfc-abaf-4203e1746228",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_choice_lightred",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c88e27ec-f366-43ec-8502-eb3579389acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cb3a384-78c2-4dfc-abaf-4203e1746228",
            "compositeImage": {
                "id": "44c9a698-25b0-406d-b7a1-6c314e0b6033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c88e27ec-f366-43ec-8502-eb3579389acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a0b009-bffe-4316-928a-685b7d3c127a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c88e27ec-f366-43ec-8502-eb3579389acf",
                    "LayerId": "afee3f32-4615-4674-b4aa-491e1f187c86"
                }
            ]
        },
        {
            "id": "53afd0dd-30ca-4d56-ade6-a3a9f3996e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cb3a384-78c2-4dfc-abaf-4203e1746228",
            "compositeImage": {
                "id": "ff6ae300-7109-4274-a29d-387cb9424f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53afd0dd-30ca-4d56-ade6-a3a9f3996e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a766c7-3117-4169-b4ca-2e45d14bea97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53afd0dd-30ca-4d56-ade6-a3a9f3996e63",
                    "LayerId": "afee3f32-4615-4674-b4aa-491e1f187c86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "afee3f32-4615-4674-b4aa-491e1f187c86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cb3a384-78c2-4dfc-abaf-4203e1746228",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 50
}