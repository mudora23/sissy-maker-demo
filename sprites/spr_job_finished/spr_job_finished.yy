{
    "id": "54500535-6165-43b4-9b38-89fc939f7c6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_finished",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 463,
    "bbox_left": 2,
    "bbox_right": 273,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cc397450-ba3c-4e47-9e28-905adcf7b2af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54500535-6165-43b4-9b38-89fc939f7c6e",
            "compositeImage": {
                "id": "4090b8ba-c92b-4df8-b37c-6ada6bdc714f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc397450-ba3c-4e47-9e28-905adcf7b2af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17926249-4850-4bfd-9008-1f04f8639b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc397450-ba3c-4e47-9e28-905adcf7b2af",
                    "LayerId": "eb253e25-771e-4917-aac9-56a6a861a3f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 468,
    "layers": [
        {
            "id": "eb253e25-771e-4917-aac9-56a6a861a3f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54500535-6165-43b4-9b38-89fc939f7c6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 277,
    "xorig": 0,
    "yorig": 0
}