{
    "id": "76e0fe8d-bade-4f94-bdf6-d3bdae797288",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Sophia_Crazy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 859,
    "bbox_left": 2,
    "bbox_right": 282,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7b9f4999-c381-401c-8ad0-6c3706285f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0fe8d-bade-4f94-bdf6-d3bdae797288",
            "compositeImage": {
                "id": "6d01541a-bfca-4837-9ec3-fdf61536f8a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9f4999-c381-401c-8ad0-6c3706285f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b5800a-0178-4a86-a281-5830388a1173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9f4999-c381-401c-8ad0-6c3706285f7b",
                    "LayerId": "01ff1d4f-10ff-47a3-8b2b-b3239473221e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 860,
    "layers": [
        {
            "id": "01ff1d4f-10ff-47a3-8b2b-b3239473221e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76e0fe8d-bade-4f94-bdf6-d3bdae797288",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 283,
    "xorig": 0,
    "yorig": 0
}