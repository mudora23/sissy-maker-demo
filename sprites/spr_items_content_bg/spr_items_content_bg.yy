{
    "id": "a2a064b2-f3cf-480f-accb-67d107aafe83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6ec1a13a-eb9c-4b79-9cbd-7630a01bd957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2a064b2-f3cf-480f-accb-67d107aafe83",
            "compositeImage": {
                "id": "eed9517a-6f47-4e07-8c08-5ba22172caa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec1a13a-eb9c-4b79-9cbd-7630a01bd957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0672dd5-2959-4ec4-998d-5929585ce333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec1a13a-eb9c-4b79-9cbd-7630a01bd957",
                    "LayerId": "54f29e87-98e7-41c1-9c50-bf022e01534f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "54f29e87-98e7-41c1-9c50-bf022e01534f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2a064b2-f3cf-480f-accb-67d107aafe83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}