{
    "id": "1f7095e3-a9f8-4e23-82e7-226965a3c971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TMC_PL_Cloud_2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "758bf52b-7466-436a-918c-0d006e30fcfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7095e3-a9f8-4e23-82e7-226965a3c971",
            "compositeImage": {
                "id": "54dc61d9-177f-4b3d-a445-2ec29296145d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758bf52b-7466-436a-918c-0d006e30fcfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720fc52b-9b1d-4b36-b3e1-cc319a1698a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758bf52b-7466-436a-918c-0d006e30fcfd",
                    "LayerId": "33535844-a896-45d2-9184-d2820f244c6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "33535844-a896-45d2-9184-d2820f244c6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f7095e3-a9f8-4e23-82e7-226965a3c971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}