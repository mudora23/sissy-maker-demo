{
    "id": "28e98ea1-d326-43ee-981c-cb294fd4e45b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Kane_Frustrated",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e2063d31-7f4b-4394-9885-79f8dc40a7be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28e98ea1-d326-43ee-981c-cb294fd4e45b",
            "compositeImage": {
                "id": "3f493930-f019-4bcf-b2f5-10cb2faf1ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2063d31-7f4b-4394-9885-79f8dc40a7be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be72bb13-4cbe-40da-8696-152e4e309b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2063d31-7f4b-4394-9885-79f8dc40a7be",
                    "LayerId": "65813e96-c7e0-475b-9de2-706f353b3944"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 779,
    "layers": [
        {
            "id": "65813e96-c7e0-475b-9de2-706f353b3944",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28e98ea1-d326-43ee-981c-cb294fd4e45b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 396,
    "xorig": 0,
    "yorig": 0
}