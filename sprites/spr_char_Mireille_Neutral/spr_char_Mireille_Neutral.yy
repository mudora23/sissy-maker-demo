{
    "id": "1d8d1492-4290-4b7f-bb4f-8eabd71120bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Mireille_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0748150a-7c92-4079-a935-7d5bb12c93ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8d1492-4290-4b7f-bb4f-8eabd71120bd",
            "compositeImage": {
                "id": "88ae52ff-36e6-41c0-9df5-bafa8ebe84f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0748150a-7c92-4079-a935-7d5bb12c93ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b195afce-3db0-415f-8b44-ad9d075f0df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0748150a-7c92-4079-a935-7d5bb12c93ce",
                    "LayerId": "34e1847b-6823-479d-a29f-512f847682e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "34e1847b-6823-479d-a29f-512f847682e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d8d1492-4290-4b7f-bb4f-8eabd71120bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 390,
    "xorig": 0,
    "yorig": 0
}