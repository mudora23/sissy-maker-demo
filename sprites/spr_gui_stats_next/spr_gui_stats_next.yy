{
    "id": "c2fcce7c-7516-4654-b028-2dc178a16f05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_stats_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7fea4438-ff25-4b32-a9d0-0750eec360c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2fcce7c-7516-4654-b028-2dc178a16f05",
            "compositeImage": {
                "id": "16f884e7-5238-45c4-a313-3c8c114dea90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fea4438-ff25-4b32-a9d0-0750eec360c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182bf765-78ab-46fb-a6ea-f05712704cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fea4438-ff25-4b32-a9d0-0750eec360c8",
                    "LayerId": "dc2f30b6-05f8-4c83-8f7c-2d4bedc9a4c1"
                }
            ]
        },
        {
            "id": "be71b6c6-42ed-43f6-a22e-5d12d225c50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2fcce7c-7516-4654-b028-2dc178a16f05",
            "compositeImage": {
                "id": "a64baa5b-62b3-4447-a285-f98da2b18b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be71b6c6-42ed-43f6-a22e-5d12d225c50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a19976e-fcd5-4cca-912f-918b363b122b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be71b6c6-42ed-43f6-a22e-5d12d225c50f",
                    "LayerId": "dc2f30b6-05f8-4c83-8f7c-2d4bedc9a4c1"
                }
            ]
        },
        {
            "id": "5c73d419-00a9-4d2d-96f4-a98fb07e6856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2fcce7c-7516-4654-b028-2dc178a16f05",
            "compositeImage": {
                "id": "70535f6d-d3cf-4c26-a56b-6f2150e59aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c73d419-00a9-4d2d-96f4-a98fb07e6856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76da13c4-4fcb-4bfd-b6e4-3fba6fa6bdfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c73d419-00a9-4d2d-96f4-a98fb07e6856",
                    "LayerId": "dc2f30b6-05f8-4c83-8f7c-2d4bedc9a4c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "dc2f30b6-05f8-4c83-8f7c-2d4bedc9a4c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2fcce7c-7516-4654-b028-2dc178a16f05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}