{
    "id": "046dc01e-4af1-4c6f-9ab9-6c6363ae1ac6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_sp_JamesIntro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 911,
    "bbox_left": 74,
    "bbox_right": 523,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a188f139-347a-4f44-a6c7-86591e877ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046dc01e-4af1-4c6f-9ab9-6c6363ae1ac6",
            "compositeImage": {
                "id": "5ae9c96e-dda2-483d-b8b4-62017890d88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a188f139-347a-4f44-a6c7-86591e877ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1716e9ad-bbc9-460a-8238-1eddafe1e814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a188f139-347a-4f44-a6c7-86591e877ca9",
                    "LayerId": "5bf39823-22b0-4695-a25e-37c3675474ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 912,
    "layers": [
        {
            "id": "5bf39823-22b0-4695-a25e-37c3675474ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "046dc01e-4af1-4c6f-9ab9-6c6363ae1ac6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 547,
    "xorig": 0,
    "yorig": 0
}