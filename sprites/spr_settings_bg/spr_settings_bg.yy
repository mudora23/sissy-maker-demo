{
    "id": "7c14bc38-e1ff-490d-a2cd-d9dcdefcd4da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 6,
    "bbox_right": 673,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "716d542e-a0aa-4ee6-8133-49b869074e0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c14bc38-e1ff-490d-a2cd-d9dcdefcd4da",
            "compositeImage": {
                "id": "7d3855a4-6c8a-4c28-ad98-567a06841358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "716d542e-a0aa-4ee6-8133-49b869074e0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dee166b-998a-4654-a90e-df72d46db55b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "716d542e-a0aa-4ee6-8133-49b869074e0e",
                    "LayerId": "9d3ef373-f362-487e-a323-d3f6f998cb22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "9d3ef373-f362-487e-a323-d3f6f998cb22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c14bc38-e1ff-490d-a2cd-d9dcdefcd4da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 681,
    "xorig": 0,
    "yorig": 0
}