{
    "id": "a569553c-21c3-4c43-a3f1-f89365e590e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_raye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 177,
    "bbox_left": 132,
    "bbox_right": 251,
    "bbox_top": 66,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "40eb001a-0632-420c-bb5f-52dbf1b5e953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a569553c-21c3-4c43-a3f1-f89365e590e1",
            "compositeImage": {
                "id": "38a6ba3d-0cc1-40cc-8e83-365b4136850a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40eb001a-0632-420c-bb5f-52dbf1b5e953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951f21ae-083b-4790-ac14-149f59819efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40eb001a-0632-420c-bb5f-52dbf1b5e953",
                    "LayerId": "1f95085e-1198-479f-b9e9-ef27dfe72b9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "1f95085e-1198-479f-b9e9-ef27dfe72b9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a569553c-21c3-4c43-a3f1-f89365e590e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}