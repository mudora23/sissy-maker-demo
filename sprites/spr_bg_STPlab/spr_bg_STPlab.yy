{
    "id": "b74b053d-ee91-49f0-a447-677319bd3018",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPlab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 800,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9921fb89-fa56-4a02-bb56-f2510b6747cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74b053d-ee91-49f0-a447-677319bd3018",
            "compositeImage": {
                "id": "721e4f1a-b8b1-4f47-9e83-b42705d631e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9921fb89-fa56-4a02-bb56-f2510b6747cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9495ef2b-d3fa-4738-99e9-d60a69f137f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9921fb89-fa56-4a02-bb56-f2510b6747cb",
                    "LayerId": "613d82a4-da39-4e45-86df-a94ff45b5a6d"
                }
            ]
        },
        {
            "id": "56bcbfb4-93ed-4df4-ad8c-11de9de92027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74b053d-ee91-49f0-a447-677319bd3018",
            "compositeImage": {
                "id": "acad2c78-196b-4b8a-a494-024d96a1c091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bcbfb4-93ed-4df4-ad8c-11de9de92027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a0cea4e-d63f-44b6-a3cb-b1b107b0a9a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bcbfb4-93ed-4df4-ad8c-11de9de92027",
                    "LayerId": "613d82a4-da39-4e45-86df-a94ff45b5a6d"
                }
            ]
        },
        {
            "id": "7ef77ae6-44bb-4e38-bedf-c96000b8b073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74b053d-ee91-49f0-a447-677319bd3018",
            "compositeImage": {
                "id": "4408fad3-dc19-4dc1-b4be-3f8027bdb93d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef77ae6-44bb-4e38-bedf-c96000b8b073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e8e5628-c8bd-412e-8307-b418a67e3068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef77ae6-44bb-4e38-bedf-c96000b8b073",
                    "LayerId": "613d82a4-da39-4e45-86df-a94ff45b5a6d"
                }
            ]
        },
        {
            "id": "ab693e00-123c-4547-a71d-3d0a00feaa7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74b053d-ee91-49f0-a447-677319bd3018",
            "compositeImage": {
                "id": "ae5e59fc-ff12-4499-9c12-76cc34073a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab693e00-123c-4547-a71d-3d0a00feaa7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef14db02-0ff8-41ac-932c-c02cb5063b27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab693e00-123c-4547-a71d-3d0a00feaa7d",
                    "LayerId": "613d82a4-da39-4e45-86df-a94ff45b5a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "613d82a4-da39-4e45-86df-a94ff45b5a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b74b053d-ee91-49f0-a447-677319bd3018",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 801,
    "xorig": 0,
    "yorig": 0
}