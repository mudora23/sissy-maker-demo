{
    "id": "8c2c0924-a64f-4dca-b2bb-386b3eafd778",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_save_delete",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "04c26423-ee55-4d20-8ba1-7c6b49c58f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c2c0924-a64f-4dca-b2bb-386b3eafd778",
            "compositeImage": {
                "id": "151bcf68-a1fb-4957-995d-e1065f0a3bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c26423-ee55-4d20-8ba1-7c6b49c58f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "986c8cea-64a8-4258-b0a1-91b92b9ef665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c26423-ee55-4d20-8ba1-7c6b49c58f51",
                    "LayerId": "08859317-e130-4cbd-af12-4b9486c702aa"
                }
            ]
        },
        {
            "id": "8fee97ca-9887-415a-bf35-e14e006afde3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c2c0924-a64f-4dca-b2bb-386b3eafd778",
            "compositeImage": {
                "id": "f8adfa99-7603-4c92-afb2-c1f679b1a73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fee97ca-9887-415a-bf35-e14e006afde3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c040270f-2943-4d28-822a-8642eb936405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fee97ca-9887-415a-bf35-e14e006afde3",
                    "LayerId": "08859317-e130-4cbd-af12-4b9486c702aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "08859317-e130-4cbd-af12-4b9486c702aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c2c0924-a64f-4dca-b2bb-386b3eafd778",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 0,
    "yorig": 0
}