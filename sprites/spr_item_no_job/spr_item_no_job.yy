{
    "id": "eed944bd-9a80-4a9f-8b70-41862f583fce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_no_job",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7703daf1-edcc-4971-a403-c27bebd0d079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed944bd-9a80-4a9f-8b70-41862f583fce",
            "compositeImage": {
                "id": "fc373e66-d70a-434b-9114-4cb20c1590b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7703daf1-edcc-4971-a403-c27bebd0d079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c6dd27-f5f9-4f19-9894-5c283a9cd080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7703daf1-edcc-4971-a403-c27bebd0d079",
                    "LayerId": "a461526c-9fcb-4c87-adbc-731e255f23c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "a461526c-9fcb-4c87-adbc-731e255f23c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eed944bd-9a80-4a9f-8b70-41862f583fce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}