{
    "id": "97743ba1-73a0-4176-bac6-3e62b90b9f22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gallery_table",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 489,
    "bbox_left": 0,
    "bbox_right": 779,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ece9f41e-f820-441d-af85-a4ca36a16f89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97743ba1-73a0-4176-bac6-3e62b90b9f22",
            "compositeImage": {
                "id": "2b088c48-245a-47bb-8c24-906c9f13fbc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece9f41e-f820-441d-af85-a4ca36a16f89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e3fe30-ba61-48e2-9238-4f162a77019b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece9f41e-f820-441d-af85-a4ca36a16f89",
                    "LayerId": "f34c690f-bfd0-419a-b0e3-54528d96b624"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 490,
    "layers": [
        {
            "id": "f34c690f-bfd0-419a-b0e3-54528d96b624",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97743ba1-73a0-4176-bac6-3e62b90b9f22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 780,
    "xorig": 0,
    "yorig": 0
}