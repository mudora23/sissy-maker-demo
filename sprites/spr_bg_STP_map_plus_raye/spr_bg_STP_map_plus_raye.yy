{
    "id": "19562a30-921d-446b-8efb-0d7b9b9b0193",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_plus_raye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8fbddd14-ad0c-4429-aa47-b827f0772be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19562a30-921d-446b-8efb-0d7b9b9b0193",
            "compositeImage": {
                "id": "2df65a11-cfad-48e8-91b5-a7c7d453514d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fbddd14-ad0c-4429-aa47-b827f0772be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c9dcc2-498c-418e-a2ca-b38ab4da9849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fbddd14-ad0c-4429-aa47-b827f0772be6",
                    "LayerId": "9bfc6637-0f2e-4709-a2a3-38ef44325966"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9bfc6637-0f2e-4709-a2a3-38ef44325966",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19562a30-921d-446b-8efb-0d7b9b9b0193",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}