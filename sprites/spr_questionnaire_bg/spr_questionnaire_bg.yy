{
    "id": "0bd3207f-408b-4f1e-bb61-3d5cf7122c8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_questionnaire_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ca7d4e10-e8ec-430e-a538-55be6ba1f0cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd3207f-408b-4f1e-bb61-3d5cf7122c8a",
            "compositeImage": {
                "id": "302672e7-101d-4469-9791-ebe69b26f46e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7d4e10-e8ec-430e-a538-55be6ba1f0cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9bcb187-5497-4cab-b555-b1d4b82c7bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7d4e10-e8ec-430e-a538-55be6ba1f0cb",
                    "LayerId": "bae4467f-6f3d-4dfa-8825-128d3c10a1bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "bae4467f-6f3d-4dfa-8825-128d3c10a1bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bd3207f-408b-4f1e-bb61-3d5cf7122c8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}