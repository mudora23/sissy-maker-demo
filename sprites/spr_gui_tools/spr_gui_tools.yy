{
    "id": "e677ad1b-9dcd-45bd-a8cb-0c255bd03057",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_tools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4c1c1352-9a32-496a-b9f1-f1a03f43d12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e677ad1b-9dcd-45bd-a8cb-0c255bd03057",
            "compositeImage": {
                "id": "8df49a51-3ea3-4a23-9134-e7345b342320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1c1352-9a32-496a-b9f1-f1a03f43d12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2748c1-be8a-4d9c-bb54-a505a0020604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1c1352-9a32-496a-b9f1-f1a03f43d12c",
                    "LayerId": "1fecd87d-ec3b-4b4c-a834-935f2d8b0fe2"
                }
            ]
        },
        {
            "id": "39529826-6a76-440f-813c-3948b3c1f941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e677ad1b-9dcd-45bd-a8cb-0c255bd03057",
            "compositeImage": {
                "id": "c99c532a-e9a6-482b-a043-0219d799ae9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39529826-6a76-440f-813c-3948b3c1f941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2c5db4-b4a5-475d-9ca1-fe5562a16eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39529826-6a76-440f-813c-3948b3c1f941",
                    "LayerId": "1fecd87d-ec3b-4b4c-a834-935f2d8b0fe2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "1fecd87d-ec3b-4b4c-a834-935f2d8b0fe2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e677ad1b-9dcd-45bd-a8cb-0c255bd03057",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}