{
    "id": "f4898915-8207-4120-8723-05bd8a898847",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_Park",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 396,
    "bbox_left": 267,
    "bbox_right": 628,
    "bbox_top": 138,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aad687c3-db9f-4502-9021-1d3ea19dd4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4898915-8207-4120-8723-05bd8a898847",
            "compositeImage": {
                "id": "03232fbc-e66a-4262-9008-f68de8d8077a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad687c3-db9f-4502-9021-1d3ea19dd4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a49e52-955d-40e7-b6e1-3f2cb518289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad687c3-db9f-4502-9021-1d3ea19dd4ea",
                    "LayerId": "b9c024f6-32f1-4b52-a83f-08f3c453a2c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "b9c024f6-32f1-4b52-a83f-08f3c453a2c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4898915-8207-4120-8723-05bd8a898847",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}