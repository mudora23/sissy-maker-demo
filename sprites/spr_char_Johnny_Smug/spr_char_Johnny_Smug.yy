{
    "id": "5adb2ca7-aad0-416c-8361-526509dd901c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Johnny_Smug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3f83398c-9276-441d-b8cd-99158fb2f347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5adb2ca7-aad0-416c-8361-526509dd901c",
            "compositeImage": {
                "id": "8e714d6e-cafa-4f87-9563-08ba61a4be8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f83398c-9276-441d-b8cd-99158fb2f347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dc92761-5955-4c99-b95f-bb8358493e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f83398c-9276-441d-b8cd-99158fb2f347",
                    "LayerId": "f4c354bf-5e65-4222-ab8c-bc817a9210b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 682,
    "layers": [
        {
            "id": "f4c354bf-5e65-4222-ab8c-bc817a9210b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5adb2ca7-aad0-416c-8361-526509dd901c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 496,
    "xorig": 0,
    "yorig": 0
}