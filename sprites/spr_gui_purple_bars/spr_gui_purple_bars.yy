{
    "id": "10dfe0de-3ab7-41ec-b90a-3cf3b6fdf146",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_purple_bars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 178,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "553aa0c8-5bb2-4bab-8073-5799327e97c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10dfe0de-3ab7-41ec-b90a-3cf3b6fdf146",
            "compositeImage": {
                "id": "a060fe57-6833-4169-abce-a43c842cdc80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553aa0c8-5bb2-4bab-8073-5799327e97c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dda8c564-0635-4d7f-9bf5-31abaf244dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553aa0c8-5bb2-4bab-8073-5799327e97c0",
                    "LayerId": "ff31d25e-0cc3-4910-9d68-9a910449eb41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "ff31d25e-0cc3-4910-9d68-9a910449eb41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10dfe0de-3ab7-41ec-b90a-3cf3b6fdf146",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 179,
    "xorig": 0,
    "yorig": 0
}