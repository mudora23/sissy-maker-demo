{
    "id": "1f590b0c-538d-4d83-95d8-aa298765886b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_use",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 18,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a6a0e16d-a3b6-47e5-b8ac-8669fe8d5de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f590b0c-538d-4d83-95d8-aa298765886b",
            "compositeImage": {
                "id": "289d7a2f-45c3-40db-aa5a-d57345ad7c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a0e16d-a3b6-47e5-b8ac-8669fe8d5de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c3cc8f-3c39-4aa5-9742-8fe5cc4dde36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a0e16d-a3b6-47e5-b8ac-8669fe8d5de9",
                    "LayerId": "67926a27-7cff-4dec-bc71-a186f8b04d76"
                }
            ]
        },
        {
            "id": "ae9e2436-84e7-40df-8b04-f2772d31a70c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f590b0c-538d-4d83-95d8-aa298765886b",
            "compositeImage": {
                "id": "d8473966-7a80-403e-bfa9-5bc17bdf4f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9e2436-84e7-40df-8b04-f2772d31a70c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aca5ad0d-84a1-4440-9775-282f2e53d3b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9e2436-84e7-40df-8b04-f2772d31a70c",
                    "LayerId": "67926a27-7cff-4dec-bc71-a186f8b04d76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "67926a27-7cff-4dec-bc71-a186f8b04d76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f590b0c-538d-4d83-95d8-aa298765886b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}