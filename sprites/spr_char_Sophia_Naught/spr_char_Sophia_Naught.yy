{
    "id": "2360ac6a-d684-41a0-bf71-fe9f220c5bfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Sophia_Naught",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 859,
    "bbox_left": 35,
    "bbox_right": 282,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d736a6af-d9b0-443f-ad4f-aef987ba32e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2360ac6a-d684-41a0-bf71-fe9f220c5bfa",
            "compositeImage": {
                "id": "0f49cb3f-bceb-4aa0-a888-b948e9b0a9d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d736a6af-d9b0-443f-ad4f-aef987ba32e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4535a928-1cfb-4ef9-8276-34d3e8629cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d736a6af-d9b0-443f-ad4f-aef987ba32e8",
                    "LayerId": "484864c9-a0fc-4ad8-b976-459685da11e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 860,
    "layers": [
        {
            "id": "484864c9-a0fc-4ad8-b976-459685da11e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2360ac6a-d684-41a0-bf71-fe9f220c5bfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 283,
    "xorig": 0,
    "yorig": 0
}