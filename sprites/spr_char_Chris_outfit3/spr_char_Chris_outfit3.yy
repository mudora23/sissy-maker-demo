{
    "id": "403998ea-31f1-43cc-8ed3-fd9e781d2b70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_outfit3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 457,
    "bbox_left": 0,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "da1f6ffd-724c-48ae-b7fd-d208ef4fd960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "403998ea-31f1-43cc-8ed3-fd9e781d2b70",
            "compositeImage": {
                "id": "fb796ec2-6529-4d6b-bbfd-b2efe1f6398c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da1f6ffd-724c-48ae-b7fd-d208ef4fd960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070e51f7-755f-42ce-9d23-307c754bbfd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da1f6ffd-724c-48ae-b7fd-d208ef4fd960",
                    "LayerId": "e3813b87-0395-4f5f-8e1b-b4d3132553d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 458,
    "layers": [
        {
            "id": "e3813b87-0395-4f5f-8e1b-b4d3132553d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "403998ea-31f1-43cc-8ed3-fd9e781d2b70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 59,
    "yorig": 2
}