{
    "id": "d48ca38a-461a-4c7a-8c05-5802cb8b0b8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clock_rest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 387,
    "bbox_left": 8,
    "bbox_right": 417,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bccc312b-2fea-4ac6-a362-cc37df74f1af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d48ca38a-461a-4c7a-8c05-5802cb8b0b8e",
            "compositeImage": {
                "id": "86f9913a-b7b0-48fe-92de-211f7c6a6c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bccc312b-2fea-4ac6-a362-cc37df74f1af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2af6f5-6393-4870-a3b8-b1486874d4a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bccc312b-2fea-4ac6-a362-cc37df74f1af",
                    "LayerId": "6cdb3709-25dc-4c5f-bd81-db497b457694"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 392,
    "layers": [
        {
            "id": "6cdb3709-25dc-4c5f-bd81-db497b457694",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d48ca38a-461a-4c7a-8c05-5802cb8b0b8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 422,
    "xorig": 0,
    "yorig": 0
}