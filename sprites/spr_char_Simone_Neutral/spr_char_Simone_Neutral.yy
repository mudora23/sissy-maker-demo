{
    "id": "9fef419c-1cda-4da4-88dd-cacc1e8629ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Simone_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 772,
    "bbox_left": 11,
    "bbox_right": 268,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "22aaad72-5e8e-42b1-98b6-f164e50f8fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fef419c-1cda-4da4-88dd-cacc1e8629ed",
            "compositeImage": {
                "id": "69088d35-689d-495b-980c-a44fc1701a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22aaad72-5e8e-42b1-98b6-f164e50f8fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eae10b9-55e6-48ef-9c6e-b698a87d2e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22aaad72-5e8e-42b1-98b6-f164e50f8fa5",
                    "LayerId": "e7e3e680-ea8e-4135-a5cd-7ad76347d3f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "e7e3e680-ea8e-4135-a5cd-7ad76347d3f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fef419c-1cda-4da4-88dd-cacc1e8629ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}