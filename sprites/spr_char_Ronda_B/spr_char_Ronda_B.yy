{
    "id": "db973d0a-1f37-4ad6-8491-b81b2b2a4f7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ronda_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 814,
    "bbox_left": 56,
    "bbox_right": 242,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "77419c78-b456-4a9e-8ea4-27703a720e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db973d0a-1f37-4ad6-8491-b81b2b2a4f7b",
            "compositeImage": {
                "id": "c5237b0b-d4a9-4230-9f5a-a19f09464563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77419c78-b456-4a9e-8ea4-27703a720e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d456ea1f-6e2e-474b-984a-e73ee1c30f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77419c78-b456-4a9e-8ea4-27703a720e8b",
                    "LayerId": "1962b97d-029b-4fe7-841f-69f5c4c02833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "1962b97d-029b-4fe7-841f-69f5c4c02833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db973d0a-1f37-4ad6-8491-b81b2b2a4f7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}