{
    "id": "4ee99fd9-979a-48ed-902b-5b07482d86e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_babysitting_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bd11e927-a6c4-49c1-a593-d9f975b7d940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ee99fd9-979a-48ed-902b-5b07482d86e0",
            "compositeImage": {
                "id": "42125ca4-9311-4ad1-99f0-76c95ae8617a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd11e927-a6c4-49c1-a593-d9f975b7d940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c47a6385-496d-479e-82ad-a8a2bf38a629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd11e927-a6c4-49c1-a593-d9f975b7d940",
                    "LayerId": "6b201bed-e98b-479b-8184-6a904bd44b84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "6b201bed-e98b-479b-8184-6a904bd44b84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ee99fd9-979a-48ed-902b-5b07482d86e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 0,
    "yorig": 0
}