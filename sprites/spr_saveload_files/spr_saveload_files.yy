{
    "id": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_saveload_files",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 0,
    "bbox_right": 660,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9da2aee2-bc4e-47d1-82c1-764f1a95b0b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "f38d133b-fc18-4d03-829e-065320e61a23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9da2aee2-bc4e-47d1-82c1-764f1a95b0b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af3c995-60ea-4080-a48f-00bb24a9d93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9da2aee2-bc4e-47d1-82c1-764f1a95b0b4",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "d58d3e6d-d264-47ba-ae01-98925f3d27bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "ceac08e1-939c-48bb-bfc3-d413c0e34f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58d3e6d-d264-47ba-ae01-98925f3d27bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb8e1b1-460c-49fe-831b-cb98ccf43471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58d3e6d-d264-47ba-ae01-98925f3d27bb",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "a6a7862f-cd7e-44b7-857c-7b365851ea2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "b455b34c-f329-4d68-a40f-793e7dbb0af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a7862f-cd7e-44b7-857c-7b365851ea2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaeae2d9-26b7-4a9d-b8af-ffea736ae42f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a7862f-cd7e-44b7-857c-7b365851ea2e",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "c76f35b9-4aa8-412b-b5ff-7b64df95155f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "0afa3baa-8bca-45a4-bd4b-881f09176e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c76f35b9-4aa8-412b-b5ff-7b64df95155f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9563eb-816d-4720-8f0e-4e2c26e4513f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c76f35b9-4aa8-412b-b5ff-7b64df95155f",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "65cc11fd-be4e-4548-9e5f-2b4266c9dbd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "7d408f61-c616-4451-87f7-0dcbc1c6547d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65cc11fd-be4e-4548-9e5f-2b4266c9dbd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7902ac16-e70c-4f52-8a15-dce6f5ce5733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65cc11fd-be4e-4548-9e5f-2b4266c9dbd9",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "5dbe5029-ec85-46ac-85ac-8d0daed6b961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "c5298a43-ec62-4f36-83c1-d6e3581fa3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dbe5029-ec85-46ac-85ac-8d0daed6b961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e379313-1a98-4a26-9594-27b19584571e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dbe5029-ec85-46ac-85ac-8d0daed6b961",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        },
        {
            "id": "d8581b41-38c8-49eb-adb5-b6dacba5e959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "compositeImage": {
                "id": "6fee835e-e4aa-48ed-ba35-33f01933c5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8581b41-38c8-49eb-adb5-b6dacba5e959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d7fd3b-6a22-4467-9bac-057cafe883f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8581b41-38c8-49eb-adb5-b6dacba5e959",
                    "LayerId": "943c92c4-3976-4652-b14e-0e6ec54eba75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "943c92c4-3976-4652-b14e-0e6ec54eba75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55725b7c-14a5-49e2-ac0d-7e6aa6072149",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 661,
    "xorig": 0,
    "yorig": 0
}