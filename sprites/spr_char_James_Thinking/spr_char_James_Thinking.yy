{
    "id": "3ef7973c-5655-4284-a2da-cf4eba6663df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Thinking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 326,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "24f64c24-e494-4a9c-9ffa-863d2735966e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ef7973c-5655-4284-a2da-cf4eba6663df",
            "compositeImage": {
                "id": "d0248572-cd32-4b38-acb0-3538948a7c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f64c24-e494-4a9c-9ffa-863d2735966e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5188c590-7129-4a10-a75e-dbfc84e58493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f64c24-e494-4a9c-9ffa-863d2735966e",
                    "LayerId": "75061a92-04ae-4b6a-a1f2-16c4b9452410"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "75061a92-04ae-4b6a-a1f2-16c4b9452410",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ef7973c-5655-4284-a2da-cf4eba6663df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}