{
    "id": "2502c8aa-1e00-49d2-a28d-9c95e01356c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Raye_Happy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a718d774-8a0f-482a-80d4-15237504cc6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2502c8aa-1e00-49d2-a28d-9c95e01356c5",
            "compositeImage": {
                "id": "59280b62-226a-4f76-b228-41b9119115de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a718d774-8a0f-482a-80d4-15237504cc6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ccb6916-8868-4f16-9779-7051fa05ae19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a718d774-8a0f-482a-80d4-15237504cc6b",
                    "LayerId": "48d75023-9aeb-4910-a81f-c77597ee0c71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 702,
    "layers": [
        {
            "id": "48d75023-9aeb-4910-a81f-c77597ee0c71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2502c8aa-1e00-49d2-a28d-9c95e01356c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 388,
    "xorig": 0,
    "yorig": 0
}