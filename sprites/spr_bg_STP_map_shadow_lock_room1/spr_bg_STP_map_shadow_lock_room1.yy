{
    "id": "3e2640e3-d707-4bfe-bdb4-06aa09739b3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_lock_room1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 188,
    "bbox_left": 115,
    "bbox_right": 223,
    "bbox_top": 68,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "782d71b7-7fa7-41ee-ad79-baa1df78e1d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e2640e3-d707-4bfe-bdb4-06aa09739b3a",
            "compositeImage": {
                "id": "a4cf0afa-893e-4499-a2c7-7788975c0b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782d71b7-7fa7-41ee-ad79-baa1df78e1d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5690e550-1e65-4bf2-92ef-d2a210ba22ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782d71b7-7fa7-41ee-ad79-baa1df78e1d3",
                    "LayerId": "2bc53b60-aa1f-4666-a4aa-767264285c39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "2bc53b60-aa1f-4666-a4aa-767264285c39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e2640e3-d707-4bfe-bdb4-06aa09739b3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}