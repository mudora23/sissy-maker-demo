{
    "id": "38250a03-fcd8-4d8b-83a8-c650535f5975",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_Apt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 534,
    "bbox_left": 141,
    "bbox_right": 377,
    "bbox_top": 369,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d09cdda1-9e04-43b0-bac9-7adbd5e51d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38250a03-fcd8-4d8b-83a8-c650535f5975",
            "compositeImage": {
                "id": "d4485e57-6c83-4fa8-8dcd-9387b8bb7837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09cdda1-9e04-43b0-bac9-7adbd5e51d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95fee43b-db5d-4f56-a8d1-943ede1b2341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09cdda1-9e04-43b0-bac9-7adbd5e51d2a",
                    "LayerId": "0f62ab0e-c034-47eb-9d40-945d0317499a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "0f62ab0e-c034-47eb-9d40-945d0317499a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38250a03-fcd8-4d8b-83a8-c650535f5975",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}