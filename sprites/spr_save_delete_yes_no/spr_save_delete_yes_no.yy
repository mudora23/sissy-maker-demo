{
    "id": "87720170-2208-44be-9caa-8704cfe0c5b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_save_delete_yes_no",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "618a8603-7b13-4e19-8197-d786e3a25002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87720170-2208-44be-9caa-8704cfe0c5b3",
            "compositeImage": {
                "id": "42e515c1-e9d8-4a6e-b2b6-fd888f40d56d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618a8603-7b13-4e19-8197-d786e3a25002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c00c31c-1ef3-4332-b25a-6ac9a85e3b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618a8603-7b13-4e19-8197-d786e3a25002",
                    "LayerId": "9a442c08-0568-4a64-b1b3-f1bd77b175c2"
                }
            ]
        },
        {
            "id": "ca94287c-6751-4c35-b2df-04784ae1b93b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87720170-2208-44be-9caa-8704cfe0c5b3",
            "compositeImage": {
                "id": "dd0ee587-5fdb-4863-870c-ef55f007f2c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca94287c-6751-4c35-b2df-04784ae1b93b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20746e28-d294-4dd9-af62-48b6b3c4659b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca94287c-6751-4c35-b2df-04784ae1b93b",
                    "LayerId": "9a442c08-0568-4a64-b1b3-f1bd77b175c2"
                }
            ]
        },
        {
            "id": "fe12fd74-a188-4e22-8917-7d859d55e58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87720170-2208-44be-9caa-8704cfe0c5b3",
            "compositeImage": {
                "id": "bc8d46de-3974-4e47-a077-1c013dd7f6d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe12fd74-a188-4e22-8917-7d859d55e58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7771604-055b-49fd-ad85-07816da059a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe12fd74-a188-4e22-8917-7d859d55e58a",
                    "LayerId": "9a442c08-0568-4a64-b1b3-f1bd77b175c2"
                }
            ]
        },
        {
            "id": "00bfc98a-4532-425d-bec2-82abb9fb0712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87720170-2208-44be-9caa-8704cfe0c5b3",
            "compositeImage": {
                "id": "aacf341b-9d52-4b0b-8199-07052c455049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00bfc98a-4532-425d-bec2-82abb9fb0712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76edb724-e3cd-40cb-87e7-28c4f444615f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00bfc98a-4532-425d-bec2-82abb9fb0712",
                    "LayerId": "9a442c08-0568-4a64-b1b3-f1bd77b175c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "9a442c08-0568-4a64-b1b3-f1bd77b175c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87720170-2208-44be-9caa-8704cfe0c5b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}