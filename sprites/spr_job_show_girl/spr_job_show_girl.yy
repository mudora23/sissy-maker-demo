{
    "id": "9cd64ebe-3dc1-49e7-bb00-76b51932b39e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_show_girl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6d6947e6-9990-4864-9f12-d4afa0bbad06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cd64ebe-3dc1-49e7-bb00-76b51932b39e",
            "compositeImage": {
                "id": "280c8059-a4cc-482c-9ea1-96196e0c4acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6947e6-9990-4864-9f12-d4afa0bbad06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520f3b86-7341-4b71-b421-a64a53c2b74d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6947e6-9990-4864-9f12-d4afa0bbad06",
                    "LayerId": "e332d5de-635d-48fa-a381-e98c2fcfdb7d"
                },
                {
                    "id": "75447584-6f86-4276-a02c-521a6136a297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6947e6-9990-4864-9f12-d4afa0bbad06",
                    "LayerId": "26483848-fa0e-49eb-aee7-d9dfd91aeb17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "26483848-fa0e-49eb-aee7-d9dfd91aeb17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cd64ebe-3dc1-49e7-bb00-76b51932b39e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e332d5de-635d-48fa-a381-e98c2fcfdb7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cd64ebe-3dc1-49e7-bb00-76b51932b39e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}