{
    "id": "46301c33-b053-46d4-a32d-4487a1900d9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_job",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4933febd-0d20-4932-a352-7681a9a981ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46301c33-b053-46d4-a32d-4487a1900d9f",
            "compositeImage": {
                "id": "1412c564-bac4-498b-a78a-e28413ae300d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4933febd-0d20-4932-a352-7681a9a981ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46de040-e462-4a78-b2f9-f0a5a8424406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4933febd-0d20-4932-a352-7681a9a981ed",
                    "LayerId": "43995e75-afbb-4517-b6d4-6650ad3ed1e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "43995e75-afbb-4517-b6d4-6650ad3ed1e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46301c33-b053-46d4-a32d-4487a1900d9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}