{
    "id": "d575f867-41b7-476a-bf27-3ac32b638baa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPlibrary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c425d7ef-3439-4f1f-a55a-d4374dd2f88e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d575f867-41b7-476a-bf27-3ac32b638baa",
            "compositeImage": {
                "id": "33051a9f-1375-4f19-9b1a-b1c8a5a70bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c425d7ef-3439-4f1f-a55a-d4374dd2f88e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710ea57f-a571-48f0-9c59-9102dbfb682c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c425d7ef-3439-4f1f-a55a-d4374dd2f88e",
                    "LayerId": "9bbc5531-24ea-4a47-b918-eecc2f2e2974"
                }
            ]
        },
        {
            "id": "b5441370-46cb-42b3-8c33-527a9b6acf63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d575f867-41b7-476a-bf27-3ac32b638baa",
            "compositeImage": {
                "id": "ee066275-2fa5-4ab9-a5bc-49b1cdb7cea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5441370-46cb-42b3-8c33-527a9b6acf63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5a75ec-7cc7-4511-8b2c-2952bf5380a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5441370-46cb-42b3-8c33-527a9b6acf63",
                    "LayerId": "9bbc5531-24ea-4a47-b918-eecc2f2e2974"
                }
            ]
        },
        {
            "id": "e1096bd1-50eb-450e-ae6e-f1fd1740c192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d575f867-41b7-476a-bf27-3ac32b638baa",
            "compositeImage": {
                "id": "eed0d1f1-4978-4d77-8192-6caf2e464954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1096bd1-50eb-450e-ae6e-f1fd1740c192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5f8b84-770f-457f-9564-5646b4693735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1096bd1-50eb-450e-ae6e-f1fd1740c192",
                    "LayerId": "9bbc5531-24ea-4a47-b918-eecc2f2e2974"
                }
            ]
        },
        {
            "id": "6930dbdb-f7d6-4e59-bc6b-40647aeeaee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d575f867-41b7-476a-bf27-3ac32b638baa",
            "compositeImage": {
                "id": "32ece922-17e7-44aa-800f-058bd3635a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6930dbdb-f7d6-4e59-bc6b-40647aeeaee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "648e907f-a634-4f82-91d6-54f6e74b3359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6930dbdb-f7d6-4e59-bc6b-40647aeeaee6",
                    "LayerId": "9bbc5531-24ea-4a47-b918-eecc2f2e2974"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9bbc5531-24ea-4a47-b918-eecc2f2e2974",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d575f867-41b7-476a-bf27-3ac32b638baa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}