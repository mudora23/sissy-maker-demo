{
    "id": "73fce534-08f7-4288-adc5-8cefe0c09372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_saveload_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "da4dd16b-4cbf-45b1-bf60-fba5822b11d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73fce534-08f7-4288-adc5-8cefe0c09372",
            "compositeImage": {
                "id": "7f7fd601-2787-4ac5-ae55-bd6ff219269f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4dd16b-4cbf-45b1-bf60-fba5822b11d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdbe5780-2063-4b01-9151-4b2c0690e1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4dd16b-4cbf-45b1-bf60-fba5822b11d4",
                    "LayerId": "0fcf40f8-fae4-4f7c-97c3-e7b573e8b1d5"
                }
            ]
        },
        {
            "id": "4e2ed457-1087-4e71-b64f-dc8bf1f9682e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73fce534-08f7-4288-adc5-8cefe0c09372",
            "compositeImage": {
                "id": "7f671037-f7e0-4c49-b849-ec0049947661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2ed457-1087-4e71-b64f-dc8bf1f9682e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a467d07c-e2d8-45ea-a669-fdd822fed54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2ed457-1087-4e71-b64f-dc8bf1f9682e",
                    "LayerId": "0fcf40f8-fae4-4f7c-97c3-e7b573e8b1d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "0fcf40f8-fae4-4f7c-97c3-e7b573e8b1d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73fce534-08f7-4288-adc5-8cefe0c09372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}