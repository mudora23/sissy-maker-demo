{
    "id": "e8ba4cb0-b402-495d-bc99-6835aa762b35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_fullscreen_on_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 6,
    "bbox_right": 354,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bc819a2a-427f-4a32-8d7b-1fc8992b40b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8ba4cb0-b402-495d-bc99-6835aa762b35",
            "compositeImage": {
                "id": "25ac38b9-c374-484e-ab31-65a782d699dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc819a2a-427f-4a32-8d7b-1fc8992b40b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5177a78b-33c2-4f71-bff3-4e1faf4ea176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc819a2a-427f-4a32-8d7b-1fc8992b40b2",
                    "LayerId": "16ea1c1a-b093-4089-b210-6e0e1662e07c"
                }
            ]
        },
        {
            "id": "25c477e4-84ac-42ab-9d93-b02d9b790649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8ba4cb0-b402-495d-bc99-6835aa762b35",
            "compositeImage": {
                "id": "aadf425a-ab5d-4b07-9291-318956ebe5c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c477e4-84ac-42ab-9d93-b02d9b790649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92acf05c-f4be-42c3-bb09-e38c6a47fe2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c477e4-84ac-42ab-9d93-b02d9b790649",
                    "LayerId": "16ea1c1a-b093-4089-b210-6e0e1662e07c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "16ea1c1a-b093-4089-b210-6e0e1662e07c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8ba4cb0-b402-495d-bc99-6835aa762b35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 360,
    "xorig": 0,
    "yorig": 0
}