{
    "id": "bf7e6e72-f118-45c7-8070-ffb8805c58c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_sp_ChrisCloseUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 56,
    "bbox_right": 785,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7ad3aa88-155c-49c4-88ba-f9a7ab8d121f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf7e6e72-f118-45c7-8070-ffb8805c58c4",
            "compositeImage": {
                "id": "099db354-8b5a-4851-a313-81851b9252b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad3aa88-155c-49c4-88ba-f9a7ab8d121f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae27ae14-f291-44bf-a3fa-d6a7bc58819c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad3aa88-155c-49c4-88ba-f9a7ab8d121f",
                    "LayerId": "088619bf-2ea8-4000-9834-c8446054885f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "088619bf-2ea8-4000-9834-c8446054885f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf7e6e72-f118-45c7-8070-ffb8805c58c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}