{
    "id": "0fb9d2c0-73e4-4ed7-b87c-d23474dd34ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_Park",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 396,
    "bbox_left": 267,
    "bbox_right": 628,
    "bbox_top": 138,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "591e1594-1238-44c4-859f-6150d1b575a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9d2c0-73e4-4ed7-b87c-d23474dd34ed",
            "compositeImage": {
                "id": "f785b879-f937-4e7a-b31e-252834925f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591e1594-1238-44c4-859f-6150d1b575a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc82715-ab60-4b75-8b9e-8617f862ea28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591e1594-1238-44c4-859f-6150d1b575a8",
                    "LayerId": "2dae978e-dc36-462a-a988-03f1da29d00e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "2dae978e-dc36-462a-a988-03f1da29d00e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fb9d2c0-73e4-4ed7-b87c-d23474dd34ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}