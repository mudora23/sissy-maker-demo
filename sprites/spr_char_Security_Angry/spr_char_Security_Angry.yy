{
    "id": "7a6ccc3f-5413-4c15-a5b9-cd7bd0dab93a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Security_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 827,
    "bbox_left": 23,
    "bbox_right": 223,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dddcda96-75f7-4bc2-855d-51945d834b16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6ccc3f-5413-4c15-a5b9-cd7bd0dab93a",
            "compositeImage": {
                "id": "1f65a543-eead-422f-a9bb-9ab9e8c677e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dddcda96-75f7-4bc2-855d-51945d834b16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93691c0-d5c2-44ea-8789-97dc5be8dea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dddcda96-75f7-4bc2-855d-51945d834b16",
                    "LayerId": "ad1f1a9f-9f9a-49f9-88a0-9d9b573de692"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "ad1f1a9f-9f9a-49f9-88a0-9d9b573de692",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a6ccc3f-5413-4c15-a5b9-cd7bd0dab93a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}