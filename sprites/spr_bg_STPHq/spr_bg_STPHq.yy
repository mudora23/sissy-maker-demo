{
    "id": "5f6a3688-b534-4711-8225-f217af816c7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPHq",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 598,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "088ffbb6-a621-4f66-9bba-2d10427f599c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6a3688-b534-4711-8225-f217af816c7c",
            "compositeImage": {
                "id": "05326eaa-e7c7-456b-b735-a058bcd379d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088ffbb6-a621-4f66-9bba-2d10427f599c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "886f360d-8e04-4005-bd80-4f117feec3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088ffbb6-a621-4f66-9bba-2d10427f599c",
                    "LayerId": "505dc075-7a4a-467b-a897-53131bf8e755"
                }
            ]
        },
        {
            "id": "426527a4-69d6-4d1e-8e99-729262972be7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6a3688-b534-4711-8225-f217af816c7c",
            "compositeImage": {
                "id": "dd16cc7b-d45f-4af6-ab1f-92794f2b4c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426527a4-69d6-4d1e-8e99-729262972be7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a1fa06-9a02-4910-9687-556c9f743630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426527a4-69d6-4d1e-8e99-729262972be7",
                    "LayerId": "505dc075-7a4a-467b-a897-53131bf8e755"
                }
            ]
        },
        {
            "id": "eccbe6d8-2fd3-4ba1-ba1b-0b2aa1be16dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6a3688-b534-4711-8225-f217af816c7c",
            "compositeImage": {
                "id": "6aac0302-958b-4f7c-9cd9-950f0431203a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eccbe6d8-2fd3-4ba1-ba1b-0b2aa1be16dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a27e60-b38a-4cd9-9c10-1ff19d05cca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eccbe6d8-2fd3-4ba1-ba1b-0b2aa1be16dd",
                    "LayerId": "505dc075-7a4a-467b-a897-53131bf8e755"
                }
            ]
        },
        {
            "id": "2e359b5e-34ba-4aab-a3c6-c2f0ee2718be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6a3688-b534-4711-8225-f217af816c7c",
            "compositeImage": {
                "id": "64b0b279-116e-4de0-8a7a-e3afdfeaa2b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e359b5e-34ba-4aab-a3c6-c2f0ee2718be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "794b4276-2c0c-48af-be3f-5baa9086334d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e359b5e-34ba-4aab-a3c6-c2f0ee2718be",
                    "LayerId": "505dc075-7a4a-467b-a897-53131bf8e755"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 599,
    "layers": [
        {
            "id": "505dc075-7a4a-467b-a897-53131bf8e755",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f6a3688-b534-4711-8225-f217af816c7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}