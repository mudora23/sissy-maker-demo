{
    "id": "32be2b8f-6727-422d-b283-d651b98c726b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "498db713-1d46-4563-8cee-6615c6397443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32be2b8f-6727-422d-b283-d651b98c726b",
            "compositeImage": {
                "id": "52bcb4a3-8985-4a28-8cd1-bf21beec1276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "498db713-1d46-4563-8cee-6615c6397443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49cc750f-9160-4541-a69e-6639a578ad74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "498db713-1d46-4563-8cee-6615c6397443",
                    "LayerId": "5cd656e9-93b7-4875-9091-a6d078a3fce5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5cd656e9-93b7-4875-9091-a6d078a3fce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32be2b8f-6727-422d-b283-d651b98c726b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}