{
    "id": "554cdcfb-9d14-49dd-ad06-ff2960187136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_rent_bills",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 296,
    "bbox_left": 117,
    "bbox_right": 151,
    "bbox_top": 220,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f9d935b6-63f8-4afc-bcd3-6c9159d81c71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554cdcfb-9d14-49dd-ad06-ff2960187136",
            "compositeImage": {
                "id": "9d2c991f-9338-43d5-8599-01aa113ab068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d935b6-63f8-4afc-bcd3-6c9159d81c71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88cb516c-3719-41cc-bac1-6c9a0e415030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d935b6-63f8-4afc-bcd3-6c9159d81c71",
                    "LayerId": "6b7dafcb-62aa-44d7-92c7-d3ce35792421"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6b7dafcb-62aa-44d7-92c7-d3ce35792421",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "554cdcfb-9d14-49dd-ad06-ff2960187136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}