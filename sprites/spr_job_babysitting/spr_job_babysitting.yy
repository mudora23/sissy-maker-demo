{
    "id": "f89d85c3-9baf-40e8-95db-efd6f433d3b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_babysitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b04a61a5-5a35-4a52-87c8-cd29f700f6cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f89d85c3-9baf-40e8-95db-efd6f433d3b8",
            "compositeImage": {
                "id": "c92a2f69-18c4-411f-becd-9ff54161b5c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04a61a5-5a35-4a52-87c8-cd29f700f6cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed1e605-ed13-483d-8887-45c9594aef5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04a61a5-5a35-4a52-87c8-cd29f700f6cd",
                    "LayerId": "a37411c1-cd46-445a-bf4e-c1dee5212064"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a37411c1-cd46-445a-bf4e-c1dee5212064",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f89d85c3-9baf-40e8-95db-efd6f433d3b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}