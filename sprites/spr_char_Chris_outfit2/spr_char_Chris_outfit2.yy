{
    "id": "1314e29e-314a-481d-b94b-f57a00ad940a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_outfit2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 457,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6f8b7faf-e05c-476b-8de3-71d31154f0fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1314e29e-314a-481d-b94b-f57a00ad940a",
            "compositeImage": {
                "id": "ede4c08f-1035-4ab1-bf3f-990d2db196b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8b7faf-e05c-476b-8de3-71d31154f0fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59666bdc-2a40-4fb5-a8ca-f1df9f1123ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8b7faf-e05c-476b-8de3-71d31154f0fb",
                    "LayerId": "83989aa8-742d-4a94-b28d-2758febe6fe6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 458,
    "layers": [
        {
            "id": "83989aa8-742d-4a94-b28d-2758febe6fe6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1314e29e-314a-481d-b94b-f57a00ad940a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 172,
    "xorig": 59,
    "yorig": 9
}