{
    "id": "db42eb91-1fd3-4b92-bee6-f5ae6258f132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_trans_control",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "97cc7dec-c45c-49f1-9b02-0b3ffc9f5c2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db42eb91-1fd3-4b92-bee6-f5ae6258f132",
            "compositeImage": {
                "id": "357c4a58-2271-4cff-981c-229a91c324a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97cc7dec-c45c-49f1-9b02-0b3ffc9f5c2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623541e4-a608-4b08-8885-d731e6548b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97cc7dec-c45c-49f1-9b02-0b3ffc9f5c2d",
                    "LayerId": "b80ebc91-7866-4e13-8477-539145d11a85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b80ebc91-7866-4e13-8477-539145d11a85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db42eb91-1fd3-4b92-bee6-f5ae6258f132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}