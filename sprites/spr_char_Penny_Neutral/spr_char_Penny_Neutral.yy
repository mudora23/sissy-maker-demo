{
    "id": "6ad563ff-1965-4fe3-b6a8-ca485d0895ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Penny_Neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 32,
    "bbox_right": 286,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4d837804-eee7-4c1e-ba54-78b1188e3c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad563ff-1965-4fe3-b6a8-ca485d0895ed",
            "compositeImage": {
                "id": "20d074e1-ffaf-44c7-b920-e3407f0725ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d837804-eee7-4c1e-ba54-78b1188e3c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eaee6c7-e0b4-4221-b115-514ffc182c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d837804-eee7-4c1e-ba54-78b1188e3c86",
                    "LayerId": "ee98fbe3-b009-4935-80de-e6e223900590"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "ee98fbe3-b009-4935-80de-e6e223900590",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ad563ff-1965-4fe3-b6a8-ca485d0895ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}