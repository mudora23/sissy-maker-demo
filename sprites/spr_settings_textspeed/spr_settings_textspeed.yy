{
    "id": "9e77d1b8-fd88-4951-bc61-7d6705eaa98b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_textspeed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 7,
    "bbox_right": 156,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d2655d45-b04b-40d3-b688-c9a98fc9fcff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e77d1b8-fd88-4951-bc61-7d6705eaa98b",
            "compositeImage": {
                "id": "78c48cec-5de7-4383-b32f-6d59284059d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2655d45-b04b-40d3-b688-c9a98fc9fcff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75b868d8-c21e-4a09-a97d-50aeb0466866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2655d45-b04b-40d3-b688-c9a98fc9fcff",
                    "LayerId": "8103a76b-b5cf-4b52-90b8-109fe9b26fa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "8103a76b-b5cf-4b52-90b8-109fe9b26fa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e77d1b8-fd88-4951-bc61-7d6705eaa98b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 163,
    "xorig": 0,
    "yorig": 0
}