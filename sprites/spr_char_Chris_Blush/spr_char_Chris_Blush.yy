{
    "id": "771a4b98-0909-48c0-901b-de6190d779b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Blush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a0323699-23ab-4d40-87d9-4ccf54ef01ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "771a4b98-0909-48c0-901b-de6190d779b7",
            "compositeImage": {
                "id": "6d4aebde-245a-4418-8cb1-f82981415417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0323699-23ab-4d40-87d9-4ccf54ef01ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf0de71-7734-40a8-9635-27c2d680bf92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0323699-23ab-4d40-87d9-4ccf54ef01ae",
                    "LayerId": "207fd381-dc0c-4146-8c4b-0aca39ab047d"
                }
            ]
        },
        {
            "id": "69d03be9-0dfb-497b-81f9-b471e1c66576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "771a4b98-0909-48c0-901b-de6190d779b7",
            "compositeImage": {
                "id": "5e9c1aa8-c103-459c-9769-1ee3b52be54f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d03be9-0dfb-497b-81f9-b471e1c66576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2611992-8ac2-4528-9925-baa6ad77e438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d03be9-0dfb-497b-81f9-b471e1c66576",
                    "LayerId": "207fd381-dc0c-4146-8c4b-0aca39ab047d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "207fd381-dc0c-4146-8c4b-0aca39ab047d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "771a4b98-0909-48c0-901b-de6190d779b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}