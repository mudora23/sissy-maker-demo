{
    "id": "aee18c3f-9f50-460a-bc0f-47e40df04f8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_hospital",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b6582f01-4768-4d9d-bad6-b54534da5173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aee18c3f-9f50-460a-bc0f-47e40df04f8b",
            "compositeImage": {
                "id": "e03588ae-705e-4c59-87a6-9c9e838a80ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6582f01-4768-4d9d-bad6-b54534da5173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d4d51f-7fc5-47e1-92e1-5f13da65ae4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6582f01-4768-4d9d-bad6-b54534da5173",
                    "LayerId": "bfe4f54e-f027-4a72-a9d5-ad8f5644ffe8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "bfe4f54e-f027-4a72-a9d5-ad8f5644ffe8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aee18c3f-9f50-460a-bc0f-47e40df04f8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}