{
    "id": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_control",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1fd26018-a987-4fe6-9783-3a5d782ab165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
            "compositeImage": {
                "id": "dcdddf57-efa6-4630-8c84-95f7225fa851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd26018-a987-4fe6-9783-3a5d782ab165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c86d3d37-466d-47ae-8124-32faaba3aa91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd26018-a987-4fe6-9783-3a5d782ab165",
                    "LayerId": "295b93f5-e899-424f-bdb7-ed87605c3031"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "295b93f5-e899-424f-bdb7-ed87605c3031",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}