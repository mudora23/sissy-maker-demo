{
    "id": "2920add4-d6b1-4b7b-98c6-4facc8b88ef3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_library",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 586,
    "bbox_left": 460,
    "bbox_right": 618,
    "bbox_top": 373,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "046401c1-f75d-49a8-9cb1-6b006c76336c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2920add4-d6b1-4b7b-98c6-4facc8b88ef3",
            "compositeImage": {
                "id": "e049a4b6-3300-4c0e-88d7-701905b133b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046401c1-f75d-49a8-9cb1-6b006c76336c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec463d0e-ff54-4feb-a20d-a430788a071b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046401c1-f75d-49a8-9cb1-6b006c76336c",
                    "LayerId": "6b36dbef-a529-4c9a-8794-1f7763b20047"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6b36dbef-a529-4c9a-8794-1f7763b20047",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2920add4-d6b1-4b7b-98c6-4facc8b88ef3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}