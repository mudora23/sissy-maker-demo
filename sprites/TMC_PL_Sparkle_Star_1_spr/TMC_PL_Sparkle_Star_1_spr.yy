{
    "id": "5ae1d7a9-2e3e-4679-aa2f-af22b141ab05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TMC_PL_Sparkle_Star_1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 24,
    "bbox_right": 69,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d0c63e1a-f2d7-46d0-8928-497863e3be8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ae1d7a9-2e3e-4679-aa2f-af22b141ab05",
            "compositeImage": {
                "id": "c79d432d-c4af-4476-b866-d5dd7aefa411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c63e1a-f2d7-46d0-8928-497863e3be8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3146bd3d-fb68-417d-975d-c5f1446ee4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c63e1a-f2d7-46d0-8928-497863e3be8a",
                    "LayerId": "b845cb73-cf7e-4270-af16-ecc93cae87d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b845cb73-cf7e-4270-af16-ecc93cae87d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ae1d7a9-2e3e-4679-aa2f-af22b141ab05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}