{
    "id": "98ca9532-d911-4649-b93f-7805f9d2e26b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 799,
    "bbox_right": 0,
    "bbox_top": 599,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f50dba47-35a2-4b19-ae87-524a88c650bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98ca9532-d911-4649-b93f-7805f9d2e26b",
            "compositeImage": {
                "id": "a79a71cf-0f52-4cdc-8b86-fe773f202cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f50dba47-35a2-4b19-ae87-524a88c650bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bc2520-6b22-44ff-a339-d232c9f9f24f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f50dba47-35a2-4b19-ae87-524a88c650bf",
                    "LayerId": "83395de9-bdc1-4d58-9737-a37186f38c28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "83395de9-bdc1-4d58-9737-a37186f38c28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98ca9532-d911-4649-b93f-7805f9d2e26b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}