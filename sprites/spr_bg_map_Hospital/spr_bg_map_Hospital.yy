{
    "id": "ac4df170-5799-49e7-bc14-f4c3bcc29852",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_Hospital",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 35,
    "bbox_right": 342,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9dc9236b-7eb0-49e9-9d67-b4f7cacdeca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac4df170-5799-49e7-bc14-f4c3bcc29852",
            "compositeImage": {
                "id": "a9a15e23-d1e6-4db7-bbf3-cd21f9915b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc9236b-7eb0-49e9-9d67-b4f7cacdeca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caeebf09-df77-466e-a758-0e5904352a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc9236b-7eb0-49e9-9d67-b4f7cacdeca4",
                    "LayerId": "8de741b8-87f8-458f-90a7-f7d12b47597f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "8de741b8-87f8-458f-90a7-f7d12b47597f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac4df170-5799-49e7-bc14-f4c3bcc29852",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}