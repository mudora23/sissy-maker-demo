{
    "id": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_kanesResidenceInside",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "45182c6c-eac3-4625-9f69-7e792307a609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
            "compositeImage": {
                "id": "85ff772e-33bf-4cbb-862d-67cb81b483e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45182c6c-eac3-4625-9f69-7e792307a609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3799d543-e436-4e48-bf38-31106c84ec1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45182c6c-eac3-4625-9f69-7e792307a609",
                    "LayerId": "b375ec88-5b58-4ead-a970-c2b760c4c65b"
                }
            ]
        },
        {
            "id": "2f5a8dbc-95bd-4020-a7ee-600e6d91006a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
            "compositeImage": {
                "id": "919f171b-ac72-47a2-aed0-a1549d652dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f5a8dbc-95bd-4020-a7ee-600e6d91006a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f13dd88-33ec-4555-bae2-8b6f92bb4c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f5a8dbc-95bd-4020-a7ee-600e6d91006a",
                    "LayerId": "b375ec88-5b58-4ead-a970-c2b760c4c65b"
                }
            ]
        },
        {
            "id": "73e8042a-6dd7-4f79-af12-081fd0fdb6f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
            "compositeImage": {
                "id": "140e8e1b-8ea7-44f3-812f-393e1605919b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e8042a-6dd7-4f79-af12-081fd0fdb6f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b249826e-7658-4cee-becb-0609d54a9b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e8042a-6dd7-4f79-af12-081fd0fdb6f1",
                    "LayerId": "b375ec88-5b58-4ead-a970-c2b760c4c65b"
                }
            ]
        },
        {
            "id": "1967e48e-5b40-4ebc-a80e-0e71d9318574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
            "compositeImage": {
                "id": "779ba60a-1676-4559-bb12-48733de638e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1967e48e-5b40-4ebc-a80e-0e71d9318574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042a18d2-941f-45aa-b02f-f21eb1682968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1967e48e-5b40-4ebc-a80e-0e71d9318574",
                    "LayerId": "b375ec88-5b58-4ead-a970-c2b760c4c65b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "b375ec88-5b58-4ead-a970-c2b760c4c65b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6ad81d9-0925-4e66-9723-cc70113a49fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}