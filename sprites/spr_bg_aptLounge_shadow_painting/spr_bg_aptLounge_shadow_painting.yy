{
    "id": "293b359f-6bd2-4784-8196-dea589d27751",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_painting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 358,
    "bbox_left": 0,
    "bbox_right": 88,
    "bbox_top": 51,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cfe0b8f6-6d16-4d24-9baf-daf9440e0e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "293b359f-6bd2-4784-8196-dea589d27751",
            "compositeImage": {
                "id": "77e6d79f-8792-4d4b-9e61-056151871c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe0b8f6-6d16-4d24-9baf-daf9440e0e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5ad5572-6bc5-4a6b-9943-efe7b7f33953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe0b8f6-6d16-4d24-9baf-daf9440e0e92",
                    "LayerId": "c2492fd3-4dd4-44c3-abe9-3404bc7862e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "c2492fd3-4dd4-44c3-abe9-3404bc7862e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "293b359f-6bd2-4784-8196-dea589d27751",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}