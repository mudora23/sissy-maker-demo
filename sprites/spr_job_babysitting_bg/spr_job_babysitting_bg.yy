{
    "id": "45dcad4e-1741-4ad9-a689-e459c47a7c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_babysitting_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "99536087-6868-4b9f-a73c-2d245271abe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45dcad4e-1741-4ad9-a689-e459c47a7c66",
            "compositeImage": {
                "id": "b9d5f144-43d5-476b-adef-3c51dfa6ba94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99536087-6868-4b9f-a73c-2d245271abe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0aa4518-07ba-4d0b-9b79-a8b9df96073b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99536087-6868-4b9f-a73c-2d245271abe8",
                    "LayerId": "dfda4bcb-0867-42f1-a5a6-8c616f50a075"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "dfda4bcb-0867-42f1-a5a6-8c616f50a075",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45dcad4e-1741-4ad9-a689-e459c47a7c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}