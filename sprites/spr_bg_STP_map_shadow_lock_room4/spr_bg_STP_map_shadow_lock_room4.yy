{
    "id": "38f59c7f-9639-42d8-b4bf-adf01c3b6f3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_lock_room4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 565,
    "bbox_left": 627,
    "bbox_right": 799,
    "bbox_top": 404,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e39c6196-1b47-44a5-b1c9-723e05f80668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38f59c7f-9639-42d8-b4bf-adf01c3b6f3e",
            "compositeImage": {
                "id": "82e9cbcf-3df5-4f9a-ada1-24d451ddcf0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39c6196-1b47-44a5-b1c9-723e05f80668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f12b4b2-8b60-4c00-9d9a-14b81b831878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39c6196-1b47-44a5-b1c9-723e05f80668",
                    "LayerId": "311fc1da-2482-4544-88d9-2da83a9d3b50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "311fc1da-2482-4544-88d9-2da83a9d3b50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38f59c7f-9639-42d8-b4bf-adf01c3b6f3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}