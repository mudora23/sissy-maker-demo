{
    "id": "010fa322-10dc-48ef-9fc5-d917287343dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_sozore_lotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4069e097-d13e-4028-a710-866a43f6c3cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "010fa322-10dc-48ef-9fc5-d917287343dd",
            "compositeImage": {
                "id": "d18cfe87-9323-49d4-b368-02765eb271f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4069e097-d13e-4028-a710-866a43f6c3cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d981af54-cb1a-43af-90d9-6b7d53d102c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4069e097-d13e-4028-a710-866a43f6c3cd",
                    "LayerId": "62d71987-dc2e-4897-a935-322fdcad965a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "62d71987-dc2e-4897-a935-322fdcad965a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "010fa322-10dc-48ef-9fc5-d917287343dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}