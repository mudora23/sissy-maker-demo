{
    "id": "dfb22be9-7cd0-4bec-ba1d-0e14c5a294cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 10,
    "bbox_right": 294,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8aa5ede5-b4c9-4473-ac3e-833567d47962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb22be9-7cd0-4bec-ba1d-0e14c5a294cd",
            "compositeImage": {
                "id": "b9287d6a-248b-456b-9278-2c41cd7ac350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa5ede5-b4c9-4473-ac3e-833567d47962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0617d6bc-ebe6-4666-b533-2512e2429881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa5ede5-b4c9-4473-ac3e-833567d47962",
                    "LayerId": "1100522d-5bab-4c05-b6a9-119472f1471c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "1100522d-5bab-4c05-b6a9-119472f1471c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfb22be9-7cd0-4bec-ba1d-0e14c5a294cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 304,
    "xorig": 0,
    "yorig": 0
}