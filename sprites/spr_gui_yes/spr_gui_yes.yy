{
    "id": "a064e86b-5fd5-4f4d-b441-dec63218fdd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_yes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 16,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a61e9d6d-6fba-441e-93cd-163b1648aa77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a064e86b-5fd5-4f4d-b441-dec63218fdd8",
            "compositeImage": {
                "id": "69f5b71c-30e5-432e-b5a6-fc33d74c9076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61e9d6d-6fba-441e-93cd-163b1648aa77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65724e3-c19a-42b6-970d-535bf6b0b350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61e9d6d-6fba-441e-93cd-163b1648aa77",
                    "LayerId": "cc79dabd-68fa-4ebe-9a87-68a1914af4a0"
                }
            ]
        },
        {
            "id": "05b3bf5b-3b7b-4e52-8584-5271ed73af22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a064e86b-5fd5-4f4d-b441-dec63218fdd8",
            "compositeImage": {
                "id": "4793aa05-5321-44b9-9d3a-555f5e0d9c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b3bf5b-3b7b-4e52-8584-5271ed73af22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67a4c1f-090f-4f7e-ac7a-8ecb4c64cc63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b3bf5b-3b7b-4e52-8584-5271ed73af22",
                    "LayerId": "cc79dabd-68fa-4ebe-9a87-68a1914af4a0"
                }
            ]
        },
        {
            "id": "52bd3db9-9980-4c7c-984a-60cc96fe7236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a064e86b-5fd5-4f4d-b441-dec63218fdd8",
            "compositeImage": {
                "id": "9c2b7639-46e1-487b-951c-5023048e2cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52bd3db9-9980-4c7c-984a-60cc96fe7236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0fe2de-9299-43a7-b1c6-22af32bb15d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52bd3db9-9980-4c7c-984a-60cc96fe7236",
                    "LayerId": "cc79dabd-68fa-4ebe-9a87-68a1914af4a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "cc79dabd-68fa-4ebe-9a87-68a1914af4a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a064e86b-5fd5-4f4d-b441-dec63218fdd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}