{
    "id": "9d46bf9a-0463-44fd-adee-887203366d5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_door2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 427,
    "bbox_left": 561,
    "bbox_right": 635,
    "bbox_top": 123,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3b07cbe4-e80c-4bad-b04c-97b023959ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d46bf9a-0463-44fd-adee-887203366d5f",
            "compositeImage": {
                "id": "fbc1f5e1-66ff-4ffb-b887-3fd5a981441b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b07cbe4-e80c-4bad-b04c-97b023959ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef9cf69-1341-4e32-8a18-2b9ec24e2677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b07cbe4-e80c-4bad-b04c-97b023959ddb",
                    "LayerId": "0cb35790-f736-4c66-8cba-26d4bbefbf2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "0cb35790-f736-4c66-8cba-26d4bbefbf2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d46bf9a-0463-44fd-adee-887203366d5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}