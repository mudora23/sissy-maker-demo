{
    "id": "074d0236-3014-4169-a43f-f800a9e7823a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_LotusDew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 415,
    "bbox_left": 7,
    "bbox_right": 223,
    "bbox_top": 265,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "079e6bc3-6e4b-4617-8995-98bc8552e4e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074d0236-3014-4169-a43f-f800a9e7823a",
            "compositeImage": {
                "id": "477f945e-e057-4fca-9410-a20299b54507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "079e6bc3-6e4b-4617-8995-98bc8552e4e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "821ee0dd-3bc4-4717-9a30-8fc619566385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "079e6bc3-6e4b-4617-8995-98bc8552e4e3",
                    "LayerId": "ad6869ca-6856-46bb-b19c-7a80d34b1a70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "ad6869ca-6856-46bb-b19c-7a80d34b1a70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "074d0236-3014-4169-a43f-f800a9e7823a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}