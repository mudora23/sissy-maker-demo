{
    "id": "08930131-5deb-48f2-b7e0-72770cbd61e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ronda_Happy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 814,
    "bbox_left": 56,
    "bbox_right": 242,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "010dd5db-d77e-496c-a9da-dd74814c06be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08930131-5deb-48f2-b7e0-72770cbd61e9",
            "compositeImage": {
                "id": "602edd0f-cf3c-4f9d-bdae-ab2550563034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010dd5db-d77e-496c-a9da-dd74814c06be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b53474-8be9-4712-bfdf-3b040aa91f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010dd5db-d77e-496c-a9da-dd74814c06be",
                    "LayerId": "7696bb13-6bd2-45e3-ae74-db6c88d96cd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "7696bb13-6bd2-45e3-ae74-db6c88d96cd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08930131-5deb-48f2-b7e0-72770cbd61e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}