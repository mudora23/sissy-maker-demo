{
    "id": "c7f0a2af-6594-470d-ba99-22e805b0f74a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c2433411-732a-4b0d-b0fa-945550e8b794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7f0a2af-6594-470d-ba99-22e805b0f74a",
            "compositeImage": {
                "id": "38266e30-cf88-4d8b-a0bb-6574ebbdcb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2433411-732a-4b0d-b0fa-945550e8b794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15fd08c-fc0a-4b55-b664-2511a2e5ba01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2433411-732a-4b0d-b0fa-945550e8b794",
                    "LayerId": "f0035d86-f437-468d-9a8e-37e151d89d0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f0035d86-f437-468d-9a8e-37e151d89d0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7f0a2af-6594-470d-ba99-22e805b0f74a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}