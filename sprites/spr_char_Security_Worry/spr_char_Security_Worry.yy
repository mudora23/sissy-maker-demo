{
    "id": "8fd33507-197a-4c23-b5b4-5edf00a614d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Security_Worry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 827,
    "bbox_left": 23,
    "bbox_right": 223,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "332f76a7-da42-4882-b819-7989a85d7dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fd33507-197a-4c23-b5b4-5edf00a614d8",
            "compositeImage": {
                "id": "a16084ab-8a41-4d2a-bd2c-4d9feecca314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "332f76a7-da42-4882-b819-7989a85d7dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d86d54-daf0-488f-af5f-eef036717c69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "332f76a7-da42-4882-b819-7989a85d7dbc",
                    "LayerId": "878a499e-5632-47c2-baee-00516c90c770"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 850,
    "layers": [
        {
            "id": "878a499e-5632-47c2-baee-00516c90c770",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fd33507-197a-4c23-b5b4-5edf00a614d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}