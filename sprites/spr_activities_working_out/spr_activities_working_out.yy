{
    "id": "5bda30ca-a1fe-4236-b151-c4d76bc3f5be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_activities_working_out",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 502,
    "bbox_left": 81,
    "bbox_right": 359,
    "bbox_top": 80,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c5462fa2-6962-4a58-a7e1-f19f32bd348d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bda30ca-a1fe-4236-b151-c4d76bc3f5be",
            "compositeImage": {
                "id": "293281ed-c97f-4044-b639-98b73969fa58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5462fa2-6962-4a58-a7e1-f19f32bd348d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8315f8a6-c636-478c-bd43-88446f9940c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5462fa2-6962-4a58-a7e1-f19f32bd348d",
                    "LayerId": "00e0bbdc-71a5-4480-966c-bffec7ac6f58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "00e0bbdc-71a5-4480-966c-bffec7ac6f58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bda30ca-a1fe-4236-b151-c4d76bc3f5be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}