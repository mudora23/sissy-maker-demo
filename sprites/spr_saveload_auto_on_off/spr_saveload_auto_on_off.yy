{
    "id": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_saveload_auto_on_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 134,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e5eee29a-88ab-4740-afc7-d7c6c0ca7249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
            "compositeImage": {
                "id": "1873ea0d-9fa0-4017-ba4a-a33a11049fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5eee29a-88ab-4740-afc7-d7c6c0ca7249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf99820-c8bb-49cf-bcdb-059ce4126ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5eee29a-88ab-4740-afc7-d7c6c0ca7249",
                    "LayerId": "b4980bd2-3d3f-46fd-b034-caa855b96fb2"
                }
            ]
        },
        {
            "id": "b910d9bd-6cbb-422c-beeb-9537651b6ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
            "compositeImage": {
                "id": "578e6760-3b30-4dd9-8217-c669c81438d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b910d9bd-6cbb-422c-beeb-9537651b6ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8515389a-bdd5-4b14-98eb-b7cf929cf5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b910d9bd-6cbb-422c-beeb-9537651b6ea8",
                    "LayerId": "b4980bd2-3d3f-46fd-b034-caa855b96fb2"
                }
            ]
        },
        {
            "id": "fe814a80-c213-4df8-8e46-3814b7c5e391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
            "compositeImage": {
                "id": "c2005f48-3122-4233-9096-5772da4d40f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe814a80-c213-4df8-8e46-3814b7c5e391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ba1657-7318-455d-97f8-12f7c13491e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe814a80-c213-4df8-8e46-3814b7c5e391",
                    "LayerId": "b4980bd2-3d3f-46fd-b034-caa855b96fb2"
                }
            ]
        },
        {
            "id": "32abd39e-2c53-4122-aeb5-7919b53701d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
            "compositeImage": {
                "id": "780424f5-8f36-4bc2-bf32-73e22b34adee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32abd39e-2c53-4122-aeb5-7919b53701d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37fdab19-e6bc-48d6-afa7-2c5e6628d94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32abd39e-2c53-4122-aeb5-7919b53701d3",
                    "LayerId": "b4980bd2-3d3f-46fd-b034-caa855b96fb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b4980bd2-3d3f-46fd-b034-caa855b96fb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f100c5da-12e2-4f37-b7da-75bfc9286eec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 0,
    "yorig": 0
}