{
    "id": "9a029128-b7cc-4df3-841e-e5cce8f4ccac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Dayna_Tired",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 663,
    "bbox_left": 85,
    "bbox_right": 287,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3de62d57-97c6-4568-80d3-09dd3b1bb9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a029128-b7cc-4df3-841e-e5cce8f4ccac",
            "compositeImage": {
                "id": "b0f52afa-e427-4f13-885c-f35c78763a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de62d57-97c6-4568-80d3-09dd3b1bb9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d97167-1bc7-437c-9a8b-551696bb4bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de62d57-97c6-4568-80d3-09dd3b1bb9b5",
                    "LayerId": "7106e250-01c4-417b-8c85-24b3734b4a21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 664,
    "layers": [
        {
            "id": "7106e250-01c4-417b-8c85-24b3734b4a21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a029128-b7cc-4df3-841e-e5cce8f4ccac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 0,
    "yorig": 0
}