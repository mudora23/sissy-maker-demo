{
    "id": "f592e93d-6755-4c73-aab5-6b72d9cb8cc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_lock_room3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 175,
    "bbox_right": 414,
    "bbox_top": 539,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "49dfb70c-56d7-4995-8a36-5d49ca9e1a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f592e93d-6755-4c73-aab5-6b72d9cb8cc0",
            "compositeImage": {
                "id": "10f6982f-87c3-448e-965c-c6fd7fb884d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49dfb70c-56d7-4995-8a36-5d49ca9e1a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6b19fd-a611-4fa6-8a1c-e477c748763f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49dfb70c-56d7-4995-8a36-5d49ca9e1a39",
                    "LayerId": "4489435b-3ecf-46ff-9493-58b87540b441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4489435b-3ecf-46ff-9493-58b87540b441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f592e93d-6755-4c73-aab5-6b72d9cb8cc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}