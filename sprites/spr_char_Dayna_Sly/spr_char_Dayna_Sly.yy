{
    "id": "6b776aba-e23e-4635-9eec-5e970c77a41b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Dayna_Sly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 663,
    "bbox_left": 83,
    "bbox_right": 250,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee3515cb-871a-482d-8220-67d6c5824a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b776aba-e23e-4635-9eec-5e970c77a41b",
            "compositeImage": {
                "id": "75f23bf2-2383-4790-8bac-d5f8e06fcb6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3515cb-871a-482d-8220-67d6c5824a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27d969f-1cbe-486a-9458-ebd436f4621c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3515cb-871a-482d-8220-67d6c5824a6c",
                    "LayerId": "2c1feb27-4b0b-4f44-b711-19d34c61de60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 664,
    "layers": [
        {
            "id": "2c1feb27-4b0b-4f44-b711-19d34c61de60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b776aba-e23e-4635-9eec-5e970c77a41b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 0,
    "yorig": 0
}