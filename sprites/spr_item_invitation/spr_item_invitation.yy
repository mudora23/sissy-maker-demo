{
    "id": "1ba2869d-dd82-4cf2-b615-078701ccd079",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_invitation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "89d55b51-27f4-4e03-91a8-0f4d9cbdee3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba2869d-dd82-4cf2-b615-078701ccd079",
            "compositeImage": {
                "id": "baa1da02-ef06-48c0-8557-fc4e04bc1afe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d55b51-27f4-4e03-91a8-0f4d9cbdee3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c54ee3-40c5-4203-9296-f23df8fc92b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d55b51-27f4-4e03-91a8-0f4d9cbdee3c",
                    "LayerId": "8ab9f621-7dbc-4d15-a3d6-f36470055bc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8ab9f621-7dbc-4d15-a3d6-f36470055bc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ba2869d-dd82-4cf2-b615-078701ccd079",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}