{
    "id": "0454ba67-d87b-4e3e-a94b-960610a79272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_job_assignment_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4283780e-54bc-4e91-9eea-ccbcab0a8129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0454ba67-d87b-4e3e-a94b-960610a79272",
            "compositeImage": {
                "id": "3cb1de6a-851b-410d-a73e-8532d24767b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4283780e-54bc-4e91-9eea-ccbcab0a8129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb89185-6a04-42a8-b55c-a79190c6f22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4283780e-54bc-4e91-9eea-ccbcab0a8129",
                    "LayerId": "1ec725c8-36e6-4d46-9ea2-fa948eb7329f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ec725c8-36e6-4d46-9ea2-fa948eb7329f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0454ba67-d87b-4e3e-a94b-960610a79272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}