{
    "id": "37dfaba4-70e3-420c-b707-6868db49eb54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deadline_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dd4a6863-e773-4263-af32-bcc506cfddea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37dfaba4-70e3-420c-b707-6868db49eb54",
            "compositeImage": {
                "id": "fa3f3b16-27a4-4000-b4cc-0ba50be0b4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd4a6863-e773-4263-af32-bcc506cfddea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138b7f6c-6492-4b25-9869-f6d4c14049ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd4a6863-e773-4263-af32-bcc506cfddea",
                    "LayerId": "298d9b2f-8227-41c9-8b5d-4d49839c4385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "298d9b2f-8227-41c9-8b5d-4d49839c4385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37dfaba4-70e3-420c-b707-6868db49eb54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}