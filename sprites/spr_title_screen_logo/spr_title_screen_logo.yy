{
    "id": "72813c2d-8179-42a3-b8ae-03d2371763c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title_screen_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 189,
    "bbox_left": 0,
    "bbox_right": 275,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "54cdca35-174e-4b92-9b8a-6242a9fcb02d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72813c2d-8179-42a3-b8ae-03d2371763c6",
            "compositeImage": {
                "id": "2fa658db-0fca-48c4-a1c9-40586ee325c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54cdca35-174e-4b92-9b8a-6242a9fcb02d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf89b75c-0723-4073-a028-5b8aa696a75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54cdca35-174e-4b92-9b8a-6242a9fcb02d",
                    "LayerId": "59be1826-7780-479d-a1a4-95f4bcab8190"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 265,
    "layers": [
        {
            "id": "59be1826-7780-479d-a1a4-95f4bcab8190",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72813c2d-8179-42a3-b8ae-03d2371763c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 305,
    "xorig": 0,
    "yorig": 0
}