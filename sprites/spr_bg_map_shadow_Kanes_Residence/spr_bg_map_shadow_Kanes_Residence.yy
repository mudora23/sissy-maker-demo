{
    "id": "e1e43a67-211e-40cd-902a-6e828bb28ab9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_Kanes_Residence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 506,
    "bbox_left": 492,
    "bbox_right": 711,
    "bbox_top": 330,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8bb60a7f-cc2b-4b3f-b83a-688799d7f7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1e43a67-211e-40cd-902a-6e828bb28ab9",
            "compositeImage": {
                "id": "263d949b-2557-48e4-a884-3fdae1f1edef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb60a7f-cc2b-4b3f-b83a-688799d7f7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba78a93-0bc9-45b4-b479-f1920de2a94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb60a7f-cc2b-4b3f-b83a-688799d7f7b4",
                    "LayerId": "7ec558d1-e0b4-459f-9116-1a053eaa0b5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "7ec558d1-e0b4-459f-9116-1a053eaa0b5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1e43a67-211e-40cd-902a-6e828bb28ab9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}