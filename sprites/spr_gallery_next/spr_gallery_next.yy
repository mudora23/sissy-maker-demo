{
    "id": "b4a5bcca-2df5-40b5-af39-c5d299a04917",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gallery_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee2d4782-5667-4c12-ba80-bd1c9f311a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a5bcca-2df5-40b5-af39-c5d299a04917",
            "compositeImage": {
                "id": "e369781b-81f3-42e8-8ef2-d5ebadd9ea33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2d4782-5667-4c12-ba80-bd1c9f311a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbada8d1-9576-4e03-bf71-d7bf4e258b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2d4782-5667-4c12-ba80-bd1c9f311a89",
                    "LayerId": "5eee4779-6f2e-4e43-9efc-256b4432419f"
                }
            ]
        },
        {
            "id": "5311a85d-f4d1-4a72-9081-4360cb7d436d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a5bcca-2df5-40b5-af39-c5d299a04917",
            "compositeImage": {
                "id": "bb2945c6-65b3-4ef4-9415-b8b8507f096b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5311a85d-f4d1-4a72-9081-4360cb7d436d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fce588-1d38-457a-b64a-488d086ece01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5311a85d-f4d1-4a72-9081-4360cb7d436d",
                    "LayerId": "5eee4779-6f2e-4e43-9efc-256b4432419f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5eee4779-6f2e-4e43-9efc-256b4432419f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4a5bcca-2df5-40b5-af39-c5d299a04917",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 0,
    "yorig": 0
}