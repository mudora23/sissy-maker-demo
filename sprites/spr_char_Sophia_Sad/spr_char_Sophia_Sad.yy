{
    "id": "3fdf9ad5-c0ad-4726-b5bf-c23a2e0e1f5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Sophia_Sad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 859,
    "bbox_left": 35,
    "bbox_right": 282,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ffda9703-c661-4887-8072-88c3a59fa018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fdf9ad5-c0ad-4726-b5bf-c23a2e0e1f5c",
            "compositeImage": {
                "id": "ab5a1200-5961-46ec-9bb7-68dfac355b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffda9703-c661-4887-8072-88c3a59fa018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "185305e7-3876-40fd-bb7f-505daa4abd04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffda9703-c661-4887-8072-88c3a59fa018",
                    "LayerId": "f05e67f6-1d09-4ecc-8b69-1994953a2d66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 860,
    "layers": [
        {
            "id": "f05e67f6-1d09-4ecc-8b69-1994953a2d66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fdf9ad5-c0ad-4726-b5bf-c23a2e0e1f5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 283,
    "xorig": 0,
    "yorig": 0
}