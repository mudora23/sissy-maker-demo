{
    "id": "90000516-f55a-4d81-b321-7c9a1c87ccf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Penny_Burst",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 32,
    "bbox_right": 286,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1bc5eb5b-304f-42c2-b5bb-c92ac692d530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90000516-f55a-4d81-b321-7c9a1c87ccf4",
            "compositeImage": {
                "id": "3cd57f7f-a04f-4f4e-ae4b-04ef9fb7df0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc5eb5b-304f-42c2-b5bb-c92ac692d530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d235163-bb8e-4f52-82d6-9d7e7b316170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc5eb5b-304f-42c2-b5bb-c92ac692d530",
                    "LayerId": "82266888-756c-48ac-9e8c-08a5375bc7ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "82266888-756c-48ac-9e8c-08a5375bc7ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90000516-f55a-4d81-b321-7c9a1c87ccf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}