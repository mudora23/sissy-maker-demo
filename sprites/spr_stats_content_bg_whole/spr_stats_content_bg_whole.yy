{
    "id": "22aa2bed-ef8f-4c85-b931-0a3cd08def02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stats_content_bg_whole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4f934c26-ec67-4ae2-bf23-819b1d6a3248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22aa2bed-ef8f-4c85-b931-0a3cd08def02",
            "compositeImage": {
                "id": "b5387dbc-de0a-43d3-8a58-260d5c8bd12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f934c26-ec67-4ae2-bf23-819b1d6a3248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b17a747-bd30-4249-a7cb-8047d67dfe3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f934c26-ec67-4ae2-bf23-819b1d6a3248",
                    "LayerId": "506b9039-64d0-4606-a6cf-a556fd7e0848"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "506b9039-64d0-4606-a6cf-a556fd7e0848",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22aa2bed-ef8f-4c85-b931-0a3cd08def02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}