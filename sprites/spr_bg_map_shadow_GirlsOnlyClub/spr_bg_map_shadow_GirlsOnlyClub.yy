{
    "id": "010757ef-85a5-4fe9-8b18-3d5bd65ff464",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_shadow_GirlsOnlyClub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 329,
    "bbox_right": 588,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5909ff1c-7610-4f11-8ced-bfb6c6c891db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "010757ef-85a5-4fe9-8b18-3d5bd65ff464",
            "compositeImage": {
                "id": "bd17cb12-6735-4fc8-9da3-3f3004a99fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5909ff1c-7610-4f11-8ced-bfb6c6c891db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02264aa0-06b7-4e63-a0e6-6b4ed9426b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5909ff1c-7610-4f11-8ced-bfb6c6c891db",
                    "LayerId": "736c5365-b20a-4727-ab4a-3fa407144413"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "736c5365-b20a-4727-ab4a-3fa407144413",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "010757ef-85a5-4fe9-8b18-3d5bd65ff464",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}