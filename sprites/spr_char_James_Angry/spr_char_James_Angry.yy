{
    "id": "31cb83e5-4357-4777-8fb0-b5fa58459981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 308,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1c78315e-a337-4cd4-b4ee-8ad56a34696b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31cb83e5-4357-4777-8fb0-b5fa58459981",
            "compositeImage": {
                "id": "3c979dd5-f176-44de-bea0-e919945effa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c78315e-a337-4cd4-b4ee-8ad56a34696b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0633da22-e0ee-4fd3-a9f4-767d872220fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c78315e-a337-4cd4-b4ee-8ad56a34696b",
                    "LayerId": "600b1efb-5ffa-4b7e-b8c7-21aa355354fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "600b1efb-5ffa-4b7e-b8c7-21aa355354fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31cb83e5-4357-4777-8fb0-b5fa58459981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}