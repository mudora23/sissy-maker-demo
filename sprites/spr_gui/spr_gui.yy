{
    "id": "d151ad21-f906-4dcf-9617-21975ef70396",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5cecf8da-ccae-479b-a6cd-3306ac6c6de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d151ad21-f906-4dcf-9617-21975ef70396",
            "compositeImage": {
                "id": "a630a724-6ffc-4cff-bb5f-aee1f7c5c60f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cecf8da-ccae-479b-a6cd-3306ac6c6de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2f1f9e-a123-4536-a359-c0b59d1a86a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cecf8da-ccae-479b-a6cd-3306ac6c6de9",
                    "LayerId": "81873131-8f4e-4572-a5a4-f0e747942e07"
                }
            ]
        },
        {
            "id": "d5e1b304-440f-4179-8def-2c047f9e968a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d151ad21-f906-4dcf-9617-21975ef70396",
            "compositeImage": {
                "id": "c49d5123-a670-4e45-a3f5-65007cebda1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e1b304-440f-4179-8def-2c047f9e968a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "848c2694-6c19-4154-b57d-abff8a75066f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e1b304-440f-4179-8def-2c047f9e968a",
                    "LayerId": "81873131-8f4e-4572-a5a4-f0e747942e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "81873131-8f4e-4572-a5a4-f0e747942e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d151ad21-f906-4dcf-9617-21975ef70396",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}