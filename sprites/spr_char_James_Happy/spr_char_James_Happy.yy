{
    "id": "479767a2-c357-4fb8-864f-a76e36122449",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_James_Happy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 776,
    "bbox_left": 52,
    "bbox_right": 307,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "77a88b46-5177-4869-b3da-464fefa5b047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "479767a2-c357-4fb8-864f-a76e36122449",
            "compositeImage": {
                "id": "71cb122f-62ab-4517-b16c-baad704942aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77a88b46-5177-4869-b3da-464fefa5b047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18992b81-a3c9-4a8a-812c-57d299bfb450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77a88b46-5177-4869-b3da-464fefa5b047",
                    "LayerId": "14de44e8-6e82-4bd0-86db-4353b8a046df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "14de44e8-6e82-4bd0-86db-4353b8a046df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "479767a2-c357-4fb8-864f-a76e36122449",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}