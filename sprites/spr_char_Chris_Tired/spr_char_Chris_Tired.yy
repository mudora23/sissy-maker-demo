{
    "id": "5634b52d-590f-4169-8562-167eac3156c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Chris_Tired",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 720,
    "bbox_left": 38,
    "bbox_right": 225,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fd2f9c9b-8c39-493e-a759-047da8ed774f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5634b52d-590f-4169-8562-167eac3156c9",
            "compositeImage": {
                "id": "9fde9fd2-7dc7-412c-839e-7904836f7c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2f9c9b-8c39-493e-a759-047da8ed774f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462ae092-4a46-4b34-973c-64b13bee6f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2f9c9b-8c39-493e-a759-047da8ed774f",
                    "LayerId": "b609d7dd-0a97-4d3d-9db2-5f501a0d5ca5"
                }
            ]
        },
        {
            "id": "f19889bb-e8ec-425d-9aa0-5414cc11cfca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5634b52d-590f-4169-8562-167eac3156c9",
            "compositeImage": {
                "id": "c0f9597f-6137-4e35-99cf-a308443d4e01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19889bb-e8ec-425d-9aa0-5414cc11cfca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f826cb46-0fc9-42e8-9ed1-581fcd4e98ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19889bb-e8ec-425d-9aa0-5414cc11cfca",
                    "LayerId": "b609d7dd-0a97-4d3d-9db2-5f501a0d5ca5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 750,
    "layers": [
        {
            "id": "b609d7dd-0a97-4d3d-9db2-5f501a0d5ca5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5634b52d-590f-4169-8562-167eac3156c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}