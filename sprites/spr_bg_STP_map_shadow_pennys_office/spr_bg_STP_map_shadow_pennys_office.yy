{
    "id": "8dfc1ad2-e5ed-43a4-b7bc-c03f50456558",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_pennys_office",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 333,
    "bbox_left": 647,
    "bbox_right": 783,
    "bbox_top": 72,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7009d1c0-f091-464e-8b14-f39c33c10125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dfc1ad2-e5ed-43a4-b7bc-c03f50456558",
            "compositeImage": {
                "id": "294e8c47-6ec1-4ce2-978c-e2e2119947ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7009d1c0-f091-464e-8b14-f39c33c10125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c89b9da-e90b-4f60-aad0-869b594be73d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7009d1c0-f091-464e-8b14-f39c33c10125",
                    "LayerId": "5892f88b-9ac5-46a6-8a6a-f2255bf7d96e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5892f88b-9ac5-46a6-8a6a-f2255bf7d96e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dfc1ad2-e5ed-43a4-b7bc-c03f50456558",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}