{
    "id": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPmainHall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 600,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ae637cec-c4a4-433d-a0f4-d41771b69edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
            "compositeImage": {
                "id": "2df5d1fb-7954-431f-a93c-1bb20cbba020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae637cec-c4a4-433d-a0f4-d41771b69edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6411eda5-2999-4864-846b-eb91e468ff05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae637cec-c4a4-433d-a0f4-d41771b69edf",
                    "LayerId": "8b656031-a210-453d-b5f3-22f4d2ed312f"
                }
            ]
        },
        {
            "id": "f8d7175d-a674-47fa-a470-edbd1eb4ad36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
            "compositeImage": {
                "id": "2bc7fa61-527f-4f17-b495-1404b02bdecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d7175d-a674-47fa-a470-edbd1eb4ad36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ce17c2-07a7-4180-bec3-5f2eeb5a089f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d7175d-a674-47fa-a470-edbd1eb4ad36",
                    "LayerId": "8b656031-a210-453d-b5f3-22f4d2ed312f"
                }
            ]
        },
        {
            "id": "88b6d245-0ff0-4975-91f0-cc14b8b2edcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
            "compositeImage": {
                "id": "97ab40ac-132a-4292-9dd5-cb09178a1272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b6d245-0ff0-4975-91f0-cc14b8b2edcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747f6eb5-64c5-4324-aa38-47689fed727e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b6d245-0ff0-4975-91f0-cc14b8b2edcb",
                    "LayerId": "8b656031-a210-453d-b5f3-22f4d2ed312f"
                }
            ]
        },
        {
            "id": "c1df70b7-808d-4c0a-aa70-601afe934897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
            "compositeImage": {
                "id": "ab04adb8-9830-4a05-8443-e637664f5e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1df70b7-808d-4c0a-aa70-601afe934897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3f520d-b316-448d-a29f-a6b445442710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1df70b7-808d-4c0a-aa70-601afe934897",
                    "LayerId": "8b656031-a210-453d-b5f3-22f4d2ed312f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 601,
    "layers": [
        {
            "id": "8b656031-a210-453d-b5f3-22f4d2ed312f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c2a0d0c-a733-4b33-8b06-09d61c6653a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}