{
    "id": "a122e9e9-1f60-4b56-9a28-63b70e8264e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 4,
    "bbox_right": 413,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f77b10cb-2d89-4d22-81ad-9412b0863da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a122e9e9-1f60-4b56-9a28-63b70e8264e4",
            "compositeImage": {
                "id": "f3218fe7-44c3-445a-a5a7-ad4806b74bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77b10cb-2d89-4d22-81ad-9412b0863da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c897723-f53d-4b4d-b2a3-2d7555069fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77b10cb-2d89-4d22-81ad-9412b0863da7",
                    "LayerId": "69c0d280-58a0-44db-a1a4-6aa6518c7236"
                }
            ]
        },
        {
            "id": "0adcdb29-de62-4056-a6b7-aa512f793494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a122e9e9-1f60-4b56-9a28-63b70e8264e4",
            "compositeImage": {
                "id": "98c04775-3be4-42bc-b3de-bfe0fe38c82f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0adcdb29-de62-4056-a6b7-aa512f793494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9d37beb-9e83-44a2-9e7e-f2b43a07967b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0adcdb29-de62-4056-a6b7-aa512f793494",
                    "LayerId": "69c0d280-58a0-44db-a1a4-6aa6518c7236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "69c0d280-58a0-44db-a1a4-6aa6518c7236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a122e9e9-1f60-4b56-9a28-63b70e8264e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 417,
    "xorig": 0,
    "yorig": 0
}