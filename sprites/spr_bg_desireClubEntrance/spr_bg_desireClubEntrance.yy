{
    "id": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_desireClubEntrance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "870a925a-4358-453a-90fb-22ba0a6a7af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
            "compositeImage": {
                "id": "900e5ad8-5d34-40a7-bc5c-692988103524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870a925a-4358-453a-90fb-22ba0a6a7af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5464989c-6a70-493f-a1fa-e40cdda1b784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870a925a-4358-453a-90fb-22ba0a6a7af7",
                    "LayerId": "331b05d6-bf34-43cd-8c7e-586909fde2f9"
                }
            ]
        },
        {
            "id": "c89e3ec5-a450-45bc-934f-4532a6ef177d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
            "compositeImage": {
                "id": "e497ea25-9110-4c9d-b10d-48680e966943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c89e3ec5-a450-45bc-934f-4532a6ef177d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1682ef07-a4fc-4d99-b100-839ea31ad220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c89e3ec5-a450-45bc-934f-4532a6ef177d",
                    "LayerId": "331b05d6-bf34-43cd-8c7e-586909fde2f9"
                }
            ]
        },
        {
            "id": "50d60f2f-87bf-4f35-88cd-64b65f438f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
            "compositeImage": {
                "id": "c7bca54c-e925-4b3e-8728-0c8777c21c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d60f2f-87bf-4f35-88cd-64b65f438f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "160bb568-a5ea-4e36-81f8-feff51c68e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d60f2f-87bf-4f35-88cd-64b65f438f01",
                    "LayerId": "331b05d6-bf34-43cd-8c7e-586909fde2f9"
                }
            ]
        },
        {
            "id": "9dd037ad-ad84-4b66-ac8a-8891c195e6b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
            "compositeImage": {
                "id": "4d7259e0-a5a1-4b51-a325-a9a915fa073f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd037ad-ad84-4b66-ac8a-8891c195e6b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a849ed03-bac3-46ed-bbe2-2df843b0f971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd037ad-ad84-4b66-ac8a-8891c195e6b6",
                    "LayerId": "331b05d6-bf34-43cd-8c7e-586909fde2f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "331b05d6-bf34-43cd-8c7e-586909fde2f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "707ed95e-03c3-4d1f-92ac-af690dbe8841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}