{
    "id": "c05d14fa-a74f-4d14-8a41-e74606ecf31b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlaceholder_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "908f780e-7250-4804-a406-37f647d98ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c05d14fa-a74f-4d14-8a41-e74606ecf31b",
            "compositeImage": {
                "id": "2948e662-5abc-49fb-b69e-2e4bbd22c139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908f780e-7250-4804-a406-37f647d98ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d57a20-4f55-4942-8825-2b577fc59b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908f780e-7250-4804-a406-37f647d98ed9",
                    "LayerId": "9a4ca717-a708-47d3-bfc6-a5fb814f50a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9a4ca717-a708-47d3-bfc6-a5fb814f50a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c05d14fa-a74f-4d14-8a41-e74606ecf31b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}