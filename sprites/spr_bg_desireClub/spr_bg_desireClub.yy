{
    "id": "1880360e-2098-4fab-9eb0-5fdcf1a5007f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_desireClub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "386682e5-8a8d-4533-b493-05c9ff0e973e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1880360e-2098-4fab-9eb0-5fdcf1a5007f",
            "compositeImage": {
                "id": "4f972e3e-60e4-4daa-a3cb-0dd48cf22972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386682e5-8a8d-4533-b493-05c9ff0e973e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f19d1129-5c8e-43db-98b8-b88bb5c848bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386682e5-8a8d-4533-b493-05c9ff0e973e",
                    "LayerId": "4e4fbf85-3fde-4b48-85d6-2aea27ce44c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4e4fbf85-3fde-4b48-85d6-2aea27ce44c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1880360e-2098-4fab-9eb0-5fdcf1a5007f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}