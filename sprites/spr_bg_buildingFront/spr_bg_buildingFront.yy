{
    "id": "244a6da3-697d-48b3-95cb-2578628fa213",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_buildingFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3fde6dd9-7956-4379-9c23-e1bc5d29d010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244a6da3-697d-48b3-95cb-2578628fa213",
            "compositeImage": {
                "id": "d6a70f94-e0f5-49aa-aa5b-1ddd2d37b9f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fde6dd9-7956-4379-9c23-e1bc5d29d010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3a1a32-2ced-488a-9e85-b4fa7b46f6ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fde6dd9-7956-4379-9c23-e1bc5d29d010",
                    "LayerId": "6b94cd38-6de4-4293-b531-4b8acab844c7"
                }
            ]
        },
        {
            "id": "85be8e0f-d9eb-4a72-9a1d-f82b23e8b3de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244a6da3-697d-48b3-95cb-2578628fa213",
            "compositeImage": {
                "id": "99a10432-07c8-401a-b196-e99fdab63cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85be8e0f-d9eb-4a72-9a1d-f82b23e8b3de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1614421f-80cd-404f-bd85-beba34c7d17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85be8e0f-d9eb-4a72-9a1d-f82b23e8b3de",
                    "LayerId": "6b94cd38-6de4-4293-b531-4b8acab844c7"
                }
            ]
        },
        {
            "id": "fb6a4ce0-1bc5-47a9-bb57-84e8b6023fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244a6da3-697d-48b3-95cb-2578628fa213",
            "compositeImage": {
                "id": "70eba522-5b02-4ec6-95e7-e6e1ba5c9b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6a4ce0-1bc5-47a9-bb57-84e8b6023fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b21d0a6-8202-4708-96b6-67c1f9336c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6a4ce0-1bc5-47a9-bb57-84e8b6023fb9",
                    "LayerId": "6b94cd38-6de4-4293-b531-4b8acab844c7"
                }
            ]
        },
        {
            "id": "2235d9a7-1ba9-4586-b6f4-d801d05dcd48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244a6da3-697d-48b3-95cb-2578628fa213",
            "compositeImage": {
                "id": "a5b29a3e-b05b-40af-9466-ebd5c5121fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2235d9a7-1ba9-4586-b6f4-d801d05dcd48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88934975-a6e5-43b2-bed6-90bd3c145006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2235d9a7-1ba9-4586-b6f4-d801d05dcd48",
                    "LayerId": "6b94cd38-6de4-4293-b531-4b8acab844c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6b94cd38-6de4-4293-b531-4b8acab844c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "244a6da3-697d-48b3-95cb-2578628fa213",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}