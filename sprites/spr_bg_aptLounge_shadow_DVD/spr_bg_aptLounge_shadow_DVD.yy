{
    "id": "4289cb08-a0b7-4c17-8970-476c8e736873",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_DVD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 402,
    "bbox_left": 429,
    "bbox_right": 518,
    "bbox_top": 358,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e4120a1c-4a7c-4c3c-a743-93d4517d70fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4289cb08-a0b7-4c17-8970-476c8e736873",
            "compositeImage": {
                "id": "7cc64726-43ce-4d0a-b9ad-65e024c2718b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4120a1c-4a7c-4c3c-a743-93d4517d70fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a2f161-2cc6-496b-9588-ceba0d3b6c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4120a1c-4a7c-4c3c-a743-93d4517d70fd",
                    "LayerId": "615dbaab-819a-4725-8d80-107bf1e3c0f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "615dbaab-819a-4725-8d80-107bf1e3c0f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4289cb08-a0b7-4c17-8970-476c8e736873",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}