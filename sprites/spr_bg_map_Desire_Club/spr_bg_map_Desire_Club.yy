{
    "id": "9cc5dca0-0bb7-476c-a2b5-75fc53ad979b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_map_Desire_Club",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 329,
    "bbox_right": 588,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12ab289f-1960-4c68-8ab3-4413ae2a7a50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cc5dca0-0bb7-476c-a2b5-75fc53ad979b",
            "compositeImage": {
                "id": "afe64d80-95aa-46a3-800d-60253c4d9c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ab289f-1960-4c68-8ab3-4413ae2a7a50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c83bf93-b0f1-43d9-9f0f-733abddb04be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ab289f-1960-4c68-8ab3-4413ae2a7a50",
                    "LayerId": "c3b19231-fa6a-40af-804a-d207ef654598"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "c3b19231-fa6a-40af-804a-d207ef654598",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc5dca0-0bb7-476c-a2b5-75fc53ad979b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}