{
    "id": "ab561cfe-88e8-4d56-95ac-3e2b277679ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Penny_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 22,
    "bbox_right": 276,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3222b155-62a6-4436-92ce-c8beb470920f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab561cfe-88e8-4d56-95ac-3e2b277679ad",
            "compositeImage": {
                "id": "4affa1a0-d7bd-4256-beba-d94e31328908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3222b155-62a6-4436-92ce-c8beb470920f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b6b3ab-43e6-4e9e-903e-b90e2528a8e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3222b155-62a6-4436-92ce-c8beb470920f",
                    "LayerId": "c3c30a30-e4f9-4e54-825e-1a9b82b4dc19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "c3c30a30-e4f9-4e54-825e-1a9b82b4dc19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab561cfe-88e8-4d56-95ac-3e2b277679ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}