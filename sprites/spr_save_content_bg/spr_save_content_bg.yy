{
    "id": "b34734b7-5396-4f57-9c04-8440c560af3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_save_content_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f912d9d6-a2d0-4bb3-a49b-ef26b21f2fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34734b7-5396-4f57-9c04-8440c560af3b",
            "compositeImage": {
                "id": "bd409c2a-df00-430b-87b2-b1bccc173e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f912d9d6-a2d0-4bb3-a49b-ef26b21f2fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b0bff7-fc40-4144-963f-fd05c4130c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f912d9d6-a2d0-4bb3-a49b-ef26b21f2fbf",
                    "LayerId": "cfdfb8f7-2054-40ee-bdea-a03d0d228103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "cfdfb8f7-2054-40ee-bdea-a03d0d228103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b34734b7-5396-4f57-9c04-8440c560af3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}