{
    "id": "439a54aa-13e2-4c43-b5a8-22280ec37802",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_choice_lightgreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c47afef1-4c2a-43fc-b400-f6e43b0efa60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "439a54aa-13e2-4c43-b5a8-22280ec37802",
            "compositeImage": {
                "id": "2303031d-3c40-49be-9738-55204059ae16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47afef1-4c2a-43fc-b400-f6e43b0efa60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab13a62-cfd3-4f5b-825f-a4a51aa36c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47afef1-4c2a-43fc-b400-f6e43b0efa60",
                    "LayerId": "368568df-9e1c-49c7-93fa-9d4c8cf840b1"
                }
            ]
        },
        {
            "id": "00d7eeb0-918d-4cde-b142-8ad3b809ed33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "439a54aa-13e2-4c43-b5a8-22280ec37802",
            "compositeImage": {
                "id": "dfe6b90c-7ead-4d67-b353-9655ff8aed94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d7eeb0-918d-4cde-b142-8ad3b809ed33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "727622fa-a05b-4ac1-a315-97f6deaf7efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d7eeb0-918d-4cde-b142-8ad3b809ed33",
                    "LayerId": "368568df-9e1c-49c7-93fa-9d4c8cf840b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "368568df-9e1c-49c7-93fa-9d4c8cf840b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "439a54aa-13e2-4c43-b5a8-22280ec37802",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 50
}