{
    "id": "2ff0d848-8a33-4c81-95cd-8950f58a3aaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deadline_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 411,
    "bbox_left": 0,
    "bbox_right": 331,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dcc4912d-8126-4841-b070-4d0aaa732d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ff0d848-8a33-4c81-95cd-8950f58a3aaa",
            "compositeImage": {
                "id": "50c1e670-38cd-44ba-9f60-97dde315ada0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcc4912d-8126-4841-b070-4d0aaa732d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81c09d0-6114-415b-aa53-329e5b0d498f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcc4912d-8126-4841-b070-4d0aaa732d03",
                    "LayerId": "de7804df-0c5b-422d-8200-9ca73da11007"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 412,
    "layers": [
        {
            "id": "de7804df-0c5b-422d-8200-9ca73da11007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ff0d848-8a33-4c81-95cd-8950f58a3aaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 332,
    "xorig": 0,
    "yorig": 0
}