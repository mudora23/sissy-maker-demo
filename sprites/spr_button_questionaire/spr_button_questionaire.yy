{
    "id": "14421564-4f38-4ee3-82c1-960a4baa64ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_questionaire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "153b188d-d0a7-4721-9dc0-237150897a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14421564-4f38-4ee3-82c1-960a4baa64ab",
            "compositeImage": {
                "id": "3c152f63-84fc-4722-9571-de6a93bcfcb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "153b188d-d0a7-4721-9dc0-237150897a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afcc6942-87ac-48f9-9a57-c1aa563d7600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "153b188d-d0a7-4721-9dc0-237150897a23",
                    "LayerId": "fa92e4f7-b6c4-4661-a98b-579edc80e1af"
                }
            ]
        },
        {
            "id": "de604975-67a3-47bd-984d-f09c9e6caa3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14421564-4f38-4ee3-82c1-960a4baa64ab",
            "compositeImage": {
                "id": "b3bbbf8f-d883-400e-bc7a-dfc473108c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de604975-67a3-47bd-984d-f09c9e6caa3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17122c1-0f78-4ca8-a622-19bd4b83689c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de604975-67a3-47bd-984d-f09c9e6caa3d",
                    "LayerId": "fa92e4f7-b6c4-4661-a98b-579edc80e1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "fa92e4f7-b6c4-4661-a98b-579edc80e1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14421564-4f38-4ee3-82c1-960a4baa64ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}