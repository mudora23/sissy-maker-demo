{
    "id": "5c4cc762-7afe-4f6b-b215-8e3cf74a4fc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_no",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 15,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "61fc2e1e-63ce-43db-9809-ff3a988ec3e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c4cc762-7afe-4f6b-b215-8e3cf74a4fc8",
            "compositeImage": {
                "id": "c0773709-a477-4f10-86bb-60708bad69a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61fc2e1e-63ce-43db-9809-ff3a988ec3e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f7546f-1e7a-44fe-a656-8b9a36ea0e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61fc2e1e-63ce-43db-9809-ff3a988ec3e9",
                    "LayerId": "330ff95e-6596-4efa-a6cf-83989eb65fc8"
                }
            ]
        },
        {
            "id": "1cb2433f-a523-43f6-b257-ca4c8f98c7e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c4cc762-7afe-4f6b-b215-8e3cf74a4fc8",
            "compositeImage": {
                "id": "28662b22-63e6-4a87-9e35-94d0e41bcf79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cb2433f-a523-43f6-b257-ca4c8f98c7e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "587b6a90-710b-47c3-a08c-172d5d2de900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cb2433f-a523-43f6-b257-ca4c8f98c7e9",
                    "LayerId": "330ff95e-6596-4efa-a6cf-83989eb65fc8"
                }
            ]
        },
        {
            "id": "f834dc64-d0de-41a1-8a10-1672cf1619a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c4cc762-7afe-4f6b-b215-8e3cf74a4fc8",
            "compositeImage": {
                "id": "a24189a4-fcd6-4f6f-a5d3-d742be7d612b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f834dc64-d0de-41a1-8a10-1672cf1619a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a344683-84f2-495c-9659-625e80cbc457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f834dc64-d0de-41a1-8a10-1672cf1619a7",
                    "LayerId": "330ff95e-6596-4efa-a6cf-83989eb65fc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "330ff95e-6596-4efa-a6cf-83989eb65fc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c4cc762-7afe-4f6b-b215-8e3cf74a4fc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 0,
    "yorig": 0
}