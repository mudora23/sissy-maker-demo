{
    "id": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_buttons_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 24,
    "bbox_right": 133,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "48ffdbca-c058-4091-bc46-02e1d149514c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "dcb3faa5-117b-4c57-90ae-764a64f23a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ffdbca-c058-4091-bc46-02e1d149514c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27b98d29-02fd-4870-8d48-299c7a7d7a8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ffdbca-c058-4091-bc46-02e1d149514c",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        },
        {
            "id": "f656edb6-9af9-47cb-88c2-85941d77ec8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "e6ec571f-035e-4b0b-8b06-638986d9d5b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f656edb6-9af9-47cb-88c2-85941d77ec8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78fbaaf-e43d-4bfd-9416-f8ee502de54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f656edb6-9af9-47cb-88c2-85941d77ec8c",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        },
        {
            "id": "5883fe82-8360-4008-bba2-3b16896f7036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "79be67c9-8096-4cee-b6a1-e6846290edf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5883fe82-8360-4008-bba2-3b16896f7036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22b1432-ecbc-4547-a8a7-13a737c4aefc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5883fe82-8360-4008-bba2-3b16896f7036",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        },
        {
            "id": "8ff6920c-ff5a-4d2b-8078-6fd3c7dab2d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "4690a9df-e46a-4637-9080-26624f3f7615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff6920c-ff5a-4d2b-8078-6fd3c7dab2d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffee5736-1f35-4e94-96fb-a299fb10ec7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff6920c-ff5a-4d2b-8078-6fd3c7dab2d6",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        },
        {
            "id": "82327ecc-a855-471b-baeb-dc4b147cc1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "ee4c1d19-acfb-4ba5-8b86-71140d40a7cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82327ecc-a855-471b-baeb-dc4b147cc1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e59ad6e-7b75-47d3-905a-10248bfc74f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82327ecc-a855-471b-baeb-dc4b147cc1b6",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        },
        {
            "id": "0d063852-1223-4051-9486-59e43be36d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "compositeImage": {
                "id": "069657c4-c6e4-4ccb-93f5-1c27879aa3c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d063852-1223-4051-9486-59e43be36d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2a38d92-2810-43ac-b290-18d8f38381d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d063852-1223-4051-9486-59e43be36d7e",
                    "LayerId": "9e05b422-a36a-48f1-aa88-76c93259bc0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "9e05b422-a36a-48f1-aa88-76c93259bc0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dab26c19-9c8f-4fd2-9966-ee90c2ad2356",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}