{
    "id": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STPdanceHall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bd2de285-0140-402e-9dfc-0cdcaf36ffca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
            "compositeImage": {
                "id": "eff145f7-bd62-475d-a5b6-ddab43cebc19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd2de285-0140-402e-9dfc-0cdcaf36ffca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80d813c-a860-44f7-8b52-b95c0c00e826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd2de285-0140-402e-9dfc-0cdcaf36ffca",
                    "LayerId": "d13d17fe-347e-418e-b99e-40c6e69b1bc3"
                }
            ]
        },
        {
            "id": "66c85365-4eea-4a09-8d4b-714565dd3f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
            "compositeImage": {
                "id": "9cc661d4-0803-4534-bd47-87c0caf08f09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c85365-4eea-4a09-8d4b-714565dd3f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966ebe31-57a6-46aa-9934-e82da4dcaa27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c85365-4eea-4a09-8d4b-714565dd3f23",
                    "LayerId": "d13d17fe-347e-418e-b99e-40c6e69b1bc3"
                }
            ]
        },
        {
            "id": "f668df91-5c16-4540-b75a-66a6a266f1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
            "compositeImage": {
                "id": "c5864f7b-c3a3-4bda-a12a-384d2af909f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f668df91-5c16-4540-b75a-66a6a266f1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74bdabf-e1dc-4f8a-8b26-ccec76488a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f668df91-5c16-4540-b75a-66a6a266f1e9",
                    "LayerId": "d13d17fe-347e-418e-b99e-40c6e69b1bc3"
                }
            ]
        },
        {
            "id": "d61922e3-1027-4944-9b7c-d5116dd07d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
            "compositeImage": {
                "id": "61c55714-b5fe-4aaf-9e3d-d8b1b7381c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61922e3-1027-4944-9b7c-d5116dd07d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d29cf4-f574-4c8d-a8d5-5a821f589750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61922e3-1027-4944-9b7c-d5116dd07d64",
                    "LayerId": "d13d17fe-347e-418e-b99e-40c6e69b1bc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "d13d17fe-347e-418e-b99e-40c6e69b1bc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71db56d7-f2f4-44a5-995b-9e961e4a52d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}