{
    "id": "a50b0a30-c3c2-40d3-b222-36f31aeda46e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_sp_PennyBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1411,
    "bbox_left": 70,
    "bbox_right": 632,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9713bb77-c237-4a8d-b851-5c99af929530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a50b0a30-c3c2-40d3-b222-36f31aeda46e",
            "compositeImage": {
                "id": "a681dd9d-087e-4f35-9fa7-772782eca496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9713bb77-c237-4a8d-b851-5c99af929530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a066d6cf-6631-4cae-b6a0-64ab57b2ac02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9713bb77-c237-4a8d-b851-5c99af929530",
                    "LayerId": "09a19a12-aaa6-43ac-9fe1-fca945efcf71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1412,
    "layers": [
        {
            "id": "09a19a12-aaa6-43ac-9fe1-fca945efcf71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a50b0a30-c3c2-40d3-b222-36f31aeda46e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 635,
    "xorig": 0,
    "yorig": 0
}