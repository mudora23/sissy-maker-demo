{
    "id": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_activities_clock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 435,
    "bbox_left": 405,
    "bbox_right": 634,
    "bbox_top": 165,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee514c1a-7d0c-497b-ada7-8f23de1be78f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "ebcd2124-46f2-4b18-a114-ec720f5521bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee514c1a-7d0c-497b-ada7-8f23de1be78f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8caf460-b550-4415-87ba-ab222e418ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee514c1a-7d0c-497b-ada7-8f23de1be78f",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        },
        {
            "id": "5b039b7b-1768-45f7-beeb-4f4fea5a40b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "28a07653-2c83-420d-b6e2-63874685196f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b039b7b-1768-45f7-beeb-4f4fea5a40b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50726e2-247c-421c-b70b-9d5deab66007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b039b7b-1768-45f7-beeb-4f4fea5a40b5",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        },
        {
            "id": "beac0306-bc4c-49cd-ae36-7ab2c1941f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "03fc99cb-8222-4132-8136-7328b0b16793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beac0306-bc4c-49cd-ae36-7ab2c1941f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6858c2d8-a23d-461a-b549-84f94df5ac7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beac0306-bc4c-49cd-ae36-7ab2c1941f7c",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        },
        {
            "id": "4f417768-1ee6-4fe9-88bf-94c1438add33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "4249119a-8b2f-4de8-897f-97efb7f6f910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f417768-1ee6-4fe9-88bf-94c1438add33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df4902c-5640-48d8-902a-4447121ddd2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f417768-1ee6-4fe9-88bf-94c1438add33",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        },
        {
            "id": "96813966-8723-4d24-a3a9-6c6ab83075a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "8c93a713-37e2-4851-b7f9-3d2237e43209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96813966-8723-4d24-a3a9-6c6ab83075a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15dcb3ff-15f3-4679-bec6-f2cfa37d6979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96813966-8723-4d24-a3a9-6c6ab83075a7",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        },
        {
            "id": "8045507c-b095-43f2-992a-17646d51e395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "compositeImage": {
                "id": "fbb81e0d-9083-47f1-877d-fd1ec0f11f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8045507c-b095-43f2-992a-17646d51e395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c76212-7d57-4c55-a049-d2fc744d115d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8045507c-b095-43f2-992a-17646d51e395",
                    "LayerId": "be71837d-60cb-4bba-908c-bc72b7ef5621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "be71837d-60cb-4bba-908c-bc72b7ef5621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}