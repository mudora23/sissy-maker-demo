{
    "id": "9ae491c1-da24-40ad-90b7-045d37c514b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_lab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 514,
    "bbox_left": 208,
    "bbox_right": 378,
    "bbox_top": 374,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "62351266-c22a-4a93-9615-7e48380d73cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ae491c1-da24-40ad-90b7-045d37c514b6",
            "compositeImage": {
                "id": "9db5d54e-9e8b-4cdd-a367-f78e012c5208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62351266-c22a-4a93-9615-7e48380d73cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e6e426-2b55-4806-91df-a2cead1b8bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62351266-c22a-4a93-9615-7e48380d73cf",
                    "LayerId": "6b2989ba-e0c9-4a78-aca9-5fb21ff7f705"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6b2989ba-e0c9-4a78-aca9-5fb21ff7f705",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ae491c1-da24-40ad-90b7-045d37c514b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}