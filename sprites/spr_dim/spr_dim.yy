{
    "id": "fee5a32e-e1db-4ce7-a032-d1d7c38348ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6eacb67b-383f-44b6-b223-ef838b742f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fee5a32e-e1db-4ce7-a032-d1d7c38348ac",
            "compositeImage": {
                "id": "45f92295-b3e3-448b-843a-ed82caf954e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eacb67b-383f-44b6-b223-ef838b742f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31906d70-1ac0-4a43-9aa8-d44f395f0a75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eacb67b-383f-44b6-b223-ef838b742f7c",
                    "LayerId": "98db4d42-2cab-41a5-b0c0-71051cb694d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "98db4d42-2cab-41a5-b0c0-71051cb694d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fee5a32e-e1db-4ce7-a032-d1d7c38348ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}