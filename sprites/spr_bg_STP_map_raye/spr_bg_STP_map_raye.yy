{
    "id": "59471be1-d3e6-4765-98cf-a76225ddd6bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_raye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 132,
    "bbox_right": 251,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "76a9225c-d180-4133-b31a-e33d7fab1827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59471be1-d3e6-4765-98cf-a76225ddd6bc",
            "compositeImage": {
                "id": "90dfd6c2-09e8-4e9c-9fe3-b6d916dded4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a9225c-d180-4133-b31a-e33d7fab1827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b3bc9f-77ef-4a76-b3ad-e3afb3397fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a9225c-d180-4133-b31a-e33d7fab1827",
                    "LayerId": "06a1e6ad-5072-465c-967f-78bf7852186e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "06a1e6ad-5072-465c-967f-78bf7852186e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59471be1-d3e6-4765-98cf-a76225ddd6bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}