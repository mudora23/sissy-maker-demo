{
    "id": "9a672b3b-013a-4080-be7c-75e0586c3cb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_aptLounge_shadow_door3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 529,
    "bbox_left": 655,
    "bbox_right": 794,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2b60c576-6d39-40a3-9ee8-e2647c530e3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a672b3b-013a-4080-be7c-75e0586c3cb7",
            "compositeImage": {
                "id": "b7f5dc21-bcd1-453b-b1b6-81a080ce02dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b60c576-6d39-40a3-9ee8-e2647c530e3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94fc27d9-65d9-4dbe-ad46-9f082eeee6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b60c576-6d39-40a3-9ee8-e2647c530e3b",
                    "LayerId": "24046101-3a69-4813-9025-b8b60ac4faf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "24046101-3a69-4813-9025-b8b60ac4faf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a672b3b-013a-4080-be7c-75e0586c3cb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}