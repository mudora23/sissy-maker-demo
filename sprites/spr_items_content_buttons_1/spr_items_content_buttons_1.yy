{
    "id": "787a1836-9ad8-4492-9f01-07a7af726ee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items_content_buttons_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 24,
    "bbox_right": 121,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fc2d3af6-eb21-4edd-9436-32347b1097f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "83ca89df-578d-4b61-bb51-59f11b4a2bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2d3af6-eb21-4edd-9436-32347b1097f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20451945-faca-4643-9098-3983e96d1f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2d3af6-eb21-4edd-9436-32347b1097f8",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        },
        {
            "id": "44a93454-2d26-48ff-a5d7-36570507c3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "7f3bd0e7-64c2-4d8d-a39f-7c5545bfd400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a93454-2d26-48ff-a5d7-36570507c3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057e59f4-7ec0-4d50-835c-8b2c4a4b83c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a93454-2d26-48ff-a5d7-36570507c3bf",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        },
        {
            "id": "7eb52080-8778-48d7-9227-876e9dc55db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "7f790793-df19-4acf-94d2-39b53df8286c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eb52080-8778-48d7-9227-876e9dc55db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224a0799-dab1-4f38-b14e-9a7de20044e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb52080-8778-48d7-9227-876e9dc55db1",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        },
        {
            "id": "b80d2c39-8332-4a3b-9bef-ac5adc938dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "1fa94117-e16a-4cd1-8385-23eb0cfe5dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b80d2c39-8332-4a3b-9bef-ac5adc938dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1de6e00-7d36-4bea-af95-85a2be0589c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80d2c39-8332-4a3b-9bef-ac5adc938dea",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        },
        {
            "id": "ec76dab5-2589-4d97-8b80-847f075232df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "d3bdd3e1-f2bd-411b-85a1-fd00f0fff9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec76dab5-2589-4d97-8b80-847f075232df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19f18446-7494-42e7-90bd-8a0086037c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec76dab5-2589-4d97-8b80-847f075232df",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        },
        {
            "id": "6d2098cb-c23e-4b41-a103-6566fa08099a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "compositeImage": {
                "id": "5bfe2881-8e59-4f13-a362-f321ad91f234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2098cb-c23e-4b41-a103-6566fa08099a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569dfbd6-af46-4324-9c5e-689906edbb0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2098cb-c23e-4b41-a103-6566fa08099a",
                    "LayerId": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "1bd06b42-aef4-45e2-9245-bb5b9a42fa36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "787a1836-9ad8-4492-9f01-07a7af726ee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}