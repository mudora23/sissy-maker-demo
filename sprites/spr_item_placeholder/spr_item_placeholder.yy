{
    "id": "064fe7e4-37c3-440c-bcbf-881e64b53a54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_placeholder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eb7a1e0d-a2c8-4dbb-83a9-31487d171a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "064fe7e4-37c3-440c-bcbf-881e64b53a54",
            "compositeImage": {
                "id": "1a8fe4d6-0551-4a74-bb5d-f425d4c2a95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7a1e0d-a2c8-4dbb-83a9-31487d171a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a43bf473-cebb-46f8-abf6-5904e7f9c347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7a1e0d-a2c8-4dbb-83a9-31487d171a21",
                    "LayerId": "f0a7e182-d0c2-43bb-bc57-16dc314eb9d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "f0a7e182-d0c2-43bb-bc57-16dc314eb9d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "064fe7e4-37c3-440c-bcbf-881e64b53a54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}