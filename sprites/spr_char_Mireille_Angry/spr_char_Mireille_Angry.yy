{
    "id": "e97e05e2-4094-4fe2-aeb7-ebe46c3d1a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Mireille_Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f88db840-1d3b-4c7e-9469-245d91847cdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e97e05e2-4094-4fe2-aeb7-ebe46c3d1a86",
            "compositeImage": {
                "id": "6605ef61-0993-4a9d-bf86-379833fed4bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88db840-1d3b-4c7e-9469-245d91847cdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb351d6a-a89d-466d-a760-b27f8b9d6eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88db840-1d3b-4c7e-9469-245d91847cdc",
                    "LayerId": "e7252730-c73f-480a-acbf-20abdd7abcf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "e7252730-c73f-480a-acbf-20abdd7abcf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e97e05e2-4094-4fe2-aeb7-ebe46c3d1a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 390,
    "xorig": 0,
    "yorig": 0
}