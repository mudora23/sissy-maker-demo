{
    "id": "835285ca-515c-4c10-a5dd-26824ebe69fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_STP_map_shadow_gym",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 190,
    "bbox_left": 264,
    "bbox_right": 590,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "54499397-22ae-4893-bdfd-75be899f7b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "835285ca-515c-4c10-a5dd-26824ebe69fd",
            "compositeImage": {
                "id": "7cb47b21-6308-49cc-8df9-a54612733798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54499397-22ae-4893-bdfd-75be899f7b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b2a048f-5d9e-4bb1-b03d-a87c94ff0c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54499397-22ae-4893-bdfd-75be899f7b06",
                    "LayerId": "a852d734-eb41-42e1-82bf-2bed57a02e3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a852d734-eb41-42e1-82bf-2bed57a02e3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "835285ca-515c-4c10-a5dd-26824ebe69fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}