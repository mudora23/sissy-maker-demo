{
    "id": "4538907a-b999-45ba-9567-7e6b73875815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_kanesResidence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "015796e1-f476-4550-ba77-2cf263806c1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4538907a-b999-45ba-9567-7e6b73875815",
            "compositeImage": {
                "id": "f6c04536-8269-4eb1-bfe3-dfb088d48e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015796e1-f476-4550-ba77-2cf263806c1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad877af-c32a-45ee-a699-8a72864ffce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015796e1-f476-4550-ba77-2cf263806c1b",
                    "LayerId": "09053ab5-b299-44eb-8171-29466de3d942"
                }
            ]
        },
        {
            "id": "0a38000c-6134-4553-a1ab-06e71820fb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4538907a-b999-45ba-9567-7e6b73875815",
            "compositeImage": {
                "id": "302a5695-acb3-4668-adb2-10f5dbd21e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a38000c-6134-4553-a1ab-06e71820fb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354a49a1-2986-43b8-80a9-1b6b9929664a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a38000c-6134-4553-a1ab-06e71820fb56",
                    "LayerId": "09053ab5-b299-44eb-8171-29466de3d942"
                }
            ]
        },
        {
            "id": "b94d6540-2913-4d7a-a973-75a3b104c105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4538907a-b999-45ba-9567-7e6b73875815",
            "compositeImage": {
                "id": "bf87353c-957b-4acd-9030-e82ac8228e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94d6540-2913-4d7a-a973-75a3b104c105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b57732-198c-4e49-a7c3-77c154430804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94d6540-2913-4d7a-a973-75a3b104c105",
                    "LayerId": "09053ab5-b299-44eb-8171-29466de3d942"
                }
            ]
        },
        {
            "id": "dfd2d366-2d0c-4601-9cf5-f6fce287fa5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4538907a-b999-45ba-9567-7e6b73875815",
            "compositeImage": {
                "id": "d9a7c6fe-b8b2-43de-93d1-65925fd1f115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd2d366-2d0c-4601-9cf5-f6fce287fa5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68c4963-e287-47b2-86c0-4110ad12c716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd2d366-2d0c-4601-9cf5-f6fce287fa5a",
                    "LayerId": "09053ab5-b299-44eb-8171-29466de3d942"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "09053ab5-b299-44eb-8171-29466de3d942",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4538907a-b999-45ba-9567-7e6b73875815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}