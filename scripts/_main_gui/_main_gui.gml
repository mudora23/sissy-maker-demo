///_main_gui(visible?);
/// @description
/// @param visible?
if scene_is_current()
{
	main_gui(argument0);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;