///_label(label);
/// @description
/// @param label
if !scene_map[? SCENE_LABEL_MARKED]
	scene_add_label(argument0, scene_map[? SCENE_POSI]);

if scene_is_current()
	scene_jump_next();

scene_map[? SCENE_POSI]++;