///draw_text_outlined(x,y,string,colour,outline_colour,size_ratio,outline_width,outfidelity);
/// @description
/// @param x
/// @param y
/// @param string
/// @param colour
/// @param outline_colour
/// @param size_ratio
/// @param outline_width
/// @param outfidelity - Fidelity of outline (recommended: 4 for small, 8 for medium, 16 for larger. Watch your performance!)


var dto_dcol=draw_get_color();

draw_set_color(argument4);

for(var dto_i=45; dto_i<405; dto_i+=360/argument7)
{
    draw_text_transformed(argument0+lengthdir_x(argument6,dto_i),argument1+lengthdir_y(argument6,dto_i),argument2,argument5,argument5,0);
}

draw_set_color(argument3);

draw_text_transformed(argument0,argument1,argument2,argument5,argument5,0);

