///_hide_all_except(name1,name2,name3,name4,name5,fade_speed);
/// @description - hide all sprite object except the given sprite character(s)
/// @param name1[optional]
/// @param name2[optional]
/// @param name3[optional]
/// @param name4[optional]
/// @param name5[optional]
/// @param fade_speed
if scene_is_current()
{
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst = instance_find(obj_sprite_object,i);
		
		if (argument_count < 2 || inst.name != argument[0]) &&
		   (argument_count < 3 || inst.name != argument[1]) &&
		   (argument_count < 4 || inst.name != argument[2]) &&
		   (argument_count < 5 || inst.name != argument[3]) &&
		   (argument_count < 6 || inst.name != argument[4])
		{
			inst.hiding = true;	
			inst.fade_speed = argument[argument_count-1];	
		}
    }
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;