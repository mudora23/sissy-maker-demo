///_start();
/// @description

scene_map[? SCENE_POSI] = 0;

if scene_is_current()
{
	if scene_map[? SCENE_LABEL_MARKED] && !sissy_maker_is_loading()
	{
		// sick event
		var health_check_times = (var_get(VAR_ENERGY) == 0) + (var_get(VAR_HP) == 0) + (var_get(VAR_ENERGY) < 20 && var_get(VAR_HP) < 50);
		var success = 0;
		var stamina_level = attributes_xp_get_level(var_get(VAR_STAMINA));
    
		repeat(health_check_times)
			for (var i = 0; i < stamina_level;i++)
				if random(1) >= 0.5
				{
				    success++;
				    break;
				}
    
		// sick
		if success < health_check_times
		{
			room_goto_ext(room_scene_hospital,true);
		}
		
		// not sick; continue the game
		scene_jump_next();
	}
	
	if scene_map[? SCENE_POSI_STEP] == 2
	{
		//auto-save
		obj_control.alarm[10]=1;
	}
	else if scene_map[? SCENE_POSI_STEP] >= 3
		scene_map[? SCENE_LABEL_MARKED] = true;
}

scene_map[? SCENE_POSI]++;