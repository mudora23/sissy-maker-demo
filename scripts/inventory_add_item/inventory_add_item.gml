///inventory_add_item(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];

	if item_map[? INVENTORY_ITEM_AMOUNT] == 0 inventory_reposition_item_to_last(item_name);
	item_map[? INVENTORY_ITEM_AMOUNT]++;
	inventory_reposition_empty_items_to_last();
	
	if item_name != ITEM_Job_Assignments && 
	   item_name != ITEM_Gum
	   {
			if instance_exists(obj_scene) && !audio_is_playing(sfx_get_item)
			    scene_play_sfx(sfx_get_item);
	   }

}

