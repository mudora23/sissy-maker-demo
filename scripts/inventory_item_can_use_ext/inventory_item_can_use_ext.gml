///inventory_item_can_use_ext(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];
	var item_cons_map = item_constants_map[? item_name];
	
	if item_name == ITEM_Lotion && var_get(VAR_CUP_SIZE) >= 3
		return "Requirement Not Met";
	else if item_name == ITEM_So_Zore_Lotion && var_get(VAR_ANUS) != -1
		return "Requirement Not Met";
	else if item_cons_map[? ITEM_USED_PER_DAY_TIMES_MAX] == 0
		return "Passive";
	else if item_map[? INVENTORY_ITEM_USED_LAST_DAY] != var_get(VAR_DAY) || item_cons_map[? ITEM_USED_PER_DAY_TIMES_MAX] < 0
		return "Active";
	else if item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES] < item_cons_map[? ITEM_USED_PER_DAY_TIMES_MAX]
		return "Active";
	else
		return "Reached Daily Limit";

}

