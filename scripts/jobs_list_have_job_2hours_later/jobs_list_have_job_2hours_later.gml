///jobs_list_have_job_2hours_later(begin_day,end_day,begin_0_23,end_0_23,name[optional]);
/// @description - return true if there's any job assigned during this period of time
/// @param begin_day
/// @param end_day
/// @param begin_0_23
/// @param end_0_23
/// @param name[optional]

var begin_day = argument[0];
var end_day = argument[1];
var begin_0_23 = argument[2];
var end_0_23 = argument[3];

begin_0_23+=2;
end_0_23+=2;
if begin_0_23 > 23
{
	begin_0_23 -= 24;
	begin_day++;
}
if end_0_23 > 23
{
	end_0_23 -= 24;
	end_day++;
}

if argument_count > 4
	return jobs_list_have_job(begin_day,end_day,begin_0_23,end_0_23,argument[4]);
else
	return jobs_list_have_job(begin_day,end_day,begin_0_23,end_0_23);

