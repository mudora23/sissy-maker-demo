///var_add_ext(var,amount,lower_bound,upper_bound);
/// @description -
/// @param var
/// @param amount
/// @param lower_bound
/// @param upper_bound

var old_value = var_get(argument0);


// check VAR_AFF_CHRIS_PENALTY
if argument0 == VAR_AFF_CHRIS_PENALTY
{
	if old_value + argument1 < 1
	{
		var_add(VAR_AFF_CHRIS_PENALTY,1 - (old_value + argument1));
	}
}


obj_control.var_map[? argument0] = clamp(var_get(argument0)+argument1,argument2,argument3);
floating_text_create(argument0,true,var_get(argument0)-old_value);

if string_copy(argument0,1,9) == "VAR_EVENT"
{
	//var event_map = sissy_maker_load_map("event.dat",obj_control.secretString);
	if ds_map_exists(obj_control.event_map,argument0)
	{
		obj_control.event_map[? argument0] = max(obj_control.event_map[? argument0],obj_control.var_map[? argument0]);
	}
	else
	{
		obj_control.event_map[? argument0] = obj_control.var_map[? argument0];
	}
	sissy_maker_save_map("event.dat",obj_control.event_map,obj_control.secretString);
	//ds_map_destroy(event_map);
}
