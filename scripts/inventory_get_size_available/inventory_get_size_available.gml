///inventory_get_size_available();
/// @description
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var current_size = 0;
	
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		
		if item_map[? INVENTORY_ITEM_AMOUNT] > 0
			current_size++;
	
		item_name = ds_map_find_next(inventory_map,item_name);
	}

	return current_size;

}

