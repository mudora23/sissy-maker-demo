///outfit_set_previous();
var outfit_current = var_get(VAR_OUTFIT);
var outfit_total = var_get(VAR_OUTFIT_TOTAL);
if outfit_current <= 1
    var_set(VAR_OUTFIT,outfit_total);
else
    var_set(VAR_OUTFIT,outfit_current-1);
