///sprites_load_from_map(map);
/// @description
/// @param map
with obj_sprite_object instance_destroy();

var x_list = argument0[? "x"];
var y_list = argument0[? "y"];
var sprite_index_list = argument0[? "sprite_index"];
var image_alpha_list = argument0[? "image_alpha"];
var image_index_list = argument0[? "image_index"];
var target_sprite_index_list = argument0[? "target_sprite_index"];
var target_sprite_alpha_list = argument0[? "target_sprite_alpha"];
var target_sprite_alpha_change_rate_list = argument0[? "target_sprite_alpha_change_rate"];
var target_image_index_list = argument0[? "target_image_index"];
var fade_speed_list = argument0[? "fade_speed"];
var slide_speed_list = argument0[? "slide_speed"];
var xtarget_list = argument0[? "xtarget"];
var ytarget_list = argument0[? "ytarget"];
var hiding_list = argument0[? "hiding"];
var flip_list = argument0[? "flip"];
var size_ratio_list = argument0[? "size_ratio"];
var name_list = argument0[? "name"];
var depth_list = argument0[? "depth"];

var list_size = ds_list_size(depth_list);

for(var i = 0; i < list_size;i++)
{
	var inst = instance_create_depth(0,0,DEPTH_CHOICE,obj_sprite_object);
	inst.x = x_list[| i];
	inst.y = y_list[| i];
	if sprite_index_list[| i] == "" inst.sprite_index = noone;
	else inst.sprite_index = asset_get_index(sprite_index_list[| i]);
	inst.image_alpha = image_alpha_list[| i];
	inst.image_index = image_index_list[| i];
	if sprite_index_list[| i] == "" inst.target_sprite_index = noone;
	else inst.target_sprite_index = asset_get_index(sprite_index_list[| i]);
	inst.target_sprite_alpha = target_sprite_alpha_list[| i];
	inst.target_sprite_alpha_change_rate = target_sprite_alpha_change_rate_list[| i];
	inst.target_image_index = target_image_index_list[| i];
	inst.fade_speed = fade_speed_list[| i];
	inst.slide_speed = slide_speed_list[| i];
	inst.xtarget = xtarget_list[| i];
	inst.ytarget = ytarget_list[| i];
	inst.hiding = hiding_list[| i];
	inst.flip = flip_list[| i];
	inst.size_ratio = size_ratio_list[| i];
	inst.name = name_list[| i];
	inst.depth = depth_list[| i];

}
