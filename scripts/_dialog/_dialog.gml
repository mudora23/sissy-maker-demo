///_dialog(string,clear_dialog[optional]);
/// @description
/// @param string
/// @param clear_dialog[optional] - the next posi needs to be _block()

if scene_is_current()
{
	if scene_map[? SCENE_TEXT] == ""
	{
		// string processing and set dialog <scene_map[? SCENE_TEXT]>
		scene_map[? SCENE_TEXT] = dialog_ref_init(argument[0]);
	}
	if obj_textbox.textbox_alpha >= 0.98
	{
		scene_map[? SCENE_CHAR_POSI]+= delta_time_sec()*obj_control.control_map[? CONTROL_TEXT_SPEED]; 
		
		var show_text_number = clamp(floor(scene_map[? SCENE_CHAR_POSI]),0,string_length(scene_map[? SCENE_TEXT]));
	
	    if show_text_number >= 1
	    {
	        var char_x = 50;
	        var char_y = 462;
        
			draw_reset();
	        for (var i = 1; i <= show_text_number;i++)
	        {
	            dialog_set_ref(i);
	            draw_text_transformed(char_x, char_y, string_char_at(scene_map[? SCENE_TEXT], i), 1, 1, 0);
	            if char_x > 650 && string_char_at(scene_map[? SCENE_TEXT], i) == " "
	            {
	                char_x = 50;
	                char_y += 30;
	            }
	            else
	            {
	                char_x += string_width(string_char_at(scene_map[? SCENE_TEXT], i));
	            }
        
	        }       
        
	    }
	
		// dialog skip (clicked)
		if ((obj_control.action_button_pressed && !mouse_on_gui()) || obj_control.action_button_holding_time >= 0.8) && !menu_exists() && !debug_menu_exists()
		{
			if show_text_number == string_length(scene_map[? SCENE_TEXT]) && scene_map[? SCENE_POSI_SEC] >= 0.1
			{
				obj_control.action_button_pressed = false;
				if argument_count > 1
					scene_jump_next(argument[1]);
				else
					scene_jump_next();
			}
			else if show_text_number < string_length(scene_map[? SCENE_TEXT]) && scene_map[? SCENE_POSI_STEP] >= 5
			{
				scene_map[? SCENE_CHAR_POSI] = string_length(scene_map[? SCENE_TEXT])
			}
		}

		
	}

}

scene_map[? SCENE_POSI]++;