///room_goto_ext(room,fade);
/// @description - requires obj_transition
/// @param room
/// @param fade

obj_transition.next_room = argument[0];
obj_transition.fade = argument[1];