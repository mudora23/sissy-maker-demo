///inventory_daily_times_reset();
/// @description
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		
		item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES] = 0;
		
		var item_name = ds_map_find_next(inventory_map,item_name);
	}

}


