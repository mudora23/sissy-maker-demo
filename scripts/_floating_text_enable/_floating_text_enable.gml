///_floating_text_enable(enable);
/// @description
/// @param enable
if scene_is_current()
{
    floating_text_enable(argument[0]);
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;