///_var_set(var,value);
/// @description
/// @param var
/// @param value
if scene_is_current()
{
	var_set(argument0,argument1);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;