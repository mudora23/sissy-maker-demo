///ds_list_value_exists(id,value);
/// @description
/// @param id
/// @param value
return ds_list_find_index(argument[0],argument[1]) >= 0