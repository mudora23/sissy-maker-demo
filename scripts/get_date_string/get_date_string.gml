///get_date_string();
/// @description

var month = date_get_month(date_current_datetime()); 
var day = date_get_day(date_current_datetime()); 
var hour = date_get_hour(date_current_datetime());
if hour < 10 hour = "0"+string(hour);
var minute = date_get_minute(date_current_datetime());
if minute < 10 minute = "0"+string(minute);
if month == 1
    var month_string = "JANUARY";
else if month == 2
    var month_string = "FEBRUARY";
else if month == 3
    var month_string = "MARCH";
else if month == 4
    var month_string = "APRIL";
else if month == 5
    var month_string = "MAY";
else if month == 6
    var month_string = "JUNE";
else if month == 7
    var month_string = "JULY";
else if month == 8
    var month_string = "AUGUEST";
else if month == 9
    var month_string = "SEPTEMBER";
else if month == 10
    var month_string = "OCTOBER";
else if month == 11
    var month_string = "NOVEMBER";
else if month == 12
    var month_string = "DECEMBER";
else
    var month_string = "";
return month_string + " " + string(day)+", "+string(hour)+":"+string(minute);
