///ref_load_from_map(map);
/// @description
/// @param map

var color_index_list = argument0[? "color_index"];
var color_color_list = argument0[? "color_color"];
var font_index_list = argument0[? "font_index"];
var font_font_list = argument0[? "font_font"];


ds_list_destroy(obj_scene.scene_text_ref_color_index);
ds_list_destroy(obj_scene.scene_text_ref_color_color);
ds_list_destroy(obj_scene.scene_text_ref_font_index);
ds_list_destroy(obj_scene.scene_text_ref_font_font);
//ds_map_destroy(obj_scene.text_ref_map);

//obj_scene.text_ref_map = ds_map_create();
obj_scene.scene_text_ref_color_index = ds_list_create();
obj_scene.scene_text_ref_color_color = ds_list_create();
obj_scene.scene_text_ref_font_index = ds_list_create();
obj_scene.scene_text_ref_font_font = ds_list_create();

ds_list_copy(obj_scene.scene_text_ref_color_index, color_index_list);
ds_list_copy(obj_scene.scene_text_ref_color_color, color_color_list);
ds_list_copy(obj_scene.scene_text_ref_font_index, font_index_list);
ds_list_copy(obj_scene.scene_text_ref_font_font, font_font_list);
//obj_scene.scene_text_ref_color_index = color_index_list;
//obj_scene.scene_text_ref_color_color = color_color_list;
//obj_scene.scene_text_ref_font_index = font_index_list;
//obj_scene.scene_text_ref_font_font = font_font_list;

//ds_list_copy(obj_scene.scene_text_ref_color_index,color_index_list);
//ds_list_copy(obj_scene.scene_text_ref_color_color,color_color_list);
//ds_list_copy(obj_scene.scene_text_ref_font_index,font_index_list);
//ds_list_copy(obj_scene.scene_text_ref_font_font,font_font_list);

//ds_map_add_list(obj_scene.text_ref_map,"color_index",obj_scene.scene_text_ref_color_index);
//ds_map_add_list(obj_scene.text_ref_map,"color_color",obj_scene.scene_text_ref_color_color);
//ds_map_add_list(obj_scene.text_ref_map,"font_index",obj_scene.scene_text_ref_font_index);
//ds_map_add_list(obj_scene.text_ref_map,"font_font",obj_scene.scene_text_ref_font_font);