///_play_bgm(bgm);
/// @description
/// @param bgm
if scene_is_current()
{
	scene_play_bgm(argument0);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;
