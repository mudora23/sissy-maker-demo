///_room_goto_ext(room,fade);
/// @description\
/// @param room
/// @param fade
if scene_is_current()
{
	if obj_transition.next_room != room_scene_deadline
		room_goto_ext(argument0,argument1);
}

scene_map[? SCENE_POSI]++;