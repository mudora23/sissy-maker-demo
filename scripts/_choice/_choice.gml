///_choice(sprite,text,label_goto,room_goto,xalign,yalign,random_order[optional]);
/// @description - choices on the screen at any given time should have the same sprite size and the same align.
/// @param sprite
/// @param text
/// @param label_goto
/// @param room_goto
/// @param xalign
/// @param yalign
/// @param random_order[optional]
if scene_is_current()
{
	var choice_sprite = argument[0];
	var choice_text = argument[1];
	var choice_label = argument[2];
	var choice_room = argument[3];
	var choice_xalign = argument[4];
	var choice_yalign = argument[5];
	var choice_number_showing = 0;
	var choice_order_list = obj_scene.scene_choice_order;

	var choice_ydifference = 10 + sprite_get_height(choice_sprite);
	
	for(var i = 0;i<instance_number(obj_choice_object);i++)
	{
		var inst = instance_find(obj_choice_object,i);
		if !(inst.hiding) choice_number_showing++;
	}
	
	var new_inst = instance_create_depth(0,0,DEPTH_CHOICE,obj_choice_object);
	new_inst.sprite_index = choice_sprite;
	new_inst.choice_text = choice_text;
	new_inst.choice_label = choice_label;
	new_inst.choice_room = choice_room;
	
	if choice_number_showing == 0
	{
		ds_list_clear(choice_order_list);
	}
	ds_list_add(choice_order_list,new_inst);
	
	if argument_count > 6 && argument[6]
		ds_list_shuffle(choice_order_list);
	
	var choice_order_size = ds_list_size(choice_order_list);
	
	/// choice re-ordering
	var border_y_space = 15;
	var border_x_space = 40;
	// x-position
	if choice_xalign == fa_left
	{
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			inst.xshow = border_x_space+sprite_get_width(choice_sprite)/2;
			inst.xhide = -border_x_space-sprite_get_width(choice_sprite)/2;
			inst.x = inst.xhide;
		}
	}
	else if choice_xalign == fa_right
	{
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			inst.xshow = room_width - border_x_space - sprite_get_width(choice_sprite)/2;
			inst.xhide = room_width + border_x_space + sprite_get_width(choice_sprite)/2;
			inst.x = inst.xhide;
		}
	}
	else
	{
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			inst.xshow = room_width/2;
			inst.xhide = room_width + border_x_space + sprite_get_width(choice_sprite)/2;
			inst.x = inst.xhide;

		}
	}
	
	
	draw_reset();
	// y-position
	if choice_yalign == fa_top
	{
		var current_y_position = border_y_space;
		
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			current_y_position += obj_control.choice_padding + obj_control.choice_margin;
			current_y_position += /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2;  
			inst.yshow = current_y_position;
			inst.yhide = current_y_position;
			inst.y = current_y_position;
			current_y_position += /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			current_y_position += obj_control.choice_margin + obj_control.choice_padding;
		}
	}
	else if choice_yalign == fa_bottom
	{
		var current_y_position = room_height - border_y_space;
		
		for(var i = choice_order_size-1;i>=0;i--)
		{
			var inst = choice_order_list[| i];
			current_y_position -= obj_control.choice_padding + obj_control.choice_margin;
			current_y_position -= /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			inst.yshow = current_y_position;
			inst.yhide = current_y_position;
			inst.y = current_y_position;
			current_y_position -= /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			current_y_position -= obj_control.choice_margin + obj_control.choice_padding;
		}
	}
	else
	{
		// spaces
		var space_total = 0;
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			space_total+=obj_control.choice_margin;
			space_total+=obj_control.choice_padding;
			space_total+=/*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			space_total+=/*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			space_total+=obj_control.choice_padding;
			space_total+=obj_control.choice_margin;
		}
		
		var current_y_position = room_height/2 - space_total/2;
		for(var i = 0;i<choice_order_size;i++)
		{
			var inst = choice_order_list[| i];
			current_y_position += obj_control.choice_padding + obj_control.choice_margin;
			current_y_position += /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			inst.yshow = current_y_position;
			inst.yhide = current_y_position;
			inst.y = current_y_position;
			current_y_position += /*max(string_height(inst.choice_text),*/string_height_auto(inst.choice_text,30,inst.sprite_width)/*)*//2; 
			current_y_position += obj_control.choice_margin + obj_control.choice_padding;
		}
	}
	
	for(var i = 0;i<choice_order_size;i++)
	{
		var inst = choice_order_list[| i];
		inst.delay = i*0.2;
	}
	
	scene_jump_next();


}
scene_map[? SCENE_POSI]++;