///sissy_maker_can_save();
/// @description
if  room == room_opening ||
	room == room_title_screen ||
    room == room_questionnaire ||
    instance_exists(obj_stay) ||
    instance_exists(obj_rest) ||
    instance_exists(obj_job) ||
	instance_exists(obj_job_instant) ||
    instance_exists(obj_missed_job) ||
    instance_exists(obj_activities)/* ||
    obj_transition.next_room != -1*/
        return false;
else
    return true;
