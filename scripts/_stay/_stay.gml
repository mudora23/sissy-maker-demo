///_stay(morning_can_stay,afternoon_can_stay,evening_can_stay,late_night_can_stay);
/// @description
/// @param morning_can_stay
/// @param afternoon_can_stay
/// @param evening_can_stay
/// @param late_night_can_stay
/// @param scene_goto
if scene_is_current()
{
	with obj_stay instance_destroy();
	var inst = instance_create_depth(0,0,DEPTH_GUI_STAY_REST,obj_stay);
	inst.morning_can_stay = argument0;
	inst.afternoon_can_stay = argument1;
	inst.evening_can_stay = argument2;
	inst.late_night_can_stay = argument3;
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;