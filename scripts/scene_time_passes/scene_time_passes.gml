///scene_time_passes();
/// @description
scene_map[? SCENE_POSI_STEP]++;
scene_map[? SCENE_TOTAL_STEP]++;
scene_map[? SCENE_POSI_SEC]+=delta_time_sec();
scene_map[? SCENE_TOTAL_SEC]+=delta_time_sec();