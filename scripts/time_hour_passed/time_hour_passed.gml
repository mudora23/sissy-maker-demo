///time_hour_passed(hour[1-24]);
/// @description
/// @param hour[1-24]

var new_time_0_23 = var_get(VAR_TIME);
new_time_0_23+=argument0;
if new_time_0_23 > 23
{
	new_time_0_23 -= 24;
	var_set(VAR_TIME,new_time_0_23);
	var_set(VAR_DAY,var_get(VAR_DAY)+1);

	
	if var_get(VAR_DAYSREMAINING) > 0
		var_set(VAR_DAYSREMAINING,var_get(VAR_DAYSREMAINING)-1);
	else if var_get(VAR_CASH) >= var_get(VAR_DEBT)
	{
		var_set(VAR_DAYSREMAINING,0);
	}
	else
	{
		var_set(VAR_DAYSREMAINING,0);
		room_goto_ext(room_scene_deadline,true);
	}
}
else
{
	var_set(VAR_TIME,new_time_0_23);
}

missed_job_update();
