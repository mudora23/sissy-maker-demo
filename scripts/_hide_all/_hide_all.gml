///_hide_all(fade_speed);
/// @description - hide the given sprite character
/// @param fade_speed
if scene_is_current()
{
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst = instance_find(obj_sprite_object,i);
		inst.hiding = true;	
		inst.fade_speed = argument[0];	
    }
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;