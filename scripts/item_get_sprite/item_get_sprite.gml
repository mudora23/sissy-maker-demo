///item_get_sprite(name);
/// @description
/// @param name
with obj_control
{
	var item_map = item_constants_map[? argument0];
	var sprite_string = item_map[? ITEM_SPRITE];
	if sprite_exists(asset_get_index(sprite_string)) return asset_get_index(sprite_string);
	else return noone;
}
