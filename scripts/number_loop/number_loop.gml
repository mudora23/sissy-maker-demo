///number_loop(number,min,max);
/// @description - number
/// @param min
/// @param max

if argument1 > argument2
    return argument0;
else if argument0 < argument1
    return argument2 - abs(argument0 - argument1) + 1;
else if argument0 > argument2
    return argument1 + abs(argument0 - argument2) - 1;
else
    return argument0;
