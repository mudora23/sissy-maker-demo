///draw_stats_longtext(x,y,string);
/// @description
/// @param x
/// @param y
/// @param string
var maxwidth = 95;
if string_width(argument2) >= maxwidth
	draw_text_transformed(argument0,argument1,argument2,maxwidth/string_width(argument2),1,0);
else
	draw_text_transformed(argument0,argument1,argument2,1,1,0);
//draw_text_outlined(argument0,argument1,argument2,c_white,c_white,1,1,50);