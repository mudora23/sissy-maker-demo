///outfit_set_next();
var outfit_current = var_get(VAR_OUTFIT);
var outfit_total = var_get(VAR_OUTFIT_TOTAL);
if outfit_current >= outfit_total
    var_set(VAR_OUTFIT,1);
else
    var_set(VAR_OUTFIT,outfit_current+1);
