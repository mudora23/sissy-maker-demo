///missed_job_update();
/// @description
if !instance_exists(obj_job)
{
	for(var i = 0;i < ds_list_size(obj_control.jobs_list_time_end_day);i++)
	{
		var the_day = obj_control.jobs_list_time_end_day[| i];
		var the_time = obj_control.jobs_list_time_end_0_23[| i];
		var the_name = obj_control.jobs_list_name[| i];

		if the_day < var_get(VAR_DAY) ||
			(the_day == var_get(VAR_DAY) && the_time < obj_control.jobs_list_time_end_0_23[| i])
			{
				var inst = instance_create_depth(0,0,DEPTH_GUI_JOB,obj_missed_job);
				inst.job_name = the_name;
				jobs_list_delete_job(the_name);
				break;
				
			}


	}
}