///inventory_item_exists(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_map = inventory_map[? argument[0]];
	return item_map[? INVENTORY_ITEM_AMOUNT] != 0;

}
