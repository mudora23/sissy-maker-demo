///floating_text_create(var, adding?, vaule);
/// @description
/// @param var
/// @param adding?
/// @param vaule
if obj_floating_text.floating_text_active
{
    var _igvar = argument0;
    var _adding = argument1;
    var _val = argument2;
    var _string = "";
    if is_real(_val)
    {
        if _val >= 0 var _val_the_sign = "+";
        else var _val_the_sign = "-";
    }
    else
        var _val_the_sign = "";
    if is_real(var_get(_igvar))
    {
        if var_get(_igvar) >= 0 var _igvar_the_sign = "";
        else var _igvar_the_sign = "-";
    }
    else
        var _igvar_the_sign = "";  
    
    /*if is_string(_igvar)
    {
        if _igvar == ITEM_Lotion
            var _string = string(_val) + " dose(s) remaining";
    }*/
    if _igvar == VAR_DAY var _string = "Day "+string(var_get(VAR_DAY));
    else if _igvar == VAR_CASH
    {
        if _adding var _string = "Cash: "+_val_the_sign+"$"+string(abs(_val));
        else var _string = "Cash: "+_igvar_the_sign+"$"+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_HP_MAX
    {
        if _adding var _string = "HP Max: "+_val_the_sign+string(abs(_val));
        else var _string = "HP Max: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_HP
    {
        if _adding var _string = "HP: "+_val_the_sign+string(abs(_val));
        else var _string = "HP: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_ENERGY_MAX
    {
        if _adding var _string = "Energy Max: "+_val_the_sign+string(abs(_val));
        else var _string = "Energy Max: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_ENERGY
    {
        if _adding var _string = "Energy: "+_val_the_sign+string(abs(_val));
        else var _string = "Energy: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_DEBT
    {
        if _adding var _string = "Debt: "+_val_the_sign+"$"+string(abs(_val));
        else var _string = "Debt: "+_igvar_the_sign+"$"+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_DAYSREMAINING
    {
        if _adding var _string = "Days remaining: "+_val_the_sign+string(abs(_val));
        else var _string = "Days remaining: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_STRENGTH
    {
        if _adding var _string = "Strength: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Strength: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_STAMINA
    {
        if _adding var _string = "Stamina: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Stamina: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_DEXTERITY
    {
        if _adding var _string = "Dexterity: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Dexterity: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_BARGAIN
    {
        if _adding var _string = "Bargain: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Bargain: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_INTELLIGENCE
    {
        if _adding var _string = "Intelligence: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Intelligence: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_CHARISMA
    {
        if _adding var _string = "Charisma: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Charisma: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_SEX_APPEAL
    {
        if _adding var _string ="Sex appeal: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Sex appeal: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_FEMALE_RATIO
    {
        if _adding var _string = "Female ratio: "+_val_the_sign+string(abs(_val));
        else var _string = "Female ratio: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_BODY_TYPE
    {
        var _string ="Body Type: "+var_get(_igvar);
    }
    else if _igvar == VAR_CUP_SIZE
    {
        if var_get(VAR_CUP_SIZE) <= 0
            var cup_size_text = "-";
        else if var_get(VAR_CUP_SIZE) <= 1
            var cup_size_text = "A";
        else if var_get(VAR_CUP_SIZE) <= 2
            var cup_size_text = "B";
        else if var_get(VAR_CUP_SIZE) <= 3
            var cup_size_text = "C";
        else if var_get(VAR_CUP_SIZE) <= 4
            var cup_size_text = "D";
        else if var_get(VAR_CUP_SIZE) <= 5
            var cup_size_text = "E";
        else if var_get(VAR_CUP_SIZE) <= 6
            var cup_size_text = "F";
        else if var_get(VAR_CUP_SIZE) <= 7
            var cup_size_text = "G";
        else if var_get(VAR_CUP_SIZE) <= 8
            var cup_size_text = "H";
        else if var_get(VAR_CUP_SIZE) <= 9
            var cup_size_text = "I";
        else
            var cup_size_text = "J";
        var _string = "Cup size: "+cup_size_text;
    }
    else if _igvar == VAR_COCK
    {
        if _adding var _string = "Cock size: "+_val_the_sign+string(abs(_val))+" inch(es)";
        else var _string = "Cock size: "+_igvar_the_sign+string(abs(var_get(_igvar)))+" inch(es)";
    }
    else if _igvar == VAR_ANUS
    {
        if var_get(VAR_ANUS) >= 4
            var anus_text = "Trained";
        else if var_get(VAR_ANUS) >= 3
            var anus_text = "Puffy";
        else if var_get(VAR_ANUS) >= 2
            var anus_text = "Normal";
        else if var_get(VAR_ANUS) >= 1
            var anus_text = "Tight";
        else if var_get(VAR_ANUS) >= 0
            var anus_text = "Loose";
        else if var_get(VAR_ANUS) >= -1
            var anus_text = "Sore";
        else if var_get(VAR_ANUS) >= -2
            var anus_text = "Wrecked";
        else
            var anus_text = "Destroyed";
        var _string = "Anus: "+anus_text;
    }
    else if _igvar == VAR_AFF_CHRIS
    {
        if _adding var _string = "Chris's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Chris's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_SIMONE
    {
        if _adding var _string = "Simone's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Simone's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_RONDA
    {
        if _adding var _string = "Ronda's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Ronda's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_SOPHIA
    {
        if _adding var _string = "Sophia's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Sophia's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_PENNY
    {
        if _adding var _string = "Penny's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Penny's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_DAYNA
    {
        if _adding var _string = "Dayna's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Dayna's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_SECURITY
    {
        if _adding var _string = "Security's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Security's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_RAYE
    {
        if _adding var _string = "Raye's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Raye's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_JOHNNY
    {
        if _adding var _string = "Johnny's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Johnny's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_AFF_MIREILLE
    {
        if _adding var _string = "Mireille's Affinity: "+_val_the_sign+string(abs(_val));
        else var _string = "Mireille's Affinity: "+_igvar_the_sign+string(abs(var_get(_igvar)));
    }
    else if _igvar == VAR_SKILL_BABYSITTING
    {
        if _adding var _string = "Babysitting skill: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Babysitting skill: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_SKILL_JAZZ
    {
        if _adding var _string = "Jazz skill: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Jazz skill: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == VAR_SKILL_SALSA
    {
        if _adding var _string = "Salsa skill: "+_val_the_sign+string(abs(_val))+"XP";
        else var _string = "Salsa skill: "+_igvar_the_sign+string(abs(var_get(_igvar)))+"XP";
    }
    else if _igvar == ITEM_Lotion
    {
        if _adding var _string = ITEM_Lotion+": "+_val_the_sign+string(abs(_val))+" Dose(s)";
        else
		{
			var _string = ITEM_Lotion+": "+_igvar_the_sign+string(inventory_get_item_amount(ITEM_Lotion))+" Dose(s) remaining";
		}
	}
    else if _igvar == ITEM_So_Zore_Lotion
    {
        if _adding var _string = ITEM_So_Zore_Lotion+": "+_val_the_sign+string(abs(_val))+" Dose(s)";
        else
		{
			var _string = ITEM_So_Zore_Lotion+": "+_igvar_the_sign+string(inventory_get_item_amount(ITEM_So_Zore_Lotion))+" Dose(s) remaining";
		}
	}
	
	
    show_debug_message("setting floating text...var is: "+string(_igvar));
    if _string != ""
    {

        ds_list_add(obj_floating_text.floating_text_string,_string);
        ds_list_add(obj_floating_text.floating_text_x ,room_width/2);
        ds_list_add(obj_floating_text.floating_text_y,room_height/2);
        ds_list_add(obj_floating_text.floating_text_increasing,true);
        ds_list_add(obj_floating_text.floating_text_alpha,0);
        ds_list_add(obj_floating_text.floating_text_life,1.5);
        if _adding && _val_the_sign == "+" && _val != 0
            ds_list_add(obj_floating_text.floating_text_sign,"+");
        else if _adding && _val_the_sign == "-"
            ds_list_add(obj_floating_text.floating_text_sign,"-"); 
        else if !_adding && _igvar_the_sign == "+"
            ds_list_add(obj_floating_text.floating_text_sign,"+");
        else if !_adding && _igvar_the_sign == "-"
            ds_list_add(obj_floating_text.floating_text_sign,"-");  
        else
            ds_list_add(obj_floating_text.floating_text_sign,"");  
    }


}










