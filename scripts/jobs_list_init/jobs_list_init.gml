///jobs_list_init();
/// @description
obj_control.jobs_map = ds_map_create();

obj_control.jobs_list_name = ds_list_create();
obj_control.jobs_list_time_begin_day = ds_list_create();
obj_control.jobs_list_time_end_day = ds_list_create();
obj_control.jobs_list_time_begin_0_23 = ds_list_create();
obj_control.jobs_list_time_end_0_23 = ds_list_create();
obj_control.jobs_list_place = ds_list_create();
obj_control.jobs_list_des = ds_list_create();

ds_map_add_list(obj_control.jobs_map, "name", obj_control.jobs_list_name);
ds_map_add_list(obj_control.jobs_map, "time_begin_day", obj_control.jobs_list_time_begin_day);
ds_map_add_list(obj_control.jobs_map, "time_end_day", obj_control.jobs_list_time_end_day);
ds_map_add_list(obj_control.jobs_map, "time_begin_0_23", obj_control.jobs_list_time_begin_0_23);
ds_map_add_list(obj_control.jobs_map, "time_end_0_23", obj_control.jobs_list_time_end_0_23);
ds_map_add_list(obj_control.jobs_map, "place", obj_control.jobs_list_place);
ds_map_add_list(obj_control.jobs_map, "des", obj_control.jobs_list_des);
