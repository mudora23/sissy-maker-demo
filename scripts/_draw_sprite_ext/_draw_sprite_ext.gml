///_draw_sprite_ext(sprite,subimg,x,y,xscale,yscale,rot,color,alpha,totalposi[>=1]);
/// @description
/// @param sprite
/// @param subimg
/// @param x
/// @param y
/// @param xscale
/// @param yscale
/// @param rot
/// @param color
/// @param alpha
/// @param totalposi - the total amount of total starting the next scene posi

if scene_map[? SCENE_POSI_CURRENT] >= scene_map[? SCENE_POSI]+1 && scene_map[? SCENE_POSI_CURRENT] <= scene_map[? SCENE_POSI]+argument9
{
	draw_sprite_ext(argument0,argument1,argument2,argument3,argument4,argument5,argument6,argument7,argument8);
}
if scene_is_current()
	scene_jump_next();
scene_map[? SCENE_POSI]++;