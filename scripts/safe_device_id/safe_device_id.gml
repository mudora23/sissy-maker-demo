///safe_device_id()

/** 
    This function needs to return a unique device ID to stop players from 
    transfering save files across devices. The code shown below is just an
    example to poorly identify a device uniquely. I reccomend however using
    a free asset on the marketplace, DeviceID, to identify the device.
    Set dID below equal to the devices ID.
	
	INI MODE To fully encrypt and make your INI files tamper-proof, all you have to do is prefix all "ini_*" functions in your project with "safe_", and change ini_open(filename) to safe_ini_open(filename, key) where key is a random string of letters that is used to encrypt your INI file. This is all that is required.

	ADVANCED MODE If you need more flexibility when you are saving data, sissy_maker_save_map will take a ds_map and a filename and write it to the disk fully encrypted with built in tamper-proofing. It will fully save and reconstitute all embedded data structures as well.
*/

var dID = string(game_id);/*strjoin("_", os_browser, os_type, os_device, os_get_language(),
    os_get_region(), os_version, display_get_width(), game_id);*/
    
return dID;
