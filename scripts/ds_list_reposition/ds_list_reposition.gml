///ds_list_reposition(id,old_index,insert_index);
/// @description
/// @param id
/// @param old_index
/// @param insert_index
var the_list = argument[0];
var old_index = clamp(argument[1],0,ds_list_size(the_list)-1);
var insert_index = clamp(argument[2],0,ds_list_size(the_list)-1);

ds_list_insert(the_list,insert_index,the_list[| old_index]);
if insert_index <= old_index old_index++;
ds_list_delete(the_list,old_index);
