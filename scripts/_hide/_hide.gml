///_hide(name,fade_speed);
/// @description - hide the given sprite character
/// @param name
/// @param fade_speed
if scene_is_current()
{
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst = instance_find(obj_sprite_object,i);
        if inst.name == argument[0]
        {
			inst.hiding = true;	
			inst.fade_speed = argument[1];	
        }
    }
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;