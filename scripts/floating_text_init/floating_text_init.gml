///floating_text_init();
/// @description
obj_floating_text.floating_text_string = ds_list_create();
obj_floating_text.floating_text_x = ds_list_create();
obj_floating_text.floating_text_y = ds_list_create();
obj_floating_text.floating_text_increasing = ds_list_create();
obj_floating_text.floating_text_alpha = ds_list_create();
obj_floating_text.floating_text_life = ds_list_create();
obj_floating_text.floating_text_sign = ds_list_create();
obj_floating_text.floating_text_active = true;
obj_floating_text.depth = DEPTH_FLOATING_TEXT;