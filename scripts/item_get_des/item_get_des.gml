///item_get_des(name);
/// @description
/// @param name
with obj_control
{
	var item_map = item_constants_map[? argument0];
	return item_map[? ITEM_DES];
}