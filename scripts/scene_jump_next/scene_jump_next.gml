///scene_jump_next(clear_dialog[optional]);
/// @description
/// @param clear_dialog[optional]
if argument_count > 0
	scene_jump(scene_map[? SCENE_POSI_CURRENT]+1,argument[0]);
else
	scene_jump(scene_map[? SCENE_POSI_CURRENT]+1);