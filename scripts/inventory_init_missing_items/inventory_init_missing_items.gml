///inventory_init_missing_items();
/// @description
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var inventory_size = inventory_get_size();
	
	// inventory - adding missing items
	var item_name = ds_map_find_first(item_constants_map);
	while(!is_undefined(item_name))
	{
		if !ds_map_exists(inventory_map,item_name)
		{
			ds_map_add_map(inventory_map,item_name,ds_map_create());
		}
	
		item_name = ds_map_find_next(item_constants_map,item_name);
	}
	
	
	// inventory - adding missing item keys
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		if !ds_map_exists(item_map,INVENTORY_ITEM_AMOUNT) ds_map_add(item_map,INVENTORY_ITEM_AMOUNT,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_USED_LAST_DAY) ds_map_add(item_map,INVENTORY_ITEM_USED_LAST_DAY,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_USED_LAST_DAY_TIMES) ds_map_add(item_map,INVENTORY_ITEM_USED_LAST_DAY_TIMES,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_USED_TOTAL_TIMES) ds_map_add(item_map,INVENTORY_ITEM_USED_TOTAL_TIMES,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_USED_DAYS_IN_A_ROW) ds_map_add(item_map,INVENTORY_ITEM_USED_DAYS_IN_A_ROW,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_IN_EFFECT_DURATION_HOUR) ds_map_add(item_map,INVENTORY_ITEM_IN_EFFECT_DURATION_HOUR,0);
		if !ds_map_exists(item_map,INVENTORY_ITEM_ORDER_INDEX) ds_map_add(item_map,INVENTORY_ITEM_ORDER_INDEX,inventory_size++);

		item_name = ds_map_find_next(inventory_map,item_name);
	}
	
}