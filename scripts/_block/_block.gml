///_block();
/// @description

if scene_is_current()
{
	// drawing the text if not clear already
	var char_x = 50;
	var char_y = 462;
        
	draw_reset();
	for (var i = 1; i <= string_width(scene_map[? SCENE_TEXT]);i++)
	{
		dialog_set_ref(i);
		draw_text_transformed(char_x, char_y, string_char_at(scene_map[? SCENE_TEXT], i), 1, 1, 0);
		if char_x > 650 && string_char_at(scene_map[? SCENE_TEXT], i) == " "
		{
		    char_x = 50;
		    char_y += 30;
		}
		else
		{
		    char_x += string_width(string_char_at(scene_map[? SCENE_TEXT], i));
		}
        
	} 
}		
scene_map[? SCENE_POSI]++;