///draw_tooltip(x,y,string);
/// @description
/// @param x
/// @param y
/// @param string
draw_set_font(font_general_13);

var xx = argument0;
var yy = argument1;
var text = argument2;
var padding = 10;
var xx2 = xx+string_width_auto(string_hash_to_newline(text),20,350)+padding*2;
var yy2 = yy+string_height_auto(string_hash_to_newline(text),20,350)+padding*2;
if xx2 > 800
{
    var xoffset = xx2 - 780;
    xx -= xoffset;
    xx2 -= xoffset;
}
if yy2 > 600
{
    var yoffset = yy2 - 580;
    yy -= yoffset;
    yy2 -= yoffset;
}

// BG
draw_set_alpha(0.85);
draw_set_color(c_black);
draw_rectangle(xx,yy,xx2,yy2,false);
draw_set_alpha(0.85);
draw_set_color(c_white);
draw_rectangle(xx,yy,xx2,yy2,true);

// Text
draw_set_alpha(1);
draw_set_color(c_white);
draw_text_auto(xx+padding,yy+padding,string_hash_to_newline(text),20,350);
//draw_text(xx+padding,yy+padding,string_hash_to_newline(text));
//draw_text_ext_transformed(xx+padding,yy+padding,string_hash_to_newline(text),20,300,1,1,1);
// reset
draw_reset();
