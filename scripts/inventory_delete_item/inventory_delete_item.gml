///inventory_delete_item(name,amount[optional]);
/// @description
/// @param name
/// @param amount[optional]
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];

	if argument_count > 1
		item_map[? INVENTORY_ITEM_AMOUNT] = max(0,item_map[? INVENTORY_ITEM_AMOUNT]-argument[1]);
	else
		item_map[? INVENTORY_ITEM_AMOUNT] = max(0,item_map[? INVENTORY_ITEM_AMOUNT]-1);
	
	if item_map[? INVENTORY_ITEM_AMOUNT] == 0
		inventory_reposition_item_to_last(item_name);
		
	floating_text_create(item_name, false, item_map[? INVENTORY_ITEM_AMOUNT]);

}
