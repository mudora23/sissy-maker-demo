///_change(name,target_sprite,fade_speed);  
/// @description - edit the given sprite character
/// @param name
/// @param target_sprite
/// @param fade_speed

if scene_is_current()
{
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst = instance_find(obj_sprite_object,i);
        if inst.name == argument[0]
        {
			inst.target_sprite_index = argument[1];	
			inst.target_sprite_alpha_change_rate = argument[2];	
        }
    }
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;