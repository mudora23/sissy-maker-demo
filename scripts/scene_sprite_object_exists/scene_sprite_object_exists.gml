///scene_sprite_object_exists(name);
/// @description
/// @param name
var inst = noone;
for (var i = 0; i < instance_number(obj_sprite_object); i++)
{
    var inst = instance_find(obj_sprite_object,i);
	if inst.name == argument[0]
		return true;
}
return false;