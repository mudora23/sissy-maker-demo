/// thumbnail_setup();
/// @description
if sprite_exists(obj_control.pausescreen)
    sprite_delete(obj_control.pausescreen);
obj_control.pausescreen = sprite_create_from_surface(application_surface, 0, 0, surface_get_width(application_surface), surface_get_height(application_surface), false, false, 0, 0);
obj_control.surf1 = surface_create(obj_control.thumbnails_width,obj_control.thumbnails_height);
surface_set_target(obj_control.surf1);
draw_sprite_ext(obj_control.pausescreen,0,0,0,obj_control.thumbnails_width/800,obj_control.thumbnails_height/600,0,c_white,1);
surface_reset_target();
if sprite_exists(obj_control.pausescreen)
    sprite_delete(obj_control.pausescreen);
obj_control.pausescreen = sprite_create_from_surface(obj_control.surf1, 0, 0, surface_get_width(obj_control.surf1), surface_get_height(obj_control.surf1), false, false, 0, 0);
surface_free(obj_control.surf1);
