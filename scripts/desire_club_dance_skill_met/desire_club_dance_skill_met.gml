///desire_club_dance_skill_met();
/// @description
return skills_xp_get_level(var_get(VAR_SKILL_JAZZ)) >= 2 || skills_xp_get_level(var_get(VAR_SKILL_SALSA)) >= 2;
