///_stay_rest_hide(scene_goto);
/// @description

if scene_is_current()
{
	with obj_stay {hiding = true; scene_goto = argument0;}
	with obj_rest {hiding = true; scene_goto = argument0;}
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;