///update_job_info();    
/// @description

// update
var des_details = "";
for(i = 0;i<ds_list_size(obj_control.jobs_list_name);i++)
{
    des_details += "You have assigned for "+obj_control.jobs_list_name[| i]+"\r";
    if obj_control.jobs_list_name[| i] == JOBNAME_Baby_Sitting
        des_details += "Location: "+obj_control.jobs_list_place[| i]+"\r"
    else
        des_details += "Location:\r"
    if var_get(VAR_DAY) == obj_control.jobs_list_time_begin_day[| i]
        var day_info = "Today";
    else if var_get(VAR_DAY)+1 == obj_control.jobs_list_time_begin_day[| i]
        var day_info = "Tomorrow";
    else if var_get(VAR_DAY) > obj_control.jobs_list_time_begin_day[| i]
        var day_info = string(var_get(VAR_DAY) - obj_control.jobs_list_time_begin_day[| i])+" day(s) before";
    else if var_get(VAR_DAY) < obj_control.jobs_list_time_begin_day[| i]
        var day_info = string(obj_control.jobs_list_time_begin_day[| i] - var_get(VAR_DAY))+" day(s) after";
    des_details += "Time: "+day_info+" "+time_0_23_to_time_stamp(obj_control.jobs_list_time_begin_0_23[| i])+" ~ "+time_0_23_to_time_stamp(obj_control.jobs_list_time_end_0_23[| i]);
    des_details += "\r\r";
}

var item_map = obj_control.item_constants_map[? ITEM_Job_Assignments];
if ds_list_size(obj_control.jobs_list_name) == 0
{
    des_details += "You haven't assigned for any jobs at the moment.";
    item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_no_job);
}
else
{
    item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_job);
}

item_map[? ITEM_DES] = des_details;

