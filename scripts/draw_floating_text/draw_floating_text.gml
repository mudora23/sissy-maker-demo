///draw_floating_text(string);

ds_list_add(obj_floating_text.floating_text_string,argument0);
ds_list_add(obj_floating_text.floating_text_x ,room_width/2);
ds_list_add(obj_floating_text.floating_text_y,room_height/2);
ds_list_add(obj_floating_text.floating_text_increasing,true);
ds_list_add(obj_floating_text.floating_text_alpha,0);
ds_list_add(obj_floating_text.floating_text_life,1.5);
ds_list_add(obj_floating_text.floating_text_sign,"");  
