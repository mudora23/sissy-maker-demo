///sissy_maker_hotfixes();
/// @description

// hotfix - visiting hospital causes the time to be an odd number instead of even. (fix for v1.00)
if var_get(VAR_TIME) % 2 == 1
{
	var_add(VAR_TIME,-1);
}

// hotfix - We no long needed the job assignment item. (fix for v1.00)
if inventory_item_exists(ITEM_Job_Assignments)
	inventory_delete_item(ITEM_Job_Assignments);

// hotfix - We no long needed the job assignment item. (fix for v1.41, in v1.50)
if var_get(VAR_JOB_BABYSITTING) > 0
{
	if !ds_map_exists(obj_control.var_map,VAR_AFF_KANE) || obj_control.var_map[? VAR_AFF_KANE] < 1
		obj_control.var_map[? VAR_AFF_KANE] = 1;
}

