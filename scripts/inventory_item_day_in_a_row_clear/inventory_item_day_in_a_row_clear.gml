///inventory_item_day_in_a_row_clear(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];
	
	item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW] = 0;

}

