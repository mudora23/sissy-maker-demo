///attributes_xp_get_level(xp);
/// @description -
/// @param xp
/*
Level: 1 = 0 XP+
Level: 2 = 10 XP+
Level: 3 = 20 XP+
Level: 4 = 45 XP+
Level: 5 = 70 XP+
*/
if argument0 >= 70
    return 5;
else if argument0 >= 45
    return 4;
else if argument0 >= 20
    return 3;
else if argument0 >= 10
    return 2;
else
   return 1; 