///success_roll(var);
/// @description
/// @param var
var success = 0;
repeat(var_get(argument0))
    success+=percent_chance(50);
return success;
