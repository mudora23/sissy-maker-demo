///jobs_list_have_job(begin_day,end_day,begin_0_23,end_0_23,name[optional]);
/// @description - return true if there's any job assigned during this period of time
/// @param begin_day
/// @param end_day
/// @param begin_0_23
/// @param end_0_23
/// @param name[optional]

var begin_day = argument[0];
var end_day = argument[1];
var begin_0_23 = argument[2];
var end_0_23 = argument[3];

for (var day = begin_day; day <= end_day; day++)
{
	if day == begin_day var begin_time = begin_0_23;
	else var begin_time = 0;
	if day == end_day var end_time = end_0_23;
	else var end_time = 23;
	
	for (var time = begin_time; time <= end_time; time++)
	{
		for (var job_index = 0; job_index < ds_list_size(obj_control.jobs_list_time_begin_day); job_index++)
		{
			var job_begin_day = obj_control.jobs_list_time_begin_day[| job_index];
			var job_end_day = obj_control.jobs_list_time_end_day[| job_index];
			var job_begin_0_23 = obj_control.jobs_list_time_begin_0_23[| job_index];
			var job_end_0_23 = obj_control.jobs_list_time_end_0_23[| job_index];
			var job_name = obj_control.jobs_list_name[| job_index];
			
			for (var job_day = job_begin_day; job_day <= job_end_day; job_day++)
			{
				if job_day == job_begin_day var job_begin_time = job_begin_0_23;
				else var job_begin_time = 0;
				if job_day == job_end_day var job_end_time = job_end_0_23;
				else var job_end_time = 23;
	
				for (var job_time = job_begin_time; job_time <= job_end_time; job_time++)
				{
					if job_day == day && job_time == time && (argument_count<=4 || argument[4]==job_name)
						return true;

				}
			}
		}
	}
}

return false;
