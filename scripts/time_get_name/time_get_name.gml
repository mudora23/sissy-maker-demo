///time_get_name(time[0-23]);
/// @description -
/// @param time[0-23]

var the_time = argument0;
if the_time >= 24 the_time-= 24;

if the_time >= 0 && the_time < 6 return "Late Night";
else if the_time >= 6 && the_time < 12 return "Morning";
else if the_time >= 12 && the_time < 18 return "Afternoon";
else return "Evening";
