///scene_is_current(step[optional]);
/// @description
/// @param step[optional]
return scene_map[? SCENE_POSI_CURRENT] == scene_map[? SCENE_POSI] && (argument_count == 0 || scene_map[? SCENE_POSI_STEP] == argument[0]);
