///_activities(sprite,scene_goto);
/// @description
/// @param sprite
/// @param scene_goto
if scene_is_current()
{
	with obj_activities instance_destroy();
	var inst = instance_create_depth(0,0,DEPTH_GUI_ACTIVITIES,obj_activities);
	inst.activities_sprite = argument0;
	inst.scene_goto = argument1;
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;