///draw_main_gui_text(x,y,string);
/// @description
/// @param x
/// @param y
/// @param string
var old_alpha = draw_get_alpha();
draw_set_alpha(image_alpha);
//draw_text_outlined(argument0,argument1,argument2,c_white,c_dkgray,1,2,50);
draw_text(argument0,argument1,argument2);
draw_set_alpha(old_alpha);