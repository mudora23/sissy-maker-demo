///draw_text_auto(x,y,text,sep,w);
/// @description
/// @param x
/// @param y
/// @param text
/// @param sep
/// @param w
var xx = argument[0];
var yy = argument[1];
var text = argument[2];
var sep = argument[3];
var w = argument[4];


if string_width(text) > w
	return draw_text_ext(xx,yy,text,sep,w);
else
	return draw_text(xx,yy,text);