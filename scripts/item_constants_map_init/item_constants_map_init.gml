///item_constants_map_init();
with obj_control
{
	item_constants_map = ds_map_create();
	
	var item_name = ITEM_Job_Assignments;
	ds_map_add_map(item_constants_map,item_name,ds_map_create());
	var item_map = item_constants_map[? item_name];
	item_map[? ITEM_DES] = "";
	item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_job);
	item_map[? ITEM_USED_PER_DAY_TIMES_MAX] = 0;
	
	var item_name = ITEM_Gum;
	ds_map_add_map(item_constants_map,item_name,ds_map_create());
	var item_map = item_constants_map[? item_name];
	item_map[? ITEM_DES] = "Chewing gum, keeps your breath fresh.";
	item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_gum);
	item_map[? ITEM_USED_PER_DAY_TIMES_MAX] = 0;
	
	var item_name = ITEM_Invitation_Letter;
	ds_map_add_map(item_constants_map,item_name,ds_map_create());
	var item_map = item_constants_map[? item_name];
	item_map[? ITEM_DES] = "This is Penny Hardth’s invitation to STP.";
	item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_invitation);
	item_map[? ITEM_USED_PER_DAY_TIMES_MAX] = 0;

	var item_name = ITEM_Lotion;
	ds_map_add_map(item_constants_map,item_name,ds_map_create());
	var item_map = item_constants_map[? item_name];
	item_map[? ITEM_DES] = "Apply the content of a sachet on your chest once a day, for 5 days in a row. Do not skip a day, or it’s going to ruin the medizin cycle.";
	item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_lotion);
	item_map[? ITEM_USED_PER_DAY_TIMES_MAX] = 1;

	var item_name = ITEM_So_Zore_Lotion;
	ds_map_add_map(item_constants_map,item_name,ds_map_create());
	var item_map = item_constants_map[? item_name];
	item_map[? ITEM_DES] = "Use this lotion if you get zore arsch. Use it once a day for three days, and you’ll be ready for the next wrack… Well you besser not over do it.";
	item_map[? ITEM_SPRITE] = sprite_get_name(spr_item_sozore_lotion);
	item_map[? ITEM_USED_PER_DAY_TIMES_MAX] = 1;
}