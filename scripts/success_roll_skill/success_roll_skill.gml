///success_roll_attribute(var);
/// @description
/// @param var
var success = 0;
repeat(skills_xp_get_level(var_get(argument0)))
    success+=percent_chance(50);
return success;
