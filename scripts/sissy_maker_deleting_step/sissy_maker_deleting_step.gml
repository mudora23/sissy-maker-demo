///sissy_maker_deleting_step();
var sliding_speed = 6;
if deleting_num == noone
{
    deleting_yes_button_x = smooth_approach(deleting_yes_button_x,deleting_yes_button_xstart,sliding_speed*delta_time_sec());
    deleting_yes_button_y = smooth_approach(deleting_yes_button_y,deleting_yes_button_ystart,sliding_speed*delta_time_sec());
    deleting_no_button_x = smooth_approach(deleting_no_button_x,deleting_no_button_xstart,sliding_speed*delta_time_sec());
    deleting_no_button_y = smooth_approach(deleting_no_button_y,deleting_no_button_ystart,sliding_speed*delta_time_sec());
    deleting_yes_button_alpha = approach(deleting_yes_button_alpha,0,sliding_speed*delta_time_sec());
    deleting_no_button_alpha = approach(deleting_no_button_alpha,0,sliding_speed*delta_time_sec());
}
else
{
    deleting_yes_button_x = smooth_approach(deleting_yes_button_x,deleting_yes_button_xtarget,sliding_speed*delta_time_sec());
    deleting_yes_button_y = smooth_approach(deleting_yes_button_y,deleting_yes_button_ytarget,sliding_speed*delta_time_sec());
    deleting_no_button_x = smooth_approach(deleting_no_button_x,deleting_no_button_xtarget,sliding_speed*delta_time_sec());
    deleting_no_button_y = smooth_approach(deleting_no_button_y,deleting_no_button_ytarget,sliding_speed*delta_time_sec());
    deleting_yes_button_alpha = approach(deleting_yes_button_alpha,1,sliding_speed*delta_time_sec());
    deleting_no_button_alpha = approach(deleting_no_button_alpha,1,sliding_speed*delta_time_sec());
}



    var spr = spr_save_delete_yes_no;
    var hover = mouse_over_area(deleting_yes_button_x,deleting_yes_button_y,sprite_get_width(spr),sprite_get_height(spr));
    if obj_control.left_pressed && hover && deleting_num != noone
    {
        sissy_maker_delete(deleting_num);
        thumbnail_update(deleting_num);
        deleting_num = noone;
        obj_control.left_pressed = false;
        scene_play_sfx(sfx_button_01);
    }
    draw_sprite_ext(spr,hover,deleting_yes_button_x,deleting_yes_button_y,1,1,0,c_white,deleting_yes_button_alpha);

    var hover = mouse_over_area(deleting_no_button_x,deleting_no_button_y,sprite_get_width(spr),sprite_get_height(spr));
    if obj_control.left_pressed && hover && deleting_num != noone
    {
        deleting_num = noone;
        obj_control.left_pressed = false;
        scene_play_sfx(sfx_button_01);
    }
    if obj_control.left_pressed || (obj_main_gui.menu_open != "save" && obj_main_gui.menu_open != "load") deleting_num = noone;
        
    draw_sprite_ext(spr,2+hover,deleting_no_button_x,deleting_no_button_y,1,1,0,c_white,deleting_no_button_alpha);








