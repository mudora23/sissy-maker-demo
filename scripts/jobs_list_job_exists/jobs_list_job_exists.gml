///jobs_list_job_exists(name);
/// @description - return true if the specific job is in the list
/// @param name
for (var i = 0; i < ds_list_size(obj_control.jobs_list_name);i++)
    if obj_control.jobs_list_name[| i] == argument[0] 
        return true;
return false;
