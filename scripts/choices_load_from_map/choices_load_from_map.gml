///choices_load_from_map(map);
/// @description
/// @param map
with obj_choice_object instance_destroy();

var x_list = argument0[? "x"];
var y_list = argument0[? "y"];
var xhide_list = argument0[? "xhide"];
var yhide_list = argument0[? "yhide"];
var xshow_list = argument0[? "xshow"];
var yshow_list = argument0[? "yshow"];
var hiding_list = argument0[? "hiding"];
var xalign_list = argument0[? "xalign"];
var yalign_list = argument0[? "yalign"];
var image_alpha_list = argument0[? "image_alpha"];
var sprite_index_list = argument0[? "sprite_index"];
var depth_list = argument0[? "depth"];
var delay_list = argument0[? "delay"];
var clicked_list = argument0[? "clicked"];
var choice_text_list = argument0[? "choice_text"];
var choice_label_list = argument0[? "choice_label"];
var choice_room_list = argument0[? "choice_room"];

var list_size = ds_list_size(depth_list);

for(var i = 0; i < list_size;i++)
{
	var inst = instance_create_depth(0,0,DEPTH_CHOICE,obj_choice_object);
	inst.x = x_list[| i];
	inst.y = y_list[| i];
	inst.xhide = xhide_list[| i];
	inst.yhide = yhide_list[| i];
	inst.xshow = xshow_list[| i];
	inst.yshow = yshow_list[| i];
	inst.hiding = hiding_list[| i];
	inst.xalign = xalign_list[| i];
	inst.yalign = yalign_list[| i];
	inst.image_alpha = image_alpha_list[| i];
	if sprite_index_list[| i] == "" inst.sprite_index = noone;
	else inst.sprite_index = asset_get_index(sprite_index_list[| i]);
	inst.depth = depth_list[| i];
	inst.delay = delay_list[| i];
	inst.clicked = clicked_list[| i];
	inst.choice_text = choice_text_list[| i];
	inst.choice_label = choice_label_list[| i];
	if choice_room_list[| i] == "" inst.choice_room = noone;
	else inst.choice_room = asset_get_index(choice_room_list[| i]);

}
