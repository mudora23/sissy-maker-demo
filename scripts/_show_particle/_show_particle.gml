///_show_particle(x,y,depth,obj);
/// @description
/// @param x
/// @param y
/// @param depth
/// @param obj

if scene_is_current()
{
	instance_create_depth(argument0,argument1,argument2,argument3);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;