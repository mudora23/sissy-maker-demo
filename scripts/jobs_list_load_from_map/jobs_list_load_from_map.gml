///jobs_list_load_from_map(map);
/// @description
/// @param map
obj_control.jobs_list_name = argument0[? "name"];
obj_control.jobs_list_time_begin_day = argument0[? "time_begin_day"];
obj_control.jobs_list_time_end_day = argument0[? "time_end_day"];
obj_control.jobs_list_time_begin_0_23 = argument0[? "time_begin_0_23"];
obj_control.jobs_list_time_end_0_23 = argument0[? "time_end_0_23"];
obj_control.jobs_list_place = argument0[? "place"];
obj_control.jobs_list_des = argument0[? "des"];
