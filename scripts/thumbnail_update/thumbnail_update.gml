/// thumbnail_update(num[optoinal]);
/// @description
/// @param num[optoinal]
if argument_count == 0 || argument[0] == 0
{
    if sprite_exists(obj_control.save0_thumbnail)
        sprite_delete(obj_control.save0_thumbnail);
    obj_control.save0_thumbnail = noone;
    obj_control.save0_date = "";
    if file_exists("save0.png")
    {
        obj_control.save0_thumbnail = sprite_add("save0.png",1,false,false,0,0);
        obj_control.save0_date = INI_SecLoadString("0","thumbnaildate");
    }
}
if argument_count == 0 || argument[0] == 1
{
    if sprite_exists(obj_control.save1_thumbnail)
        sprite_delete(obj_control.save1_thumbnail);
    obj_control.save1_thumbnail = noone;
    obj_control.save1_date = "";
    if file_exists("save1.png")
    {
        obj_control.save1_thumbnail = sprite_add("save1.png",1,false,false,0,0);
        obj_control.save1_date = INI_SecLoadString("1","thumbnaildate");
    }
}
if argument_count == 0 || argument[0] == 2
{
    if sprite_exists(obj_control.save2_thumbnail)
        sprite_delete(obj_control.save2_thumbnail);
    obj_control.save2_thumbnail = noone;
    obj_control.save2_date = "";
    if file_exists("save2.png")
    {
        obj_control.save2_thumbnail = sprite_add("save2.png",1,false,false,0,0);
        obj_control.save2_date = INI_SecLoadString("2","thumbnaildate");
    }
}
if argument_count == 0 || argument[0] == 3
{
    if sprite_exists(obj_control.save3_thumbnail)
        sprite_delete(obj_control.save3_thumbnail);
    obj_control.save3_thumbnail = noone;
    obj_control.save3_date = "";
    if file_exists("save3.png")
    {
        obj_control.save3_thumbnail = sprite_add("save3.png",1,false,false,0,0);
        obj_control.save3_date = INI_SecLoadString("3","thumbnaildate");
    }
}
if argument_count == 0 || argument[0] == 4
{
    if sprite_exists(obj_control.save4_thumbnail)
        sprite_delete(obj_control.save4_thumbnail);
    obj_control.save4_thumbnail = noone;
    obj_control.save4_date = "";
    if file_exists("save4.png")
    {
        obj_control.save4_thumbnail = sprite_add("save4.png",1,false,false,0,0);
        obj_control.save4_date = INI_SecLoadString("4","thumbnaildate");
    }
}

