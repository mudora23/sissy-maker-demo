///scene_jump(label/posi,clear_dialog[optional]);
/// @description
/// @param label/posi
/// @param clear_dialog[optional]

obj_scene.scene_map[? SCENE_POSI_STEP] = 0;
obj_scene.scene_map[? SCENE_POSI_SEC] = 0;
if is_string(argument[0]) obj_scene.scene_map[? SCENE_POSI_CURRENT] = obj_scene.scene_label_map[? argument[0]];
else obj_scene.scene_map[? SCENE_POSI_CURRENT] = argument[0];
if argument_count == 1 || argument[1]
{
	obj_scene.scene_map[? SCENE_TEXT] = "";
	obj_scene.scene_map[? SCENE_CHAR_POSI] = 0;

	ds_list_clear(obj_scene.scene_text_ref_color_index);
	ds_list_clear(obj_scene.scene_text_ref_color_color);
	ds_list_clear(obj_scene.scene_text_ref_font_index);
	ds_list_clear(obj_scene.scene_text_ref_font_font);
}