///ynum_to_y(sprite,size_ratio,ynum);
/// @description - 9x9 position to real y position
/// @param sprite
/// @param size_ratio
/// @param ynum
//Calculate sprite width and offset
var sprite_body_height = sprite_get_height(argument0)*argument1;

var yreal_top = 0;
var yreal_bot = 600-sprite_body_height;
var yreal_jump = (yreal_bot - yreal_top) / 8; // 1 = most top; 9 = most bot; 8 jumps in total from top to bot
var ynum_jumps = argument2 - 1;
        
if (ynum_jumps < 0)
    return yreal_top + sprite_body_height * ynum_jumps;
else if (ynum_jumps > 8)
    return yreal_bot + sprite_body_height * (ynum_jumps - 8);
else
    return yreal_top + yreal_jump*ynum_jumps;