///_border(how_many_borders[optional]);
/// @description - not a block
/// @param how_many_borders[optional]

if argument_count > 0 var how_many = argument[0];
else var how_many = 1;
var target_posi = floor(scene_map[? SCENE_POSI] / 10000)*10000+10000*how_many;

if scene_is_current()
{
	scene_jump(target_posi);
}

scene_map[? SCENE_POSI] = target_posi;