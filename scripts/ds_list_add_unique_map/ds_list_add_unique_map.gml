///ds_list_add_unique_map(id,value);
/// @description - add the value to the list if its not in the list
/// @param id
/// @param value
if !ds_list_value_exists(argument[0],argument[1])
{
	ds_list_add(argument[0],argument[1]);
	ds_list_mark_as_map(argument[0],ds_list_size(argument[0])-1);
}