///success_roll_attribute(var);
/// @description
/// @param var
var success = 0;
repeat(attributes_xp_get_level(var_get(argument0)))
    success+=percent_chance(30);
return success;
