///_move(name,new_xnum,new_ynum,slide_speed[spd<10=ratio;spd>10=pixel/sec]);  
/// @description -
/// @param name
/// @param new_xnum
/// @param new_ynum
/// @param slide_speed[spd<10=ratio;spd>10=pixel/sec]

if scene_is_current()
{
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst = instance_find(obj_sprite_object,i);
        if inst.name == argument[0]
        {
			inst.xtarget = xnum_to_x(inst.sprite_index,inst.size_ratio,argument[1]);
			inst.ytarget = ynum_to_y(inst.sprite_index,inst.size_ratio,argument[2]);
			inst.slide_speed = argument[3];
        }
    }
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;