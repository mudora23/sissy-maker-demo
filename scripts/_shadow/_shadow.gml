///_shadow(sprite,shadow_label,shadow_room,fade_speed,depth);
/// @description - create a shadow object
/// @param sprite
/// @param shadow_label
/// @param shadow_room
/// @param fade_speed
/// @param depth

if scene_is_current()
{
    var inst = instance_create_depth(0,0,argument[4],obj_shadow_object);
	inst.sprite_index = argument[0];
	inst.shadow_label = argument[1];
	inst.shadow_room = argument[2];
	inst.fade_speed = argument[3];
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;
