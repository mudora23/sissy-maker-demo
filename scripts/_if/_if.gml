///_if(condition,_action_if_true,argument0,argument1,argument2,argument3,argument4,argument5,argument6,argument7,argument8,argument9,argument10,argument11,argument11,argument12,argument13);
/// @description
/// @param condition
/// @param _action_if_true
/// @param argument0
/// @param argument1
/// @param argument2...
if argument[0] && argument_count >= 2
{
	if argument_count == 2
		script_execute(argument[1]);
	else if argument_count == 3
		script_execute(argument[1],argument[2]);
	else if argument_count == 4
		script_execute(argument[1],argument[2],argument[3]);
	else if argument_count == 5
		script_execute(argument[1],argument[2],argument[3],argument[4]);
	else if argument_count == 6
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5]);
	else if argument_count == 7
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
	else if argument_count == 8
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7]);
	else if argument_count == 9
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
	else if argument_count == 10
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9]);
	else if argument_count == 11
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10]);
	else if argument_count == 12
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
	else if argument_count == 13
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
	else if argument_count == 14
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13]);
	else if argument_count == 15
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14]);
	else if argument_count == 16
		script_execute(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14],argument[15]);
	else
		_void();
}
else
{
	_void();
}

