///var_add(var,amount);
/// @description -
/// @param var
/// @param amount
obj_control.var_map[? argument0] = var_get(argument0)+argument1;
floating_text_create(argument0,true,argument1);


if string_copy(argument0,1,9) == "VAR_EVENT"
{
	//var event_map = sissy_maker_load_map("event.dat",obj_control.secretString);
	if ds_map_exists(obj_control.event_map,argument0)
	{
		obj_control.event_map[? argument0] = max(obj_control.event_map[? argument0],obj_control.var_map[? argument0]);
	}
	else
	{
		obj_control.event_map[? argument0] = obj_control.var_map[? argument0];
	}
	sissy_maker_save_map("event.dat",obj_control.event_map,obj_control.secretString);
	//ds_map_destroy(event_map);
}