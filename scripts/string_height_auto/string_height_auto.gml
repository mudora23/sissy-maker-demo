///string_height_auto(text,sep,w);
/// @description
/// @param text
/// @param sep
/// @param w
var text = argument[0];
var sep = argument[1];
var w = argument[2];


if string_width(text) > w
	return string_height_ext(text,sep,w);
else
	return string_height(text);