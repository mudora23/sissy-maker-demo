/// thumbnail_init();
/// @description

/// thumbnail & file variables
obj_control.pausescreen = noone;
obj_control.surf1 = noone;
obj_control.thumbnails_width = 80;
obj_control.thumbnails_height = 60;

obj_control.save0_thumbnail = noone;
obj_control.save0_date = "";
obj_control.save1_thumbnail = noone;
obj_control.save1_date = "";
obj_control.save2_thumbnail = noone;
obj_control.save2_date = "";
obj_control.save3_thumbnail = noone;
obj_control.save3_date = "";
obj_control.save4_thumbnail = noone;
obj_control.save4_date = "";