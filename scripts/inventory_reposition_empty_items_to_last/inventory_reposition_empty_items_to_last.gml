///inventory_reposition_empty_items_to_last();
/// @description
/// @param name
with obj_control
{
	var inventory_available_size = inventory_get_size_available();
	var inventory_first_empty_item_index = inventory_find_first_empty_item_index();
	while (inventory_first_empty_item_index != noone && inventory_first_empty_item_index < inventory_available_size)
	{
		inventory_reposition_item_to_last(inventory_get_item_name(inventory_first_empty_item_index));
		inventory_first_empty_item_index = inventory_find_first_empty_item_index();
	}

}

