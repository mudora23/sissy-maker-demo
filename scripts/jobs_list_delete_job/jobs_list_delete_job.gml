///jobs_list_delete_job(name);
/// @description - delete the job
/// @param name
for(var i = 0;i< ds_list_size(obj_control.jobs_list_name);i++)
{
    if obj_control.jobs_list_name[| i] == argument0
    {
        ds_list_delete(obj_control.jobs_list_name,i);
        ds_list_delete(obj_control.jobs_list_time_begin_day,i);
        ds_list_delete(obj_control.jobs_list_time_end_day,i);
        ds_list_delete(obj_control.jobs_list_time_begin_0_23,i);
        ds_list_delete(obj_control.jobs_list_time_end_0_23,i);
		ds_list_delete(obj_control.jobs_list_place,i);
		ds_list_delete(obj_control.jobs_list_des,i);
        i--;
    } 
}
