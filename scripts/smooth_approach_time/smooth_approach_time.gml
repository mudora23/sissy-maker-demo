///smooth_approach_time(current, target, current_duration, duration);
/// @description -
/// @param current
/// @param target
/// @param current_duration[sec]
/// @param duration[sec]
if argument3 == 0 return argument1;
else return smooth_approach(argument0, argument1, clamp(argument2 / argument3,0,1));
