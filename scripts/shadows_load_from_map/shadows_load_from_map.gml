///shadows_load_from_map(map);
/// @description
/// @param map
with obj_shadow_object instance_destroy();


var sprite_index_list = argument0[? "sprite_index"];
var image_alpha_list = argument0[? "image_alpha"];
var image_alpha_min_list = argument0[? "image_alpha_min"];
var image_alpha_max_list = argument0[? "image_alpha_max"];
var image_alpha_increasing_list = argument0[? "image_alpha_increasing"];
var fade_speed_list = argument0[? "fade_speed"];
var shadow_label_list = argument0[? "shadow_label"];
var shadow_room_list = argument0[? "shadow_room"];
var depth_list = argument0[? "depth"];

var list_size = ds_list_size(depth_list);

for(var i = 0; i < list_size;i++)
{
	var inst = instance_create_depth(0,0,DEPTH_BG_ELE_SHADOW,obj_shadow_object);
	if sprite_index_list[| i] == "" inst.sprite_index = noone;
	else inst.sprite_index = asset_get_index(sprite_index_list[| i]);
	inst.image_alpha = image_alpha_list[| i];
	inst.image_alpha_min = image_alpha_min_list[| i];
	inst.image_alpha_max = image_alpha_max_list[| i];
	inst.image_alpha_increasing = image_alpha_increasing_list[| i];
	inst.fade_speed = fade_speed_list[| i];
	inst.shadow_label = shadow_label_list[| i];
	if shadow_room_list[| i] == "" inst.shadow_room = noone;
	else inst.shadow_room = asset_get_index(shadow_room_list[| i]);
	inst.depth = depth_list[| i];

}
