///inventory_get_size();
/// @description
return ds_map_size(obj_control.var_map[? VAR_INVENTORY_MAP]);