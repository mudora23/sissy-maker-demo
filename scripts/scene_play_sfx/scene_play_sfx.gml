///play_sfx(sfx);
/// @description
/// @param sfx
var sound_id = audio_play_sound(argument0,1,false);
audio_sound_gain(sound_id, obj_control.control_map[? CONTROL_SFX_LEVEL], 0);