///sissy_maker_delete(num);
/// @description
/// @param slot
save_name = "save"+string(argument0);
if file_exists(save_name+".png")
    file_delete(save_name+".png");
if file_exists(save_name+"stats.dat")
    file_delete(save_name+"stats.dat");
if file_exists(save_name+"inventory.dat")
    file_delete(save_name+"inventory.dat");
if file_exists(save_name+"item_day.dat")
    file_delete(save_name+"item_day.dat");
if file_exists(save_name+"scene.dat")
    file_delete(save_name+"scene.dat");
if file_exists(save_name+"ref.dat")
    file_delete(save_name+"ref.dat");
if file_exists(save_name+"jobs.dat")
    file_delete(save_name+"jobs.dat");
if file_exists(save_name+"choice.dat")
    file_delete(save_name+"choice.dat");
if file_exists(save_name+"sprite.dat")
    file_delete(save_name+"sprite.dat");
if file_exists(save_name+"shadow.dat")
    file_delete(save_name+"shadow.dat");