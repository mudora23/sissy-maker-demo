///success_roll_hard(var);
/// @description
/// @param var
var success = 0;
repeat(var_get(argument0))
    success+=percent_chance(30);
return success;
