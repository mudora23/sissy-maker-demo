///_play_sfx(sfx);
/// @description
/// @param sfx
if scene_is_current()
{
    scene_play_sfx(argument0);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;
