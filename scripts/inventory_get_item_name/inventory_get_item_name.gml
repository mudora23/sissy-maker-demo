///inventory_get_item_name(position_index);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		
		if item_map[? INVENTORY_ITEM_ORDER_INDEX] == argument[0]
		{
			return item_name;
		}
	
		item_name = ds_map_find_next(inventory_map,item_name);
	}

}

