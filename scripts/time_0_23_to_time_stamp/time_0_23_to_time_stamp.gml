///time_0_23_to_time_stamp(real);
/// @description
/// @param real

var number = round(real(argument0));
while number < 0 number += 24;
while number > 23 number -= 24;
if number < 10 return "0"+string(number)+":00";
else return string(number)+":00";
