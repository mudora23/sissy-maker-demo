///inventory_get_item_amount(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];

	return item_map[? INVENTORY_ITEM_AMOUNT];

}