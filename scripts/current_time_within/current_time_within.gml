///current_time_within(start_time,end_time);
/// @description -
/// @param start_time
/// @param end_time
if argument[0] <= argument[1]
	return var_get(VAR_TIME) >= argument[0] && var_get(VAR_TIME) <= argument[1];
else
	return (var_get(VAR_TIME) >= argument[0] && var_get(VAR_TIME) >= argument[1]) ||
	       (var_get(VAR_TIME) <= argument[0] && var_get(VAR_TIME) <= argument[1]);