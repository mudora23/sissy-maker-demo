///INI_SecSaveReal(KEY ,Value, INI_NAME);
/// @description
/// @param KEY
/// @param Value
/// @param INI_NAME
dataLine=argument0;
dataS=string(argument1);
randomize();
var secdata = "";
for (var i = 0; i < 9; i++) {
    var randnumb= randomSeedChar();
    secdata =string(randnumb+secdata);
    }
secdata = string(secdata+dataS);
var forsave = base64_encode(string(secdata));
 ini_open( string(argument2)+".ini" );
 ini_write_string( "data", string(dataLine), string(forsave));
 ini_close();
 


