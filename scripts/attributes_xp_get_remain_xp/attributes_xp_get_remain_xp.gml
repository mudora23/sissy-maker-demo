///attributes_xp_get_remain_xp(xp);
/// @description -
/// @param xp
/*
Level: 1 = 0 XP+
Level: 2 = 10 XP+
Level: 3 = 20 XP+
Level: 4 = 45 XP+
Level: 5 = 70 XP+
*/
if argument0 >= 70
    return argument0 - 70;
else if argument0 >= 45
    return argument0 - 45;
else if argument0 >= 20
    return argument0 - 20;
else if argument0 >= 10
    return argument0 - 10;
else
   return argument0; 