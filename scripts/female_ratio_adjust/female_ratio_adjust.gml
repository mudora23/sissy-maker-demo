///female_ratio_adjust();
/// @description

if attributes_xp_get_level(var_get(VAR_STRENGTH)) >= 5
{
	if var_get(VAR_BODY_TYPE) != "Inverted Triangle"
		var_set(VAR_BODY_TYPE, "Inverted Triangle");
    return -0.2;
    
}
else if attributes_xp_get_level(var_get(VAR_STRENGTH)) >= 4
{
	if var_get(VAR_BODY_TYPE) != "Apple Shape"
		var_set(VAR_BODY_TYPE, "Apple Shape");
    return -0.1;
}
/*else if var_get(VAR_BODY_TYPE) == "Pear Shape"
    return 0.1;
else if var_get(VAR_BODY_TYPE) == "Hour Glass"
    return 0.2;*/
else
    return 0;