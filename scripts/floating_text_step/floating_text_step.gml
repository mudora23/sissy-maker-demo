///floating_text_step();
/// @description
for(var i = 0; i < ds_list_size(obj_floating_text.floating_text_string);i++)
{
    if i == 0 || obj_floating_text.floating_text_y[| i-1] <= room_height/2 - 40
    {
        // alpha
        if obj_floating_text.floating_text_increasing[| i]
        {
	        if obj_floating_text.floating_text_alpha[| i] == 0
	        {
	            if obj_floating_text.floating_text_sign[| i] == "-"
	            {
	                scene_play_sfx(sfx_lost_points);
	                repeat(15)
	                {
	                    if random(1) >= 0.5
	                        instance_create_depth(random_range(obj_floating_text.floating_text_x[| i]-string_width(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_x[| i]+string_width(obj_floating_text.floating_text_string[| i])/2),random_range(obj_floating_text.floating_text_y[| i]-string_height(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_y[| i]+string_height(obj_floating_text.floating_text_string[| i])/2),DEPTH_FLOATING_TEXT_EFFECT,obj_particle_smoke);
	                    else
	                        instance_create_depth(random_range(obj_floating_text.floating_text_x[| i]-string_width(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_x[| i]+string_width(obj_floating_text.floating_text_string[| i])/2),random_range(obj_floating_text.floating_text_y[| i]-string_height(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_y[| i]+string_height(obj_floating_text.floating_text_string[| i])/2),DEPTH_FLOATING_TEXT_EFFECT,obj_particle_cloud);
	                }
	            }
	            else if obj_floating_text.floating_text_sign[| i] == "+"
	            {
	                scene_play_sfx(sfx_get_points);
	                repeat(30)
	                {
	                    instance_create_depth(random_range(obj_floating_text.floating_text_x[| i]-string_width(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_x[| i]+string_width(obj_floating_text.floating_text_string[| i])/2),random_range(obj_floating_text.floating_text_y[| i]-string_height(obj_floating_text.floating_text_string[| i])/2,obj_floating_text.floating_text_y[| i]+string_height(obj_floating_text.floating_text_string[| i])/2),DEPTH_FLOATING_TEXT_EFFECT,obj_particle_shinny_up);
	                }
	            }
	        }
            obj_floating_text.floating_text_alpha[| i] = clamp(obj_floating_text.floating_text_alpha[| i] + 3*delta_time_sec(), 0, 0.8);
            if obj_floating_text.floating_text_alpha[| i] == 0.8 obj_floating_text.floating_text_increasing[| i] = false;
        }
        else if obj_floating_text.floating_text_life[| i] > 0
            obj_floating_text.floating_text_life[| i]-= delta_time_sec();
        else
            obj_floating_text.floating_text_alpha[| i] = clamp(obj_floating_text.floating_text_alpha[| i] - 1.2*delta_time_sec(), 0, 0.8);
            
        // position
        obj_floating_text.floating_text_y[| i] -= 60*delta_time_sec();


    }
    
    // deleting
    if obj_floating_text.floating_text_alpha[| i] <= 0 && !obj_floating_text.floating_text_increasing[| i]
    {
        ds_list_delete(obj_floating_text.floating_text_string,i);
        ds_list_delete(obj_floating_text.floating_text_x,i);
        ds_list_delete(obj_floating_text.floating_text_y,i);
        ds_list_delete(obj_floating_text.floating_text_increasing,i);
        ds_list_delete(obj_floating_text.floating_text_alpha,i);
        ds_list_delete(obj_floating_text.floating_text_life,i);
        ds_list_delete(obj_floating_text.floating_text_sign,i);
        i--;
    }
    else
    {
        draw_set_font(font_general_20);
        draw_set_valign(fa_center);
        draw_set_halign(fa_center);
        draw_set_alpha(obj_floating_text.floating_text_alpha[| i]); 

        if obj_floating_text.floating_text_sign[| i] == "+"
            var the_color = make_color_rgb(58, 130, 59);
        else if obj_floating_text.floating_text_sign[| i] == "-"
            var the_color = make_color_rgb(109, 55, 48);
        else
            var the_color = c_black;
        draw_text_outlined(obj_floating_text.floating_text_x[| i],obj_floating_text.floating_text_y[| i],obj_floating_text.floating_text_string[| i],c_white,the_color,1,3,8);
        draw_reset();
    }
}

