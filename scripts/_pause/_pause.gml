///_pause(hardsec,softsec,auto);
/// @description
/// @param hardsec
/// @param softsec
/// @param auto
if scene_is_current()
{
	// clicking
	if (obj_control.action_button_pressed || obj_control.action_button_holding_time >= 0.8) && scene_map[? SCENE_POSI_SEC] >= argument0 && !menu_exists() && !debug_menu_exists()
		scene_jump_next();
	// non-clicking
	else if scene_map[? SCENE_POSI_SEC] >= argument0 + argument1 && argument2
		scene_jump_next();
}

scene_map[? SCENE_POSI]++;