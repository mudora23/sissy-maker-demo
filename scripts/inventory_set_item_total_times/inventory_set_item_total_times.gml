///inventory_set_item_total_times(name,times);
/// @description
/// @param name
/// @param times
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];

	item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] = argument[1];

}