///scene_stop_sfx(sfx);
/// @description
/// @param sfx
if audio_is_playing(argument0)
	audio_stop_sound(argument0);
