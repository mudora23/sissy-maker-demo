///_job_instant(name);
/// @description
/// @param name

if scene_is_current()
{
	with obj_job instance_destroy();
    with obj_job_instant instance_destroy();
	var inst = instance_create_depth(0,0,DEPTH_GUI_JOB,obj_job_instant);
	inst.job_name = argument[0];
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;