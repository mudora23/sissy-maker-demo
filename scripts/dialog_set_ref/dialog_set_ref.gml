///dialog_set_ref(char);
/// @description - set the ref we previously recorded.
/// @param charposi

for(var i = 0; i< ds_list_size(obj_scene.scene_text_ref_color_index);i++)
{
    if obj_scene.scene_text_ref_color_index[| i] == argument0
        draw_set_color(obj_scene.scene_text_ref_color_color[| i]);
}
for(var j = 0; j< ds_list_size(obj_scene.scene_text_ref_font_index);j++)
{
    if obj_scene.scene_text_ref_font_index[| j] == argument0
        draw_set_font(obj_scene.scene_text_ref_font_font[| j]);
}
