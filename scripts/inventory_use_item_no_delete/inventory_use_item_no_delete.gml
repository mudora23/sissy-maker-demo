///inventory_use_item_no_delete(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];
	
	
	// Used today
	if var_get(VAR_DAY) == item_map[? INVENTORY_ITEM_USED_LAST_DAY]
	{
		item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES]+=1;
		item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW] = max(item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW],1);
	}
	
	// Used yesterday
	else if var_get(VAR_DAY) == item_map[? INVENTORY_ITEM_USED_LAST_DAY] + 1
	{
		item_map[? INVENTORY_ITEM_USED_LAST_DAY]=var_get(VAR_DAY);
		item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES]=1;
		item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW]+=1;
	}
	
	// Else
	else
	{
		item_map[? INVENTORY_ITEM_USED_LAST_DAY]=var_get(VAR_DAY);
		item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES]=1;
		item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW]=1;
	}
	
	item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES]+=1;
	
	
	
	
///////////// Item effects /////////////
	
	/* 
	Lotion 
	Requirement: 5 days in a row
	Effect: cup size +1, up to 3
	*/
	if item_name == ITEM_Lotion
	{
		// floating text
		if inventory_item_day_x_in_a_row(ITEM_Lotion) == 1
			draw_floating_text(ITEM_Lotion+" has been used for 1 day");
		else if inventory_item_day_x_in_a_row(ITEM_Lotion) > 1
			draw_floating_text(ITEM_Lotion+" has been used for "+string(inventory_item_day_x_in_a_row(ITEM_Lotion))+" days in a row");
											
		// effect?
		if inventory_item_day_x_in_a_row(ITEM_Lotion) >= 5
		{
			var_add_ext(VAR_CUP_SIZE,1,0,3);						
			inventory_item_day_in_a_row_clear(ITEM_Lotion);	
			item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] = 0;
			
		}
										
	}
	
	/* 
	So_Zore_Lotion 
	Effect: anus +1, only works when anus is either -1,0,1.
	*/
	
	if item_name == ITEM_So_Zore_Lotion
	{
		// floating text
		/*if item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] == 1
			draw_floating_text(ITEM_So_Zore_Lotion+" has been used for 1 day");
		else if item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] > 1
			draw_floating_text(ITEM_So_Zore_Lotion+" has been used for "+string(item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES])+" days");
		*/	
										
		// effect?
		if item_map[? INVENTORY_ITEM_USED_LAST_DAY_TIMES] == 1 && (var_get(VAR_ANUS) == -1 || var_get(VAR_ANUS) == 0 || var_get(VAR_ANUS) == 1)/*item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] >= 3*/
			var_set(VAR_ANUS,var_get(VAR_ANUS)+1);
		else
			var_set(VAR_ANUS,var_get(VAR_ANUS));
		item_map[? INVENTORY_ITEM_USED_TOTAL_TIMES] = 0;
			
										
	}
	
	
	
}


