///_show(name,sprite,xnum,ynum,size_ratio,depth,flip,fade_speed);  
/// @description - create given sprite character
/// @param name
/// @param sprite
/// @param xnum
/// @param ynum
/// @param size_ratio
/// @param depth
/// @param flip
/// @param fade_speed

if scene_is_current()
{
	/*// finding if the sprite object exists; if not, create one.
    var inst = noone;
    
    for (var i = 0; i < instance_number(obj_sprite_object); i++)
    {
        var inst_created = instance_find(obj_sprite_object,i);
        if inst_created.name == argument[0] && !inst_created.hiding
        {
            inst = inst_created;
            break;
        }
    }
    if inst == noone
	{*/
    var inst = instance_create_depth(0,0,argument[5],obj_sprite_object);
	
	// setup
	inst.name = argument[0];
	inst.sprite_index = argument[1];
	inst.xtarget = xnum_to_x(argument[1],argument[4],argument[2]);
	inst.ytarget = ynum_to_y(argument[1],argument[4],argument[3]);
	inst.x = inst.xtarget;
	inst.y = inst.ytarget;
	inst.size_ratio = argument[4];
	inst.flip = argument[6];
	inst.fade_speed = argument[7];
	//}
	
	scene_jump_next();
}

scene_map[? SCENE_POSI]++;
