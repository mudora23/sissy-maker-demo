///_var_add(var,amount);
/// @description
/// @param var
/// @param amount
if scene_is_current()
{
	var_add(argument0,argument1);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;