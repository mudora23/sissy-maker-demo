///var_init_missing_keys();
/// @description -

with obj_control
{
	
	if !ds_map_exists(var_map,VAR_CURRENT_ROOM) var_map[? VAR_CURRENT_ROOM] = room_get_name(room);
	if !ds_map_exists(var_map,VAR_CURRENT_MUSIC) var_map[? VAR_CURRENT_MUSIC] = "";
	if !ds_map_exists(var_map,VAR_MAIN_GUI_HIDING) var_map[? VAR_MAIN_GUI_HIDING] = true;
	if !ds_map_exists(var_map,VAR_MAIN_GUI_PLACENAME) var_map[? VAR_MAIN_GUI_PLACENAME] = "";
	
	if !ds_map_exists(var_map,VAR_DAY) var_map[? VAR_DAY] = 0;
	if !ds_map_exists(var_map,VAR_CASH) var_map[? VAR_CASH] = 1000;
	if !ds_map_exists(var_map,VAR_HP_MAX) var_map[? VAR_HP_MAX] = 100;
	if !ds_map_exists(var_map,VAR_HP) var_map[? VAR_HP] = 100;
	if !ds_map_exists(var_map,VAR_ENERGY_MAX) var_map[? VAR_ENERGY_MAX] = 100;
	if !ds_map_exists(var_map,VAR_ENERGY) var_map[? VAR_ENERGY] = 100;
	if !ds_map_exists(var_map,VAR_DEBT) var_map[? VAR_DEBT] = 30000;
	if !ds_map_exists(var_map,VAR_DAYSREMAINING) var_map[? VAR_DAYSREMAINING] = 90;
	if !ds_map_exists(var_map,VAR_TIME) var_map[? VAR_TIME] = 12;
	
	if !ds_map_exists(var_map,VAR_STRENGTH) var_map[? VAR_STRENGTH] = 0;
	if !ds_map_exists(var_map,VAR_STAMINA) var_map[? VAR_STAMINA] = 0;
	if !ds_map_exists(var_map,VAR_DEXTERITY) var_map[? VAR_DEXTERITY] = 0;
	if !ds_map_exists(var_map,VAR_BARGAIN) var_map[? VAR_BARGAIN] = 0;
	if !ds_map_exists(var_map,VAR_INTELLIGENCE) var_map[? VAR_INTELLIGENCE] = 0;
	if !ds_map_exists(var_map,VAR_CHARISMA) var_map[? VAR_CHARISMA] = 0;
	if !ds_map_exists(var_map,VAR_SEX_APPEAL) var_map[? VAR_SEX_APPEAL] = 0;
	
	if !ds_map_exists(var_map,VAR_SKILL_BABYSITTING) var_map[? VAR_SKILL_BABYSITTING] = 0;
	if !ds_map_exists(var_map,VAR_SKILL_SALSA) var_map[? VAR_SKILL_SALSA] = 0;
	if !ds_map_exists(var_map,VAR_SKILL_JAZZ) var_map[? VAR_SKILL_JAZZ] = 0;
	
	if !ds_map_exists(var_map,VAR_FEMALE_RATIO) var_map[? VAR_FEMALE_RATIO] = 0.5;
	if !ds_map_exists(var_map,VAR_BODY_TYPE) var_map[? VAR_BODY_TYPE] = "Banana Shape";
	if !ds_map_exists(var_map,VAR_CUP_SIZE) var_map[? VAR_CUP_SIZE] = 0;
	if !ds_map_exists(var_map,VAR_COCK) var_map[? VAR_COCK] = 4;
	if !ds_map_exists(var_map,VAR_COCK_MAX) var_map[? VAR_COCK_MAX] = 12;
	if !ds_map_exists(var_map,VAR_COCK_MIN) var_map[? VAR_COCK_MIN] = 2;
	if !ds_map_exists(var_map,VAR_ANUS) var_map[? VAR_ANUS] = 1;
	if !ds_map_exists(var_map,VAR_ANUS_MAX) var_map[? VAR_ANUS_MAX] = 4;
	if !ds_map_exists(var_map,VAR_ANUS_MIN) var_map[? VAR_ANUS_MIN] = -3;
	
	if !ds_map_exists(var_map,VAR_AFF_CHRIS) var_map[? VAR_AFF_CHRIS] = 1;
	if !ds_map_exists(var_map,VAR_AFF_SIMONE) var_map[? VAR_AFF_SIMONE] = 0;
	if !ds_map_exists(var_map,VAR_AFF_RONDA) var_map[? VAR_AFF_RONDA] = 0;
	if !ds_map_exists(var_map,VAR_AFF_SOPHIA) var_map[? VAR_AFF_SOPHIA] = 0;
	if !ds_map_exists(var_map,VAR_AFF_PENNY) var_map[? VAR_AFF_PENNY] = 0;
	if !ds_map_exists(var_map,VAR_AFF_DAYNA) var_map[? VAR_AFF_DAYNA] = 0;
	if !ds_map_exists(var_map,VAR_AFF_SECURITY) var_map[? VAR_AFF_SECURITY] = 0;
	if !ds_map_exists(var_map,VAR_AFF_RAYE) var_map[? VAR_AFF_RAYE] = 0;
	if !ds_map_exists(var_map,VAR_AFF_KANE) var_map[? VAR_AFF_KANE] = 0;
	if !ds_map_exists(var_map,VAR_AFF_JOHNNY) var_map[? VAR_AFF_JOHNNY] = 0;
	if !ds_map_exists(var_map,VAR_AFF_MIREILLE) var_map[? VAR_AFF_MIREILLE] = 0;
	
	if !ds_map_exists(var_map,VAR_AFF_CHRIS_PENALTY) var_map[? VAR_AFF_CHRIS_PENALTY] = 0;
	
	
	if !ds_map_exists(var_map,VAR_OUTFIT_TOTAL) var_map[? VAR_OUTFIT_TOTAL] = 3;
	if !ds_map_exists(var_map,VAR_OUTFIT) var_map[? VAR_OUTFIT] = 1;
	
	// event
	if !ds_map_exists(var_map,VAR_EVENT_HOSPITAL_VISIT) var_map[? VAR_EVENT_HOSPITAL_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_APT_VISIT) var_map[? VAR_EVENT_APT_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_LOTUS_DEW_VISIT) var_map[? VAR_EVENT_LOTUS_DEW_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_HQ_VISIT) var_map[? VAR_EVENT_STP_HQ_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_MAINHALL_VISIT) var_map[? VAR_EVENT_STP_MAINHALL_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_LIBRARY_VISIT) var_map[? VAR_EVENT_STP_LIBRARY_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_GYM_VISIT) var_map[? VAR_EVENT_STP_GYM_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_LAB_VISIT) var_map[? VAR_EVENT_STP_LAB_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STP_PRINCIPAL_VISIT) var_map[? VAR_EVENT_STP_PRINCIPAL_VISIT] = 0;
	
	if !ds_map_exists(var_map,VAR_EVENT_FIRST_MISSION) var_map[? VAR_EVENT_FIRST_MISSION] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_RONDA_RUTHLESS) var_map[? VAR_EVENT_RONDA_RUTHLESS] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_SIMONE_SAPIENZA) var_map[? VAR_EVENT_SIMONE_SAPIENZA] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01) var_map[? VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_FLU) var_map[? VAR_EVENT_FLU] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_BANKRUPT) var_map[? VAR_EVENT_BANKRUPT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_DEADLINE) var_map[? VAR_EVENT_DEADLINE] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_DANCER) var_map[? VAR_EVENT_DANCER] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_DANCER_LOTUS_RESTROOM) var_map[? VAR_EVENT_DANCER_LOTUS_RESTROOM] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_DEMO_END) var_map[? VAR_EVENT_DEMO_END] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_SORE_ASS) var_map[? VAR_EVENT_SORE_ASS] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_ASS_LOTION) var_map[? VAR_EVENT_ASS_LOTION] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_DANCER_AT_STP) var_map[? VAR_EVENT_DANCER_AT_STP] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_RAYE_SANCHES_INTRO) var_map[? VAR_EVENT_RAYE_SANCHES_INTRO] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_RAYE_SANCHES_FUCKED) var_map[? VAR_EVENT_RAYE_SANCHES_FUCKED] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER) var_map[? VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_KANES_CANDY_CANE) var_map[? VAR_EVENT_KANES_CANDY_CANE] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_KANES_CANDY_CANE2) var_map[? VAR_EVENT_KANES_CANDY_CANE2] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_RUNAWAY) var_map[? VAR_EVENT_RUNAWAY] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STRIP_CLUB_PT1) var_map[? VAR_EVENT_STRIP_CLUB_PT1] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STRIP_CLUB_PT2) var_map[? VAR_EVENT_STRIP_CLUB_PT2] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STRIP_CLUB_PT3) var_map[? VAR_EVENT_STRIP_CLUB_PT3] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STRIP_CLUB_FIRST_VISIT) var_map[? VAR_EVENT_STRIP_CLUB_FIRST_VISIT] = 0;
	if !ds_map_exists(var_map,VAR_EVENT_STRIP_CLUB_THE_STAGE) var_map[? VAR_EVENT_STRIP_CLUB_THE_STAGE] = 0;
	
	// progress
	if !ds_map_exists(var_map,VAR_TRAINING_SIMONE) var_map[? VAR_TRAINING_SIMONE] = 0;
	if !ds_map_exists(var_map,VAR_TRAINING_RONDA) var_map[? VAR_TRAINING_RONDA] = 0;
	if !ds_map_exists(var_map,VAR_TRAINING_RAYE) var_map[? VAR_TRAINING_RAYE] = 0;
	
	// jobs
	if !ds_map_exists(var_map,VAR_JOB_BABYSITTING) var_map[? VAR_JOB_BABYSITTING] = 0;
	if !ds_map_exists(var_map,VAR_JOB_SHOWGIRL) var_map[? VAR_JOB_SHOWGIRL] = 0;
	
	// inventory
	if !ds_map_exists(var_map,VAR_INVENTORY_MAP) ds_map_add_map(var_map,VAR_INVENTORY_MAP,ds_map_create());
	inventory_init_missing_items();
	
	// Others
	if !ds_map_exists(var_map,VAR_MIREILLE_SUGGESTION_CLOTHES) var_map[? VAR_MIREILLE_SUGGESTION_CLOTHES] = 0; #macro VAR_MIREILLE_SUGGESTION_CLOTHES "VAR_MIREILLE_SUGGESTION_CLOTHES"
	if !ds_map_exists(var_map,VAR_TEMP1) var_map[? VAR_TEMP1] = 0; #macro VAR_TEMP1 "VAR_TEMP1"
}
