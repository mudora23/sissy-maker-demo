///jobs_list_add_job(name,begin_day,end_day,begin_0_23,end_0_23,place,des);
/// @description
/// @param name
/// @param begin_day
/// @param end_day
/// @param begin_0_23
/// @param end_0_23
/// @param place
/// @param des

ds_list_add(obj_control.jobs_list_name,argument[0]);
ds_list_add(obj_control.jobs_list_time_begin_day,argument[1]);
ds_list_add(obj_control.jobs_list_time_end_day,argument[2]);
ds_list_add(obj_control.jobs_list_time_begin_0_23,argument[3]);
ds_list_add(obj_control.jobs_list_time_end_0_23,argument[4]);
ds_list_add(obj_control.jobs_list_place,argument[5]);
ds_list_add(obj_control.jobs_list_des,argument[6]);
