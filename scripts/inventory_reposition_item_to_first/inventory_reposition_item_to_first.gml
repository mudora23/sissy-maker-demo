///inventory_reposition_item_to_first(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];
	var item_position = item_map[? INVENTORY_ITEM_ORDER_INDEX];
	
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		
		if item_name == argument[0]
		{
			item_map[? INVENTORY_ITEM_ORDER_INDEX] = 0;
		}
		else if item_map[? INVENTORY_ITEM_ORDER_INDEX] < item_position
		{
			item_map[? INVENTORY_ITEM_ORDER_INDEX]++;
		}
	
		item_name = ds_map_find_next(inventory_map,item_name);
	}

	
}