///_hide_particle(obj);
/// @description
/// @param obj

if scene_is_current()
{
	with argument0 instance_destroy();
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;