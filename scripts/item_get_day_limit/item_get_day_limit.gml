///item_get_day_limit(name);
/// @description
/// @param name
with obj_control
{
	var item_map = item_constants_map[? argument0];
	return item_map[? ITEM_USED_PER_DAY_TIMES_MAX];
}