///dialog_ref_init(string);
/// @description - setup ref and return the string with no ref
/// @param string

ds_list_clear(obj_scene.scene_text_ref_color_index);
ds_list_clear(obj_scene.scene_text_ref_color_color);
ds_list_clear(obj_scene.scene_text_ref_font_index);
ds_list_clear(obj_scene.scene_text_ref_font_font);

var ref_color_index = scene_text_ref_color_index;
var ref_color_color = scene_text_ref_color_color;
var ref_font_index = scene_text_ref_font_index;
var ref_font_font = scene_text_ref_font_font;


var dialog_no_ref = argument0;
    
for (var i = 1; i <= string_length(dialog_no_ref);i++)
{
    var char_skip = 0;
    var the_char = string_char_at(dialog_no_ref, i);
    
    var color_string = "";
    
    if the_char == "["
    {
        char_skip++;
        the_char = string_char_at(dialog_no_ref, i+char_skip);
        
        ////////// setup color //////////
        if the_char == "c"
        {
        char_skip++;
            the_char = string_char_at(dialog_no_ref, i+char_skip);
            
            if the_char == "="
            {
                char_skip++;
                color_string = "";
                the_char = string_char_at(dialog_no_ref, i+char_skip);
                while(the_char!="]")
                {
                    color_string += the_char;
                    char_skip++;
                    the_char = string_char_at(dialog_no_ref, i+char_skip);
                }
                char_skip++;
                ds_list_add(ref_color_index,i);
                ds_list_add(ref_color_color,make_color_hex(color_string));
                //draw_set_color(make_color_hex(color_string));
                dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                // i += char_skip;
                
            }
        
        }
        ////////// setup color END //////////
        ////////// setup bold //////////  
        else if the_char == "b"
        {
            char_skip++;
            the_char = string_char_at(dialog_no_ref, i+char_skip);
            
            if the_char == "]"
            {
                char_skip++;
                ds_list_add(ref_font_index,i);
                ds_list_add(ref_font_font,font_general_bold);
                //draw_set_font(font_general_bold);
                dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                // i += char_skip;
                
            }
        }
        ////////// setup bold END //////////
        ////////// setup italic / italicbold //////////  
        else if the_char == "i"
        {
            char_skip++;
            the_char = string_char_at(dialog_no_ref, i+char_skip);
            
            if the_char == "]"
            {
                char_skip++;
                ds_list_add(ref_font_index,i);
                ds_list_add(ref_font_font,font_general_italic);
                //draw_set_font(font_general_italic);
                dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                // i += char_skip;
            }
            else if the_char == "b"
            {
                char_skip++;
                the_char = string_char_at(dialog_no_ref, i+char_skip);
                
                if the_char == "]"
                {
                    char_skip++;
                    ds_list_add(ref_font_index,i);
                    ds_list_add(ref_font_font,font_general_italic_bold);
                    //draw_set_font(font_general_italic_bold);
                    dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                    // i += char_skip;
                }
            }
        }
        ////////// setup italic / italicbold END //////////
    
        ////////// reset //////////
        else if the_char == "/"
        {
            char_skip++;
            the_char = string_char_at(dialog_no_ref, i+char_skip);
            
            // color
            if the_char == "c"
            {
                char_skip++;
                the_char = string_char_at(dialog_no_ref, i+char_skip);
                
                if the_char == "]"
                {
                    char_skip++;
                    ds_list_add(ref_color_index,i);
                    ds_list_add(ref_color_color,c_white);
                    //draw_set_color(c_white);
                    dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                    // i += char_skip;
                }
            }
            
            // bold
            else if the_char == "b"
            {
                char_skip++;
                the_char = string_char_at(dialog_no_ref, i+char_skip);
                
                if the_char == "]"
                {
                    char_skip++;
                    ds_list_add(ref_font_index,i);
                    ds_list_add(ref_font_font,font_general);
                    //draw_set_font(font_general);
                    dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                    // i += char_skip;
                }
            }
            
            // italic
            else if the_char == "i"
            {
                char_skip++;
                the_char = string_char_at(dialog_no_ref, i+char_skip);
                
                if the_char == "]"
                {
                    char_skip++;
                    ds_list_add(ref_font_index,i);
                    ds_list_add(ref_font_font,font_general);
                    //draw_set_font(font_general);
                    dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                    // i += char_skip;
                }
                // italicbold
                else if the_char == "b"
                {
                    char_skip++;
                    the_char = string_char_at(dialog_no_ref, i+char_skip);
                    if the_char == "]"
                    {
                        char_skip++;
                        ds_list_add(ref_font_index,i);
                        ds_list_add(ref_font_font,font_general);
                        //draw_set_font(font_general);
                        dialog_no_ref = string_copy(dialog_no_ref,1,i-1)+string_copy(dialog_no_ref,i+char_skip,string_length(dialog_no_ref)-i);
                        // i += char_skip;
                    }
                }
            }
        }
        ////////// reset END //////////
    }
}
return dialog_no_ref;

