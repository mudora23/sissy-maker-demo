///ds_list_delete_value(id,value);
/// @description - delete the value if its in the list
/// @param id
/// @param value
ds_list_delete(argument[0],ds_list_find_index(argument[0],argument[1]));