///_job(name,scene_goto);
/// @description
/// @param name
/// @param scene_goto
if scene_is_current()
{
	with obj_job instance_destroy();
    with obj_job_instant instance_destroy();
	var inst = instance_create_depth(0,0,DEPTH_GUI_JOB,obj_job);
	inst.job_name = argument0;
	inst.scene_goto = argument1;
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;


