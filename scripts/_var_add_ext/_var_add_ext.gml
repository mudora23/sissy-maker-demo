///_var_add_ext(var,amount,min,max);
/// @description
/// @param var
/// @param amount
/// @param min
/// @param max
if scene_is_current()
{
	var_add_ext(argument0,argument1,argument2,argument3);
	scene_jump_next();
}
scene_map[? SCENE_POSI]++;