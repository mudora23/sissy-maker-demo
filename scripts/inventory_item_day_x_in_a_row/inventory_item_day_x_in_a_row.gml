///inventory_item_day_x_in_a_row(name);
/// @description
/// @param name
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var item_name = argument[0];
	var item_map = inventory_map[? item_name];
	
	if item_map[? INVENTORY_ITEM_USED_LAST_DAY] < var_get(VAR_DAY) - 1
		return 0;
	else
		return item_map[? INVENTORY_ITEM_USED_DAYS_IN_A_ROW];

}

