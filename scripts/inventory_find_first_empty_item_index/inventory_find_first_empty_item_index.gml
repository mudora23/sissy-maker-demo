///inventory_find_first_empty_item_index();
/// @description
with obj_control
{
	var inventory_map = var_map[? VAR_INVENTORY_MAP];
	var mini_position = noone;
	
	var item_name = ds_map_find_first(inventory_map);
	while(!is_undefined(item_name))
	{
		var item_map = inventory_map[? item_name];
		
		if item_map[? INVENTORY_ITEM_AMOUNT] == 0
		{
			if mini_position == noone mini_position = item_map[? INVENTORY_ITEM_ORDER_INDEX];
			else if item_map[? INVENTORY_ITEM_ORDER_INDEX] < mini_position
				mini_position = item_map[? INVENTORY_ITEM_ORDER_INDEX];
		}
	
		item_name = ds_map_find_next(inventory_map,item_name);
	}

	return mini_position;

}

