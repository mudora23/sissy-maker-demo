{
    "id": "b6d629c9-b473-4962-889a-2784eabe6a0f",
    "modelName": "GMRoom",
    "mvc": "1.0",
    "name": "room_scene_stp_raye",
    "IsDnD": false,
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "3d918555-f412-40a6-8904-0a2d3bd6a677",
        "66335aa0-419f-4473-85d5-751f4143fb3d",
        "b1a4d134-574c-4c1a-b813-aa07420d3fb5",
        "b6762b63-930c-4e11-98eb-c3d46f7cf20a",
        "2d4789d8-9eea-43e2-90ed-afe0b8855f92",
        "45817474-dcae-4ccb-9d8c-15132f277b72",
        "a545eaed-95e9-4a63-82e5-8e96e8582c8b",
        "e820b48d-f703-4e1e-be37-2f4d1dfefef2"
    ],
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "id": "68b2cdd0-2d54-4c54-88e0-ee8fb68a2e4d",
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Instances",
            "userdefined_depth": false,
            "visible": true,
            "instances": [
                {
                    "id": "3d918555-f412-40a6-8904-0a2d3bd6a677",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_7F392AB2",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_7F392AB2",
                    "objId": "42dbcc52-2dd2-4980-95de-811041fccc94",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "66335aa0-419f-4473-85d5-751f4143fb3d",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_3A1789BB",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_3A1789BB",
                    "objId": "8f45702b-31e9-4a77-aefe-87b73687e3d6",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "b1a4d134-574c-4c1a-b813-aa07420d3fb5",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_17984189",
                    "x": 32,
                    "y": 64,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_17984189",
                    "objId": "cae46b9f-68d3-448b-99e9-3b349db00f15",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "b6762b63-930c-4e11-98eb-c3d46f7cf20a",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_767D7409",
                    "x": 0,
                    "y": 448,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_767D7409",
                    "objId": "e203347b-165c-4c50-9821-836baa14c549",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "2d4789d8-9eea-43e2-90ed-afe0b8855f92",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_423B14BD",
                    "x": 64,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_423B14BD",
                    "objId": "7a849f5e-160f-47a4-92ad-9edf7021f376",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "45817474-dcae-4ccb-9d8c-15132f277b72",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_7D5B5F18",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_7D5B5F18",
                    "objId": "7f8896d9-a99b-4968-954f-c72c105ba532",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "a545eaed-95e9-4a63-82e5-8e96e8582c8b",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_20DF559D",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_20DF559D",
                    "objId": "91c72ffb-2deb-4c3c-acc8-31f686ac624c",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "e820b48d-f703-4e1e-be37-2f4d1dfefef2",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": false,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_460B8079",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_460B8079",
                    "objId": "cb27435d-e0e2-4b47-9be4-5155a4722202",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                }
            ]
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "id": "5e25ff41-9ffd-45a7-a5d1-c582570b0c77",
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Background",
            "userdefined_depth": false,
            "visible": true,
            "animationFPS": 15,
            "animationSpeedType": 0,
            "colour": {
                "Value": 4278190080
            },
            "hspeed": 0,
            "htiled": false,
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings": {
        "id": "07f3e066-e44d-4366-9c0f-f4282b030732",
        "modelName": "GMRoomPhysicsSettings",
        "mvc": "1.0",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "inheritPhysicsSettings": false
    },
    "roomSettings": {
        "id": "95ed9188-3125-4b3b-a1ee-1f85f5fa1c7f",
        "modelName": "GMRoomSettings",
        "mvc": "1.0",
        "Height": 600,
        "Width": 800,
        "inheritRoomSettings": false,
        "persistent": false
    },
    "viewSettings": {
        "id": "021f8b0d-26ca-4a00-84d8-2ba772788dea",
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0",
        "clearDisplayBuffer": false,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false
    },
    "views": [
        {
            "id": "335c7366-fbf5-47eb-a22c-297957741eae",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "c67639c1-7016-446f-9645-1dadd9c5815f",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "34fb7805-7668-4741-94ce-2d6aa7f59843",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "e4179c62-165d-403e-b73e-5a67abde6af6",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "4ae96c1e-fac1-41fc-821b-43e847e3828e",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "6ec9aee8-e980-455f-a1a8-ed748ba22fc3",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "16a3ae0f-7b30-4b3b-86f1-d43d8f96dc56",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "4aa83fd5-38be-4e35-a6dc-c5e8e7e9d305",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        }
    ]
}