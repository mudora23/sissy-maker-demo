{
    "id": "9693c7db-0c05-4fbf-aa54-f6b07815b8bc",
    "modelName": "GMRoom",
    "mvc": "1.0",
    "name": "room_scene_desire_club_entrance",
    "IsDnD": false,
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "1065568d-2b98-40d6-93c9-3a9810b472c2",
        "07d56e3b-d921-4386-9d4c-84ee5fb500fd",
        "d5e135e8-02ad-4012-818e-f0c6970c724c",
        "25b860c9-4750-47a1-902d-3dedc5c39665",
        "8c1f90df-407b-4bbd-acb4-1339dbc2cb4d",
        "fca44718-77bd-46fe-84b2-68fbd627836e",
        "ec18fa18-3e4b-43a7-a475-9a4c286d49dd",
        "b8fea0e8-0e53-492a-b7ee-f048d1f0c277"
    ],
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "id": "b8ec360e-433c-4099-98e0-f983db20220b",
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Instances",
            "userdefined_depth": false,
            "visible": true,
            "instances": [
                {
                    "id": "1065568d-2b98-40d6-93c9-3a9810b472c2",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_B0C0393",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_B0C0393",
                    "objId": "42dbcc52-2dd2-4980-95de-811041fccc94",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "07d56e3b-d921-4386-9d4c-84ee5fb500fd",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_6B356DCA",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_6B356DCA",
                    "objId": "8f45702b-31e9-4a77-aefe-87b73687e3d6",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "d5e135e8-02ad-4012-818e-f0c6970c724c",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_29AD3AA1",
                    "x": 32,
                    "y": 64,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_29AD3AA1",
                    "objId": "cae46b9f-68d3-448b-99e9-3b349db00f15",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "25b860c9-4750-47a1-902d-3dedc5c39665",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_228D84F1",
                    "x": 0,
                    "y": 448,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_228D84F1",
                    "objId": "e203347b-165c-4c50-9821-836baa14c549",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "8c1f90df-407b-4bbd-acb4-1339dbc2cb4d",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_229FCF60",
                    "x": 64,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_229FCF60",
                    "objId": "7a849f5e-160f-47a4-92ad-9edf7021f376",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "fca44718-77bd-46fe-84b2-68fbd627836e",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_55BC9D6A",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_55BC9D6A",
                    "objId": "7f8896d9-a99b-4968-954f-c72c105ba532",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "ec18fa18-3e4b-43a7-a475-9a4c286d49dd",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": true,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_65A729D0",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_65A729D0",
                    "objId": "f6bc7500-1326-4807-bfd1-eb3de3d7ce98",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "b8fea0e8-0e53-492a-b7ee-f048d1f0c277",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": false,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_5B5799C5",
                    "x": 32,
                    "y": 32,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_5B5799C5",
                    "objId": "9fbf4265-7d53-459a-83b7-c9d4a4321d28",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                }
            ]
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "id": "603673e3-e67b-4747-98cd-fab528138982",
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Background",
            "userdefined_depth": false,
            "visible": true,
            "animationFPS": 15,
            "animationSpeedType": 0,
            "colour": {
                "Value": 4278190080
            },
            "hspeed": 0,
            "htiled": false,
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings": {
        "id": "7f59cfe4-d9a8-42e3-9280-51e45a0642ff",
        "modelName": "GMRoomPhysicsSettings",
        "mvc": "1.0",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "inheritPhysicsSettings": false
    },
    "roomSettings": {
        "id": "b58e2794-34a0-4c51-a091-07c7351e900a",
        "modelName": "GMRoomSettings",
        "mvc": "1.0",
        "Height": 600,
        "Width": 800,
        "inheritRoomSettings": false,
        "persistent": false
    },
    "viewSettings": {
        "id": "db323d6c-d61a-40a0-b218-5d0f5222fe5d",
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0",
        "clearDisplayBuffer": false,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false
    },
    "views": [
        {
            "id": "696eb55c-3b78-4a63-83b1-d76b81c6510c",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "8f6117eb-61b3-42a6-82ea-d9126fe43010",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "150715ee-cdd8-49cd-9ef4-7e63a7c5ae3a",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "da2098a9-1ebe-4acf-8207-bd3331e70eb4",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "aa437317-9798-4ad2-8e79-b0c2a75848ff",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "f86052e6-83ed-400e-bc3b-4ece19b87b53",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "a2a10926-376d-41fc-9bd6-f3249a614798",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "c55f557a-6ac0-4e58-aa60-9608c7703e01",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        }
    ]
}