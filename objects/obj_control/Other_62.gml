if ds_map_find_value(async_load, "status") == 0
{
    msg = ds_map_find_value(async_load, "result");

	var prefix = "status:";
	if string_length(msg) > string_length(prefix) && string_copy(msg,1,string_length(prefix)) == prefix
	{
		online_status = string_copy(msg,string_length(prefix)+1,string_length(msg)-string_length(prefix));
		show_debug_message("online_status: "+string(online_status));
		if online_status == "error"
			http_get(LINK_ERROR);
	}
	var prefix = "version:";
	if string_length(msg) > string_length(prefix) && string_copy(msg,1,string_length(prefix)) == prefix
	{
		online_version = real(string_copy(msg,string_length(prefix)+1,string_length(msg)-string_length(prefix)));
		show_debug_message("online_version: "+string(online_version));
		if online_version > real(version)
			http_get(LINK_VERSION_LINK);
	}
	var prefix = "versionLink:";
	if string_length(msg) > string_length(prefix) && string_copy(msg,1,string_length(prefix)) == prefix
	{
		online_versionLink = string_copy(msg,string_length(prefix)+1,string_length(msg)-string_length(prefix));
		show_debug_message("online_versionLink: "+string(online_versionLink));
	}
	var prefix = "error:";
	if string_length(msg) > string_length(prefix) && string_copy(msg,1,string_length(prefix)) == prefix
	{
		online_error = string_copy(msg,string_length(prefix)+1,string_length(msg)-string_length(prefix));
		show_debug_message("online_error: "+string(online_error));
	}

	////////////////////////////////////////////////////////////////////
	if online_status == "inactive" game_end();
	if online_status == "error" && online_error != ""
	{
		show_message(online_error);
		game_end();
	}
	if online_version > real(version) && online_versionLink != ""
	{
		if !instance_exists(obj_new_version_popup)
			instance_create(613,5,obj_new_version_popup);
	}
}
