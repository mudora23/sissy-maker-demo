// main gui is hiding by default
if !sissy_maker_is_loading()
{
	main_gui(false);
	var_map[? VAR_MAIN_GUI_PLACENAME] = "";
	var_map[? VAR_CURRENT_ROOM] = room_get_name(room);
}

// returning to title screen, reseting...
if room == room_title_screen
{
	// variables setup
	ds_map_destroy(obj_control.var_map);
	var_init();
	var_init_missing_keys();

	// job
	ds_map_destroy(obj_control.jobs_map);
	jobs_list_init();

	//item
	//ds_map_destroy(obj_control.item_day_map);
	//item_day_init();
}