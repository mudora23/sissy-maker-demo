{
    "id": "8f45702b-31e9-4a77-aefe-87b73687e3d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "398c6a97-e704-465d-bc68-fed26ec8e13e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "f89bac19-2b67-4e17-8eb4-dae200ab6606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "57aceeb5-7f4f-48f9-853c-4a3d490383b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "db7f96f0-8258-4ac8-9e08-f52ced3f75c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "698a3575-c5f3-4549-a9f8-d73af9353de3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "5b4468fa-8df3-4f63-848b-e247008591cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        },
        {
            "id": "2631747b-d4d8-4210-9b15-573af913cd58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "8f45702b-31e9-4a77-aefe-87b73687e3d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}