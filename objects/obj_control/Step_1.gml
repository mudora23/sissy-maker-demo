// controls
action_button_pressed = keyboard_check_pressed(vk_space) ||
						keyboard_check_pressed(vk_enter) ||
                        mouse_check_button_pressed(mb_left);
						
action_button_holding = keyboard_check(vk_space) ||
						keyboard_check(vk_enter) ||
                        mouse_check_button(mb_left);						
						
cancel_button_pressed = keyboard_check_pressed(vk_escape) ||
                        mouse_check_button_pressed(mb_right);
						
left_pressed = mouse_check_button_pressed(mb_left);

if action_button_holding
	action_button_holding_time+=delta_time_sec();
else
	action_button_holding_time = 0;




// music
/*if instance_exists(obj_scene)
{
	if !audio_is_playing(music_index)
	{
		audio_stop_sound(music_id);
		music_id = audio_play_sound(music_index,1,true);
	}
}
else
	audio_stop_sound(music_id);*/
	
	
if obj_control.var_map[? VAR_CURRENT_MUSIC] == ""
	var music_index = noone;
else
	var music_index = asset_get_index(obj_control.var_map[? VAR_CURRENT_MUSIC]);
	
if instance_exists(obj_scene)
{
    
    if audio_is_playing(music_id)
    {
        if !audio_is_playing(music_index)
        {
            if audio_sound_get_gain(music_id) != 0
            {
                audio_sound_gain(music_id, 0, approach(audio_sound_get_gain(music_id)*100,0,50*delta_time_sec()));
            }
            else
                audio_stop_sound(music_id);
            
        }
        else
        {
            if audio_sound_get_gain(music_id) != (control_map[? CONTROL_BGM_LEVEL])
            {
                audio_sound_gain(music_id, control_map[? CONTROL_BGM_LEVEL], approach(abs(audio_sound_get_gain(music_id) - (control_map[? CONTROL_BGM_LEVEL]))*100,0,50*delta_time_sec()));
            }
        }
    }
    else
    {
        if music_index != noone
        {
            music_id = audio_play_sound(music_index,1,true);
            audio_sound_gain(music_id, 0, 0);
            audio_sound_gain(music_id, (control_map[? CONTROL_BGM_LEVEL]), 500);
        }
    }
}
else
{
    if audio_sound_get_gain(music_id) != 0
    {
        audio_sound_gain(music_id, 0, approach(audio_sound_get_gain(music_id)*100,0,50*delta_time_sec()));
    }
    else
        audio_stop_sound(music_id);
}