// save_string
secretString = "SissyMakerAdvancedSaving";

// settings
control_map = ds_map_create();
if file_exists("controls.dat")
    control_map = sissy_maker_load_map("controls.dat",secretString);
else
{
	control_map[? CONTROL_BGM_LEVEL] = 0.6;
	control_map[? CONTROL_SFX_LEVEL] = 0.6;
	control_map[? CONTROL_FULLSCREEN] = false;
	control_map[? CONTROL_AUTOSAVE] = true;
	control_map[? CONTROL_TEXT_SPEED]= 30;
	
	sissy_maker_save_map("controls.dat",control_map,secretString);
}

// randomized
randomize();

// thumbnail and saving variables
thumbnail_init();

// item constants
item_constants_map_init();

// variables setup
var_init();
var_init_missing_keys();

// job
jobs_list_init();

// music
music_id = noone;

// textbox
textbox_y = 600;
textbox_alpha = 0;

// control
action_button_pressed = false;
action_button_holding = false;
action_button_holding_time = 0;
cancel_button_pressed = false;

choice_padding = 10;
choice_margin = 5;

debug = 0;
debug_enabled = false;

room_speed = 99999;
depth = DEPTH_DEBUG;

// version number
version = "0.96";

// getting online information
online_status = "";
online_version = noone;
online_versionLink = "";
online_error = "";
http_get(LINK_STATUS);
http_get(LINK_VERSION);

// making an event map if not exists (backword compatibility / removed somehow)
if !file_exists("event.dat")
{
	
	event_map = ds_map_create();
	file_num = 0;
	while file_exists("save"+string(file_num)+"stats.dat")
	{
		var temp_var_map = sissy_maker_load_map("save"+string(file_num)+"stats.dat",obj_control.secretString);
		var key = ds_map_find_first(temp_var_map);
		while(!is_undefined(key))
		{
			if string_copy(key,1,9) == "VAR_EVENT"
			{
				if ds_map_exists(event_map,key)
				{
					event_map[? key] = max(event_map[? key],temp_var_map[? key]);
				}
				else
				{
					event_map[? key] = temp_var_map[? key];
				}

			}
			var key = ds_map_find_next(temp_var_map,key);
		}
		file_num++;
		ds_map_destroy(temp_var_map);
	}

	sissy_maker_save_map("event.dat",event_map,obj_control.secretString);
}
else
	event_map = sissy_maker_load_map("event.dat",obj_control.secretString);



// next room
//room_goto(room_title_screen);
