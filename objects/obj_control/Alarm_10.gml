/// @description Insert description here
// You can write your code in this editor

// auto save
if obj_control.control_map[? CONTROL_AUTOSAVE] && !sissy_maker_is_loading()
{
	if !in_room_transition() && sissy_maker_can_save()
	{
		thumbnail_setup();
		sissy_maker_save(0);	
	}
	else alarm[10]=1;
}
