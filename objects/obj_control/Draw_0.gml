///debug
if debug == 1
{

	draw_sprite(spr_dim,-1,0,0);
	
    text_x = 20;
    text_y = 20;
	draw_set_font(font_general_20);
    draw_text_outlined(text_x, text_y, "Type a number to switch between pages.", c_white, c_black, 1,1,2);
	draw_set_font(font_general_13);
	text_y += 40;
	draw_text_outlined(text_x, text_y, "Going to a location.", c_white, c_black, 1,1,2);
    text_y += 40;
	var the_room = room_title_screen;
    while(true)
    {
        
        if mouse_over_area(text_x,text_y,string_width(room_get_name(the_room)),string_height(room_get_name(the_room)))
        {
            draw_text_outlined(text_x, text_y, room_get_name(the_room), c_yellow, c_black, 1,1,2);
            if mouse_check_button_pressed(mb_left)
                room_goto_ext(the_room,true);
        }
        else
        {
            draw_text_outlined(text_x, text_y, room_get_name(the_room), c_white, c_black, 1,1,2);
        }
            
        if the_room == room_last
            break;
        else
        {
            text_y += 25;
            the_room = room_next(the_room);
        }
    }
	
    text_x = 350;
    text_y = 60;
	draw_text_outlined(text_x, text_y, "General variables", c_white, c_black, 1,1,2);
	text_y += 15;

	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "Day ("+string(var_get(VAR_DAY))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "Cash ("+string(var_get(VAR_CASH))+")";
	var minusB = "-5000";
	var plusB = "+5000";
	var minus = "-500";
	var plus = "+500";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CASH,-5000);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CASH,-500);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CASH,500);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CASH,5000);
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "HP ("+string(var_get(VAR_HP))+")";
	var minusB = "-100";
	var plusB = "+100";
	var minus = "-10";
	var plus = "+10";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_HP,-100);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_HP,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_HP,10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_HP,100);
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "ENERGY ("+string(var_get(VAR_ENERGY))+")";
	var minusB = "-100";
	var plusB = "+100";
	var minus = "-10";
	var plus = "+10";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_ENERGY,-100);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_ENERGY,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_ENERGY,10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_ENERGY,100);
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "DAYSREMAINING ("+string(var_get(VAR_DAYSREMAINING))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DAYSREMAINING,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DAYSREMAINING,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DAYSREMAINING,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DAYSREMAINING,10);
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "TIME ("+string(var_get(VAR_TIME))+")";
	//var minusB = "-8";
	var plusB = "+8";
	//var minus = "-2";
	var plus = "+2";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) time_hour_passed(8);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) time_hour_passed(8));
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) time_hour_passed(2);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) time_hour_passed(8);
	
	
	text_x = 350;
	text_y += 40;
	draw_text_outlined(text_x, text_y, "Stats", c_white, c_black, 1,1,2);
	text_y += 15;
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "STRENGTH ("+string(var_get(VAR_STRENGTH))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STRENGTH,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STRENGTH,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STRENGTH,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STRENGTH,10);
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "STAMINA ("+string(var_get(VAR_STAMINA))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STAMINA,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STAMINA,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STAMINA,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_STAMINA,10);
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "DEXTERITY ("+string(var_get(VAR_DEXTERITY))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DEXTERITY,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DEXTERITY,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DEXTERITY,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_DEXTERITY,10);
	
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "BARGAIN ("+string(var_get(VAR_BARGAIN))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_BARGAIN,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_BARGAIN,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_BARGAIN,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_BARGAIN,10);
	
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "INTELLIGENCE ("+string(var_get(VAR_INTELLIGENCE))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_INTELLIGENCE,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_INTELLIGENCE,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_INTELLIGENCE,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_INTELLIGENCE,10);
	
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "CHARISMA ("+string(var_get(VAR_CHARISMA))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CHARISMA,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CHARISMA,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CHARISMA,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_CHARISMA,10);
	
	//////////////////////////////////////////////////////////////
	
	text_x = 350;
	text_y += 25;
	var name = "SEX_APPEAL ("+string(var_get(VAR_SEX_APPEAL))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SEX_APPEAL,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SEX_APPEAL,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SEX_APPEAL,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SEX_APPEAL,10);
	
	
	
	draw_set_font(font_general_30b);
	var name = "DEBUG PAGE 1";
	draw_text_outlined(room_width-20-string_width(name), room_height-20-string_height(name), name, c_white, c_black, 1,1,2);
	
	
	
	draw_reset();






}


if debug == 2
{

	draw_sprite(spr_dim,-1,0,0);
	
    text_x = 20;
    text_y = 20;
	draw_set_font(font_general_20);
    draw_text_outlined(text_x, text_y, "Type a number to switch between pages.", c_white, c_black, 1,1,2);
	draw_set_font(font_general_13);
	text_y += 40;

	draw_text_outlined(text_x, text_y, "Stats 2", c_white, c_black, 1,1,2);
	text_y += 15;

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "FEMALE_RATIO ("+string(var_get(VAR_FEMALE_RATIO))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-0.1";
	var plus = "+0.1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_FEMALE_RATIO,max(0.0,var_get(VAR_FEMALE_RATIO)-0.1));
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_FEMALE_RATIO,min(1.0,var_get(VAR_FEMALE_RATIO)+0.1));
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "CUP_SIZE ("+string(var_get(VAR_CUP_SIZE))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_CUP_SIZE,max(0,var_get(VAR_CUP_SIZE)-1));
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_CUP_SIZE,min(10,var_get(VAR_CUP_SIZE)+1));
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "COCK ("+string(var_get(VAR_COCK))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_COCK,1,var_get(VAR_COCK_MIN),var_get(VAR_COCK_MAX));
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_COCK,-1,var_get(VAR_COCK_MIN),var_get(VAR_COCK_MAX));
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "ANUS ("+string(var_get(VAR_ANUS))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_ANUS,max(var_get(VAR_ANUS_MIN),var_get(VAR_ANUS)-1));
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_ANUS,min(var_get(VAR_ANUS_MAX),var_get(VAR_ANUS)+1));
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	
	text_x = 20;
	text_y += 40;
	draw_text_outlined(text_x, text_y, "AFF", c_white, c_black, 1,1,2);
	text_y += 15;
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_CHRIS ("+string(var_get(VAR_AFF_CHRIS))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_CHRIS,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_DAYNA ("+string(var_get(VAR_AFF_DAYNA))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_DAYNA,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_DAYNA,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_PENNY ("+string(var_get(VAR_AFF_PENNY))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_PENNY,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_PENNY,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_RONDA ("+string(var_get(VAR_AFF_RONDA))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_RONDA,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_RONDA,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_SECURITY ("+string(var_get(VAR_AFF_SECURITY))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SECURITY,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SECURITY,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_SIMONE ("+string(var_get(VAR_AFF_SIMONE))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SIMONE,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SIMONE,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_SOPHIA ("+string(var_get(VAR_AFF_SOPHIA))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SOPHIA,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_SOPHIA,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_RAYE ("+string(var_get(VAR_AFF_RAYE))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_RAYE,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_RAYE,1,1,4);
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_JOHNNY ("+string(var_get(VAR_AFF_JOHNNY))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_JOHNNY,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_JOHNNY,1,1,4);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_MIREILLE ("+string(var_get(VAR_AFF_MIREILLE))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_MIREILLE,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_MIREILLE,1,1,4);
	
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	var name = "AFF_KANE ("+string(var_get(VAR_AFF_KANE))+")";
	//var minusB = "-0.1";
	//var plusB = "+0.1";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 180;
	/*var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)-10);
	text_x += 60;*/
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_KANE,-1,1,4);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add_ext(VAR_AFF_KANE,1,1,4);
	
	/*text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_DAY,var_get(VAR_DAY)+10);*/
	
	
	
	
	
	
	
	
	text_x = 350;
	text_y = 60;
	draw_text_outlined(text_x, text_y, "Skill", c_white, c_black, 1,1,2);
	text_y += 15;
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "SKILL_BABYSITTING ("+string(var_get(VAR_SKILL_BABYSITTING))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_BABYSITTING,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_BABYSITTING,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_BABYSITTING,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_BABYSITTING,10);
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "SKILL_JAZZ ("+string(var_get(VAR_SKILL_JAZZ))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_JAZZ,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_JAZZ,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_JAZZ,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_JAZZ,10);
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "SKILL_SALSA ("+string(var_get(VAR_SKILL_SALSA))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_SALSA,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_SALSA,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_SALSA,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_SKILL_SALSA,10);
	
	
	text_x = 350;
	text_y += 60;
	draw_text_outlined(text_x, text_y, "Jobs/Train/Study counter", c_white, c_black, 1,1,2);
	text_y += 15;
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "TRAINING_RONDA ("+string(var_get(VAR_TRAINING_RONDA))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_RONDA,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_RONDA,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_RONDA,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_RONDA,10);
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "TRAINING_SIMONE ("+string(var_get(VAR_TRAINING_SIMONE))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_SIMONE,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_SIMONE,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_SIMONE,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_TRAINING_SIMONE,10);
	
	//////////////////////////////////////////////////////////////
	text_x = 350;
	text_y += 25;
	var name = "JOB_BABYSITTING ("+string(var_get(VAR_JOB_BABYSITTING))+")";
	var minusB = "-10";
	var plusB = "+10";
	var minus = "-1";
	var plus = "+1";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 230;
	var hover = mouse_over_area(text_x,text_y,string_width(minusB),string_height(minusB));
	if hover draw_text_outlined(text_x, text_y, minusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_JOB_BABYSITTING,-10);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_JOB_BABYSITTING,-1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_JOB_BABYSITTING,1);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plusB),string_height(plusB));
	if hover draw_text_outlined(text_x, text_y, plusB, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plusB, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_add(VAR_JOB_BABYSITTING,10);
	
	
	
	
	
	
	
	draw_set_font(font_general_30b);
	var name = "DEBUG PAGE 2";
	draw_text_outlined(room_width-20-string_width(name), room_height-20-string_height(name), name, c_white, c_black, 1,1,2);
	
	
	
	draw_reset();

}
if debug == 3
{

	draw_sprite(spr_dim,-1,0,0);
	
    text_x = 20;
    text_y = 20;
	draw_set_font(font_general_20);
    draw_text_outlined(text_x, text_y, "Type a number to switch between pages.", c_white, c_black, 1,1,2);
	draw_set_font(font_general_13);
	text_y += 40;

	draw_text_outlined(text_x, text_y, "Events Unlocked", c_white, c_black, 1,1,2);
	text_y += 15;

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_APT_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_APT_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_APT_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_APT_VISIT,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_HQ_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_HQ_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_HQ_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_HQ_VISIT,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_MAINHALL_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_MAINHALL_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_MAINHALL_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_MAINHALL_VISIT,true);
	
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_LIBRARY_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_LIBRARY_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_LIBRARY_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_LIBRARY_VISIT,true);
	
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_GYM_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_GYM_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_GYM_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_GYM_VISIT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_LAB_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_LAB_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_LAB_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_LAB_VISIT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STP_PRINCIPAL_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STP_PRINCIPAL_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_PRINCIPAL_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STP_PRINCIPAL_VISIT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_HOSPITAL_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_HOSPITAL_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_HOSPITAL_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_HOSPITAL_VISIT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_LOTUS_DEW_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_LOTUS_DEW_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_LOTUS_DEW_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_LOTUS_DEW_VISIT,true);
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 40;
	if var_get(VAR_EVENT_FIRST_MISSION) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_FIRST_MISSION ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_FIRST_MISSION,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_FIRST_MISSION,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_RONDA_RUTHLESS) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_RONDA_RUTHLESS ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RONDA_RUTHLESS,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RONDA_RUTHLESS,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_SIMONE_SAPIENZA) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_SIMONE_SAPIENZA ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_SIMONE_SAPIENZA,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_SIMONE_SAPIENZA,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_JAMES_LAKIN_AND_CHRIS_01 ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_FLU) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_FLU ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_FLU,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_FLU,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_BANKRUPT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_BANKRUPT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_BANKRUPT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_BANKRUPT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_DEADLINE) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_DEADLINE ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DEADLINE,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DEADLINE,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_DEMO_END) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_DEMO_END ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DEMO_END,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DEMO_END,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_DANCER) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_DANCER ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_DANCER_LOTUS_RESTROOM) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_DANCER_LOTUS_RESTROOM ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER_LOTUS_RESTROOM,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER_LOTUS_RESTROOM,true);
	
	draw_set_font(font_general_30b);
	var name = "DEBUG PAGE 3";
	draw_text_outlined(room_width-20-string_width(name), room_height-20-string_height(name), name, c_white, c_black, 1,1,2);
	
	
	
	draw_reset();

}

if debug == 4
{

	draw_sprite(spr_dim,-1,0,0);
	
    text_x = 20;
    text_y = 20;
	draw_set_font(font_general_20);
    draw_text_outlined(text_x, text_y, "Type a number to switch between pages.", c_white, c_black, 1,1,2);
	draw_set_font(font_general_13);
	text_y += 40;

	draw_text_outlined(text_x, text_y, "Events Unlocked", c_white, c_black, 1,1,2);
	text_y += 15;

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_SORE_ASS) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_SORE_ASS ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_SORE_ASS,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_SORE_ASS,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_ASS_LOTION) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_ASS_LOTION ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_ASS_LOTION,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_ASS_LOTION,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_DANCER_AT_STP) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_DANCER_AT_STP ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER_AT_STP,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_DANCER_AT_STP,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_RAYE_SANCHES_INTRO) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_RAYE_SANCHES_INTRO ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RAYE_SANCHES_INTRO,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RAYE_SANCHES_INTRO,true);

	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_RAYE_SANCHES_FUCKED) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_RAYE_SANCHES_FUCKED ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RAYE_SANCHES_FUCKED,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RAYE_SANCHES_FUCKED,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_RONDA_TALKS_ABOUT_THE_DANCER ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_KANES_CANDY_CANE) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_KANES_CANDY_CANE ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_KANES_CANDY_CANE,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_KANES_CANDY_CANE,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_KANES_CANDY_CANE2) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_KANES_CANDY_CANE ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_KANES_CANDY_CANE2,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_KANES_CANDY_CANE2,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STRIP_CLUB_FIRST_VISIT) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STRIP_CLUB_FIRST_VISIT ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_FIRST_VISIT,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_FIRST_VISIT,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STRIP_CLUB_PT1) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STRIP_CLUB_PT1 ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT1,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT1,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STRIP_CLUB_PT2) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STRIP_CLUB_PT2 ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT2,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT2,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STRIP_CLUB_PT3) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STRIP_CLUB_PT3 ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT3,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_PT3,true);
	
	//////////////////////////////////////////////////////////////
	text_x = 20;
	text_y += 25;
	if var_get(VAR_EVENT_STRIP_CLUB_THE_STAGE) var unlocked = "YES";
	else var unlocked = "NO";
	var name = "EVENT_STRIP_CLUB_THE_STAGE ("+string(unlocked)+")";
	var minus = "NO";
	var plus = "YES";
	draw_text_outlined(text_x, text_y, name, c_white, c_black, 1,1,2);
	text_x += 350;
	var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
	if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_THE_STAGE,false);
	text_x += 60;
	var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
	if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
	else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
	if hover && mouse_check_button_pressed(mb_left) var_set(VAR_EVENT_STRIP_CLUB_THE_STAGE,true);
	
	draw_set_font(font_general_30b);
	var name = "DEBUG PAGE 4";
	draw_text_outlined(room_width-20-string_width(name), room_height-20-string_height(name), name, c_white, c_black, 1,1,2);
	
	
	
	draw_reset();

}



if debug_enabled && keyboard_check_pressed(ord("1"))
{
	scene_play_sfx(sfx_button_01);
	if debug == 1 debug = 0;
	else debug = 1;	
	
}
if debug_enabled && keyboard_check_pressed(ord("2"))
{
	scene_play_sfx(sfx_button_01);
	if debug == 2 debug = 0;
	else debug = 2;	
	
}
if debug_enabled && keyboard_check_pressed(ord("3"))
{
	scene_play_sfx(sfx_button_01);
	if debug == 3 debug = 0;
	else debug = 3;		
	
}
if debug_enabled && keyboard_check_pressed(ord("4"))
{
	scene_play_sfx(sfx_button_01);
	if debug == 4 debug = 0;
	else debug = 4;		
	
}