{
    "id": "9fbf4265-7d53-459a-83b7-c9d4a4321d28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_desire_club_entrance",
    "eventList": [
        {
            "id": "4290bec9-08b0-49fd-8428-a2238de37c53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9fbf4265-7d53-459a-83b7-c9d4a4321d28"
        },
        {
            "id": "ce65b12e-31e8-4753-90eb-64c14f8780e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "9fbf4265-7d53-459a-83b7-c9d4a4321d28"
        },
        {
            "id": "0dc0d4ff-d60d-41ae-909b-ccac9ddbe2bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9fbf4265-7d53-459a-83b7-c9d4a4321d28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}