event_inherited();
	
_start();
	_placename(PLACENAME_desire_club_entrance);
	_main_gui(true);
	_show("BG",spr_bg_desireClubEntrance,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if !var_get(VAR_EVENT_STRIP_CLUB_PT2) // this is read when the player has been here before
		_jump("EVENT_STRIP_CLUB_PT2");
	else if !var_get(VAR_EVENT_STRIP_CLUB_PT3) // this is read only when the player has already passed the req.
		_jump("EVENT_STRIP_CLUB_PT3");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Late Night");
		
_border(100); // add content before this
		
_label("Stay");
	_stay(true,true,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("Menu");
	_block();

_label("StayRestHide");
	_stay_rest_hide("Menu");
	_block();
	
_border(100); // add content before this
	
_label("Morning");
	_show("chris",spr_char_Chris_Neutral,2,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#6678ff](Looks like this is the final call for all the partiers. A big crowd is milling about in the street in front of Desire. Wasted people, drunks, and hookers all trying to find their way home.)[/c]");
	_dialog("[c=#6678ff](The salacious glamour that this place seems to have surrounding it at night is gone in the morning light. Now it just looks dirty. This sucks. Johnny’s nowhere to be seen.)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] This sucks dick. And not in the fun way.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I was just thinking about that, actually.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Were you, now?");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] N-not like that, dummy!");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You know James, the club’s empty right now. I could give you a private dance session and you could-");
	_dialog("[c=#6678ff]James:[/c] Don’t think about that at a time like this! This place looks dangerous at this time.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] We’ll be lucky if we don’t get mugged on our way home.");
	_dialog("[c=#ff00ff]Chris:[/c] Or ravished.");
	_jump("Menu");

_border(100); // add content before this

_label("Afternoon");
	_jump("Menu");

_border(100); // add content before this

_label("Evening");
	_show("johnny",spr_char_Johnny_Smug,1.5,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Johnny:[/c] Come on in the best showgirls of the town! Only at Desire Club.");
	_jump("Menu");

_border(100); // add content before this

_label("Late Night");
	_show("johnny",spr_char_Johnny_Horny,1.8,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Johnny:[/c] All right, pussy, pussy, pussy! Come on in cunny lovers! Here at the Desire Club we're slashing the price of muff in half! Give us an offer on our vast selection of pussy!");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I think Johnny’s had too much booze.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] It works better for his advertisement, though.");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Haven’t I heard that a quote from a movie, or something?");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Yeah, I heard it once when I was a kid…");
	_jump("Menu");
	
_border(100); // add content before this

_label("Menu");
	_main_gui(true);
	if current_time_get_name() == "Evening" || current_time_get_name() == "Late Night"
		_hide_all_except("BG","johnny",FADE_NORMAL);
	else
		_hide_all_except("BG",FADE_NORMAL);
	if (current_time_get_name() == "Evening" || current_time_get_name() == "Late Night") &&
	   !scene_sprite_object_exists("johnny")
		_show("johnny",spr_char_Johnny_Smug,1.5,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	else
		_void();
	if (current_time_get_name() == "Evening" || current_time_get_name() == "Late Night") &&
	   //desire_club_female_met() &&
	   desire_club_dance_skill_met() &&
	   desire_club_dance_cup_size_met()
	{
		_choice(spr_choice_s,"Show Girl","SHOW_GIRL",noone,fa_right,fa_center);
		_choice(spr_choice_s,"Enter Desire Club! ($40)","ENTER INTRO",noone,fa_right,fa_center);
	}
	else
	{
		_void();
		_void();
	}
	if current_time_get_name() == "Morning"
		_choice(spr_choice,"Talk","Talk",noone,fa_right,fa_center);
	else if current_time_get_name() == "Evening" || current_time_get_name() == "Late Night"
		_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	else
		_void();
	if (current_time_get_name() == "Evening" || current_time_get_name() == "Late Night")
	{
		_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
		_choice(spr_choice_s,"Exit","Exit",noone,fa_right,fa_center);
	}
	else
	{
		_choice(spr_choice,"Stay","Stay",noone,fa_center,fa_center);
		_choice(spr_choice,"Exit","Exit",noone,fa_center,fa_center);
	}
	_block();

_border(100); // add content before this

_label("SHOW_GIRL");
	_dialog("[c=#39f979]Johnny:[/c] Get your ass in there and shake it.");
	
	_main_gui(false);
	_show("JOB_BG",spr_job_show_girl_bg,5,5,1,DEPTH_CHARACTER-3,false,FADE_NORMAL);
	_show("JOB_DES",spr_job_show_girl,10,5,1,DEPTH_CHARACTER-4,false,FADE_NORMAL);
	_move("JOB_DES",5,5,3);
	_pause(0.5,0,true);
	_choice(spr_choice_lightgreen,"Assign","Job - Show Girl Assigning Yes",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","Menu",noone,fa_center,fa_bottom);
	_block();

_border(100); // add content before this

_label("ENTER INTRO");
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Johnny:[/c] Chris works here, so she can go in. James, you’re just a customer like anyone else. So it’s $40 to enter.");
	_dialog("[c=#ff00ff]Chris:[/c] Well James, I will not work if you’re going in. But I’ll give you a little sample if you’d like! Hee hee!");
	if var_get(VAR_CASH) >= 40
		_jump("ENTER Confirm");
	else
		_jump("ENTER Fail");
	
_label("ENTER Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");
	_jump("Menu");
	
_label("ENTER Confirm");
	_var_add(VAR_CASH,-40);
	_room_goto_ext(room_scene_desire_club,true);
	_block();

_border(100); // add content before this

_label("Exit");
	if current_time_get_name() == "Evening" || current_time_get_name() == "Late Night" 
	{
		_choice(spr_choice_s,"Yes",noone,room_scene_main_map,fa_right,fa_center);
		_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	}
	else
	{
		_choice(spr_choice,"Yes",noone,room_scene_main_map,fa_center,fa_center);
		_choice(spr_choice,"No","Menu",noone,fa_center,fa_center);
	}
	_dialog("[c=#FFFF00]Back to the main map?[/c]",false);
	_block();

_border(100); // add content before this

_label("Talk");
	if current_time_get_name() == "Morning"
		_jump("Talk Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Talk Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Talk Evening");
	else
		_jump("Talk Late Night");

_border(100); // add content before this

_label("Talk Morning");
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](Not gonna lie, I’m not a big fan of chatting with people too hammered and horny to stand up straight.)[/c]");
	_dialog("[c=#6678ff](...At least with their legs.)[/c]");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Afternoon");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Evening");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Ah! Nothing quite like workin’ in the business of flesh, eh James?");
	_dialog("[c=#6678ff]James:[/c] If you say so.");
	_change("johnny",spr_char_Johnny_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] The money, the glamour; T&A every day! Plus the off-duty fun ya can have with such -erm ‘talented’ women!");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Late Night");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] You know I used tah be a carphenter?");
	_dialog("[c=#6678ff]James:[/c] Uh, I think you’ve had a bit too much to drink.");
	_dialog("[c=#39f979]Johnny:[/c] Poundin’ nailsh eeevrey day! Pssh! Fuckhin grunt work it wassh! Hic!");
	_change("johnny",spr_char_Johnny_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Now I pound other thingsh, eh? Like that cute-asshed fried a’ yers! Wheresh Chrish? I need to sshow her somethin’!");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Eeek! Save me, James!");
	_jump("Menu");

_border(100); // add content before this

_label("EVENT_STRIP_CLUB_PT2");
	_choice(spr_choice,"Continue","EVENT_STRIP_CLUB_PT2_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris arrive at Desire Club and meet the owner, Johnny D’Vitta.[/c]",false);
	_block();
	
_label("EVENT_STRIP_CLUB_PT2_START");
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#6678ff](As Chris and I approach the newly opened Club, a strange looking guy approaches from the front door. Something about him immediately skeeves me out.)[/c]");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris is as oblivious as always.)[/c]");
	_show("johnny",spr_char_Johnny_Horny,1.5,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]???:[/c] Well, well! What have we, here?");
	_dialog("[c=#ff00ff]Chris:[/c] Hiya!");
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]???:[/c] Two new entrants into the perfect place for dancing and desire! Hello, hello!");
	_dialog("[c=#39f979]???:[/c] Sorry, the club is closed to patrons at the moment. We’re only letting in staff till we can fill our vacancies.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Actually, that’s why we’re here.");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I was hoping I could be a dancer!");
	_change("johnny",spr_char_Johnny_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Ah! Fantastic! Put ‘er there! Name’s Johnny D’Vitta. I’m the owner of this joint. Welcome! I hope you’re ready to shake it!");
	_if(var_get(VAR_AFF_JOHNNY)<1,_var_set,VAR_AFF_JOHNNY,1);
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m always ready!");
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] I hope so! Well, consider this your tryout: Impress me! Let’s see some dance moves!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris begins her personal dance routine, bouncing and jiggling to a beat that only she can hear.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](...I really should stop staring.)[/c]");
	
_label("EVENT_STRIP_CLUB_PT2_START_REQ");
	_pause(0.2,0,true);
	if desire_club_female_met()
	{
		_void();//_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
		_void();//_dialog("[c=#39f979]Johnny:[/c] What style, what grace! And such a pretty little face! She’s perfect! Heh heh.");
	}
	else
	{
		_void();//_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
		_void();//_dialog("[c=#39f979]Johnny:[/c] Hmm… I like what I see, but I’m worried about Desire’s regular clients. They want more- ah, ‘conventional’ female dancers.");
	}
	_pause(0.2,0,true);
	if desire_club_dance_skill_met()
	{
		_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
		_dialog("[c=#39f979]Johnny:[/c] Mmmm, I think I’m in love… or at least in lust. Bend over and do that thing again, that hip twerk of yours… Yes, that’s it! Now that's a dancer I’d like to drape over a counter, and-");
	}
	else
	{
		_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
		_dialog("[c=#39f979]Johnny:[/c] …No, no, no, no! Did a drunken monkey teach you how to dance? You’re supposed to be titillating their groins. You look like you're trying to pound nails with your butt!");
	}
	_pause(0.2,0,true);
	if desire_club_dance_cup_size_met()
	{
		_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
		_dialog("[c=#39f979]Johnny:[/c] Yowza! Look at them boobies bounce! I gotta admit: though I’m a bit of an ass man myself, you may well have tipped the balance for me!");
		_void();
	}
	else
	{
		_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
		_dialog("[c=#39f979]Johnny:[/c] Sorry, you're fine and all, but I’ve got a zero tolerance policy for flat tits.");
		_dialog("[c=#39f979]Johnny:[/c] You look too young! and the last thing I want is cops breathing down my neck. Ya gotta get some fat on them sweater puppies before I can take you on.");
	}
	_var_set(VAR_EVENT_STRIP_CLUB_PT2,true);
	if /*desire_club_female_met() && */desire_club_dance_skill_met() && desire_club_dance_cup_size_met()
		_jump("STRIP_CLUB_REQ Met");
	else
		_jump("STRIP_CLUB_REQ Fail");
	
_label("STRIP_CLUB_REQ Met");
	_var_set(VAR_EVENT_STRIP_CLUB_PT3,true); // mark this as read too as the player already passes the req.
	_var_add_ext(VAR_AFF_JOHNNY,2,1,4);
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] All right, I think I’ve seen enough. You’re hired!");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Really? Horray!");
	_change("johnny",spr_char_Johnny_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] The pleasure’s all mine, doll!");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](The scummy guy bends forward and takes Chris’ hand in his, planting a sloppy kiss on the top of it.)[/c]");
	_dialog("[c=#6678ff](...I really hope she knows what she’s doing with this.)[/c]");
	_dialog("[c=#39f979]Johnny:[/c] Say, what’s your name by the way?");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] It’s… Chris.");
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Well, “Chris,” you and I are going to get along just fine, I think. Come back soon, I’ll have some work lined up for ya!");
	_room_goto_ext(room_scene_desire_club,true);
		
_label("STRIP_CLUB_REQ Fail");	
	_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Hrmm… I’ll admit: you’ve got potential. But I need to see some serious improvement before I can take you on as one of my girls, capiche?");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Aww! Was it my dancing?");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I think it has more to do with how he ‘views’ you, Chris.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Come on, we can come back some other time and try again.");
	_room_goto_ext(room_scene_main_map,true);
	
_border(100); // add content before this
	
_label("EVENT_STRIP_CLUB_PT3");
	_choice(spr_choice,"Continue","EVENT_STRIP_CLUB_PT3_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris arrive at Desire Club and meet the owner, Johnny D’Vitta.[/c]",false);
	_block();
	
_label("EVENT_STRIP_CLUB_PT3_START");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("johnny",spr_char_Johnny_Horny,1.5,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Johnny:[/c] Well well, look who’s back!");
	_dialog("[c=#ff00ff]Chris:[/c] I want to try out again! I know I can do it!");
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Are you sure, sweet cheeks? You didn’t do so hot last time.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m ready.");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Jeez, Chris! What’s with those eyes?");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I can do this, James! Just you watch!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris begins her personal dance routine, bouncing and jiggling to a beat that only she can hear.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](...I really should stop staring.)[/c]");
	_jump("EVENT_STRIP_CLUB_PT2_START_REQ");

_border(100); // add content before this
	
_label("Job - Show Girl Assigning Yes");
	_placename(PLACENAME_desire_club);
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_change("BG",spr_bg_desireClub,FADE_SLOW);
	_pause(1,0,true);
	_show("chris",spr_char_Chris_Neutral,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](Gosh! I’m always so darn nervous! My heart is beating out of my chest!)[/c]");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff](I hope I don’t disappoint. I don’t think I could ever get used to doing something like this!)[/c]");
	_dialog("[c=#ff00ff](My hands are shaking, my body’s blushing all over and I can feel sweat building on my forehead.)[/c]");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff](Still, I’ve never been so excited before. I want people to see me, to want me. I want to show off and bare my body to everyone!)[/c]");
	_dialog("Speaker voice: All right! Ladies and Gentlemen, may I direct your eyes to the stage. Please give a warm, one-handed welcome to Crystal!"); 
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff](W-wait, what?! What’s wrong with my real name?)[/c]");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff](Oh man, I wasn’t expecting this! I just wanted to-)[/c]");
	_dialog("Speaker voice: Er, I’m sorry folks, it seems I must have mistimed the introduction: May I direct your eyes to the stage: Crystaaaal!"); 
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff](Aaaah! I missed my cue! Guess there’s no helping it. Deep breaths, Chris. Confidence! Exude confidence!)[/c]");

	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_desire_club_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);

	_dialog("[c=#ff00ff](I begin my dance, sashaying across the long boulevard leading to the pole in the middle of the room.)[/c]");
	_dialog("[c=#ff00ff](I don’t miss a beat as I slink across the stage, crawling to my hands and knees before I reach the pole as if begging for it, like the pole is a metaphor for cock.)[/c]");
	_dialog("[c=#ff00ff](My ass is in the air, I hide my face behind my bangs as I twerk and twist and jolt my hips back and forth. I’m swaying, extending my arms back behind me as if to allow for a deep pounding.)[/c]");

	_change("CG",spr_hentai_cg_desire_club_2,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff00ff](I stand, the only thought going through my mind being: “Will they notice it?” I want them to see me as I see myself: a strong, confident female. But I still have…)[/c]");
	_dialog("[c=#ff00ff](The worries fly from my mind as I do a hard spin in place, locking eyes with a stranger and striding confidently over to him. He grins and tosses cash onto the dais. I shake my breasts in his face as thanks.)[/c]");
	_dialog("[c=#ff00ff](I strip my shirt off, tossing it carelessly across the aisle as I bounce and twist, grabbing the pole and sliding up and down it as best as I can. I’m still a bit new at this.)[/c]");
	_dialog("[c=#ff00ff](The music transfixes me. I embrace it, sticking my ass out as I peel out of more and more clothing. By the end I’m down to just a pair of sky blue panties that match my eyes.)[/c]");
	_dialog("[c=#ff00ff](I finish with a girlish flourish, bending over to give a full view of my butt before I flounce off the stage, my face a red mess as I catch Mireille’ eyes following me all the way backstage.)[/c]");
	_var_set(VAR_EVENT_STRIP_CLUB_THE_STAGE,true);

	_job_instant(JOBNAME_Show_Girl);
	_pause(0.5,0,true);
	_choice(spr_choice_lightgreen,"OK","Job - Show Girl Finish",noone,fa_center,fa_bottom);
	_block();

_label("Job - Show Girl Finish");
	_room_goto_ext(room_scene_main_map,true);

_border(100); // add content before this








	
	