{
    "id": "942400aa-681e-4e05-83b1-a2c72438bd6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particle_shinny_up",
    "eventList": [
        {
            "id": "f5f68bda-8f7f-48a8-946e-71f65f6f889a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "942400aa-681e-4e05-83b1-a2c72438bd6d"
        },
        {
            "id": "dcc8be31-d84d-410a-b239-31d64adb7337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "942400aa-681e-4e05-83b1-a2c72438bd6d"
        },
        {
            "id": "637cc284-12d2-4726-bc02-2c05f0dc69c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "942400aa-681e-4e05-83b1-a2c72438bd6d"
        },
        {
            "id": "c7d34c1f-79b2-4211-84f1-7adb4a010cd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "942400aa-681e-4e05-83b1-a2c72438bd6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}