event_inherited();

_start();
	_placename(PLACENAME_STPlab);
	_main_gui(true);
	_show("BG",spr_bg_STPlab,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_STP_LAB_VISIT) && 
	   (current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon")
		_jump("EVENT_STP_LAB_VISIT");
	//else if !var_get(VAR_EVENT_ASS_LOTION) && 
	//        var_get(VAR_EVENT_SORE_ASS) && 
	//        (current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon")
	//	_jump("EVENT_ASS_LOTION");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Closed");
	else
		_jump("Closed");

_border(100); // add content before this
		
_label("Stay");
	_stay(true,true,false,false);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Morning");
_label("Afternoon");
	_show("sophia",spr_char_Sophia_Neutral,1.8,YNUM_SOPHIA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Sophia:[/c] Willkommen. How can I help you?");
	_jump("Menu");

_label("Evening");
_label("Late Night");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The lab is closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","sophia",FADE_NORMAL);
	_choice(spr_choice_s,"Energy Drink\r(Cost: $15)","Energy Drink Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Bubble Butt Enhancer\r(Cost: $50)","Bubble Butt Enhancer Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Blossom! The lotion\r(Cost: $100)","Blossom! The lotion Intro",noone,fa_right,fa_center);
	//if var_get(VAR_EVENT_ASS_LOTION)
	//	_choice(spr_choice_s,"“So Zore” Lotion\r(Cost: $150)","“So Zore” Lotion Intro",noone,fa_right,fa_center);
	//else
		_void();
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Energy Drink Intro");
	_change("sophia",spr_char_Sophia_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Energy Drink, if you’re feeling tired, and need a bit of extra energy for your daily activities.... this is for you!");
	_dialog("[c=#ff3333]Sophia:[/c] Don’t abuse it, nothing is better than a good night of sleeping.");
	if var_get(VAR_HP) >= 10 && var_get(VAR_CASH) >= 15
		_jump("Energy Drink Confirm");
	else
		_jump("Energy Drink Fail");

_label("Energy Drink Confirm");
	_choice(spr_choice_s,"Yes","Energy Drink Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Buy an Energy Drink? (Cost: $15)[/c]",false);
	_block();
	
_label("Energy Drink Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough HP or energy.[/c]");
	_jump("Menu");

_label("Energy Drink Confirm Yes");
	_var_add(VAR_HP,-10);
	_var_add(VAR_CASH,-15);
	_var_add_ext(VAR_ENERGY,50,0,var_get(VAR_ENERGY_MAX)+max_energy_bonus());
	_jump("Menu");

_border(100); // add content before this

_label("Bubble Butt Enhancer Intro");
	_change("sophia",spr_char_Sophia_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Bubble Butt Enhancer designed to maintain body mass, but if you run around too much you’re will end up with skinny legs.");
	_change("sophia",spr_char_Sophia_Sad,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Taking Bubble Butt Enhancer vithout regular exercises vill make you fat. Take care.");
	if  var_get(VAR_CASH) >= 50
		_jump("Bubble Butt Enhancer Confirm");
	else
		_jump("Bubble Butt Enhancer Fail");

_label("Bubble Butt Enhancer Confirm");
	_choice(spr_choice_s,"Yes","Bubble Butt Enhancer Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Buy a Bubble Butt Enhancer? (Cost: $50)[/c]",false);
	_block();
	
_label("Bubble Butt Enhancer Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");
	_jump("Menu");

_label("Bubble Butt Enhancer Confirm Yes");
	_var_add(VAR_CASH,-50);
	_var_set(VAR_BODY_TYPE,"Pear Shape");
	_jump("Menu");

_border(100); // add content before this

_label("Blossom! The lotion Intro");
	_change("sophia",spr_char_Sophia_Naught,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Blossom Lotion enhances your bust.  It vill give cup size in 5 days.");
	_change("sophia",spr_char_Sophia_Sad,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Inside the each bottle you have 5 small sachets. Apply the content of a sachet on your chest once a day, for 5 days in a row. Do not skip a day, or it’s going to ruin the medizin cycle.");
	if var_get(VAR_CASH) >= 100
		_jump("Blossom! The lotion Confirm");
	else
		_jump("Blossom! The lotion Fail");

_label("Blossom! The lotion Confirm");
	_choice(spr_choice_s,"Yes","Blossom! The lotion Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Buy a 'Blossom! The lotion'? (Cost: $100)[/c]",false);
	_block();
	
_label("Blossom! The lotion Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");
	_jump("Menu");

_label("Blossom! The lotion Confirm Yes");
	_var_add(VAR_CASH,-100);
	_add_item(ITEM_Lotion);
	_add_item(ITEM_Lotion);
	_add_item(ITEM_Lotion);
	_add_item(ITEM_Lotion);
	_add_item(ITEM_Lotion);

	_draw_item_popup(spr_item_lotion_popup);
	_dialog("[c=#FFFF00]You got the lotion (5 days).[/c]");
	_jump("Menu");

_border(2); // add content before this

_label("“So Zore” Lotion Intro");
	//_change("sophia",spr_char_Sophia_Naught,FADE_NORMAL);
	//_dialog("[c=#ff3333]Sophia:[/c] Blossom Lotion enhances your bust.  It vill give cup size in 5 days.");
	//_change("sophia",spr_char_Sophia_Sad,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Use this lotion if you get zore arsch. Use it once a day for three days, and you’ll be ready for the next wrack… Well you besser not over do it.");
	if var_get(VAR_CASH) >= 150
		_jump("“So Zore” Lotion Confirm");
	else
		_jump("“So Zore” Lotion Fail");

_label("“So Zore” Lotion Confirm");
	_choice(spr_choice_s,"Yes","“So Zore” Lotion Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Buy a “So Zore” Lotion? (Cost: $150)[/c]",false);
	_block();
	
_label("“So Zore” Lotion Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");
	_jump("Menu");

_label("“So Zore” Lotion Confirm Yes");
	_var_add(VAR_CASH,-150);
	_add_item(ITEM_So_Zore_Lotion);
	_add_item(ITEM_So_Zore_Lotion);
	_add_item(ITEM_So_Zore_Lotion);

	_draw_item_popup(spr_item_sozore_lotion_popup);
	_dialog("[c=#FFFF00]You got “So Zore” Lotion (3 days).[/c]");
	_jump("Menu");

_border(98); // add content before this

_label("Talk");
	_change("sophia",spr_char_Sophia_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] I’m glad you like our products. We hope to develop new formulas soon.");
	_jump("Menu");

_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main hall?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_STP_LAB_VISIT");
	_var_set(VAR_EVENT_STP_LAB_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,0.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("sophia",spr_char_Sophia_Neutral,1.8,YNUM_SOPHIA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_if(var_get(VAR_AFF_SOPHIA)<1,_var_set,VAR_AFF_SOPHIA,1);
	_dialog("*: Willkommen to my store! How can I help you?");
	_dialog("*: Oh you’re the new member.");
	_dialog("[c=#ff3333]Sophia:[/c] Mein name is Sophia Verukta, I’m pleased to meet you! I run this store!");
	_dialog("[c=#ff3333]Sophia:[/c] Hier we sell supplements that aid transsexuellen reassignment, medizin and other body transformation items.");
	
	_change("sophia",spr_char_Sophia_Naught,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Every produkt sold hier helps our STP lab’s research and development of new medizin.");
	
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] This is so cool!");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] And expensive…");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](I’ve got my work cut out for me, that’s for sure.  Better keep my hand tight on my cash or Chris will spend it all here.)[/c]");
	
	_change("sophia",spr_char_Sophia_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] By the way, our research group is always looking for voluntaries for testing new produkts and report the results of our current ones. Would you like to be a voluntary?");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] What do you think James?");
	
	_choice(spr_choice,"“Sounds risky. The reports sound useful though, assuming that’s free...”","EVENT_STP_LAB_VISIT_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“If it’s in the budget, we’ll go for it.  We just need to be careful.”","EVENT_STP_LAB_VISIT_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_STP_LAB_VISIT_CHOICE_1_1");
	_var_add_ext(VAR_AFF_SOPHIA,-1,1,4);
	_jump("EVENT_STP_LAB_VISIT_after_CHOICE_1");
	
_label("EVENT_STP_LAB_VISIT_CHOICE_1_2");
	_var_add_ext(VAR_AFF_SOPHIA,1,1,4);
	_jump("EVENT_STP_LAB_VISIT_after_CHOICE_1");
	
_label("EVENT_STP_LAB_VISIT_after_CHOICE_1");
	_dialog("[c=#ff00ff]Chris:[/c] Sophia, let me know when you have a spot for me.");
	_dialog("[c=#ff3333]Sophia:[/c] Danke, your support is appreciated.  Stay tuned for new products.");
	
	_jump("Menu");

_border(2); // add content before this
/*
_label("EVENT_ASS_LOTION");
	_choice(spr_choice,"Continue","EVENT_ASS_LOTION_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris follow Penny’s advice to talk with Sophia Veruckta.[/c]",false);
	_block();
	
_label("EVENT_ASS_LOTION_START");
	_void();//_var_set(VAR_EVENT_ASS_LOTION,true);
	_show("sophia",spr_char_Sophia_Neutral,1.8,YNUM_SOPHIA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Blush,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Sophia:[/c] Wunderbar! You’re a level huh? I’ve never met a newcomer that could be a level 2.");
	_dialog("[c=#ff00ff]Chris:[/c] A bit too hardcore but I made it.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] By the way, what level are you Veruckta?");
	_dialog("[c=#ff3333]Sophia:[/c] ...");
	_change("sophia",spr_char_Sophia_Naught,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] ... !!");
	_change("sophia",spr_char_Sophia_Crazy,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Your situationen can’t be overlooked, please get in position.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hey! Don’t dodge my question!");
	_dialog("[c=#ff00ff]Chris:[/c] …but fine …Alright.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](No idea what are they talking about and I’m not curious either.)[/c]");
	_dialog("[c=#6678ff](I’ll just listen how the talk is going to develop.)[/c]");
	_change("sophia",spr_char_Sophia_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] Use this. This is “So Zore” lotion, it’s an experimental medicine developed by STP Lab.");
	_change("sophia",spr_char_Sophia_Crazy,FADE_NORMAL);
	_dialog("[c=#ff3333]Sophia:[/c] If it proves effective It will be available on our store.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff](Are they ignoring me… they are totally ignoring me!)[/c]");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_sophia_lotion_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#ff00ff]Chris:[/c] This is embarrassing.");
	_dialog("[c=#ff3333]Sophia:[/c] No reason to be embarrassed I’m a professional doctor.");
	_dialog("[c=#ff00ff]Chris:[/c] The embarrassing part is to be butt wrecked… It’s defeat for me!");
	_dialog("[c=#ff3333]Sophia:[/c] Tee Hee! You will get used to it soon.");
	
	_change("CG",spr_hentai_cg_sophia_lotion_2,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#ff00ff]Chris:[/c] Oh… ouch… ugh… it hurts…");
	_dialog("[c=#ff3333]Sophia:[/c] Soon it will start to feel gut.");
	_dialog("[c=#ff00ff]Chris:[/c] Haaa… you sound like my first boyfriend…");
	_dialog("[c=#ff00ff]Chris:[/c] …but yeah… it does feel good.");
	_dialog("[c=#ff3333]Sophia:[/c] Looks like Penny didn’t cum inside you, though. Then again, she never does.");
	_dialog("[c=#ff00ff]Chris:[/c] I think she’s a sadist. She gets off by splitting people in two.");
	
	_change("CG",spr_hentai_cg_sophia_lotion_3,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#ff00ff]Chris:[/c] Can I stay here a little until I relax?");
	_dialog("[c=#ff3333]Sophia:[/c] Ja, you can stay hier all you want.");
	_dialog("[c=#ff00ff]Chris:[/c] It feels really good.");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_pause(0.5,0,true);
	_execute(time_hour_passed,2);
	
	_show("sophia",spr_char_Sophia_Neutral,1.8,YNUM_SOPHIA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	
	_dialog("[c=#ff00ff]Chris:[/c] 3 days, I may ask James to help me <3");
	_dialog("[c=#ff3333]Sophia:[/c] Ja I bet he will be glad to help you.");
	
	_add_item(ITEM_So_Zore_Lotion);
	_add_item(ITEM_So_Zore_Lotion);
	_add_item(ITEM_So_Zore_Lotion);
	_draw_item_popup(spr_item_sozore_lotion_popup);
	_dialog("[c=#FFFF00]You got “So Zore” Lotion (3 days).[/c]");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] ...");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] THEY ACTED LIKE I WASN’T EVEN HERE!!!");
	_var_set(VAR_EVENT_ASS_LOTION,true);
	
	if current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon"
		_jump("Menu");
	else
		_jump("Closed");
*/
_border(98); // add content before this

