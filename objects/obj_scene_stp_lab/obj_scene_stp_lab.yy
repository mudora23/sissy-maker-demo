{
    "id": "91c72ffb-2deb-4c3c-acc8-31f686ac624c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_lab",
    "eventList": [
        {
            "id": "a7de780f-a2b8-4c4a-90d5-1a8b073ad905",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91c72ffb-2deb-4c3c-acc8-31f686ac624c"
        },
        {
            "id": "911412e5-e959-4d8c-b087-ea29ee1b6fae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "91c72ffb-2deb-4c3c-acc8-31f686ac624c"
        },
        {
            "id": "24402cc3-9657-406b-bdc4-6bdc2946c436",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "91c72ffb-2deb-4c3c-acc8-31f686ac624c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}