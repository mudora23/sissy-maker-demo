event_inherited();

_start();
	_placename(PLACENAME_kanesResidence);
	_main_gui(true);
	_show("BG",spr_bg_kanesResidence,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_if(var_get(VAR_AFF_KANE)<1,_var_set,VAR_AFF_KANE,1);
	
	if var_get(VAR_JOB_BABYSITTING) == 0
		_jump("JOB_BABYSITTING_FIRST");
	//else if var_get(VAR_JOB_BABYSITTING) >= 10 && percent_chance(60) && !var_get(VAR_EVENT_KANES_CANDY_CANE)
	//	_jump("EVENT_KANES_CANDY_CANE");
	//else if var_get(VAR_JOB_BABYSITTING) >= 10 && percent_chance(30) && var_get(VAR_EVENT_KANES_CANDY_CANE)
	//	_jump("EVENT_KANES_CANDY_CANE_RE");
	else
		_jump("Normal");

_border(100); // add content before this

_label("JOB_BABYSITTING_FIRST");
	_play_sfx(sfx_knocking);
	_pause(1,0,true);
	_dialog("[c=#ff00ff]Chris:[/c] Hi! My name is Chris Ashee, I’m assigned for babysitting.");
	_dialog("[c=#39f979]Mr. Kane:[/c] Nice to meet you, Chris! We’re glad you came. Please come on in.");
	_dialog("[c=#39f979]Mrs. Kane:[/c] Denise, Douglas! Come here, this Chris, she will take care of you.");
	_dialog("[c=#39f979]Denise:[/c] Hiiiii!");
	_dialog("[c=#39f979]Douglas:[/c] Hi!");
	_dialog("[c=#39f979]Mrs. Kane:[/c] Don’t worry they are not much trouble I promise.");
	_dialog("[c=#ff00ff]Chris:[/c] Such nice kids!");
	_dialog("[c=#ff00ff](I know they are little devils I know)[/c]");
	_dialog("[c=#39f979]Mr. Kane:[/c] You two behave, we’ll be back soon. Here Chris this is our phone number.");
	_dialog("[c=#39f979]Mrs. Kane:[/c] Chris, they go to bed at 10:00pm. Keep Denise off the biscuits pot.");
	_dialog("[c=#ff00ff]Chris:[/c] Right!");
	_dialog("[c=#39f979]Mrs. Kane:[/c] Dinner is read in the fridge, you find snacks in the cabinet if you want.");
	_dialog("[c=#ff00ff]Chris:[/c] Thank you Mrs. Kane.");
	_dialog("[c=#ff00ff]Chris:[/c] So what do you wanna play?");
	_dialog("[c=#39f979]Douglas:[/c] Video games!");
	_dialog("[c=#39f979]Denise:[/c] I want to watch Priiiincess Knight!");
	_dialog("[c=#ff00ff]Chris:[/c] Alright, alright!");
	
	_play_sfx(sfx_kane_house);
	_job(JOBNAME_Baby_Sitting,"Finished");
	_block();
	
_border(100); // add content before this


_label("Normal");
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](Here I go again!)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff](Those brats... I should have bought a taser!)[/c]");
	_hide("chris",FADE_NORMAL);
	_play_sfx(sfx_knocking);
	_pause(1,0,true);
	_dialog("[c=#39f979]Mr. Kane:[/c] I thanks for coming!");
	_dialog("[c=#ff00ff]Chris:[/c] My pleasure!");
	
	_play_sfx(sfx_kane_house);
	_job(JOBNAME_Baby_Sitting,"Finished");
	_block();

_border(100); // add content before this

_label("Finished");
	_stop_sfx(sfx_kane_house);
	_choice(spr_choice_lightgreen,"OK",noone,room_scene_main_map,fa_center,fa_bottom);
	_block();
	

_border(2); // add content before this
/*
_label("EVENT_KANES_CANDY_CANE");
	_choice(spr_choice,"Continue","EVENT_KANES_CANDY_CANE_START",noone,fa_center,fa_center);
	if var_get(VAR_ANUS) <= -2
		_choice(spr_choice,"Cancel","EVENT_KANES_CANDY_CANE_CANCEL",noone,fa_center,fa_center);
	else
		_void();
	_dialog("[c=#FFFF00]Chris is required for babysitting at Kane’s residence again, but there are no kids to be taken care of… only Mr. Kane, home alone…[/c]",false);
	_block();

_label("EVENT_KANES_CANDY_CANE_CANCEL");
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](Here I go, into the mouth of hell again!)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff](Those friggin’ brats. I should have bought a taser!)[/c]");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh! Uh- Mr. Kane!");
	_dialog("[c=#ff00ff]Chris:[/c] G-good afternoon! Where is everybody?");
	_show("kane",spr_char_Kane_Smiling,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] My wife and kids went to their grandmother’s house.");
	_change("kane",spr_char_Kane_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] It’s just you and me.");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff](I have a bad feeling about this. And my butt can’t take it anymore.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Um… Sorry Mr. Kane, I must have come here by mistake! You know, I’m so used to working here I just came without thinking…");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I think I’m actually assigned to work somewhere else today.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] See you! Buh-Bye!");
	_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
	_room_goto_ext(room_scene_main_map,FADE_NORMAL);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_START");
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](Here I go, into the mouth of hell again!)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff](Those friggin’ brats. I should have bought a taser!)[/c]");
	_hide_all_except("BG",FADE_NORMAL);
	_play_sfx(sfx_knocking);
	_pause(1,0,true);
	_show("kane",spr_char_Kane_Smiling,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Thanks for coming, Chris.");
	_show("chris",spr_char_Chris_Blush,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh! Uh- Mr. Kane!");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] G-good afternoon! Where is everybody?");
	_change("kane",spr_char_Kane_Smiling,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] My wife and kids went to their grandmother’s house.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh… wait! then Why did you call me here?");
	_change("kane",spr_char_Kane_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] ...");
	_dialog("[c=#39f979]Mr. Kane:[/c] Please come in.");
	
	_hide_all_except("BG",FADE_NORMAL);
	_pause(1,0,true);
	_change("BG",spr_bg_kanesResidenceInside,FADE_NORMAL);
	_pause(0.5,0,true);
	
	_show("chris",spr_char_Chris_Neutral,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Horny,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](That look he just gave me… it said everything. Until now I’ve never seen Mr. Kane as the handsome man he is.)[/c]");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff](There is something really charming about his fatherly presence. It makes me feel secure, protected.)[/c]");
	_dialog("[c=#ff00ff](It’s like I can trust him with everything… Come to think of it, Mrs. Kane must trust him too… Shit I’ve got a bad feeling about this.)[/c]");
	_dialog("[c=#FFFF00]“… you’re a sissy slut…”[/c]",false);
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] !");
	_dialog("[c=#ff00ff](What was that?)[/c]");
	_dialog("[c=#ff00ff](All of a sudden, I’m all ready to buckle and take his cock in my mouth.)[/c]");
	_dialog("[c=#ff00ff](Just thinking about it made my cock throb under my undies…)[/c]");
	_change("kane",spr_char_Kane_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Judging from the way you’re staring at my crotch, I believe you know why I called you here.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] *slurp* Yes… I want your cock in my mouth.");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_mr_kane_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#39f979]Mr. Kane:[/c] That’s why I love STP Sissies: they never disappoint!");
	_dialog("[c=#ff00ff]Chris:[/c] ‘We never disappoint…’ I like the sound of that!");
	_dialog("[c=#ff00ff](I don’t want to let him down. I want his thing up and hard inside my mouth.)[/c]");
	_dialog("[c=#ff00ff](We don’t spend much time at all with teasing and naughty talk. His cock was still limp when I took it in. He’s all business!)[/c]");
	_dialog("[c=#ff00ff](I put all my effort to give Mr. Kane the best blowjob he’s ever had. I want to please him.)[/c]");
	_dialog("[c=#ff00ff](His cock became hard as rock after a minute or so; I couldn’t fit it in my mouth anymore. It was around the time I started to choke on the chode that I realized the trouble I was getting into…)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] *slurp* Mr. Kane, please be gentle <3!");
	_dialog("[c=#39f979]Mr. Kane:[/c] None of my sissies have ever complained before. Are you a greenhorn?");
	_dialog("[c=#ff00ff]Chris:[/c] I- uh… Yeah. I just started. *smack*");
	_dialog("[c=#ff00ff](Fearing what was about to come, I made sure to suck and lick his dick in the sloppiest manner I could manage.)[/c]");
	
	_change("CG",spr_hentai_cg_mr_kane_2,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#ff00ff]Chris:[/c] I couldn’t imagine a man of as decent as you Mr Kane, involved with little sissy girls like me.");
	_dialog("[c=#39f979]Mr. Kane:[/c] Little girls like you love my thick candy cane. And here it goes, up your pink asshole!");
	_dialog("[c=#ff00ff]Chris:[/c] Ahhhhh! S-so hard and thick!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Oooh you’re clinging so tight to my cock with your asshole. You’re stretched to the limit. It feels amazing.");
	_dialog("[c=#ff00ff]Chris:[/c] Yes…. Do it! P-push it further! Ohh shit!");
	_dialog("[c=#ff00ff](Oh yeah I got Mr. Kane’s Candy Cane all inside, So big and hard!)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Feels amazing… woah! Wait, s-slow down!");
	_dialog("[c=#ff00ff](Ah, I need to get used to it’s girth.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Please Mr Kane Let me move. Pace it down. Slow");
	_dialog("[c=#ff00ff]Chris:[/c] Ahh alright. Like this. Shit! That’s a lot of cock!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Chris, let me pound your untrained sissy ass until I cum inside you? I’ll pound you really hard and really fast… It may hurt a little, but soon you’ll get used to it.");
	_dialog("[c=#39f979]Mr. Kane:[/c] I even pay you a little extra… what do you think?");
	
	_choice(spr_choice,"“Don’t do that please! You’re gonna wreck my bumhole!”","EVENT_KANES_CANDY_CANE_START_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“Ahhh… fucking do it!”","EVENT_KANES_CANDY_CANE_START_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();

_label("EVENT_KANES_CANDY_CANE_START_CHOICE_1_1");
	_dialog("[c=#ff00ff](We fucked for like an hour or so, in a slow and loving pace.)[/c]");
	_dialog("[c=#ff00ff](Then I felt Mr Kane’s candy cane losing its strength.)[/c]");
	_dialog("[c=#ff00ff](He may have cum, but I didn’t notice. I have a bad feeling about this.)[/c]");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_var_set(VAR_TIME,22);
	_pause(0.5,0,true);
	
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Frustrated,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] *Sigh*");
	_dialog("[c=#ff00ff]Chris:[/c] Uh… Mr. Kane? Did you cum?");
	_dialog("[c=#39f979]Mr. Kane:[/c] Yeah… kinda.");
	_dialog("[c=#ff00ff](My half-wrecked ass in the air, I turned to look at Mr. Kane, he was already buckling his pants.)[/c]");
	_dialog("[c=#ff00ff](Shit I must have messed it up somehow.)[/c]");
	_dialog("[c=#39f979]Mr. Kane:[/c] My family will be back soon. Please get ready to go.");
	_dialog("[c=#ff00ff](A sudden fear leapt across my spine. I wanted to feel this again.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] I - I’m sorry I wasn’t as good as you expected. Please give me another chance!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Yeah I’ll see about it in the future.");
	
	_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
	_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
	_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
	_room_goto_ext(room_scene_main_map,true);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_START_CHOICE_1_2");

	if var_get(VAR_CUP_SIZE) > 0
		_change("CG",spr_hentai_cg_mr_kane_4,FADE_SLOW);
	else
		_change("CG",spr_hentai_cg_mr_kane_3,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#39f979]Mr. Kane:[/c] Good! Take it like the sissy slut you are!");
	_dialog("[c=#ff00ff]Chris:[/c] AAAAAAHHHHHHHH!");
	_dialog("[c=#39f979]Mr. Kane:[/c] That’s how I like it! Take it in your sissy pussy!");
	_dialog("[c=#ff00ff]Chris:[/c] FUUUUCK!");
	_dialog("[c=#ff00ff](I felt like I would pass out! The only thing I could hear was his crotch slapping against my butt at a manic speed.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Please cum inside me! Fill me up!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Oh, I’m not done with you yet. There is more to come!");
	_dialog("[c=#ff00ff]Chris:[/c] AAAAAAAAAAHHHHHHH SHIIIIIIT!");
	_dialog("[c=#ff00ff](Mr. Kane wrapped his hands around my waist and his thrusts gained even more speed and depth.)[/c]");
	_dialog("[c=#ff00ff](He was hard as rock.)[/c]");
	_dialog("[c=#ff00ff](He was so close.)[/c]");
	_dialog("[c=#39f979]Mr. Kane:[/c] AAHHH AAAAHHH I’m gonna cummmmm");
	_dialog("[c=#39f979]Mr. Kane:[/c] URRRRGHHH!!!!");
	_dialog("[c=#ff00ff](When Mr. Kane took out his limp cock which made a pop sound followed by a gush of semen. It poured out of my asshole like a waterfall.)[/c]");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_var_set(VAR_TIME,22);
	_pause(0.5,0,true);
	
	_show("chris",spr_char_Chris_Horny,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Horny,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Holy cow, I’m spent! You’re such a good sissy.");
	_change("kane",spr_char_Kane_Frustrated,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] My god, look at the time! We were having so much fun i didn’t even notice.");
	_change("kane",spr_char_Kane_Smiling,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Let’s clean up this mess before my wife gets back.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Thank you Mr. Kane. Until next time.");
	
	_hide_all_except("BG",FADE_NORMAL);
	_pause(1,0,true);
	_change("BG",spr_bg_kanesResidence,FADE_NORMAL);
	_pause(0.5,0,true);
	
	if var_get(VAR_ANUS) <= 2
	{
		_var_add_ext(VAR_ANUS,-3,-3,4);
		_var_add(VAR_CASH,450);
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_show("chris",spr_char_Chris_Tired,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
		_dialog("[c=#ff00ff](The way home was hard and tortuous with my wobbly legs, Each step sent a stinging pain in my overused ass… but I’m happy.)[/c]");
		_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
		_dialog("[c=#ff00ff]Chris:[/c] Happy like a sissy slut.");
		_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
		_var_add(VAR_EVENT_KANES_CANDY_CANE2,1);
		_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_room_goto_ext(room_scene_main_map,true);
		_block();
	}
	else
	{
		_void();
		_var_add(VAR_CASH,450);
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_show("chris",spr_char_Chris_Horny,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
		_dialog("[c=#ff00ff](Pfft! He bought my “virgin” role play! Gullible Mr. Kane… It drove him crazy to fuck my ass, hahaha! I’m so happy!)[/c]");
		_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
		_dialog("[c=#ff00ff]Chris:[/c] Happy like a sissy slut.");
		_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
		_var_add(VAR_EVENT_KANES_CANDY_CANE2,1);
		_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_room_goto_ext(room_scene_main_map,true);
		_block();
	}
	*/
_border(2); // add content before this	
	/*
_label("EVENT_KANES_CANDY_CANE_RE");
	_choice(spr_choice,"Go for it!","EVENT_KANES_CANDY_CANE_RE_START",noone,fa_center,fa_center);
	_choice(spr_choice,"Maybe another day.","EVENT_KANES_CANDY_CANE_RE_CANCEL",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Mr. Kane has an “extra” job for Chris again.[/c]",false);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_RE_CANCEL");	
	_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
	_room_goto_ext(room_scene_main_map,true);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_RE_START");
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff](Here I go, into the mouth of hell again!)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff](Those friggin’ brats. I should have bought a taser!)[/c]");
	_hide_all_except("BG",FADE_NORMAL);
	_play_sfx(sfx_knocking);
	_pause(1,0,true);
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Smiling,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Thanks for coming again, Chris.");
	_change("kane",spr_char_Kane_Horny,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] ...Or should I say “Sissy-Slut?”");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Mr. Kane, you dog!");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] ...Where is everybody?");
	_dialog("[c=#39f979]Mr. Kane:[/c] I think you know better than to ask that now, don’t you? *Smack!*");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh! M-mr. Kane, my tushie!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Get inside and bend over, my Candy Cane has been hard and waiting all day for you.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Haha! Horray!");
	
	_hide_all_except("BG",FADE_NORMAL);
	_pause(1,0,true);
	_change("BG",spr_bg_kanesResidenceInside,FADE_NORMAL);
	_pause(0.5,0,true);
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_mr_kane_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#39f979]Mr. Kane:[/c] Mmmh, yeah, get your mouth back on this fat cock, you Sissy!");
	_dialog("[c=#ff00ff]Chris:[/c] Mmph, *slurp* I love it when you talk dirty, Mr. Kane.");
	_dialog("[c=#ff00ff](We don’t spend much time at all with teasing and naughty talk. He just jams his rock-hard penis into my mouth and I get to sucking.)[/c]");
	_dialog("[c=#ff00ff](I put all my effort to give Mr. Kane the best blowjob he’s ever had. I want to please him.)[/c]");
	_dialog("[c=#ff00ff](I can’t fit it in my mouth fully, though I take in a bit more than last time. As soon I start to choke on the chode I realize the trouble I’m getting myself into once again…)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] *slurp* Mr. Kane, please be gentle <3!");
	_dialog("[c=#39f979]Mr. Kane:[/c] You don’t want gentle, do you slut?");
	_dialog("[c=#ff00ff]Chris:[/c] He smacks my ass so hard I shiver. I return to my docile position of worshipping his manhood.");
	_dialog("[c=#ff00ff](Fearing what is about to come, I make sure to suck and lick his dick in the sloppiest manner I can manage.)[/c]");
	
	_change("CG",spr_hentai_cg_mr_kane_2,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#ff00ff]Chris:[/c] Before today, I couldn’t imagine a man as decent as you Mr Kane, involved with little sissy girls like me.");
	_dialog("[c=#39f979]Mr. Kane:[/c] Little girls like you love my thick candy cane. And here it goes, up your pink asshole!");
	_dialog("[c=#ff00ff]Chris:[/c] Ahhhhh! S-so hard and thick!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Oooh you’re clinging so tight to my cock with your asshole. You’re stretched to the limit. It feels amazing.");
	_dialog("[c=#ff00ff]Chris:[/c] Yes…. Do it! P-push it further! Ohh shit!");
	_dialog("[c=#ff00ff](Oh yeah I got Mr. Kane’s Candy Cane all inside, So big and hard!)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Feels amazing… woah! Wait, s-slow down!");
	_dialog("[c=#ff00ff](Ah, I need to get used to it’s girth.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Please Mr Kane Let me move. Pace it down. Slow");
	_dialog("[c=#ff00ff]Chris:[/c] Ahh alright. Like this. Shit! That’s a lot of cock!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Chris, let me pound your untrained sissy ass until I cum inside you? I’ll pound you really hard and really fast… It may hurt a little, but soon you’ll get used to it.");
	_dialog("[c=#39f979]Mr. Kane:[/c] I even pay you a little extra… what do you think?");
	
	_choice(spr_choice,"“Don’t do that please! You’re gonna wreck my bumhole!”","EVENT_KANES_CANDY_CANE_RE_START_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“Ahhh… fucking do it!”","EVENT_KANES_CANDY_CANE_RE_START_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_RE_START_CHOICE_1_1");
	_dialog("[c=#ff00ff](We fucked for like an hour or so, in a slow and loving pace.)[/c]");
	_dialog("[c=#ff00ff](Then I felt Mr Kane’s candy cane losing its strength.)[/c]");
	_dialog("[c=#ff00ff](He may have cum, but I didn’t notice. I have a bad feeling about this.)[/c]");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_var_set(VAR_TIME,22);
	_pause(0.5,0,true);
	
	_show("chris",spr_char_Chris_Worry,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Frustrated,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] *Sigh*");
	_dialog("[c=#ff00ff]Chris:[/c] Uh… Mr. Kane? Did you cum?");
	_dialog("[c=#39f979]Mr. Kane:[/c] Yeah… kinda.");
	_dialog("[c=#ff00ff](My half-wrecked ass in the air, I turned to look at Mr. Kane, he was already buckling his pants.)[/c]");
	_dialog("[c=#ff00ff](Shit I must have messed it up somehow.)[/c]");
	_dialog("[c=#39f979]Mr. Kane:[/c] My family will be back soon. Please get ready to go.");
	_dialog("[c=#ff00ff](A sudden fear leapt across my spine. I wanted to feel this again.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] I - I’m sorry I wasn’t as good as you expected. Please give me another chance!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Yeah I’ll see about it in the future.");
	
	_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
	_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
	_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
	_room_goto_ext(room_scene_main_map,true);
	_block();
	
_label("EVENT_KANES_CANDY_CANE_RE_START_CHOICE_1_2");

	if var_get(VAR_CUP_SIZE) > 0
		_change("CG",spr_hentai_cg_mr_kane_4,FADE_SLOW);
	else
		_change("CG",spr_hentai_cg_mr_kane_3,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#39f979]Mr. Kane:[/c] Good! Take it like the sissy slut you are!");
	_dialog("[c=#ff00ff]Chris:[/c] AAAAAAHHHHHHHH!");
	_dialog("[c=#39f979]Mr. Kane:[/c] That’s how I like it! Take it in your sissy pussy!");
	_dialog("[c=#ff00ff]Chris:[/c] FUUUUCK!");
	_dialog("[c=#ff00ff](I felt like I would pass out! The only thing I could hear was his crotch slapping against my butt at a manic speed.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Please cum inside me! Fill me up!");
	_dialog("[c=#39f979]Mr. Kane:[/c] Oh, I’m not done with you yet. There is more to come!");
	_dialog("[c=#ff00ff]Chris:[/c] AAAAAAAAAAHHHHHHH SHIIIIIIT!");
	_dialog("[c=#ff00ff](Mr. Kane wrapped his hands around my waist and his thrusts gained even more speed and depth.)[/c]");
	_dialog("[c=#ff00ff](He was hard as rock.)[/c]");
	_dialog("[c=#ff00ff](He was so close.)[/c]");
	_dialog("[c=#39f979]Mr. Kane:[/c] AAHHH AAAAHHH I’m gonna cummmmm");
	_dialog("[c=#39f979]Mr. Kane:[/c] URRRRGHHH!!!!");
	_dialog("[c=#ff00ff](When Mr. Kane took out his limp cock which made a pop sound followed by a gush of semen. It poured out of my asshole like a waterfall.)[/c]");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_var_set(VAR_TIME,22);
	_pause(0.5,0,true);
	
	_show("chris",spr_char_Chris_Horny,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("kane",spr_char_Kane_Horny,2,YNUM_KANE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Holy cow, I’m spent! You’re such a good sissy.");
	_change("kane",spr_char_Kane_Frustrated,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] My god, look at the time! We were having so much fun i didn’t even notice.");
	_change("kane",spr_char_Kane_Smiling,FADE_NORMAL);
	_dialog("[c=#39f979]Mr. Kane:[/c] Let’s clean up this mess before my wife gets back.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Thank you Mr. Kane. Until next time.");
	
	_hide_all_except("BG",FADE_NORMAL);
	_pause(1,0,true);
	_change("BG",spr_bg_kanesResidence,FADE_NORMAL);
	_pause(0.5,0,true);
	
	if var_get(VAR_ANUS) <= 2
	{
		_var_add_ext(VAR_ANUS,-3,-3,4);
		_var_add(VAR_CASH,450);
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_show("chris",spr_char_Chris_Blush,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
		_dialog("[c=#ff00ff]Chris:[/c] Thank you Mr. Kane. Until next time.");
		_void();
		_void();
		_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
		_var_add(VAR_EVENT_KANES_CANDY_CANE2,1);
		_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_room_goto_ext(room_scene_main_map,true);
		_block();
	}
	else
	{
		_void();
		_var_add(VAR_CASH,450);
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_show("chris",spr_char_Chris_Horny,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
		_dialog("[c=#ff00ff](Mmm… I swear, Mr. Kane becomes more and more rough every time he fucks me. He’s like a car piston!)[/c]");
		_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
		_dialog("[c=#ff00ff]Chris:[/c] I should hit him up tomorrow!");
		_var_add(VAR_EVENT_KANES_CANDY_CANE,1);
		_var_add(VAR_EVENT_KANES_CANDY_CANE2,1);
		_var_add_ext(VAR_ENERGY,-40,0,var_get(VAR_ENERGY));
		_execute(jobs_list_delete_job,JOBNAME_Baby_Sitting);
		_room_goto_ext(room_scene_main_map,true);
		_block();
	}
*/
_border(96); // add content before this

