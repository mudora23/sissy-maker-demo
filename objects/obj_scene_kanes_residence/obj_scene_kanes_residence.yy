{
    "id": "46e7c452-6324-4670-b7b7-cc3cbce394f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_kanes_residence",
    "eventList": [
        {
            "id": "26f3c050-22e2-44c8-a2b0-ee22f9242755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46e7c452-6324-4670-b7b7-cc3cbce394f6"
        },
        {
            "id": "da264459-461c-4a32-a802-4a917a15b5fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "46e7c452-6324-4670-b7b7-cc3cbce394f6"
        },
        {
            "id": "1da03394-4d74-482a-af0e-39717f3634c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "46e7c452-6324-4670-b7b7-cc3cbce394f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}