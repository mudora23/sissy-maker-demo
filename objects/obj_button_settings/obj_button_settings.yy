{
    "id": "521e6aca-4184-4e75-b5b4-ca94aee18ea5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_settings",
    "eventList": [
        {
            "id": "3759e92b-4fd4-4355-8e53-391a6a6c674d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "521e6aca-4184-4e75-b5b4-ca94aee18ea5"
        },
        {
            "id": "d775a3e6-2958-4bf4-bc2a-313f068ca909",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "521e6aca-4184-4e75-b5b4-ca94aee18ea5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "visible": true
}