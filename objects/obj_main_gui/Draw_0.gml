// set alpha
if obj_control.var_map[? VAR_MAIN_GUI_HIDING] image_alpha = approach(image_alpha,0,5*delta_time_sec());
else image_alpha = approach(image_alpha,1,5*delta_time_sec());

draw_set_valign(fa_center);
draw_set_halign(fa_left);
draw_set_color(c_white);

//draw time
draw_set_font(font_clock_24);
draw_sprite_ext(sprite_index,0,0,0,1,1,0,c_white,image_alpha);
draw_text_transformed_color(x+5,y+108, time_0_23_to_time_stamp(var_get(VAR_TIME)),0.7,1,0,c_black,c_black,c_black,c_black,image_alpha);

// draw stats
draw_set_font(font_main_gui);
if var_get(VAR_HP) <= 20 || var_get(VAR_ENERGY) <= 20
	draw_sprite_ext(sprite_index,2,0,0,1,1,0,c_white,image_alpha);
else
	draw_sprite_ext(sprite_index,1,0,0,1,1,0,c_white,image_alpha);
draw_main_gui_text(x+10,y+73,"HP:"+string(round(var_get(VAR_HP)))+"/"+string(round(var_get(VAR_HP_MAX)+max_hp_penalty())));
draw_main_gui_text(x+115,y+73,"Energy:"+string(round(var_get(VAR_ENERGY)))+"/"+string(round(var_get(VAR_ENERGY_MAX)+max_energy_bonus())));
draw_main_gui_text(x+245,y+73,"Cash:$"+string(round(var_get(VAR_CASH))));

// draw place
draw_set_font(font_main_gui_placename);
draw_sprite_ext(sprite_index,3,0,0,1,1,0,c_white,image_alpha);
draw_main_gui_text(x+10,y+37,string(obj_control.var_map[? VAR_MAIN_GUI_PLACENAME])+" - "+string_upper(current_time_get_name()));

draw_reset();


