{
    "id": "74e90356-fa5c-46c3-bb0d-2567cf438bf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main_gui",
    "eventList": [
        {
            "id": "3567a917-20b5-4fc9-8107-7eb49207ec80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74e90356-fa5c-46c3-bb0d-2567cf438bf5"
        },
        {
            "id": "2df90844-6368-4112-ab41-135b1650ac65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "74e90356-fa5c-46c3-bb0d-2567cf438bf5"
        },
        {
            "id": "363c0a86-7ec5-4091-950f-71308b5ab8ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "74e90356-fa5c-46c3-bb0d-2567cf438bf5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6b27a879-4e45-406c-95b5-793ab96e206b",
    "visible": true
}