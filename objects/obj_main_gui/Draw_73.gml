var clicked = obj_control.left_pressed;
stats_page = smooth_approach(stats_page,stats_page_target,SLIDE_FAST*delta_time_sec());

if menu_open == "save"
{

        // BG
        draw_sprite(spr_save_content_bg,-1,0,0);
        
        var xx = 70;
        var yy = 100;
        var file_bg = spr_saveload_files;
        var file_bg_widtht = sprite_get_width(file_bg);
        var file_bg_height = sprite_get_height(file_bg);
        var thumbnailxxoffset = 150;
        var thumbnailyyoffset = 10;
        var datexxoffset = 280;
        var dateyyoffset = 20;
        var delete_bg = spr_save_delete;
        var delete_bg_widtht = sprite_get_width(delete_bg);
        var delete_bg_height = sprite_get_height(delete_bg);
        var delete_xxoffset = 675;
        

        //var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,0/*hover && !obj_control.control_map[? CONTROL_AUTOSAVE]*/,xx,yy);
        draw_sprite(spr_saveload_files,2,xx,yy);
        if sprite_exists(obj_control.save0_thumbnail)
        {
            draw_sprite(obj_control.save0_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save0_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save0_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        /*if sissy_maker_can_save() && hover && clicked && !obj_control.control_map[? CONTROL_AUTOSAVE]
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_save(0);
        }*/
        
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);

        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(0);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,hover,xx,yy);
        draw_sprite(spr_saveload_files,3,xx,yy);
        if sprite_exists(obj_control.save1_thumbnail)
        {
            draw_sprite(obj_control.save1_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save1_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save1_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sissy_maker_can_save() && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_save(1);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(1);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,hover,xx,yy);
        draw_sprite(spr_saveload_files,4,xx,yy);
        if sprite_exists(obj_control.save2_thumbnail)
        {
            draw_sprite(obj_control.save2_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save2_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save2_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sissy_maker_can_save() && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_save(2);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(2);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,hover,xx,yy);
        draw_sprite(spr_saveload_files,5,xx,yy);
        if sprite_exists(obj_control.save3_thumbnail)
        {
            draw_sprite(obj_control.save3_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save3_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save3_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sissy_maker_can_save() && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_save(3);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(3);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,hover,xx,yy);
        draw_sprite(spr_saveload_files,6,xx,yy);
        if sprite_exists(obj_control.save4_thumbnail)
        {
            draw_sprite(obj_control.save4_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save4_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save4_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sissy_maker_can_save() && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_save(4);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(4);
        }
        
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            scene_play_sfx(sfx_button_01);
            obj_control.left_pressed = false;
            menu_open = "";
        }
        
        
        var xx = 600;
        var yy = 65;
        var file_bg = spr_saveload_auto_on_off;
        var file_bg_widtht = sprite_get_width(file_bg);
        var file_bg_height = sprite_get_height(file_bg);
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(file_bg,obj_control.control_map[? CONTROL_AUTOSAVE]*2 + hover,xx,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            obj_control.control_map[? CONTROL_AUTOSAVE] = !obj_control.control_map[? CONTROL_AUTOSAVE];
            sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
        }
        if hover
        {
            if obj_control.control_map[? CONTROL_AUTOSAVE]
                draw_tooltip(xx,yy+30,"Click to disable auto save in the first slot.");
            else
                draw_tooltip(xx,yy+30,"Click to enable auto save in the first slot.");
        }

}
else if menu_open == "load"
{

        // BG
        draw_sprite(spr_load_content_bg,-1,0,0);
    
        var xx = 70;
        var yy = 100;
        var file_bg = spr_saveload_files;
        var file_bg_widtht = sprite_get_width(file_bg);
        var file_bg_height = sprite_get_height(file_bg);
        var thumbnailxxoffset = 150;
        var thumbnailyyoffset = 10;
        var datexxoffset = 280;
        var dateyyoffset = 20;
        var delete_bg = spr_save_delete;
        var delete_bg_widtht = sprite_get_width(delete_bg);
        var delete_bg_height = sprite_get_height(delete_bg);
        var delete_xxoffset = 675;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,sprite_exists(obj_control.save0_thumbnail) && hover,xx,yy);
        draw_sprite(spr_saveload_files,2,xx,yy);
        if sprite_exists(obj_control.save0_thumbnail)
        {
            draw_sprite(obj_control.save0_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save0_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save0_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sprite_exists(obj_control.save0_thumbnail) && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_load(0);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(0);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,sprite_exists(obj_control.save1_thumbnail) && hover,xx,yy);
        draw_sprite(spr_saveload_files,3,xx,yy);
        if sprite_exists(obj_control.save1_thumbnail)
        {
            draw_sprite(obj_control.save1_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save1_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save1_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sprite_exists(obj_control.save1_thumbnail) && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_load(1);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(1);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,sprite_exists(obj_control.save2_thumbnail) && hover,xx,yy);
        draw_sprite(spr_saveload_files,4,xx,yy);
        if sprite_exists(obj_control.save2_thumbnail)
        {
            draw_sprite(obj_control.save2_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save2_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save2_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sprite_exists(obj_control.save2_thumbnail) && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_load(2);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(2);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,sprite_exists(obj_control.save3_thumbnail) && hover,xx,yy);
        draw_sprite(spr_saveload_files,5,xx,yy);
        if sprite_exists(obj_control.save3_thumbnail)
        {
            draw_sprite(obj_control.save3_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save3_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save3_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sprite_exists(obj_control.save3_thumbnail) && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_load(3);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(3);
        }
        yy += 93;
        
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(spr_saveload_files,sprite_exists(obj_control.save4_thumbnail) && hover,xx,yy);
        draw_sprite(spr_saveload_files,6,xx,yy);
        if sprite_exists(obj_control.save4_thumbnail)
        {
            draw_sprite(obj_control.save4_thumbnail,-1,xx+thumbnailxxoffset,yy+thumbnailyyoffset);
            //draw_text(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save4_date));
			draw_text_outlined(xx+datexxoffset,yy+dateyyoffset,string_hash_to_newline(obj_control.save4_date),c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
        }
        if sprite_exists(obj_control.save4_thumbnail) && hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_load(4);
        }
        var hover = mouse_over_area(xx+delete_xxoffset,yy,delete_bg_widtht,delete_bg_height);
        draw_sprite(delete_bg,hover,xx+delete_xxoffset,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            sissy_maker_deleting_create(4);
        }
        
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            menu_open = "";
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }
        
        var xx = 600;
        var yy = 65;
        var file_bg = spr_saveload_auto_on_off;
        var file_bg_widtht = sprite_get_width(file_bg);
        var file_bg_height = sprite_get_height(file_bg);
        var hover = mouse_over_area(xx,yy,file_bg_widtht,file_bg_height);
        draw_sprite(file_bg,obj_control.control_map[? CONTROL_AUTOSAVE]*2 + hover,xx,yy);
        if hover && clicked
        {
            clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
            obj_control.control_map[? CONTROL_AUTOSAVE] = !obj_control.control_map[? CONTROL_AUTOSAVE];
            sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
        }
        if hover
        {
            if obj_control.control_map[? CONTROL_AUTOSAVE]
                draw_tooltip(xx,yy+30,"Click to disable auto save in the first slot.");
            else
                draw_tooltip(xx,yy+30,"Click to enable auto save in the first slot.");
        }

}
else if menu_open == "exit"
{
		// exit BG
		draw_sprite(spr_content_bg,-1,0,0);
		draw_sprite(spr_exit_warning,-1,0,0);
		

		
        var xx = 320;
        var yy = 470;
        var spr_bg = spr_gui_yes;
        var spr_bg_width = sprite_get_width(spr_bg);
        var spr_bg_height = sprite_get_height(spr_bg);
        var hover = mouse_over_area(xx,yy,spr_bg_width,spr_bg_height);
		draw_sprite(spr_gui,hover,xx,yy);
        draw_sprite(spr_bg,hover,xx,yy);
        if hover && clicked && sissy_maker_can_save()
        {
			clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
			var inst = instance_create_depth(0,0,DEPTH_SAVELOAD,obj_saving);
			inst.saving_slot = 0;
			inst.returning = true;
			menu_open = "";
			viewing_while_hiding = false;
        }
		
        var xx = 430;
        var yy = 470;
        var spr_bg = spr_gui_no;
        var spr_bg_width = sprite_get_width(spr_bg);
        var spr_bg_height = sprite_get_height(spr_bg);
        var hover = mouse_over_area(xx,yy,spr_bg_width,spr_bg_height);
		draw_sprite(spr_gui,hover,xx,yy);
        draw_sprite(spr_bg,hover,xx,yy);
        if hover && clicked
        {
			clicked = false;
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
			menu_open = "";
			viewing_while_hiding = false;
        }
		
        draw_reset();
}
else if menu_open == "gallery" && gallery_full == noone
{
		// Gallery BG
		draw_sprite(spr_content_bg,-1,0,0);
		draw_sprite(spr_gallery_title,-1,35,15);
		
		for(var current_page = 0; current_page <= floor(ds_list_size(gallery_list)/9); current_page++)
		{
			var table_xx = 20 + (current_page - gallery_page)*800;
			var table_yy = 80;
			draw_sprite(spr_gallery_table,-1,table_xx,table_yy);
			for(var slot = 0;slot<9;slot++)
			{
				if ds_list_size(gallery_list) > current_page*9+slot
				{
					var slot_xx = table_xx + 100 + 200*(slot % 3); 
					var slot_yy = table_yy + 20 + 150*floor(slot / 3);
					var slot_spr = gallery_list[| current_page*9+slot];
					if asset_get_type(sprite_get_name(slot_spr)+"_s") == asset_sprite
						slot_spr = asset_get_index(sprite_get_name(slot_spr)+"_s");
					var slot_event = gallery_event_list[| current_page*9+slot];
					var slot_unlocked = (obj_control.event_map[? slot_event] >= 1);
				
					draw_sprite(spr_gallery_table_box,slot_unlocked,slot_xx,slot_yy);
					if slot_unlocked
					{
						draw_sprite_stretched(slot_spr,-1,slot_xx,slot_yy,gallery_spr_w,gallery_spr_h);
						if mouse_over_area(slot_xx,slot_yy,gallery_spr_w,gallery_spr_h)
						{
							draw_rectangle_color(slot_xx,slot_yy,slot_xx+gallery_spr_w,slot_yy+gallery_spr_h,c_yellow,c_yellow,c_yellow,c_yellow,true);
							if clicked
							{
								clicked = false;
					            gallery_full = gallery_list[| current_page*9+slot];
					            obj_control.left_pressed = false;
					            scene_play_sfx(sfx_button_01);
							
							
							}
						
						}
					}
				}
			
			
			}
		}

		



        draw_reset();
		
        /*var xx = 300;
        var yy = 555;
        var spr_bg = spr_gallery_previous;
        var spr_bg_width = sprite_get_width(spr_bg);
        var spr_bg_height = sprite_get_height(spr_bg);
        var hover = mouse_over_area(xx,yy,spr_bg_width,spr_bg_height);
        draw_sprite(spr_bg,hover,xx,yy);
        if hover && clicked
        {
			clicked = false;
            gallery_page_target = clamp(gallery_page_target - 1, 0, floor(ds_list_size(gallery_list)/9-1));
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }
		
        var xx = 430;
        var yy = 555;
        var spr_bg = spr_gallery_next;
        var spr_bg_width = sprite_get_width(spr_bg);
        var spr_bg_height = sprite_get_height(spr_bg);
        var hover = mouse_over_area(xx,yy,spr_bg_width,spr_bg_height);
        draw_sprite(spr_bg,hover,xx,yy);
        if hover && clicked
        {
			clicked = false;
            gallery_page_target = clamp(gallery_page_target + 1, 0, floor(ds_list_size(gallery_list)/9-1));
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }
		
		gallery_page = smooth_approach(gallery_page,gallery_page_target,12*delta_time_sec());
		*/
        
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            menu_open = "";
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }
}
else if menu_open == "stats"
{

        // STAT BG
		draw_sprite(spr_stats_empty_content_bg,-1,0,0);
        draw_sprite(spr_stats_content_bg,-1,0,0);
        
        draw_set_font(font_general_12b);
        draw_set_halign(fa_center);
        draw_set_valign(fa_center);
        
        // content 1
        if var_get(VAR_CUP_SIZE) <= 0
            var cup_size_text = "-";
        else if var_get(VAR_CUP_SIZE) <= 1
            var cup_size_text = "A";
        else if var_get(VAR_CUP_SIZE) <= 2
            var cup_size_text = "B";
        else if var_get(VAR_CUP_SIZE) <= 3
            var cup_size_text = "C";
        else if var_get(VAR_CUP_SIZE) <= 4
            var cup_size_text = "D";
        else if var_get(VAR_CUP_SIZE) <= 5
            var cup_size_text = "E";
        else if var_get(VAR_CUP_SIZE) <= 6
            var cup_size_text = "F";
        else if var_get(VAR_CUP_SIZE) <= 7
            var cup_size_text = "G";
        else if var_get(VAR_CUP_SIZE) <= 8
            var cup_size_text = "H";
        else if var_get(VAR_CUP_SIZE) <= 9
            var cup_size_text = "I";
        else
            var cup_size_text = "J";
    
        if var_get(VAR_ANUS) >= 4
            var anus_text = "Trained";
        else if var_get(VAR_ANUS) >= 3
            var anus_text = "Puffy";
        else if var_get(VAR_ANUS) >= 2
            var anus_text = "Normal";
        else if var_get(VAR_ANUS) >= 1
            var anus_text = "Tight";
        else if var_get(VAR_ANUS) >= 0
            var anus_text = "Loose";
        else if var_get(VAR_ANUS) >= -1
            var anus_text = "Sore";
        else if var_get(VAR_ANUS) >= -2
            var anus_text = "Wrecked";
        else
            var anus_text = "Destroyed";
            
        
        draw_stats_text(86,118,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_STRENGTH)))));
        draw_stats_text(86,118+27,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_STAMINA)))));
        draw_stats_text(86,118+27*2,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_DEXTERITY)))));
        var val = var_get(VAR_STRENGTH); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(216,118,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        var val = var_get(VAR_STAMINA); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(216,118+27,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        var val = var_get(VAR_DEXTERITY); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(216,118+27*2,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        
        draw_stats_text(89,255,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_BARGAIN)))));
        draw_stats_text(89,255+26.5,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_INTELLIGENCE)))));
        draw_stats_text(89,255+26.5*2,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_SEX_APPEAL)))));
        draw_stats_text(89,255+26.5*3,string_hash_to_newline("LV"+string(attributes_xp_get_level(var_get(VAR_CHARISMA)))));
        var val = var_get(VAR_BARGAIN); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(222,255,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        var val = var_get(VAR_INTELLIGENCE); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(222,255+26.5,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        var val = var_get(VAR_SEX_APPEAL); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(222,255+26.5*2,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        var val = var_get(VAR_CHARISMA); if val > 70 var plus = "+"; else var plus = "";
        draw_stats_text(222,255+26.5*3,string_hash_to_newline(string(min(val,70))+"XP"+plus));
        
        draw_stats_text(187,413,string_hash_to_newline("F"+string(round((var_get(VAR_FEMALE_RATIO)+female_ratio_adjust())*100))+"%"+" M"+string(round((1-(var_get(VAR_FEMALE_RATIO)+female_ratio_adjust()))*100))+"%"));
        draw_stats_longtext(187,413+26.5,string_hash_to_newline(string(var_get(VAR_BODY_TYPE)))); 
        draw_stats_text(187,413+26.5*2,string_hash_to_newline(string(cup_size_text))); 
        draw_stats_text(187,413+26.5*3,string_hash_to_newline(string(var_get(VAR_COCK))+" inches")); 
        draw_stats_text(187,413+26.5*4,string_hash_to_newline(string(anus_text))); 
        
		
		var stats_page_i = 0;
		if ceil(stats_page) == stats_page_i || floor(stats_page) == stats_page_i
		{
			var stats_xx_offset = (stats_page_i - stats_page)*200;
			var stats_page_alpha = 1 - abs(stats_page_i - stats_page);
			draw_set_alpha(stats_page_alpha);
			
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*2);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*3);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*4);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*4);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*5);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*6);
			
	        draw_stats_text(591+stats_xx_offset,115,string_hash_to_newline("LV"+string(var_get(VAR_AFF_CHRIS)))); 
	        draw_stats_text(591+stats_xx_offset,115+27,string_hash_to_newline("LV"+string(var_get(VAR_AFF_SIMONE)))); 
	        draw_stats_text(591+stats_xx_offset,115+27*2,string_hash_to_newline("LV"+string(var_get(VAR_AFF_RONDA)))); 
	        draw_stats_text(591+stats_xx_offset,115+27*3,string_hash_to_newline("LV"+string(var_get(VAR_AFF_SOPHIA)))); 
	        draw_stats_text(591+stats_xx_offset,115+27*4,string_hash_to_newline("LV"+string(var_get(VAR_AFF_PENNY)))); 
	        draw_stats_text(591+stats_xx_offset,115+27*5,string_hash_to_newline("LV"+string(var_get(VAR_AFF_SECURITY)))); 
	        //draw_stats_text(591+stats_xx_offset,115+27*6,string_hash_to_newline("LV"+string(var_get(VAR_AFF_DAYNA))));
			
	        if var_get(VAR_AFF_CHRIS) >= 1 var name = "Chris";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115,string_hash_to_newline(name));
	        if var_get(VAR_AFF_SIMONE) >= 1 var name = "Simone";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27,string_hash_to_newline(name));
	        if var_get(VAR_AFF_RONDA) >= 1 var name = "Ronda";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27*2,string_hash_to_newline(name));
	        if var_get(VAR_AFF_SOPHIA) >= 1 var name = "Sophia";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27*3,string_hash_to_newline(name));
	        if var_get(VAR_AFF_PENNY) >= 1 var name = "Penny";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27*4,string_hash_to_newline(name));
	        if var_get(VAR_AFF_SECURITY) >= 1 var name = "Security";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27*5,string_hash_to_newline(name));
	        //if var_get(VAR_AFF_DAYNA) >= 1 var name = "Dayna";
	        //else var name = "?";
	        //draw_stats_text(673+stats_xx_offset,115+27*6,string_hash_to_newline(name));
			
			draw_set_alpha(1);
		}
		/*var stats_page_i = 1;
		if ceil(stats_page) == stats_page_i || floor(stats_page) == stats_page_i
		{
			var stats_xx_offset = (stats_page_i - stats_page)*200;
			var stats_page_alpha = 1 - abs(stats_page_i - stats_page);
			draw_set_alpha(stats_page_alpha);
			
			
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*2);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*3);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*4);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*4);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*5);
			draw_sprite(spr_gui_purple_bars,-1,570+stats_xx_offset,103+27*6);
			
	        draw_stats_text(591+stats_xx_offset,115,string_hash_to_newline("LV"+string(var_get(VAR_AFF_RAYE)))); 
	        draw_stats_text(591+stats_xx_offset,115+27,string_hash_to_newline("LV"+string(var_get(VAR_AFF_KANE)))); 
	        draw_stats_text(591+stats_xx_offset,115+27*2,string_hash_to_newline("LV"+string(var_get(VAR_AFF_JOHNNY)))); 
			draw_stats_text(591+stats_xx_offset,115+27*3,string_hash_to_newline("LV"+string(var_get(VAR_AFF_MIREILLE)))); 

	        if var_get(VAR_AFF_RAYE) >= 1 var name = "Raye";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115,string_hash_to_newline(name));
	        if var_get(VAR_AFF_KANE) >= 1 var name = "Mr. Kane";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27,string_hash_to_newline(name));
	        if var_get(VAR_AFF_JOHNNY) >= 1 var name = "Johnny";
	        else var name = "?";
			draw_stats_text(673+stats_xx_offset,115+27*2,string_hash_to_newline(name));
	        if var_get(VAR_AFF_MIREILLE) >= 1 var name = "Mireille";
	        else var name = "?";
	        draw_stats_text(673+stats_xx_offset,115+27*3,string_hash_to_newline(name));

			draw_set_alpha(1);
		}
		*/
		
		
        
		

		/*var arrow_xx = 530;
		var arrow_yy = 180;
		var arrow_spr = spr_gui_stats_prev;
		var arrow_enable = stats_page_target > stats_page_min;
		var arrow_hover = mouse_over_area(arrow_xx,arrow_yy,sprite_get_width(arrow_spr),sprite_get_height(arrow_spr));
		if arrow_enable
			draw_sprite(arrow_spr,arrow_hover,arrow_xx,arrow_yy);
		else
			draw_sprite(arrow_spr,2,arrow_xx,arrow_yy);
		if arrow_enable && arrow_hover && clicked
		{
			clicked = false;
			stats_page_target--;
			scene_play_sfx(sfx_button_01);
		}
		

		var arrow_xx = 750;
		var arrow_yy = 180;
		var arrow_spr = spr_gui_stats_next;
		var arrow_enable = stats_page_target < stats_page_max;
		var arrow_hover = mouse_over_area(arrow_xx,arrow_yy,sprite_get_width(arrow_spr),sprite_get_height(arrow_spr));
		if arrow_enable
			draw_sprite(arrow_spr,arrow_hover,arrow_xx,arrow_yy);
		else
			draw_sprite(arrow_spr,2,arrow_xx,arrow_yy);
		if arrow_enable && arrow_hover && clicked
		{
			clicked = false;
			stats_page_target++;
			scene_play_sfx(sfx_button_01);
		}
		*/
		
		
		
		
		
		
		
        draw_stats_text(446,389,string_hash_to_newline(string(round(var_get(VAR_HP)))+"/"+string(round(var_get(VAR_HP_MAX)+max_hp_penalty())))); 
        draw_stats_text(446,389+27,string_hash_to_newline(string(round(var_get(VAR_ENERGY)))+"/"+string(round(var_get(VAR_ENERGY_MAX)+max_energy_bonus()))));
        
        draw_stats_text(448,453,string_hash_to_newline(string(round(var_get(VAR_CASH)))));
        draw_stats_text(448,453+27,string_hash_to_newline(string(round(var_get(VAR_DEBT)))));
        draw_stats_text(448,453+27*2,string_hash_to_newline("in "+string(round(var_get(VAR_DAYSREMAINING)))+" day(s)"));
        
        
        if skills_xp_get_level(var_get(VAR_SKILL_BABYSITTING)) >= 1
            var babysitting_text = "Baby Sitting (LV"+string(skills_xp_get_level(var_get(VAR_SKILL_BABYSITTING)))+")";
        else
            var babysitting_text = "?";
        draw_stats_text(673,375,string_hash_to_newline(babysitting_text)); 
        
        //if skills_xp_get_level(var_get(VAR_SKILL_JAZZ)) >= 1
        //    var jazz_text = "Jazz (LV"+string(skills_xp_get_level(var_get(VAR_SKILL_JAZZ)))+")";
        //else
            var jazz_text = "-";
        draw_stats_text(673,375+28.5,string_hash_to_newline(jazz_text));
		
        //if skills_xp_get_level(var_get(VAR_SKILL_SALSA)) >= 1
        //    var salsa_text = "Salsa (LV"+string(skills_xp_get_level(var_get(VAR_SKILL_SALSA)))+")";
        //else
            var salsa_text = "-";
        draw_stats_text(673,375+28.5*2,string_hash_to_newline(salsa_text)); 
		
		
        draw_stats_text(673,375+28.5*3,string_hash_to_newline("-")); 
        draw_stats_text(673,375+28.5*4,string_hash_to_newline("-")); 
        draw_stats_text(673,375+28.5*5,string_hash_to_newline("-")); 
    
        //draw_sprite_ext(spr_Menu_Status_Center,-1,411,213,1,1,0,c_white,1);  
        
        draw_reset();
        
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            menu_open = "";
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }

}
else if menu_open == "items"
{

        // inventory BG
        draw_sprite(spr_content_bg,-1,x,y);
		
		
		// inventory area
		draw_sprite(spr_items_content,items_page-1,x+50,y+40);
		
			// button
			var button_1_x = x+100;
			var button_1_y = y+60;
			var button_2_x = x+255;
			var button_2_y = y+60;
			var button_spr = spr_items_content_buttons;
			var button_spr_width = sprite_get_width(button_spr);
			var button_spr_height = sprite_get_height(button_spr);
			
			if mouse_over_area(button_1_x,button_1_y,button_spr_width,button_spr_height) && clicked
			{
				clicked = false;
	            obj_control.left_pressed = false;
				items_page = 1;
				scene_play_sfx(sfx_button_01);
			}
			if mouse_over_area(button_2_x,button_2_y,button_spr_width,button_spr_height) && clicked
			{
				clicked = false;
	            obj_control.left_pressed = false;
				items_page = 2;
				scene_play_sfx(sfx_button_01);
			}
			if items_page == 1
			{
				draw_sprite(button_spr,1,button_1_x,button_1_y);
				draw_sprite(button_spr,2,button_2_x,button_2_y);
			}
			else if items_page == 2
			{
				draw_sprite(button_spr,0,button_1_x,button_1_y);
				draw_sprite(button_spr,3,button_2_x,button_2_y);
			}
			
			
        // character previous
        var draw_x = 470;
        var draw_y = 200;
        var hover = mouse_over_area(draw_x,draw_y,sprite_get_width(spr_gui_previous),sprite_get_height(spr_gui_previous));
        draw_sprite(spr_gui_previous,hover,draw_x,draw_y);
        if hover && clicked
        {
			clicked = false;
            scene_play_sfx(sfx_button_01);
            outfit_set_previous();
            obj_control.left_pressed = false;
        }
        
        // character next
        var draw_x = 693;
        var draw_y = 200;
        var hover = mouse_over_area(draw_x,draw_y,sprite_get_width(spr_gui_next),sprite_get_height(spr_gui_next));
        draw_sprite(spr_gui_next,hover,draw_x,draw_y);
        if hover && clicked
        {
			clicked = false;
            scene_play_sfx(sfx_button_01);
            outfit_set_next();
            obj_control.left_pressed = false;
        }   
        // character
        //var draw_x = 387;
        //var draw_y = 137;
        //draw_text(draw_x,draw_y,'Outfit '+string(var_get(igvar_outfit)));
        
        var draw_x = 617;
        var draw_y = 90;
        draw_set_halign(fa_center);
        if var_get(VAR_OUTFIT) == 2
            draw_sprite_ext(spr_char_Chris_outfit2,-1,draw_x,draw_y,1,1,0,c_white,1);
        else if var_get(VAR_OUTFIT) == 3
            draw_sprite_ext(spr_char_Chris_outfit3,-1,draw_x,draw_y,1,1,0,c_white,1);
        else
            draw_sprite_ext(spr_char_Chris_outfit1,-1,draw_x,draw_y,1,1,0,c_white,1);  
            
        draw_set_halign(fa_left);
        
        if items_page == 1
		{
	        // inventory (3x3)
	        var x_total = 3;
	        var y_total = 3;
	        var item_width = 110;
	        var item_height = 119;
	        var draw_x_left = 95;
	        var draw_y_top = 135;
        
	        for(var drawing_x_index = 0; drawing_x_index < x_total; drawing_x_index++)
	            for(var drawing_y_index = 0; drawing_y_index < y_total; drawing_y_index++)
	            {
	                var item_posi = drawing_x_index + drawing_y_index*x_total;
				
					var item_x = draw_x_left + floor(item_posi % x_total) * item_width;
	                var item_y = draw_y_top + floor(item_posi / y_total) * item_height;
					draw_sprite(spr_items_content_empty_box,-1,item_x,item_y);
				
	                if item_posi < inventory_get_size()
	                {
						var item_name = inventory_order_index_get_name(item_posi);
						var item_amount = inventory_get_item_amount(item_name);
	                    var item_sprite = item_get_sprite(item_name);
						var item_hover = mouse_over_area(item_x,item_y,sprite_get_width(item_sprite),sprite_get_height(item_sprite));
						
	                    if item_amount > 0
						{
							draw_sprite(item_sprite,-1,item_x,item_y);
							
							if item_hover
							{
								var button_xx = item_x+sprite_get_width(item_sprite)/2-sprite_get_width(spr_gui)/2;
								var button_yy = item_y+sprite_get_height(item_sprite)-sprite_get_height(spr_gui);
								var button_hover = mouse_over_area(button_xx,button_yy,sprite_get_width(spr_gui),sprite_get_height(spr_gui));
							
								if !inventory_item_can_use(item_name)
								{
									//draw_sprite(spr_gui,0,button_xx,button_yy);
									//draw_sprite(spr_gui_use,0,button_xx,button_yy);
								}
								else
								{
									draw_sprite(spr_gui,button_hover,button_xx,button_yy);
									draw_sprite(spr_gui_use,0,button_xx,button_yy);
									if button_hover && clicked
									{
										clicked = false;
										obj_control.left_pressed = false;
										inventory_use_item_no_delete(item_name);
										inventory_delete_item(item_name,1);
										scene_play_sfx(sfx_button_01);
										
									}
								}
							}

						}

	                }
	            }
            
	        // inventory (3x3) (tooltip)
	        var x_total = 3;
	        var y_total = 3;
	        var item_width = 110;
	        var item_height = 119;
	        var draw_x_left = 95;
	        var draw_y_top = 135;
        
	        for(var drawing_x_index = 0; drawing_x_index < x_total; drawing_x_index++)
	            for(var drawing_y_index = 0; drawing_y_index < y_total; drawing_y_index++)
	            {
	                var item_posi = drawing_x_index + drawing_y_index*x_total;
	                if item_posi < inventory_get_size()
	                {
						var item_name = inventory_order_index_get_name(item_posi);
	                    var item_x = draw_x_left + floor(item_posi % x_total) * item_width;
	                    var item_y = draw_y_top + floor(item_posi / y_total) * item_height;
	                    var item_sprite = item_get_sprite(item_name);
	                    var item_des = item_get_des(item_name);
	                    var item_amount = inventory_get_item_amount(item_name);
                    
	                    if item_amount > 0 && mouse_over_area(item_x,item_y,sprite_get_width(item_sprite),sprite_get_height(item_sprite))
	                    {
                    
	                        if item_name == ITEM_Lotion || item_name == ITEM_So_Zore_Lotion
								var tooltip_string = string(item_name)+" ("+string(item_amount)+" times remaining)"+"\r"+string(item_des);
							else if item_amount == 1	
								var tooltip_string = string(item_name)+"\r"+string(item_des);
							else
								var tooltip_string = string(item_name)+" (Amount: "+string(item_amount)+")"+"\r"+string(item_des);
							
							//tooltip_string+="\r\rUsage: "+inventory_item_can_use_ext(item_name);
							
							draw_tooltip(item_x+sprite_get_width(item_sprite)+20,item_y,tooltip_string);
                    
                    
	                    }
	                }
	            } 
        }
		else if items_page == 2
		{
		
			var xx = 78;
			var yy = 130;
			var spr = spr_job_back;
			var spr_width = sprite_get_width(spr);
			var spr_height = sprite_get_height(spr);
			var padding = 10;
			
			// Baby Sitting
			if var_get(VAR_JOB_BABYSITTING) > 0 || jobs_list_job_exists(JOBNAME_Baby_Sitting)
			{
				// BG
				draw_sprite(spr,-1,xx,yy);
				
				// ICON
				draw_sprite(spr_job_babysitting_icon,-1,xx+10,yy);
				
				draw_set_font(font_general_13b);
				draw_set_color(c_white);
				draw_text_outlined(xx+60,yy+20,JOBNAME_Baby_Sitting,c_white,c_dkpurple,1,OUTLINE_WIDTH,OUTLINE_FID);
				draw_set_font(font_general_10b);
				draw_set_halign(fa_center);
				draw_text_outlined(xx+240,yy+10,"completion(s)",c_white,c_dkpurple,1,1,20);
				draw_set_font(font_general_20);
				draw_text_outlined(xx+240,yy+27,var_get(VAR_JOB_BABYSITTING),c_white,c_dkpurple,1,1,20);
				
				draw_reset();
				
				if jobs_list_job_exists(JOBNAME_Baby_Sitting)
				{
					// assignment icon
					draw_sprite(spr_job_assignment_icon,-1,xx+297,yy+15);
					
					if mouse_over_area(xx,yy,spr_width,spr_height)
					{
						var job_index = ds_list_find_index(obj_control.jobs_list_name,JOBNAME_Baby_Sitting);
						var job_des_details = "";
						job_des_details += "Job: "+JOBNAME_Baby_Sitting+"\r";
						job_des_details += "Location: "+obj_control.jobs_list_place[| job_index]+"\r";
						if var_get(VAR_DAY) == obj_control.jobs_list_time_begin_day[| job_index]
						    var job_day = "Today";
						else if var_get(VAR_DAY)+1 == obj_control.jobs_list_time_begin_day[| job_index]
						    var job_day = "Tomorrow";
						else if var_get(VAR_DAY) > obj_control.jobs_list_time_begin_day[| job_index]
						    var job_day = string(var_get(VAR_DAY) - obj_control.jobs_list_time_begin_day[| job_index])+" day(s) before";
						else if var_get(VAR_DAY) < obj_control.jobs_list_time_begin_day[| job_index]
						    var job_day = string(obj_control.jobs_list_time_begin_day[| job_index] - var_get(VAR_DAY))+" day(s) after";
						job_des_details += "Time: "+job_day+" "+time_0_23_to_time_stamp(obj_control.jobs_list_time_begin_0_23[| job_index])+" ~ "+time_0_23_to_time_stamp(obj_control.jobs_list_time_end_0_23[| job_index]);
						draw_tooltip(xx+350,yy+5,job_des_details);	
					}
				}
				yy += spr_height + padding;
			}
			
			
			

		}
		    
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            menu_open = "";
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }


}
else if menu_open == "tools"
{

        // BG
        draw_sprite(spr_content_bg,-1,x,y);
    
        // Title
        var draw_x = 70;
        var draw_y = 25;
        draw_sprite_ext(spr_settings_title,-1,draw_x,draw_y,0.7,0.7,0,c_white,1);
        
        // Music Vol
        var draw_x = 70;
        var draw_y = 100;
        var settings_bg = spr_settings_bg;
        draw_sprite(settings_bg,0,draw_x,draw_y);
        draw_sprite(spr_settings_music,0,draw_x+20,draw_y+15);
            var button_x = draw_x+230;
            var button_y = draw_y+10;
            var button_spr = spr_settings_bar;
            var button_spr_width = sprite_get_width(button_spr);
            var button_spr_height = sprite_get_height(button_spr);
            var button_on_hover = mouse_over_area(button_x,button_y,button_spr_width,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                obj_control.left_pressed = false;
                scene_play_sfx(sfx_button_01);
                holding = "musiclevel";
            }
            if holding == "musiclevel" && mouse_check_button(mb_left)
            {
                if mouse_x <= button_x
                    obj_control.control_map[? CONTROL_BGM_LEVEL] = 0;
                else if mouse_x >= button_x + button_spr_width
                    obj_control.control_map[? CONTROL_BGM_LEVEL] = 1;
                else
                    obj_control.control_map[? CONTROL_BGM_LEVEL] = (mouse_x - button_x) / button_spr_width;
            }
            if holding == "musiclevel" && mouse_check_button_released(mb_left)
            {
                holding = "";
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }

            draw_sprite(button_spr,0,button_x,button_y);
            draw_sprite_part(button_spr,1,0,0,button_spr_width*obj_control.control_map[? CONTROL_BGM_LEVEL],button_spr_height,button_x,button_y);

        draw_y+=93;
    
        // Sound Vol
        draw_sprite(settings_bg,0,draw_x,draw_y);
        draw_sprite(spr_settings_sound,0,draw_x+20,draw_y+15);
            var button_x = draw_x+230;
            var button_y = draw_y+10;
            var button_spr = spr_settings_bar;
            var button_spr_width = sprite_get_width(button_spr);
            var button_spr_height = sprite_get_height(button_spr);
            var button_on_hover = mouse_over_area(button_x,button_y,button_spr_width,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                obj_control.left_pressed = false;
                scene_play_sfx(sfx_button_01);
                holding = "soundlevel";
            }
            if holding == "soundlevel" && mouse_check_button(mb_left)
            {
                if mouse_x <= button_x
                    obj_control.control_map[? CONTROL_SFX_LEVEL] = 0;
                else if mouse_x >= button_x + button_spr_width
                    obj_control.control_map[? CONTROL_SFX_LEVEL] = 1;
                else
                    obj_control.control_map[? CONTROL_SFX_LEVEL] = (mouse_x - button_x) / button_spr_width;
            }
            if holding == "soundlevel" && mouse_check_button_released(mb_left)
            {
                holding = "";
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }

            draw_sprite(button_spr,0,button_x,button_y);
            draw_sprite_part(button_spr,1,0,0,button_spr_width*obj_control.control_map[? CONTROL_SFX_LEVEL],button_spr_height,button_x,button_y);
        draw_y+=93;
    
        // Text Speed
        draw_sprite(settings_bg,0,draw_x,draw_y);
        draw_sprite(spr_settings_textspeed,0,draw_x+20,draw_y+15);
            var button_x = draw_x+230;
            var button_y = draw_y+10;
            var button_spr = spr_settings_bar;
            var button_spr_width = sprite_get_width(button_spr);
            var button_spr_height = sprite_get_height(button_spr);
            var button_on_hover = mouse_over_area(button_x,button_y,button_spr_width,button_spr_height);
            var min_speed = 10;
            var max_speed = 100;
            if clicked && button_on_hover
            {
                clicked = false;
                obj_control.left_pressed = false;
                scene_play_sfx(sfx_button_01);
                holding = "textspeed";
            }
            if holding == "textspeed" && mouse_check_button(mb_left)
            {
                if mouse_x <= button_x
                    obj_control.control_map[? CONTROL_TEXT_SPEED] = min_speed;
                else if mouse_x >= button_x + button_spr_width
                    obj_control.control_map[? CONTROL_TEXT_SPEED] = max_speed;
                else
                    obj_control.control_map[? CONTROL_TEXT_SPEED] = min_speed+((mouse_x - button_x) / button_spr_width)*(max_speed-min_speed);
            }
            if holding == "textspeed" && mouse_check_button_released(mb_left)
            {
                holding = "";
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }

            draw_sprite(button_spr,0,button_x,button_y);
            draw_sprite_part(button_spr,1,0,0,button_spr_width*((obj_control.control_map[? CONTROL_TEXT_SPEED]-min_speed)/(max_speed-min_speed)),button_spr_height,button_x,button_y);
        draw_y+=93;
    
        // Auto save
        draw_sprite(settings_bg,0,draw_x,draw_y);
        draw_sprite(spr_settings_autosave,0,draw_x+20,draw_y+15);
            var button_x = draw_x+230;
            var button_y = draw_y+10;
            var button_spr = spr_settings_on_off;
            var button_spr_width = sprite_get_width(button_spr);
            var button_spr_height = sprite_get_height(button_spr);
            var button_on_hover = mouse_over_area(button_x,button_y,button_spr_width/2,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                scene_play_sfx(sfx_button_01);
                obj_control.left_pressed = false;
                obj_control.control_map[? CONTROL_AUTOSAVE] = true;
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }
            var button_on_hover = mouse_over_area(button_x+button_spr_width/2,button_y,button_spr_width/2,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                scene_play_sfx(sfx_button_01);
                obj_control.left_pressed = false;
                obj_control.control_map[? CONTROL_AUTOSAVE] = false;
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }
            draw_sprite(button_spr,obj_control.control_map[? CONTROL_AUTOSAVE],button_x,button_y);
        draw_y+=93;
    
        // Screen
        draw_sprite(settings_bg,0,draw_x,draw_y);
        draw_sprite(spr_settings_screen,0,draw_x+20,draw_y+15);
            var button_x = draw_x+230;
            var button_y = draw_y+10;
            var button_spr = spr_settings_fullscreen_on_off;
            var button_spr_width = sprite_get_width(button_spr);
            var button_spr_height = sprite_get_height(button_spr);
            var button_on_hover = mouse_over_area(button_x,button_y,button_spr_width/2,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                obj_control.left_pressed = false;
                scene_play_sfx(sfx_button_01);
                obj_control.control_map[? CONTROL_FULLSCREEN] = false;
                if window_get_fullscreen()
                    window_set_fullscreen(false);
				sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }
            var button_on_hover = mouse_over_area(button_x+button_spr_width/2,button_y,button_spr_width/2,button_spr_height);
            if clicked && button_on_hover
            {
                clicked = false;
                obj_control.left_pressed = false;
                scene_play_sfx(sfx_button_01);
                obj_control.control_map[? CONTROL_FULLSCREEN] = true;
                if !window_get_fullscreen()
                    window_set_fullscreen(true);
                sissy_maker_save_map("controls.dat",obj_control.control_map,obj_control.secretString);
            }
            draw_sprite(button_spr,obj_control.control_map[? CONTROL_FULLSCREEN],button_x,button_y);
        draw_y+=93;
    
    
        
        
        var xx = 670;
        var yy = 565;
        var back_bg = spr_saveload_back;
        var back_bg_width = sprite_get_width(back_bg);
        var back_bg_height = sprite_get_height(back_bg);
        var hover = mouse_over_area(xx,yy,back_bg_width,back_bg_height);
        draw_sprite(spr_saveload_back,hover,670,565);
        if hover && clicked
        {
			clicked = false;
            menu_open = "";
            obj_control.left_pressed = false;
            scene_play_sfx(sfx_button_01);
        }


}


mouse_on_buttons = false;
if obj_control.cancel_button_pressed && room != room_opening && room != room_title_screen && room != room_questionnaire
{
	scene_play_sfx(sfx_button_01);
	obj_control.cancel_button_pressed = false;
	if menu_open == ""
	{
		thumbnail_setup();
		menu_open = "tools";
	}
	else menu_open = "";
}


// buttons
if (!obj_control.var_map[? VAR_MAIN_GUI_HIDING] || menu_exists()) && gallery_full == noone
{
	if room != room_title_screen
	{
	    var xx = 750;
	    var yy = 0;
	    //tools
	    var button_bg = spr_gui_tools;
	    var button_bg_width = sprite_get_width(button_bg);
	    var button_bg_height = sprite_get_height(button_bg);
	    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
	    if hover mouse_on_buttons = true;
	    draw_sprite(button_bg,hover || menu_open == "tools",xx,yy);
	    if hover && clicked 
	    {
	        clicked = false;
	        obj_control.left_pressed = false;
	        scene_play_sfx(sfx_button_01);
	        if menu_open == "" thumbnail_setup();
	        if menu_open != "tools" menu_open = "tools";
	        else { menu_open = ""; viewing_while_hiding = false;}
	    }
	    xx -= 80;

	    //stats
	    var button_bg = spr_gui;
	    var button_bg_width = sprite_get_width(button_bg);
	    var button_bg_height = sprite_get_height(button_bg);
	    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
	    if hover mouse_on_buttons = true;
	    draw_sprite(button_bg,hover || menu_open == "stats",xx,yy);
	    draw_sprite(spr_gui_stats,hover || menu_open == "stats",xx,yy);
	    if hover && clicked 
	    {
	        clicked = false;
	        obj_control.left_pressed = false;
	        scene_play_sfx(sfx_button_01);
	        if menu_open == "" thumbnail_setup();
	        if menu_open != "stats" menu_open = "stats";
	        else { menu_open = ""; viewing_while_hiding = false;}
	    }
			if menu_open != ""
			{
				yy += 30;
			    //exit
			    var button_bg = spr_gui;
			    var button_bg_width = sprite_get_width(button_bg);
			    var button_bg_height = sprite_get_height(button_bg);
			    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
			    if hover mouse_on_buttons = true;
			    if !sissy_maker_can_save() var img_index = 2;
			    else if hover || menu_open == "exit" var img_index = 1;
			    else var img_index = 0;
			    draw_sprite(button_bg,img_index,xx,yy);
			    draw_sprite(spr_gui_exit,img_index,xx,yy);
			    if hover && clicked && sissy_maker_can_save()
			    {
			        clicked = false;
			        obj_control.left_pressed = false;
			        scene_play_sfx(sfx_button_01);
			        if menu_open == "" thumbnail_setup();
			        if menu_open != "exit" menu_open = "exit";
			        else { menu_open = ""; viewing_while_hiding = false;}
			    }
				yy -= 30;
			}
	    xx -= 80;
	    //items
	    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
	    if hover mouse_on_buttons = true;
	    draw_sprite(button_bg,hover || menu_open == "items",xx,yy);
	    draw_sprite(spr_gui_items,hover || menu_open == "items",xx,yy);
	    if hover && clicked 
	    {
	        clicked = false;
	        obj_control.left_pressed = false;
	        scene_play_sfx(sfx_button_01);
	        if menu_open == "" thumbnail_setup();
	        if menu_open != "items"{
	            menu_open = "items";
	            // update_job_info (no longer needed)
	            //update_job_info();
	        }
	        else { menu_open = ""; viewing_while_hiding = false;}
	    }
			if menu_open != ""
			{
				yy += 30;
			    //gallery
			    var button_bg = spr_gui;
			    var button_bg_width = sprite_get_width(button_bg);
			    var button_bg_height = sprite_get_height(button_bg);
			    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
			    if hover mouse_on_buttons = true;
			    draw_sprite(button_bg,hover || menu_open == "gallery",xx,yy);
			    draw_sprite(spr_gui_gallery,hover || menu_open == "gallery",xx,yy);
			    if hover && clicked 
			    {
			        clicked = false;
			        obj_control.left_pressed = false;
			        scene_play_sfx(sfx_button_01);
			        if menu_open == "" thumbnail_setup();
			        if menu_open != "gallery" menu_open = "gallery";
			        else { menu_open = ""; viewing_while_hiding = false;}
			    }
				yy -= 30;
			}
	    xx -= 80;
	    //load
	    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
	    if hover mouse_on_buttons = true;
	    draw_sprite(button_bg,hover || menu_open == "load",xx,yy);
	    draw_sprite(spr_gui_load,hover || menu_open == "load",xx,yy);
	    if hover && clicked 
	    {
	        clicked = false;
	        obj_control.left_pressed = false;
	        scene_play_sfx(sfx_button_01);
	        if menu_open == "" thumbnail_setup();
	        if menu_open != "load" menu_open = "load";
	        else { menu_open = ""; viewing_while_hiding = false;}
	        thumbnail_update();
	    }
	    xx -= 80;
	    //save
	    var hover = mouse_over_area(xx,yy,button_bg_width,button_bg_height);
	    if hover mouse_on_buttons = true;
	    if !sissy_maker_can_save() var save_img_index = 2;
	    else if hover || menu_open == "save" var save_img_index = 1;
	    else var save_img_index = 0;
	    draw_sprite(button_bg,save_img_index,xx,yy);
	    draw_sprite(spr_gui_save,save_img_index,xx,yy);
	    if hover && clicked && sissy_maker_can_save()
	    {
	        clicked = false;
	        obj_control.left_pressed = false;
	        scene_play_sfx(sfx_button_01);
	        if menu_open == "" thumbnail_setup();
	        if menu_open != "save" menu_open = "save";
	        else { menu_open = ""; viewing_while_hiding = false;}
	        thumbnail_update();
	    }
	    xx -= 80;
	}	
	// deleting buttons step
	sissy_maker_deleting_step();
}



// gallery_full
if gallery_full != noone
{
	var col = make_color_rgb(239,255,101);
	draw_rectangle_color(0,0,room_width,room_height,col,col,col,col,false);
	draw_sprite(gallery_full,-1,0,0);
	if clicked
	{
		gallery_full = noone;
	}
}