if room == room_opening
{
    gmlvideo_video_draw(myvideo, x, y, sprite_width, sprite_height);

	// end
	total_time = total_time + delta_time_sec();
	
    if total_time >= 52
    {
        room_goto_ext(room_title_screen,true);
    }
    
    ///skip
    if keyboard_check(vk_anykey) || mouse_check_button(mb_any)
    {
        holding_time = holding_time + delta_time_sec();
        if holding_time >= 1.5
        {
            room_goto_ext(room_title_screen,true);
        }
    }
    else
        holding_time = max(0,holding_time - delta_time_sec());
        
    if holding_time > 0
    {
        draw_set_halign(fa_right);
        draw_set_valign(fa_top);
        draw_set_font(font_general_30b);
        draw_set_alpha(holding_time);
        draw_text_outlined(room_width-20,+10,"Hold ANY key to SKIP",c_white,c_black,1,3,50);
        draw_reset();
    }
}

