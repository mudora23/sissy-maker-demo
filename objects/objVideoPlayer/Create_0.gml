///Example
gmlvideo_flushcache();

myvideo = -1;
loadFile = false;

if (loadFile)
    myvideo = gmlvideo_load(get_open_filename("vid|*.vid",""));
else
    myvideo =  gmlvideo_load("Opening.vid");
    
//gmlvideo_video_volume(myvideo); //<--Mute volume

if (myvideo == -1)
	room_goto(room_title_screen);
    //show_error("VIDEO FILE FAILED TO LOAD", true);
else
{
	os_powersave_enable(false);
	gmlvideo_video_volume(myvideo,0.1*obj_control.control_map[? CONTROL_BGM_LEVEL]);
}
holding_time = 0;
total_time = 0;