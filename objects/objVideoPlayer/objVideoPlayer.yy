{
    "id": "26d4c7f3-e882-4c07-a443-d4207f7a01a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objVideoPlayer",
    "eventList": [
        {
            "id": "0259c9b8-164e-4cb5-90f7-315ab67f8c0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26d4c7f3-e882-4c07-a443-d4207f7a01a9"
        },
        {
            "id": "0e799a01-3586-413a-a42f-899a9be97587",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "26d4c7f3-e882-4c07-a443-d4207f7a01a9"
        },
        {
            "id": "f98be454-744b-46f9-ad80-a1e62f563456",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "26d4c7f3-e882-4c07-a443-d4207f7a01a9"
        },
        {
            "id": "62b3d969-f57d-4cbf-993c-edbd23fc19c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "26d4c7f3-e882-4c07-a443-d4207f7a01a9"
        },
        {
            "id": "f53b8c7b-bd95-4a3f-af5b-d60f97015049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "26d4c7f3-e882-4c07-a443-d4207f7a01a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c05d14fa-a74f-4d14-8a41-e74606ecf31b",
    "visible": true
}