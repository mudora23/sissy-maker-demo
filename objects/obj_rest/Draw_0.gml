if hiding || processing
    shadow_image_alpha = approach(shadow_image_alpha, 0, 1.2*delta_time_sec());
else
    shadow_image_alpha = approach(shadow_image_alpha, 0.9, 1.2*delta_time_sec());
    
draw_sprite_ext(spr_black,-1,0,0,1,1,0,c_white,shadow_image_alpha);

if hiding || processing
{
    x = approach(x,x_hide,1200*delta_time_sec());
    y = approach(y,y_hide,1200*delta_time_sec());
}
else
{
    x = smooth_approach(x,x_target,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_target,slide_speed*delta_time_sec());
}
if hiding && x == x_hide && y == y_hide
{
	scene_jump(scene_goto);
    instance_destroy();
}
draw_self();

var cur_time = var_get(VAR_TIME);

var xx = x+196;
var yy = y+260;
var button_image = spr_clock_button;
var button_image_width = sprite_get_width(button_image);
var button_image_height = sprite_get_height(button_image);
var clicked = mouse_check_button_pressed(mb_left);
var hover = mouse_over_area(xx,yy,button_image_width,button_image_height);
draw_sprite_ext(button_image, 2+hover, xx,yy,1,1,0,c_white,1);
if hover && clicked
{
	scene_play_sfx(sfx_click);
    clicked = false;
    rest_time = max(rest_time-1,0);
}
xx += 80;

var hover = mouse_over_area(xx,yy,button_image_width,button_image_height);
draw_sprite_ext(button_image, hover, xx,yy,1,1,0,c_white,1);
if hover && clicked
{
	scene_play_sfx(sfx_click);
    clicked = false;
    rest_time = min(rest_time+1,6);
}

var new_time = cur_time + rest_time*2;
if new_time > 23 new_time -= 24;

draw_set_halign(fa_center);
draw_set_valign(fa_center);

draw_set_color(c_black);
draw_set_font(font_clock_30);
draw_text(x+117,y+280,time_0_23_to_time_stamp(new_time));
draw_reset();




if processing
{
	processing_timer-=delta_time_sec();
	if processing_timer <= 0
	{
		processing_timer = processing_timer_max;
		if rest_time > 0 &&
		    (morning_can_stay && current_time_get_name() == "Morning" ||
		     afternoon_can_stay && current_time_get_name() == "Afternoon" ||
		     evening_can_stay && current_time_get_name() == "Evening" ||
		     late_night_can_stay && current_time_get_name() == "Late Night")
		{
		    time_hour_passed(2);
		    if !audio_is_playing(sfx_clock)
		        scene_play_sfx(sfx_clock);
		    rest_time--;
		    rested_time++;
	
		    var_add_ext(VAR_ENERGY,var_get(VAR_ENERGY_MAX)*(20+rested_time*2)/100,0,var_get(VAR_ENERGY_MAX)+max_energy_bonus()); // 20% / 22% ......
		    var_add_ext(VAR_HP,(var_get(VAR_HP_MAX)+max_hp_penalty())*0.02,0,var_get(VAR_HP_MAX)+max_hp_penalty());

		}
		else
		{
		    hiding = true;
		}
	}
}