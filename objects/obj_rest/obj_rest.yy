{
    "id": "471c9e3d-5516-4b16-aa33-2f4d29335cda",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rest",
    "eventList": [
        {
            "id": "4c03830c-07ec-48e0-955d-f2f7859e0b2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "471c9e3d-5516-4b16-aa33-2f4d29335cda"
        },
        {
            "id": "09da2b84-32fc-449f-a192-03c1937c99d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "471c9e3d-5516-4b16-aa33-2f4d29335cda"
        },
        {
            "id": "13d2dc9d-9613-4ee6-8345-22de6966ea73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "471c9e3d-5516-4b16-aa33-2f4d29335cda"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "d48ca38a-461a-4c7a-8c05-5802cb8b0b8e",
    "visible": true
}