{
    "id": "1f85dd3f-922f-4fdb-a471-1a7d09366656",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_job",
    "eventList": [
        {
            "id": "36be73b7-ae9f-4dab-a2f5-7a321d5dad95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f85dd3f-922f-4fdb-a471-1a7d09366656"
        },
        {
            "id": "396a6997-db37-40fa-ab19-3be6b3fda024",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1f85dd3f-922f-4fdb-a471-1a7d09366656"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "54500535-6165-43b4-9b38-89fc939f7c6e",
    "visible": true
}