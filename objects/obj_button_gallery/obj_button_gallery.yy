{
    "id": "45f0151b-b5e9-4411-83df-874f51313570",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_gallery",
    "eventList": [
        {
            "id": "354babf5-b0df-4ede-842c-647d6c9f876f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45f0151b-b5e9-4411-83df-874f51313570"
        },
        {
            "id": "e3e587e9-0b49-498e-b174-56b71f3f48a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "45f0151b-b5e9-4411-83df-874f51313570"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "visible": true
}