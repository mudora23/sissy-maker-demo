///Paste Destroy Event code here
//------------------------------

//DESTROY EVENT BEGIN
//----------------------------
part_particles_clear(ps0);
part_system_clear(ps0);
part_system_destroy(ps0);

part_type_destroy(pt0);

//DESTROY EVENT END
//----------------------------

