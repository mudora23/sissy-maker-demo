{
    "id": "9587ea34-ea90-4976-b572-fc03eb308dff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particle_cloud",
    "eventList": [
        {
            "id": "6ad9e6b6-9b93-4de4-90df-f55353c0a6c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9587ea34-ea90-4976-b572-fc03eb308dff"
        },
        {
            "id": "9e0833f9-64c1-4519-b14f-81db6f8deca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9587ea34-ea90-4976-b572-fc03eb308dff"
        },
        {
            "id": "309e7941-c3a7-4d81-ad4f-7c367f4d23b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "9587ea34-ea90-4976-b572-fc03eb308dff"
        },
        {
            "id": "33072635-50dd-4137-8961-f532d6cef5e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9587ea34-ea90-4976-b572-fc03eb308dff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}