/// Info and optional settings 

//Change the scale of the effect
//This affects the particle size and emitter region
effect_scale = 1;

///Paste Create Event code here
//-----------------------------


//CREATE EVENT BEGIN
//----------------------------

//--- Create Particle System : Particle System 1 ---
ps0 = part_system_create();
part_system_depth(ps0,DEPTH_FLOATING_TEXT_EFFECT);

//--- Create Emitter : Emitter 1 ---
em0 = part_emitter_create(ps0);
em0_delaystart = 1/60;
em0_delay = 0;
em0_streaming = false;
em0_single_shot = 0;
em0_has_shot = false;

//--- Create Particle : Particle 1 ---
pt0 = part_type_create();
part_type_size(pt0,0.3*effect_scale,0.5*effect_scale,0,0);
part_type_scale(pt0,1*effect_scale,1*effect_scale);
part_type_life(pt0,36,100);
part_type_gravity(pt0,0,0);
part_type_speed(pt0,1,1.5,0,0);
part_type_direction(pt0,45,135,0,0);
part_type_orientation(pt0,0,0,0,0,1);
part_type_blend(pt0,0);
part_type_alpha3(pt0,0.07,0.15,0);
part_type_color2(pt0,16777215,16777215);
part_type_sprite(pt0,TMC_PL_Cloud_2_spr,1,1,0);



pt_maximum_life=100;
stop=false;

//CREATE EVENT END
//----------------------------

update_rate = 1/60;
update_rate_max = 1/60;
part_system_automatic_update(ps0,false);
repeat(pt_maximum_life){
    if (!stop)
    {
    
    if (!em0_has_shot)
    {
        em0_delay -= delta_time_sec();
        if (em0_delay <= 0)
        {           
            
            em0_delay = em0_delaystart;
            part_emitter_region(ps0,em0,x,x,y,y,1,1);
            part_emitter_burst(ps0,em0,pt0,1);
            if (em0_single_shot) {em0_has_shot=true}
        }
    }
    
    }
	part_system_update(ps0);
}
sys_life = 1.8;


