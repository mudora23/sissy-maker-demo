event_inherited();

_start();
	_placename(PLACENAME_hospital);
	_main_gui(true);
	_show("BG",spr_bg_hospital,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_HOSPITAL_VISIT)
		_jump("EVENT_HOSPITAL_VISIT");
	else
		_jump("Normal");
		

_border(100); // add content before this

_label("EVENT_HOSPITAL_VISIT");
	_var_set(VAR_EVENT_HOSPITAL_VISIT,true);
	_var_set(VAR_EVENT_FLU,true);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_choice(spr_choice_s,"Continue","EVENT_HOSPITAL_VISIT_CHOICE_1",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Chris doesn’t feel well after spending all his energy...[/c]",false);
	_block();
	
_label("EVENT_HOSPITAL_VISIT_CHOICE_1");
	_dialog("[c=#ff00ff]Chris:[/c] *cough cough*");
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Are you alright?");
	_dialog("[c=#ff00ff]Chris:[/c] Yes I think so, but it’s so cold! *");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Let see... Geez! You’re burning hot!");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Why thank you <3!");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Idiot, I didn’t mean it like that, better get to the hospital.");
	_hide_all(FADE_SLOW);
	_pause(1,0,true);
	_jump("resting");
	

_border(100); // add content before this

_label("Normal");
	_var_set(VAR_EVENT_HOSPITAL_VISIT,true);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_choice(spr_choice_s,"Continue","Normal_CHOICE_1",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Chris doesn’t feel well after spending all his energy...[/c]",false);
	_block();
	
_label("Normal_CHOICE_1");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James, please… Come closer I got to tell you… something… very important…");
	_dialog("[c=#ff00ff]Chris:[/c] Heed my last words, James…");
	_dialog("[c=#6678ff]James:[/c] C’mon you’re going to make it. It’s just flu.");
	_hide_all(FADE_SLOW);
	_pause(1,0,true);
	_jump("resting");

_border(100); // add content before this

_label("resting");
	_var_set(VAR_DAY,var_get(VAR_DAY)+1);
	if var_get(VAR_DAYSREMAINING) > 0
	{
		_var_set(VAR_DAYSREMAINING,var_get(VAR_DAYSREMAINING)-1);
		_void();
	}
	else
	{
		_var_set(VAR_DAYSREMAINING,0);
		_room_goto_ext(room_scene_deadline,true);
	}
	_var_set(VAR_TIME,8);
	_var_add_ext(VAR_HP,(var_get(VAR_HP_MAX)+max_hp_penalty())*0.8,0,var_get(VAR_HP_MAX)+max_hp_penalty());
	_var_add_ext(VAR_ENERGY,(var_get(VAR_ENERGY_MAX)+max_energy_bonus())*0.8,0,var_get(VAR_ENERGY_MAX)+max_energy_bonus());
	_choice(spr_choice_lightgreen,"Go to the next location",noone,room_scene_main_map,fa_center,fa_center);
	_dialog("[c=#FFFF00]Chris stays there for 1 day.[/c]",false);
	_block();
	

_border(100); // add content before this

	
	
	
	
	
	
	







