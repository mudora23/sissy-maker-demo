{
    "id": "80d4bf17-5905-461b-8703-87a956ac5de6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_hospital",
    "eventList": [
        {
            "id": "ef8a6063-1490-4031-8c93-73af914c7775",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80d4bf17-5905-461b-8703-87a956ac5de6"
        },
        {
            "id": "eaa717ed-0a86-468a-bb54-2dddc0dc1b56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "80d4bf17-5905-461b-8703-87a956ac5de6"
        },
        {
            "id": "57e1ba02-c7d4-47f7-bd8f-3678d5890e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "80d4bf17-5905-461b-8703-87a956ac5de6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}