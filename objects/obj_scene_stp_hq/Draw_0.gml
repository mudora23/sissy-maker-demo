event_inherited();
	
_start();
	_placename(PLACENAME_STPHq);
	_main_gui(true);
	_show("BG",spr_bg_STPHq,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_STP_HQ_VISIT)
		_jump("EVENT_STP_HQ_VISIT");
	else if !var_get(VAR_EVENT_DEMO_END) && 
	        var_get(VAR_EVENT_RONDA_RUTHLESS) && 
			var_get(VAR_EVENT_SIMONE_SAPIENZA) && 
			var_get(VAR_JOB_BABYSITTING) >= 4 && 
			(current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening")
		_jump("EVENT_DEMO_END");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Late Night");
		
_border(100); // add content before this
		
_label("Stay");
	_stay(true,true,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("Menu");
	_block();

_label("StayRestHide");
	_stay_rest_hide("Menu");
	_block();
	
_border(100); // add content before this
	
_label("Morning");
	_show("security",spr_char_Security_Fierce,1.8,YNUM_SECURITY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m getting used to this place it’s like being in school again.");
	_jump("Menu");

_border(100); // add content before this

_label("Afternoon");
	_show("security",spr_char_Security_Fierce,1.8,YNUM_SECURITY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] When I was a kid this building didn’t looked half as good as it’s now.");
	_jump("Menu");

_border(100); // add content before this

_label("Evening");
	_show("security",spr_char_Security_Fierce,1.8,YNUM_SECURITY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Wow look at all those cars parked in here.");
	_dialog("[c=#ff00ff]Chris:[/c] What more impressive are the cars parked there! Sport cars, sedans, even limos!");
	_dialog("[c=#6678ff]James:[/c] Looks like STP has its sponsors after all.");
	_jump("Menu");

_border(100); // add content before this

_label("Late Night");
	_show("security",spr_char_Security_Tired,1.8,YNUM_SECURITY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh this was an interesting day!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Sure. Let’s follow everybody else’s example and head home. There is nothing for us here.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] While I still have energy the day isn’t over.");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] *Groan*");
	_jump("Menu");
	
_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","security",FADE_NORMAL);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Go to Main Hall",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Exit","Exit",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Exit");
	_choice(spr_choice_s,"Yes",noone,room_scene_main_map,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main map?[/c]",false);
	_block();

_border(100); // add content before this

_label("Talk");
	if current_time_get_name() == "Morning"
		_jump("Talk Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Talk Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Talk Evening");
	else
		_jump("Talk Late Night");

_border(100); // add content before this

_label("Talk Morning");
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Chris, James, Good morning!");
	
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Wow good morning! Damn Penny is really evil for putting you to work all night.");
	_dialog("[c=#ff3333]Security:[/c] What are you taking about? I was asleep all night.");
	
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I swear I saw you yesterday shuffling around like a zombie. How come?");
	
	_change("security",spr_char_Security_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] You saw me? I don’t remember that.");
	_dialog("[c=#ff00ff]Chris:[/c] Hrrrrmm… ");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Weird…)[/c]");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Afternoon");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](The security woman is mumbling to herself. She looks tired.)[/c]");
	
	_change("security",spr_char_Security_Tired,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Oh so tired I got to go home Zzzzz.");
	
	_change("security",spr_char_Security_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c]… Oh hey you! identify yourselves.");
	
	_play_sfx(sfx_block);
	_pause(1,0,true);
	_change("security",spr_char_Security_Tired,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Authorized personnel only! Ah it’s you…");
	
	_change("security",spr_char_Security_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] You kids should go inside to avoid unwanted attention.");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Evening");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](The Security Guard seems really energetic. She’s even whistling a happy song.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] Are you hopped up on energy drinks? Those are bad for you, you know...");
	_dialog("[c=#ff3333]Security:[/c] No way, I don’t need that bullshit.  This is one hundred percent natural energy here.");
	_dialog("[c=#6678ff](She’s either lying or she’s one tough lady.)[/c]");
	_jump("Menu");

_border(100); // add content before this

_label("Talk Late Night");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("security",spr_char_Security_Tired,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Zzzzz…");
	_dialog("[c=#ff3333]Security:[/c] Zzzzzzzzzzzz…");
	
	_change("security",spr_char_Security_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] !!!!");
	
	_play_sfx(sfx_block);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Security:[/c] Authorized personnel only!! Oh, it’s you…");
	
	_change("security",spr_char_Security_Tired,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] STP is closed.  Just go home.");
	_dialog("[c=#ff00ff]Chris:[/c] What about you?");
	_dialog("[c=#ff3333]Security:[/c] My shift’s done in the morning.");
	_dialog("[c=#ff00ff]Chris:[/c] huh?");
	_jump("Menu");

_border(100); // add content before this

_label("EVENT_STP_HQ_VISIT");
	_var_set(VAR_EVENT_STP_HQ_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Here we are in the factory of fabulous!");
	_dialog("[c=#6678ff]James:[/c] I thought this was a cosmetics factory...");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I know this neighborhood, but I never imagined that STP headquarters were here in this building.");
	_dialog("[c=#6678ff]James:[/c] Well… keeping secrets is their business.");
	
	_pause(1,0,true);
	_play_sfx(sfx_block);
	_if(var_get(VAR_AFF_SECURITY)<1,_var_set,VAR_AFF_SECURITY,1);
	_show("security",spr_char_Security_Angry,1.8,YNUM_SECURITY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("*: Stop right there, only authorized personnel can enter this building.");
	
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Whoa! She’s big!");
	
_label("EVENT_STP_HQ_VISIT_CHOICE_1");
	_hide_all_except("BG","security",FADE_NORMAL);
	_choice(spr_choice_s,"Talk","EVENT_STP_HQ_VISIT_CHOICE_1_1",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Use Item","EVENT_STP_HQ_VISIT_CHOICE_1_2",noone,fa_right,fa_center);
	_block();
	
_label("EVENT_STP_HQ_VISIT_CHOICE_1_1");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What’s is in this building?");
	_change("security",spr_char_Security_Fierce,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] This is a training center and agency.");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James show him our invitation.");
	_jump("EVENT_STP_HQ_VISIT_CHOICE_1");
	
_label("EVENT_STP_HQ_VISIT_CHOICE_1_2");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_play_sfx(sfx_get_item);
	_draw_item_popup(spr_item_invitation_popup);
	_dialog("[c=#6678ff](I handed over the card Penny gave me.  The Security guard looks it over briefly and hands it back.)[/c]");
	
	_change("security",spr_char_Security_Fierce,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Hmm… Looks legit.  Just a minute.");

	_play_sfx(sfx_radio);
	_pause(1,0,true);
	_stop_sfx(sfx_radio);
	_dialog("[c=#ff3333]Security:[/c] Hmm. You must be Chris, please come in.");
	
	_pause(1,0,true)
	_play_sfx(sfx_block);
	
	_change("security",spr_char_Security_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Hold on, who are you?");
	
	_hide_all_except("BG","security",FADE_NORMAL);
	
	_choice(spr_choice_s,"...Lover","EVENT_STP_HQ_VISIT_CHOICE_2_1",noone,fa_right,fa_center,true);
	_choice(spr_choice_s,"...Dear friend","EVENT_STP_HQ_VISIT_CHOICE_2_2",noone,fa_right,fa_center,true);
	_choice(spr_choice_s,"...Manager","EVENT_STP_HQ_VISIT_CHOICE_2_3",noone,fa_right,fa_center,true);
	_dialog("[c=#6678ff]James:[/c] I’m Chris’ ...",false);
	_block();
	
_label("EVENT_STP_HQ_VISIT_CHOICE_2_1");
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_jump("EVENT_STP_HQ_VISIT_after_CHOICE_2");
	
_label("EVENT_STP_HQ_VISIT_CHOICE_2_2");
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_jump("EVENT_STP_HQ_VISIT_after_CHOICE_2");
	
_label("EVENT_STP_HQ_VISIT_CHOICE_2_3");
	_var_add_ext(VAR_AFF_SECURITY,1,1,4);
	_jump("EVENT_STP_HQ_VISIT_after_CHOICE_2");
	

_label("EVENT_STP_HQ_VISIT_after_CHOICE_2");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] Hmmm… Just a minute.");
	
	_play_sfx(sfx_radio);
	_pause(1,0,true);
	_stop_sfx(sfx_radio);
	
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You embarrasses me!");
	
	_change("security",spr_char_Security_Fierce,FADE_NORMAL);
	_dialog("[c=#ff3333]Security:[/c] James huh?");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](She’s scanning me head to toe.)[/c]");
	_dialog("[c=#ff3333]Security:[/c] You can go in. Ms. Hardth is waiting for you in the main hall.");

	_hide_all_except("BG","security",FADE_NORMAL);
	
	_choice(spr_choice_s,"Go to Main Hall",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"Exit",noone,room_scene_main_map,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("EVENT_DEMO_END");
	_choice(spr_choice,"Continue","EVENT_DEMO_END_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Chris is startled after hearing his name on the speaker phone.[/c]",false);
	_block();
	
_label("EVENT_DEMO_END_START");
	_void();//_var_set(VAR_EVENT_DEMO_END,true);
	_play_sfx(sfx_speaker);
	_pause(2,0,true);
	_stop_sfx(sfx_speaker);
	_dialog("Speaker phone voice: Trainee Chris Ashee, Trainee Chris Ashee, please report to my Office.");
	
	_play_sfx(sfx_steps);
	_hide_all(FADE_NORMAL);
	_pause(2,0,true);
	_stop_sfx(sfx_steps);
	_placename(PLACENAME_STPmainhall);
	_show("BG",spr_bg_STPmainHall,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Smug,1.8,YNUM_PENNY,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_dialog("[c=#ff3333]Penny:[/c] Please James wait outside, I need to talk with Chris, in private.");
	
	_hide("penny",FADE_NORMAL);
	_hide("chris",FADE_NORMAL);
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](When I left Penny’s office, I got a bad feeling, one I couldn’t describe.  It wasn’t like I had much of a choice, I was just the guy paying the bills.)[/c]");
	
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](But then when I saw Ronda, I couldn’t he’ll but feel my worry was justified.)[/c]");
	
	_show("ronda",spr_char_Ronda_Smug,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] What are you doing here? Where is Chris?");
	_dialog("[c=#6678ff]James:[/c] Inside, talking to Penny…");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] You’re eavesdropping!");
	_dialog("[c=#ff3333]Ronda:[/c] Shhhhhh!");
	_dialog("[c=#6678ff]James:[/c] It’s wrong, tell you.");
	
	_change("ronda",spr_char_Ronda_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Shut up! I want to hear!");
	_dialog("[c=#ff3333]Penny:[/c] I see so much talent in you… I was looking for this day since I’ve smacked your bubble butt the first time... Behold The Ruler of STP.");
	_dialog("[c=#ff3333]Ronda:[/c] Gasp! N-Not the Ruler! No… It’s too soon for that…");
	_dialog("[c=#6678ff]James:[/c] W-What’s going on?");
	_dialog("[c=#ff3333]Ronda:[/c] Oh poor Chris so young… I can’t bear it.");
	
	_hide_all(FADE_SLOW);
	_pause(2,0,true);
	_placename(PLACENAME_STPpennyOffice);
	_show("BG",spr_bg_STPpennyOffice,5,5,1,DEPTH_BG,false,FADE_SLOW);
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_show("chris",spr_char_Chris_Blush,8,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_dialog("[c=#ff00ff](I was really excited, but at the same time really really nervous.  What could Miss Penny want from me?)[/c]");
	_dialog("[c=#ff3333]Penny:[/c] Now look at you Chris… I’m impressed by your development.");
	_dialog("[c=#ff3333]Penny:[/c] The time you’ve spent with Ronda really paid off.");
	_dialog("[c=#ff3333]Penny:[/c] Your Grades with Simone Classes are also very good.");
	_dialog("[c=#ff3333]Penny:[/c] And the Kane’s are very happy with your services.");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] That’s why you’re here to today, every Trainee from STP needs to got through a initiation, now you got to meet The Ruler.");
	
	_hide("penny",FADE_NORMAL);
	_hide("chris",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_main_gui(false);
	_show("CG",spr_hentai_cg_penny_side_angle,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff3333]Penny:[/c] STP is a very serious and secret institution, now that you’re here there is no turning back!");
	_dialog("[c=#ff00ff]Chris:[/c] I’ll not turn back, the same for my decision to become a woman.");
	_dialog("[c=#ff3333]Penny:[/c] Very well. The take off your clothes.");
	
	_change("CG",spr_hentai_cg_penny_down_angle,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff3333]Penny:[/c] The Ruler is especially shaped in order to attack pressure point of nerves at the base of the spine, causing temporary neurological paralysis and a shift of consciousness.");
	_dialog("[c=#ff3333]Penny:[/c] In fewer words IFYBO… It Fucks Your Brains Out!");
	_dialog("[c=#ff3333]Penny:[/c] Each level enforces our alliance our trust.");
	
	_change("CG",spr_hentai_cg_penny_mounted,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff00ff]Chris:[/c] P-please Penny, it’s too freakin’ big! It’s going to tear me apart!");
	_dialog("[c=#ff3333]Penny:[/c] I could do that indeed. But do not worry… You will not endure it until the last level… Not yet!");
	_dialog("[c=#ff3333]Penny:[/c] Let the initiation ritual of sodomy begin!");
	
	_hide("DIM",FADE_NORMAL);
	_hide("CG",FADE_NORMAL);
	_change("BG",spr_bg_STPHq,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] NOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!");
	
	_pause(2,0,true);
	_var_set(VAR_EVENT_DEMO_END,true);
	_execute(time_hour_passed,2);
	
	_choice(spr_choice,"Continue playing",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_choice(spr_choice,"Return to main menu",noone,room_title_screen,fa_center,fa_center);
	_dialog("[c=#FFFF00]This is the end of the demo.[/c]",false);
	_block();

_border(100); // add content before this
