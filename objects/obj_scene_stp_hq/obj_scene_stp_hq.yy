{
    "id": "7f8896d9-a99b-4968-954f-c72c105ba532",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_hq",
    "eventList": [
        {
            "id": "e0743f78-a3a0-46da-979d-bfa5fb2e1d53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f8896d9-a99b-4968-954f-c72c105ba532"
        },
        {
            "id": "477ec913-e22b-4e1b-a534-b43215a09487",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "7f8896d9-a99b-4968-954f-c72c105ba532"
        },
        {
            "id": "d02c6175-3f0c-4dc2-8578-75eee2bc9230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7f8896d9-a99b-4968-954f-c72c105ba532"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}