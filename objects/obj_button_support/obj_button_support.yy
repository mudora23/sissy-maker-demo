{
    "id": "de428cd5-9738-4264-aa9e-9b348ee23ad3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_support",
    "eventList": [
        {
            "id": "e7e4072f-eb26-4be3-8dfa-cbfa5c4b2036",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de428cd5-9738-4264-aa9e-9b348ee23ad3"
        },
        {
            "id": "8ec7c3bd-72b0-496f-a0a5-6144ce6bb4c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "de428cd5-9738-4264-aa9e-9b348ee23ad3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "visible": true
}