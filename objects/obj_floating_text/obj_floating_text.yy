{
    "id": "46c97d0b-5eaf-448b-9cc0-dd782e169ff8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floating_text",
    "eventList": [
        {
            "id": "749c9769-0ba5-401f-b770-1b952bf9e6cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46c97d0b-5eaf-448b-9cc0-dd782e169ff8"
        },
        {
            "id": "e5f65333-22bc-475d-8380-bb8293c8d07b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "46c97d0b-5eaf-448b-9cc0-dd782e169ff8"
        },
        {
            "id": "9e275ae2-e31a-4ec2-bdcb-862286c8fd97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "46c97d0b-5eaf-448b-9cc0-dd782e169ff8"
        }
    ],
    "maskSpriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}