{
    "id": "f0288999-50e7-470b-b01f-600ecefdb65c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stay",
    "eventList": [
        {
            "id": "37992de0-ad26-4486-87b6-e92a14697f05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0288999-50e7-470b-b01f-600ecefdb65c"
        },
        {
            "id": "90410115-926e-4b8a-9409-eeda3781828f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f0288999-50e7-470b-b01f-600ecefdb65c"
        },
        {
            "id": "6f2f2491-ec2d-41e7-ba30-08416055022a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f0288999-50e7-470b-b01f-600ecefdb65c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "97946599-2fb2-4e0f-8db1-dfc621b78a96",
    "visible": true
}