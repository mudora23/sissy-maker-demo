{
    "id": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "d776be59-010e-40c3-9480-f1dad6ab8725",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8aaa39b-0ffd-4757-88ed-d09b191a7643"
        },
        {
            "id": "1ad8510f-f33c-4551-a5b4-0f264f263d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c8aaa39b-0ffd-4757-88ed-d09b191a7643"
        },
        {
            "id": "a5c7367b-9d09-4c00-9467-fae2454cb084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c8aaa39b-0ffd-4757-88ed-d09b191a7643"
        },
        {
            "id": "63b0e674-e3ee-4265-8c21-7903dff0338c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c8aaa39b-0ffd-4757-88ed-d09b191a7643"
        },
        {
            "id": "0422d31c-9158-4b16-8722-53c4e1170ff5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8aaa39b-0ffd-4757-88ed-d09b191a7643"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "45f1b3ce-c391-4290-9b9e-6476e8ec4502",
    "visible": true
}