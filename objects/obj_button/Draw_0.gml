///Draw the button
//if hide_in_sub_menu && sub_menu_active() exit;
var i;
if (press || press_effect)
{
    i = press_index;
}
else
if (hover)
{
    i = hover_index;
}
else
{
    i = idle_index;
}

draw_sprite_ext(sprite_index, i, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);


var old_image_alpha = image_alpha;
draw_set_alpha(image_alpha);

///////////////////
// tooltip
if hover hover_timer++;
else hover_timer = 0;
/*if tooltip != ''
    if hover_timer >= room_speed/2 {
        draw_sprite_ext(spr_tooltip, -1, 1900, 1080-167, (string_width(tooltip)/2)/(sprite_get_width(spr_tooltip)+5), (string_height(tooltip)/2)/(sprite_get_height(spr_tooltip)+5), image_angle, image_blend, image_alpha);
        draw_text_transformed(1900, 1080-167, tooltip, 0.5, 0.5, image_angle);
    }*/
///////////////////

draw_set_font(font_general);
draw_set_valign(fa_middle);
draw_set_halign(fa_middle);
draw_set_color(c_white);

if !is_string(text)
{
    if i = press_index draw_sprite(text,-1,x,y);
    else draw_sprite(text,-1,x,y);
}
else
{
    if i = press_index draw_text(x, y, string_hash_to_newline(text));
    else draw_text(x, y, string_hash_to_newline(text));
}
draw_reset();




/* */
/*  */
