


///State of the button

if image_alpha > 0
{
    ///Check for being pressed
    if (mouse_check_button(mb_left))
    {
        //Check if the mouse button has been released
        if (obj_control.left_pressed)
        {
            press = false;
            
            if mouse_over() && (!instance_exists(obj_main_gui) || obj_main_gui.menu_open == "")
            {
                //Set off "Pressed" event to do differet
                //things depending on the button being pressed
                event_user(0);
                obj_control.left_pressed = false;
            }
        }
    }
    else
    {
        if (mouse_over())
        {
            //Button is being hovered over
            hover = true;
            
            //Is button being pressed?
            if (mouse_check_button_pressed(mb_left))
            {
                press = true;
            }
        }
        else
        {
            hover = false;
        }
    }
}
else
{
    hover = false;
    press = false;
}

