///Set variables
press = false;
press_effect = false;
hover = false;
text = "";
hide_in_sub_menu = false;
image_alpha = 0;
hide_between_scenes = false;
between_scenes = false;
tooltip = "";
hover_timer = 0;

///Image indices for each "state"
idle_index = 0;
hover_index = 1;
press_index = 2;

depth = DEPTH_GUI;

/// fading delay
alarm[0]=20;


