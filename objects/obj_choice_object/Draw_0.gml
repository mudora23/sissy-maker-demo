if delay > 0 delay-=delta_time_sec();
else if !hiding
{
	x = smooth_approach(x,xshow,3*delta_time_sec());
	y = smooth_approach(y,yshow,3*delta_time_sec());
	image_alpha = approach(image_alpha,1,3*delta_time_sec());
}
else
{
	x = smooth_approach(x,xshow,3*delta_time_sec());
	y = smooth_approach(y,yshow,3*delta_time_sec());
	if clicked image_alpha = approach(image_alpha,0,3*delta_time_sec());
	else image_alpha = approach(image_alpha,0,3*delta_time_sec()*2);
	if image_alpha == 0 instance_destroy();
}

if !menu_exists() && !debug_menu_exists() && !popup_menu_exists()
{
	draw_reset();
	hover = !hiding && image_alpha >= 0.95 && mouse_over_area_center(x,y,sprite_width,string_height_auto(choice_text,30,sprite_width)+obj_control.choice_padding*2);
	draw_sprite_ext(sprite_index,hover,x,y,1,(/*max(string_height(choice_text),*/string_height_auto(choice_text,30,sprite_width)/*)*/+obj_control.choice_padding*2)/sprite_height,0,c_white,image_alpha);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_alpha(image_alpha);
	draw_text_auto(x,y,choice_text,30,sprite_width);
	draw_reset();


	if !hiding && hover && obj_control.action_button_pressed
	{
		scene_play_sfx(sfx_click);
		obj_control.action_button_pressed = false;
		clicked = true;
		if choice_label != noone
			scene_jump(choice_label);
		if choice_room != noone
			room_goto_ext(choice_room,true);
	
		with(obj_choice_object) hiding = true;
	}
}
