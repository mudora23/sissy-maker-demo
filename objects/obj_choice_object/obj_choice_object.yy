{
    "id": "a1696454-f154-4913-bfb9-e384849e80f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_choice_object",
    "eventList": [
        {
            "id": "d3d114a6-c45a-44cd-b905-11daa9ce0d80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1696454-f154-4913-bfb9-e384849e80f0"
        },
        {
            "id": "e627b6ee-a0d7-4cb8-87c5-c10d4e51d4fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a1696454-f154-4913-bfb9-e384849e80f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}