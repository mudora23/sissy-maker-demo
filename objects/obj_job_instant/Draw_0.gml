if hiding || job_name == ""
{
    shadow_image_alpha = approach(shadow_image_alpha, 0, 1.2*delta_time_sec());
    x = smooth_approach(x,x_hide,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_hide,slide_speed*delta_time_sec());
}
else
{
    shadow_image_alpha = approach(shadow_image_alpha, 0.9, 1.2*delta_time_sec());
    x = smooth_approach(x,x_target,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_target,slide_speed*delta_time_sec());
}

if hiding && shadow_image_alpha == 0
{
    instance_destroy();
}
    
// BG
draw_sprite_ext(spr_black,-1,0,0,1,1,0,c_white,shadow_image_alpha);

if job_name != "" && !processed
{

	// calculating
	if job_name == JOBNAME_Show_Girl
	{
		total_hours_worked = 0;
		total_hours_max = 10;
		work_until = 6;
		energy_per_two_hours = -14;

		while(var_get(VAR_TIME)!=work_until)
		{
			time_hour_passed(2);
			total_hours_worked+=2;
			var_add_ext(VAR_ENERGY,energy_per_two_hours,0,var_get(VAR_ENERGY_MAX)+max_energy_bonus());
		}
		
		total_hours_worked = clamp(total_hours_worked,0,total_hours_max);

		repeat(skills_xp_get_level(var_get(VAR_SKILL_JAZZ))+skills_xp_get_level(var_get(VAR_SKILL_SALSA))+var_get(VAR_STAMINA)+var_get(VAR_SEX_APPEAL))
			success += choose(0,1);
				
		work_ratio = total_hours_worked / total_hours_max;
		payment = 140+30*success;
		payment_real = payment * sqr(work_ratio);
		if payment_real == 0
		{
		    var_add_ext(VAR_SKILL_JAZZ,var_get(VAR_SKILL_JAZZ)/2,0,var_get(VAR_SKILL_JAZZ));
			var_add_ext(VAR_SKILL_SALSA,var_get(VAR_SKILL_SALSA)/2,0,var_get(VAR_SKILL_SALSA));
		}
		else
		{
			var_add(VAR_SKILL_JAZZ,ceil(success/2));
			var_add(VAR_SKILL_SALSA,ceil(success/2));
		}
		var_add(VAR_JOB_SHOWGIRL,1);
		var_add(VAR_CASH,payment_real);
			
		// rating
		rate = min(success,4); // 0 ~ 4
			
		rate_real = round(rate * work_ratio);
		if rate_real == 1
			rate_real_letter = "C";
		else if rate_real == 2
			rate_real_letter = "B";
		else if rate_real == 3
			rate_real_letter = "A";
		else if rate_real >= 4
			rate_real_letter = "S";
		else
			rate_real_letter = "Unsatisfied";

		if payment-payment_real == 0
		{
			des = "SUCCESSFUL!";
			scene_play_sfx(sfx_job_good);
		}
		else if rate_real_letter == "Unsatisfied" || rate_real_letter == "C"
		{
			des = "FINISHED!";
			scene_play_sfx(sfx_job_bad);
		}
		else
		{
			des = "WELL DONE!";
			scene_play_sfx(sfx_job_good);
		}
		
	}

	processed = true;
}


if processed
{
	draw_self();
    
	draw_set_valign(fa_center);
	draw_set_halign(fa_center);
	draw_set_color(c_white);
	draw_set_font(font_general_30);
	draw_text(x+490,y+60,"A "+string(job_name)+" job is");
	draw_set_color(make_color_rgb(90, 237, 102));
	draw_set_font(font_general_50b);
	draw_text(x+490,y+140,des);

	draw_set_valign(fa_top);
	draw_set_halign(fa_left);
	draw_set_color(c_white);
	draw_set_font(font_general_20b);
	draw_text(x+296,y+200,"Client Satisfaction Rate:");
	draw_set_color(make_color_rgb(176, 252, 183));
	draw_text(x+610,y+200,rate_real_letter);
	draw_set_color(c_white);
	draw_text(x+296,y+240,"Late Penalty:");
	draw_set_color(make_color_rgb(176, 252, 183));
	draw_text(x+470,y+240,"$"+string(payment-payment_real));
	draw_set_color(c_white);
	draw_text(x+296,y+280,"Earnings:");
	draw_set_color(make_color_rgb(176, 252, 183));
	draw_text(x+421,y+280,"$"+string(payment_real));
	draw_reset();
}


