{
    "id": "b125094e-6bff-43c6-afff-d9a04e11a50d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_job_instant",
    "eventList": [
        {
            "id": "cda1f360-6f13-4351-bb5d-72f8ea0089da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b125094e-6bff-43c6-afff-d9a04e11a50d"
        },
        {
            "id": "661d8bb0-7c86-4d10-855b-2bea9465abed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b125094e-6bff-43c6-afff-d9a04e11a50d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "54500535-6165-43b4-9b38-89fc939f7c6e",
    "visible": true
}