{
    "id": "f25182f9-8f49-4c9a-80d2-09ddeef8da0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particle_shinny_screen",
    "eventList": [
        {
            "id": "899a6ff1-a868-41d5-9a43-06e80632b890",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f25182f9-8f49-4c9a-80d2-09ddeef8da0c"
        },
        {
            "id": "d491ac26-2133-4947-820c-440445f72174",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f25182f9-8f49-4c9a-80d2-09ddeef8da0c"
        },
        {
            "id": "d91bf5e8-5683-41b5-b11d-b80cf09556d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "f25182f9-8f49-4c9a-80d2-09ddeef8da0c"
        },
        {
            "id": "61e6b4b7-12ed-46c0-a7a4-7c39862dcd1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f25182f9-8f49-4c9a-80d2-09ddeef8da0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}