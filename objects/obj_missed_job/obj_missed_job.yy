{
    "id": "0e76e366-3be1-4a34-8bbc-56a6289b5f5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_missed_job",
    "eventList": [
        {
            "id": "fe4dff90-273b-45cc-a69a-1b24ea9eaa76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e76e366-3be1-4a34-8bbc-56a6289b5f5e"
        },
        {
            "id": "c03f93cf-d782-414a-96d8-5d19f5745a30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0e76e366-3be1-4a34-8bbc-56a6289b5f5e"
        },
        {
            "id": "61b67f51-b117-4285-9356-4afc50a32364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0e76e366-3be1-4a34-8bbc-56a6289b5f5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6f58d52a-7375-4f1e-9f0b-71232ca1a9ba",
    "visible": true
}