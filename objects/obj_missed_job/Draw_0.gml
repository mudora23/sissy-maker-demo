if !leaving
{
    shadow_image_alpha = approach(shadow_image_alpha, 0.9, 1.2*delta_time_sec());
    x = smooth_approach(x,x_target,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_target,slide_speed*delta_time_sec());
}
else
{
    shadow_image_alpha = approach(shadow_image_alpha, 0, 1.2*delta_time_sec());
    x = smooth_approach(x,x_hide,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_hide,slide_speed*delta_time_sec());
}


if leaving && shadow_image_alpha == 0
    instance_destroy();

draw_sprite_ext(spr_black,-1,0,0,1,1,0,c_white,shadow_image_alpha);
draw_self();

draw_set_valign(fa_center);
draw_set_halign(fa_center);
draw_set_color(c_white);
draw_set_font(font_general_30);
draw_text(x+450,y+60,string_hash_to_newline("A "+string(job_name)+" job is"));
draw_set_color(make_color_rgb(239, 86, 99));
draw_set_font(font_general_50b);
draw_text(x+450,y+140,string_hash_to_newline("MISSED!"));

draw_set_valign(fa_top);
draw_set_halign(fa_left);
draw_set_color(c_white);
draw_set_font(font_general_20b);
draw_text(x+250,y+200,string_hash_to_newline("Client Satisfaction Rate:"));
draw_set_color(make_color_rgb(224, 118, 127));
draw_text(x+570,y+200,string_hash_to_newline("Unsatisfied"));
draw_set_color(c_white);
draw_text(x+250,y+240,string_hash_to_newline("Earnings:"));
draw_set_color(make_color_rgb(224, 118, 127));
draw_text(x+375,y+240,string_hash_to_newline("$0"));
draw_set_color(c_white);
draw_text(x+250,y+280,string_hash_to_newline("Current "+string(job_name)+" Skill Level:"));
draw_set_color(make_color_rgb(224, 118, 127));
draw_text(x+670,y+280,string_hash_to_newline(skills_xp_get_level(var_get(var_skill_get_from_job_name(string(job_name))))));
draw_reset();


if !menu_exists() && !debug_menu_exists() && obj_control.action_button_pressed && shadow_image_alpha > 0.8
{
    obj_control.action_button_pressed = false;
    leaving = true;
}
