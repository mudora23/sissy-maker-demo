event_inherited();

_start();
	_placename(PLACENAME_apt);
	_show("BG",spr_bg_buildingFront,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_void();//_add_item(ITEM_Job_Assignments);
	_add_item(ITEM_Gum);
	_execute(inventory_reposition_item_to_first,ITEM_Gum);
	_void();//_execute(inventory_reposition_item_to_first,ITEM_Job_Assignments);
	_play_sfx(sfx_traffic);
	_pause(1,0,true);
	
	_dialog("*: It’s good to be home!");

    _stop_sfx(sfx_traffic);
    _play_sfx(sfx_steps);
	_change("BG",spr_black,FADE_SLOW);
	_pause(1.5,0,true);
	_change("BG",spr_bg_intro,FADE_SLOW);
	_stop_sfx(sfx_steps);
	_show("chris",spr_char_sp_JamesIntro,0,1,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_move("chris",5,1,SLIDE_NORMAL);
	
	_dialog("[c=#6678ff](My name is James Lakin, I’m 24 years old, I’ve lived here in my grandfather’s apartment building since I was a kid.)[/c]");
	_dialog("[c=#6678ff](My grandfather loved games, in general I mean. If it was playable, he’d play it. He collected games from all over the world, board games, card games, dice games, puzzles­­ and of course­­ video games.)[/c]");
	_dialog("[c=#6678ff](When I was 5, he hooked me on video games and became game pals from then on.)[/c]");
	_dialog("[c=#6678ff](That is, until he died two years ago, age 87. He was a lucid old man right up to the end.)[/c]");
	_dialog("[c=#6678ff](Granddad put me as the sole owner of the apartment building in his will. He owned other property, given to other family members, but that doesn’t matter to me. This place is special to me.)[/c]");
	_dialog("[c=#6678ff](It’s big, the rooms are spacious and the neighborhood? College party town right in the center of the action.)[/c]");
	_dialog("[c=#6678ff](I don’t have any great aspirations, so for the time being my game plan is to live off the resources I have.)[/c]");
	_dialog("[c=#6678ff](I make ends meet as a Landlord, renting rooms to fellow college students.)[/c]");
	_dialog("[c=#6678ff](And one particular tenant is quite troublesome.)[/c]");

	_change("BG",spr_bg_aptLounge,FADE_SLOW);
	_hide("chris",FADE_NORMAL);
	_pause(1,0,true);
	
_label("Shadows");
	_shadow(spr_bg_aptLounge_shadow_dice,"Game",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_door1,"Chris",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_door2,"Empty Room",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_door3,"Empty Room",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_DVD,"Party",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_painting,"Game",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_pizza_box_and_cans,"Party",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_aptLounge_shadow_rent_bills,"Bills",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_block();

_border(100); // add content before this

_label("Empty Room");
	_dialog("[c=#6678ff]James:[/c] No one lives here yet.");
	_jump("Shadows");

_border(100); // add content before this

_label("Party");
	_dialog("[c=#6678ff]James:[/c] Someone had a DVD party while I was away... and didn’t bother putting anything back.  What a mess!");
	_jump("Shadows");
	
_label("Game");
	_dialog("[c=#6678ff]James:[/c] A fine example of granddad’s love of games.");
	_jump("Shadows");
	
_label("Bills");
	_dialog("[c=#6678ff]James:[/c]  The mailbox, stuffed with unpaid bills… 10 months back rent from me alone… hmm… might need to crack down.");
	_jump("Shadows");
	
_label("Chris");
    _play_sfx(sfx_door_opening);
	_dialog("[c=#6678ff]James:[/c] What the...");
	_jump("ChrisRoom");
	
_label("ChrisRoom");	
	_change("BG",spr_bg_aptBedroom,FADE_NORMAL);
	_placename(PLACENAME_bedroom);
	_play_bgm(snd_opening);
	_show("chris",spr_char_sp_ChrisCloseUp,5,5,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,60,false);
	_hide("chris",FADE_FAST);
	_show("chris",spr_char_sp_ChrisBlanket01,2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	
	_dialog("*: It’s my room! Can’t I have some privacy?");
	
	_show("chris",spr_char_James_Angry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	
	_dialog("[c=#6678ff]James:[/c] If you wanted privacy, you should have locked the door.");
	_dialog("*: At least knock before barging in! You almost saw me naked *blushing* / *Chris blushes*");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	
	_dialog("[c=#6678ff](The door was open, she was watching porn aloud, it’s almost like she did it on purpose.)[/c]");
	
	_choice(spr_choice,"Ha, that’s hilarious.  Trying to seduce me isn’t paying your rent.","choice1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"\"Well, with all the moaning and wet noises.  Sounded like a naughty girl was doing something fun.\"","choice1_2",noone,fa_center,fa_center,true);
	_block();

_label("choice1_1");	
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_dialog("*: You’re so boooooooring!");
	_jump("after_choice1");

_label("choice1_2");	
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_dialog("*: You’re a peeping tom! <3!");
	_dialog("[c=#6678ff]James:[/c] Maybe… but I’m here on business.  I need rent, not a peep show.");
	_jump("after_choice1");
	
_label("after_choice1");
	_hide("chris",FADE_NORMAL);
	_hide("james",FADE_NORMAL);
	_change("BG",spr_bg_intro,FADE_SLOW);
	_show("chris",spr_char_sp_ChrisBlanket02,5,1,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_stop_bgm();
	
	_dialog("[c=#6678ff](This is Christopher Ashee. Most people call her Chris.  When people call her Christopher or slip up and say ‘he’, you’re basically picking a fight.)[/c]");
	_dialog("[c=#6678ff](After 10 months of rent arrears, I could easily evict Chris from my apartment.  Maybe I’m stunned by her audacity.  Maybe I’m still trying to figure why I keep her around.)[/c]");
	_dialog("[c=#6678ff](Her mannerisms make it hard to find a job. Maybe it’s pity.  Maybe it’s something else…)[/c]");

	_hide("chris",FADE_NORMAL);
	_pause(1,0,true);
	_change("BG",spr_bg_aptBedroom,FADE_SLOW);
	
	_show("chris",spr_char_sp_ChrisBlanket01,2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	
	_dialog("[c=#ff00ff]Chris:[/c] I told you that I’m saving money for my transformation, you should be more supportive. I want to look cute for you <3!");
	_dialog("[c=#6678ff](Right now, Chris is just a crossdresser. One that only sometimes passes as young woman.  It’s probably obvious, but she dreams to become a full-fledged transsexual.)[/c]");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	
	_dialog("[c=#6678ff]James:[/c] Supportive?  Clean up your mess and I’ll think about it.");
	
	_play_sfx(sfx_doorbell);
	_pause(2,0,true);
	_change("BG",spr_black,FADE_SLOW);
	_hide("chris",FADE_NORMAL);
	_hide("james",FADE_NORMAL);
	_pause(1.5,0,true);
	_change("BG",spr_bg_aptLounge,FADE_SLOW);
	_show("chris",spr_char_Chris_Neutral,2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	
	_dialog("[c=#6678ff]James:[/c] Waiting for somebody?");
	_dialog("[c=#ff00ff]Chris:[/c] No.");
	_dialog("[c=#ff00ff]Chris:[/c] I got it.");
	
	_play_sfx(sfx_intercom);
	_pause(2,0,true);
	_stop_sfx(sfx_intercom);
	
	_dialog("[c=#ff00ff]Chris:[/c] Hello! Who is this?");
	_dialog("*: How cute! You must be Chris Ashee!  Your voice is so soft and delicate.");
	_dialog("[c=#ff3333]Penny :[/c] I’m Penny Hardth from STP app. I’d like to talk you in person.");
	
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);

	_dialog("[c=#ff00ff]Chris:[/c] STP app? What??? I can’t believe it!");
	
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);

	_dialog("[c=#ff00ff]Chris:[/c] Please come on in.");
	
	_change("BG",spr_bg_intro,FADE_SLOW);
	_hide("chris",FADE_SLOW);
	_hide("james",FADE_SLOW);
	_show("penny",spr_char_sp_PennyBig,5,0.2,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show_particle(0,0,DEPTH_CHARACTER-1,obj_particle_shinny_screen);
	_move("penny",5,1,200);
	_pause(5,0,false);
	_hide_particle(obj_particle_shinny_screen);
	_hide("penny",FADE_SLOW);
	_change("BG",spr_bg_aptLounge,FADE_SLOW);
	_show("chris",spr_char_Chris_Blush,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Blush,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,2,YNUM_PENNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_var_set(VAR_AFF_PENNY,1);
	
	_dialog("[c=#ff00ff]Chris:[/c] Whoa, so beautiful!");
	_dialog("[c=#6678ff]James:[/c] Yeah, she’s stunning.");
	_dialog("[c=#ff00ff]Chris:[/c] And that business attire? Super snazzy!");
	_dialog("[c=#6678ff]James:[/c] You don’t see elegance like that everyday...");
	
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] And she’s sooo cute!!  <3");
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	
	_dialog("[c=#6678ff]James:[/c] You’re right though, she looks so young, must be some sort of prodigy.");
	_dialog("[c=#ff00ff]Chris:[/c] Yeah a super prodigy!  She’s like a cute kid, huh?");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] The suit’s custom tailored, for sure.");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, it’s impossible to find stuff like that in the juniors section!");
	
	_change("penny",spr_char_Penny_Burst,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Shut the Fuck up!  Keep that shot up and I’ll rape you head off and make your boyfriend watch.");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Huh?");
	_dialog("[c=#ff3333]Penny:[/c] And don’t think you’re getting of the hook, either.  After I’m done with her, you’re next!");
	
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I don’t know where the violent attitude came from, but it kinda makes me wet.  <3");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Psh, you two are made for each other.");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hold on, we’re not actually dating or anything.");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, James just lives here.");
	
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Actually I own the apartments here. I’m James Lakin, nice to meet you.");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Pleasure’s mine, James. I have... business with Chris.");
	_dialog("[c=#ff00ff]Chris:[/c] Don’t mind him. James is just a curious guy.");
	_dialog("[c=#ff3333]Penny:[/c] Chris, we have been following your activities closely.");
	_dialog("[c=#ff3333]Penny:[/c] You’re partially bursting with potential, so I had to get a closer look at you.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Penny circles around Chris like a shark in bloody waters.  It seems a chill up my spine.  There’s something... off about Penny.  I just can’t put my finger on it.)[/c]");
	_dialog("[c=#ff3333]Penny:[/c] Yes… Yes… very good.");
	_dialog("[c=#ff3333]Penny:[/c] I have an offer to you. I want you as a trainee at STP.");
	
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yes! Please!");
	_dialog("[c=#6678ff]James:[/c] Chris looks so excited she could explode.  I just hope it’s nothing dangerous, a loan shark, maybe?  I’d never heard of this ‘STP’.");
	
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] You’re drooling on my new shoes! Stop that!");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] I’ve come to accept you into the program, but there’s the matter of a small fee...");
	_dialog("[c=#ff00ff]Chris:[/c] Yes, anything! I pay whatever you want! I-");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hold on, you can’t afford rent!  How do you expect to pay for this ‘program’ whatever it is?");
	
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Please just let me slide a little bit longer?  Think of it as an investment!");
	
	_choice(spr_choice,"\"An investment?  Give me a break… \"","choice2_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"\"I’ll think about it.  What is this STP anyway?  Sounds interesting.\"","choice2_2",noone,fa_center,fa_center,true);
	_block();

_label("choice2_1");
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_dialog("[c=#ff00ff]Chris:[/c] *pouts*");
	_dialog("[c=#6678ff]James:[/c] What is STP, anyway?");
	_jump("after_choice2");
	
_label("choice2_2");
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_jump("after_choice2");
	
_label("after_choice2");
	_dialog("[c=#ff3333]Penny:[/c] STP, it stands for Sissy Training Program. We are an institution that helps men become women.");
	_dialog("[c=#ff3333]Penny:[/c] We recently launched a computer applications to awake and gather potential talents.");
	_dialog("[c=#6678ff]James:[/c] So weird porn you watch everyday has something to do with it?");
	_dialog("[c=#ff00ff]Chris:[/c] You’ve been meddling with my computer?!");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Nope, you watch videos with your door open.");
	_dialog("[c=#ff3333]Penny:[/c] The hypnotic videos meant to induce “sissyfication” into youngsters with that inclination. You should try it James, if you ever doubt your masculinity.");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I-I’ll pass, thanks.");
	_dialog("[c=#ff3333]Penny:[/c] STP is a also a job agency and we have plenty of carrier programs for transsexuals in training.");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] James, how much Chris owes you?");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] $"+string(var_get(VAR_DEBT))+".");
	
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] That much? Gosh I didn’t realize it!");
	_dialog("[c=#ff3333]Penny:[/c] I have a proposition for you both: James, support Chris through her training at STP and we’ll hadn’t you total control of her training, management and earnings.");
	_dialog("[c=#ff00ff]Chris:[/c] I’d do anything to be a part of STP, but I’m not gonna be a slave to him forever, right? I mean what if we don’t raise enough money?");
	_dialog("[c=#ff3333]Penny:[/c] I expected you’d say that. We’ll set a deadline for the debt payment.");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] "+string(var_get(VAR_DAYSREMAINING))+" days. If you can’t raise the money until then, You’re free and James can only blame his poor management.");
	_dialog("[c=#ff3333]Penny:[/c] It’s a WIN/WIN deal.");
	_dialog("[c=#ff3333]Penny:[/c] What about it James? It would be a shame if we lose this much potential! *smack!*");
	
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Aaaah <3!");
	_dialog("[c=#6678ff]( I’ve never noticed how jiggly Chris’ butt is. And it could only stand to get better… I have some money to throw around… How hard would it be to do this on the side?   It’d probably be fun and… if I didn’t Chris would just mope around anyway.)[/c]");
	
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Alright, if Chris it’s fine with it, I’m game.");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Good! I see you tomorrow in my office. Take this invitation.");
	
	_add_item_unique(ITEM_Invitation_Letter);
	_draw_item_popup(spr_item_invitation_popup);
	_dialog("[c=#FFFF00]Item acquired: Penny Hardth STP invitation[/c]");
	
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh my gosh! I can’t believe it!  I’m so excited, there’s no way I’ll get a wink of sleep.");
	
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Good, use the free time to clean the mess you left in the hall.");
	
	_hide_all(FADE_SLOW);
	_pause(2,0,true);
	_var_add_ext(VAR_DAYSREMAINING,-1,0,var_get(VAR_DAYSREMAINING));
	_var_set(VAR_DAY,var_get(VAR_DAY)+1);
	_var_set(VAR_TIME,10);
	_play_bgm(snd_apt);
	
	_main_gui(true);
	_show("BG",spr_bg_aptLoungeClean,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Good morning! Let’s get started!");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] We’re going to STP Headquarters, before we do we need to check if everything is in order.");
	
	_choice(spr_choice,"Talk","choice3_1",noone,fa_center,fa_bottom);
	_block();
	
_label("choice3_1");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] How goes?");
	
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m so excited! I’m going to train at STP!");
	
	_choice(spr_choice,"\"I’m excited too, I’m hard just thinking about it!\"","choice4_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"\"I hope it works for you. What’s good for you is good for me.\"","choice4_2",noone,fa_center,fa_center,true);
	_block();
	
_label("choice4_1");
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You’re just excited to see Penny again, aren’t you? Hmpf!");
	_jump("after_choice4");

_label("choice4_2");
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yes, that’s the spirit of partnership!");
	_jump("after_choice4");

_label("after_choice4");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] So whenever you want to talk with me, we meet here in the apartment.");
	
	_choice(spr_choice,"Talk","choice5_1",noone,fa_center,fa_bottom);
	_block();
	
_label("choice5_1");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Enough chit-chat, let’s get going.");
	
	_hide_all(FADE_NORMAL);
	_main_gui(false);
	
	_show("BG",spr_bg_map,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("BG_front",spr_bg_map_front,5,5,1,DEPTH_BG-1,false,FADE_NORMAL);
	
	_show("BG_Apt",spr_bg_map_Apt,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_Hospital",spr_bg_map_Hospital,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_LotusDew",spr_bg_map_LotusDew,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_Park",spr_bg_map_Park,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_GirlsOnlyClub",spr_bg_map_GirlsOnlyClub,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_STPHeadquarters",spr_bg_map_STPHeadquarters,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	
_label("choice_main_map");
	_pause(0.5,0,true);
	_shadow(spr_bg_map_shadow_Apt,"going_Apt",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_Hospital,"going_Hospital",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_LotusDew,"going_LotusDew",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_Park,"going_Park",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_GirlsOnlyClub,"going_GirlsOnlyClub",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_STPHeadquarters,"going_STPHeadquarter",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_block();

_label("going_Apt");
	_dialog("[c=#ff00ff]Chris:[/c] This is our apartment building.");
	_dialog("[c=#6678ff]James:[/c] MY apartment building.");
	_jump("choice_main_map");
	
_label("going_Hospital");
	_dialog("[c=#ff00ff]Chris:[/c] This is the hospital. I hope we don’t need to go there.");
	_jump("choice_main_map");
	
_label("going_LotusDew");
	_dialog("[c=#ff00ff]Chris:[/c] Lotus Dew, my favorite hangout.");
	_jump("choice_main_map");
	
_label("going_Park");
_label("going_GirlsOnlyClub");
	_dialog("[c=#ff00ff]Chris:[/c] This location is locked in the demo. Please consider supporting us on patreon :)");
	_dialog("[c=#6678ff]James:[/c] Okay.");
	_jump("choice_main_map");
	
_label("going_STPHeadquarter");
	_dialog("[c=#ff00ff]Chris:[/c] This is STP building.");
	_choice(spr_choice,"Yes","going_STPHeadquarter processing",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to the STP building?[/c]",false);
	_block();

_label("going_STPHeadquarter processing");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_stp_hq,true);
	_block();
	
	
