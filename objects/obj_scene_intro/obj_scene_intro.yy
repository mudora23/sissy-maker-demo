{
    "id": "cae46b9f-68d3-448b-99e9-3b349db00f15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_intro",
    "eventList": [
        {
            "id": "342b93dd-7cca-4192-bdbb-82b9ac85e9c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cae46b9f-68d3-448b-99e9-3b349db00f15"
        },
        {
            "id": "2678c01f-6e90-486d-9ece-b4102a68d792",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "cae46b9f-68d3-448b-99e9-3b349db00f15"
        },
        {
            "id": "c8ecaee6-5ceb-407d-9558-dda363251b69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cae46b9f-68d3-448b-99e9-3b349db00f15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}