{
    "id": "cb27435d-e0e2-4b47-9be4-5155a4722202",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_raye",
    "eventList": [
        {
            "id": "22acc780-b7fa-486a-a860-98c05e846d9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb27435d-e0e2-4b47-9be4-5155a4722202"
        },
        {
            "id": "e19c371f-fe24-4144-8365-032d9d8cd5bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "cb27435d-e0e2-4b47-9be4-5155a4722202"
        },
        {
            "id": "f0d9e5d3-f93b-407a-be7f-168031c822fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cb27435d-e0e2-4b47-9be4-5155a4722202"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}