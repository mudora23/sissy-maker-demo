event_inherited();

_start();
	_placename(PLACENAME_STPdancehall);
	_main_gui(true);
	_show("BG",spr_bg_STPdanceHall,5,5,1,DEPTH_BG,false,FADE_NORMAL);
 
	if !var_get(VAR_EVENT_RAYE_SANCHES_INTRO) && !var_get(VAR_EVENT_DANCER_LOTUS_RESTROOM) && current_time_get_name() == "Afternoon"
		_jump("EVENT_RAYE_SANCHES_INTRO");
	else if !var_get(VAR_EVENT_RAYE_SANCHES_FUCKED) && var_get(VAR_EVENT_DANCER_LOTUS_RESTROOM) && current_time_get_name() == "Afternoon"
		_jump("EVENT_RAYE_SANCHES_FUCKED");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Stay");
	_stay(true,true,true,false);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Afternoon"
		_jump("Menu");
	else
		_jump(current_time_get_name());

_border(100); // add content before this

_label("Afternoon");
	_show("raye",spr_char_Raye_Neutral,1.2,YNUM_RAYE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Raye:[/c] Vamos mover el esqueleto!");
	_jump("Menu");

_border(100); // add content before this

_label("Morning");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Raye is not here yet.");
	_dialog("[c=#ff00ff]Chris:[/c] I bet she’s been partying all night.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I… I bet she was.");
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The Dance Hall is closed.)[/c]",false);
	_block();
	
_border(100); // add content before this

_label("Evening");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] The coach is gone already.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah… I’d love to dance a little in evenings too.");
	_dialog("[c=#6678ff]James:[/c] Her classes are really something.");
	_dialog("[c=#ff00ff]Chris:[/c] …hmpf!");
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The Dance Hall is closed.)[/c]",false);
	_block();
	
_border(100); // add content before this

_label("Late Night");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_show("chris",spr_char_Chris_Pout,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] It’s too quiet.");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] No point even trying to go there.");
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, no music playing… means no dance class.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] True that.");
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The Dance Hall is closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","raye",FADE_NORMAL);
	_choice(spr_choice_s,"Salsa Class\r(Cost: $180, Energy Cost: 40)","Salsa Class Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"JAZZ Class\r(Cost: $180, Energy Cost: 30)","JAZZ Class Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Salsa Class Intro");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] My favorite dance!  It’s just fun to dance Salsa.  But it’s twice as fun with somebody else, right James?");
	if var_get(VAR_ENERGY) >= 40 && var_get(VAR_CASH) >= 180
		_jump("Salsa Class Confirm");
	else
		_jump("Salsa Class Fail");

_label("Salsa Class Confirm");
	_choice(spr_choice_s,"Yes","Salsa Class Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Begin Salsa Class? (Cost: $180, Energy Cost: 40)[/c]",false);
	_block();
	
_label("Salsa Class Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Salsa Class Confirm Yes");
	_activities(spr_activities_working_out,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-180);
	_var_add(VAR_ENERGY,-40);
	_var_add(VAR_CHARISMA,1);
	_var_add(VAR_SKILL_SALSA,1+success_roll_attribute(VAR_STAMINA)+success_roll_attribute(VAR_DEXTERITY));
	_var_add(VAR_TRAINING_RAYE,1);
	_block();

_border(100); // add content before this

_label("JAZZ Class Intro");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] The estilo libre! Dancing to Jazz improves your dexterity, rhythm and general coordination.  If you can dance to Jazz you probably can dance most of the other styles.");
	if var_get(VAR_ENERGY) >= 30 && var_get(VAR_CASH) >= 180
		_jump("JAZZ Class Confirm");
	else
		_jump("JAZZ Class Fail");

_label("JAZZ Class Confirm");
	_choice(spr_choice_s,"Yes","JAZZ Class Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Begin JAZZ Class? (Cost: $180, Energy Cost: 30)[/c]",false);
	_block();
	
_label("JAZZ Class Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("JAZZ Class Confirm Yes");
	_activities(spr_activities_working_out,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-180);
	_var_add(VAR_ENERGY,-30);
	_var_add(VAR_DEXTERITY,1);
	_var_add(VAR_SKILL_JAZZ,1+success_roll_attribute(VAR_STAMINA)+success_roll_attribute(VAR_DEXTERITY));
	_var_add(VAR_TRAINING_RAYE,1);
	_block();

_border(100); // add content before this

_label("Talk");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Hey Chris do this move!");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](Raye does a sexy dance move, like the one she did at Lotus.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](Damn! Her confidence and that smoking body?  She’s too hot!)[/c]");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](I feel a chill down my spine.  Chris was glaring at me.)[/c]");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Gulp!");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] What?  Is Chris still too shy to try this one?");
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_jump("Menu");


_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main hall?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_RAYE_SANCHES_INTRO");
	_hide_all_except("BG",FADE_NORMAL);
	_choice(spr_choice,"Continue","EVENT_RAYE_SANCHES_INTRO_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris meet the new dance coach.[/c]",false);
	_block();
		
_label("EVENT_RAYE_SANCHES_INTRO_START");
	_var_set(VAR_EVENT_RAYE_SANCHES_INTRO,true);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER+1,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("raye",spr_char_Raye_Neutral,1.2,YNUM_RAYE,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Ah it’s Chris…");
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] …and James the producer!");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Bienvenidos to my dancing classes!");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Raye Sanchez! Good to see you!");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I see that you're acquainted...");
	_dialog("[c=#ff3333]Raye:[/c] See I finally found a job! The best part is that I found it here as dancing coach!");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I saw you dancing at Lotus! You’re so cool!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](I had a feeling Chris was watching us at the club… She must have eyes on the back of her head.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](Well, at least I didn’t accept Raye’s invitation. Chris would have caused a scene!)[/c]");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](...)[/c]");
	_dialog("[c=#6678ff](Why do I care??!!)[/c]");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Earth calling James. Earth Calling James.");
	_change("raye",spr_char_Raye_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] I’m a bit nervous about the classes. It’s one thing to dance, teaching is another way around.");
	_dialog("[c=#ff3333]Raye:[/c] Do you think I can handle it?");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_choice(spr_choice,"“With a talented coach like you, Chris will be dancing like a pro in no time.”","EVENT_RAYE_SANCHES_INTRO_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“Nah, you’ll be fine. Dancing nowadays is all about shaking butts, twerking… stuff like that.”","EVENT_RAYE_SANCHES_INTRO_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_RAYE_SANCHES_INTRO_CHOICE_1_1");
	_var_add_ext(VAR_AFF_RAYE,1,1,4);
	_var_add_ext(VAR_AFF_RONDA,-1,1,4);
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’ll do my best!");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Thanks for the support, amigos!");
	_jump("Menu");

_label("EVENT_RAYE_SANCHES_INTRO_CHOICE_1_2");
	_var_add_ext(VAR_AFF_RAYE,-1,1,4);
	_var_add_ext(VAR_AFF_RONDA,1,1,4);
	_change("raye",spr_char_Raye_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] No me subestimes! I’ll teach how to write poetry with the body!");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I love to shake my butt though.");
	_jump("Menu");


_border(100); // add content before this

_label("EVENT_RAYE_SANCHES_FUCKED");
	_hide_all_except("BG",FADE_NORMAL);
	_choice(spr_choice,"Continue","EVENT_RAYE_SANCHES_FUCKED_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris meet the new dance coach.[/c]",false);
	_block();
		
_label("EVENT_RAYE_SANCHES_FUCKED_START");
	_var_set(VAR_EVENT_RAYE_SANCHES_FUCKED,true);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER+1,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("raye",spr_char_Raye_Happy,1.2,YNUM_RAYE,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] I’m so happy that I found a job here at STP. This place in wonderful! I’m surrounded by Transsexuals, it feels like family.");
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] It would be great if a place like this existed when I was changing myself.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] No kidding, you’re a trans?  I… didn’t notice.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Yeah, I didn’t notice either but…");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_raye_restroom_1_B,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](I really SHOULD have noticed!)[/c]");
	_dialog("[c=#6678ff](Normal girls don’t say “put in my ass first, I like it more in the ass”… ha, as if she had any other option.  Damn! I went in like a fool… literally.)[/c]");
	_dialog("[c=#6678ff](But goddamn! She is too hot! The sex was hot! In fact, it was the best sex I ever had…)[/c]");
	
	_change("CG",spr_hentai_cg_raye_restroom_2_B,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#6678ff](Yeah, I wasn’t complaining in the moment.  Raye is a total package.  Everything about her just screams sex.)[/c]");
	_dialog("[c=#6678ff](The softness of her skin, the smell of her sweat, the way she moved her hips!  I couldn’t ask for anything else.)[/c]");
	
	_change("CG",spr_hentai_cg_raye_restroom_3_1,FADE_SLOW);
	_pause(2,0,true);
	_show("CG2",spr_hentai_cg_raye_restroom_3_2,5,5,1,DEPTH_BG_ELE-1,false,FADE_SLOW);
	_pause(0.5,0,false);
	
	_dialog("[c=#6678ff](Her eagerness when it comes to working a cock with that booty of hers… blows my mind.)[/c]");
	_dialog("[c=#6678ff](She was one hundred percent a woman there.)[/c]");
	_dialog("[c=#6678ff](No way I’d turn down a chance for another round.)[/c]");
	_dialog("[c=#6678ff](I only then realized Chris and Raye were looking straight at me.)[/c]");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_change("BG",spr_bg_STPdanceHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER+1,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Worry,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("raye",spr_char_Raye_Happy,1.2,YNUM_RAYE,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	
	_dialog("[c=#ff00ff]Chris:[/c] ...");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] The hombre is in shock!");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah well, lately it’s been happening a lot.");
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_choice(spr_choice,"“No arguments here, you’re a hundred percent babe!”","EVENT_RAYE_SANCHES_FUCKED_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“Hot or not, you really should warn people about your ‘extra features’...”","EVENT_RAYE_SANCHES_FUCKED_CHOICE_1_2",noone,fa_center,fa_center,true);
	_dialog("[c=#ff3333]Raye:[/c] I believe it was a nice surprise, wasn’t it James?",false);
	_block();
	
_label("EVENT_RAYE_SANCHES_FUCKED_CHOICE_1_1");
	_var_add_ext(VAR_AFF_RAYE,1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_var_add_ext(VAR_AFF_RONDA,-1,1,4);
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James!");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Gracias amigo.");
	if current_time_get_name() == "Afternoon"
		_jump("Menu");
	else
		_jump(current_time_get_name());
	
_label("EVENT_RAYE_SANCHES_FUCKED_CHOICE_1_2");
	_var_add_ext(VAR_AFF_RAYE,-1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_var_add_ext(VAR_AFF_RONDA,1,1,4);
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James! That was terrible apologize us!");
	_change("raye",spr_char_Raye_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] Bueno, at least he admitted that I’m a woman. I’m happy with that. <3");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] You fooled me!");
	_dialog("[c=#ff00ff]Chris:[/c] I got that, but why are you so jumpy about her?");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Nothing…");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Raye:[/c] hahahaha");
	if current_time_get_name() == "Afternoon"
		_jump("Menu");
	else
		_jump(current_time_get_name());
	

_border(100); // add content before this
