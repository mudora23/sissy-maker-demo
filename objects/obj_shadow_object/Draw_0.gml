// image_alpha
if image_alpha_increasing
{
	image_alpha = approach(image_alpha,image_alpha_max,fade_speed*delta_time_sec());
	if image_alpha == image_alpha_max || !mouse_over() image_alpha_increasing = false;
}
else
{
	image_alpha = approach(image_alpha,image_alpha_min,fade_speed*delta_time_sec());
	if image_alpha == image_alpha_min && mouse_over() image_alpha_increasing = true;
}

if mouse_over() && !menu_exists() && !debug_menu_exists() && !popup_menu_exists()
{
	draw_sprite_ext(sprite_index,-1,0,0,1,1,0,c_yellow,image_alpha);
	hover = true;
	if obj_control.action_button_pressed
	{
		scene_play_sfx(sfx_button_01);
		obj_control.action_button_pressed = false;
		if shadow_label != noone
			scene_jump(shadow_label);
		if shadow_room != noone
			room_goto_ext(shadow_room,true);
			
		with(obj_shadow_object) instance_destroy();
	}

}
else
	hover = false;