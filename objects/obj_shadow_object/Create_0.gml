sprite_index = noone;

image_alpha = 0.1;
image_alpha_min = 0.1;
image_alpha_max = 0.3;
image_alpha_increasing = true;

fade_speed = FADE_SLOW;

shadow_label = noone;
shadow_room = noone;


// no need to save
hover = false;