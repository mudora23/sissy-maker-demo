if instance_exists(obj_scene)
{
	if obj_scene.scene_map[? SCENE_TEXT] != ""
	{
		textbox_alpha = smooth_approach(textbox_alpha,1,10*delta_time_sec());
		textbox_y = smooth_approach(textbox_y,600-150,10*delta_time_sec());
	}
	else
	{
		textbox_alpha = smooth_approach(textbox_alpha,0,10*delta_time_sec());
		textbox_y = smooth_approach(textbox_y,600,10*delta_time_sec());
	}

	draw_reset();
	draw_sprite_ext(sprite_index,-1,0,textbox_y,1,1,0,c_white,textbox_alpha);
}
else
{
	textbox_alpha = smooth_approach(textbox_alpha,0,10*delta_time_sec());
	textbox_y = smooth_approach(textbox_y,600,10*delta_time_sec());
}