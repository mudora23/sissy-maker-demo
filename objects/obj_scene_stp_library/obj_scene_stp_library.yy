{
    "id": "e22948f3-9f44-42ff-979e-62a7c976bb05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_library",
    "eventList": [
        {
            "id": "c627eca0-2326-43ba-84ce-e1466fa0a3cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e22948f3-9f44-42ff-979e-62a7c976bb05"
        },
        {
            "id": "2cb4fe76-3662-4f5b-a36e-83c4bf6745c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e22948f3-9f44-42ff-979e-62a7c976bb05"
        },
        {
            "id": "74856311-b625-439a-bbdb-9cc449ea6dce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e22948f3-9f44-42ff-979e-62a7c976bb05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}