event_inherited();
	
_start();
	_placename(PLACENAME_STPlibrary);
	_main_gui(true);
	_show("BG",spr_bg_STPlibrary,5,5,1,DEPTH_BG,false,FADE_NORMAL);

	if !var_get(VAR_EVENT_STP_LIBRARY_VISIT) && 
	   (current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening")
		_jump("EVENT_STP_LIBRARY_VISIT");
	else if !var_get(VAR_EVENT_SIMONE_SAPIENZA) && 
	        var_get(VAR_EVENT_FIRST_MISSION) && 
			var_get(VAR_INTELLIGENCE) >= 2 && 
			current_time_get_name() == "Evening" && 
			(success_roll_attribute(VAR_INTELLIGENCE)+success_roll_attribute(VAR_BARGAIN)+success_roll(VAR_AFF_SIMONE))
		_jump("EVENT_SIMONE_SAPIENZA");
	//else if var_get(VAR_EVENT_SIMONE_SAPIENZA) && 
	//		var_get(VAR_INTELLIGENCE) >= 2 && 
	//		current_time_get_name() == "Evening" && 
	//		(success_roll_hard(VAR_INTELLIGENCE)+success_roll_hard(VAR_BARGAIN)+success_roll_hard(VAR_AFF_SIMONE))
	//	_jump("EVENT_SIMONE_SAPIENZA");
	else if current_time_get_name() == "Morning"
		_jump("Closed");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Closed");

_border(100); // add content before this
	
_label("Stay");
	_stay(false,true,true,false);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Afternoon");
_label("Evening");
	_show("simone",spr_char_Simone_Neutral,1.8,YNUM_SIMONE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_jump("Menu");

_label("Morning");
_label("Late Night");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The library is closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","simone",FADE_NORMAL);
	_choice(spr_choice_s,"Humanities class\r(Cost: $30, Energy Cost: 30)","Humanities class Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Etiquette class\r(Cost: $30, Energy Cost: 30)","Etiquette class Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Babysitting class\r(Cost: $20, Energy Cost: 40)","Babysitting class Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Humanities class Intro");
	_dialog("[c=#ff3333]Simone:[/c] “The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.” Studies of General knowledge, History and Arithmetics.");
	if var_get(VAR_ENERGY) >= 30 && var_get(VAR_CASH) >= 30
		_jump("Humanities class Confirm");
	else
		_jump("Humanities class Fail");

_label("Humanities class Confirm");
	_choice(spr_choice_s,"Yes","Humanities class Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Take the Humanities class? (Cost: $30, Energy Cost: 30)[/c]",false);
	_block();
	
_label("Humanities class Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Humanities class Confirm Yes");
	_activities(spr_activities_studying,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-30);
	_var_add(VAR_ENERGY,-30);
	_var_add(VAR_INTELLIGENCE,1);
	_var_add(VAR_TRAINING_SIMONE,1);
	_block();

_border(100); // add content before this

_label("Etiquette class Intro");
	_dialog("[c=#ff3333]Simone:[/c] This improves your social skills: how to talk, posture, manners and confidence. The lady’s essentials.");
	if var_get(VAR_ENERGY) >= 30 && var_get(VAR_CASH) >= 30
		_jump("Etiquette class Confirm");
	else
		_jump("Etiquette class Fail");

_label("Etiquette class Confirm");
	_choice(spr_choice_s,"Yes","Etiquette class Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Take the Etiquette class? (Cost: $30, Energy Cost: 30)[/c]",false);
	_block();
	
_label("Etiquette class Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Etiquette class Confirm Yes");
	_activities(spr_activities_studying,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-30);
	_var_add(VAR_ENERGY,-30);
	_var_add(VAR_BARGAIN,1);
	_var_add(VAR_TRAINING_SIMONE,1);
	_block();

_border(100); // add content before this

_label("Babysitting class Intro");
	_dialog("[c=#ff3333]Simone:[/c] Babysitting profession, you will learn pedagogy and children psychology. It’s not hard to learn, plenty of job opportunities, but you’ll never get rich doing this.");
	if var_get(VAR_ENERGY) >= 40 && var_get(VAR_CASH) >= 20
		_jump("Babysitting class Confirm");
	else
		_jump("Babysitting class Fail");

_label("Babysitting class Confirm");
	_choice(spr_choice_s,"Yes","Babysitting class Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Take the Babysitting class? (Cost: $20, Energy Cost: 40)[/c]",false);
	_block();
	
_label("Babysitting class Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Babysitting class Confirm Yes");
	_activities(spr_activities_studying,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-20);
	_var_add(VAR_ENERGY,-40);
	_var_add(VAR_SKILL_BABYSITTING,1+success_roll_attribute(VAR_CHARISMA)+success_roll_attribute(VAR_DEXTERITY));
	_var_add(VAR_TRAINING_SIMONE,1);
	_block();

_border(100); // add content before this

_label("Talk");
	_dialog("[c=#ff3333]Simone:[/c] I heard you want a job. If you’re having trouble to learn a new skill, I recommend you study a little before taking professional classes.");
	_jump("Menu");

_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main hall?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_STP_LIBRARY_VISIT");
	_var_set(VAR_EVENT_STP_LIBRARY_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,0.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] This is our library and studies center.");
	_dialog("[c=#ff00ff]Chris:[/c] Where are the professors, teachers and etc? I can only see a student.");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] No, that geek with head buried in the book, is our humanities teacher, Simone. She looks young, but she’s an old rag at heart.");
	
	_if(var_get(VAR_AFF_SIMONE)<1,_var_set,VAR_AFF_SIMONE,1);
	_show("simone",spr_char_Simone_Worry,1.8,YNUM_SIMONE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] \"For you to insult me, I must first value your opinion.\"");
	_dialog("[c=#ff3333]Penny:[/c] Get used to that, she’s always quoting thinkers and philosophers, because it makes her look smart.");
	
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I got one!");
	_dialog("[c=#ff3333]Simone:[/c] ok…");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_choice(spr_choice,"“The only true wisdom is in knowing you know nothing.”","EVENT_STP_LIBRARY_VISIT_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“You want weapons? We’re in a library. Books are the best weapons in the world. This room’s the greatest arsenal we could have.”","EVENT_STP_LIBRARY_VISIT_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_STP_LIBRARY_VISIT_CHOICE_1_1");
	_var_add_ext(VAR_AFF_SIMONE,-1,1,4);
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] But you know, I think in our modern world you better study to be successful.");
	_dialog("[c=#ff3333]Simone:[/c] ……");
	
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] ……");
	_dialog("[c=#ff3333]Penny:[/c] ……");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What?");
	_dialog("[c=#ff00ff]Chris:[/c] I think you got it all wrong!");
	
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I did?");
	
	_jump("EVENT_STP_LIBRARY_VISIT_after_CHOICE_1");
	
_label("EVENT_STP_LIBRARY_VISIT_CHOICE_1_2");
	_var_add_ext(VAR_AFF_SIMONE,1,1,4);
	_change("simone",spr_char_Simone_Thinking,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] That’s… insightful.  Who’s is it?");
	
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Doctor Who.");
	
	_change("simone",spr_char_Simone_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] ……");
	
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] ……");
	
	_jump("EVENT_STP_LIBRARY_VISIT_after_CHOICE_1");

_label("EVENT_STP_LIBRARY_VISIT_after_CHOICE_1");
	_dialog("[c=#ff3333]Penny:[/c] ……");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] *sigh*");
	_dialog("[c=#ff3333]Penny:[/c] Besides being a geek, Simone’s also coordinates the STP classes. If you need to learn anything, here is the place.");

	_change("simone",spr_char_Simone_Thinking,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] “I cannot teach anybody anything. I can only make them think”");

	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] You better teach Chris something, because that’s what you’re paid for.");

	_change("simone",spr_char_Simone_Worry,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] Fine.");
	_jump("Menu");

_border(100); // add content before this

_label("EVENT_SIMONE_SAPIENZA");
	_show("simone",spr_char_Simone_Neutral,1.8,YNUM_SIMONE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_choice(spr_choice_s,"Yes","EVENT_SIMONE_SAPIENZA_CHOICE_1_1",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Simone is inviting you to handcraft class. Accept invitation?[/c]",false);
	_block();

_label("EVENT_SIMONE_SAPIENZA_CHOICE_1_1");
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("simone",spr_char_Simone_Thinking,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] Chris, have you considered crafting as profession?");
	_dialog("[c=#ff3333]Simone:[/c] Handcraft is a noble profession. line that requires attention to detail, coordination, dexterity, precision and imagination.");

	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Handcraft?  What’s creating have to do with anything?  Does Simone expect is to sell souvenirs or something?)[/c]");

	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff](Oh well, at least Chris looks excited about it.  Profit is profit I guess.)[/c]");
	
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] If you can find me an artisan job, I’d love to learn.");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](Simone shoots me this sly look, like she knows something I don’t.)[/c]");
	
	_change("simone",spr_char_Simone_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Simone:[/c] Crafting has countless uses. Penny’s too materialistic, she thinks it’s a waste of time.");
	_dialog("[c=#ff3333]Simone:[/c] Think of it as the basics.  Without them a plastic surgeon would be a glorified butcher.");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_simone_hand_job_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](Oh!  I get it now. Hand Crafting, huh?  Just a little demonstration under the table.)[/c]");

	_choice(spr_choice,"\"An ambidextrous student should get double marks on this close, huh?\"","EVENT_SIMONE_SAPIENZA_CHOICE_2_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"\"Just one hand?  What is this, the beginner course?\"","EVENT_SIMONE_SAPIENZA_CHOICE_2_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_SIMONE_SAPIENZA_CHOICE_2_1");
	_var_add_ext(VAR_AFF_SIMONE,1,1,4);
	_var_add(VAR_INTELLIGENCE,3);
	_dialog("[c=#ff3333]Simone:[/c] Oh my, there’s nothing better than an eager student.  Ambidextrous artisans get the hard work in my classes.");
	
	_jump("EVENT_SIMONE_SAPIENZA_after_CHOICE_2");
	

_label("EVENT_SIMONE_SAPIENZA_CHOICE_2_2");	
	_var_add_ext(VAR_AFF_SIMONE,-1,1,4);
	_var_add(VAR_BARGAIN,3);
	_dialog("[c=#ff3333]Simone:[/c] I can use both hands just done.  Were you listening?  It’s all about the fundamentals.");
	
	_jump("EVENT_SIMONE_SAPIENZA_after_CHOICE_2");
	
_label("EVENT_SIMONE_SAPIENZA_after_CHOICE_2");	
	_change("CG",spr_hentai_cg_simone_hand_job_2,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](Then I forgot all about the lecture and just focused on the sensation.)[/c]");
	_dialog("[c=#6678ff](Oh my… Her hands are so delicate… Her movements… Such expertise!)[/c]");
	_dialog("[c=#6678ff](This is the best hand job I’ve ever had!)[/c]");
	_dialog("[c=#6678ff](At this rate, I won’t last... I’m gonna come!)[/c]");
	
	_change("CG",spr_hentai_cg_simone_hand_job_3,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff3333]Simone:[/c] See? This is what I call handcraft work.");
	
	if var_get(VAR_EVENT_SIMONE_SAPIENZA)
		_jump("EVENT_SIMONE_SAPIENZA_OVER");
	else
		_jump("EVENT_SIMONE_SAPIENZA_CONTINUE");
		
_label("EVENT_SIMONE_SAPIENZA_OVER");
	_main_gui(true);
	_show("simone",spr_char_Simone_Neutral,1.8,YNUM_SIMONE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_execute(time_hour_passed,2);
	if current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening"
		_jump("Menu");
	else
		_jump("Closed");

		
_label("EVENT_SIMONE_SAPIENZA_CONTINUE");
	_var_set(VAR_EVENT_SIMONE_SAPIENZA,true);
	_dialog("[c=#6678ff]James:[/c] Holy fuck!  A dick?! I got a hand job from a...");	
	_dialog("[c=#ff00ff]Chris:[/c] Wow! I didn’t notice either.");
	_dialog("[c=#ff3333]Simone:[/c] Every employee of STP is a transsexual.");
	_dialog("[c=#ff00ff]Chris:[/c] Wow!");
	_dialog("[c=#6678ff]James:[/c] Really??");
	
	_change("CG",spr_hentai_cg_all_characters_bg,FADE_SLOW);
	_pause(0.3,0,true);
	_show("CG_James",spr_hentai_cg_all_characters_James,5,5,1,DEPTH_BG_ELE-10,false,FADE_SLOW);
	_pause(0.5,0,true);
	_show("CG_Simone",spr_char_Simone_B,2.5,YNUM_SIMONE-0.05,1,DEPTH_BG_ELE-1,false,FADE_SLOW);
	_pause(0.5,0,true);
	_show("CG_Ronda",spr_char_Ronda_B,0.8,YNUM_RONDA-0.04,1,DEPTH_BG_ELE-2,false,FADE_SLOW);
	_pause(0.5,0,true);
	_show("CG_Sophia",spr_char_Sophia_B,9.12,YNUM_RONDA-0.04,1,DEPTH_BG_ELE-4,false,FADE_SLOW);
	_dialog("[c=#6678ff]James:[/c] Well, I suspected Ronda. But Sophia?  No way.");
	_dialog("[c=#ff3333]Simone:[/c] Yup");
	
	_show("CG_Security",spr_char_Security_B,7.4,YNUM_RONDA-0.04,1,DEPTH_BG_ELE-3,false,FADE_SLOW);
	_dialog("[c=#6678ff]James:[/c] The security guard?");
	_dialog("[c=#ff3333]Simone:[/c] Yup");
	
	_show("CG_Penny",spr_char_Penny_B,2.1,YNUM_RONDA-0.05,1,DEPTH_BG_ELE-5,false,FADE_SLOW);
	_dialog("[c=#6678ff]James:[/c] Penny too?");
	_dialog("[c=#ff3333]Simone:[/c] Yup");
	_dialog("[c=#6678ff](No wonder she does such great handjob... She had her own dick to practice with. )[/c]");
	_dialog("[c=#6678ff](By that logic, transsexual women could be really amazing sex partners…)[/c]");
	_dialog("[c=#6678ff](What am I thinking?)[/c]");
	_dialog("[c=#ff3333]Simone:[/c] Looks like James needs some time to recover from the shock.");
	_dialog("[c=#ff00ff]Chris:[/c] I really like this place!");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_pause(0.5,0,true);
	_show("simone",spr_char_Simone_Neutral,1.8,YNUM_SIMONE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_execute(time_hour_passed,2);
	if current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening"
		_jump("Menu");
	else
		_jump("Closed");
	

_border(100); // add content before this

