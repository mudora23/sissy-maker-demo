{
    "id": "f48ae8a1-8cf5-4aa0-9d94-4b31718df7c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_questionnaire",
    "eventList": [
        {
            "id": "90a7f81d-8d2a-482f-bf6d-c7d6432ae7a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f48ae8a1-8cf5-4aa0-9d94-4b31718df7c8"
        },
        {
            "id": "08c86823-b17f-410c-891f-950fde428c42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f48ae8a1-8cf5-4aa0-9d94-4b31718df7c8"
        },
        {
            "id": "34f9658e-44b5-406c-9a24-3996cf7381e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "f48ae8a1-8cf5-4aa0-9d94-4b31718df7c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c2fe0c08-ea2c-429d-b4c1-50e18a73ad14",
    "visible": true
}