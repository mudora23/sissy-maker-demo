///questions
switch(question)
{
    case 1:
        text = "What’s your favorite color?";
        button_1 = "Light Sky Blue";
        button_2 = "Deep Pink";
        button_3 = "Violet";
        break;
    case 2:
        text = "What’s your favorite sexual position?";
        button_1 = "Cow girl";
        button_2 = "Doggy Style";
        button_3 = "";
        break;
    case 3:
        text = "If you were stripping for somebody, what part you take off first?";
        button_1 = "My top first";
        button_2 = "My bottom first";
        button_3 = "I'd get naked as soon as possible";
        break;
    case 4:
        text = "What’s the hardest thing to tell to your parents?";
        button_1 = "I’m not virgin anymore";
        button_2 = "I’m gay";
        button_3 = "I’m not going to the college";
        break;
    case 5:
        text = "An unattractive person of the opposite sex stole you a kiss, your reaction is:";
        button_1 = "Punch him/her on the face";
        button_2 = "Say “Are you crazy?”";
        button_3 = "Say “Alright, you owe me one...”";
        break;
    case 6:
        text = "Where is the best place to seat in a classroom?";
        button_1 = "First row";
        button_2 = "Middle row";
        button_3 = "Last row";
        break;
    case 7:
        text = "Favorite Music Style";
        button_1 = "Heavy Metal";
        button_2 = "Jazz";
        button_3 = "Dance Music";
        break;
    case 8:
        text = "The most important in a good relationship is:";
        button_1 = "Respect";
        button_2 = "Love";
        button_3 = "Good Sex";
        break;
    case 9:
        text = "The perfect cock shape is:";
        button_1 = "Long and thin";
        button_2 = "Short and thick";
        button_3 = "Neither";
        break;
    case 10:
        text = "When you look at the mirror what do you see?";
        button_1 = "Someone strange";
        button_2 = "A boy";
        button_3 = "A girl";
        break;
    case 11:
        text = "You're in a club, which restroom you go.";
        button_1 = "Womens room";
        button_2 = "Mens room";
        button_3 = "";
        break;
    case 12:
        text = "Thank you for participating.";
        button_1 = "";
        button_2 = "";
        button_3 = "";
        break;

}



///draw
draw_sprite(spr_questionnaire_bg,-1,0,0);

// sliding
if question > 11 question_leave_delay+=delta_time_sec();
    
if question > 11 && question_leave_delay > 1
{
    xcurrent = smooth_approach(xcurrent,x3,2.2*delta_time_sec());
    ycurrent = smooth_approach(ycurrent,y3,2.2*delta_time_sec());
    
    question_alpha = approach(question_alpha,0,1.8*delta_time_sec());
    
    if question_alpha == 0
        room_goto_ext(room_scene_intro,true);
}
else
{
    xcurrent = smooth_approach(xcurrent,x2,2.2*delta_time_sec());
    ycurrent = smooth_approach(ycurrent,y2,2.2*delta_time_sec());
    
    question_alpha = approach(question_alpha,1,1.8*delta_time_sec());
}


// drawing

draw_set_font(font_general_30);
draw_sprite_ext(sprite_index,-1,xcurrent,ycurrent,1,1,0,c_white,question_alpha);
xx = xcurrent+250; yy = ycurrent+30;
draw_text_ext_transformed_color(xx,yy, text, 45, 450, 1, 1, 0, c_white, c_white, c_white, c_white, question_alpha);
yy = yy + 35 + string_height_ext(text, 45, 450);

draw_set_font(font_general_20);
if button_1 != ""
{
    if mouse_x >= xx-20 && mouse_y >= yy-10 && mouse_x <= xx + 450 + 40 && mouse_y <= yy + string_height_ext(button_1, 45, 450) + 20
    {
        draw_sprite_ext(spr_button_questionaire,1,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_1, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_1, 45, 450, 1, 1, 0, c_yellow, c_yellow, c_yellow, c_yellow, question_alpha);
        if mouse_check_button_pressed(mb_left)
        {
            scene_play_sfx(sfx_button_00);
            switch(question)
            {
                case 1:
                    var_add_ext(VAR_FEMALE_RATIO,-0.05,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 2:
                    var_add(VAR_STRENGTH,5);
                    var_add(VAR_CUP_SIZE,1);
                    break;
                case 3:
                    var_add(VAR_SEX_APPEAL,5);
                    break;
                case 4:
                    var_add_ext(VAR_FEMALE_RATIO,0.1,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 5:
                    var_add_ext(VAR_FEMALE_RATIO,0.05,0+female_ratio_adjust(),1+female_ratio_adjust());
                    var_add(VAR_STRENGTH,5);
                    break;
                case 6:
                    var_add(VAR_INTELLIGENCE,5);
                    break;
                case 7:
                    var_add(VAR_STAMINA,5);
                    break;
                case 8:
                    var_add(VAR_BARGAIN,5);
                    break;
                case 9:
                    var_add(VAR_COCK,1);
                    break;
                case 10:
                    var_add(VAR_SEX_APPEAL,5);
                    break;
                case 11:
                    var_add_ext(VAR_FEMALE_RATIO,0.1,0+female_ratio_adjust(),1+female_ratio_adjust());
                    var_set(VAR_BODY_TYPE,"Pear Shape");
                    var_add(VAR_INTELLIGENCE,5);
                    break;
                default:
                    break;
            }
            question++;
        }
    }
    else
    {
        draw_sprite_ext(spr_button_questionaire,0,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_1, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_1, 45, 450, 1, 1, 0, c_white, c_white, c_white, c_white, question_alpha);
        // make_color_rgb(239, 86, 99)
    }
}
yy = yy + 35 + string_height_ext(button_1, 45, 450);
if button_2 != ""
{
    if mouse_x >= xx-20 && mouse_y >= yy-10 && mouse_x <= xx + 450 + 40 && mouse_y <= yy + string_height_ext(button_2, 45, 450) + 20
    {
        draw_sprite_ext(spr_button_questionaire,1,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_2, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_2, 45, 450, 1, 1, 0, c_yellow, c_yellow, c_yellow, c_yellow, question_alpha);
        if mouse_check_button_pressed(mb_left)
        {
            scene_play_sfx(sfx_button_00);
            switch(question)
            {
                case 1:
                    var_add_ext(VAR_FEMALE_RATIO,0.05,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 2:
                    var_add(VAR_STAMINA,5);
                    var_set(VAR_BODY_TYPE,"Pear Shape");
                    break;
                case 3:
                    var_add(VAR_DEXTERITY,5);
                    break;
                case 4:
                    var_add_ext(VAR_FEMALE_RATIO,-0.1,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 5:
                    var_add_ext(VAR_INTELLIGENCE,-5,0,99999);
                    break;
                case 6:
                    var_add(VAR_BARGAIN,5);
                    break;
                case 7:
                    var_add(VAR_CHARISMA,5);
                    break;
                case 8:
                    var_add_ext(VAR_FEMALE_RATIO,0.1,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 9:
                    var_set(VAR_ANUS,2);
                    break;
                case 10:
                    var_add_ext(VAR_SEX_APPEAL,-5,0,99999);
                    break;
                case 11:
                    var_add_ext(VAR_FEMALE_RATIO,-0.1,0+female_ratio_adjust(),1+female_ratio_adjust());
                    var_set(VAR_BODY_TYPE,"Banana Shape");
                    var_add(VAR_SEX_APPEAL,5);
                    break;
                default:
                    break;
            }
            question++;
        }
    }
    else
    {
        draw_sprite_ext(spr_button_questionaire,0,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_2, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_2, 45, 450, 1, 1, 0, c_white, c_white, c_white, c_white, question_alpha);
    }
}
yy = yy + 35 + string_height_ext(button_2, 45, 450);
if button_3 != ""
{
    if mouse_x >= xx-20 && mouse_y >= yy-10 && mouse_x <= xx + 450 + 40 && mouse_y <= yy + string_height_ext(button_3, 45, 450) + 20
    {
        draw_sprite_ext(spr_button_questionaire,1,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_3, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_3, 45, 450, 1, 1, 0, c_yellow, c_yellow, c_yellow, c_yellow, question_alpha);
        if mouse_check_button_pressed(mb_left)
        {
            scene_play_sfx(sfx_button_00);
            switch(question)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    var_add(VAR_CHARISMA,5);
                    break;
                case 4:
                    var_add(VAR_INTELLIGENCE,5);
                    break;
                case 5:
                    var_add_ext(VAR_FEMALE_RATIO,-0.05,0+female_ratio_adjust(),1+female_ratio_adjust());
                    var_add(VAR_BARGAIN,5);
                    break;
                case 6:
                    var_add(VAR_CHARISMA,5);
                    break;
                case 7:
                    var_add(VAR_DEXTERITY,5);
                    break;
                case 8:
                    var_add(VAR_SEX_APPEAL,5);
                    break;
                case 9:
                    var_add_ext(VAR_FEMALE_RATIO,-0.15,0+female_ratio_adjust(),1+female_ratio_adjust());
                    break;
                case 10:
                    var_add(VAR_CHARISMA,5);
                    break;
                case 11:
                    break;
                default:
                    break;
            }
            question++;
        }
    }
    else
    { 
        draw_sprite_ext(spr_button_questionaire,0,xx-20,yy-10,(450+40)/sprite_get_width(spr_button_questionaire),(string_height_ext(button_3, 45, 450)+20)/sprite_get_height(spr_button_questionaire),0,c_white,question_alpha);
        draw_text_ext_transformed_color(xx,yy, button_3, 45, 450, 1, 1, 0, c_white,c_white,c_white,c_white, question_alpha);
    }
}
yy = yy + 20 + string_height_ext(button_3, 45, 450);