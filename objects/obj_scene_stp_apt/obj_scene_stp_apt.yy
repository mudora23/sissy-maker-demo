{
    "id": "89d87556-b36b-439d-a8c9-a87a3f942388",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_apt",
    "eventList": [
        {
            "id": "d691e265-f94b-449c-94e0-7e28664d31be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89d87556-b36b-439d-a8c9-a87a3f942388"
        },
        {
            "id": "2ab6cf6b-3ff5-4584-bc4b-3640670755fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "89d87556-b36b-439d-a8c9-a87a3f942388"
        },
        {
            "id": "60b6d977-83d2-412c-abd7-b9a9c09efba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89d87556-b36b-439d-a8c9-a87a3f942388"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}