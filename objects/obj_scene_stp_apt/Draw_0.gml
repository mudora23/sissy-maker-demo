event_inherited();

_start();
	_placename(PLACENAME_apt);
	_main_gui(true);
	_show("BG",spr_bg_aptLoungeClean,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_var_set(VAR_EVENT_APT_VISIT,true);

	if !var_get(VAR_EVENT_BANKRUPT) &&
		var_get(VAR_CASH)<30
		_jump("EVENT_BANKRUPT");
	//else if var_get(VAR_AFF_CHRIS_PENALTY) >= 5 && var_get(VAR_AFF_CHRIS) <= 1
	//	_jump("EVENT_RUNAWAY");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Late Night");

_border(100); // add content before this

_label("Stay");
	_stay(true,true,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
	
_label("Rest");
	_rest(true,true,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	_jump("Menu");

_border(100); // add content before this

_label("Morning");
	if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.75
		_jump("Morning ENERGY_4");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.5
		_jump("Morning ENERGY_3");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.25
		_jump("Morning ENERGY_2");
	else
		_jump("Morning ENERGY_1");

_border(100); // add content before this

_label("Afternoon");
	if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.75
		_jump("Afternoon ENERGY_4");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.5
		_jump("Afternoon ENERGY_3");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.25
		_jump("Afternoon ENERGY_2");
	else
		_jump("Afternoon ENERGY_1");	

_border(100); // add content before this

_label("Evening");
	if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.75
		_jump("Evening ENERGY_4");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.5
		_jump("Evening ENERGY_3");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.25
		_jump("Evening ENERGY_2");
	else
		_jump("Evening ENERGY_1");

_border(100); // add content before this

_label("Late Night");
	if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.75
		_jump("Late Night ENERGY_4");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.5
		_jump("Late Night ENERGY_3");
	else if var_get(VAR_ENERGY) >= var_get(VAR_ENERGY_MAX) * 0.25
		_jump("Late Night ENERGY_2");
	else
		_jump("Late Night ENERGY_1");

_border(100); // add content before this

_label("Morning ENERGY_4");	
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Good Morning ♫!");
	_dialog("[c=#6678ff]James:[/c] Morning!");
	_jump("Menu");
	
_border(100); // add content before this
	
_label("Morning ENERGY_3");	
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] *stretches*");
	_dialog("[c=#6678ff]James:[/c] What’s up?");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Just a bit tired, but I’m good<3");
	_jump("Menu");
		
_border(100); // add content before this

_label("Morning ENERGY_2");	
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_dialog("[c=#6678ff]James:[/c] You have black rings under your eyes. Maybe you should rest more.");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Don’t underestimate the power of make-up!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Morning ENERGY_1");	
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] *Groan*");
	_dialog("[c=#6678ff]James:[/c] Yeah, you need more rest.");
	_dialog("[c=#ff00ff]Chris:[/c] ok...");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon ENERGY_4");
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Good afternoon ♫!");
	_dialog("[c=#6678ff]James:[/c] Hey, Chris.  Looking chipper.");
	_dialog("[c=#ff00ff]Chris:[/c] I’m just getting started!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon ENERGY_3");
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);	
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Whew, long day, huh?");
	_dialog("[c=#6678ff]James:[/c] Still a bit left.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Ok! Let’s finish strong!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon ENERGY_2");	
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Dragging your feet already?");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m still good to go!");
	_dialog("[c=#6678ff]James:[/c] ...");
	_jump("Menu");
	
_border(100); // add content before this

_label("Afternoon ENERGY_1");	
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] *Groan*");
	_dialog("[c=#6678ff]James:[/c] You need rest.");
	_dialog("[c=#ff00ff]Chris:[/c] You might be right.. *yawn*");
	_jump("Menu");
	
_border(100); // add content before this

_label("Evening ENERGY_4");	
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Evenin’ James ♫!");
	_dialog("[c=#6678ff]James:[/c] You’re looking energetic, as always.");
	_jump("Menu");
		
_border(100); // add content before this

_label("Evening ENERGY_3");	
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hmm. Getting dark...");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Sleepy?");
	_dialog("[c=#ff00ff]Chris:[/c] No way, just pointing out... obvious stuff.");
	_jump("Menu");
		
_border(100); // add content before this

_label("Evening ENERGY_2");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_dialog("[c=#6678ff]James:[/c] You’re having trouble keeping your eyes open.  That’s not a good sign.");
	_dialog("[c=#ff00ff]Chris:[/c] It’s just dark and... *yawn*... maybe I’m a little sleepy.");
	_jump("Menu");
			
_border(100); // add content before this

_label("Evening ENERGY_1");	
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Uuuugh...");
	_dialog("[c=#6678ff]James:[/c] You look like a zombie. The cure is sleep!");
	_dialog("[c=#ff00ff]Chris:[/c] Fine... I guess I should, huh?");
	_jump("Menu");
	
_border(100); // add content before this

_label("Late Night ENERGY_4");	
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Happy,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] It’s late and I feel greeeat. ♫!");
	_dialog("[c=#6678ff]James:[/c] How are you so full of energy right now?!  Are you using a cheat code?");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Huh?");
	_jump("Menu");
		
_border(100); // add content before this

_label("Late Night ENERGY_3");	
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Worry,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Whoa? Is it really that late?");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Yeah. Aren’t you sleepy?");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] No way! I’m a night owl!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Late Night ENERGY_2");	
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] You look tired. No wonder, look at the time!");
	_dialog("[c=#ff00ff]Chris:[/c] But what about the after-party?!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Late Night ENERGY_1");	
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Tired,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Sooo sleepy.");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Well yeah, it’s the middle of the night! You should rest.");
	_dialog("[c=#ff00ff]Chris:[/c] *nods sleepily*");
	_jump("Menu");

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","chris",FADE_NORMAL);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Rest","Rest",noone,fa_right,fa_center);
	/*if inventory_item_exists(ITEM_Lotion)
		_choice(spr_choice_s,"Use "+ITEM_Lotion,"Lotion",noone,fa_right,fa_center);
	else*/
		_void();
	/*if inventory_item_exists(ITEM_So_Zore_Lotion)
		_choice(spr_choice_s,"Use "+ITEM_So_Zore_Lotion,"So_Zore_Lotion",noone,fa_right,fa_center);
	else*/
		_void();
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

/*_label("Lotion");
	_choice(spr_choice_s,"Yes","Lotion Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Use '"+ITEM_Lotion+"'?[/c]",false);
	_block();

_label("Lotion Yes");
	_execute(inventory_use_item_no_delete,ITEM_Lotion);
	_execute(inventory_delete_item,ITEM_Lotion,1);
	
	if inventory_item_day_x_in_a_row(ITEM_Lotion) == 1
		_execute(draw_floating_text,"'Blossom! The lotion' has been used for 1 day");
	else if inventory_item_day_x_in_a_row(ITEM_Lotion) > 1
		_execute(draw_floating_text,"'Blossom! The lotion' has been used for "+string(inventory_item_day_x_in_a_row(ITEM_Lotion))+" days in a row");
	else
		_void();
	_if(inventory_item_day_x_in_a_row(ITEM_Lotion) >= 5,_jump,"Lotion 5days");
	_jump("Menu");
	
_label("Lotion 5days");	
	_var_add_ext(VAR_CUP_SIZE,1,0,3);
	_execute(inventory_item_day_in_a_row_clear,ITEM_Lotion);
	_jump("Menu");*/

_border(2); // add content before this

/*_label("So_Zore_Lotion");
	_choice(spr_choice_s,"Yes","So_Zore_Lotion Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Use '"+ITEM_So_Zore_Lotion+"'?[/c]",false);
	_block();

_label("So_Zore_Lotion Yes");
	_execute(inventory_use_item_no_delete,ITEM_So_Zore_Lotion);
	_execute(inventory_delete_item,ITEM_So_Zore_Lotion,1);
	
	if inventory_item_day_x_in_a_row(ITEM_So_Zore_Lotion) == 1
		_execute(draw_floating_text,ITEM_So_Zore_Lotion+" has been used for 1 day");
	else if inventory_item_day_x_in_a_row(ITEM_So_Zore_Lotion) > 1
		_execute(draw_floating_text,ITEM_So_Zore_Lotion+" has been used for "+string(inventory_item_day_x_in_a_row(ITEM_So_Zore_Lotion))+" days in a row");
	else
		_void();
	_if(inventory_item_day_x_in_a_row(ITEM_So_Zore_Lotion) >= 3,_jump,"So_Zore_Lotion 3days");
	_jump("Menu");
	
_label("So_Zore_Lotion 3days");	
	_var_set(VAR_ANUS,2);
	_execute(inventory_item_day_in_a_row_clear,ITEM_So_Zore_Lotion);
	_jump("Menu");*/

_border(98); // add content before this

_label("Talk");
	if current_time_get_name() == "Late Night" && 
	   !var_get(VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01) &&
	   var_get(VAR_EVENT_SIMONE_SAPIENZA) &&
	   var_get(VAR_EVENT_RONDA_RUTHLESS) &&
	   var_get(VAR_AFF_CHRIS) >= 3 &&
	   (success_roll_attribute(VAR_BARGAIN) + success_roll(VAR_AFF_CHRIS))
		_jump("EVENT_JAMES_LAKIN_AND_CHRIS_01");

	else if current_time_get_name() == "Morning"
	{
		if var_get(VAR_AFF_CHRIS) >= 4
			_jump("Morning AFF_4");
		else if var_get(VAR_AFF_CHRIS) >= 3
			_jump("Morning AFF_3");
		else if var_get(VAR_AFF_CHRIS) >= 2
			_jump("Morning AFF_2");
		else
			_jump("Morning AFF_1");
	}
	else if current_time_get_name() == "Afternoon"
	{
		if var_get(VAR_AFF_CHRIS) >= 4
			_jump("Afternoon AFF_4");
		else if var_get(VAR_AFF_CHRIS) >= 3
			_jump("Afternoon AFF_3");
		else if var_get(VAR_AFF_CHRIS) >= 2
			_jump("Afternoon AFF_2");
		else
			_jump("Afternoon AFF_1");
	}
	else if current_time_get_name() == "Evening"
	{
		if var_get(VAR_AFF_CHRIS) >= 4
			_jump("Evening AFF_4");
		else if var_get(VAR_AFF_CHRIS) >= 3
			_jump("Evening AFF_3");
		else if var_get(VAR_AFF_CHRIS) >= 2
			_jump("Evening AFF_2");
		else
			_jump("Evening AFF_1");
	}
	else
	{
		if var_get(VAR_AFF_CHRIS) >= 4
			_jump("Late Night AFF_4");
		else if var_get(VAR_AFF_CHRIS) >= 3
			_jump("Late Night AFF_3");
		else if var_get(VAR_AFF_CHRIS) >= 2
			_jump("Late Night AFF_2");
		else
			_jump("Late Night AFF_1");
	}

_border(100); // add content before this

_label("Morning AFF_4");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hi James! ♫");
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hi Chris.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Do you want me to suck you cock?");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Huh?");
	_dialog("[c=#ff00ff]Chris:[/c] I can suck it with my butt, want to see?");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Oh my…");
	_jump("Menu");
		
_border(100); // add content before this

_label("Morning AFF_3");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I noticed you’ve been staring at my butt often. ♫");
	_show("james",spr_char_James_Blush,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Really?");
	_dialog("[c=#ff00ff]Chris:[/c] Do think I should wear something sexier for you?");
	_dialog("[c=#6678ff]James:[/c] Hmmm… no.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah it’s better to wear nothing at all.");
	_dialog("[c=#6678ff]James:[/c] W-what? Wait!!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Morning AFF_2");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] How are you doing?");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m doing good.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You know, you’re officially my manager, but that doesn’t make you my pimp, ok?");
	_jump("Menu");
		
_border(100); // add content before this

_label("Morning AFF_1");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What’s up?");
	_dialog("[c=#ff00ff]Chris:[/c] Why are you so boooring?");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What’s wrong with you?!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon AFF_4");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hi ♫ James!");
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hi Chris.");
	_dialog("[c=#ff00ff]Chris:[/c] Want an afternoon pick me up?");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Huh?");
	_dialog("[c=#ff00ff]Chris:[/c] Nobody’s looking, so it’s all yours!");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Oh my…");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon AFF_3");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] How’s my manager doing this afternoon? ♫");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] This isn’t about me.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, but it could be...");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What do you mean by that?");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] My little secret. Hehe.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] ?");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon AFF_2");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Feeling O.k?");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, not bad.");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I could have a worse manager, could have a better one too...");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hey!");
	_jump("Menu");
		
_border(100); // add content before this

_label("Afternoon AFF_1");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What now?");
	_dialog("[c=#ff00ff]Chris:[/c] Nothing.");
	_dialog("[c=#6678ff]James:[/c] ...");
	_jump("Menu");
	
_border(100); // add content before this

_label("Evening AFF_4");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hi James! ♫");
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Sup Chris?");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] How about an evening quickie?");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Uhm...");
	_dialog("[c=#ff00ff]Chris:[/c] Or would you prefer something... longer?");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Oh my…");
	_jump("Menu");
	
_border(100); // add content before this

_label("Evening AFF_3");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You’ve been a big help with everything.");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] You think so?");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Yep, I think you need a reward.  <3");
	_dialog("[c=#6678ff]James:[/c] Really?");
	_dialog("[c=#ff00ff]Chris:[/c] Maybe I’ll forget to wear underwear for you.");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Huh?");
	_jump("Menu");
	
_border(100); // add content before this

_label("Evening AFF_2");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Penny for your thoughts.");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m fine.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] If you were a bit nicer to me, I bet I’d work harder.  Maybe.");
	_jump("Menu");
	
_border(100); // add content before this

_label("Evening AFF_1");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] *stare*");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Do I have something on my face?");
	_dialog("[c=#ff00ff]Chris:[/c] Yes, your face.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] What’s wrong that?!");
	_jump("Menu");
	
_border(100); // add content before this

_label("Late Night AFF_4");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hi James! ♫");
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Hi Chris.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Since we’re up, you want some special time?");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Special time?");
	_dialog("[c=#ff00ff]Chris:[/c] Yeah, I’ll even let you pick the hole. <3");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Oh my…");
	_jump("Menu");
	
_border(100); // add content before this

_label("Late Night AFF_3");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You think I’m sexy, huh?. ♫");
	_show("james",spr_char_James_Blush,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Uh... what?");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I was singing.  But you do don’t you?");
	_dialog("[c=#6678ff]James:[/c] ...");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Well, maybe if I’m feeling nice I’ll give you a taste!  <3");
	_dialog("[c=#6678ff]James:[/c] hmmm....");
	_jump("Menu");
	
_border(100); // add content before this

_label("Late Night AFF_2");
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] *yawn* don’t you ever sleep?");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Why? You expecting company?");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] That’s not...");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Well too bad. I don’t like you like that. <3");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("Then what’s with the ‘<3’");
	_jump("Menu");
	
_border(100); // add content before this

_label("Late Night AFF_1");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m bored!");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Then sleep it’s late!");
	_dialog("[c=#ff00ff]Chris:[/c] No, I mean you’re boring me. Be less boring.");
	_dialog("[c=#6678ff]James:[/c] Whatever.");
	_jump("Menu");

_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_main_map,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Move to the next location?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_BANKRUPT");
	_var_set(VAR_EVENT_BANKRUPT,true);
	_choice(spr_choice,"Continue","EVENT_BANKRUPT_CHOICE_1",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James and Chris find themselves out of cash...[/c]",false);
	_block();
	
_label("EVENT_BANKRUPT_CHOICE_1");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Worry,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Damn we’re totally broke!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Someone here should be working…");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Of course. James, these are hard times, you should flip some burgers.");
	_dialog("[c=#6678ff]James:[/c] I’m the manager here… You must to take [c=#FFFF00]Babysitting Classes[/c].");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Luckily I sold an old video game cartridge today! Here is [c=#FFFF00]180$[/c].");
	_var_add(VAR_CASH,180);
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Thank you so much! I won’t disappoint you!");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Alright, alright.");
	_jump("Menu");

_border(100); // add content before this

_label("EVENT_JAMES_LAKIN_AND_CHRIS_01");
	_var_set(VAR_EVENT_JAMES_LAKIN_AND_CHRIS_01,true);
	_choice(spr_choice,"Continue","EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_1",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James questions about gender and sexuality.[/c]",false);
	_block();
	
_label("EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_1");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Chris… After the all this stuff happening at STP, I’ve got done questions about gender and sexuality. They’re two things apart, right?");
	_dialog("[c=#ff00ff]Chris:[/c] Sure.");
	_dialog("[c=#6678ff]James:[/c] Do you consider yourself homosexual?");
	_dialog("[c=#ff00ff]Chris:[/c] I’m bisexual, it’s hard to explain.");
	_dialog("[c=#6678ff]James:[/c] Give it a try.");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] For example; If I date a girl or a TS girl, I consider it a homosexual relationship.");
	_dialog("[c=#6678ff]James:[/c] Then if you date a guy, it’s straight?")
	_choice(spr_choice,"Kinda makes sense...","EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_2_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"That’s a bit of a stretch.","EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_2_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_2_1");
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_jump("EVENT_JAMES_LAKIN_AND_CHRIS_01_after_CHOICE_2");
	
_label("EVENT_JAMES_LAKIN_AND_CHRIS_01_CHOICE_2_2");
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_jump("EVENT_JAMES_LAKIN_AND_CHRIS_01_after_CHOICE_2");
	
_label("EVENT_JAMES_LAKIN_AND_CHRIS_01_after_CHOICE_2");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Hehe… Are you worried?");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I’m not!");
	_dialog("[c=#ff00ff]Chris:[/c] C’mon don’t be shy!");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I’m just tired.  I gonna crash, g’night.");
	_dialog("[c=#ff00ff]Chris:[/c] Seeee ya <3!");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Ha! Interesting…");
	_jump("Menu");


_border(2); // add content before this
/*
_label("EVENT_RUNAWAY");
	_var_set(VAR_EVENT_RUNAWAY,true);
	_choice(spr_choice,"Wait for Chris to come back.","EVENT_RUNAWAY_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James heads to his apartment lounge but Chris is nowhere to be seen.[/c]",false);
	_block();

_label("EVENT_RUNAWAY_START");
	_execute(draw_floating_text,"Four days later…");
	_var_add(VAR_DAY,var_get(VAR_DAY)+4);
	if var_get(VAR_DAYSREMAINING) >= 4
		_var_set(VAR_DAYSREMAINING,var_get(VAR_DAYSREMAINING)-4);
	else
		_var_set(VAR_DAYSREMAINING,0);
	if var_get(VAR_DAYSREMAINING) <= 0
		_room_goto_ext(room_scene_deadline,true);
	else
		_void();
	_show("chris",spr_char_Chris_Worry,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Chris, I’ve been looking everywhere for you.  Where have you been?");
	_dialog("[c=#ff00ff]Chris:[/c] Oh? NOW you’re worried about me…");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James… I don’t like hanging around with people who don’t give a damn about me.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] ...");
	_change("chris",spr_char_Chris_Tired,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I… just needed a few days to chill and think.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …and I decided to come back.  I owe you money, and I still plan to pay it back.");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You may be cute, but no one pushes me around like that!");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I’m sorry.  I guess I have been a little bit of a slave driver.  I need to keep your feelings in mind when I make choices.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I’ll try to be more careful.");
	_change("chris",spr_char_Chris_Tired,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m gonna grab something to eat and take a shower.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_hide_all_except("BG","chris",FADE_NORMAL);
	_pause(0.5,0,true);
	_dialog("[c=#ff00ff](James… you idiot!)[/c]");
	_var_set(VAR_AFF_CHRIS_PENALTY,var_get(VAR_AFF_CHRIS_PENALTY)-5);
	_var_add_ext(VAR_STRENGTH,-4,0,var_get(VAR_STRENGTH));
	_var_add_ext(VAR_STAMINA,-4,0,var_get(VAR_STAMINA));
	_var_add_ext(VAR_DEXTERITY,-4,0,var_get(VAR_DEXTERITY));
	_var_add_ext(VAR_BARGAIN,-4,0,var_get(VAR_BARGAIN));
	_var_add_ext(VAR_INTELLIGENCE,-4,0,var_get(VAR_INTELLIGENCE));
	_var_add_ext(VAR_CHARISMA,-4,0,var_get(VAR_CHARISMA));
	_var_add_ext(VAR_SEX_APPEAL,-4,0,var_get(VAR_SEX_APPEAL));
	_jump("Menu");
*/
_border(98); // add content before this




