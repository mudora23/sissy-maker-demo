if hiding
{
    shadow_image_alpha = approach(shadow_image_alpha, 0, 1.2*delta_time_sec());
    x = approach(x,x_hide,1200*delta_time_sec());
    y = approach(y,y_hide,1200*delta_time_sec());
}
else
{
    shadow_image_alpha = approach(shadow_image_alpha, 0.9, 1.2*delta_time_sec());
    x = smooth_approach(x,x_target,slide_speed*delta_time_sec());
    y = smooth_approach(y,y_target,slide_speed*delta_time_sec());

    if abs(x - x_target) <= 50
    {
        clock_image_timer+=delta_time_sec();
        if clock_image_timer >= clock_image_speed
        {
            if clock_image_index < image_number - 1
            {
                clock_image_index++;
                clock_image_timer = 0;
            }
            else
            {
                clock_image_leave_delay-=delta_time_sec();
                if clock_image_leave_delay <= 0
				{
                    hiding = true;
					time_hour_passed(2);
				}
            }
        }
    }
}

if hiding && shadow_image_alpha == 0
{
    if is_string(scene_goto) || scene_goto != noone
    {
        scene_jump(scene_goto);
    }
    instance_destroy();
}
    
// BG
draw_sprite_ext(spr_black,-1,0,0,1,1,0,c_white,shadow_image_alpha);
// SPRITE
draw_sprite_ext(activities_sprite,-1,x,y,1,1,0,c_white,1);
// CLOCK
draw_sprite_ext(sprite_index,clock_image_index,x,y,1,1,0,c_white,1);

if !audio_is_playing(sfx_clock)
    scene_play_sfx(sfx_clock);