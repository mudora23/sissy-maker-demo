depth = DEPTH_GUI_ACTIVITIES;

activities_sprite = noone;
scene_goto = noone;

// position
x = 900;
y = 0;
x_hide = 900;
y_hide = 0;
x_target = 0;
y_target = 0;

slide_speed = 3;

hiding = false;
shadow_image_alpha = 0;

clock_image_index = 0;
clock_image_speed = 0.6;
clock_image_timer = 0;
clock_image_leave_delay = 0.3;