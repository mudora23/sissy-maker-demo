{
    "id": "889214d9-4330-4c69-952f-95606821997b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_activities",
    "eventList": [
        {
            "id": "cc600d2b-fde7-4fd3-87b2-d45ab98a8804",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "889214d9-4330-4c69-952f-95606821997b"
        },
        {
            "id": "4c685823-1270-4c05-a2f2-2247b390fa5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "889214d9-4330-4c69-952f-95606821997b"
        },
        {
            "id": "4f034fa9-fe18-4278-8ab8-a0322b14e74c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "889214d9-4330-4c69-952f-95606821997b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "26dab7d1-7dd8-4fbe-80f6-2dcc8781ff5a",
    "visible": true
}