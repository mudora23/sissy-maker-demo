{
    "id": "f6bc7500-1326-4807-bfd1-eb3de3d7ce98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_lotus_dew",
    "eventList": [
        {
            "id": "37d81af7-31fe-46fc-942b-3a0d3189ee1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6bc7500-1326-4807-bfd1-eb3de3d7ce98"
        },
        {
            "id": "626d1f71-d30c-44a5-b8d7-bf5b4205ea97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "f6bc7500-1326-4807-bfd1-eb3de3d7ce98"
        },
        {
            "id": "6560bd46-a0db-4476-97f9-84395fbf0b2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f6bc7500-1326-4807-bfd1-eb3de3d7ce98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}