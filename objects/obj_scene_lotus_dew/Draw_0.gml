event_inherited();

_start();
	_placename(PLACENAME_lotus_dew);
	_main_gui(true);
	_show("BG",spr_bg_LotusClub,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_LOTUS_DEW_VISIT) && 
	   current_time_get_name() == "Late Night"
		_jump("EVENT_LOTUS_DEW_VISIT");
	else if current_time_get_name() == "Late Night"
		_jump("Late Night");
	else
		_jump("Closed");
		

_border(100); // add content before this

_label("Stay");
	_stay(false,false,false,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Late Night"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Late Night");
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("dayna",spr_char_Dayna_Sly,1.8,YNUM_DAYNE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff00ff]Chris:[/c] Hey Dayna!");
	_dialog("[c=#39f979]Dayna:[/c] Sup Chris, James.");
	_change("dayna",spr_char_Dayna_Party,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] James, if you succeed as Chris manager, I want you to take care of my DJ carrier.");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] That would be cool!");
	_jump("Menu");

_border(100); // add content before this

_label("Morning");
_label("Afternoon");
_label("Evening");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_choice(spr_choice,"Back to the Main Map",noone,room_scene_main_map,fa_center,fa_center);
	_dialog("[c=#6678ff](It’s closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","dayna",FADE_NORMAL);
	if !scene_sprite_object_exists("dayna")
		_show("dayna",spr_char_Dayna_Sly,1.8,YNUM_DAYNE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	else
		_void();
	_change("dayna",spr_char_Dayna_Sly,FADE_NORMAL);
	_choice(spr_choice_s,"Order a drink","Order a drink Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Pay a drink to...","Pay a drink to... Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Look around","Look around Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Exit","Move",noone,fa_right,fa_center);
	_block();


_border(100); // add content before this

_label("Order a drink Intro");
	_dialog("[c=#6678ff]James:[/c] I’ll take a…");
	_choice(spr_choice_s,"Beer (Cost: $5)","Order a drink Beer",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Cosmopolitan (Cost: $10)","Order a drink Cosmopolitan",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Bourbon (Cost: $8)","Order a drink Bourbon",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Cancel","Menu",noone,fa_right,fa_center);
	_block();

_label("Order a drink Beer");
	if var_get(VAR_CASH) >= 5
		_jump("Order a drink Beer Confirm");
	else
		_jump("Order a drink Fail");
	
_label("Order a drink Cosmopolitan");
	if var_get(VAR_CASH) >= 10
		_jump("Order a drink Cosmopolitan Confirm");
	else
		_jump("Order a drink Fail");
		
_label("Order a drink Bourbon");
	if var_get(VAR_CASH) >= 8
		_jump("Order a drink Bourbon Confirm");
	else
		_jump("Order a drink Fail");

_label("Order a drink Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");

_label("Order a drink Beer Confirm");
	_var_add(VAR_CASH,-5);
	_dialog("[c=#39f979]Dayna:[/c] Chris usually drinks beer too, I know she’s a girl, but her attitude and charm is alluring. If I ever go bi-curious I’ll hit her.");
	_dialog("[c=#6678ff](I cough disarmingly.)[/c]");
	_dialog("[c=#39f979]Dayna:[/c] What?");
	_dialog("[c=#6678ff]James:[/c] It’s nothing…");
	_jump("Menu");

_label("Order a drink Cosmopolitan Confirm");
	_var_add(VAR_CASH,-10);
	_dialog("[c=#39f979]Dayna:[/c] A cosmo? Really? You DO know this drink is girly as hell, right?");
	_dialog("[c=#6678ff]James:[/c] So, what?");
	_dialog("[c=#39f979]Dayna:[/c] Last time I mixed one of these, it was for some geeky girl.  Real freak that one.");
	_dialog("[c=#39f979]Dayna:[/c] Between you and me, she into porn novels. After two cosmos she was reciting Marques de Sade.");
	_dialog("[c=#6678ff]James:[/c] What a scene.");
	_jump("Menu");

_label("Order a drink Bourbon Confirm");
	_var_add(VAR_CASH,-8);
	_dialog("[c=#39f979]Dayna:[/c] Whoa, you’re a regular cowboy, huh?  That reminds me, one day, I sold  bourbon shots to a big lady, apparently she works as a security guard.");
	_dialog("[c=#39f979]Dayna:[/c] Second time I see her she’s all like “I never drink bourbon ever, give me vodka!” as if she was a totally different person.");
	_dialog("[c=#6678ff]James:[/c] That’s weird.");
	_jump("Menu");

_border(100); // add content before this

_label("Pay a drink to... Intro");
	_choice(spr_choice_s,"Chris","Pay a drink to Chris",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Dayna","Pay a drink to Dayna",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Cancel","Menu",noone,fa_right,fa_center);
	_block();
	
_label("Pay a drink to Chris");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](No, it’s better save some money.  Plus, I should keep her sober, she’s working.)[/c]");
	_jump("Menu");
	
_label("Pay a drink to Dayna");
	//if !var_get(VAR_EVENT_STRIP_CLUB_PT1) && (skills_xp_get_level(var_get(VAR_SKILL_JAZZ)) >= 1 || skills_xp_get_level(var_get(VAR_SKILL_SALSA)) >= 1)
	//	_jump("EVENT_STRIP_CLUB_PT1_INTRO");
	//else
		_change("dayna",spr_char_Dayna_Tired,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Nah, I’m on the clock. Gonna take it easy. Look me up when my shift’s over, yeah?");
	_jump("Menu");

_border(100); // add content before this

_label("Look around Intro");
	//if !var_get(VAR_EVENT_DANCER_LOTUS_RESTROOM) && current_time_within(2,4) && ratio_chance(1/3) 
	//	_jump("EVENT_DANCER");
	//else
		_jump("Look around Normal");
		
_border(100); // add content before this
/*
_label("EVENT_DANCER");
	_hide_all_except("BG",FADE_NORMAL);
	_dialog("[c=#FFFF00]The electronic music booms in the dance floor and James spots a lady standing out with her dancing skills.[/c]");
	_choice(spr_choice,"Yes","EVENT_DANCER_CHOICE_1_1",noone,fa_center,fa_center);
	_choice(spr_choice,"No","Menu",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Keep looking at her?[/c]",false);
	_block();
	
_label("EVENT_DANCER_CHOICE_1_1");
	_var_add(VAR_EVENT_DANCER,1);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](I look around for Chris with no luck, but a woman on the dance floor catches my eye.)[/c]");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff](Wow, she’s got moves! …and she’s a looker too.)[/c]");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff](… Shit! She’s looking at me!)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](Now she’s showing everything she got.)[/c]");
	_dialog("[c=#6678ff](She’s so sexy… It’s like she’s trying to impress me… and man, am I impressed.)[/c]");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](I look around for Chris, fighting back a sudden guilty feeling.  Is it from flirting with the mysterious lady?)[/c]");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Why the heck do I feel guilty about that?)[/c]");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](The music stops and when I turn back towards the dance floor, that ‘mysterious lady’ stands right in front of me.)[/c]");
	_show("raye",spr_char_Raye_Neutral,1.8,YNUM_RAYE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("*: Hello, stud! How did you liked my performance?");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] It was stunning.  You make it look easy.  I bet you’re a pro.");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] Yes, my name is Raye I’m a professional dancer and coach. I just got in town and I’m looking for a job.");
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] Dayna told me that you are a producer.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff](I glare towards Dayna, but looks away whistling innocently.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Yeah something like that, but I’m no professional…");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] I bet there is something else you can do like a pro.");
	_change("raye",spr_char_Raye_Flirt,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] Why don’t you show me back there, in the restroom. Come on!");
	_choice(spr_choice,"Yes","EVENT_DANCER_CHOICE_2_1",noone,fa_center,fa_center);
	_choice(spr_choice,"No","EVENT_DANCER_CHOICE_2_2",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Sneak off and follow the lady to the bathroom?[/c]",false);
	_block();
	
_label("EVENT_DANCER_CHOICE_2_1");
	_void();//_var_add(VAR_EVENT_DANCER_LOTUS_RESTROOM,1);
	_var_add_ext(VAR_AFF_RAYE,1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,-2,1,4);
	_var_add_ext(VAR_AFF_RONDA,-1,1,4);
	_choice(spr_choice,"Continue","EVENT_DANCER_LOTUS_RESTROOM",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James follows the dancer to the rest room.[/c]",false);
	_block();
	
_label("EVENT_DANCER_LOTUS_RESTROOM");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](Raye leads me by the hand to the restroom, moving quickly.)[/c]");
	_dialog("[c=#6678ff](It doesn’t keep her luscious ass from shimming to pulse of the music. Thankfully, I get a nice view from behind.)[/c]");
	_change("james",spr_char_James_Happy,FADE_NORMAL);
	_dialog("[c=#6678ff](Her butt is a dancer on its own merit.)[/c]");
	_change("raye",spr_char_Raye_Happy,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] C’mon hurry! They call it a quickie for a reason!");
	_change("BG",spr_black,FADE_SLOW);
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](She leads us to one of the free booths.)[/c]");
	_dialog("[c=#6678ff](The restroom feels too open, exposed. It makes it hard to concentrate. Anyone could just barge in at any moment. But that makes it kind of exciting too…)[/c]");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris, he could come looking for me… shit. Not him, her… HER!)[/c]");
	_dialog("[c=#39f979]Raye:[/c] Hey, you what are you waiting for?");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_raye_restroom_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](Raye lifts her beautiful yellow dress, exposing her firm buttocks. The world outside that restroom booth vanished.)[/c]");
	_dialog("[c=#39f979]Raye:[/c] I want in the butt.");
	_dialog("[c=#6678ff]James:[/c] But…");
	_dialog("[c=#39f979]Raye:[/c] Yeah! Right in the butt! Come on in! I’m more than ready for you…");
	_dialog("[c=#39f979]Raye:[/c] Pussies are for marriage, but you can fuck me in the asshole all you want!");
	_dialog("[c=#6678ff]James:[/c] I mean… I am hard and ready to go… so…");
	_choice(spr_choice,"'Yeah baby, I’m a back-door man!'","EVENT_DANCER_LOTUS_RESTROOM_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"'Well, I’m a bit of a romantic, so I usually do pussies first. But your ass is looking mighty fine...'","EVENT_DANCER_LOTUS_RESTROOM_CHOICE_1_2",noone,fa_center,fa_center,true);
	_dialog("[c=#39f979]Raye:[/c] You like doing it in the ass, don’t you?",false);
	_block();
	
_label("EVENT_DANCER_LOTUS_RESTROOM_CHOICE_1_1");
	_var_add_ext(VAR_AFF_RAYE,-1,1,4);
	_dialog("[c=#39f979]Raye:[/c] That's the spirit! A great ass is an ass, right? (Oops. I'm not fooling anyone talking like that.)");
	_dialog("[c=#39f979]Raye:[/c] It’s good dick though.");
	_jump("EVENT_DANCER_LOTUS_RESTROOM_after_CHOICE_1");
	
_label("EVENT_DANCER_LOTUS_RESTROOM_CHOICE_1_2");
	_var_add_ext(VAR_AFF_RAYE,1,1,4);
	_dialog("[c=#39f979]Raye:[/c] I knew it, you naughty boy! Go on, shove it in! Please <3");
	_jump("EVENT_DANCER_LOTUS_RESTROOM_after_CHOICE_1");
	
_label("EVENT_DANCER_LOTUS_RESTROOM_after_CHOICE_1");
	_change("CG",spr_hentai_cg_raye_restroom_2,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#39f979]Raye:[/c] My naughty girl pussy is dripping wet.");
	_dialog("[c=#39f979]Raye:[/c] Ahh yesss! See how it slid right in! Yes, I’m such anal slut.");
	_dialog("[c=#39f979]Raye:[/c] Ohhh yess you’re so hard! I can feel you sawing right on my prost— err…");
	_dialog("[c=#6678ff]James:[/c] huh?");
	_dialog("[c=#39f979]Raye:[/c] Pussy, I mean pussy! It feels so good! You’re gonna make me cum, without touching my clit!");
	_dialog("[c=#6678ff](Her skin is covered by a sheet of sweat, from the frantic dance. It’s only making it easier to slide in and out her!)[/c]");
	_dialog("[c=#6678ff](Humping her butt backward while making a circular movement… She’s an expert… This… this is a sex dance!)[/c]");
	_dialog("[c=#6678ff](Her skin feels great… this is too much… i’m…)[/c]");
	_dialog("[c=#6678ff]James:[/c] …i’m gonna cum!");
	_dialog("[c=#39f979]Raye:[/c] Yeah, let loose! Your dick feels sooo hard!");
	_dialog("[c=#39f979]Raye:[/c] We are going to cum together! Do it, harder! Fuck me! Fuck me until I cum!");
	_dialog("[c=#6678ff]James:[/c] Y- Yeah! I’m cumming! I’m cumming so hard!");
	_change("CG",spr_hentai_cg_raye_restroom_3_1,FADE_SLOW);
	_pause(2,0,true);
	_show("CG2",spr_hentai_cg_raye_restroom_3_2,5,5,1,DEPTH_BG_ELE-1,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#39f979]Raye:[/c] aaaahhhhhhh yeaahhhhhh");
	_dialog("[c=#39f979]Raye:[/c] So full. You pumped me full of cum! You dirty bastard! Your dick gushed so much inside me…");
	_dialog("[c=#39f979]Raye:[/c] Naughty boy! Did you liked stretching my ass and filling me with cum?");
	_dialog("[c=#39f979]Raye:[/c] Now, let’s get outta here! I could go all day, but this is just a quickie.");
	
	_main_gui(true);
	_execute(time_hour_passed,2);
	_hide_all_except("BG",FADE_NORMAL);
	_change("BG",spr_bg_LotusClub,FADE_NORMAL);
	_pause(0.5,0,true);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James! Where have you been?");
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I was just… uh… taking a leak.");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Alright, alright. I don’t go to the men’s room. There are a lot of people screwing in there.");
	_dialog("[c=#ff00ff]Chris:[/c] No class.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] In the restroom?  Yeah, no class at all.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] ...");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] What?");
	_dialog("[c=#6678ff]James:[/c] Nothing.");
	_var_add(VAR_EVENT_DANCER_LOTUS_RESTROOM,1);
	if current_time_get_name() == "Late Night"
		_jump("Menu");
	else
		_jump("Closed");	
		
		
_label("EVENT_DANCER_CHOICE_2_2");
	_var_add_ext(VAR_AFF_RAYE,-1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Sorry, I’m waiting for someone else, she’s about to arrive.");
	_change("raye",spr_char_Raye_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] Oh right, the girl you’ve came with. No hard feelings.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] That’s not what I mean… Y-you got it all wrong!");
	_change("raye",spr_char_Raye_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Raye:[/c] Well, maybe next time.");
	_execute(time_hour_passed,2);
	if current_time_get_name() == "Late Night"
		_jump("Menu");
	else
		_jump("Closed");
*/	
_border(100); // add content before this
		
_label("Look around Normal");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](Just unknown faces, and Chris talking with a group of strangers, better not bother her.)[/c]");
	_jump("Menu");

_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_main_map,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Move to the next location?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_LOTUS_DEW_VISIT");
	_var_set(VAR_EVENT_LOTUS_DEW_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] This is my favorite hangout.");
	if var_get(VAR_CHARISMA) >= 3
		_jump("EVENT_LOTUS_DEW_VISIT_CHOICE_1_CHARISMA");
	else
		_jump("EVENT_LOTUS_DEW_VISIT_after_CHOICE_1");
		
_label("EVENT_LOTUS_DEW_VISIT_CHOICE_1_CHARISMA");
	_dialog("*: Hey Chris!");
	_dialog("*: Sup Chris!");
	_dialog("[c=#6678ff]James:[/c] I see you’re like a celebrity here.");
	_dialog("[c=#ff00ff]Chris:[/c] No, I’m just fabulous ♫!");
	_jump("EVENT_LOTUS_DEW_VISIT_after_CHOICE_1");
	
_label("EVENT_LOTUS_DEW_VISIT_after_CHOICE_1");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] That’s the bartender and the house’s DJ. She’s the gossip girl. Hey Dayna!");
	_if(var_get(VAR_AFF_DAYNA)<1,_var_set,VAR_AFF_DAYNA,1);
	_show("dayna",spr_char_Dayna_Sly,1.8,YNUM_DAYNE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Yo Chris! What’s up!");
	_dialog("[c=#ff00ff]Chris:[/c] Dayna, this is James Lakin, my manager.");
	_dialog("[c=#39f979]Dayna:[/c] Since when you’ve become an entertainer?");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Since I was born ♫! Now I don’t perform for free anymore, though.");
	_dialog("[c=#39f979]Dayna:[/c] Oh that’s great I guess.");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James, let’s play a game? See Dayna? Dick or no Dick?");
	_change("dayna",spr_char_Dayna_Worry,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] What weird game is that? Oh I got it…");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](After thinking about all today’s events…)[/c]");
	_choice(spr_choice,"“Dick”","EVENT_LOTUS_DEW_VISIT_CHOICE_2_1",noone,fa_center,fa_center);
	_choice(spr_choice,"“No dick”","EVENT_LOTUS_DEW_VISIT_CHOICE_2_2",noone,fa_center,fa_center);
	_block();

_label("EVENT_LOTUS_DEW_VISIT_CHOICE_2_1");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Wrong. Now you to pay me a drink.");
	_jump("Menu");
	
_label("EVENT_LOTUS_DEW_VISIT_CHOICE_2_2");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Alright! Eh… You look relieved haha! Dayna the drink is on me.");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] But it’s my money… argh!");
	_jump("Menu");

_border(2); // add content before this
/*
_label("EVENT_STRIP_CLUB_PT1_INTRO");
	_choice(spr_choice,"Continue","EVENT_STRIP_CLUB_PT1_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Dayna usually won’t accept drinks while she’s working, but today…[/c]",false);
	_block();

_label("EVENT_STRIP_CLUB_PT1_START");
	_show("james",spr_char_James_Blush,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff](As I approach Dayna i feel a weird sense of trepidation. She’s turned me down when I’ve offered her drinks before, so...)[/c]");
	_change("dayna",spr_char_Dayna_Worry,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Uh, whats with the look, James?");
	_dialog("[c=#6678ff](Shit. I’m just standing here, blushing like an idiot.)[/c]");
	_dialog("[c=#6678ff]James:[/c] N-nothing! I just figured you’d like a drink.");
	_change("dayna",spr_char_Dayna_Party,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] My my, how bold of you.");
	_change("dayna",spr_char_Dayna_Sly,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] ...Sure, why not? I’ll accept one, just this once. At least from a cutie like you.");
	_dialog("[c=#6678ff](She downs the glass in one gulp. I’m left slack jawed at the display.)[/c]");
	_change("dayna",spr_char_Dayna_Worry,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] *sigh* I really love this job, James. But it keeps getting harder and harder every day.");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Seems like something’s on your mind. Penny for your thoughts?");
	_change("dayna",spr_char_Dayna_Sly,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Heh, nosy aren’t you? Careful, in a place like this you never know where your nose might lead.");
	_dialog("[c=#39f979]Dayna:[/c] I suppose I can tell you. You did after all buy me a drink.");
	_change("dayna",spr_char_Dayna_Worry,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] A new club is opening up in town. The “Desire Club.” Word on the street is they’re hiring dancer girls for shows.");
	
	_main_gui(false);
	_show("JOB_MAP",spr_bg_map,5,5,1,DEPTH_CHARACTER-5,false,FADE_FAST);
	_show("JOB_MAP_Apt",spr_bg_map_Apt,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_GirlsOnlyClub",spr_bg_map_GirlsOnlyClub,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_Hospital",spr_bg_map_Hospital,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_LotusDew",spr_bg_map_LotusDew,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_STPHeadquarters",spr_bg_map_STPHeadquarters,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_Park",spr_bg_map_Park,5,5,1,DEPTH_CHARACTER-7,false,FADE_FAST);
	_show("JOB_MAP_front",spr_bg_map_front,5,5,1,DEPTH_CHARACTER-8,false,FADE_FAST);
	if jobs_list_job_exists(JOBNAME_Baby_Sitting)
		_show("JOB_MAP_Kanes",spr_bg_map_Kanes_Residence,5,5,1,DEPTH_CHARACTER-9,false,FADE_FAST);
	else
		_void();
	
	_show("JOB_Desire_Club",spr_blank,5,5,1,DEPTH_CHARACTER-9,false,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_Desire_Club",spr_bg_map_Desire_Club,FADE_FAST);
	_pause(0.5,0,true);
	_change("JOB_Desire_Club",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_Desire_Club",spr_bg_map_Desire_Club,FADE_FAST);
	_pause(0.5,0,true);
	_change("JOB_Desire_Club",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_Desire_Club",spr_bg_map_Desire_Club,FADE_FAST);
	_pause(0.5,0,true);
	
	_main_gui(true);
	_hide_all_except("BG","dayna","james",FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] ...Along with some bartenders.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Ah, so you’re worried about competition.");
	_dialog("[c=#39f979]Dayna:[/c] I… what if a bunch of the regulars start going there? I’ll be out of a job in a week! Not to mention the loss of tips…");
	_dialog("[c=#6678ff](She looks seriously dejected about the idea of a rival club ruining her profits. Maybe I can cheer her up.)[/c]",false);
	
	_choice(spr_choice,"Odds are: only poor college kids with no spending cash will ever bother going over there.","EVENT_STRIP_CLUB_PT1_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"Everyone knows Lotus is for cool people.","EVENT_STRIP_CLUB_PT1_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_STRIP_CLUB_PT1_CHOICE_1_1");
	_var_add_ext(VAR_AFF_DAYNA,-1,1,4);
	_change("dayna",spr_char_Dayna_Tired,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Nah I heard that drinks in that kind of clubs are hell expensive. Soon they will be crowded with rich people.");
	_change("dayna",spr_char_Dayna_Party,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] But thanks for trying to cheer me up.");
	_jump("EVENT_STRIP_CLUB_PT1_after_CHOICE_1");
	
_label("EVENT_STRIP_CLUB_PT1_CHOICE_1_2");
	_change("dayna",spr_char_Dayna_Worry,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] How can you be so sure?");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Well… you’re here, aren’t you?");
	_dialog("[c=#39f979]Dayna:[/c] I...");
	_var_add_ext(VAR_AFF_DAYNA,1,1,4);
	_change("dayna",spr_char_Dayna_Sly,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] Well, aren’t you just the sweetest thing?");
	_jump("EVENT_STRIP_CLUB_PT1_after_CHOICE_1");
	
_label("EVENT_STRIP_CLUB_PT1_after_CHOICE_1");
	_change("dayna",spr_char_Dayna_Sly,FADE_NORMAL);
	_dialog("[c=#39f979]Dayna:[/c] I guess I’m overreacting. It’s not like I own the Lotus anyhow. Maybe it wouldn’t even be so bad to work at Desire myself, if things go south here.");
	_dialog("[c=#39f979]Dayna:[/c] Thanks, James. Remind me to return the favor someday. I’ll even let you choose where I plant the “favor”.");
	_var_set(VAR_EVENT_STRIP_CLUB_PT1,true);
	_jump("Menu");
*/	

_border(98); // add content before this




