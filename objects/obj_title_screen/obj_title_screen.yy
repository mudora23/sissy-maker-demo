{
    "id": "bc95265c-7cf4-4579-84c6-dd8bbe2bd65e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title_screen",
    "eventList": [
        {
            "id": "0e7c7590-26eb-4571-80e7-2ee149e4a760",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc95265c-7cf4-4579-84c6-dd8bbe2bd65e"
        },
        {
            "id": "ac432ccd-ec16-4911-8c30-e03d0f7643d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bc95265c-7cf4-4579-84c6-dd8bbe2bd65e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cbe59b03-d964-4067-962f-4189d2e33e6e",
    "visible": true
}