///logo
if entry
{
    logo_y = approach(logo_y,logo_y_down,50*delta_time_sec());
    if logo_y == logo_y_down
        entry = false;
}
else
{
    if goingup
    {
        logo_y = approach(logo_y,logo_y_up,12*delta_time_sec());
        if logo_y == logo_y_up
            goingup = false;
    }
    else
    {
        logo_y = approach(logo_y,logo_y_down,12*delta_time_sec());
        if logo_y == logo_y_down
            goingup = true;
    }
}
logo_image_alpha = smooth_approach(logo_image_alpha,1,1.2*delta_time_sec());
draw_self();
draw_sprite_ext(spr_title_screen_logo,-1,logo_x,logo_y,1,1,0,c_white,logo_image_alpha);

draw_set_font(font_general_13);
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);
draw_set_color(c_dkgray);
if obj_control.debug_enabled
	draw_text(795,600,"v"+string(obj_control.version)+" demo-D");
else
	draw_text(795,600,"v"+string(obj_control.version)+" demo");
draw_reset();
