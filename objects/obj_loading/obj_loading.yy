{
    "id": "6cdb66bf-2b02-48b1-9f7c-0d2bd7b068a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loading",
    "eventList": [
        {
            "id": "7f96ea67-f570-422e-85ce-23ded861d12f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cdb66bf-2b02-48b1-9f7c-0d2bd7b068a6"
        },
        {
            "id": "dbf6decd-1daa-47a2-9e2e-e4c92669a558",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6cdb66bf-2b02-48b1-9f7c-0d2bd7b068a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}