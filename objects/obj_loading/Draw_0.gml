// You can write your code in this editor

if saving_slot != noone
{
	var save_name = "save"+string(saving_slot);
	
	if saving_frame == 0
	{
		// saving thumbnail
		
	}
	else if saving_frame == 1
	{
		// saving thumbnaildate

	}
	else if saving_frame == 2
	{
		// saving stats
		ds_map_destroy(obj_control.var_map);
		obj_control.var_map = sissy_maker_load_map(save_name+"stats.dat",obj_control.secretString);
		var_init_missing_keys();
		room_goto(asset_get_index(obj_control.var_map[? VAR_CURRENT_ROOM]));
	}
	else if saving_frame == 3
	{
		// saving scene variables
		ds_map_destroy(obj_scene.scene_map);
		obj_scene.scene_map = sissy_maker_load_map(save_name+"scene.dat",obj_control.secretString);
		ds_map_destroy(text_ref_map);
		text_ref_map = sissy_maker_load_map(save_name+"ref.dat",obj_control.secretString);
		ref_load_from_map(text_ref_map);
	}
	else if saving_frame == 4
	{
		// saving jobs
		ds_map_destroy(obj_control.jobs_map);
		obj_control.jobs_map = sissy_maker_load_map(save_name+"jobs.dat",obj_control.secretString);
		jobs_list_load_from_map(obj_control.jobs_map);

	}
	else if saving_frame == 5
	{
		choice_map = sissy_maker_load_map(save_name+"choice.dat",obj_control.secretString);
		choices_load_from_map(choice_map);
		ds_map_destroy(choice_map);
	}
	else if saving_frame == 6
	{
		sprite_map = sissy_maker_load_map(save_name+"sprite.dat",obj_control.secretString);
		sprites_load_from_map(sprite_map);
		ds_map_destroy(sprite_map);

	}
	else if saving_frame == 7
	{
		shadow_map = sissy_maker_load_map(save_name+"shadow.dat",obj_control.secretString);
		shadows_load_from_map(shadow_map);
		ds_map_destroy(shadow_map);

	}
	else if saving_frame == 8
	{
		// hotfixes
		sissy_maker_hotfixes();

	}
	else
	{
		obj_main_gui.menu_open = "";
		instance_destroy();
		
	}

}

else
	instance_destroy();



saving_frame++;