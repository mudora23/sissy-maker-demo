{
    "id": "33993db1-bca7-4d92-970b-b5ba885f3894",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_new_version_popup",
    "eventList": [
        {
            "id": "c36d1197-1769-443e-a931-4854441438ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33993db1-bca7-4d92-970b-b5ba885f3894"
        },
        {
            "id": "2447851b-7e4e-40b9-b7aa-30e6e5f4f2f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "33993db1-bca7-4d92-970b-b5ba885f3894"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ee3eab28-1039-4442-ad76-66a3f076a1a3",
    "visible": true
}