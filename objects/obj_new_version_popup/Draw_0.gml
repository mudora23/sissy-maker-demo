if obj_control.online_version > real(obj_control.version) && obj_control.online_versionLink != ""
{

	if room == room_title_screen
	{
		if mouse_over()
			image_alpha = 0.9;
		else
			image_alpha = 1;
		if alpha_increasing
		{
			alpha = alpha + alpha_increasing_rate * delta_time_sec();
			if alpha >= 1 alpha_increasing = false;
		}
		else
		{
			alpha = alpha - alpha_increasing_rate * delta_time_sec();
			if alpha <= 0.3 alpha_increasing = true;
		}

		
		var clicked = mouse_check_button_pressed(mb_left) && obj_main_gui.menu_open == "";
		
		draw_self();
		
		draw_set_halign(fa_center);
		draw_set_valign(fa_bottom);
		var text_padding = 0;
		draw_set_font(font_general_13b);
		var text_width = string_width(text_1);
		var text_height = string_height(text_1);
		var xx = x+sprite_width/2;
		var yy = y+text_padding+text_height+5;
		draw_text_color(xx,yy,text_1,c_white,c_white,c_white,c_white,alpha);
		
		draw_set_font(font_general_13b);
		var text_width = string_width(text_2);
		var text_height = string_height(text_2);
		yy += text_padding+text_height;
		draw_text_color(xx,yy,text_2,c_white,c_white,c_white,c_white,alpha);
		
		draw_set_font(font_general_10b);
		var text_width = string_width(text_b);
		var text_height = string_height(text_b);
		yy += text_padding+text_height;
		draw_text_color(xx,yy,text_b,c_white,c_white,c_white,c_white,alpha);
		
		draw_reset();
		if mouse_over() && clicked
		{
			scene_play_sfx(sfx_button_01);
			url_open(obj_control.online_versionLink); 
		}
		
	}
	else
	{
		image_alpha = 0;
	}

}