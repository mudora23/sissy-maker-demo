{
    "id": "2fff0d0b-312f-4fa5-b172-af9c3b4c0ac9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_new",
    "eventList": [
        {
            "id": "da68f5cd-46a7-4866-a613-eb7d3b880c33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fff0d0b-312f-4fa5-b172-af9c3b4c0ac9"
        },
        {
            "id": "9427e303-bcd7-4e38-beb5-36875cd87124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2fff0d0b-312f-4fa5-b172-af9c3b4c0ac9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "visible": true
}