{
    "id": "8a2be30f-8c45-48c2-9c21-7bf952fc4e2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_load",
    "eventList": [
        {
            "id": "8d7cfa6f-f1c1-45d9-84c4-5bd3a3685c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a2be30f-8c45-48c2-9c21-7bf952fc4e2d"
        },
        {
            "id": "b4ff595f-6c62-4ace-af05-bf369258c517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8a2be30f-8c45-48c2-9c21-7bf952fc4e2d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c8aaa39b-0ffd-4757-88ed-d09b191a7643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd198e4d-2c1d-44a7-9721-9795c24f2694",
    "visible": true
}