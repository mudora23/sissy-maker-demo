// You can write your code in this editor

if saving_slot != noone && instance_exists(obj_scene)
{
	var save_name = "save"+string(saving_slot);
	
	if saving_frame == 0
	{
		// saving thumbnail
		if sprite_exists(obj_control.pausescreen)
		    sprite_save(obj_control.pausescreen, 0, string(save_name)+".png");

	}
	else if saving_frame == 1
	{
		// saving thumbnaildate
		INI_SecSaveString(string(saving_slot),get_date_string(),"thumbnaildate");

	}
	else if saving_frame == 2
	{
		// saving stats
		sissy_maker_save_map(save_name+"stats.dat",obj_control.var_map,obj_control.secretString);
	}
	else if saving_frame == 3
	{
		// saving scene variables
		sissy_maker_save_map(save_name+"scene.dat",obj_scene.scene_map,obj_control.secretString);
		
		// saving ref
		var text_ref_map = ds_map_create();
		
		var temp_scene_text_ref_color_index = ds_list_create();
		var temp_scene_text_ref_color_color = ds_list_create();
		var temp_scene_text_ref_font_index = ds_list_create();
		var temp_scene_text_ref_font_font = ds_list_create();
		
		ds_list_copy(temp_scene_text_ref_color_index,obj_scene.scene_text_ref_color_index);
		ds_list_copy(temp_scene_text_ref_color_color,obj_scene.scene_text_ref_color_color);
		ds_list_copy(temp_scene_text_ref_font_index,obj_scene.scene_text_ref_font_index);
		ds_list_copy(temp_scene_text_ref_font_font,obj_scene.scene_text_ref_font_font);
		
		ds_map_add_list(text_ref_map,"color_index",temp_scene_text_ref_color_index);
		ds_map_add_list(text_ref_map,"color_color",temp_scene_text_ref_color_color);
		ds_map_add_list(text_ref_map,"font_index",temp_scene_text_ref_font_index);
		ds_map_add_list(text_ref_map,"font_font",temp_scene_text_ref_font_font);
		
		
		sissy_maker_save_map(save_name+"ref.dat",text_ref_map,obj_control.secretString);
		
		ds_map_destroy(text_ref_map);
	}	
	else if saving_frame == 4
	{
		// saving jobs
		sissy_maker_save_map(save_name+"jobs.dat",obj_control.jobs_map,obj_control.secretString);
	}
	else if saving_frame == 5
	{
		// saving choices step1
		with obj_choice_object ds_list_add(obj_saving.choice_id_list,id);
		
		// saving choices step2
		var x_list = ds_list_create();
		var y_list = ds_list_create();
		var xhide_list = ds_list_create();
		var yhide_list = ds_list_create();
		var xshow_list = ds_list_create();
		var yshow_list = ds_list_create();
		var hiding_list = ds_list_create();
		var xalign_list = ds_list_create();
		var yalign_list = ds_list_create();
		var image_alpha_list = ds_list_create();
		var sprite_index_list = ds_list_create();
		var depth_list = ds_list_create();
		var delay_list = ds_list_create();
		var clicked_list = ds_list_create();
		var choice_text_list = ds_list_create();
		var choice_label_list = ds_list_create();
		var choice_room_list = ds_list_create();

		for(var i = 0; i<ds_list_size(obj_saving.choice_id_list);i++)
		{
			var choice_id = obj_saving.choice_id_list[| i];
			ds_list_add(x_list,choice_id.x);
			ds_list_add(y_list,choice_id.y);
			ds_list_add(xhide_list,choice_id.xhide);
			ds_list_add(yhide_list,choice_id.yhide);
			ds_list_add(xshow_list,choice_id.xshow);
			ds_list_add(yshow_list,choice_id.yshow);
			ds_list_add(hiding_list,choice_id.hiding);
			ds_list_add(xalign_list,choice_id.xalign);
			ds_list_add(yalign_list,choice_id.yalign);
			ds_list_add(image_alpha_list,choice_id.image_alpha);
			if choice_id.sprite_index == noone ds_list_add(sprite_index_list,"");
			else ds_list_add(sprite_index_list,sprite_get_name(choice_id.sprite_index));
			ds_list_add(depth_list,choice_id.depth);
			ds_list_add(delay_list,choice_id.delay);
			ds_list_add(clicked_list,choice_id.clicked);
			ds_list_add(choice_text_list,choice_id.choice_text);
			ds_list_add(choice_label_list,choice_id.choice_label);
			if choice_id.choice_room == noone ds_list_add(choice_room_list,"");
			else ds_list_add(choice_room_list,room_get_name(choice_id.choice_room));
		}
		ds_map_add_list(choice_map,"x",x_list);
		ds_map_add_list(choice_map,"y",y_list);
		ds_map_add_list(choice_map,"xhide",xhide_list);
		ds_map_add_list(choice_map,"yhide",yhide_list);
		ds_map_add_list(choice_map,"xshow",xshow_list);
		ds_map_add_list(choice_map,"yshow",yshow_list);
		ds_map_add_list(choice_map,"hiding",hiding_list);
		ds_map_add_list(choice_map,"xalign",xalign_list);
		ds_map_add_list(choice_map,"yalign",yalign_list);
		ds_map_add_list(choice_map,"image_alpha",image_alpha_list);
		ds_map_add_list(choice_map,"sprite_index",sprite_index_list);
		ds_map_add_list(choice_map,"depth",depth_list);
		ds_map_add_list(choice_map,"delay",delay_list);
		ds_map_add_list(choice_map,"clicked",clicked_list);
		ds_map_add_list(choice_map,"choice_text",choice_text_list);
		ds_map_add_list(choice_map,"choice_label",choice_label_list);
		ds_map_add_list(choice_map,"choice_room",choice_room_list);
		
		// saving choices step3
		sissy_maker_save_map(save_name+"choice.dat",choice_map,obj_control.secretString);
		ds_map_destroy(choice_map);
	}
	else if saving_frame == 6
	{
		// saving sprite step1
		with obj_sprite_object ds_list_add(obj_saving.sprite_id_list,id);
		
		// saving sprite step2
		var x_list = ds_list_create();
		var y_list = ds_list_create();
		var sprite_index_list = ds_list_create();
		var image_alpha_list = ds_list_create();
		var image_index_list = ds_list_create();
		var target_sprite_index_list = ds_list_create();
		var target_sprite_alpha_list = ds_list_create();
		var target_sprite_alpha_change_rate_list = ds_list_create();
		var target_image_index_list = ds_list_create();
		var fade_speed_list = ds_list_create();
		var slide_speed_list = ds_list_create();
		var xtarget_list = ds_list_create();
		var ytarget_list = ds_list_create();
		var hiding_list = ds_list_create();
		var flip_list = ds_list_create();
		var size_ratio_list = ds_list_create();
		var name_list = ds_list_create();
		var depth_list = ds_list_create();
		
		for(var i = 0; i<ds_list_size(obj_saving.sprite_id_list);i++)
		{
			var sprite_id = obj_saving.sprite_id_list[| i];
			ds_list_add(x_list,sprite_id.x);
			ds_list_add(y_list,sprite_id.y);
			if sprite_id.sprite_index == noone ds_list_add(sprite_index_list,"");
			else ds_list_add(sprite_index_list,sprite_get_name(sprite_id.sprite_index));
			ds_list_add(image_alpha_list,sprite_id.image_alpha);
			ds_list_add(image_index_list,sprite_id.image_index);
			if sprite_id.target_sprite_index == noone ds_list_add(target_sprite_index_list,"");
			else ds_list_add(target_sprite_index_list,sprite_get_name(sprite_id.target_sprite_index));
			ds_list_add(target_sprite_alpha_list,sprite_id.target_sprite_alpha);
			ds_list_add(target_sprite_alpha_change_rate_list,sprite_id.target_sprite_alpha_change_rate);
			ds_list_add(target_image_index_list,sprite_id.target_image_index);
			ds_list_add(fade_speed_list,sprite_id.fade_speed);
			ds_list_add(slide_speed_list,sprite_id.slide_speed);
			ds_list_add(xtarget_list,sprite_id.xtarget);
			ds_list_add(ytarget_list,sprite_id.ytarget);
			ds_list_add(hiding_list,sprite_id.hiding);
			ds_list_add(flip_list,sprite_id.flip);
			ds_list_add(size_ratio_list,sprite_id.size_ratio);
			ds_list_add(name_list,sprite_id.name);
			ds_list_add(depth_list,sprite_id.depth);
		}
		ds_map_add_list(sprite_map,"x",x_list);
		ds_map_add_list(sprite_map,"y",y_list);
		ds_map_add_list(sprite_map,"sprite_index",sprite_index_list);
		ds_map_add_list(sprite_map,"image_alpha",image_alpha_list);
		ds_map_add_list(sprite_map,"image_index",image_index_list);
		ds_map_add_list(sprite_map,"target_sprite_index",target_sprite_index_list);
		ds_map_add_list(sprite_map,"target_sprite_alpha",target_sprite_alpha_list);
		ds_map_add_list(sprite_map,"target_sprite_alpha_change_rate",target_sprite_alpha_change_rate_list);
		ds_map_add_list(sprite_map,"target_image_index",target_image_index_list);
		ds_map_add_list(sprite_map,"fade_speed",fade_speed_list);
		ds_map_add_list(sprite_map,"slide_speed",slide_speed_list);
		ds_map_add_list(sprite_map,"xtarget",xtarget_list);
		ds_map_add_list(sprite_map,"ytarget",ytarget_list);
		ds_map_add_list(sprite_map,"hiding",hiding_list);
		ds_map_add_list(sprite_map,"flip",flip_list);
		ds_map_add_list(sprite_map,"size_ratio",size_ratio_list);
		ds_map_add_list(sprite_map,"name",name_list);
		ds_map_add_list(sprite_map,"depth",depth_list);
		
		// saving sprite step3
		sissy_maker_save_map(save_name+"sprite.dat",sprite_map,obj_control.secretString);
		ds_map_destroy(sprite_map);

	}
	else if saving_frame == 7
	{

		// saving shadow step1
		with obj_shadow_object ds_list_add(obj_saving.shadow_id_list,id);
		
		// saving shadow step2
		var sprite_index_list = ds_list_create();
		var image_alpha_list = ds_list_create();
		var image_alpha_min_list = ds_list_create();
		var image_alpha_max_list = ds_list_create();
		var image_alpha_increasing_list = ds_list_create();
		var fade_speed_list = ds_list_create();
		var shadow_label_list = ds_list_create();
		var shadow_room_list = ds_list_create();
		var depth_list = ds_list_create();
		
		for(var i = 0; i< ds_list_size(obj_saving.shadow_id_list);i++)
		{
			var shadow_id = obj_saving.shadow_id_list[| i];
			if shadow_id.sprite_index == noone ds_list_add(sprite_index_list,"");
			else ds_list_add(sprite_index_list,sprite_get_name(shadow_id.sprite_index));
			ds_list_add(image_alpha_list,shadow_id.image_alpha);
			ds_list_add(image_alpha_min_list,shadow_id.image_alpha_min);
			ds_list_add(image_alpha_max_list,shadow_id.image_alpha_max);
			ds_list_add(image_alpha_increasing_list,shadow_id.image_alpha_increasing);
			ds_list_add(fade_speed_list,shadow_id.fade_speed);
			ds_list_add(shadow_label_list,shadow_id.shadow_label);
			if shadow_id.shadow_room == noone ds_list_add(shadow_room_list,"");
			else ds_list_add(shadow_room_list,room_get_name(shadow_id.shadow_room));
			ds_list_add(depth_list,shadow_id.depth);
		}
		ds_map_add_list(shadow_map,"sprite_index",sprite_index_list);
		ds_map_add_list(shadow_map,"image_alpha",image_alpha_list);
		ds_map_add_list(shadow_map,"image_alpha_min",image_alpha_min_list);
		ds_map_add_list(shadow_map,"image_alpha_max",image_alpha_max_list);
		ds_map_add_list(shadow_map,"image_alpha_increasing",image_alpha_increasing_list);
		ds_map_add_list(shadow_map,"fade_speed",fade_speed_list);
		ds_map_add_list(shadow_map,"shadow_label",shadow_label_list);
		ds_map_add_list(shadow_map,"shadow_room",shadow_room_list);
		ds_map_add_list(shadow_map,"depth",depth_list);
	
		// saving shadow step3
		sissy_maker_save_map(save_name+"shadow.dat",shadow_map,obj_control.secretString);
		ds_map_destroy(shadow_map);
	}
	else
	{
		thumbnail_update(saving_slot);
		if returning room_goto_ext(room_title_screen,true);
		instance_destroy();
	}
}
else
{
	if returning room_goto_ext(room_title_screen,true);
	instance_destroy();
}



saving_frame++;