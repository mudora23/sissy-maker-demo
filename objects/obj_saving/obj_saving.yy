{
    "id": "e8ade98c-bcb1-4aa1-9aeb-4ecc2adbe320",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_saving",
    "eventList": [
        {
            "id": "2c463446-07f4-4b02-99fe-182475e9fd73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8ade98c-bcb1-4aa1-9aeb-4ecc2adbe320"
        },
        {
            "id": "2d10a19a-ca18-4dbb-9836-b4af6765c1da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e8ade98c-bcb1-4aa1-9aeb-4ecc2adbe320"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}