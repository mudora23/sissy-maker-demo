{
    "id": "7a849f5e-160f-47a4-92ad-9edf7021f376",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition",
    "eventList": [
        {
            "id": "14c1765f-a5c8-4eb5-a194-6dc28f68c984",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a849f5e-160f-47a4-92ad-9edf7021f376"
        },
        {
            "id": "0ed3d65c-91c0-479b-80dd-444ac5b1f74e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7a849f5e-160f-47a4-92ad-9edf7021f376"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "db42eb91-1fd3-4b92-bee6-f5ae6258f132",
    "visible": true
}