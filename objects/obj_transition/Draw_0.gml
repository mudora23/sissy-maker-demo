if fade 
    var fade_rate_var = fade_rate*delta_time_sec();
else
    var fade_rate_var = 1;


if next_room == -1 || room == next_room
{
    image_alpha = approach(image_alpha,0,fade_rate_var);
    if image_alpha == 0
        next_room = -1;
}
else
{
    image_alpha = approach(image_alpha,1,fade_rate_var);
    if image_alpha == 1
	{
		previous_room = room;
        room_goto(next_room);
	}
}

if fade
    draw_sprite_ext(spr_white,-1,0,0,1,1,0,c_black,image_alpha);