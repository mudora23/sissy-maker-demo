{
    "id": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene",
    "eventList": [
        {
            "id": "ac050dfb-f9bf-46ad-9e58-b926352ba95d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42dbcc52-2dd2-4980-95de-811041fccc94"
        },
        {
            "id": "512b8179-d163-4c53-8a1b-2c7b64e43859",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "42dbcc52-2dd2-4980-95de-811041fccc94"
        },
        {
            "id": "cd07b593-4d86-4d32-80b5-46fb4bc7cc4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "42dbcc52-2dd2-4980-95de-811041fccc94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cbfd0b90-7862-48e5-8b92-838d1b12d038",
    "visible": true
}