ds_map_destroy(scene_map);
ds_map_destroy(scene_label_map);
ds_list_destroy(scene_choice_order);

ds_list_destroy(scene_text_ref_color_index);
ds_list_destroy(scene_text_ref_color_color);
ds_list_destroy(scene_text_ref_font_index);
ds_list_destroy(scene_text_ref_font_font);