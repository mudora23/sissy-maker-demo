// variables
scene_map = ds_map_create();
if !ds_map_exists(scene_map,SCENE_POSI) scene_map[? SCENE_POSI] = 0;
if !ds_map_exists(scene_map,SCENE_POSI_STEP) scene_map[? SCENE_POSI_STEP] = 0;
if !ds_map_exists(scene_map,SCENE_TOTAL_STEP) scene_map[? SCENE_TOTAL_STEP] = 0;
if !ds_map_exists(scene_map,SCENE_POSI_SEC) scene_map[? SCENE_POSI_SEC] = 0;
if !ds_map_exists(scene_map,SCENE_TOTAL_SEC) scene_map[? SCENE_TOTAL_SEC] = 0;
if !ds_map_exists(scene_map,SCENE_POSI_CURRENT) scene_map[? SCENE_POSI_CURRENT] = 0;
if !ds_map_exists(scene_map,SCENE_LABEL_MARKED) scene_map[? SCENE_LABEL_MARKED] = false;
if !ds_map_exists(scene_map,SCENE_TEXT) scene_map[? SCENE_TEXT] = "";
if !ds_map_exists(scene_map,SCENE_CHAR_POSI) scene_map[? SCENE_CHAR_POSI] = 0;





scene_text_ref_color_index = ds_list_create();
scene_text_ref_color_color = ds_list_create();
scene_text_ref_font_index = ds_list_create();
scene_text_ref_font_font = ds_list_create();



scene_label_map = ds_map_create();

scene_choice_order = ds_list_create();

depth = DEPTH_TEXTBOX_TEXT;

/// stop the music
if !sissy_maker_is_loading()
	scene_stop_bgm();
