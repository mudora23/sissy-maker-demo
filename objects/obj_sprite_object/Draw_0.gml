// image_alpha
if !hiding
{
	image_alpha = approach(image_alpha,1,fade_speed*delta_time_sec());
}
else
{
	image_alpha = approach(image_alpha,0,fade_speed*delta_time_sec());
	if image_alpha == 0 instance_destroy();
}

// sliding
if slide_speed > 10
{
	x = approach(x,xtarget,slide_speed*delta_time_sec());
	y = approach(y,ytarget,slide_speed*delta_time_sec());
}
else
{
	x = smooth_approach(x,xtarget,slide_speed*delta_time_sec());
	y = smooth_approach(y,ytarget,slide_speed*delta_time_sec());
}
// flip
if flip
    var flip_dir = -1;
else
    var flip_dir = 1;


var is_bg = (string_copy(sprite_get_name(sprite_index),1,6) == "spr_bg");
var is_chris = (string_copy(sprite_get_name(sprite_index),1,14) == "spr_char_Chris");

if image_number > 1 && is_bg && current_time_get_name() == "Afternoon" image_index = 1;
else if image_number > 2 && is_bg && current_time_get_name() == "Evening" image_index = 2;
else if image_number > 3 && is_bg && current_time_get_name() == "Late Night" image_index = 3;
else if image_number > 2 && is_chris && var_get(VAR_CUP_SIZE) >= 2 image_index = 2;
else if image_number > 1 && is_chris && var_get(VAR_CUP_SIZE) >= 1 image_index = 1;
else image_index = 0;


if target_sprite_index != noone && target_sprite_index != sprite_index
{

    if target_sprite_alpha >= 1.2
    {
        sprite_index = target_sprite_index;
        target_sprite_index = noone;

		
        draw_sprite_ext(sprite_index,image_index,x+flip*sprite_width*size_ratio,y,flip_dir*size_ratio,size_ratio,0,make_color_rgb(255,255,255),image_alpha);
    }
    else
    {
		var target_is_bg = (string_copy(sprite_get_name(target_sprite_index),1,6) == "spr_bg");
		var target_is_chris = (string_copy(sprite_get_name(target_sprite_index),1,14) == "spr_char_Chris");
		if sprite_get_number(target_sprite_index) > 1 && target_is_bg && current_time_get_name() == "Afternoon" target_image_index = 1;
		else if sprite_get_number(target_sprite_index) > 2 && target_is_bg && current_time_get_name() == "Evening" target_image_index = 2;
		else if sprite_get_number(target_sprite_index) > 3 && target_is_bg && current_time_get_name() == "Late Night" target_image_index = 3;
		//else if sprite_get_number(target_sprite_index) > 2 && target_is_chris && var_get(VAR_CUP_SIZE) >= 2 target_image_index = 2;
		else if sprite_get_number(target_sprite_index) > 1 && target_is_chris && var_get(VAR_CUP_SIZE) >= 1 target_image_index = 1;
		else target_image_index = 0;
		
		
		
	
        target_sprite_alpha += target_sprite_alpha_change_rate*delta_time_sec();
        //draw_cg_dim(sprite_index,image_alpha-target_sprite_alpha/1.2,target_sprite_index,target_sprite_alpha);
        draw_sprite_ext(sprite_index,image_index,x+flip*sprite_width*size_ratio,y,flip_dir*size_ratio,size_ratio,0,make_color_rgb(255,255,255),image_alpha-target_sprite_alpha/1.2);
        draw_sprite_ext(target_sprite_index,target_image_index,x+flip*sprite_get_width(target_sprite_index)*size_ratio,y,flip_dir*size_ratio,size_ratio,0,make_color_rgb(255,255,255),target_sprite_alpha);
    }

}
else
{
    target_sprite_alpha = 0;
    draw_sprite_ext(sprite_index,image_index,x+flip*sprite_width*size_ratio,y,flip_dir*size_ratio,size_ratio,0,make_color_rgb(255,255,255),image_alpha);
}