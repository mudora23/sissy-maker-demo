{
    "id": "da33221f-6bb4-4ad0-84e5-4fc81b406a48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sprite_object",
    "eventList": [
        {
            "id": "20c03b95-84d4-417e-b8c9-32bb6afc843b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da33221f-6bb4-4ad0-84e5-4fc81b406a48"
        },
        {
            "id": "1a40491b-b3a0-4625-98c2-cd33be15304f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "da33221f-6bb4-4ad0-84e5-4fc81b406a48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}