{
    "id": "0e167523-16b3-4fab-8ee8-ebc5340c5192",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_desire_club",
    "eventList": [
        {
            "id": "f2fd86f0-e992-4885-abf4-d2ac0d860e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e167523-16b3-4fab-8ee8-ebc5340c5192"
        },
        {
            "id": "575d4300-0445-4e28-8f90-09b23c8aed36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0e167523-16b3-4fab-8ee8-ebc5340c5192"
        },
        {
            "id": "b32e7c73-e7bd-49e6-b9ba-cac3c1aaee9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0e167523-16b3-4fab-8ee8-ebc5340c5192"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}