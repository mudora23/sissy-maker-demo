event_inherited();

_start();
	_placename(PLACENAME_desire_club);
	_main_gui(true);
	_show("BG",spr_bg_desireClub,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_STRIP_CLUB_FIRST_VISIT)
		_jump("STRIP_CLUB_FIRST_VISIT");

	else if current_time_get_name() == "Morning"
		_jump("Closed");
	else if current_time_get_name() == "Afternoon"
		_jump("Closed");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Late Night");

_border(100); // add content before this
		
_label("Stay");
	_stay(false,false,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Evening" || current_time_get_name() == "Late Night"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this


_label("Evening");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("mireille",spr_char_Mireille_Neutral,1.8,YNUM_MIREILLE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Mireille:[/c] Welcome to Desire club! The best place in town to drink, relax and see the lovely sights.");
	_jump("Menu");


_label("Late Night");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("mireille",spr_char_Mireille_Neutral,1.8,YNUM_MIREILLE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Mireille:[/c] Welcome! Heyyy James! You caught me in a good mood, tonight! I’ve got a feeling it's going to be a special one.");
	_dialog("[c=#39f979]Mireille:[/c] Johnny’s letting me drink to my heart's content for once! You never know: I might actually get drunk for once!");
	_jump("Menu");

_label("Morning");
_label("Afternoon");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_desire_club);
	_change("BG",spr_bg_desireClub,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_choice(spr_choice,"Back to the entrance",noone,room_scene_desire_club_entrance,fa_center,fa_center);
	_dialog("[c=#6678ff](Desire Club is closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","mireille",FADE_NORMAL);
	if !scene_sprite_object_exists("mireille")
		_show("mireille",spr_char_Mireille_Neutral,1.8,YNUM_MIREILLE,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	else
		_void();
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_choice(spr_choice_s,"Order a drink","Order a drink Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Buy a Drink for…","Pay a drink to... Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Look around","Look around Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Exit","Exit",noone,fa_right,fa_center);
	_block();


_border(100); // add content before this

_label("Order a drink Intro");
	_dialog("[c=#6678ff]James:[/c] I’ll take a…");
	_choice(spr_choice_s,"Beer (Cost: $5)","Order a drink Beer",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Bloody Mary (Cost: $16)","Order a drink Bloody Mary",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Sex on the Beach (Cost: $18)","Order a drink Sex on the Beach",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Margarita (Cost: $20)","Order a drink Margarita",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Cancel","Menu",noone,fa_right,fa_center);
	_block();

_label("Order a drink Beer");
	if var_get(VAR_CASH) >= 5
		_jump("Order a drink Beer Confirm");
	else
		_jump("Order a drink Fail");
	
_label("Order a drink Bloody Mary");
	if var_get(VAR_CASH) >= 16
		_jump("Order a drink Bloody Mary Confirm");
	else
		_jump("Order a drink Fail");
		
_label("Order a drink Sex on the Beach");
	if var_get(VAR_CASH) >= 18
		_jump("Order a drink Sex on the Beach Confirm");
	else
		_jump("Order a drink Fail");
		
_label("Order a drink Margarita");
	if var_get(VAR_CASH) >= 20
		_jump("Order a drink Margarita Confirm");
	else
		_jump("Order a drink Fail");
		

_label("Order a drink Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money.[/c]");



_label("Order a drink Beer Confirm");
	_var_add(VAR_CASH,-5);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("mireille",spr_char_Mireille_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Beer? C’mon if you want the attention of the ladies here, you got buy something to show that you’ve got money.");
	_dialog("[c=#6678ff]James:[/c] But I’m a light drinker.");
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Then buy a lot of beers.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](*Sigh* This is scam!)[/c]");
	_jump("Menu");

_label("Order a drink Bloody Mary Confirm");
	_var_add(VAR_CASH,-16);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("mireille",spr_char_Mireille_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] You men are such gullible fools… look at all these girls! Attractive huh? But without their sexy clothes and makeup, they are just like any average woman you’d see on the streets.");
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] If a woman wants make an impression she has to get all dressed up for your amusement. It’s silly. Yet you fork over handfuls for a sight of skin.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] I... guess I’ll try to keep that in mind.");
	_jump("Menu");

_label("Order a drink Sex on the Beach Confirm");
	_var_add(VAR_CASH,-18);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("mireille",spr_char_Mireille_Tired,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Please don’t make the “Sex on the bitch” pun when you’re surrounded by bitches.");
	_dialog("[c=#6678ff]James:[/c] In such an inspiring place, I bet it’s a bit overdone, right?");
	_change("mireille",spr_char_Mireille_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] You’re damn right it is!");
	_jump("Menu");
	
_label("Order a drink Margarita Confirm");
	_var_add(VAR_CASH,-20);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] If you want to get on one of the ladies’ good side, you should buy presents for them. The right present will show how much you know her tastes, and that you’re genuinely interested in her.");
	_dialog("[c=#6678ff]James:[/c] Where would be the best place to buy presents for them?");
	_dialog("[c=#39f979]Mireille:[/c] You will find something interesting everywhere. It just depends on the lady.");
	_dialog("[c=#39f979]Mireille:[/c] Try the clothes store, and the gift shop. It’s a good place to start, at least.");
	_dialog("[c=#6678ff]James:[/c] Thanks for the tip, Mireille.");
	_var_set(VAR_MIREILLE_SUGGESTION_CLOTHES,true);
	// (indicate these locations on the map when we make them available)
	_jump("Menu");

_border(100); // add content before this

_label("Pay a drink to... Intro");
	_choice(spr_choice_s,"Chris ($18)","Pay a drink to Chris",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Mireille ($20)","Pay a drink to Mireille",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Cancel","Menu",noone,fa_right,fa_center);
	_block();
	
_label("Pay a drink to Chris");
	if var_get(VAR_CASH) >= 18
		_jump("Pay a drink to Chris Confirm");
	else
		_jump("Order a drink Fail");
	
_label("Pay a drink to Chris Confirm");
	_var_add(VAR_CASH,-18);
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] “Sex on the Beach” brings out the bitch inside you!");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Ouch! That was a bad one James…");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff](Oh gosh! Now I’m making that silly pun too… This place is contagious.)[/c]");
	_jump("Menu");
	
_label("Pay a drink to Mireille");
	if var_get(VAR_CASH) >= 20
		_jump("Pay a drink to Mireille Confirm");
	else
		_jump("Order a drink Fail");	
	
_label("Pay a drink to Mireille Confirm");
	_var_add(VAR_CASH,-20);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Are you trying to get me drunk in order to seduce me?");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff](She chugs a whole Margarita as if it was water.)[/c]");
	_dialog("[c=#39f979]Mireille:[/c] Pfft, nice try!");
	_jump("Menu");

_border(100); // add content before this

_label("Look around Intro");
	_jump("Look around Normal");
	
_label("Look around Normal");
	_dialog("[c=#6678ff](Chris is doing her show, the dance classes really did a lot of good for her. She’s pretty damn good.)[/c]");
	_jump("Menu");
		
_border(100); // add content before this

_label("Exit");
	_choice(spr_choice_s,"Yes",noone,room_scene_desire_club_entrance,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the entrance?[/c]",false);
	_block();

_border(100); // add content before this

_label("STRIP_CLUB_FIRST_VISIT");
	_hide_all(FADE_NORMAL);
	_main_gui(false);
	_pause(1,0,true);
	_choice(spr_choice,"Continue","STRIP_CLUB_FIRST_VISIT_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]James is about to visit a world that he has only seen in movies…[/c]",false);
	_block();

_label("STRIP_CLUB_FIRST_VISIT_START");
	_placename(PLACENAME_desire_club);
	_main_gui(true);
	_show("BG",spr_bg_desireClub,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("johnny",spr_char_Johnny_Horny,1.8,YNUM_JOHNNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#39f979]Johnny:[/c] Step into my humble love shack, you two! This is where the magic happens!");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oh, wow!");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Wow indeed.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](As we enter we can feel the sudden shift in atmosphere. The lighting is much darker than at the Lotus Dew. The mood is subdued and sexual.)[/c]");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](There’s girls everywhere, with body types ranging from shortstacks like Penny to outright amazons. They’re all scantily clad and seem at home in the murky interior.)[/c]");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](As Johnny leads us to the bar, I spot a few shady looking dudes huddling in a booth giving me the stink eye. One’s got one of the girls in his lap and is slowly stroking at something of hers under the table.)[/c]");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Watcha’ looking at, James?");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] W-what? Nothing!");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] No need to get so defensive, silly!");
	_dialog("[c=#39f979]Johnny:[/c] Come here, you two! I want to introduce you to our lovely bartender!");
	_dialog("[c=#39f979]Johnny:[/c] -And might I say, you are looking sexy as hell tonight, Mirelle!");
	
	_show("mireille",spr_char_Mireille_Tired,0.8,YNUM_MIREILLE,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_pause(1,0,true);
	_if(var_get(VAR_AFF_MIREILLE)<1,_var_set,VAR_AFF_MIREILLE,1);
	_dialog("[c=#39f979]Mireille:[/c] Aw save it boss, my pants aren’t a treasure chest for you to break into.");
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] ...Who are these two?");
	_change("johnny",spr_char_Johnny_Smug,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] Allow me to introduce you to Chris and James! Chris is going to be our newest addition to the team.");
	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Pleased to meet you!");
	_change("johnny",spr_char_Johnny_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Johnny:[/c] You ever need any help settling in, Mireille is the girl to ask! I’ll be back, gotta talk to some of the ladies.");
	_hide("johnny",FADE_NORMAL);
	_move("mireille",1.8,YNUM_MIREILLE,SLIDE_FAST);
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Heh, sorry for the rough greeting. Johnny can get a bit greedy with his eyes.");
	_change("mireille",spr_char_Mireille_Tired,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Although, maybe I wouldn’t mind so much if your eyes got just a little bit greedy.");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Even as she’s saying this she’s leaning over the top of the bar with her boobs spilling out! Her smile is downright predatory...");
	
	_choice(spr_choice,"“You just made my eyes greedier!”","STRIP_CLUB_FIRST_VISIT_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“So- ah, why is it so expensive here?”","STRIP_CLUB_FIRST_VISIT_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("STRIP_CLUB_FIRST_VISIT_CHOICE_1_1");
	_var_add_ext(VAR_AFF_MIREILLE,1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,-1,1,4);
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Oh my!");
	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] James behave!");
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] C’mon you saw what she just did! You could learn something from her…");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](In order to misdirect Chris anger, I quickly change the subject.)[/c]");
	_dialog("[c=#6678ff]James:[/c] So- ah, why is it so expensive here?");
	_jump("STRIP_CLUB_FIRST_VISIT_after_CHOICE_1");
		
_label("STRIP_CLUB_FIRST_VISIT_CHOICE_1_2");
	_var_add_ext(VAR_AFF_MIREILLE,-1,1,4);
	_change("mireille",spr_char_Mireille_Angry,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Young guys ducking me already… I’m really getting past my prime… ah the drinks.");
	_jump("STRIP_CLUB_FIRST_VISIT_after_CHOICE_1");

_label("STRIP_CLUB_FIRST_VISIT_after_CHOICE_1");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Yeah, these drinks cost a fortune compared to our regular bar!");
	_change("mireille",spr_char_Mireille_Neutral,FADE_NORMAL);
	_dialog("[c=#39f979]Mireille:[/c] Haha! Why that’s easy: we’re trying to appeal to more wealthy clientele");
	_dialog("[c=#39f979]Mireille:[/c] Here in Desire, you pay for the drinks… but the view is free. Hehe!");
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff](She winks coquettishly at us. Both Chris and I clam up.)[/c]");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I can tell!");
	_dialog("[c=#6678ff]James:[/c] Holy moly.");
	_var_set(VAR_EVENT_STRIP_CLUB_FIRST_VISIT,true);
	if current_time_get_name() == "Evening" || current_time_get_name() == "Late Night"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this

