{
    "id": "469e926b-2841-4687-8e40-b535afc598d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particle_smoke",
    "eventList": [
        {
            "id": "dbe8f850-2463-4af3-abd6-7014a79e2e9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "469e926b-2841-4687-8e40-b535afc598d3"
        },
        {
            "id": "51849ea7-be7a-470e-9264-4f79ffe673a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "469e926b-2841-4687-8e40-b535afc598d3"
        },
        {
            "id": "6c72b45b-c890-4496-a87e-63db4fa6c12a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "469e926b-2841-4687-8e40-b535afc598d3"
        },
        {
            "id": "99188d88-536f-421b-8022-62588f91bba6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "469e926b-2841-4687-8e40-b535afc598d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}