event_inherited();

_start();
	_placename(PLACENAME_apt);
	_main_gui(false);
	_show("BG",spr_bg_aptLoungeClean,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_DEADLINE)
		_jump("EVENT_DEADLINE");
	else
		_jump("Normal");
		

_border(100); // add content before this

_label("EVENT_DEADLINE");
	_var_set(VAR_EVENT_DEADLINE,true);
	_choice(spr_choice,"Continue","EVENT_DEADLINE_CHOICE_1",noone,fa_center,fa_center); 
	_dialog("[c=#FFFF00]DEADLINE HAS EXPIRED AND CHRIS CAN'T PAY HIS DEBIT.[/c]",false);
	_block();
	
_label("EVENT_DEADLINE_CHOICE_1");
	_show("dim",spr_dim,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("deadline",spr_deadline_01,10,4,1,DEPTH_BG_ELE-1,false,FADE_NORMAL);
	_move("deadline",5,4,5);
	_dialog("[c=#6678ff]James:[/c] I know the deadline expired, but broke my heart see you working there, I’ll give you 5 more days.");
	_var_add(VAR_DAYSREMAINING,4);
	_pause(1,0,true);
	_room_goto_ext(room_scene_apt,true);
	_block();
	
_border(100); // add content before this

_label("Normal");
	_show("dim",spr_dim,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("deadline",spr_deadline_02,10,5,1,DEPTH_BG_ELE-1,false,FADE_NORMAL);
	_move("deadline",5,5,5);
	_choice(spr_choice_lightred,"Return to the title screen",noone,room_title_screen,fa_center,fa_bottom); 
	_block();


_border(100); // add content before this





