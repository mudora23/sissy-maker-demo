{
    "id": "e1342092-5fe4-4ddb-ae6d-a6ce6c5e8cff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_main_map",
    "eventList": [
        {
            "id": "413cdcda-4ccf-4649-8c20-b4cadef1e80c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1342092-5fe4-4ddb-ae6d-a6ce6c5e8cff"
        },
        {
            "id": "366bcf31-5997-4b86-9fdc-d8510824a60a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e1342092-5fe4-4ddb-ae6d-a6ce6c5e8cff"
        },
        {
            "id": "3807a3c4-614d-451e-947d-0cc70d607f8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e1342092-5fe4-4ddb-ae6d-a6ce6c5e8cff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}