event_inherited();

_start();
	_placename(PLACENAME_mainmap);
	_show("BG",spr_bg_map,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("BG_front",spr_bg_map_front,5,5,1,DEPTH_BG-1,false,FADE_NORMAL);
	
	_show("BG_Apt",spr_bg_map_Apt,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_Hospital",spr_bg_map_Hospital,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_LotusDew",spr_bg_map_LotusDew,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_Park",spr_bg_map_Park,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	//if var_get(VAR_EVENT_STRIP_CLUB_PT1)
	//	_show("BG_StripBlub",spr_bg_map_Desire_Club,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	//else
		_show("BG_GirlsOnlyClub",spr_bg_map_GirlsOnlyClub,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	_show("BG_STPHeadquarters",spr_bg_map_STPHeadquarters,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	if jobs_list_job_exists(JOBNAME_Baby_Sitting)
		_show("BG_Kanes",spr_bg_map_Kanes_Residence,5,5,1,DEPTH_BG_ELE,false,FADE_NORMAL);
	else
		_void();
_jump("choice_main_map");		

_border(100); // add content before this

_label("choice_main_map");
	_pause(0.5,0,true);
	_shadow(spr_bg_map_shadow_Apt,"going_Apt",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_Hospital,"going_Hospital",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_LotusDew,"going_LotusDew",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_Park,"going_Park",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	//if var_get(VAR_EVENT_STRIP_CLUB_PT1)
	//	_shadow(spr_bg_map_shadow_GirlsOnlyClub,"going_DesireClub",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	//else if !var_get(VAR_EVENT_STRIP_CLUB_PT1)
		_shadow(spr_bg_map_shadow_GirlsOnlyClub,"going_GirlsOnlyClub",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_map_shadow_STPHeadquarters,"going_STPHeadquarter",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	if jobs_list_job_exists(JOBNAME_Baby_Sitting)
		_shadow(spr_bg_map_shadow_Kanes_Residence,"going_Kanes_Residence",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	else
		_void();

	_block();

_border(100); // add content before this

_label("going_Apt");
	_choice(spr_choice,"Yes","going_Apt processing",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Back to the apartment?[/c]",false);
	_block();

_label("going_Apt processing");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_apt,true);
	_block();

_border(100); // add content before this

_label("going_Hospital");
	_dialog("[c=#ff00ff]Chris:[/c] This is the hospital. I hope we don’t need to go there.");
	_jump("choice_main_map");

_border(100); // add content before this

_label("going_LotusDew");
	_dialog("[c=#ff00ff]Chris:[/c] Lotus Dew, my favorite hangout.");
	if time_get_name(var_get(VAR_TIME)+2) == "Late Night"
		_jump("going_LotusDew Confirm");
	else
		_jump("going_LotusDew Fail");
		
_label("going_LotusDew Confirm");	
	_choice(spr_choice,"Yes","going_LotusDew processing",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to Lotus Dew?[/c]",false);
	_block();
	
_label("going_LotusDew processing");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_lotus_dew,true);
	_block();
	
	
_label("going_LotusDew Fail");
	_dialog("[c=#ff00ff]Chris:[/c] It only opens in the late night.");
	_jump("choice_main_map");


_border(100); // add content before this

_label("going_Park");
_label("going_GirlsOnlyClub");
	_dialog("[c=#ff00ff]Chris:[/c] This location is locked in the demo. Please consider supporting us on patreon :)");
	_dialog("[c=#6678ff]James:[/c] Alright...");
	_jump("choice_main_map");

_border(100); // add content before this

_label("going_STPHeadquarter");
	_dialog("[c=#ff00ff]Chris:[/c] This is STP building.");
	_choice(spr_choice,"Yes","going_STPHeadquarter processing",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to the STP building?[/c]",false);
	_block();
	
_label("going_STPHeadquarter processing");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_stp_hq,true);
	_block();

_border(100); // add content before this

_label("going_Kanes_Residence");
	_dialog("[c=#ff00ff]Chris:[/c] This is where I’m going to work.");
	_dialog("[c=#ff00ff]Chris:[/c] I hope I can tame those brats.");
	_dialog("[c=#ff00ff]Chris:[/c] I have a babysitting job there at 12:00.");
	if jobs_list_have_job_2hours_later(var_get(VAR_DAY),var_get(VAR_DAY),var_get(VAR_TIME),var_get(VAR_TIME),JOBNAME_Baby_Sitting)
		_jump("going_Kanes_Residence Confirm");
	else
		_jump("choice_main_map");

_label("going_Kanes_Residence Confirm");
	_choice(spr_choice,"Yes","going_Kanes_Residence processing",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to Kane’s residence?[/c]",false);
	_block();

_label("going_Kanes_Residence processing");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_kanes_residence,true);
	_block();

_border(2); // add content before this

_label("going_DesireClub");	
	//if time_get_name(var_get(VAR_TIME)+2) == "Evening" || time_get_name(var_get(VAR_TIME)+2) == "Late Night"
	//	_jump("going_DesireClub Confirm");
	//else
		_jump("going_DesireClub Fail");
		
//_label("going_DesireClub Confirm");
//	_choice(spr_choice,"Yes","going_DesireClub processing",noone,fa_center,fa_center);
//	_choice(spr_choice,"No","choice_main_map",noone,fa_center,fa_center);
//	_dialog("[c=#FFFF00]Going to Desire Club?[/c]",false);
//	_block();
	
//_label("going_DesireClub processing");
//	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
//	_execute(time_hour_passed,2);
//	_room_goto_ext(room_scene_desire_club_entrance,true);
//	_block();

_label("going_DesireClub Fail");
	_dialog("[c=#ff00ff]Chris:[/c] It only opens in the evening or late night.");
	_jump("choice_main_map");

_border(98); // add content before this