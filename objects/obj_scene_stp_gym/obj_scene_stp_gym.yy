{
    "id": "557725cc-4636-4aad-be07-05ccdda32a51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_gym",
    "eventList": [
        {
            "id": "55475c2d-f4d6-4007-9076-5d6dfaab0f08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "557725cc-4636-4aad-be07-05ccdda32a51"
        },
        {
            "id": "f43c571a-cbd3-4a46-9f29-1ae600a865a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "557725cc-4636-4aad-be07-05ccdda32a51"
        },
        {
            "id": "b3387e16-2e9b-47ab-b400-fe9d7194485a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "557725cc-4636-4aad-be07-05ccdda32a51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}