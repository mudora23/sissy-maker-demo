event_inherited();

_start();
	_placename(PLACENAME_STPgym);
	_main_gui(true);
	_show("BG",spr_bg_STPgym,5,5,1,DEPTH_BG,false,FADE_NORMAL);

	if !var_get(VAR_EVENT_STP_GYM_VISIT) && 
	   (current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening")
		_jump("EVENT_STP_GYM_VISIT");
	else if !var_get(VAR_EVENT_RONDA_RUTHLESS) && 
	        var_get(VAR_EVENT_FIRST_MISSION) && 
			(var_get(VAR_STRENGTH) + var_get(VAR_STAMINA) >= 3) && 
			(current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening") && 
			(success_roll_attribute(VAR_STRENGTH)+success_roll_attribute(VAR_STAMINA)+success_roll(VAR_AFF_RONDA))
		_jump("EVENT_RONDA_RUTHLESS");
	//else if var_get(VAR_EVENT_RONDA_RUTHLESS) && 
	//		(var_get(VAR_STRENGTH) + var_get(VAR_STAMINA) >= 3) && 
	//		(current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening") && 
	//		(success_roll_hard(VAR_STRENGTH)+success_roll_hard(VAR_STAMINA)+success_roll_hard(VAR_AFF_RONDA))
	//	_jump("EVENT_RONDA_RUTHLESS");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Stay");
	_stay(true,true,true,false);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("StayRestActivitiesEnd");
	_block();

_label("StayRestHide");
	_stay_rest_hide("StayRestActivitiesEnd");
	_block();
	
_label("StayRestActivitiesEnd");
	if current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening"
		_jump("Menu");
	else
		_jump("Closed");

_border(100); // add content before this

_label("Morning");
	_show("ronda",spr_char_Ronda_Happy,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Ronda:[/c] Hi there! Nice to see you got the motivation.  Use that to sculpt yourself into a lean mean machine! Time to work off that bubble butt!");
	_jump("Menu");

_border(100); // add content before this

_label("Afternoon");
	_show("ronda",spr_char_Ronda_Happy,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Ronda:[/c] Put that stagnating energy to good use!  Clock’s ticking ladies!  Time to turn that afternoon gut into a burning workout!");
	_jump("Menu");

_border(100); // add content before this

_label("Evening");
	_show("ronda",spr_char_Ronda_Happy,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_dialog("[c=#ff3333]Ronda:[/c] Here to burn off dinner?  Good plan.   Go out hard and fast like a star with my killer workouts!");
	_jump("Menu");

_border(100); // add content before this

_label("Late Night");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_play_sfx(sfx_door_opening);
	
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_dialog("[c=#6678ff](The Gym is closed.)[/c]",false);
	_block();

_border(100); // add content before this

_label("Menu");
	_hide_all_except("BG","ronda",FADE_NORMAL);
	_choice(spr_choice_s,"Cardio Workout\r(Cost: $30, Energy Cost: 30)","Cardio Workout Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Strength Training\r(Cost: $30, Energy Cost: 30)","Strength Training Intro",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Cardio Workout Intro");
	_change("ronda",spr_char_Ronda_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] The Cardio Workout includes resistance and aerobics training. It helps with weight loss and builds stamina and energy for your daily activities.");
	if var_get(VAR_ENERGY) >= 30 && var_get(VAR_CASH) >= 30
		_jump("Cardio Workout Confirm");
	else
		_jump("Cardio Workout Fail");

_label("Cardio Workout Confirm");
	_choice(spr_choice_s,"Yes","Cardio Workout Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Begin Cardio Workout? (Cost: $30, Energy Cost: 30)[/c]",false);
	_block();
	
_label("Cardio Workout Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Cardio Workout Confirm Yes");
	_activities(spr_activities_working_out,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-30);
	_var_add(VAR_ENERGY,-30);
	_var_add(VAR_STAMINA,1);
	_var_add(VAR_TRAINING_RONDA,1);
	_block();

_border(100); // add content before this


_label("Strength Training Intro");
	_change("ronda",spr_char_Ronda_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Strength Training builds physical strength, body mass, and gets you in good shape. Don’t worry about getting overboard, my training program is made for female fitness, you won’t end up like Lou Ferrigno.");
	if var_get(VAR_ENERGY) >= 30 && var_get(VAR_CASH) >= 30
		_jump("Strength Training Confirm");
	else
		_jump("Strength Training Fail");

_label("Strength Training Confirm");
	_choice(spr_choice_s,"Yes","Strength Training Confirm Yes",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Begin Strength Training? (Cost: $30, Energy Cost: 30)[/c]",false);
	_block();
	
_label("Strength Training Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FF8080]Not enough money or energy.[/c]");
	_jump("Menu");

_label("Strength Training Confirm Yes");
	_activities(spr_activities_working_out,"StayRestActivitiesEnd");
	_var_add(VAR_CASH,-30);
	_var_add(VAR_ENERGY,-30);
	_var_add(VAR_STRENGTH,1);
	_var_add(VAR_TRAINING_RONDA,1);
	_block();

_border(100); // add content before this

_label("Talk");
	//if !var_get(VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER) && var_get(VAR_EVENT_DANCER_AT_STP) && current_time_get_name() == "Evening"
	//	_jump("EVENT_RONDA_TALKS_ABOUT_THE_DANCER");
	//else
		_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("ronda",spr_char_Ronda_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Check out these guns.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Ronda flexes her arms-- she seems very proud of them.)[/c]");
	
	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Impressive! …Chris?");
	
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] …");
	
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris is awestruck, I can even see drool coming out her open mouth.)[/c]");

	_jump("Menu");


_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main hall?[/c]",false);
	_block();

_border(100); // add content before this

_label("EVENT_STP_GYM_VISIT");
	_var_set(VAR_EVENT_STP_GYM_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,0.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] STP Infrastructure counts with a very equipped gymnasium, and professional coaches that will assist your training.");
	
	_if(var_get(VAR_AFF_RONDA)<1,_var_set,VAR_AFF_RONDA,1);
	_show("ronda",spr_char_Ronda_Smug,1.8,YNUM_RONDA,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Hey penny what’s up?");
	_dialog("[c=#ff3333]Penny:[/c] We got newcomers… I mean a newcomer.");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](… What was that?)[/c]");
	
	_change("ronda",spr_char_Ronda_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Oh my! Look at you, you’re so pretty! I’m Ronda Ruthless, nice to meet you. What’s you name sweetheart?");
	_dialog("[c=#ff00ff]Chris:[/c] Chris Ashee.");
	_dialog("[c=#ff3333]Ronda:[/c] We’re going to have a good time working out that bubble butt of yours, Chris.");
	
	_change("ronda",spr_char_Ronda_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] This lazy ass by your side is your boyfriend? He could use some exercises too.");
	_dialog("[c=#ff00ff]Chris:[/c] No, no, his just a friend!");
	_dialog("[c=#6678ff]James:[/c] James Lakin.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_choice(spr_choice,"“Yes I’d love to start some sport activity what you recommend?”","EVENT_STP_GYM_VISIT_CHOICE_1_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"“I used to run around my block every morning, but it’s hard to keep the habit by my own.”","EVENT_STP_GYM_VISIT_CHOICE_1_2",noone,fa_center,fa_center,true);
	_block();
	
_label("EVENT_STP_GYM_VISIT_CHOICE_1_1");
	_var_add_ext(VAR_AFF_RONDA,-1,1,4);
	_jump("EVENT_STP_GYM_VISIT_after_CHOICE_1");
	
_label("EVENT_STP_GYM_VISIT_CHOICE_1_2");
	_var_add_ext(VAR_AFF_RONDA,1,1,4);
	_jump("EVENT_STP_GYM_VISIT_after_CHOICE_1");
	
_label("EVENT_STP_GYM_VISIT_after_CHOICE_1");
	_dialog("[c=#ff3333]Ronda:[/c] Come here then, and I’ll break both Chris’s and your butt. It will be fun!");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Go for it James.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Penny catches my attention while Rhonda sizes up Chris.  She holds out her hands like she’s measuring something long…)[/c]");
	
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff](It hits me all at once.  It was a warning.)[/c]");
	_jump("Menu");


_border(100); // add content before this


_label("EVENT_RONDA_RUTHLESS");
	_show("ronda",spr_char_Ronda_Happy,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_choice(spr_choice_s,"Yes","EVENT_RONDA_RUTHLESS_CHOICE_1_1",noone,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Ronda is inviting you to do some leg press. Accept invitation?[/c]",false);
	_block();

_label("EVENT_RONDA_RUTHLESS_CHOICE_1_1");
	_void();//_var_set(VAR_EVENT_RONDA_RUTHLESS,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("ronda",spr_char_Ronda_Happy,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Hey Chris? Want help with your workout?");
	_dialog("[c=#ff00ff]Chris:[/c] Of course. What are we going to do?");
	
	_change("ronda",spr_char_Ronda_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] I heard you’re undergoing Penny Hardth’s test. So a 200lb leg press, huh?  Those skinny legs ain’t gonna cut it.");
	_dialog("[c=#ff3333]Ronda:[/c] Lucky for you, I know a secret method to work those leg muscle faster.  You’ll be pushing 200lbs in no time.");
	_dialog("[c=#ff3333]Ronda:[/c] Pay close attention, James. You will help Chris at home with this one.");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_show("DIM",spr_dim,5,5,1,DEPTH_BG-1,false,FADE_SLOW);
	_show("CG",spr_hentai_cg_leg_press_1,5,5,1,DEPTH_BG_ELE,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](They certainly look like they’re having fun, but is this really necessary?  Ronda is probably doing this on purpose.)[/c]");
	_dialog("[c=#6678ff](This is so disrespectful… Or am I just… jealous?)[/c]");
	_dialog("[c=#6678ff](I can’t take my eyes off them. Pushing their bodies together in a perfect missionary position like that...)[/c]");
	_dialog("[c=#6678ff](What am I doing in this place?!)[/c]");
	_dialog("[c=#ff3333]Ronda:[/c] Yes like this! Are you feeling the tension?");
	_dialog("[c=#ff00ff]Chris:[/c] Yes, it’s getting hard!");

	_choice(spr_choice,"\"Are you two done yet?  It’s embarrassing…\"","EVENT_RONDA_RUTHLESS_CHOICE_2_1",noone,fa_center,fa_center,true);
	_choice(spr_choice,"If you want to take of your clothes and keep going, don’t mind me.","EVENT_RONDA_RUTHLESS_CHOICE_2_2",noone,fa_center,fa_center,true);
	_block();

_label("EVENT_RONDA_RUTHLESS_CHOICE_2_1");
	_var_add_ext(VAR_AFF_RONDA,-1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_dialog("[c=#ff3333]Ronda:[/c] James, never stop a series of exercises in the middle.  Sweat until you drop! No pain no gain. Don’t you think Chris?");
	_dialog("[c=#ff00ff]Chris:[/c] Hmpf… Yes Ronda. So what you have to do, I guess.");
	_jump("EVENT_RONDA_RUTHLESS_after_CHOICE_2");
	
_label("EVENT_RONDA_RUTHLESS_CHOICE_2_2");
	_var_add_ext(VAR_AFF_RONDA,1,1,4);
	_var_add_ext(VAR_AFF_CHRIS,1,1,4);
	_dialog("[c=#ff3333]Ronda:[/c] Looks like James is having fun too. I bet he can’t wait to try this at home!");
	_jump("EVENT_RONDA_RUTHLESS_after_CHOICE_2");
	
_label("EVENT_RONDA_RUTHLESS_after_CHOICE_2");
	_change("CG",spr_hentai_cg_leg_press_2,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#ff3333]Ronda:[/c] Yup the harder the better for your butt muscles. Don’t you think? C’mon, ten times.");
	_dialog("[c=#ff00ff]Chris:[/c] Yes ma’am… hrrmm!");
	_dialog("[c=#ff3333]Ronda:[/c] Look James, it’s important to use your own weight and push all the way down. Chris has to feel you close to her body. The harder you make it the better are the results from this exercise.");
	_dialog("[c=#ff3333]Ronda:[/c] Keep your balance… yes! Let it down all the way hmmm very good! Now up! Alright! Let it down slowly Ahhhh!");
	_dialog("[c=#ff3333]Ronda:[/c] Don’t be shy. It won’t hurt you… A little closer, yeah… oh yeah!");
	_dialog("[c=#ff3333]Ronda:[/c] Okay that’s it for today.");

	_change("CG",spr_hentai_cg_after_gym_bg,FADE_SLOW);
	_show("CG2",spr_hentai_cg_after_gym_bg2,5,5,1,DEPTH_BG_ELE-2,false,FADE_SLOW);
	_show("CG3",spr_hentai_cg_after_gym_bg3,5,5,1,DEPTH_BG_ELE-1,false,FADE_SLOW);
	_pause(0.5,0,false);
	_dialog("[c=#6678ff](Sheesh, look at the cum stains on those yoga pants.)[/c]");
	_dialog("[c=#ff3333]Ronda:[/c] I’m sure you’re able to leg press 200lb now.");
	_dialog("[c=#ff00ff]Chris:[/c] Thank you very much Ronda, I’d be surprised if I could even walk tomorrow!");
	
	_main_gui(true);
	_hide_all_except("BG",FADE_NORMAL);
	_pause(0.5,0,true);
	_show("ronda",spr_char_Ronda_Happy,1.8,YNUM_RONDA,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_execute(time_hour_passed,2);
	_var_set(VAR_EVENT_RONDA_RUTHLESS,true);
	if current_time_get_name() == "Morning" || current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening"
		_jump("Menu");
	else
		_jump("Closed");

_border(2); // add content before this

/*_label("EVENT_RONDA_TALKS_ABOUT_THE_DANCER");
	_hide_all_except("BG",FADE_NORMAL);
	_choice(spr_choice,"Continue","EVENT_RONDA_TALKS_ABOUT_THE_DANCER_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Ronda talks about the new dance coach.[/c]",false);
	_block();

_label("EVENT_RONDA_TALKS_ABOUT_THE_DANCER_START");
	_var_set(VAR_EVENT_RONDA_TALKS_ABOUT_THE_DANCER,true);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER+1,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("ronda",spr_char_Ronda_Worry,1.8,YNUM_RONDA,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Ronda:[/c] Reye thinks she’s hot shit, huh?  A greenhorn like her at STP, should be charging so much for classes!");
	_dialog("[c=#ff3333]Ronda:[/c] She lacks a didactic approach, doesn’t work till noon… ARGH!  What makes her think she thinks she’s so special, huh?");
	_dialog("[c=#ff3333]Ronda:[/c] Her dancing classes doesn’t build strength nor stamina. USELESS!");
	_change("chris",spr_char_Chris_Horny,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Someone’s got a cru~ush!");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Sounds like it…");
	_dialog("[c=#ff3333]Ronda:[/c] She pisses me off!");
	_jump("Menu");
*/
_border(98); // add content before this