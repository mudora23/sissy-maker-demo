{
    "id": "dc295475-e7d1-4c33-aee6-6a5448d81aaa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_pennys_office",
    "eventList": [
        {
            "id": "84b0355a-b522-469f-b6c9-e7f8ac630668",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc295475-e7d1-4c33-aee6-6a5448d81aaa"
        },
        {
            "id": "163f4d10-1543-45a3-9198-44edf4879670",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "dc295475-e7d1-4c33-aee6-6a5448d81aaa"
        },
        {
            "id": "416b2477-2b02-4858-9abc-e17a9203ad36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dc295475-e7d1-4c33-aee6-6a5448d81aaa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}