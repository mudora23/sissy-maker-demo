event_inherited();

_start();
	_placename(PLACENAME_STPpennyOffice);
	_main_gui(true);
	_show("BG",spr_bg_STPpennyOffice,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	
	if !var_get(VAR_EVENT_STP_PRINCIPAL_VISIT) && 
	   (current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening")
		_jump("EVENT_STP_PRINCIPAL_VISIT");
	else if !var_get(VAR_EVENT_FIRST_MISSION) && 
			var_get(VAR_TRAINING_SIMONE) > 0 && 
			var_get(VAR_TRAINING_RONDA) > 0 && 
			var_get(VAR_EVENT_STP_LAB_VISIT) && 
			(current_time_get_name() == "Afternoon" || current_time_get_name() == "Evening") && 
			(success_roll_attribute(VAR_INTELLIGENCE)+success_roll_attribute(VAR_STRENGTH))
		_jump("EVENT_FIRST_MISSION");
	else if current_time_get_name() == "Morning"
		_jump("Closed");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Closed");
		

_border(100); // add content before this


_label("Afternoon");
_label("Evening");
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_pause(1,0,true);
	_jump("Menu");

_border(100); // add content before this

_label("Morning");
_label("Late Night");
_label("Closed");
	_hide_all_except("BG",FADE_NORMAL);
	_placename(PLACENAME_STPmainhall)
	_change("BG",spr_bg_STPmainHall,FADE_NORMAL);
	_pause(0.5,0,true);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] There is a sign on the door.");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] “Busy, don’t disturb!”");
	_dialog("[c=#6678ff]James:[/c] We better leave or you know what’s going to happen to our heads…");
	_dialog("[c=#ff00ff]Chris:[/c] *Gulp*");
	_choice(spr_choice,"Back to the Main Hall",noone,room_scene_stp_main_hall,fa_center,fa_center);
	_block();

_border(100); // add content before this

_label("Menu");
	_main_gui(true);
	_hide_all_except("BG","penny",FADE_NORMAL);
	_choice(spr_choice_s,"Job - Babysitting","Job - Babysitting Checking",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Talk","Talk",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Stay","Stay",noone,fa_right,fa_center);
	_choice(spr_choice_s,"Move to the Next Location","Move",noone,fa_right,fa_center);
	_block();

_border(100); // add content before this

_label("Job - Babysitting Checking");
	if skills_xp_get_level(var_get(VAR_SKILL_BABYSITTING)) < 1
		_jump("Job - Babysitting Fail");
	else
		_jump("Job - Babysitting Intro");
		
_label("Job - Babysitting Fail");
	_play_sfx(sfx_no_skills);
	_dialog("[c=#FFFF00]You don’t have a babysitting license yet, you need to take some babysitting classes first.[/c]");
	_jump("Menu");
		
		
_label("Job - Babysitting Intro");
	_dialog("[c=#ff3333]Penny:[/c] Once you’re assigned to a job, commit yourself to the task, be punctual and trustworthy.");
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] STP is a serious institution and we don’t tolerate slackers.");
	_dialog("[c=#ff3333]Penny:[/c] If for any reason you fail to accomplish the task, you’ll receive a penalty or even loose your professional license.");
	if jobs_list_job_exists(JOBNAME_Baby_Sitting)
		_jump("Job - Babysitting Exists");
	else
		_jump("Job - Babysitting Assigning");
	
	
_label("Job - Babysitting Exists");
	_dialog("[c=#FFFF00]You have assigned for this job already. Check Items menu for the assignment details.[/c]");
	_jump("Menu");
	
_label("Job - Babysitting Assigning");
	_main_gui(false);
	_show("JOB_BG",spr_job_babysitting_bg,5,5,1,DEPTH_CHARACTER-3,false,FADE_NORMAL);
	_show("JOB_DES",spr_job_babysitting,10,5,1,DEPTH_CHARACTER-4,false,FADE_NORMAL);
	_move("JOB_DES",5,5,3);
	_pause(0.5,0,true);
	_choice(spr_choice_lightgreen,"Assign","Job - Babysitting Assigning Yes",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","Menu",noone,fa_center,fa_bottom);
	_block();
	
_label("Job - Babysitting Assigning Yes");
	_show("JOB_MAP",spr_bg_map,5,5,1,DEPTH_CHARACTER-5,false,FADE_FAST);
	_show("JOB_MAP_Apt",spr_bg_map_Apt,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_GirlsOnlyClub",spr_bg_map_GirlsOnlyClub,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_Hospital",spr_bg_map_Hospital,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_LotusDew",spr_bg_map_LotusDew,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_STPHeadquarters",spr_bg_map_STPHeadquarters,5,5,1,DEPTH_CHARACTER-6,false,FADE_FAST);
	_show("JOB_MAP_Park",spr_bg_map_Park,5,5,1,DEPTH_CHARACTER-7,false,FADE_FAST);
	_show("JOB_MAP_front",spr_bg_map_front,5,5,1,DEPTH_CHARACTER-8,false,FADE_FAST);
	_show("JOB_MAP_Kanes",spr_blank,5,5,1,DEPTH_CHARACTER-9,false,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_MAP_Kanes",spr_bg_map_Kanes_Residence,FADE_FAST);
	_pause(0.5,0,true);
	_change("JOB_MAP_Kanes",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_MAP_Kanes",spr_bg_map_Kanes_Residence,FADE_FAST);
	_pause(0.5,0,true);
	_change("JOB_MAP_Kanes",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("JOB_MAP_Kanes",spr_bg_map_Kanes_Residence,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_get_item);
	_execute(jobs_list_add_job,JOBNAME_Baby_Sitting,var_get(VAR_DAY)+1,var_get(VAR_DAY)+1,12,22,PLACENAME_kanesResidence,"");
	_draw_item_popup(spr_item_job_popup);
	_dialog("[c=#FFFF00]Your job assignment is updated.[/c]");
	_jump("Menu");
	
_border(100); // add content before this

_label("Talk");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Don’t pester me with chit-chat, I have work to do.");
	_jump("Menu");
	
_border(100); // add content before this

_label("Stay");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Are you kidding right? GTFOOH!");
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] What?");
	_change("penny",spr_char_Penny_Burst,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Get The Fuck Out Of Here!");
	_room_goto_ext(room_scene_stp_main_hall,true);
	_block();
	
_border(100); // add content before this

_label("Move");
	_choice(spr_choice_s,"Yes",noone,room_scene_stp_main_hall,fa_right,fa_center);
	_choice(spr_choice_s,"No","Menu",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Back to the main hall?[/c]",false);
	_block();
	
_border(100); // add content before this

_label("EVENT_STP_PRINCIPAL_VISIT");
	_var_set(VAR_EVENT_STP_PRINCIPAL_VISIT,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] This is my office. The design is meant to reflect the way I do business.");
	_dialog("[c=#ff3333]Penny:[/c] I don’t beat around bushes, I like things straight to the point.");
	_dialog("[c=#ff00ff]Chris:[/c] Your place is awesome!");
	_dialog("[c=#6678ff]James:[/c] It certainly emanates power over everything.");
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] No time for flattery. Don’t you fool around here or I will RYHO.");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] RYHO?");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] … We better go.");
	_jump("Menu");
	
_border(100); // add content before this

_label("EVENT_FIRST_MISSION");
	_var_set(VAR_EVENT_FIRST_MISSION,true);
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_choice(spr_choice_s,"Continue","EVENT_FIRST_MISSION_CHOICE_1",noone,fa_right,fa_center);
	_dialog("[c=#FFFF00]Penny wants to talk about business and motivation.[/c]",false);
	_block();
	
_label("EVENT_FIRST_MISSION_CHOICE_1");
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] In order to incentive trainees I give missions to newcomers. If the trainees complete missions, they get prizes.");
	_dialog("[c=#ff3333]Penny:[/c] Your first mission is [c=#FFFF00]to acquire a job skill, and leg press 200lbs.[/c]");
	_dialog("[c=#ff3333]Penny:[/c] When you feel ready come back here for the test. I’ll be available on weekdays after 18:00.");
	_dialog("[c=#ff3333]Penny:[/c] Now go home and rest well, tomorrow brings a hard day of training.");
	_var_add_ext(VAR_ENERGY,-10,0,var_get(VAR_ENERGY));
	_execute(time_hour_passed,2);
	_room_goto_ext(room_scene_apt,true);
	_block();
	
_border(100); // add content before this
