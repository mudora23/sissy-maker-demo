{
    "id": "d53bd576-7a1b-4cda-9d7a-c03fbd6aa349",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scene_stp_main_hall",
    "eventList": [
        {
            "id": "6fb9db5e-a0e4-45e8-a7b5-7b8d76a90de5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d53bd576-7a1b-4cda-9d7a-c03fbd6aa349"
        },
        {
            "id": "86935741-9586-485d-af4d-ceac98afd273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d53bd576-7a1b-4cda-9d7a-c03fbd6aa349"
        },
        {
            "id": "1d61108d-ab4a-4834-8151-bf5134dc99ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d53bd576-7a1b-4cda-9d7a-c03fbd6aa349"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "42dbcc52-2dd2-4980-95de-811041fccc94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}