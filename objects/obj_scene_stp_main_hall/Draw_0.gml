event_inherited();

_start();
	_placename(PLACENAME_STPmainhall);
	_main_gui(true);
	_show("BG",spr_bg_STPmainHall,5,5,1,DEPTH_BG,false,FADE_NORMAL);

	
	if !var_get(VAR_EVENT_STP_MAINHALL_VISIT)
		_jump("EVENT_STP_MAINHALL_VISIT");
	//else if !var_get(VAR_EVENT_SORE_ASS) && var_get(VAR_EVENT_DEMO_END)
	//	_jump("EVENT_SORE_ASS");
	//else if !var_get(VAR_EVENT_DANCER_AT_STP) && var_get(VAR_EVENT_DANCER) && var_get(VAR_EVENT_STP_GYM_VISIT) && var_get(VAR_EVENT_STP_LIBRARY_VISIT) && var_get(VAR_EVENT_STP_LAB_VISIT) && current_time_get_name() == "Afternoon"
	//	_jump("EVENT_DANCER_AT_STP");
	else if scene_get_previous_room() != room_scene_stp_hq
		_jump("choice_main_hall");
	else if current_time_get_name() == "Morning"
		_jump("Morning");
	else if current_time_get_name() == "Afternoon"
		_jump("Afternoon");
	else if current_time_get_name() == "Evening"
		_jump("Evening");
	else
		_jump("Late Night");


_border(100); // add content before this

_label("Stay");
	_stay(true,true,true,true);
	_choice(spr_choice,"OK","StayRestStart",noone,fa_center,fa_bottom);
	_choice(spr_choice,"Cancel","StayRestHide",noone,fa_center,fa_bottom);
	_block();
		
_label("StayRestStart");
	_stay_rest_start("choice_main_hall");
	_block();

_label("StayRestHide");
	_stay_rest_hide("choice_main_hall");
	_block();


_border(100); // add content before this


_label("Morning");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#6678ff](I never imagined it would be so calm here at morning. I guess most of the trainees have day jobs. Not Chris, she’s a lazy bum.)[/c]");

	_change("chris",spr_char_Chris_Pout,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] My ears are burning, someone must be talking evil of me.");
	
	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("*gulp*");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Looks like Chris’ perception stat isn’t as low as she lets on.)[/c]");
	
	_jump("choice_main_hall");
	
_border(100); // add content before this

_label("Afternoon");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER,true,FADE_NORMAL);
	_dialog("[c=#6678ff](A lot of women around here, it’s hard to tell which ones are transsexual unless I hear them talking. Here in this place, it’s safer to assume they all have dicks.)[/c]");
	
	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] You look intimidated.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Do I?");
	
	_jump("choice_main_hall");

_border(100); // add content before this

_label("Evening");
	_show("james",spr_char_James_Thinking,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER-1,true,FADE_NORMAL);
	_dialog("[c=#6678ff](This must be the busiest time for them.  A few men are wandering around too. Rich men, I presume by the looks of the tailored suits and expensive wrist watches.)[/c]");
	_dialog("[c=#6678ff]James:[/c] Yeah. I was just going over that in my narration, actually.");

	_change("chris",spr_char_Chris_Happy,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Maybe we can find a sponsor!");

	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] That would be really good.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](As long as I get my payment.)[/c]");
	
	_jump("choice_main_hall");

_border(100); // add content before this

_label("Late Night");
	_show("james",spr_char_James_Worry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("chris",spr_char_Chris_Neutral,1.8,YNUM_CHRIS,1,DEPTH_CHARACTER-1,true,FADE_NORMAL);
	_dialog("[c=#6678ff](Without all the trainees and coaches around this place is like a zombie apocalypse movie.)[/c]");
	_dialog("[c=#ff00ff]Chris:[/c] James, what’s up?");

	_change("james",spr_char_James_Angry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Gaaah!  Don’t scare me like that.");

	_change("chris",spr_char_Chris_Worry,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] What the hell?");

	_change("james",spr_char_James_Blush,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Nothing.");
	
	_jump("choice_main_hall");

_border(100); // add content before this

_label("choice_main_hall");
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	//if !var_get(VAR_EVENT_DANCER_AT_STP)
		_change("BG",spr_bg_STP_map,FADE_NORMAL);
	//else
	//	_change("BG",spr_bg_STP_map_plus_raye,FADE_NORMAL);
	_pause(0.5,0,true);
	_shadow(spr_bg_STP_map_shadow_exit,"going_exit",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_gym,"going_gym",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lab,"going_lab",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_library,"going_library",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_pennys_office,"going_pennys_office",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	//if !var_get(VAR_EVENT_DANCER_AT_STP)
		_shadow(spr_bg_STP_map_shadow_lock_room1,"going_lock_room1",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	//else
	//	_shadow(spr_bg_STP_map_shadow_raye,"going_raye",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room2,"going_lock_room2",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room3,"going_lock_room3",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room4,"going_lock_room4",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_main_hall,"stay Confirm",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_block();

_border(100); // add content before this

_label("going_exit");
	_choice(spr_choice,"Yes",noone,room_scene_main_map,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Return to the main map?[/c]",false);
	_block();

_border(100); // add content before this

_label("going_gym");
	_choice(spr_choice,"Yes",noone,room_scene_stp_gym,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to STP gym?[/c]",false);
	_block();

_border(100); // add content before this

_label("going_lab");
	_choice(spr_choice,"Yes",noone,room_scene_stp_lab,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to STP lab?[/c]",false);
	_block();

_border(100); // add content before this

_label("going_library");
	_choice(spr_choice,"Yes",noone,room_scene_stp_library,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to STP library?[/c]",false);
	_block();

_border(100); // add content before this

_label("going_pennys_office");
	_choice(spr_choice,"Yes",noone,room_scene_stp_pennys_office,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to Penny’s office?[/c]",false);
	_block();

_border(2); // add content before this

_label("going_raye");
	//_choice(spr_choice,"Yes",noone,room_scene_stp_raye,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Going to STP dance hall?[/c]",false);
	_block();

_border(98); // add content before this

_label("going_lock_room1");
_label("going_lock_room2");
_label("going_lock_room3");
_label("going_lock_room4");
	_dialog("[c=#FF8080]This location is not availiable yet, please go to patreon and support us so we can make more contents. :D[/c]");
	_jump("choice_main_hall");

_border(100); // add content before this

_label("stay Confirm");
	_dialog("[c=#6678ff](This is where we are at the moment.)[/c]");
	_choice(spr_choice,"Yes","Stay",noone,fa_center,fa_center);
	_choice(spr_choice,"No","choice_main_hall",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Stay?[/c]",false);
	_block();

_border(100); // add content before this

	
	
_label("EVENT_STP_MAINHALL_VISIT");
	_var_set(VAR_EVENT_STP_MAINHALL_VISIT,true);
	_show("chris",spr_char_Chris_Neutral,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_show("james",spr_char_James_Neutral,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Welcome to STP Headquarters.");
	_dialog("[c=#ff3333]Penny:[/c] STP was born from research done by two scientists, Dr.Joseph Hardth, my father, and Dr. Henry Macow.");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] After a disagreement of... philosophy, they pursued different paths of science.");
	
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Dr. Macow was shortsighted, seeing the research as a ‘cure’ for homosexuality. Father used his knowledge and help those young closeted find themselves a better way of life.");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] In short, father built this facility. Dr. Macow become his prosecutor.");
	
	_change("penny",spr_char_Penny_Angry,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Dr. Macow wasn’t his only enemy. Politicians, the media… they accused father of so much. They labeled him a threat to society. It was too much for him, so he became recluse.");
	
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](Wow crazy story… I’d love to meet Dr. Hardth one day.)[/c]");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] We are running low in personnel, so we disabled a few training sectors.");
	
	_change("penny",spr_char_Penny_Smug,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] I’m sure everything going to be back to normal in the full version when Sissy Maker is fully funded. Trustworthy backers all around the world make it possible.");
	_dialog("[c=#6678ff](There comes the 4th wall stuff again.)[/c]");
	
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] For now I’ll introduce the available training facilities.");
	
	_jump("choice_main_hall");

_border(2); // add content before this

/*_label("EVENT_SORE_ASS");
	_choice(spr_choice,"Continue","EVENT_SORE_ASS_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]Outside the Office, in STP main hall, James and Ronda hears the flick of the door knob.[/c]",false);
	_block();
	
_label("EVENT_SORE_ASS_START");
	_var_set(VAR_EVENT_SORE_ASS,true);
	_var_set(VAR_ANUS,-1);
	_var_add_ext(VAR_HP,-10,0,var_get(VAR_HP_MAX)+max_hp_penalty());
	_play_sfx(sfx_door_opening);
	_placename(PLACENAME_STPmainhall);
	_show("BG",spr_bg_STPmainHall,5,5,1,DEPTH_BG,false,FADE_NORMAL);
	_show("james",spr_char_James_Angry,8,YNUM_JAMES,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_dialog("[c=#6678ff]James:[/c] Ronda where are you goi-");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff](She’s gone. She sure has quick legs!)[/c]");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff](Chris came out of Penny’s Office, she looks a little flustered and on top of that she’s limping…)[/c]");
	_dialog("[c=#6678ff](What the hell happened in there?)[/c]");
	_show("penny",spr_char_Penny_Smug,1.8,YNUM_PENNY,1,DEPTH_CHARACTER,false,FADE_SLOW);
	_dialog("[c=#ff3333]Penny:[/c] Congratulations level 2!");
	_show("chris",spr_char_Chris_Blush,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER-1,false,FADE_SLOW);
	_dialog("[c=#ff00ff]Chris:[/c] Thank you… augh!");
	_change("james",spr_char_James_Worry,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Chris! Are you ok?");
	_change("chris",spr_char_Chris_Neutral,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] I’m more than ok! I’m great!");
	_change("penny",spr_char_Penny_Neutral,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Talk to Sophia she may help you.");
	_jump("choice_main_hall");
*/
_border(2); // add content before this
/*
_label("EVENT_DANCER_AT_STP");
	_choice(spr_choice,"Continue","EVENT_DANCER_AT_STP_START",noone,fa_center,fa_center);
	_dialog("[c=#FFFF00]As you walk into STP main hall, you spot Penny at the gym entrance.[/c]",false);
	_block();
	
_label("EVENT_DANCER_AT_STP_START");
	_var_set(VAR_EVENT_DANCER_AT_STP,true);
	_show("chris",spr_char_Chris_Happy,9.2,YNUM_CHRIS,1,DEPTH_CHARACTER,false,FADE_NORMAL);
	_show("james",spr_char_James_Happy,8,YNUM_JAMES,1,DEPTH_CHARACTER+1,false,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Nice! Can you hear that?");
	_dialog("[c=#6678ff]James:[/c] Yeah, the music really livens the place up.");
	_change("james",spr_char_James_Thinking,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Is this a special event?");
	_dialog("[c=#ff00ff]Chris:[/c] Partyyyyy!");
	_change("james",spr_char_James_Neutral,FADE_NORMAL);
	_dialog("[c=#6678ff]James:[/c] Look there it’s Penny.");
	_change("chris",spr_char_Chris_Blush,FADE_NORMAL);
	_dialog("[c=#ff00ff]Chris:[/c] Oops!");
	_show("penny",spr_char_Penny_Neutral,1.8,YNUM_PENNY,1,DEPTH_CHARACTER-1,false,FADE_NORMAL);
	_dialog("[c=#ff3333]Penny:[/c] Chris, James, good timing. We just open a new facility with dance classes. Go there and introduce yourselves.");
	_dialog("[c=#ff3333]Penny:[/c] The dance coach Raye Sanchez is waiting for you. At the gym.");
	
	_main_gui(false);
	_hide_all_except("BG",FADE_NORMAL);
	_change("BG",spr_bg_STP_map_plus_rayeBlank,FADE_NORMAL);
	_show("NEWSTP_RYNE",spr_blank,5,5,1,DEPTH_BG_ELE_SHADOW+1,false,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("NEWSTP_RYNE",spr_bg_STP_map_raye,FADE_FAST);
	_pause(0.5,0,true);
	_change("NEWSTP_RYNE",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("NEWSTP_RYNE",spr_bg_STP_map_raye,FADE_FAST);
	_pause(0.5,0,true);
	_change("NEWSTP_RYNE",spr_blank,FADE_FAST);
	_pause(0.5,0,true);
	_play_sfx(sfx_map_kane);
	_change("NEWSTP_RYNE",spr_bg_STP_map_raye,FADE_FAST);
	_pause(0.5,0,true);
	
	_shadow(spr_bg_STP_map_shadow_exit,"going_exit",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_gym,"going_gym",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lab,"going_lab",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_library,"going_library",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_pennys_office,"going_pennys_office",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_raye,"going_raye",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room2,"going_lock_room2",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room3,"going_lock_room3",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_lock_room4,"going_lock_room4",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_shadow(spr_bg_STP_map_shadow_main_hall,"stay Confirm",noone,FADE_SUPER_SLOW,DEPTH_BG_ELE_SHADOW);
	_block();
*/	
_border(96); // add content before this
