///Manage loading
//gmlvideo_asyncAssoc[? id]

var iid = async_load[? "id"], framebuffer_data = gmlvideo_asyncAssoc[? iid];
if (!is_undefined(framebuffer_data)) {
    //Belongs to us
    if (async_load[? "status"]) {
        ds_set_embedded(framebuffer_data[? "frame_buffer"], framebuffer_data[? "buffer_index"], framebuffer_data[? "frame_index"], "buffer");
        ds_set_embedded(framebuffer_data[? "frame_buffer"], GMLVIDEO_BUFFERLOADED, framebuffer_data[? "frame_index"], "status");
    } else {
        show_debug_message("Failed to load buffer");
        buffer_delete(framebuffer_data[? "buffer_index"]);
        ds_set_embedded(framebuffer_data[? "frame_buffer"], GMLVIDEO_NOBUFFER, framebuffer_data[? "frame_index"]);
    }
    ds_map_destroy(gmlvideo_asyncAssoc[? iid]);
    ds_map_delete(gmlvideo_asyncAssoc, iid);
}


