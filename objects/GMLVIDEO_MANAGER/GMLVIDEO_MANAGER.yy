{
    "id": "12e04de5-27ed-419d-b804-953e34b77b39",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GMLVIDEO_MANAGER",
    "eventList": [
        {
            "id": "976dbcd9-f313-4de9-be11-f5adc9a73414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12e04de5-27ed-419d-b804-953e34b77b39"
        },
        {
            "id": "f2ef8da1-e3ae-4d31-ab39-40e577355fe2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12e04de5-27ed-419d-b804-953e34b77b39"
        },
        {
            "id": "b5410167-f1c7-480e-a69f-20fe13398a5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "12e04de5-27ed-419d-b804-953e34b77b39"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}