///Update videos
var videos = gmlvideo_instances, s = ds_list_size(videos), i, video,
    d = delta_time / 1000000, u, u_frame, frame_count,
    frames_to_buffer = gmlvideo[? "buffer_frames"];

for (i = 0; i < s; ++i) {
    video = videos[| i];
    video_fps = ds_get_embedded(video, "manifest", "target_fps");
    frame_count = ds_get_embedded(video, "manifest", "frame_count");
    
    //Update framepos
    if (video[? "playing"]) {
        video[? "frame_progress"] += video[? "speed"] * d * video_fps;
        
        if (video[? "frame_progress"] >= 1) {
            
            //Keyframe checking
            if (video[? "frame_lastdrawn"] != video[? "frame"] && frame_is_keyframe(video[? "manifest"], video[? "frame"])) {
                video[? "frame_progress"] = 1;
                video[? "frame_redraw"] = true;
            } else {
                var advance = max(floor(video[? "frame_progress"]), 1);
                video[? "frame_progress"] = frac(video[? "frame_progress"]);
                gmlvideo_video_framejump(video, advance, video[? "frame_progress"]);
            }
        }
    } else {
        //make sure audio is paused
    }
    
    //Usually for mobile - loss on os pause
    if (os_is_paused())
        video[? "frame_redraw"] = true;
    
    //Do frame management
    u_s = ds_list_size(video[? "frame_buffer"]);
    for (u = 0; u < u_s; ++u) {
        //Cycle frames L to R (wrapping as needed)
        u_frame = (u + video[? "frame"]) mod frame_count;
        if (u <= frames_to_buffer/*in scope*/) {
            _gmlvideo_queue_frame(video, u_frame);
        } else {
            //not a relevant frame
            _gmlvideo_dequeue_frame(video, u_frame);
        }
    }
}

/* */
/*  */
