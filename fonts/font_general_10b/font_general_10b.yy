{
    "id": "66f8b2c9-8efe-44e1-9af2-bfc31150b3c3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_10b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bee7eb81-c573-46cd-9517-f1029317cd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "77dafd41-5bee-4fa7-9fdd-b6888d4312fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 13,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cddfbcf0-ce56-4e0f-820c-21746181bdea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "74c11afb-9a27-4119-a463-55012ccc31b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "173523b3-1c4b-4cd3-b1ec-afd4f256c181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 52,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e28886f4-5f39-4cc2-949b-719e3a5b826d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 65,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3f2b3576-a12f-4157-8e7c-e9683ab2c7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 78,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2fffeec2-7d88-4213-a6e1-142a27f78b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 91,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8bb64206-812c-4771-989b-a5c2b5618e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 104,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0b5417ec-bb60-489d-84d9-966a1d49fa1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 117,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73d425a9-0fdb-4931-97fe-917335ba226e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "db43a0c7-ebc6-4bdd-b9ea-c656c797ff65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 0,
                "y": 17
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "71587168-1307-4153-abb3-86b46dc0c6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 13,
                "y": 17
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8a25497d-3047-4aca-aa49-fbfd759088d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 26,
                "y": 17
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "affd044e-fd4a-488f-84ab-1dc957937157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 39,
                "y": 17
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "77d59d51-3ad8-4022-8253-e1eae95371d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 52,
                "y": 17
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8a5755eb-7922-4518-a330-33116c62e2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 17
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e29dfadb-7e2e-4a79-a0d3-5b2d9960fd3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 78,
                "y": 17
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "90bcf9eb-2eb5-4c7c-9087-dbd5b76935e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 91,
                "y": 17
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "588b78d8-79e1-45c4-bcd7-512dc02842e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 17
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ce1c2ac3-c088-4c99-8b98-9a64b256ce3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 117,
                "y": 17
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "87ad6ab0-fee1-467c-927d-c94c4d658f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 130,
                "y": 17
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "13d68e46-5436-4cef-bcc5-e4dad1df36c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 0,
                "y": 34
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f5b4bbcd-0959-4add-a963-22998bd006bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 13,
                "y": 34
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3c291f13-47f2-471f-9fb7-8e651c4bd7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 34
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "86c21ba6-bc73-465f-bd38-94d79e69344e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 39,
                "y": 34
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "35787c1a-92c6-47f2-808d-db970f166080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 52,
                "y": 34
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f9be0655-6c11-46f2-9d5d-98de3f34b9d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 65,
                "y": 34
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "57ded773-a9f8-45f9-9944-2494031710e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9d84e049-4531-468f-8be2-a0c3f3561767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 91,
                "y": 34
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5ad357fe-5fef-441f-afda-8fd97d284d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 104,
                "y": 34
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6187dcd1-71f5-4142-a7fc-4a20e492e8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 117,
                "y": 34
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "93941d13-9650-4d33-9b5a-5836dd08c85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 130,
                "y": 34
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "72eb2cf8-b6df-4163-bac4-56c17cb454ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 0,
                "y": 51
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a829632d-1f1a-44b2-8223-22fd6cc55be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 13,
                "y": 51
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4f6aa636-3501-45a0-a243-03be6a3bd3aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 26,
                "y": 51
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0da5b208-2a36-466e-b9df-93186c6b8298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 51
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d2bc083c-5be4-4406-bf0e-5d8b3b3fd475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 52,
                "y": 51
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a2d63104-c485-4418-8a58-4003bec44dee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 51
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "09b9f885-ee90-4a69-a86c-8e335f9b1123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 78,
                "y": 51
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f30dadfc-cb9a-4b7a-836a-5003ccdc397b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 91,
                "y": 51
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3f87c668-e1d6-4b08-9ea1-f1073aa97535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 104,
                "y": 51
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c110c4f0-73f6-4b6d-8167-c48b64abc376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 51
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "51738384-97c3-4703-9a9b-f54981c28a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 51
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "98b39eef-5b89-42a9-afcc-3aaffc7862e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 0,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c5d0180a-a7cd-4fc2-8d21-a72fdbeed316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 68
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "407ab912-6318-4edf-ba1c-7192fde66673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 26,
                "y": 68
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "974c3c02-b3c7-4dc1-b085-bfdaf99ea0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 68
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5b396b00-1ea0-4ba1-bf59-8f209eec66da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "14c7c936-c3ea-4ce3-89aa-930ad37a4e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 68
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "267c9c90-a912-4673-9f82-169198fae407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 68
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "90fd1548-88b7-444e-b4ce-cddf1319df7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 91,
                "y": 68
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4dbad894-1fd9-4e31-8f5e-4af855451442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 68
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "05f76c02-a131-4db6-a936-0e82edac1a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 68
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "47f8749f-bb3c-419d-8f84-5ff398e45561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 130,
                "y": 68
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "522a30e4-560d-46b5-a232-ed687507d8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 0,
                "y": 85
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "88026701-0bca-4f60-b0c0-fe27c9e05651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 13,
                "y": 85
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c27c7083-b9a6-4f8e-9833-44ae0a906846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 26,
                "y": 85
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "95893db7-f39f-4442-9db3-69cba98f3fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 39,
                "y": 85
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5044c2c8-ac24-487e-a731-dd30cfe0de80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 85
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "202e7553-f673-4c3c-9fb8-9e01d054528e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 85
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f42fd784-50a8-4c38-9948-cc8c06cf79a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 85
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6fb1338b-8356-4e31-a97b-8f087a55eafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 91,
                "y": 85
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "702e185b-01cc-4eac-ab0e-15cf612afe4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 85
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "36f7b7bf-ca29-45ab-8964-19c938a15f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 117,
                "y": 85
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ea13b568-c82e-47e5-8e42-2bd46f96ea54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 130,
                "y": 85
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "64c516cd-a2ad-472d-8bac-202bc7abe5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 0,
                "y": 102
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "48f502e4-9e15-4cb6-adbc-cb93c51fb595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 13,
                "y": 102
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8fbe8180-ba02-482e-891a-cb282989f1d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 102
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2dabe33a-1694-40b8-af75-48075182056b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 39,
                "y": 102
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c0d622f4-f655-4e11-bdc5-7ac084fafce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 52,
                "y": 102
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "898ef2f3-5368-44bb-9d23-93762d1513e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 102
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "28e537e3-2a37-4286-86ea-e88ac8f32b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 102
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e017ebca-0259-453a-b719-693e2d52dff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 91,
                "y": 102
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cb14711f-c418-40a6-8c6c-930e68dfe85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 102
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "dfca2425-9e9e-4074-90cc-4a1e1ef7bc1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 102
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "592b33a1-06d4-4b43-af10-5a08af6563f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 130,
                "y": 102
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "60b6082b-9e2b-4a2a-869b-ec365bf33d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 0,
                "y": 119
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "91832f96-221d-49c6-aeee-cf5b52ee229d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 13,
                "y": 119
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f36f999d-2785-43da-a5a1-a1d5ffb0bb99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 119
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9808aa9b-92f2-41b2-b127-6591ef2ff3dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 39,
                "y": 119
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4414f456-098c-4870-94de-0063c676ba5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 119
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b53d1322-46cd-4c85-a74b-b6d0a0d27db6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 65,
                "y": 119
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "126aecc5-c599-4ed4-aa60-14ee95c0552b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 119
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cef11a15-6c88-4ede-bd02-75f15fcb8ef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 91,
                "y": 119
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6eb350c1-bb30-402d-a582-6600c8f38057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 119
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f5611fcd-bbc3-4dcd-a6ab-d730320cfdeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 119
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "efee07e8-db5b-4f34-ac46-a3ed31b529b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 130,
                "y": 119
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "77c16d3f-ec93-499a-a069-94a1c39e1023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 0,
                "y": 136
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a4fa7563-f965-4132-b3aa-b085b6923333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 13,
                "y": 136
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "138ed625-a31b-4c21-aba0-2b9c1eab98c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 136
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0fb0c0b0-7520-414c-938c-2ebf9326d9d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 136
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "36ef0d36-9b5c-48e5-b04c-c130654d3b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 3,
                "shift": 8,
                "w": 1,
                "x": 52,
                "y": 136
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7855b6b4-be49-4185-b6a0-f492d51a79d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 136
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6b259f18-e56e-43c2-a2e0-145c51969629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 78,
                "y": 136
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "16f9afa4-180c-4341-917a-ca2f4b67c8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 104,
                "y": 136
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "456a0cbf-aa8a-4040-965b-8b4ff3941839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 117,
                "y": 136
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "dba227f3-a20f-4a8d-b172-b9d52606a742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 130,
                "y": 136
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "ba413fe6-9818-400f-baa8-ee8318ea71c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 0,
                "y": 153
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "2eb19675-e7d4-4e6e-b61f-a38fa636f562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 13,
                "y": 153
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "73c61fb7-df53-4770-b6b4-8185f4d9d2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 26,
                "y": 153
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "810c4a9c-cd6a-4c7b-9d2c-ac275051dc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f4908658-ee2f-4c5e-b5a9-9e6457070471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "824ffc38-02a4-4dae-b472-a774f610f460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "d5703987-07f0-46ae-8546-b785d1bd0764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "ada571d8-fb68-4c87-9bdc-78105d46e612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "326ce82d-fa9e-457a-9ea8-e0657d08f3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "95b7c1f1-80b4-4a18-bd0e-00b43a6137a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "45575845-5374-40ed-8bde-c13b4cd72a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "9cc78b39-607d-442a-ae1b-d7006dbfe6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "9df3e344-950a-4626-83a2-021a46bd5a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "04ac2edb-0424-4f62-8393-8b73eee6030d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "9f8741bb-142d-4afe-b486-649c261efac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "8c3ec46b-0ff6-499e-8aa8-238fd2ece2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "ea533289-838e-4fe2-88bb-e3d772543db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "6b3eaccb-dfe5-494e-9bdb-27904d79f3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "0a023957-4204-4382-a53e-03f5834f23d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "ce5c2346-dc97-4a5b-8b20-40ff42c19547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "bb71c1c7-b615-44f9-bc84-5e856710b86a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "5f5e0659-7b3e-4849-9a20-eaafa22c2c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "8cf80940-29c6-4e40-b32f-d72308639619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "6a4478bb-e089-4d44-b332-52696cc86a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "3c112a10-5bf2-4bae-a796-aef65c6e3f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "136d1a02-86f2-498d-83e1-89dccdecc8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "6cc625e4-f3a8-4039-b0a4-221c57e8c7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "80cec4ab-bac5-4894-8e17-1944b4935092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a9c3ff2a-61fb-40fd-bd6a-a8e8dcc0d31a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "9a2aae8b-6f57-40b8-bacf-7969f42adce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "c52ab8bf-15c8-4cb7-bee8-d09f1d869f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "06ae5687-753f-496a-9b96-d659d16f5783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "fdd8f8db-5165-4a5f-b85e-ee0fef6c2c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "683e3897-916e-4cb8-87c2-e097fbbc1f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b1eb6ff7-bc28-401b-b8e1-7136bac521a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "c1efca83-3655-48bf-b16b-6ed964d636b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "b20667ee-f6a1-449d-a0f2-ee3a34c998c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "30ac35ab-c740-466c-adbf-c534b191e9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "d56c81fd-db78-4e8a-bc84-0b595f5c702c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "107c3772-bd77-4c4d-9c60-5b048cbb87ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "f847515d-abf6-4cd1-89a3-0713afa1823a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9f82a7c2-d34b-43ba-8b15-f9477387e246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "eaeaf036-c854-48bd-b693-8b712e408f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "31537719-daef-40a3-b1f0-ce7f4169bf7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f3dcd86e-7f1e-4e44-830e-9f109901b193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "19a0ea8c-ba5f-4046-a245-9149595dc0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "78259ba6-63f9-4580-b6cf-19c414febc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}