{
    "id": "3c6469a8-99de-43de-9129-d93aaa1dca64",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_20",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "618481f1-5cc5-418a-8875-619b47efeea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "513de7bf-9a48-4035-9499-425a542d37e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 25,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "43b7ce49-12bf-4ca5-a443-5712a11044a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 50,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "386443e4-fee5-4dee-9176-70acdd850a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 75,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "00486f82-dcd0-4936-a53a-ea2c8c61d763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 100,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2cb68ab1-e8b6-4226-94be-7b7d1610f8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 125,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "812a32df-b54a-404a-8630-e5ddbb1c96b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 150,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "694e47f4-f875-4e88-b836-2c08d6014a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 175,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a79b1696-9b73-4d71-8fb9-650ae84fc9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 200,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "952929ad-9fd9-4be9-91fc-91628c6ac0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 225,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "78eb06b9-050b-4278-aa96-cbcd68097168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 250,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c493349e-649c-4cbc-95f3-6b7d9ca1aa81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 33
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "915ebfa8-4228-45b2-8673-948137c232ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 25,
                "y": 33
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f621268d-8894-4173-9bfa-e37be68d5048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 50,
                "y": 33
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cce549e8-e3ac-4561-ba10-8a151f1d27de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 75,
                "y": 33
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "99c90aaf-0558-4553-943c-c5a4ca0f9234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 100,
                "y": 33
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "da93c050-f815-4bba-aa36-551d93d8be55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 125,
                "y": 33
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4b6da55a-0c8c-423a-baba-631f03cd099a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 150,
                "y": 33
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1e6168a2-28a7-4ab3-adc7-f501e0410713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 175,
                "y": 33
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3f51b0db-9b1e-4541-8dd4-c1d4254574bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 200,
                "y": 33
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "dcda97b7-d9e5-4452-9626-13fb4b1b2647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 225,
                "y": 33
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "29494b44-4563-4415-a540-2453064374d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 250,
                "y": 33
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7329c504-4caf-42f9-a696-4ee54c854313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6185fef8-2fb6-42f0-b37b-3e9edfe77916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 25,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4524e15c-1697-47cd-b666-97a8ebcc2bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 50,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f9e12105-283f-4292-adf8-d63bb0bd4378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 75,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "31d0f7fa-e9e0-4a4b-8963-ed231986fd72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 100,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d67c9a10-67f5-445a-a32d-79c799044ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 125,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e008e483-9769-4f79-b3d6-a2beb41e6265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 150,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b62a5669-ef1b-453a-a0bd-8c526a2a5f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 175,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6bf450e5-4c21-480d-9a15-c918bacb3c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 200,
                "y": 66
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "41e2f2a4-e792-4eee-8789-e4047411a5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 225,
                "y": 66
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b26a22e5-cfbd-47fe-a2db-0664c1537df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 250,
                "y": 66
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0a13e352-0eba-4d18-af30-bdefaf96c21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 0,
                "y": 99
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7e561a2c-f792-41d6-89c9-511838fbde17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 25,
                "y": 99
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5cb680f8-cacc-4f5b-b175-ec2eb028b0d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 99
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "26d4a44f-efdc-47fc-b1d6-e99497d21a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 75,
                "y": 99
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4d474bcb-f001-4df8-ba6f-8d5e5c0cc052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 100,
                "y": 99
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2a752bf3-adc1-472d-8936-2cb4ddba45e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 125,
                "y": 99
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "506009f1-050a-4223-9fa4-2708ed04fd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 150,
                "y": 99
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dc7bd1c2-f19f-4ab1-8dc3-c2696494ce80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 175,
                "y": 99
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6845bf2c-6db8-4b77-9d8a-18abb30fa995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 200,
                "y": 99
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f9851d10-d26c-4e89-bda4-3308c3d99c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 225,
                "y": 99
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "63c29598-21d6-41d3-8b18-384b3a2910e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 250,
                "y": 99
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "923bfb1e-965a-4033-8940-4453153de3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 0,
                "y": 132
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ffb52358-3d08-4ac2-9cdf-28dbe2207c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 25,
                "y": 132
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "07e5275b-d4d9-44dd-bbde-88145382bae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 50,
                "y": 132
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d7a3249a-6665-41d5-b909-cc643abb5b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 75,
                "y": 132
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f132ad55-80e1-40ef-95d8-6a9a40559c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 100,
                "y": 132
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "37da01ef-c17a-46e8-9d40-5f3daaeb67ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 125,
                "y": 132
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "678a3f95-0a86-487e-8599-e07f25f863d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 150,
                "y": 132
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4a3d17a1-65e5-4d95-8616-140aa7d5c2cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 175,
                "y": 132
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "612de0e8-063d-481f-8600-e1001453f3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 200,
                "y": 132
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "da94c118-7ff5-47a5-9612-d64f44cb62ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 225,
                "y": 132
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4ac418bc-91e8-459c-8b34-6612820ce21a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 250,
                "y": 132
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1a5ba1db-235a-49e2-9e36-844d6889ab41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 0,
                "y": 165
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "596d12e0-6233-4c1d-9fc0-7d793a0b6bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 25,
                "y": 165
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "86a67ac6-1e9d-4dba-a24b-b672afa9ca49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 50,
                "y": 165
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f9510588-0b7b-41bd-b58f-d07e02f50fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 75,
                "y": 165
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6c008e6d-31cd-401c-bdbb-b074ad1e6ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 100,
                "y": 165
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "622e03b2-d02e-4646-b922-76a189326370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 125,
                "y": 165
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a6f1b0ba-3377-4945-a04e-fca2075de5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 150,
                "y": 165
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a05b785d-ec67-44d6-885a-3fb5634ca3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 175,
                "y": 165
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "461a61fc-4b07-4556-aed3-3f81fa8164ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 200,
                "y": 165
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d9c4484c-2ae4-40e2-87d1-1ff79ad7f7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 225,
                "y": 165
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "df066515-e3c6-41f4-ab1e-047e895b23b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 250,
                "y": 165
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2560637f-c2df-4e7b-9bfa-489f821e4493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 0,
                "y": 198
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fd7d2f04-a960-4236-a28e-69dc50a80aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 25,
                "y": 198
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "00dd1267-aee9-469d-a609-463bd27fb53d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 50,
                "y": 198
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "06e15a8d-119b-4e12-bd0c-af89c053c2e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 75,
                "y": 198
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b58341c9-6344-4de8-86ed-66c2ea7ad29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 100,
                "y": 198
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ff49f02e-c25f-403a-a43f-79bc282df889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 125,
                "y": 198
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1b03dd1e-175c-4955-ab10-4568127a597e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 150,
                "y": 198
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "185e9646-b5bd-4a3d-bb20-85d5fe8951b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 175,
                "y": 198
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "12c04d71-84e4-4f8d-ae7f-0ccb57464f9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 200,
                "y": 198
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5a527785-b852-40ce-b6f5-caf32f27773c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 225,
                "y": 198
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "64fe1a6a-f44d-475b-a0bd-827fc86fa5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 250,
                "y": 198
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "396cd732-de9e-43cb-be1a-449084420b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 0,
                "y": 231
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6bd943d8-5606-4def-ada0-6f10ac533fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 25,
                "y": 231
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "823f2d26-0057-49a9-bd97-00af338a6455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 50,
                "y": 231
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "66566964-0a90-4cf2-b44b-231fbd0f43a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 75,
                "y": 231
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ac7b6900-612e-4188-845a-ccbd6e1d95e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 231
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "988548f5-06ca-4ef9-b7dd-e80fa1f6d9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 125,
                "y": 231
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6db3f424-1cba-4771-a208-1876d954efbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 150,
                "y": 231
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8d47c84d-26f9-4bcf-8acf-ecfb2b2022b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 175,
                "y": 231
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a782f31e-a5dd-41fc-8fc4-57a8006ebc5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 200,
                "y": 231
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d69b20fb-05b5-4eca-a8ba-2bfff93179a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 225,
                "y": 231
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b3a6f0fe-3cfc-4737-8e99-60d793f7f797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 250,
                "y": 231
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "be9aac91-2257-4136-bded-3459236ecf72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 0,
                "y": 264
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "309b037a-dff6-4567-8c2e-da9f6c62595b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 25,
                "y": 264
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d2480c7e-e3f4-488e-b002-c5a312cea7f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 50,
                "y": 264
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "dffbb2e4-8a1f-4bb8-b8b1-ade484af46e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 75,
                "y": 264
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "318e95a8-1bd0-4de2-95f0-37800539fbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 100,
                "y": 264
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "15902fc3-ecaa-4ff5-a598-678f6b4544a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 264
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7451a9c1-ae89-49be-96f7-98006aa6796f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 150,
                "y": 264
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "ba6d8f93-18ee-488f-b841-4c224cef470f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 200,
                "y": 264
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "e2677b28-ade7-48c1-909a-f8887473b4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 225,
                "y": 264
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "1cda4cbc-e43e-4317-bfc7-e2806baa6498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 250,
                "y": 264
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "f9dfe497-90bf-4463-bf00-bc03f637fff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 0,
                "y": 297
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "00af1a87-e02a-4124-b6c0-384a5306087a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 25,
                "y": 297
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "a6bc7fcf-f23d-48af-8228-957e89da65c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 50,
                "y": 297
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "bcba5324-946b-4b29-a8bc-4e7fd68a5e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "42011df7-5c97-49c8-9b31-77977e9435a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "2082d279-407a-4c99-a7a7-febcb4081dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "b5948ccc-1a08-4a1d-b3e8-452b4a3d0a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "c913c3cf-7696-4fec-a094-7e1f3c1e0611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "e34eec34-85a2-4608-9ad8-3fb8fc514234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "cb140df7-a62d-4750-af8f-76ffdb4426dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "b6191e58-4c88-43ec-8de0-bd5c03093a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "5c2c7cad-976c-429e-a838-89ccf38c6e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "5854142c-0b70-4ca6-82da-c1187922c490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "00b2b2cb-1caf-4a71-9741-9ba53fbb5825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "b7731830-54ab-4683-b0f0-a83e15707f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "533985fa-86f4-4079-87e9-bd20462804e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "d3bb6369-9a73-493d-addc-da3d79daa6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "92babee2-8663-47eb-be9b-a9223590426c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "c9923dc9-800c-4c52-8fc4-ecd9c84e11f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "25364785-79ec-4245-b9ca-b4049042c64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "f75f0a63-2930-4877-aece-fd482a4eb126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "99405181-4bb8-4772-8089-bb5403ec0cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "9ad302ca-264a-4d7d-90c9-ec7a6f9f8952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "9b78fd59-307f-4b4b-822d-a32635a8beeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "e4846185-6151-4606-8a1f-0ad63b1be618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "c5549290-507c-4c05-9316-5821a16cb171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "92f849f8-23ed-4977-aaa7-4eb1f1ea9902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "de104cab-f8be-4a0f-b0be-c33fb62d7323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "4a9f168d-6665-4847-90e3-70be7ee619d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "94b2b3f0-c558-4e0d-aab2-6e6a0c635e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "303adce4-d64f-4a33-af6e-d21f192d9d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "f1200639-af69-4544-8298-a44d1e0d0803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "2be96039-788d-44a3-aa9d-96e38e78fdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "a48c4013-ed32-4985-8c93-2ca385f85d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "b365aa5c-36d8-417d-9cef-7d337af3efd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 104
        },
        {
            "id": "222be918-020f-4bcb-a8eb-1155e6dc6be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "7b17523d-9007-4f24-96b3-7ed493c9b2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "fdb12309-634e-4222-bbf1-fc693309d8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "a51a11f9-ba59-4639-8a81-ee419a6af9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "b4aaa756-3672-4906-aea8-9c369f9266a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "571562d7-9fde-487c-b53e-0ef3c5934db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "48e13d2c-0e99-4586-8213-660f293fa33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "0604cb51-332b-4f20-9d7a-695a6b14d55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "22fcf22e-96ce-4014-a4ea-7f7c0d2511b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "b2e9bc60-90d7-4575-b295-0bf90e23a5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "d25ff050-738a-4c44-90ab-5e058b49ef4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "9147139c-7e23-4406-9e09-9358b4ec36fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "096d4b9d-b8ba-4192-a2d2-53bcc38007dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "eae01833-8fb1-431e-819c-5ec88ff56334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "4d5fbc48-6f8d-4d64-b079-448de222d5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "4afd3f44-393a-419e-9e3a-b5ea2d328dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "ca37b42a-850f-43b9-a241-bf2327b055de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "f7a59d7c-a49a-498f-8ebc-40efe2f033d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "2f7c75d2-fd88-420c-bac4-c61669a6176f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "108d9777-54de-4d63-b021-4620df064913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "9d46a484-0329-4226-8a0a-b6ff90409684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "74260c80-54b5-404a-ad2a-9075e2ec9180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "9a075503-8d91-4ef5-a2fb-930c072dc4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "9210c585-f40b-4200-a54c-9d2df0ed3e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "51ce8329-a334-4c14-a869-5f8ae5a1b175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "60153037-da06-4f0f-ba8a-58361ec0f80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "d5f649c0-ea2b-4274-9f1b-ee6ba0e3b817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "734b7d51-f252-4e91-b64b-bd3d60b11527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "48f128e2-d96a-4423-9f4f-6d2a8963a188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "18890b38-9008-4fdb-8a58-109efef7789e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "7d4d701d-62f0-46d7-a6ad-96354a13f9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "613e1ea0-f45f-4989-a80f-b9467117aaec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "b0a2511d-ca99-43a3-8318-52fcfe1d2432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "78b4edb5-ea82-461e-af5b-2017e50b4831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "bc5edbd0-c2e6-45a5-9686-a7e4d74bc4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "a3f7a0cd-8add-4af6-81a4-0862d0c06986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "e7eacec1-e30c-4007-a865-8d806f51c134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "18c8a7d8-96c3-45ff-bb8b-fa6ec5f3a74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "4fe55826-3109-4c81-8b31-83e7225a7545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "54971ed5-626d-47da-ae59-079f0d8154e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "485166a0-257f-48e9-bb86-2666594d6f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8208
        },
        {
            "id": "8fcd4164-fa21-46db-85bb-f4cf89d9de3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "e3dee212-fb94-4762-939f-b384cee0aac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "fc608ef7-c260-41d2-aa78-8d3e029b9b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "553fc000-d8dc-4fd2-87bd-4e15337f31a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "e4d93502-0ce1-4ea3-a201-914a35eff4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "740916a4-3c83-47b5-82ff-49da1d529087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2c68c396-22bb-4e12-99ee-d16eb78e8e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "9ab36855-9e39-4135-9fea-3f7b3282dfda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "57e84f2d-6865-40d6-bae2-7146d8e61788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "525c77e7-9810-4164-b51b-06bd4b0dde09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8208
        },
        {
            "id": "40e58a97-4d19-4e06-8f90-e75c9febe089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "67e344ee-8ae0-4721-96b9-14f0e9783e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "86b3c948-3128-48cd-96ff-010af22dcb7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "16cd7dd6-446c-4d1e-b77f-4d930b99b8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "8684f982-e42f-4aa3-a432-735966f0937e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "fff24a49-7b8e-41d9-8030-6241bff3f711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "10662547-3e24-483a-ab71-d404454f9a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "3c3f6f6b-1009-4547-8d51-910095903505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "6eb5653a-56bd-4f14-b037-e69154868b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "7a3bb866-5324-4b46-b2df-5a0821161d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "c80ac6cc-ad87-467e-86a6-e1dfc65570b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "070e170c-abb9-4930-be42-1bc8c2bee5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "64e4da29-d849-4f4d-9c7b-415c222ac4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "f1ffa9b3-22a7-4b7a-a8b6-a57a0c97841d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "33433b28-b581-419e-8d87-880be2fd20cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8208
        },
        {
            "id": "53ab1207-4a8f-46f8-ad79-6b85341f69cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "3558d9c7-1598-43c3-bae2-84052ca55d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "0b535a57-7910-415b-9b64-c34d62f38f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "932ae3fd-f2cc-4074-a6d9-89d792fc91ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "32ae45b4-e615-4d05-837d-a5a34ac8a4e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "5d46d698-6514-4373-895f-0c1140f72ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "0dead847-c4c2-47d0-adfe-3637d8cca13f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "09b7ec81-3882-4e3c-9d4a-8132d28a78e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "9f429f44-848a-46d5-9d66-8ce15b802cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "9dbfcd65-508c-413b-a0b4-c479a1a30a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "eabc2d09-15be-4c48-9818-20b61272b583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "460acc8f-f6e4-4bb0-a564-99f8a7625c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "5edc4c96-2f91-4c4b-a495-a3fd621d8bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}