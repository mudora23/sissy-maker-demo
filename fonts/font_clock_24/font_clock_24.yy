{
    "id": "92c0138e-ddf8-41ba-bdc7-12003810efe0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_clock_24",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VCR OSD Mono",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "78177263-58e4-4ba5-812a-05c721aceebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2f323b47-032c-4e2a-aeec-357ea73cc62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 4,
                "x": 21,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7aa50c07-5611-476b-bf12-fe73bcac7b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 42,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "78839045-51d1-47b2-aec3-1ffe508bc200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "96af36e1-fc24-40e1-8afb-bdcd276f6bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c1687ca1-4360-403d-8cd1-f0c249ea44f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c30320d3-cbb0-4e75-b1ac-b433b11f684b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a981733a-074c-4a23-87a1-c4d27e66422e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 5,
                "x": 147,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b2058862-ac15-4995-84d2-faa3f543f66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 168,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1fe9e480-bdf9-4509-a080-6b75a1f5a402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 189,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "64ed283d-03d2-4de7-989a-6eac107937fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 0,
                "y": 33
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f7d5268d-dc22-4dcf-963e-6eb8962d1735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 33
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "70af8d2d-7895-4e10-93a9-6435c03df394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 42,
                "y": 33
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0e87921f-4b58-4a03-b9d1-741e562fc4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 63,
                "y": 33
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2dc59cca-ddb6-4e7a-a65a-9c5495aa73f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 4,
                "x": 84,
                "y": 33
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "528e7232-901c-48af-806a-3458183a3fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 33
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fccd27e1-aa0a-4f21-bcbb-7a9ea02e46ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 33
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a8263955-caaf-4c65-846e-98db7a344b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 147,
                "y": 33
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "84ceea8d-892b-4cef-809f-6b7d499d1ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 33
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "129cff45-6a83-4544-b882-335524acb349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 33
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "29717926-141d-495c-87c8-dc9d8029b7ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 66
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ae57a43f-0765-44a0-bb18-8a82231ac631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 66
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b32126b9-a4b2-4bf7-bfa9-28002641ad04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9fd28941-8ff9-490c-b5af-5177d87cb630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3d5cf9ff-4052-4538-9aa4-4a3442adc69f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b5d8836f-55cd-4baa-8ec4-d7c58aee75bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e3003357-d55c-42ab-9358-cd1df55d4c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 4,
                "x": 126,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7cdf6f42-033f-4464-b7a8-14d2d8c4f4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 7,
                "x": 147,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e3b42604-1958-4b2c-85df-016b754c176b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 168,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a8544ef6-b62b-45c3-96a3-0f97a2feee5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "81be554a-6568-4d60-b4ea-b5539ad2daa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 0,
                "y": 99
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4847eff2-af92-48e5-850f-663f7a358e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 99
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e8dcbe34-9faf-49f4-b1b2-7aa13edf4233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 99
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7dc75cff-a42b-40b0-8942-e45042fb1d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 99
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7a79abf8-da5e-4852-82d0-d12f25784a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 99
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "39f17f04-b27d-4164-8351-dca17feafaa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 99
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0a06fdd5-44a1-4e7a-975b-bd318fcfae77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 99
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6c4c622a-0ed2-418b-99cf-59f08d4a5274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 99
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5cfb8796-c128-464f-a78d-376daf3cd00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 99
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "70a86043-7104-47f3-a845-1e2063b35dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 99
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c60e5ccc-9564-4ad5-90e3-7b03bcc64de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 132
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8983c885-3f66-4f9a-a13b-e95c6d327f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 21,
                "y": 132
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "18e20e88-e4ee-43f8-997e-68924ccc4800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 42,
                "y": 132
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5307ddb3-c7f5-421f-9769-15be9f58a70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 132
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "25dc941c-3a00-457a-9c79-9f00432fbc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 132
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "95e60446-7a70-46cd-96d5-389998801238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 132
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "12b786ea-6a62-4675-b0df-c379c83cf71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 132
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "68511a50-ac63-4666-81d0-e49f0d42c6c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 132
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a39d7c83-2756-4783-ae21-164b60835a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 132
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c4702b51-f0fa-4f30-9c2c-3f9707e36134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 132
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3bd62b9a-2466-4bbe-823c-1dd27ecefda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 165
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f1342564-5fd0-4c57-8726-f8402d84565f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 165
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b7fbd64c-67a1-40fd-860b-91b100c1b350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 165
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "efbb7f3c-bf48-4e99-b940-94589e608894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 165
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e1f50b0f-bb28-4454-a7a2-2374406189ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 165
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "aeae20bc-925f-4dcb-8300-60dcabea926d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 165
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6f5e3d8d-f6a1-464a-8624-132473bc289e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 165
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "076b60dc-c8b6-4d56-a679-924b838e15ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 165
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "707c449e-5199-46ec-af7a-1160d02d6a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 165
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d7c06aeb-c863-4302-86dd-98e626acb9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 6,
                "shift": 19,
                "w": 10,
                "x": 189,
                "y": 165
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "50e8e2b3-a4b7-4dda-a5e0-9483e256ba8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 198
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e9595829-b1a9-41e7-a1d3-533d0bfa4e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 10,
                "x": 21,
                "y": 198
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "21c7adc0-00f2-4977-9549-cf2afdfbceda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 198
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a9ec9845-4caf-4a93-aed4-7b6e0b8e6728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 63,
                "y": 198
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "519f0a79-47a6-4327-a0d4-ea72e57fdaaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 84,
                "y": 198
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "95296650-32e7-487a-9e76-7c6e56ab718e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 198
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a20fb344-e6f0-47ea-8afc-c7b7d41e35e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 198
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "14f49051-1a49-4c6d-a74d-4f8dfe294710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 198
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1d3c1c33-bd13-42aa-bb2a-384a2e1ed153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 198
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9fb060c3-a7e5-4a5e-88e6-ac4bfa3a45cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 198
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "502c8f5a-fa1a-4cfe-ba2b-6364905c6598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 3,
                "shift": 19,
                "w": 12,
                "x": 0,
                "y": 231
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4482a073-4fe7-420d-9797-33102a60ca25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 231
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "67320072-6869-4282-a8c8-e678ead92384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 231
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "88382b1e-970c-457f-b842-903abb41c454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 63,
                "y": 231
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8b1fdc1c-3ddb-4c70-975e-78068c8c5cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 84,
                "y": 231
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "604861cb-9129-4d9c-8a20-eca2161f978d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 231
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "365d9d77-6810-4fc4-a008-509639cdf717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 8,
                "shift": 19,
                "w": 4,
                "x": 126,
                "y": 231
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2866209c-9fb4-40b5-8dba-5ebd74b6ec8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 231
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3ca7fc5e-5d57-4bd1-a249-e1f9845553a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 231
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b445f649-8ab1-4bc3-bc2a-135f6335546a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 231
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e54914f5-c799-42b0-b5b1-c6755a135ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 264
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "53bba9a9-b4d8-4105-a108-86870e64f909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 264
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d3b1cbe1-9a25-479a-ba47-e73ce2e619d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 42,
                "y": 264
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7403d06e-a501-4ae3-bfa2-44cb80d2e928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 63,
                "y": 264
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "78486191-aac1-4696-8f5a-b46c57db53e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 84,
                "y": 264
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "61ff8a30-3477-4bfc-84ed-92f446718a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 105,
                "y": 264
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "58a974c2-89aa-41de-8e38-23bd2cc4b6ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 126,
                "y": 264
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "434cf281-d3d5-4cc7-9128-24667e751c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 264
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2e7dfe9e-7722-4d3f-a098-2edcb3a2408b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 264
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f02c1102-63c6-48e1-9b6a-b72e8d3d3aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 189,
                "y": 264
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a54de9f3-ed8b-491c-a862-2ddc036b471d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 0,
                "y": 297
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "906dfd15-524e-4695-9532-d26a1a987d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 21,
                "y": 297
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "768cff68-9054-4174-8d78-4a84127fb957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 8,
                "shift": 19,
                "w": 4,
                "x": 42,
                "y": 297
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2874afa8-4ac2-405d-b462-a2cff15f61c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 63,
                "y": 297
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6cf208d8-9a46-4330-92ae-bb270a797b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 84,
                "y": 297
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": null,
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}