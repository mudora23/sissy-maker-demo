{
    "id": "34b6fdf7-99fb-474d-9edc-8d2eb3a7689a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_30b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7ac6c931-13f4-46f3-9710-7f2ab15416b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 12,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "49b2f3b3-bc77-407b-bba5-a7035f4c4c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 6,
                "x": 37,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "be1f4713-3249-40d7-9207-292725c8de50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 74,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b012f1a4-a70b-4e10-b982-08ad6a6da013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 22,
                "x": 111,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "15772325-6c91-4962-8e61-5f46ae395e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 148,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "891b40e9-7576-4d4d-91b3-59eb49cca0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 185,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a4393790-afd9-4c8f-a2df-ccf69e364376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 222,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8cab311a-c97f-4bdc-a32e-07bc0c4ea7d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 259,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9076820c-3221-4ae2-b31a-7f2e999c9ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 9,
                "x": 296,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4f002b8a-21b7-49a6-ab23-7db02db2fe60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 333,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b00b8666-d97c-4eb3-af96-7b56491d359b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 370,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2a14ecbe-dcd1-42e0-a80e-4bbc2f64d9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 0,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9b72a5c9-7cec-45fa-b470-c68106a10395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 37,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4e063dd8-6741-483f-bc79-08101f4fadc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 74,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "813fa630-92dd-4f83-8b29-43b588157d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 6,
                "x": 111,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b51e6b0a-9884-4939-bf1f-c4df72754b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 148,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b4803c37-f952-4a8f-80ff-8827e14be932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 185,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d94c71c3-4def-4950-982c-718866de7c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 5,
                "shift": 23,
                "w": 11,
                "x": 222,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b74a18ac-0855-40c1-9749-9b87ea054cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 259,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c953a27e-5534-4b0d-ad71-11b30de0ccf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 296,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b01b9c3b-3542-49f8-8503-fb79182fa7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 333,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cb48ed4b-e396-44d2-8835-6b29c427de96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 370,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a630c20a-4b44-40cc-849d-84aff1b111a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 0,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "59bd0b6f-8913-45d6-9c5c-5504e92f4a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 37,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "be42f185-3b15-41f7-a024-f9bee20befe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 74,
                "y": 96
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9d8b535d-e6fe-4752-ae28-9e7587072ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 111,
                "y": 96
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "704650e6-9420-480d-ba41-f0ef70649b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 6,
                "x": 148,
                "y": 96
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "da8d049c-df89-486d-9ca5-6fb920cdc98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 185,
                "y": 96
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8b184a19-baf8-433e-8f8f-ee1f57430cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 222,
                "y": 96
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "727343d9-232b-4a61-af6a-552a02285b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 4,
                "shift": 23,
                "w": 18,
                "x": 259,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "38c039ca-a77f-4344-88f0-5a92dafbc448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 5,
                "shift": 23,
                "w": 15,
                "x": 296,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9b2f6059-17a8-4189-aa94-00364e3e06bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 333,
                "y": 96
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c4e9b74f-3e1b-4611-8514-4dd8ef7c9190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 26,
                "x": 370,
                "y": 96
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e7b13f36-c675-48b7-ac62-b42f943c720e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 0,
                "y": 144
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c565c3b1-a1f6-4214-a13c-baf2ba6fa176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 37,
                "y": 144
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dfbd5404-8f3d-49fb-8099-67e95b2d6187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 74,
                "y": 144
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "48d74148-48c9-4186-9a49-1cb29bbffeab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 111,
                "y": 144
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9c425ea0-24ef-4153-87e2-54d793b7439a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 148,
                "y": 144
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "26cf49d8-a7d6-40b6-a1eb-9aff7ba1733a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 185,
                "y": 144
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e5442e89-f46b-403b-a5d4-6fed08a8bbf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 222,
                "y": 144
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "14b8503a-46f2-49c5-8997-01c12ebdb39d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 259,
                "y": 144
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d62b2cde-1c59-4b2d-ada4-8d186d4a1fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 296,
                "y": 144
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8b6ed1c8-d4d6-4c94-9c52-1e77c52afb4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": -1,
                "shift": 21,
                "w": 19,
                "x": 333,
                "y": 144
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "79f7a203-c23f-463f-9d6e-1d0026be152a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 3,
                "shift": 25,
                "w": 23,
                "x": 370,
                "y": 144
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "81a1b7f0-71d9-4d0b-bbf1-c84d120970f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 0,
                "y": 192
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4721e404-7d79-4085-9caf-e770fb30aee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 37,
                "y": 192
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6401075f-e916-4b0e-9b4e-e043f0119973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 74,
                "y": 192
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "755fea76-1aaa-4ae4-9fce-e397f9308d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 111,
                "y": 192
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5bed14ad-b009-4647-8ee2-c951513975cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 148,
                "y": 192
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0a73404b-7090-4170-9436-9a731331f0dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 2,
                "shift": 28,
                "w": 29,
                "x": 185,
                "y": 192
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e5c9e222-3694-405f-92c3-29dfcf34f4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 22,
                "x": 222,
                "y": 192
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dc642f65-b82b-4162-9cb3-d0b5859d40d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 259,
                "y": 192
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3e151824-51c4-4fb9-ac14-8b249b82f4d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 296,
                "y": 192
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d750a0dd-46ed-4a7d-a66f-5475038017ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 333,
                "y": 192
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "561f5806-ce8e-4d33-a371-b3c258e4046a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 370,
                "y": 192
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "031cb81e-ceef-4729-9ee0-becf4504a746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 1,
                "shift": 35,
                "w": 35,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "63967db3-f819-4358-915b-cb2e433aea4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 37,
                "y": 240
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "00e5cc06-2a6c-4e69-8b41-5b4e25aadd37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 74,
                "y": 240
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c85e05e0-3d2c-4ecb-b9da-edc707c2e9ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 111,
                "y": 240
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0029b240-eb5f-40b2-9e15-d8ccad3c24c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 148,
                "y": 240
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "619127e1-47e4-4e03-ae51-d5a5773ce1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": -2,
                "shift": 14,
                "w": 16,
                "x": 185,
                "y": 240
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3e166316-a490-443a-a6cb-09c7be7c145c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 222,
                "y": 240
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5b646f30-f344-48d7-8fb8-ca875b7e7eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 5,
                "shift": 23,
                "w": 15,
                "x": 259,
                "y": 240
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fb307a5a-bfbd-4d1e-b4b5-8ce2bd9fc9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 296,
                "y": 240
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9423c683-e54e-4b2f-aa9b-2e0fe06f6feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 8,
                "shift": 23,
                "w": 8,
                "x": 333,
                "y": 240
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6b73954b-8cec-4c16-be04-5f4ea8848dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 370,
                "y": 240
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "55e2e75f-a1d7-4e45-ba1b-4ff9b05620e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 288
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c066d79b-0f87-4e80-8d56-afbca1ad8375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 37,
                "y": 288
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2fbc1003-7e1a-4d84-931f-1e67ded4dfeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 74,
                "y": 288
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7e425164-f255-4a3a-942d-87431207db65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 111,
                "y": 288
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ec01631c-615f-4faf-b648-e3ececd66e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 148,
                "y": 288
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "698d88b5-3ce3-4630-980b-7e72225f150e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 185,
                "y": 288
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "81fff35f-03db-4519-b046-9c29a7f488aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 222,
                "y": 288
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b007a80a-b129-488e-89be-a087d1123ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 259,
                "y": 288
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "785a6269-1d3a-40a2-aa6c-903bd7894b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": -1,
                "shift": 15,
                "w": 12,
                "x": 296,
                "y": 288
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "55454ec9-baa9-49f3-97e5-4611835b02be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 333,
                "y": 288
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8a600036-10e0-4cbf-bfcd-e46400f87c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 4,
                "shift": 12,
                "w": 7,
                "x": 370,
                "y": 288
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "676de27e-691a-41c9-b739-28a71d8fc58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 0,
                "y": 336
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "753067c1-dcdf-4f66-ad90-2625c916e7a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 37,
                "y": 336
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d20de4c3-ca79-4097-845d-2fff9c18657b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 74,
                "y": 336
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "829eed90-5b36-4278-9d85-021bed379adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 111,
                "y": 336
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4fa6501b-205e-49c9-bc70-8af85faf95c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 148,
                "y": 336
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8e9f036b-0ad0-46ad-8278-74bfb8098249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 185,
                "y": 336
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ad484325-1056-4e16-b527-b9ac3205d72a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 222,
                "y": 336
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "51aeb7e1-2f48-477d-b838-15a24d273511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 259,
                "y": 336
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5c480858-461e-45dd-8274-4d25eeb87a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 296,
                "y": 336
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "843bfe30-5b4d-43e3-92a1-6fc5227e0716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 23,
                "x": 333,
                "y": 336
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "aaaeb65b-d95f-4cc7-bff6-94a5a0fe0c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": 0,
                "shift": 31,
                "w": 33,
                "x": 370,
                "y": 336
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2969dec3-7bfb-4126-9e6e-1950c77ef0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 0,
                "y": 384
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "85735a96-ccc3-40aa-ab5f-622a7b90ac41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 37,
                "y": 384
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5ea4e75a-fd2a-4eef-a1ce-3b6d055fd320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 74,
                "y": 384
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "dc633525-a45e-4e5c-892e-23f1ce486be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 111,
                "y": 384
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "96fe0789-35fe-4412-8e2e-acb6f20b6b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 10,
                "shift": 23,
                "w": 4,
                "x": 148,
                "y": 384
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f3d39d75-a293-4e5f-ae32-2a4fdbb3aa3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 185,
                "y": 384
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "67c98e4b-8d4a-4c00-b048-ffb85a89bdb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 222,
                "y": 384
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "dfdaac6a-de76-4950-93d8-f960f1547b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 48,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 296,
                "y": 384
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "de5f9ded-c7e6-40e7-b70f-0398d25d32f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 333,
                "y": 384
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "fe7bce17-ebd8-4e33-be2b-e0aa18631193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 370,
                "y": 384
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "387e2d32-0174-4b43-8a15-f23db9095abd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 48,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 0,
                "y": 432
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "2520238d-bb8b-4b2b-9863-8b505dfdf437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 48,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 37,
                "y": 432
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "abe63686-9abb-46e6-9f02-2a88b6495882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 48,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 74,
                "y": 432
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "209bd327-6f04-4ac7-b2fd-23942100f714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "59a75ea9-1f33-45ba-affd-9500ff7741f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "5613737e-9cb4-4736-83cd-264aaead46a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "1d0d6fb5-3595-4a7e-88bf-7d5173569355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "3d09627f-d214-4f5f-a74e-7801b525bbe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "d421bcc7-c469-4cfc-a1bb-f28a6729ff54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "30f1fdb1-e5f0-45b7-b11a-41c9f699fff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "13a1f374-ff15-4e7d-95fd-c3fd6005cfcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "3efdda50-245e-4b62-ae44-a676650f468c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "68a59ddc-a216-498f-85ab-979be831d5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "1fb6f847-9bc4-406e-bb74-df03c502b2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "b51d03bc-066a-4bec-b300-cb16c8d4d448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8965c2d0-52c7-4fe0-9746-9160de5c458a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "b8f2e22a-4a87-47e6-8d9f-a0e786ae06a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "4eb1b533-c465-4b02-8aa3-933d0bc8167a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "64e73e85-bc7c-41a6-b72c-d31ed425ef3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 97
        },
        {
            "id": "bbf0f90b-6816-45fb-80c1-afae0a519995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "909caffd-c272-40e3-8e41-ce9a6b0e8efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "0e51be6e-521e-4204-a96c-5f7b03a184f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 117
        },
        {
            "id": "830ec31c-98da-4e13-a468-08317c0494ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "26497643-a733-4d7a-a797-01dbf98fefbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "ba5797ac-c20d-4cc7-98e3-47cb678c1ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "6f7cfdbc-098a-4ccf-9d86-a145d3531333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "134dbb6a-1652-429c-9f12-b55af602d577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "29c129b0-5b4c-48f4-88be-142200eb5524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "95cf16c0-3887-48ab-9974-c71158cba9a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "3c96f322-f0fa-48ea-8011-cb0c85932701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "1282c3a2-a36c-43a4-87be-e34fe28aa0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "e22c90db-8dfa-4b7d-9daf-cc9e2321a312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "a0bdd168-f25b-42f4-a185-79dbe312579a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "126da0c2-3aac-4679-b23a-e630f4cc5dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "8aeb09c2-1ee2-435e-8c0d-f768ff188b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "621039e3-8140-4ec0-a727-9096d9805f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "819e06a4-edd7-4a18-8e97-c22d70ea10ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "b0de2f83-d5a9-47cf-9e8c-15aa88031171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "fa3e182e-735d-4b52-b473-8e10927f6b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "bc2ac19a-b991-4853-a4f4-0a2d413fd85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "ee553a40-fcff-4eba-ad41-d64c4cda7afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "5ccb0de5-07f1-461c-b4b6-40a527f3fee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "950564f4-bea2-4f62-8660-5f6a065bc018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "19d10cfb-c8bf-4c74-bd88-860a238509c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "e3ccfe2d-05b0-4e14-a853-a9b642a170c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "9ad01d9a-96dc-4d63-9167-3e5ef20f3b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "3e84828c-b2b5-4e0e-bb69-0ca8d94a63a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "35dfcbeb-ef48-4ced-a328-daa3c491a142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "d66f7cc4-ffb9-4849-a9c9-03a65ee3c298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "2ca9d45d-619a-4115-b312-f8234318f30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "1ef6190e-9f17-425b-94eb-e4823bbfb781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "acd3f89f-1c0f-4c1d-a497-1bff216de024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "19480f0f-4147-4beb-9a09-28b7d8c3ae9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 111
        },
        {
            "id": "2d0e5e71-891d-4003-b342-85b7259ac932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 114
        },
        {
            "id": "3523a0fd-4a9b-495f-a8a5-19e1bf853b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "5b2ded86-72a1-4305-9291-78c360e9508d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 117
        },
        {
            "id": "cc6d360b-3348-4668-a246-76bfa2118fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "38545fac-1e40-4721-a93e-76f7e3851d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "274616d3-00f0-4c19-9f1b-1cded266eeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "05e88ce5-e286-4035-a7e9-3f233c11801b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "cc4c4593-8fd2-4efa-b5ab-a2fec45db1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "c8bfcdc0-a0bd-4026-b493-8564ae778b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "c0b8696c-800a-4833-8df1-60616d2cee4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "ab13478f-ff61-4624-9e9c-aa3d715033c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "7c17b9d4-5bd5-4624-96c4-6586d3fe9dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "5abbca4e-3db6-4892-a50c-6a7ff8ad9ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "1a3e3fb4-d68d-4d9b-a501-c24b4e22651a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "c58af55a-35b8-462d-8922-a38524c0257b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "1dd951a8-15fa-401e-a624-87aac56fadfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "e9e8269b-6df8-4b97-a88b-5c45d527b6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "0a83fc0f-23f6-408c-acb2-2de786b4dcf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "a16be608-e9df-4220-b7e2-1f9837f8ca6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "c6671652-6e17-4acb-8896-59bff99017a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8208
        },
        {
            "id": "e47d90db-6144-4166-973f-59774c8137ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "772a233e-9815-4d2c-81d2-8b482c625d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "1df50536-a47f-4c71-a0f6-69935e3b1cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "3caf3124-fbc6-4238-9b9a-a4fd3c888ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "3e98ad3c-69f6-452b-9e08-7ace1a1616ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "16e577fc-554e-4067-a0dd-9f93d5b86da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "a044d8fb-0a6a-4bc0-9182-4b0cb13a7c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "10b4594b-44b1-43b4-b14b-cf40f9f39f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "303bb86b-cc02-43db-80c9-d69aa534e56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "cabc4c32-4ae2-4608-a72e-4170e0427c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d2da3e05-84f5-4378-a228-062a5aa35aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "315996c1-ecaa-491d-afb5-83cfc8b6c3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8208
        },
        {
            "id": "b358f686-a8b7-4a8c-89e5-369a2fe31135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "4d12697a-8719-4411-a2e5-4495cda5066e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "328e8a1b-838f-4ee1-a700-a602eb8a3039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "62da4d38-26fd-411b-b1c9-e8c9de203691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "419d040d-a40e-4e58-acac-42d1234587e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "37ae92c8-592c-44fc-a763-290f233fbaf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "65a2f4ba-aa55-4675-b2b5-14106297df4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "a0cad890-5784-4beb-8d3a-c93b63b0934f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "738bd6e3-8156-4871-8c52-cd5b8c59f273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "2d534a99-a693-4b7b-aafa-73a753538e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "946faa2f-1d3b-425f-a389-8e57df1b95bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "a6cd0681-81ce-42c6-a87b-c7a14d7f21dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "dda2f27f-6aff-44ad-966a-137591d2d9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "346aaa9f-8cda-4e56-a7a0-8e48c9b136f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "6fc82cdf-1299-4243-b585-db82700c5ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "dd4921cb-c69d-4f1d-b4fa-5583335ca5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8208
        },
        {
            "id": "0c7e0aa9-0e46-40ab-ae33-1847c6807995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "8a83bb06-4909-423c-aef2-b3d0a7b0ec67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 97
        },
        {
            "id": "bece7715-c610-46a6-b400-a5e644631512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 44
        },
        {
            "id": "6961e819-bcf5-4f01-b70e-c58dd115b37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 46
        },
        {
            "id": "1024bb3a-56b2-4446-a0a7-6abd45b82db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "898f8443-0a74-49a9-8fcd-322a873b398d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "4a8a2e22-272c-4448-9eaf-277b54b7051c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "d94c717f-9b30-4da4-8cde-e82b6324c221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "6a68a76c-8fa0-47f2-8fd8-c77797fd369a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "b3650a94-8a1e-4bf6-8cf3-f729ee7e9cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "f7a73b7a-5488-4553-83d5-ec26d8233202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "a59ca317-4019-426f-a716-eed12ef14fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        },
        {
            "id": "d2ab6217-fcac-423e-a2b7-1e1adc37d79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "bc188602-c1f9-4e5f-bc0d-1cef3ca9b1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "e2563b30-a850-47da-847f-09e15b55152d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8217,
            "second": 115
        },
        {
            "id": "eed6ca4a-34dc-4a8e-be55-4356f35cf7fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}