{
    "id": "ab8b380d-a111-41f1-bfc3-eaf6e7aa8051",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_bold",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9509d8fa-60e9-459e-9cc3-2f2bc1511bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ed37d381-d542-49b6-bbde-57dfeb629da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 24,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "db7d4d15-200d-4f56-b859-60b4807a632e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 48,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "40b41bb8-08da-4651-b29a-868e0c9c1d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 72,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "189eb72b-c6a1-4004-8e04-6c414dea4fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 96,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9f20f2ea-9c45-43e4-b6a7-ac9f4ce469a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 120,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "642dd892-7cbb-4af0-b636-40a7bb2df42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 144,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7d3b595e-8fc9-4aeb-a7d6-cdc3af421075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 168,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "888e25fa-265a-4cc6-857d-515eff6a88fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 192,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "52758aba-241a-4af4-9658-805c53bbf9b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 216,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1887b5ab-11dd-4328-9110-d06a381c94d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 240,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2c8c7f13-bfe9-45da-a5cd-e56865d6260a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 0,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "16d2bd7a-4537-4dfd-888d-ed896552e755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 30
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cab3090b-cf00-4991-b8c4-322dedc4dbf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 48,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c428a247-f7d8-40db-a693-ed47bfd22709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f7edce6c-722f-4d2d-adaf-26d85ef3c0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 96,
                "y": 30
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "58e8cda1-013f-471c-aebb-85e931fe361d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 120,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ad31e7ae-6215-4f29-a131-d80330acf50e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 144,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "197fd703-c70e-437a-953c-7e406eef9561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f9027226-e8ec-4d20-b647-4b1ecb71bcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 192,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f7469d30-b4e1-4b10-b9bf-c3a549ad8774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 216,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "92dadc2d-201c-4317-a724-3e8d4a4db3e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 240,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ce7e2285-4ba2-479a-a909-7866e79d4ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 0,
                "y": 60
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8d971ab0-c6a0-47db-8a80-de55f8b8adb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 24,
                "y": 60
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b42605ec-cfa8-4d4f-915e-06cdc6022692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 48,
                "y": 60
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3d29a39b-fba9-4bdd-b3f9-6b25774398c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 72,
                "y": 60
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "600da926-6ec3-4a59-a3d1-df2a2c6a8a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 96,
                "y": 60
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cd1357a9-3a2e-4191-8713-7e8d11311549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 120,
                "y": 60
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8eb1c625-3310-42da-8b22-589ac4873833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 144,
                "y": 60
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "479ed0b2-2bb2-4b3a-b1f8-10d93225ccd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 168,
                "y": 60
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4e3851ed-6857-48af-a698-6e40f547a893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 192,
                "y": 60
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c23dab82-76a8-4db7-b6e3-93bcd4013584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 216,
                "y": 60
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "39c07562-101c-44ea-86b1-ea5dc0978945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 240,
                "y": 60
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2facf0e1-683e-4399-998b-2c8c9572ad9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 0,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b43336b8-c189-470d-8ff6-07648f1ae65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 24,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "463344a7-c021-4eec-9840-b1a58bcd88a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "24a2ec38-dc29-43d2-8b54-b47a0a79b82f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 72,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a4e7d5f0-e6d0-466c-be44-d01986124025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 96,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3c67c655-783a-4167-a260-e9d315701320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 120,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "14ecbd36-540f-4eb7-83dc-62b1765e12bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 144,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "916b7a17-0777-40ba-a9cc-fc87cbaa2123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 168,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "aa64038b-18d0-4de5-a39a-058f76c1704a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 192,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a72fc16e-4622-47dc-a2db-34f790758019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 216,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "613900b3-4652-4cc8-bcf9-5ddfe7b2d603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 240,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2999d21d-5ccf-4b46-9166-7b2c5db08767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 0,
                "y": 120
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1c9b3c99-e8ed-4e6e-adf2-7de34989e7f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 24,
                "y": 120
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b3bb15fd-4d13-48bc-a18a-9ced76e43e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 48,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2755d2bd-d3f8-499f-987c-fbc25953d983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 72,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e173c298-b746-430c-a87c-4a7e3fb0c451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 96,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c10f9714-53be-4073-8554-bfbf85832b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 120,
                "y": 120
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "32105bea-2f37-4443-8f37-8a224b2a48e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 144,
                "y": 120
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3e8c4ffe-11ac-459f-b709-10d47687704d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 120
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c9879c69-a76d-4341-8ab2-958b48d3c22e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 192,
                "y": 120
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "399c8743-7dbf-450d-95f9-6afe45bb4347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 216,
                "y": 120
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3e2ea45c-d241-4d70-9960-e47cccad3941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 240,
                "y": 120
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3f0a488e-b8e0-429c-9c89-e7a9e2b1e45b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 0,
                "y": 150
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e03094fa-9ad8-45e4-b830-b7c5b2f6c597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 24,
                "y": 150
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b99ef87e-ea05-4aff-9bf8-ad4e054e8cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 48,
                "y": 150
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "35f60c73-54ae-4f45-989d-8c172f351610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 72,
                "y": 150
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "646f3d26-c716-40cf-bd05-56b8af7368fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 96,
                "y": 150
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "16cc3e32-c859-46c4-b280-e04f995878a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 120,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d3447344-5af8-43e8-8b34-1a8498a57f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 144,
                "y": 150
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "88786fb6-511c-4b75-84eb-4b95189a2842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 168,
                "y": 150
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1085c6b7-56eb-4d32-aeec-049055cf422b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 192,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "62a155bf-8979-4e3f-8506-efdc5955d87d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 216,
                "y": 150
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5be7d6ad-6d66-4725-959e-844257bb1d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d1ff3dac-8cc9-4e79-8a68-875f6f73e40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 180
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3202d01c-94ea-4d57-ac46-df4caaeab9ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 24,
                "y": 180
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "78836783-6f21-4ce7-8d52-6f8efe902403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 48,
                "y": 180
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ed2b0579-822f-4b47-abcb-47131281a38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 180
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ca981df9-a414-4369-94e3-9624275dcb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 180
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9357a763-0675-4f63-9f58-c6936f7a7df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 120,
                "y": 180
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9231dff6-c829-4c2a-b5a7-a414dfaeadf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 144,
                "y": 180
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9d3d67b7-7fd2-4d8e-b303-0a6c3f9abaff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 168,
                "y": 180
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "09829295-978f-4b4a-b6d8-328cf77431ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 192,
                "y": 180
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a6d5f3e5-faf1-4f40-81fe-667d056d0ad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 216,
                "y": 180
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f9fab313-ad45-4d93-8ba7-bf7aa334bc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 240,
                "y": 180
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7c3fc64d-b5b2-4fce-b9ac-595ab2303aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 0,
                "y": 210
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1f3f6d75-a03b-4704-b958-fe19d10d2a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 24,
                "y": 210
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a6ed3547-6f5b-48c2-8118-fc51f7b3d5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 48,
                "y": 210
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "17fc07d3-db92-4767-afcf-60f2df0bcfe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 210
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "474057e9-224e-4e63-9916-d465e3c77b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 96,
                "y": 210
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e4c2019f-566e-4686-9b60-6793b5cfe50b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 210
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8e3aa46d-166b-4e67-8b8b-5161a7d0966b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 144,
                "y": 210
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "146e48a4-2e69-4fbd-a6a9-a0d7507c9338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 168,
                "y": 210
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a1a7909d-2bf2-4d7f-ad46-02363d9d4d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 192,
                "y": 210
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8fac88c7-598c-4b8d-b1f5-4fb1b34a6052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 216,
                "y": 210
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c3e423c9-7d5e-4026-928f-3d3bbc0efed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 240,
                "y": 210
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3ae8b823-315b-446e-9605-9c646447d222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b3716913-542d-49ef-818a-b8103921112d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 24,
                "y": 240
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a82b4c51-7de2-4642-b599-b663786f06a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 48,
                "y": 240
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e1c0bd2d-76e0-4f5f-a301-50706d26f151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 72,
                "y": 240
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "382a16e2-0992-480a-9623-ff4e3c7bce93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 6,
                "shift": 14,
                "w": 2,
                "x": 96,
                "y": 240
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d6306888-f853-4c2b-afda-1333c48f2625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 240
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "05cc9bef-27aa-4e61-b409-181139004cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 144,
                "y": 240
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "2cface4a-8633-4783-a352-490169f9a0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 192,
                "y": 240
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "137e318e-70d0-4317-bc41-d0380ac939d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 216,
                "y": 240
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "cf14d7b8-2165-4e35-81bd-985d22c6cfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 240,
                "y": 240
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "5b0a55ee-5e52-49f5-bf17-340c7f5a9ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 0,
                "y": 270
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "6c4aa19d-b896-4661-972f-3bbc985cf606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 24,
                "y": 270
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "9a6ced1b-1e2b-4925-b589-28eca049aa68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 48,
                "y": 270
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b6529152-d42f-4122-b39f-98f5e3732063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "3f8cfdb6-07d1-478d-8de8-1ce83056bade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "1a07f7df-81a9-476f-9009-0b8ef8edec17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "7d0ac995-af61-4dd0-87fd-d9a9874d28e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "444fd0c8-76e6-434f-851d-7be6bfe36e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "bc5a4c20-edef-4a1a-be4a-aa6897cb42a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "4556e28d-95bf-412b-b7be-e9e9a810db87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "e6c5cc91-2c8c-4d18-89a1-572efb51c0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "a5f7bcea-9e69-4c71-994a-b4dfebd6e679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "469cd4b8-3200-4294-bae9-c9adc2436914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "602b9868-5a5e-4f1e-b8f2-1edaad309888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "5e9edb2c-6542-4689-8583-d15ae18961c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "3be18ac8-5724-4421-84b8-5ae63689092d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "776ceb1e-506e-43b8-a8dd-abd0eb54a543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "7fc1ce2c-cd30-49fa-8e2d-54d912644e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "9ddac95e-fc98-4cfc-a886-9c765327213f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "20b1327b-72f7-4dfc-bc7e-4f1587ba227b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "cbebebaf-f3ed-48ef-8dcd-90f752bc9152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "1ebd4261-8507-435e-8096-3e6954fb835b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "8d34c648-dbf9-4451-a5bf-9f61510dd1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "4d0cc3d2-88eb-4f2a-9d32-730e1be81789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "62137c92-273d-4e48-bc12-2b6d9268dced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "07bb1b04-6152-4175-94cf-bc3f704ba859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "d8fbf158-405a-4d0c-9db8-90b30a38cfef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "2fe9cdb0-ca0e-403a-a637-e326b6e3f685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "ba298023-570a-49fd-bd3c-f05d9e002aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "d219b5dc-60c2-4da4-80d2-3e9d5bf20206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "f906a933-ec6d-419c-ab0a-1d64c66e4986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "026ae899-41b0-4616-bcb5-0397926c5b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "74d5fb57-a7b8-4c99-bb13-265078c2832e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "cf4cebe7-6214-403f-9403-00e8ec3bd2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "fd01da1b-d9fa-46a7-bc39-ff5b9dcad972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "d07321ea-560b-46ff-b7bc-39581d56e356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "502fddcc-d1ff-4aab-885b-f00592d358d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "304cbed6-cea6-495e-9ee4-66b7d1befbd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "efa2c621-deef-42a3-8cfa-cb94120b27bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "0381da66-1576-40a3-93ea-473a6b756748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "f7a62d0d-eeca-4ef4-86e1-04898ef91de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "e24f6b2f-68d0-4966-b5d9-c475533e1fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "c039706c-1658-42e8-bbee-8f6f87e49306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "6e1e7b97-2b96-41f4-a922-89f92eb3c769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "d62ba2db-566b-4d4f-a3c2-41119e461a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "f5eb3285-fa94-485c-a223-d2a89f8046a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "ea27de1c-8fcd-4f37-aea4-72ff9667d931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "446c84e0-0c5d-4804-9924-4a3d066053cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "ddbb7a97-93d3-4430-bf7d-8b860de7d09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "6a427886-b782-42cb-9108-2d8c4d7077d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "00f3d1bf-6ab9-4f48-b6a8-960050f6ea24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "e7479233-3f42-469c-9b62-dc4ed0ef34bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "39e5f537-e2ab-4b3c-a4bf-cb8f99a04d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "6bad9b92-c7ca-4dcf-ac88-794ac4afe603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "572e2c60-e47b-489c-9855-42b568379e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "2530c16f-4d0b-420f-8127-9d561f31abe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "7a00a686-69c4-4132-b273-69861de0b20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "ab8bfbe4-9e1c-4486-830b-03af9a5924a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "3de6ffdc-5b7a-44ac-8ba9-feb0a79c1b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "07420f4f-6b40-4509-9628-a7a014a25fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "22ab1f09-70c4-41be-af13-41d50db1479f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "cedb6297-8aea-4d0c-8e0c-472deaec87cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "f71c8e97-bee8-4201-9ba3-a0a949a0403d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "ee034f9c-a684-4036-9e2f-4743e8dc3b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "55293692-9a6e-4054-b4a6-52474a44bab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "48e1250c-100e-4372-a691-c85e18deb488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "2b5a5f7a-beb8-422b-8ecf-0dd8970932fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "181d95a8-299f-4f30-af7a-238f9c26a2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "628fa8c3-94ff-43fb-9360-b4ee17976035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "dcfde4aa-f23f-47a6-aba6-b5e7cf9e675b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "489f8d9e-85bb-4054-86f2-55f44e597a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "57343c64-e74e-4928-8a4a-4b47cf4a6a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "7f4b927b-d4c2-46ae-ba6c-11949a9020db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "d24fbc26-7482-440b-b966-5139cfcaa54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "fedc3587-711a-4447-b7c0-ecac718d19e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "bd19386b-953a-4f82-9237-940bc03be212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "cd9760bf-2b1b-4780-8d11-45d659c91cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "ef8d3783-657c-4c9b-b45b-d7c2e0a39d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "8308d63d-4e09-4dc5-af02-16f8395378db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "0a32e3d3-80ce-4cc1-a5a1-20bf380a6688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "82059c00-9a44-4543-8bbf-898e809373c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "b92cacd3-6b43-4bf3-8fe9-b3e180aa7e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "924ba3a5-5773-40e0-8a18-fd77b1548eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "1aa9e817-f557-4135-b086-2fd564e69e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8208
        },
        {
            "id": "6db75bb0-a219-470f-87c2-d69755685950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "f0548912-1573-445e-a82f-8ac316ac7cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 97
        },
        {
            "id": "8eb92f76-fe08-4f90-87ef-04ec8f6ba83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "f6c4c60d-c599-4999-80cb-4808e7079257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "4cd6d016-4fd4-496c-8d56-1becbeefd722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "ce46fd45-a035-493e-8017-a814ff3285d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "33aa65c5-ad05-4648-81aa-07fb6e814e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "8a4711c7-2913-4410-9081-4f9ab830eae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "160d5d53-e989-4abd-a711-eae85d15145c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "8e9ad19c-e535-4a6a-953b-4701330f7578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "7da387f0-61d6-40a5-9da2-97bc9dc980f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "b9424ade-3ecd-4b2f-897a-ee1f51d5fdfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "9c5cd8ec-ff5c-4966-8cdb-d4cd3c7240b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "eff49be0-6d85-4a49-a65f-c9d71c25a4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "303b79e0-3ffb-4932-a228-5d05d67dfdf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "f78003e0-7a56-40ea-9793-d103f05ab462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}