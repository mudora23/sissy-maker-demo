{
    "id": "2f4e03c4-132d-44e2-b26d-bedbffd96956",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_13b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b3226d34-0651-4f86-b8a1-83fbf11bcc7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 21,
                "y": 68
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b149bd76-044e-4dd0-9d19-14da91a10067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 167,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "28f7569c-e1f9-472c-b420-2f4943b6dce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 201,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7ae93fb2-7f8a-4f8f-9239-d6fc6383ae34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d89f1ae5-6dd4-4163-b9bb-a23aba37917b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ae9788b5-ebc0-46e6-bea7-fe3cb02a4770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a2c9ea5c-e47b-4f75-b000-db71d02fae87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "08706d3d-842c-4541-b306-f763111dca12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 229,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cbd1b9bf-6c50-4aa4-a0a8-0e634fb3a699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 83,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8d959641-29e0-4eb8-8888-a33570f06263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 76,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "61fd02e1-7bb5-49af-b90a-5b5092d7e779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 159,
                "y": 68
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "17df5d51-c8ff-474d-b6e5-d602f4011393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 221,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ea96ee08-1fc0-4b98-82fb-c5cc2a6d2068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 133,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "06a08ea7-ddd6-416f-9793-3a53ade3ed37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 177,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9ea11483-8a02-47b4-9ed1-5f729dbb82dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 212,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "065941aa-d639-417d-b042-25083ec1c90e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9fe61548-9042-4ceb-8ef9-653a0fe17c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 215,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bec76438-3397-4a1e-b1ba-cbb4d0923a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 114,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fff2bf15-0878-4723-9a95-ee9237a45eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 71,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d225ee70-d5b2-431b-93d7-2089084937b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 81,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "90411cf7-e0f4-4e3c-a20f-24521f930c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 91,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3e23e8b1-1a08-4811-8783-1ad7cf3938a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 111,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d1f03642-0e1d-4457-ab42-8778e4b61af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 226,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0ba12dc8-a5a7-434a-b1c0-b6f341074c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "216ecd99-a543-427b-9096-a375267658d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 191,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b99f02cc-b494-4cad-936e-2226828ddf2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 24
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ecda33a7-9c30-4e6d-a3a6-826ef1dbd784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 208,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "37553b51-4e7c-417e-a8fa-65a3d1c07826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 138,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a4a8bc0c-eb42-4c5e-b7bc-f8c3c824f21f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 106,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "65d76af7-3be1-470a-a2c0-803d2537773a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 97,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4c9ebc40-777e-411e-acd4-b1467588574a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 28,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "36bb11d3-01b7-4ffd-a26a-d90feefbde9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 37,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "68967911-5c79-4696-974b-1934197a1fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e36aba8a-5af2-45b1-a3de-70a3ec25556c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1d53038f-8caa-490b-ba74-ea29275b0f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 171,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "40e7f06a-3ec8-454c-b03e-b83e502f6523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "94ebb744-f7aa-4379-81ef-8ae7c183be0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "53441cff-805f-4f0d-99d5-be1bf5f7f4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 61,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2977ae25-cc47-44f1-8b7b-b91e574e1d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "df37bb53-3bf0-4e55-819b-76c9f5276811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 24
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "950e4217-8215-4189-9bc8-4c69b885d7f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8f41fda2-5cfd-47e7-8849-6d47d072e29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 172,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f6451085-e2a3-4916-8aeb-3f7c3ebfb826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 181,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "374d0698-2948-46c9-969a-183843161f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a3e08556-d444-4cf4-ac06-e0eaf6778fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 211,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "75ea9d28-6401-4dcc-ad83-45322308ba1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "80e62907-affb-4760-9cfa-4045f1142e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 127,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5293a3f6-eda5-444a-b3a8-b99cacba10e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c164fad2-6b8e-4d3f-8927-f0cef6f5ea3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a2c9a98a-7b19-404b-bdd0-d5bdee1b3295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d01efd48-2667-41c6-a52d-4c59287c5b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "245b26dd-e813-4b30-9dd4-013691bcb0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 151,
                "y": 46
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "14f924db-5a1c-4b76-aa27-969dca6379c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 94,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7bff2b4f-2402-428a-aea0-dba5c49102a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f681769c-b3a8-4740-8777-335bb3afe8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "60d3536e-dccd-4d41-b31e-da0973088255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c6eb560a-c9ee-4fc1-8b02-eacb7a7b36f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "837c7d23-1991-41d6-8ce2-456cb537a48f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2dd5a3c5-3c97-4e41-9984-323670240e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "362813fb-a7a3-4527-9e2e-06b0008db228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 90,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d9805a14-28d2-48ee-84f0-f1bb95ce0cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 239,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "31912c27-fa2f-4131-a4f6-a7b08c83661b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 69,
                "y": 68
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2951ee3e-6856-4242-b1e9-495fe3e3a48c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 189,
                "y": 68
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e2a5d8bb-35db-4400-9ec6-e18918449e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7fb07521-4294-47ef-8684-0abaff7190d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 6,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 224,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f9833ad8-e41e-4e5e-9306-a2bd2eb7c33e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "37ff6fa0-844d-4cb1-9f26-b9f13f99eda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 193,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4cc21efe-62fa-4123-9347-1708b8308e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 131,
                "y": 46
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7d501196-3d22-42d0-8cf4-aa3466ea6434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3a5dd6c6-4650-44eb-9a4f-45607c978b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f234d175-701a-4c4e-9efd-249242f579d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "86aff454-9437-4cd0-af9d-a4478b829573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1f0873d7-5c13-4d7c-b725-ccc2c0cec543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a64332be-7c30-4f86-9c6c-dd012f4078a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 127,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "695e2363-077e-4004-93d0-74bc21bcd1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 231,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ee36dc6c-844b-4897-b0f9-49ed2ce2ae58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 116,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "73e28ebf-dd3e-47df-a453-84aaaeab9dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 121,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "baf15f7d-5076-488b-b9f8-086573d9b6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "173d8c9c-4c83-4efc-bc5a-14a196696a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 138,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "10cf34fd-9f48-4b23-81a9-d1c0c793f07c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a557027b-8019-4327-a28f-e93725e34374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4c542e7d-2892-49e4-afa4-e0d4d4fd973d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "454e716b-41e4-4935-a6f6-5ca7d9c0cc80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 53,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "333269f3-268e-4fe8-8b91-95ce1e5e3f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 45,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "84e9200d-e7b4-4a4b-ab55-a8924266bcff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 61,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "04e8f1a1-6f93-4d48-8781-b331a0d70aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "177327d9-29d4-4f8f-b59d-1e0619150d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f61bddfa-f227-42d0-a8ea-1e1e5b967cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a1fa6f3d-b403-413b-9993-befd14006630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 171,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8da289d8-010c-4574-b9b3-8cf10436ca46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7354630f-071c-4e0f-833e-38e7ee2f10e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "66c6b448-ac18-4e11-af63-c569564e30d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "52f4cfa9-bc67-4860-b4b6-79498d3ca319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 197,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "11e2c781-1c4b-4731-adab-e9defb34379d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e9e30c78-561f-4999-b760-d6f349565a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 11,
                "y": 68
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "f4374cb1-9286-4222-95b1-aa8d9b45ca2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 68
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "42f80ae7-9e4a-4ff5-b6f2-05d51fb1d8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 216,
                "y": 68
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "11568d72-5bf9-49a4-8729-e368181c342c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 9,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 220,
                "y": 68
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "4a0709b1-3520-43ed-bf6c-d9585e8d223c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 9,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 151,
                "y": 68
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "a791edbc-d16a-47f4-8dc7-6e6f37f3a755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 9,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "647d8478-80cc-4028-bb85-92480595f95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 24
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3dadb910-edcd-48c5-bf5b-30981ac3d0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "9a506bce-3de9-448b-9eac-d2235331da31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "1eefa2f8-e4e3-4bf1-a3ee-37746b901f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "1023f16c-e9e1-4dc7-b277-54b6bfbcc395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2fb370fd-be67-4782-a5ca-52d68fad5264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "cbbd9154-a5f1-4a6b-b3b7-7bf8914322b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "ccaabd08-4f2b-48b8-a4f4-ef049245e39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "59d083b0-eb30-4d3a-98f8-8d18bf5a83ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "86964ece-12b8-4c6e-a719-c6eb5f4362a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "ce6e0534-9554-4e54-8e16-8b71756a7d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "23566286-b6da-4b96-818a-c8f87b9ac3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "14681c5c-9669-42f0-8d3b-7ad37f910d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "3f8c0af7-017d-4fe9-b171-08e43bb2eb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "31fb5ad1-ce55-48d2-8f7d-0232f33c4a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "fe67baa1-e059-4eb3-b832-fb27f6d7d109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "6bebacfe-b89d-4c7e-aeba-122d60111a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "77f75296-8ab0-4c41-8d3f-59a745dd08aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "ad597463-1997-41ea-84b6-be23a428d210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "36d73342-37c2-409e-9731-14530a6ac327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "1467d8d9-f034-4040-801c-7463bc09fb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "58432df7-082f-437c-aede-b5c39d169bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "811a1c9e-4c19-4504-ae06-fee123dbe5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "a1e9554e-49e8-46e6-a52b-2b5cf85a7475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "0ba884f8-810a-483b-b0d9-7d61f717e3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "678fbe27-e9a8-46ee-8012-80602f356e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "b87e9813-389b-4f51-b08e-8f7b23c8fe37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "7f8683bc-a81f-4721-8383-db42fd3368f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a8abb84d-c01e-4c32-b44d-310204ecaaa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "990f8efa-7b91-4647-977c-99d2bd56fa67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "351c621a-e494-4a1f-b2cf-3a0432fd5040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "b3914347-d47c-4d5c-9fd3-79689b511772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "93a0558c-c757-4e19-ab6c-a70350aec1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "d4daca1f-38c4-4d99-a5df-29e47fe2bfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "7a4752c0-2c64-4331-94be-353214b115ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "ebd45350-55a3-4aba-a3a7-5c1e22a7ae8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "1032f715-8150-47ca-86c5-3ef77479c79e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "8cbde9cc-2f23-442a-83c7-ca59a861546e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "c20d7126-76c4-4709-9229-2d6dd4925151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "08c3d2df-0c7f-466b-9bf2-2e99fe5745a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "d7f567ae-addb-40b7-b1e1-277fd8741a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b0fdf6a2-e378-48e6-b69d-5653826119ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "439d155c-b637-424b-a21d-67abae3cd368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "310686a5-7258-4268-895b-0000a4171c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "110fd73b-2a15-48d4-9cd4-65d279633967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "c3529a4e-123b-49e3-8c91-1f44d834962a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "a373aa42-3fb2-4b07-a73d-07902b40ad3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "f035158e-5ec8-4fcb-a2b6-5eb4d556fdd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "c9a6865e-bef6-454a-8dbc-6381a1c977a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "9247aea4-a6d4-4cf2-825e-07f087f03ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "127bc67f-d600-4745-b4f8-091067a24e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "62388def-34f5-429b-b661-f726c38fb85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "0444e4e3-34d7-4993-ad77-e008b423ea53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "543274fe-0b8f-42d1-9f06-164bccb97622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "c2d2c4ce-cbf7-4173-ac60-f2a586547f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "cd2d0139-5151-4c8e-8eaf-1ba6911df830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "c9a59cbf-28e3-4522-bf79-de894cc9d2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "8191b488-f9ad-4069-a3ed-40a3da19b5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "a939bf1d-b192-4da6-9047-adf47e56f916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "13bca226-9731-4d55-870c-f369688bb006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "877c8ffd-900a-44c4-ad8a-eddd90ec8efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "f403a8d3-5397-413e-9914-69b9cb092cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "ed33df4b-b584-4e97-a4e7-266c6ccf2bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "7e3f42e1-ec4e-47db-8004-3458e6347485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "663ec2e9-7deb-4e60-8443-06ec7cf7261c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "60a3c7cd-2cbf-4f0e-9693-b7dc03174ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "13905c35-c472-4cbe-8089-efe4ee00a63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "0ccece0e-616e-4ea7-a44c-71ffed22c88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "a20e13b4-8206-4126-afc3-f7e4296d9e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "24d2843d-e308-4f50-ac65-7a21b0aa4355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "f50c99a8-a31d-416c-bfb0-636f6a73a17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "75252f40-2b08-49d6-b81f-c3c60fc66672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "0f53c7c6-83df-4958-8fda-62b4813da44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "aafe7443-c062-487d-a9b0-527d9c4bec8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "85356725-a01c-4744-8691-798f8aaee773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "b3ab893a-dfd4-40a3-8aac-3c457885f2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "c5979f3e-a6ae-4567-b338-3ee5746ebb03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "ec59d9e9-a423-4219-b891-b7571fdac053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f3c81b68-9312-4d55-8ec3-af57e62d4ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "5a5cc492-9316-404f-9fdb-c6f806784ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "094225d4-3796-49f3-8880-e5f192caccfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 115
        },
        {
            "id": "0e16b722-7366-48b0-97b8-366b1e802a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 13,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}