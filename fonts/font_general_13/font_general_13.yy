{
    "id": "b7e01ba4-7cb6-4958-9cd8-73d773d35600",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_13",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8a82bc21-cfd2-4331-8525-7382d1f26f9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "32a66867-f288-4c57-822e-c76157e5681f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 17,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "48f4c661-002c-4bc2-8265-1b099e6ead82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "490db3df-fffe-4937-963f-0f659d78d00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "be35424e-9925-4711-8aab-4f406f3e2131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "89ae8117-b132-41a1-ac8a-e5c837b9bed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 85,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "36029595-1c65-45f1-9fa8-37cc31ee82f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 102,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "302b0ddf-3a62-4206-9991-7593b8610f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 119,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fa60c613-dbeb-4915-a43e-e5ab616feebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 136,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6fb7f99b-2808-4276-9898-1a930e54c97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 153,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8e638d88-a1f2-4e84-8fe3-a05e04683fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0bc9a44a-7151-474b-af2a-b2880798288e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 0,
                "y": 22
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "76f2365b-bbd4-4132-bcf0-1438509b81da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 17,
                "y": 22
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5c5ab877-78d5-429f-b653-d17b359490f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "97a21305-06c3-4a40-92f8-3444e28c596c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 51,
                "y": 22
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "340d6c7d-7da0-44a1-b2a6-7b86c6a5d6ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 22
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4af4e6c3-bbe4-4194-88bd-4a2e5cc03f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 85,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ccad2698-0fd8-4869-aabe-b43e36ee8d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 102,
                "y": 22
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "73f18e55-f9a7-4187-9e47-14b67f942326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8420e0b6-ff6a-4aa6-8eb6-e2fe7aa1fea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 136,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6460d184-e602-4a1d-887a-9b637a0821ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 153,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e5703b02-802c-4630-a607-dd07d0350b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 170,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5045e4f7-29ef-4030-8287-30e170b7628b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 0,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8b4c96c2-627c-4f41-81d7-6d8406d1b078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 17,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "da80499a-62ed-4848-b30a-d781d9b59e7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bb663638-2167-440e-9cb8-c1f31f181c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "71b18d2e-30e5-497e-8f75-c0a94085b37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1b23a3ec-d42c-452d-89d4-a6b29893e025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b91a8369-1407-4058-b2cf-bcbf516df85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "940eff9c-b5b8-4ea7-a9a6-c4c6163df0d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e3cc2a9f-df91-490a-b2b9-3bb939bde755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 136,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d1340bed-a201-4d1b-972e-4528768c93a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "94bcfe7d-4e94-4c21-ba4b-90dbbba29a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 170,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c0ee2bdc-339c-46ae-8948-1e41d7e3554b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 0,
                "y": 66
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6175a8f6-9621-48bc-a3e9-73a8f71019b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 17,
                "y": 66
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4f8cd00f-9346-47e4-8a60-27f7b47510ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8343790f-21f8-4098-acfa-1f7e62255f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 51,
                "y": 66
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "35ad9aa0-afa1-4a8c-b77b-970cb1684052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 66
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fa1f1f5a-77ba-4c2b-9df4-c35625e9c4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 66
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bdc9d647-321f-4fe2-815b-c79482e3c21a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 66
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5f3770d0-8513-42f8-b72b-0e713a213368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 119,
                "y": 66
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4c3d9a04-edbc-4b53-98be-276390b4fe98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 136,
                "y": 66
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bf626c56-0528-4961-9655-4181c9af9fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 66
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "15ced383-e1e1-4299-a30f-d0d41dc26a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 66
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fb3089e0-b905-438a-9ec5-641416ac0c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 0,
                "y": 88
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "87db3da1-5758-4047-b880-df63b9352de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 88
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e30e0e94-e6d2-47a9-8078-f86b52bb2cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 34,
                "y": 88
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cf82f13b-4b5b-4bf2-91e5-0433ee2654f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 51,
                "y": 88
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "359ead30-0064-4764-9e09-f646f7a651ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 88
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "59daff44-c52c-4ac3-b909-dabd9ba78e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 85,
                "y": 88
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5fad0de4-aca5-4152-b3af-e5db70944cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 102,
                "y": 88
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7d24746d-e696-4953-8ac6-e5f0b6b2efb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 119,
                "y": 88
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5d4b5419-5a0c-4e1a-9f0b-ed72a19e36d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 136,
                "y": 88
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a368231c-7d9e-4860-8e37-97a223ad7851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 153,
                "y": 88
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4c09f5be-d8cd-4237-8a79-f5ebef49f11c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 88
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a5b27aad-7e66-41da-b6ca-3788fd323f2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 0,
                "y": 110
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fe832e20-b88e-46f8-95d8-f3ea532c5a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 17,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "19b9b016-f06b-457d-ae4e-28d4d47dbc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d1457d0c-dc6a-42d3-bbe8-d1fcd20bffb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 110
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ce017764-a220-43b7-8da8-feae404921fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 68,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1a23b80d-a44f-475e-a6bc-09c68e4875c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 85,
                "y": 110
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7ce771e0-191d-4d88-bef0-f7b70aff18ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 102,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0c38a0dc-7302-442c-b565-66b5751793f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "67f5bac9-99ef-4597-8921-c9500dd6320c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 110
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7183f38c-27d8-45d0-aeed-e4fe4bfbd8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 153,
                "y": 110
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0c8d4e60-3a03-4bf4-a60c-b9010184cd4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 170,
                "y": 110
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1055c93b-e854-4892-8204-40d902dd9e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 0,
                "y": 132
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "67b953c8-c173-4e4c-bc0e-2aa44c5ec460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 17,
                "y": 132
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a1614a5e-d513-47ba-81bd-c7533e8e88d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 34,
                "y": 132
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e5e4937d-df50-495b-8d08-cc6018ee08c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 132
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "927db10a-3c16-460e-9fb3-bf08d32c238a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 68,
                "y": 132
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2e71c547-de95-4771-946b-38d03d941a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 132
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f7b3f1b8-68ee-4f40-a737-985fed7d5ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 132
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "797b1c3a-d82b-4e25-adcd-e75852feb562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 119,
                "y": 132
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6880ebdf-bacb-4305-be74-dbc473dc0b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 136,
                "y": 132
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9afc5fae-7b06-48be-8fea-d22b2f2c7cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 153,
                "y": 132
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f1b36102-a5c7-46da-885f-d1796daa57ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 170,
                "y": 132
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3a700570-dd68-440a-84df-b544f1f59045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 154
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "89940d64-84fa-4507-93dc-570a0c4d4d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 17,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fc9d7e71-0532-471c-bbdc-8ef8d7035ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 34,
                "y": 154
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4abb2099-97a5-49a8-a1f6-814f9fd80811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 154
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "951db0a2-212d-4833-8a6b-f686d3869c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 68,
                "y": 154
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "49c097e6-ea80-4a95-ad6b-fc4c520dbf18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5f32835e-6dfe-468d-957a-1db60beafca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 102,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c30a2185-99de-4a3f-a155-02e921080fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 119,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f9f6c4c-c8fe-4731-86ed-61dac1619be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 136,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a402b94f-91cb-48a6-8aa8-ca58c35462b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 153,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "56542482-610b-4688-b776-616dedb70003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 170,
                "y": 154
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "59caf746-6ea3-4d18-bd0e-8716288aa678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 0,
                "y": 176
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8e57a5e0-9d16-43b3-bd4f-89a84d8f865d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 176
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a6776a34-5b53-4467-827a-17051a49a75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 34,
                "y": 176
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "620be966-1141-4c9d-af14-cb25efd5c1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 51,
                "y": 176
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ffcf4a87-4908-48a8-bf00-1522b0703cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 9,
                "w": 2,
                "x": 68,
                "y": 176
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c8769334-7bde-45e6-bd1d-7a5aca398c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 85,
                "y": 176
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4b5ceb38-a65a-49c7-af06-4b84c818526c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 102,
                "y": 176
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "6121b46c-2897-4248-bd63-e95d95954224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 136,
                "y": 176
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "ca157dc8-c211-4479-9942-dcef110c24f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 153,
                "y": 176
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "89197aaf-24d9-4075-8a31-09591bfe2b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 170,
                "y": 176
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "9906e9cc-5417-44ec-a949-8c7bf2be79d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 22,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 0,
                "y": 198
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "e6a0f468-f8c9-438f-9624-7cb91625afcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 22,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 17,
                "y": 198
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "d77eced3-bbd5-4031-a743-9cb5751ea5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 34,
                "y": 198
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "aa1ef25f-10d9-453b-87b0-8bb67ae81b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "179242dd-6696-4f38-affb-2e8a6bd0cf48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "df9a79c8-d7d0-4c2e-b506-fb608644a154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "2a1cbb33-92e0-4f5e-a9b8-8061c3ce0ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "3043ffed-7d90-41aa-85f6-4dd2edf16a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e3b16ce5-1e98-4a2a-a310-a31b720f6c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "82aa4ba4-3e4b-4471-a47e-bb1bd9fd490d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "74b55680-ae0d-4e3b-86fb-135cd468bce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "1b82dc21-b900-42b6-bcd7-796138a75571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "4b5078c9-3c63-4d9d-afa7-c2c3b7fcae0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "7470529e-468c-4a99-a795-67592ae03542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "fef9fe7a-d214-40dd-8c09-d0998d6f2cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "2cbeab60-e93c-4eb7-9dfc-6616cc3478ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "4395ebe1-a759-4335-b8bc-3771a7adade8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "aa4ebb30-cdb2-4b45-bef9-6046af03123c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "43d35004-4452-4faa-920d-5dd0c473fa14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "cda82338-0a16-40c2-a59e-a5f0fce8635f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "85f124c1-eb3a-4f85-86d5-817b22716c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "cdd7b5a9-ee62-48bc-a507-0abe53edb681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "441318bb-bbfd-4b53-8ec8-47af968a368b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "6d1999fa-e66e-4670-be5d-d4468e99e195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "210311ae-fd2e-47a4-87b4-9526faf5e3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "adb5eb5d-0883-4407-8426-337152241b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "1b6ff5b6-0f65-4479-8608-b78daae51132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 104
        },
        {
            "id": "f4709665-5cbb-45ca-8c7c-4e91ae8ed1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "bfdb5442-d898-4305-a6a1-a25c805aa73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "3f27d8fd-37d6-440d-ad74-ce26e5cc37a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "36b21523-ad1a-4a1e-a29d-2097f8de9e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "bad2a24b-4d0e-4a3e-b051-bd322eac10aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "1a9ce9d3-e5cf-4653-aedc-98c4dec7a645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "1bb2141e-f7c9-418a-bc56-0890d06bc03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "19489626-2ccb-4e2b-9b3d-873aa051b8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "8fcfa692-9f4e-416f-a751-f8285bce5a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "d454519d-54f1-458a-bd9c-6211376f2fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "c4a057a7-46b5-4e38-9680-4e0a8d93e9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "340f5d18-1788-4c8d-8ddd-ee4d6c7235e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "f54c6532-d0c6-4144-a933-8d2cd04a619a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "51b8b284-43c5-41fb-a313-6cc83dcfe7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "728fcb76-700e-4573-bdcc-d57bb7120f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "5991214c-ca36-4966-9d12-ae25dd3601bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "432820d8-8cfd-4586-9389-a906e68e601e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "5b0c8f1f-9d8e-49ce-9a69-878659db1c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "8f3d9818-4894-4ae4-9f7b-93ea10a96f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "1e7e82a2-8205-4109-bfba-1c428ac696e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "1538015e-cadc-4050-80f0-57c08c589f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "bd2d467f-0b32-45d4-965e-1feb132bfdc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "5c6a7537-e9eb-4492-8b13-0a54c12125d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "15e4acee-2414-46d4-98e7-ec06fcd1f45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "b17189fc-37d8-401e-a02b-cc83fe9411e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "98c20db2-921e-450f-95b2-b1671d47484f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "959cb4ea-bf4c-4e51-9b0e-93a9726b88db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "ebe440f4-9e08-4c57-ac1b-30216f37e279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "88e730ca-0deb-4b38-85e0-02b833845302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "5c995891-8382-4593-aa2b-c43f41a1d267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "159ec98d-fe91-454c-b9aa-fd3c66731196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "c67b9756-334f-4e89-b217-67a93f2aa7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "3de250f6-9935-483c-8581-8c282ce42e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "e1db7508-24df-4524-94ce-4096c9b32f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "9024728d-85ec-407a-b22c-20255bfb245b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "70c81632-ec3d-4c28-ad8f-39b27a563701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "d089d962-339f-4a2c-b91d-8059d12d16b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "7b258729-04a5-48b4-bd20-67c284700c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "66ecfa4f-f318-47a5-99ab-af21b0c680ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "dfd6ee08-60cb-438c-b5f2-75fc34936ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a8a9bea3-02d3-4d13-9080-b6245cea67dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "930411cf-6028-467d-b12e-452f368c293e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "52aaaa56-1772-4697-8e53-caa271e7cbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "9ee82520-e92e-4e8a-b1c7-5e42feee1950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8208
        },
        {
            "id": "6d870070-93af-43fe-a855-b0d7b08c7f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "6062585f-a61a-4b45-9494-94f8013f1642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "4025e765-28ff-415c-9d64-20fa96e3392d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "8dc03bf3-b2c7-4de7-960a-42a1f39bd808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "791e115d-426b-4911-9f5e-02aed5ed19bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "99aa38be-08f8-4b7b-a735-f61aa9644cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "830abf4a-23d0-417e-9f1a-e1dcb6bbc34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "26ebf923-7a55-489b-8b20-b0e6cab2cbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "22ce7235-73e7-4638-9c8b-93e3723e9670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "66bae0a0-f260-435d-a5b1-3028933ef4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "65ce91a4-9544-404d-a79b-2a4227ee9c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "38eaf8c0-444b-4ef1-a3b7-466434198a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "29012728-a34f-4d61-a226-abdd41df42a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "4b195593-ea26-4143-8549-a870987149b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "440e1e9d-131a-4e71-83ef-6a2362fb3e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "a7d7fe2a-f5d2-41e8-a1bc-62a63aa742f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "03cd0050-7135-49ad-ace8-9506cb37580f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "32a0668f-6ddb-4e04-9ce2-d07d6517d1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "b3a66554-9b63-4961-8d0f-6194165295c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "7343c28d-4157-4d92-9e5a-6ac787ec17a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "83940809-5652-4799-99bc-cd043f1b4086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "66399e2a-7615-472d-9b22-9f8876cf871d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "e010d7be-d245-4796-8934-b7fe58a0c450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "bc3c74ec-eaf6-4438-8aae-103b8baca049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "f33d442c-265c-4989-a16c-3f3c1c0a6995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 115
        },
        {
            "id": "7d1a8ebe-7ac2-4546-9eb8-962d87c6ef91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 13,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}