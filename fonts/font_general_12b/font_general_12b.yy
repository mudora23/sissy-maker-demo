{
    "id": "bef07160-b174-4220-bfe4-ec2222e07a9a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_12b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9ab5cfac-6b05-4bf7-9ff0-b97cd2a6aef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 155,
                "y": 44
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1bdee4d5-e495-4232-b15d-ac429f98053f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 116,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "aeba30b6-2251-4d42-abab-93d77a84d0ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 6,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "43007239-3914-458a-af15-a2a50fe8a872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4aa6aeb7-bcca-4feb-b5a7-ff04d0141c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 174,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d1f21a52-1dc9-4816-a47e-b4f4296e1d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d9f56468-07d1-421e-940d-901c0c283125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "039a1b5f-da5b-4f2b-8ce3-38c16b10da17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 6,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 137,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e64482ed-77b6-478b-83c2-011cfa7037c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "61cbc4c7-ea75-4a24-ad04-bb8f89a4fa6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "270fc404-7a5b-4d8e-84a1-df37262893d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2778b32a-fff6-48aa-8d2e-603ea35b3239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7df50329-db4c-4d62-9fa9-bc2630129807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2f8d8bd9-9715-43c7-b9a8-af4ce4df7524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 77,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "352043c8-7442-4956-866a-5f69fb12a017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 120,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d3d6ac00-d5e2-4dac-863b-8967e993faf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6c059a50-2e24-48d3-afdd-e7ad857bf7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 154,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cf8029ee-2076-4120-9b03-2a10c868ef87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 21,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e65e1133-dcb3-4bf7-8981-374268d2387e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0e6b0fd2-f4fb-4820-9831-2cd978f40b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7a68407f-559e-46dc-8de1-0f8a1ed05855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4dac5365-60eb-4e25-96c3-10c7b872a210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1ac34099-f994-444d-9a5a-7ed18486cbb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 137,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "858a76e7-d271-4f61-b5a3-d26d721d9b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 164,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cbee65dc-ab4f-4d43-918c-add63e598a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7b081761-7ffc-45d5-9694-08d947d01e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1f619106-ddb0-4dde-a0b9-f132ea2dac21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 101,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ea219a50-0615-4aec-aad3-aea329b54a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 33,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d55418c3-086c-4286-a5be-fb48011326bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 14,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cec13da8-2b82-4393-a734-b380e7a5a45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "67801ca5-041d-4eb0-9d84-469046dd241c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 202,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "71b6722d-a174-431a-a449-8ef85b048735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 233,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c046e39a-8c8a-4cee-bd8f-5436fd4252ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2229c716-3946-4c58-943d-f60146627419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7460a069-86b7-40ad-9367-5360897b13fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c4b1c80-4c93-4953-9acc-2ac893327a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 84,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "843d7b83-ebc4-4a98-8eff-4384a40c81ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 124,
                "y": 23
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "eb59d3ce-2998-4309-8c45-6166032aae82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 128,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "79886495-5364-40b8-b7b6-a06b68a46eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2ad09623-5354-4e82-8497-7ba588fef487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ac8fd387-4339-4922-87c0-50ced5228f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a9bc6f11-5e25-4d44-b986-1978835029d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 105,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6ce07621-8e19-41ba-b7ca-d27769fe02b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 162,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0c67d7c3-7812-462d-bcbc-84470ab9cc7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3f116c82-7c7d-4461-8ba2-843fd07b38c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "10874262-303b-4fa6-8b9f-4bf9534f252d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e42dfaba-7905-417f-9118-6f6adb89dbcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "12807187-e674-4e92-b2fe-6890bcbfb262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6e5f143a-cb6c-42a9-9860-8d6b46ff378a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "92f163ab-2a84-4efd-ad55-fb6848461512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "07c3d08a-4a83-4a1a-aa33-30a854246c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 144,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "532a6837-2d1b-4ba6-9992-d268d24759da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6d8fdaa1-920b-4d27-beb2-c20ae664f23b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b918f09c-7bf7-4890-b9c7-d56530beaadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b25d6b28-eedd-47b5-93fc-e6679b240f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "439f4103-258d-4b77-8adc-0b660880354e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a6b7c07f-a5d8-4a5c-8ca7-02401ae93ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "876cb0c5-3258-4c29-8179-260e94b12c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "90c712ee-b3fc-4d63-8dce-6901d5ca5b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4cdf2677-69c2-4632-b501-ea4a2759ae41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 246,
                "y": 44
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "97322582-66df-4a68-b6fe-af73a69138ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 218,
                "y": 23
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "dcf91fcd-de26-4133-b48c-cf876687ac2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "474d1612-3f1e-4ab3-bff2-7b94e90f6407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 89,
                "y": 65
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "45dd3662-e496-4b16-9f5f-18e0c200908b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cde88f9e-13e1-48f8-8cfd-868653a10d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 5,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 132,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "204a2342-f752-4b0e-bed8-dc39af4874ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 64,
                "y": 23
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d4ba0f80-9f9d-44f0-b7ea-3ad8e4ccafaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "33257b3a-f18d-4f55-bc90-05b8232f243a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 209,
                "y": 23
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "703a3eb1-7e8b-4b3a-a245-c5858d604596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 227,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "68f77d47-7f3b-473b-b693-d34fe3981f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 245,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3f101960-7e47-467c-9da6-ce8ada81258a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 177,
                "y": 44
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "091c028d-81e2-424c-9e2b-6eba7f327a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ced1b3cd-5e39-4923-9ffd-86007c0d71be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f0151a66-7615-4dd4-8710-7fd2b44e910e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 72,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5ee33ea0-7731-4ed0-bb7a-09e514e5a44f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 170,
                "y": 44
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0448cfaf-a65f-46ca-94d9-fe700606299a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1f184cc8-d2ce-4866-9784-b0209e5d2dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 65
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d86798af-5ade-44fb-809a-0ca18e80756a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "edeb59af-483a-4e11-afb3-31d82d12dc37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "64e8cc05-f1c7-4053-bc64-ec596223b9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8d63ff49-c7c9-4d64-9e7e-7d726dc639e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 46,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "775bc067-e31b-4712-b96c-9ac32bc93953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 55,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "687a5d80-fef0-4644-b367-5046c5d2132c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 226,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "62f74e13-3056-43bb-9b1c-4e73c6a7037b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 219,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9d9c8578-704a-4e59-bf6f-c562476e7a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 27,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "06f9ace2-392d-4ba5-9c03-b3b8b0741b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 101,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "25f33ffd-9cfc-4fd8-98f3-1ed2ac4f502b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 114,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1eab8c6b-0c35-40c3-ae30-c36bbcfa4c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3543b37e-4d75-4f4a-98a8-14d62c9f4f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 94,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "773d57f7-0721-477b-98c7-d7b1a8f7538d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "401cd442-3a14-444f-a76d-0c8fd9a1b25e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4ddb2948-5421-4ab7-8523-66de2fb7df56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 201,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dc9c49c4-47b6-43ed-bd2f-0c2d7da6a8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 4,
                "shift": 9,
                "w": 2,
                "x": 97,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a9deb818-f930-42e7-a85a-ae3b6408a4bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b3f31e14-47bc-43dd-b03b-d89954cad177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 210,
                "y": 44
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "02ad47ce-012b-4d5a-9a04-91dec794c884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 83,
                "y": 65
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "9518dca0-5d41-4e9a-a685-7ae5fdaae286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 9,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 128,
                "y": 65
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "1900973d-144b-4777-b01f-2504fe81b2eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 9,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "af5a1e72-3798-4feb-a2ed-a31f5cea6975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "180f58c8-1b54-4e6f-8047-39818707d867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "2f4a0d8c-fb8f-4336-b537-e2c1c5764a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 2
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f4fe88fc-ccb7-4bc1-a01f-aca92cdfbbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e0e429a3-5479-4757-8758-377c1c4acbde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "197c9761-4722-4ab3-a538-8363f7d34bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "8557bbe4-1404-4149-aacf-3ee468007f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "9837f035-e1df-4adb-838a-775acf57b39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "672ea700-ca53-433e-8b0c-17b24406b9bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "1156bc6c-ed3a-4fb6-b2bc-6295815cbd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "673ce39b-f744-4980-a844-8a41a3541ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "7be45ff1-760e-4139-b395-8744e0a05044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "b9881a99-adcf-45ac-9765-9642107e87d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "c937b0ce-eee5-48ad-a339-9386387b950f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "33d3351c-28a5-45aa-b86a-82000ef57bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "c91e287e-43d1-4d98-9bd5-35776a733494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "926d50f3-73d9-4212-92c5-693a0a4bf739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "133e6bc0-02f2-4e2f-a8b8-67a1f881fdfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "7995fce0-9824-4bf2-9991-bfb988a9e6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "31daa2c8-cef0-475e-8d49-439e0776448c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "7455bbd5-e698-4f05-aee3-fbf4345ff730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "a2fd6e3a-5fc9-4158-8a1a-5438eaf0cf47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "28a3b633-2716-4904-aa62-e39314ed52af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "81434cc5-8b37-45f2-a0bf-8ca0c4d8e78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "d72be48a-bf9b-4479-a9df-f7e88b8276a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "dd156a5b-717e-4885-9c14-29bffe03fbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "b673282b-a9bd-4994-bd3f-c059cdb5cc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "0ed2f961-1b29-4013-8d12-c802701631b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "3b5ce62c-6b9f-4d68-9ec2-75ebba596d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "b5454ce4-6d79-4dec-b04e-57447f37b98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "b386421a-f9fc-4f9d-a39b-0ebafb1a7459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "0708dced-bf9d-4b82-ba37-81c0ce383710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ed138a2b-1e88-4624-9180-563b0eb3794a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "d36f4953-135c-40ee-8cb4-9082b26deda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "0582195f-a4cc-44d8-8626-07dfe3f244c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "5b594088-1939-46b6-898d-6d016caca7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "1486a4a9-cf1d-46eb-80f4-5ef9714c709b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "e606678c-5cdd-47b9-9776-f36e7f7ca348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "de2c9feb-2219-4a4c-86a1-64e67943fc6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "629b4ae2-e05e-4eec-bd0e-1b4331651efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "4292d980-0327-438b-8741-63c979ab6c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "2596d1a5-b097-42b0-b1a2-10c0c5578947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "8cdc7f2a-c7c9-4371-9983-ebfe237a7181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "a3ac00c6-46ff-42e1-b7c3-08fa289997df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "89f15af6-d19c-4d85-b08c-e5bcd9ac089d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "2f4bf9ea-d3bf-4ce9-9e3b-4b4ee8f4c447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "4b880798-969f-42c0-a65e-ac54a4b2be76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "ff6b0098-2e8e-4870-abb8-7f2cd34d9a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "45eba7d3-8157-4cb2-8828-579dbc158deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "d00fbf1d-4576-47ca-a819-fdfdfef2a14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "c1568f25-d5e1-407c-88e6-7676248eab04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "ac26817a-0625-485a-9972-a9e2d98c4652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "69aa85f9-a6cc-429e-8e0f-bb273f355261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "a7cb576e-60b3-4520-8bf2-d1016dff6cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "20f56b98-f17a-4947-994d-4cb078f4cdc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "4fdea3cf-8675-4030-9fe3-4dc5d9848bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "674e43b8-d8e1-4dac-b78b-0358b987b694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "b591993d-c218-4999-a8bf-ee83a11e73d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "e2d1b19f-0ef4-467b-8120-c897b6ca415f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "3a3e51bc-3c2e-44a2-ba87-cf7b2bcd908f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "730d4301-d79b-4ea5-9450-3b784d4e86d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "7a6f1bb2-9b9e-459f-8dc8-ad63014e606c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "4a6865a5-e0c7-4aa5-998c-3b04e19f0e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "78f568c7-acea-4982-b401-7199d56f5ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "11a31a32-ed69-4043-9f7a-52baa2de9923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "d71e4085-5e5d-48cb-aecd-0283d7838a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "f375aaa7-760e-484c-ae79-50191a7f068b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "3c404a69-b775-4848-9660-8bb996fed36a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "12a8e7d7-4bc8-4143-9117-7736a2b04bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "09e41b9c-72e0-496b-a0a2-f77ae96b2252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "0cee03e4-8fbe-402d-82d4-ebf14a2a2ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "8a9f3da9-d1f9-406c-82b6-433fa161cec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "49be914e-81aa-4682-aeed-42904cd10011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "c6f768b3-e198-429b-bdbb-699f249e88b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "8d088a0d-977e-4235-a115-c7a6608665b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "5d95fcc3-872d-4d8f-8194-a618c089ed41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 115
        },
        {
            "id": "8f275938-84da-4f67-bb9d-7be1e21ae675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}