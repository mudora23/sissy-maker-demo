{
    "id": "af369d46-2075-44e5-b844-0d38a8dc2041",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main_gui",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4c608e47-ea3f-4bcf-93f2-25dfc1f867d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e7cb9930-cfcc-4620-a9e0-267f23b58269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 15,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b1de2234-422a-46df-97d6-ca3cc60bf0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b308fcd2-99f9-4758-b685-6af66fd363fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8b394ec0-a05d-4d21-82cf-6b06fbd39514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fb2c4331-bd10-4f7d-a36e-a405712e7ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 75,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "41a4e2c0-0624-46f7-b431-7dc40704f0e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a7417a2b-02a1-48be-b3e8-51888f0a537f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 105,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "784a7965-abb2-47d8-8bda-d497c83525f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 120,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "98e03bcb-4cf4-403b-a968-8ce12e0bfd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 135,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f68efcb7-54b2-402f-9c59-9f14e13e55da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 150,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "de5b1461-42af-45e8-a927-213b7a504abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 0,
                "y": 19
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "48c39d48-2d16-4f10-b976-f64b71fcac6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 15,
                "y": 19
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2ddbab55-a217-4d30-baf5-b3a3ef399102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 19
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ab7ffeef-a8ee-45d9-8a9e-b1514c51ed38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 45,
                "y": 19
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "063b9f13-1bcf-423c-8797-1577984d6e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 19
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1f89dc3c-b0f5-41d7-9a93-bfe55d50113a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 19
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3ae3b952-3cda-46c4-a190-8b9c4fb7d781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 90,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "870cb77a-25eb-413e-ac85-dcbf7eead1f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 105,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d6c6fcf3-f58d-4eec-8229-b8291a89de18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 120,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9846b521-1ad5-4784-a90f-89d945a52cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 135,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5542ada1-6a90-4626-b226-3b9db5c5ea6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 150,
                "y": 19
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ecd30ffc-ca21-40d2-b90f-c58671fa4ff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 0,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d7c16b1a-5e17-479b-8b83-94f34848d090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 15,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "118dea83-42cc-4357-b5be-4213fc7c6508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "671cf06b-3577-40a7-9481-7773669a928f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0cc39b1d-51a1-4b4b-a55a-d3ba34ef3f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 60,
                "y": 38
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5d18520a-f1b9-47d0-8280-080ade53ef5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 75,
                "y": 38
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2c55870b-87e3-42ff-baf8-dc328107f4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 90,
                "y": 38
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "34fd705c-7bd0-45e5-bae5-e07a2964dc93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 105,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ac7c97e5-e153-4404-a1c9-12e95ae880e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 120,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3278a3b4-1ef4-4c0c-a8dc-4cac6033965c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 135,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cbb42a6c-95d2-4131-a7e1-9006c939d85d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 150,
                "y": 38
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0a8dd4b0-6b75-4bef-b95d-f5ecd545e88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 0,
                "y": 57
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ca13c243-b843-4f7d-845a-968801a769bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 15,
                "y": 57
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e3197c1f-d09a-4491-a362-b500feee532c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 57
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fc718655-db2d-4315-aa87-acb71ee4c1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 57
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "893bda24-11eb-47f8-b951-590c525edfef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 57
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d9dbdda4-bccf-4b51-86ae-d60dc39c153f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 75,
                "y": 57
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7007ed02-f8df-410f-9b4c-bb2e332e3c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 57
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "235b600f-5e17-4fd3-bd08-c89a0d0ffb98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 105,
                "y": 57
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1d4d5706-07fc-42bb-9de5-680da4bc76f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 120,
                "y": 57
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9bdabe0d-c14f-427e-9b53-768b4e04d16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 135,
                "y": 57
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cad1f3af-00a4-4f36-b01f-982ea2f1ee12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 57
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e169f5cf-834d-4c39-9d6d-9f1b29170ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 0,
                "y": 76
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "47fa6e62-45ab-4a51-8ad2-c30f3512894b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 76
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ff7b23a5-c38d-4202-86c6-ad628579a4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 30,
                "y": 76
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "de9c36f7-86db-47a1-8d09-6291b26a2a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 45,
                "y": 76
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e9ec8275-d900-42d2-a4db-3ffbe77f32bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 76
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "73e41532-5e1b-4502-9810-a58350607579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 75,
                "y": 76
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "146bc042-b5f3-4505-9626-6c962219f635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 90,
                "y": 76
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1daba226-1166-4db8-994b-c4ff74aa1db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 105,
                "y": 76
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dc74fc8f-33cd-4f24-8b29-846f08fa53e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 76
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8c667ee8-5c1a-4ee9-91c5-85651d89150e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 135,
                "y": 76
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "99537b08-1577-475a-bb94-01b5a86a4568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 76
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "de7db8ab-574d-4b78-b04f-891fa094ac33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 0,
                "y": 95
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c6f0adbe-5ab7-425d-af7d-6bbf8cb21893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 15,
                "y": 95
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "651fe7cc-53dc-41f7-a868-5a3131f9bd7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 30,
                "y": 95
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ec31c917-9133-430f-b418-d6841f9bf625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 95
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e2de1395-d455-4813-8183-7d6ab3ffdfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 60,
                "y": 95
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c89e54ef-4a0f-4ba7-9025-d6302c23c0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 75,
                "y": 95
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "84923d9b-bfc6-4af4-a88f-8841e50f8c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 90,
                "y": 95
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d4da61d3-7c2e-44d4-a924-0a99b6ee382c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 95
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "68323d69-ef6c-4ced-8eee-e19717e27571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 120,
                "y": 95
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7ce19ecf-d5bb-49b6-9b59-705e92241155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 135,
                "y": 95
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "aec34504-dd95-4139-a8f9-cd1ef48f4fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 95
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "56c14065-773a-40be-9167-d5c749fb6418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 0,
                "y": 114
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "16e2d5c4-04fa-43e1-ad41-469c69d462aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 15,
                "y": 114
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bb649cc7-1930-4c12-8f4b-bc788fead486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 114
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a3ebc073-99e4-4a36-945b-8e4838558f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 45,
                "y": 114
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3ffc02dd-260e-47d8-a7b0-0f5f981b6689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 114
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2bc4ec76-2ab7-4715-8a19-e7cbf861ed07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 114
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "df140dcd-2d89-4b0b-9dae-ee62abeb8942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 114
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "409e6626-7439-43b2-8f58-27f19a0c2da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 114
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4e00a660-0f90-4756-b1ff-757a8166ad30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 114
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "357ca110-5b2d-4617-90dd-e1870415b6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 135,
                "y": 114
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d2262399-cc89-46a7-a9d9-6708fad85164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 150,
                "y": 114
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3e4c58de-369d-4170-8fba-1ecb8c31a9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 0,
                "y": 133
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a66f755f-6774-446b-ba60-84d57a5e5ae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 15,
                "y": 133
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1ffc3cae-5d9e-4061-932f-b1f32976c487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 133
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4acd0505-76e1-498f-a06b-34eeb882f9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 133
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9411147c-9078-49c0-a734-ef6d73e525d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 133
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2d3f2d7b-826a-41b1-bfcc-057d75ecb30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 75,
                "y": 133
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1a7a9d3c-3bfa-4f88-9cf3-763b509889c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 133
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "273e723b-6fb5-4316-ab7b-c887aaf1e401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 133
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7dff61bd-046d-4fd1-b5f1-ecc64c2ad32d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 133
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3a1f0515-502c-44b5-91a9-9293a6cd0148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 133
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "eb110dc1-ea85-4a14-968b-d8cab3464d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 150,
                "y": 133
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "490295ab-74df-441a-b776-3c62387eb966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 0,
                "y": 152
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2e6ab91a-2b66-4bfa-a0c4-1db2f04391e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 15,
                "y": 152
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1fa8d09e-2e04-4cc8-802a-3bc0e97d2dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 152
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "40821113-8c29-4645-85d1-4ca6312ec1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 45,
                "y": 152
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e6d6b4cf-434b-4bf8-a2b3-607edf140f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 60,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "03e9fc76-6e8e-4c3c-bf0f-51344a025829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 75,
                "y": 152
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ee7ee1bf-5cbc-4933-9479-7fc46fa85cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 90,
                "y": 152
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "08b99577-010e-47ea-b0e9-ad4abe2da89d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 120,
                "y": 152
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "6f45e01a-51f4-433c-8377-99f3532bc116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 135,
                "y": 152
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "08864039-5107-4f73-9241-073ca29b923b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 152
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "5b58be9a-dc65-4e5d-ae5c-628a18c22334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 0,
                "y": 171
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "ac5f2199-0d8c-454d-9267-a95a94c69a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 15,
                "y": 171
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "a5bda72a-b99d-480f-8673-e37161462ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 30,
                "y": 171
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "115a675f-5069-46fd-8f40-3b4ff99c7b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "28c5b5a8-21f6-462b-b534-b2b6a24827c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "51c9f461-88f3-4df5-8b33-4550c76bb3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "9ce22695-628c-4104-ac1d-fc434851341d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "70a87fbb-6cf2-4b61-a191-2252857547d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "1c4f4664-9638-4138-ac90-554cfac9da18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "24bc3012-792b-445b-bf07-14e81a16f1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "10676f88-8c19-45e1-a9c4-110da74c359d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "7c35a8ca-742c-4b81-8dd1-d865c8fe18b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "6e24764b-6e99-4df8-af7f-e84bec9b039e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "e0bf2d7c-e96d-4670-ab4d-8cd5f63ea8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "a7e2f752-2f6a-4404-aafe-5722fcbd5e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "75d42496-45c6-48ca-ad65-216c53971d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "07c94a9c-aff6-4b1f-a331-849932ca9843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "062f85e9-c30a-4964-a878-edb4c20534e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "e0dc80d4-eb74-4136-9a35-1657eed89395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "53ddadfe-dffd-42e8-96a9-d92d8ff923ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "8e4a0892-9980-423a-afb5-92d2079824cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "395f5c37-80e4-448d-b6ad-f82db2e5ed6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "2cc9ac13-b24d-4e3f-b544-9177d209f2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "f88fb008-15e3-47d4-bf78-8e64ba58eed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "5f74b1a7-93ec-48e2-a023-2889d380730b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e5a2a975-1f65-4598-9ff5-e41ecfba5fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "8bc4e945-56e6-4d9a-9306-d0395023aca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "67738424-4164-49c0-bf7a-a6d303ef5cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "17f840ae-fd1b-4f5b-9298-6e4e0ecfcbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "6ca846ea-f60f-4c19-8477-6a2ce1bd2bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "24ab1e0d-e22c-4ef3-b66f-c933e92ba8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "d83db67d-b383-4840-abb1-81e501857823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8a358231-4834-4cc5-a0fc-65b97b8bf37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "06be7332-74c2-42fe-abdc-c3fabd328e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "4c5a10d2-6054-4caa-8157-38a570ff6276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7b5cba20-d53f-41b9-b236-2bc3b1f86818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "6e36cb9f-b3a0-43fa-be1b-ebb13a1ed790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "a65358f0-224a-4787-8b4f-534523b9788f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a9eed7ab-53e8-4f3a-b631-829e19b4b916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "82839267-e1ea-4051-acf3-852e8ad75714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "2b14c40a-6952-45b5-ab56-599c39722da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "f21045f0-b4f9-4c54-8a29-25aaf6833768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "85046f87-473d-4f1e-916c-82ef58d51ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "5b87420c-90bc-4dde-a842-ff62a4a2f4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "512d634e-1abf-4464-949f-01c0155e4412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "d8b4ea15-4066-4934-82b7-d1fe9884e0d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "170861ad-8982-4427-be58-697e8f459a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "317f6e73-628a-45e4-b525-117cf5639050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "66aab12c-009d-4b10-841b-6dd0947a3e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "3c7dc787-6026-41e3-9573-d983586b7dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "46747072-7a97-4f4e-b86b-b1730f0e8300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "1a6f3168-9a78-42ae-9440-f2ee0c9ab5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "979d3a9c-f569-46d7-b6af-8461625b7f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "19f9dcfd-d443-4d86-8991-c19df4054740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "36e6d7e5-9bd3-4c0c-a9f4-df36700c827c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "e589d189-5078-4d49-9cf7-35d39bfaf2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "994ca0c6-eda3-4485-9465-d81ae2f93662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "7f86a359-1c7e-40d7-b091-30b7fc11a332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "10f5fcc4-26ff-47b2-ae71-d28c6cffe489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "121076d3-e02f-4131-b068-4310fb82ab01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "e9a7577c-6b13-482b-9c9b-ebb40ca4f82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "37a248f6-df18-4183-8b27-9e2a849f424b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 115
        },
        {
            "id": "90c99b6a-9a34-4df1-bdc9-b08ccb248038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 11,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}