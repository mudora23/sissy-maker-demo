{
    "id": "f16b5c2b-2050-46ae-b38d-ea8a67abb462",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_italic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ac9baeab-1876-41ef-b717-bac3cc6c9236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b894df25-7864-417b-9d93-134af38f5b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 21,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bffb0aee-b786-4b26-bc49-8567faef19d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 7,
                "x": 42,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3b2b489d-f32f-4c19-a676-890077878cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 14,
                "x": 63,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "688dc098-2f07-472e-9405-0fa16728aafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 84,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a92ea6ee-9786-4197-b628-d7937ba3f2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 105,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "66b1d6fe-10d7-4838-9a80-82ffe29f8091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 126,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a412e34a-e626-4258-87cc-5d03c0e83018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 2,
                "shift": 4,
                "w": 3,
                "x": 147,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2888e742-abaa-458a-99a2-61fea2757971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 168,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f1c1e631-b285-499e-9915-343bbd6bf631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 189,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cc6155b6-dd6a-456f-a2dc-a97619de33b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "58437761-01ca-4700-860f-bbb14c18f5f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 0,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d7cf5588-663a-49e8-952e-0f8407a340f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 21,
                "y": 30
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7d1271cf-cd74-4b79-823c-d8d7ca0b022e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 42,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0d990629-80fa-4ac5-9eda-802421cca496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 3,
                "x": 63,
                "y": 30
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d6ce989d-dec0-4e8b-b398-f5c2b93527a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 84,
                "y": 30
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f9696873-a587-4915-9c1c-464718a50c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 105,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "40ef0a30-d935-47a0-ba59-c22c5a13dd0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 126,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e1ac1c62-8cbb-4ab6-abf9-1a99a857894a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 147,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2522111e-957e-45c3-bd97-59d615ee4b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "00a95207-6d9d-49be-a0da-6896dc522c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 189,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "77bb52f2-cb7c-4f20-9b24-5b44402b967e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "356d6591-0a53-464b-8356-dc91de53dc9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 0,
                "y": 60
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4a85985e-0594-4b4d-a3f5-77a0d314af6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 21,
                "y": 60
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "de23c006-7a98-474e-9257-b381fb7cf623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 42,
                "y": 60
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bbb47d82-ed9d-438e-a5bf-b757f8406ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 63,
                "y": 60
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c8aaea17-ccec-4fd0-8d3a-10324de16aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 84,
                "y": 60
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "90ffb511-6aa3-4891-8fff-b1f6c4a7c1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 60
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "072f4871-443a-4119-9990-698cfa41e91b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 126,
                "y": 60
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c583a075-ebf8-4fd3-855a-2596eda34ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 60
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "37143a27-c4e7-4ffa-a481-d24ce28d84fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 168,
                "y": 60
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "850ad7a0-ef40-413e-9a6a-23a275928277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 189,
                "y": 60
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4efc5f0d-ce65-4276-8db1-dfeec6166b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 210,
                "y": 60
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d4c7ccb3-f0ce-4fbb-975f-f7cbd0fb31ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 0,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5b3df859-4cb4-4313-8401-d3b92f0350a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 21,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d3dde3cf-863e-4d36-a2a3-b8c2c7a3b78f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 42,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "babe1e62-8d4b-4060-acd3-6a711d0b72ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 63,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f1a7b9b1-dd4b-4f82-8876-2dbd8a041d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 84,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ff7bb86d-7269-4316-9cf1-4500d4cd15c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 14,
                "x": 105,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f74fe9d9-fcc1-478d-9338-4d5e1f583e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 126,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "42ef8aa1-45c7-4c25-bfd2-b38a7670cffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 147,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e2bed7bb-b7db-4f24-b040-39b0482b02a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 168,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "eec8653d-ede3-4a51-9024-d92a62378049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 189,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f37c356e-2a9f-44aa-bddb-d973206bc4bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 210,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "29e783ca-4636-4d9f-9c19-c88e33f0f3b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 0,
                "y": 120
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a404bb4f-ceab-4554-b66e-a3dcba50e2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 21,
                "y": 120
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1ae4d551-19c3-4ef8-b92d-89ddc79d6e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 42,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f168b7a6-cbb4-4e44-a0c9-50f82fc51528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 63,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fc5d7c0f-9adb-4e92-883b-3f54f88cfa66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 84,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f995eaf4-d3a1-45cf-b154-8a0a4fa54667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 18,
                "x": 105,
                "y": 120
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b32af9ea-cc8b-4f52-9a06-b6aebae9690f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 126,
                "y": 120
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "277109cb-2c39-4fb0-af75-e836e479c9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 147,
                "y": 120
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "946f70f9-7a29-43de-bb17-30f7749af8f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 13,
                "x": 168,
                "y": 120
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a380e0d8-daec-49c5-8236-6834965b3aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 189,
                "y": 120
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c03645ed-b8c8-4b73-8c36-36e2e0b0e8cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 13,
                "x": 210,
                "y": 120
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c3e1b999-141e-48b9-bcc7-7c8940b9552f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 3,
                "shift": 20,
                "w": 19,
                "x": 0,
                "y": 150
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0e015798-45a3-49ff-bae4-8f994b829836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -1,
                "shift": 13,
                "w": 16,
                "x": 21,
                "y": 150
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7f4819a3-5e77-43ac-890b-489d85172a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 42,
                "y": 150
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c4a4ab8a-17b0-49ed-b9ac-8b0f1ce355ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 14,
                "x": 63,
                "y": 150
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fcad4dfd-7d1d-4c7c-8af8-d782a5fb7ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 150
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "63f230c1-a6e7-447f-a007-925e26413ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 105,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b9a6824e-bdb1-4e6a-84f6-6e944b0c5c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 150
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "87bbf598-dbd2-4740-a812-91416b71773c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 150
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d54dafe5-cbe3-4fb3-a10f-a064be0a4cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 168,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8233b910-a4dd-4b55-bbbd-f1f6ca138918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 6,
                "shift": 13,
                "w": 3,
                "x": 189,
                "y": 150
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7bd2d80c-1645-4a6f-91f7-d099879d3cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bdd12bd5-a8fb-4a6d-a15e-f7864226a2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 0,
                "y": 180
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8e046b82-dc5f-4c26-86ab-d30e998c4be3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 21,
                "y": 180
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f9d3afb0-1873-4f0c-b526-cc26a82929bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 42,
                "y": 180
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f6f550a3-b182-4f01-b378-1522a927a3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 63,
                "y": 180
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "58ce4672-7f16-4706-bcdf-bc480c354e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 84,
                "y": 180
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5f851cba-a3fa-4cbd-b33b-61b41fc2c5f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 105,
                "y": 180
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0b9a2bdd-33df-41ee-9d69-e2dd4c31fcba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 126,
                "y": 180
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c87cb0d0-a207-4110-a862-2f48748d3d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 147,
                "y": 180
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ece812ac-f9ec-429f-83fa-2df402825b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -2,
                "shift": 9,
                "w": 11,
                "x": 168,
                "y": 180
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a220a783-460b-4aec-b514-248585b5d9f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 189,
                "y": 180
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b31ee408-13f3-4cfc-9bf8-2a25f9f7599d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 210,
                "y": 180
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e7f8d6d1-890b-4969-9dc6-158bb8070fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 0,
                "y": 210
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3a86bf82-77e2-4992-ae7f-d5968b5a3b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 21,
                "y": 210
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "622c7ab8-38e2-4d29-b113-14d9adb04f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 42,
                "y": 210
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c48c361b-c3ad-4edd-b64c-16a308d3ff0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": -2,
                "shift": 13,
                "w": 14,
                "x": 63,
                "y": 210
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "773b3ba1-c20d-46cf-8a2e-6190a8b0964c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 84,
                "y": 210
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a9f2a575-88ed-4781-9caf-64124762b399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 210
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5569a532-b584-4971-a73b-bfe177e395f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 126,
                "y": 210
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5d38597e-adf7-47bf-a92c-9fc8d1970884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 147,
                "y": 210
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "76d27dac-4212-4a54-ad9d-684e56f1f878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 168,
                "y": 210
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dc8cfeea-d434-4003-8df3-8ec389f311fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 13,
                "x": 189,
                "y": 210
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5f56329e-819c-4e7d-ad02-799169c871e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 210,
                "y": 210
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "254ee7f1-d9d9-492b-a484-0f9432f4bd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "85046a05-d8ac-469b-b660-1ab3844d67b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 21,
                "y": 240
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d14f207f-8c3b-4940-98f0-33b28542bf06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 42,
                "y": 240
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "2756a13f-b5d4-434f-bc67-8afd1d7da9b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 10,
                "x": 63,
                "y": 240
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9d038b61-6474-419e-a112-ccffe92ef98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 13,
                "w": 2,
                "x": 84,
                "y": 240
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "96776a82-8e96-4a1f-ad1d-112632537680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": -2,
                "shift": 9,
                "w": 10,
                "x": 105,
                "y": 240
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "75f32731-0d74-4168-8fc0-c876cae376ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 126,
                "y": 240
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "f30ce343-7ddc-40a8-ac7d-04688214c830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 168,
                "y": 240
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "310cca90-92c9-460d-82b5-f82f7d5ebd09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 30,
                "offset": 4,
                "shift": 9,
                "w": 4,
                "x": 189,
                "y": 240
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "6f1014b3-8fd0-4946-8f36-c79dbabe60be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 210,
                "y": 240
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "f2ddd51e-4deb-40de-8543-54fa2efa905f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 0,
                "y": 270
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "f11988e6-adb9-4562-9eed-fac2efb8de63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 21,
                "y": 270
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "9151b51b-e0cd-4f52-88bd-2c7dd47b0cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 13,
                "x": 42,
                "y": 270
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "1e698bcb-cbe5-4e1d-976b-ac8419133579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "4874d613-051b-4bab-84a8-2ecf3b4a83d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "de073417-538c-42d7-a470-a799073e2a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "03b4d7db-dd23-4475-8ccc-a471f59089da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "cfa53fb0-b2ce-4bbc-83a4-d09fbed1e18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "3abe557f-51b3-4f8f-856a-3a3c94ea5254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "e9babcf5-dbfe-4e55-a1a9-aa1b0c2a98fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "5693cc10-27dd-4b86-b08e-7f103e2a04f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "c6db6ee4-881c-4705-a648-40644e9d2bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "3b5197b3-8e69-44c5-bb95-e13c8a8da6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "0597ca2e-16f3-4ac0-a4ef-ce44cf9faa5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "5861e526-8786-4662-b23d-67c7950fedb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "297ca082-f65b-41f6-a074-f7be6f4abdde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "89203dfd-5589-4c87-8977-47c7af9f3e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "3fcffc13-1acb-47fe-9cd8-4191928dcb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "f88ebc45-502e-40b8-b188-23670de82613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "e0a95d02-780e-4eb6-9dd6-d3cbdc29052c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "c9762e4c-cfea-4bc7-9ca9-58a77ed71119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "2842a2f0-3729-4815-afed-0ee23d067895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4d31b330-8dc4-4e74-9c2b-235c320b5175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "52642b79-7940-4922-82fc-5b65bfb054d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "a5dd3861-dd8e-4ec3-ba2f-f191af4c6032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "605970fd-e86a-4b62-b911-0e2fd41695b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "d43c9415-f7a9-4be8-a81e-9465717c769d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "37fc5956-9b32-4b62-a275-a88508c27bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "e898ac9f-558a-44ba-afd5-e29b076958ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "4ff45988-051f-4b0d-850a-daed24740217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "bcc79951-28f5-4df8-b1e4-dbeef96e4479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "e79d0e6a-6446-4786-864d-10b51325d5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "b1536365-5f97-481c-91be-3c5f3750b6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "c4b7a0e9-4ab7-4789-ade4-a75bd70cbbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "1457ccd9-65dd-4498-89db-c9b62079c782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "f4eacf7c-7004-4b53-ac6a-0443a9de8ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "721edefa-f5c9-46d1-967d-c677149521d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "bc6b241d-a424-45be-a160-e27e1d6cea4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "bc22c5b5-06f6-432f-9259-1133c7084b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "4456e22a-daf7-485f-b6b4-ab273c5de674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "c467cea4-4475-48c4-90e0-70a0152e9077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "8fff5b0a-69d8-4280-8434-2817e16e9dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "6a002b26-46f9-4c3b-89c7-eb0312f57a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "f103303f-b83f-4720-8939-b723ed4aa800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "9b1a5386-f84e-49e4-a425-f1c3792acfef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "c4d96daf-ea06-436e-b6f8-fc9f31caff7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "d16a6222-bbdb-4048-9ca1-2bc49930f86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "52df0f78-aa8d-4b3b-9a4d-bfc9275fe7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "a1308cce-2ccc-48ab-908b-46f84889ccb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "543e8e8e-07c0-44a7-a675-8378a2e9637b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "25a5efaf-579f-4ea3-96f6-ceec295386d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "14854e7e-b669-4130-9d40-e73cd0618b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "640ccf84-ea35-49a3-885c-2aede1cf7df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "7c957056-930a-4189-aac7-b4e3a220afc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "36d508a4-e33c-40f3-b101-ccab4727038c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "6e73e815-7061-47f1-89a2-d765bf23e7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "93d28db3-fb8d-4ff4-84b3-a30d172c4324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "5b4843bc-d2c2-4247-90ba-40a1c3b5a440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "c557ad10-bd1c-4121-a240-da101a112df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "c5bb3d0d-5175-4f22-a0ca-83af485d2ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "46e445ee-3da1-48ea-af50-a521b31f9cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "a0ab1e30-23c9-4e99-9604-913516edaa12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "80ef7567-54e6-48de-a470-ae800e5e7f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "5762d269-00e0-44fc-a4d3-8b44af8f2264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "be4c6016-afd4-45fb-bb13-6da872672197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "f636b033-6cf6-4388-ad8c-602deb315653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "ba905d4e-b9ca-4e47-95aa-27d91967cb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8208
        },
        {
            "id": "5fff2733-7068-413a-8858-b422b10c64b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "0b8d48d5-ff4f-45a1-8d6b-2780c7d17309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "05adc036-5949-442a-af7e-ab26f3c2392a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "5bb46357-91dd-4ae0-aaba-6f561126d615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "832dbdfe-07a0-4cf3-a8c6-9ccb60999659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "b4b08e4b-b0f8-428f-9e02-e2a69d519805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "d5eaacc0-7dfc-45a7-9a8e-0fc806e1a826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "348b7b51-fa2b-4e70-9c5f-b16bf73922f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "dd25ab26-5b5d-4b6c-b67d-33c3765b83fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "aa08b64b-25b1-4d68-add9-6aeab2d86eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "ebfa4705-757b-47a1-a877-e00d55937599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "bedbd0eb-cfc1-48a2-8394-a6f72ccff804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "7ed1df10-85e3-4ee0-b63c-39c4fe6e76a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "65c89673-5452-49ad-80e7-54879c1ed655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "d83a5182-81e4-47d8-9dff-f8b8c0ea2692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8208
        },
        {
            "id": "18a06662-255f-4c23-9314-03e669f3f1c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "a5815a02-0ee0-4cfa-9c5c-4e213c35377b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "cd7ccafd-dc5d-4000-8558-b02a259ea01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "79be8885-98c5-4e2b-9a37-dd937afaf5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8208
        },
        {
            "id": "1e286675-6070-43e0-bed9-f05063f16ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "6659ff99-4fa2-4771-b0c2-c75db6c286f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "f9aedb72-64b8-4d64-ab43-4edf532f352b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "f99f4255-a908-4548-8015-4ac990c28963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "5cd1638d-38b3-4c2d-a4dc-58dac9a3703d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "fb9ed582-00b4-4eb4-a093-fd63a4952e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "fa662ef9-edd2-42cc-95f9-1b1d91e7cba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "351a1175-e846-4495-aa44-e3a03941f8d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "906f0c10-fbb7-4a03-8dd3-fe3eae0d28e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "f5b26239-116f-441a-a38b-96e321ad44fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 116
        },
        {
            "id": "5e70c723-62cc-4dfe-a8a9-9c947ead3774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 18,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}