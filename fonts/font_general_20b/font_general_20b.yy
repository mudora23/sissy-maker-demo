{
    "id": "4d7f7552-77e2-42d8-a089-f5131abf28ea",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_20b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0e147c1b-6aaf-49a7-81b1-7e711e114fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "91b1b7a7-107d-4719-87a1-13062e052ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 26,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2a542abc-d6af-4614-854b-4da9652d597e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "909ebb05-daa2-4684-b9fb-3c2da3641e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 78,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a8809899-20bf-4f3e-941f-5f9b9baff75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 104,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dfc2e304-fac3-4ac6-a0ae-12115829c262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 130,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b4aed6dd-4362-4791-aa8d-2dad333f9088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 156,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7c8f41d3-4c97-4e0a-8a55-90b27faa5666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "87cb4978-d06a-468d-9d26-81621a8185a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 208,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b6b3a577-4e64-4524-b787-97d447772ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 234,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7995c3d2-1cdc-4270-801f-da16582f1e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 260,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "612aa304-9415-4753-b19c-a2a26d004cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 0,
                "y": 33
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "995604e1-0a11-431f-b02d-1429e4a11f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 26,
                "y": 33
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "acf90e74-1d0b-4f18-9db7-c60ffe40b139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 52,
                "y": 33
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "92c6b937-7ae1-431a-b713-7ec28ffeb79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 78,
                "y": 33
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "42ea364d-012b-48b9-b1b1-e24d25ad239e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 104,
                "y": 33
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "10fd8957-2751-473c-a65b-c1b8dc7edae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 130,
                "y": 33
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c2e29606-a05c-426b-adbd-24505be06da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 4,
                "shift": 16,
                "w": 7,
                "x": 156,
                "y": 33
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "85bc168a-ed3d-42d3-8ade-41f32d48bf9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 182,
                "y": 33
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dd985915-ec8e-4392-8cb4-5caf94728867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 208,
                "y": 33
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5015fc69-c2c0-44b2-8a53-2370dfb0b304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 33
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7d98dda7-f064-4d18-a490-7a67677bd350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 260,
                "y": 33
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "76f3159c-82f3-405c-9848-b0c7ec32c98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 0,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ce1332d7-ab48-4346-b7ec-96ef7fcb41fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 26,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4558dfa7-91d5-4a99-8c97-1982c75685a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 52,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7e8743dd-b7e0-4ada-9f30-e4f6e6dcccd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 78,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b606ccce-531b-4230-8da8-f8deda5814f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 104,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "16adac68-ef39-45bc-a577-b46235aefcec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "af13d3cd-74e8-420b-a449-3f9b9a5c9adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 156,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "739b0370-7015-4b8e-9350-4999ade236cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 182,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4c4bef1b-1b9a-4aba-8da9-7a415d083f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 208,
                "y": 66
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "58b9930e-5397-4c4b-afb0-d866f4777540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 234,
                "y": 66
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "73fca63c-0feb-4bae-afc3-3785f1af8e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 21,
                "w": 16,
                "x": 260,
                "y": 66
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "99ea2a67-cc6f-4442-bf2b-c08774e83143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 0,
                "y": 99
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "78c4865a-de88-4698-a4c1-b78bc9bba972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 26,
                "y": 99
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "42d9bc26-0980-4278-9043-aef78fad259a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 52,
                "y": 99
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9d96cf80-8444-4289-b98f-81f412bf5361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 78,
                "y": 99
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e73bfb0-ab3a-452a-934e-7a392e54317f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 104,
                "y": 99
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "180a2164-7b23-4dff-9052-e1c2e0b31eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 130,
                "y": 99
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c6d8ba2c-6459-46bc-8c0a-918839e45353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 156,
                "y": 99
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d8ce3cbf-dabf-4948-bf37-f25b97cde459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 182,
                "y": 99
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5d74d7ea-109c-4b57-8052-725efc716582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 208,
                "y": 99
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ac02b226-16b5-45c4-9d15-b8d9ea5e86a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 234,
                "y": 99
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "13f6d874-8872-4323-a737-b15c76261ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 260,
                "y": 99
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0042694b-f3bd-40e7-90e6-07032858e75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 0,
                "y": 132
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d21b4113-6fc8-40f3-99d3-1fbe2e8757a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 26,
                "y": 132
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3a4225b2-15b3-40d6-b78a-fbd734393f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 52,
                "y": 132
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ef65c0d9-ee25-42d0-998b-45ace980157d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 78,
                "y": 132
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1d27feee-a10f-4cd1-ad99-eb1eeb833dce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 104,
                "y": 132
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bc5ee229-e2ff-48ed-9bc2-37da16f6871a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 130,
                "y": 132
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d9a19fdb-97fb-4850-8e3b-ad6a148b1e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 156,
                "y": 132
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8a3949ae-c555-4da2-88b9-e948bd6bbe1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 182,
                "y": 132
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f9e64c53-3d56-46ba-a28c-d101792990f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 208,
                "y": 132
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5ed28df7-4d74-43a1-b015-5998e7713bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 234,
                "y": 132
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "91bdb2a3-06f2-4241-bba9-7049e8d610b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 260,
                "y": 132
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "740e0f9f-56a7-463f-9341-ee079b4225a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 0,
                "y": 165
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9ce14947-da26-496a-8864-aba062d1c9d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 26,
                "y": 165
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "dab364ee-b785-4a80-9daf-37d528f6b52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 52,
                "y": 165
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6af41654-0e54-44be-bf9a-20cf5fdd68e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 78,
                "y": 165
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e0351444-a945-4909-881e-0cd24f70bd10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 104,
                "y": 165
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "beb4f287-b710-4eff-9e8c-1a3c70a87284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 130,
                "y": 165
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a7f69887-9dd0-4b87-ab81-65769e74500c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 156,
                "y": 165
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fd1b1d77-de22-444c-87da-5d584824c601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 182,
                "y": 165
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "57869b98-ea96-4d9a-acfb-031c62b8433f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 208,
                "y": 165
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5efbc3da-a87d-4137-8eb5-cc9f8e017d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 234,
                "y": 165
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c378497b-16cd-4444-9695-7967a1b45be3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 260,
                "y": 165
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4c2b83e8-64ef-4ce8-8061-dd1cdcedd064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 0,
                "y": 198
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b98c45ba-b30f-4381-87bd-0efc4c106880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 26,
                "y": 198
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "201d2291-13da-4822-a6e4-b91f4da25560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 52,
                "y": 198
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8d0ffff6-8dfb-4443-ba4a-a57a7554dd0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 78,
                "y": 198
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e56804d3-a01c-4324-a62f-bf180f3f6ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 198
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "41ee9ae8-6cb2-4e0f-a143-dd8cfab33977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 130,
                "y": 198
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "328e0a17-60b9-4660-8db3-a45812847d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 156,
                "y": 198
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9cf993a4-25d7-47de-a534-e5f38286cf3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 182,
                "y": 198
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "538dd2f2-27f4-4a49-af7f-a8753e80691c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 208,
                "y": 198
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d6ab7f73-0c87-40c3-96f1-bb69dae4cc97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 234,
                "y": 198
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bc7b0ed6-1cf7-4c1f-830c-315027277b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 3,
                "shift": 8,
                "w": 5,
                "x": 260,
                "y": 198
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "32323a22-d669-4186-b2d3-9c7e2009725e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 0,
                "y": 231
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "eaea4805-2920-4b6a-a0d3-0b1746070726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 26,
                "y": 231
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d693fe90-f2aa-47a4-bfde-eb8d2a472bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 52,
                "y": 231
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7c643e69-c269-4c24-9417-106779b9fcaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 78,
                "y": 231
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "77781c09-39a8-4543-a8fb-ec6061e62758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 104,
                "y": 231
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8b4b516e-895f-46c1-aab2-a987b4bb49b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 130,
                "y": 231
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "931b311a-8e97-49c3-ac7b-da4d97b7447b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 156,
                "y": 231
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c060798b-864e-42b4-a876-d79ae24454d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 182,
                "y": 231
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d6ac522b-5070-40df-90a0-67351a6a5b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 208,
                "y": 231
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "47da8597-63af-4f26-ad17-3315375261f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 234,
                "y": 231
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "470ea334-85d6-43b5-ac25-01a2fec37c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 260,
                "y": 231
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6493a8e4-65ad-4ad0-9cd5-119b1a91b425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 0,
                "y": 264
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f1386550-47ff-4633-8482-21127d6f49f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 26,
                "y": 264
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "027f422d-15e1-42f7-839d-5046fb2908dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 52,
                "y": 264
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "56cbd650-9f32-4a42-b0bc-9613221bc921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 78,
                "y": 264
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ce14ba1f-5063-463e-90e2-45ac81a40d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 7,
                "shift": 16,
                "w": 3,
                "x": 104,
                "y": 264
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "244fc725-5dbb-4e9e-83dc-d5343ee0c01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 130,
                "y": 264
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "954abf88-c542-4856-8c0f-d0b62e7ef416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 156,
                "y": 264
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "1b87e302-0d44-4973-8500-6613f21a17ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 208,
                "y": 264
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "efe615c5-25f5-4612-859e-6cc56bb2c8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 234,
                "y": 264
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "ea60541c-45a0-41a5-97f2-ba3d9f7c957b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 33,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 260,
                "y": 264
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "9f45a1eb-66cf-4f31-b74e-5ff0404ccc11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 0,
                "y": 297
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "5055e853-d50e-4232-af7a-70a922ba9db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 26,
                "y": 297
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "44ecf040-883e-4e0e-82a3-305cf2b6325e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 52,
                "y": 297
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "23d52441-64b4-4507-a574-9b52df55104f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e45a6acb-f9f8-41eb-abcb-f835361ebe43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9d82df95-8c90-49b2-bab7-16da513cb610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "2870ebc9-8f89-45a1-91d6-c356bed23b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "e93957c8-a621-4425-bafb-ffef7995496a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ef1ff845-3bfc-459c-ab7e-4b7f6c6d892a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "787b5e10-f369-4ba7-9745-15ef43356c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "b6ea1aa7-bea4-4374-8707-854de894ee20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "df3c1bec-b079-4227-8f15-81ea82218516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "80379a2f-2a08-4f5a-b556-1c26f083e31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "7d1c1e06-3b70-4c5d-9e9d-7fa823bd5d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "f7b2fe9e-1145-4162-95a7-bdcade92af27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "9c43da3d-f403-4bdb-be2d-0d13663e742e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "75d1a2a5-3192-48a2-9200-87cfb676bd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "5d4c0b9a-799b-45c7-bbf6-f25e5569d904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "b8c146c6-ff72-41c1-8e72-afb0f51660cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "18f9d797-2f02-4bef-972d-8d8ad684f8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "374bb7b4-7bbb-4e29-9fb4-2d50abc918c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "40e1ad5d-3bb1-467f-979c-2a715c50f4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "9e6deb6a-3efb-4cbc-8fbd-f91874a2dd27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "846962d1-998a-42f5-8cf5-69b93503bc01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "9a3c94e6-253f-43a5-972b-b6f984920b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "def3e65d-2313-4096-8906-9eaa68b599ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "ddba9fff-3c75-4f7e-98af-47cd5962630a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "6283f3b9-4e59-4424-b218-8dbcfa0a43ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "07cb63b6-56a7-45c8-acd3-e38e8757b659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "a0df71e6-e881-4dc2-bef2-731f913e8fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "75f9521e-145f-4509-ae3a-5e027c5dd3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "8a911885-a2f0-476e-9a83-13f590818891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "f0af9845-63d3-4ed9-b60d-29e808470934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "4e7a56bf-9959-4b2a-9cb8-3160e3f4ba91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "5d65cf76-b5e0-4bfc-ba64-bcfbc6e37c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "e2e6c52a-efaa-4cdc-8d86-49106b9a2b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "bc5a4ef9-70bb-4b83-b738-a743ee8aeea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "4466a4a6-be6a-4f58-909d-d9f94047f768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "16e6e908-e295-49bf-b6ae-d79330170e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "ae646f8a-790d-4c01-860a-b3d98d74530d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "2289586c-a77b-4a5b-973e-ff487d09cb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "eaedf250-4f6b-4514-a712-dd20ae26ea08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "7bf0f06c-7cd9-471e-abbe-50d19aa2d1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "e740c33a-a7f4-4386-aa1e-292f213e7771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "7ccbbe99-e54d-4da1-a283-e4f8240968c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "d707835e-4860-4ff5-8353-a8fddf69bb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "478ef3b6-ef3e-4eae-86d0-1257aba0ef35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "f738f3e9-a2d3-4044-b4b6-0de43ecbe7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "2157c4b4-b8d6-4cfe-bb57-5c43b45e2b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "89db19b6-9ad1-42f1-bdff-eac86126f5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "a0ee80a6-ace9-4a21-9303-93a4d280a899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "85130681-07d5-45fb-b22f-f6c885c824e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "2f5026ee-b484-445e-ba66-0ef590227670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "b618ea96-f09b-44c3-bd4e-508060b1d396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "eafc90b3-f90a-478b-92a4-8f9ad3be8fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "4ec4fb2a-a6ed-4e38-b25c-ff38cf34c799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "2fcb44a9-5c86-4b3a-b000-79e1af57cbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "30f62c74-9a31-4652-bfdf-456321a2c402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "94562a6c-3162-487b-8157-85b23a148609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "1f3f2ef0-5586-4779-90df-49c1ea870792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "a7850e5e-7c6e-4604-863f-eae98e43dfb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "aea3e301-8881-4a15-ace5-b306f4758ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "27036e1e-9139-4452-9ca3-f3210cd57a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "75d8da4b-bad0-4a12-a4bd-371b931b643c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "22dd1631-b0d3-4021-8da7-e2907edfb367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "040fbe70-c7d1-45f1-ab23-f4c247df3229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "0320ad60-6a96-4b43-a86e-9b49594ca852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "4818c5c8-2315-4781-a720-61608f220db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "ca2cd5d2-936b-4d52-9cc4-615bcd2f3413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "7e1598aa-3f0c-4a65-ab09-6e8a5c05b840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "d4306f29-6ca5-433b-b4b8-88f486c20ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "580290d1-492a-40b9-b4b1-84cb28e8be69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "d692ff74-bb36-4485-9db2-2f5df847a0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "30205ffe-6853-49ba-a39f-6e08c2c03634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "79bdaf16-9037-4f8f-82df-86325fcc4906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "08b86438-6502-4d05-be1b-faa4ce6267ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "83e1103f-64f6-4f1f-90d6-c0dd853017c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "c3102862-4ead-40e9-a71b-ad572efef6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "534bdf6d-4c06-45d0-8ac4-caeb23234fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "79c391e6-52dc-4a0e-ab12-638162c28a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "315027fa-f6ff-4213-9800-277b69753df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "0550ccba-28b8-4652-8e4a-b2787df6d581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "2e874426-c325-4857-8646-e91ccc49051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "a583324b-eb3a-4577-8bef-58907a68f67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8208
        },
        {
            "id": "4b93bd98-e89c-4bbc-b4a7-e76bdd42e885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "b1d379bc-6ab1-4450-99c4-4fb8c05ae35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 97
        },
        {
            "id": "7b508444-e2af-4e90-abb1-915daa1fdb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "17b2df1c-954e-4936-a468-6c8a13843cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "736c2fd8-72c6-4b0e-8868-cf3ed58f7541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "d0567353-d780-4802-bbed-e0fbcf60172a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "8fb8ca11-e879-4efb-9194-4f5ffed9bcf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "7c239b22-be32-4047-b1f0-c091a8fbde75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "f8d49c53-e622-4b76-ada6-f275fc5d077f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "c1a8936c-ca1a-4b4d-99e5-9561f332d7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "8d1ba919-5eda-4308-96ec-3d461e91d7ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "c23fac2e-c849-4551-ae0e-bdc151c48478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "128d3082-0d95-4110-939b-8b7a492fb8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "af057e6c-cd39-4e5d-810f-005fef7eece8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "bd0567ed-ddb5-4045-bd88-6a3ce4b7cdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "e74a7368-5665-4650-94bf-c40a1c83b5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 20,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}