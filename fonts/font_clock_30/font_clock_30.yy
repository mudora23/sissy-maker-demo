{
    "id": "a30028e6-cef7-49f0-a4b5-8453caa082a6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_clock_30",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VCR OSD Mono",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "24bfb056-fa46-4086-8a9f-1a27bbe992c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 41,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6951c8d5-40ce-45d6-abc1-11c87867061f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 4,
                "x": 25,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d81ef709-dbf9-4919-9967-b4c7e64c0d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 50,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e88c901e-7a28-4689-b0ea-c8cccaad5b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d91d62da-998f-48fd-b3f3-d2b6cec6b991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "01d22c7f-d640-4fa2-8bd7-ee3f04c239a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ef1f1143-c3cc-4fbd-a086-d725f7d34c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cb7860e4-e32e-4458-9147-59a766c55f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 6,
                "x": 175,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8ab3c98d-57d0-4995-b451-b3f1fa2264db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 10,
                "x": 200,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a2837bc7-83a2-4a3d-9188-b47db133d8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 10,
                "x": 225,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0b515cfb-7eec-4b84-8aee-8dc1724c65f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 0,
                "y": 41
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6ae51124-8f33-4f6d-92f0-152f47e655cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a2676a88-7b15-4eb7-a77a-cc855f67fc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 8,
                "x": 50,
                "y": 41
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb2f2d01-02ba-475e-a74f-ee0b03a4a17b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 75,
                "y": 41
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8bb971a9-2fc7-4489-a222-e9a03e3979a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 4,
                "x": 100,
                "y": 41
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8d65ba7b-1e8e-4f0d-9a86-b3a7a233b503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9432932f-c2df-483c-821c-39eb32a5a62e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6082b199-d804-40cc-bd46-a062eb0b8130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 175,
                "y": 41
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9ae485a6-9e30-46f8-a75a-38f78019a0cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 41
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1c9c2e62-0502-4adb-b9a6-e76bb3385fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 41
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "377a1b23-03b0-4822-b2bd-a373f1a3b1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 82
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4b07d3d7-6fe9-4112-83f2-3f36e57a34b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 82
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6f0969b0-c0c3-4c57-aa91-bfe36e8eb8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7d6c1d78-ac45-4eea-a770-8fc7d502793c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6115d797-b80c-45bb-b651-c4a01c486d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "01a5577c-b39c-4c0c-a849-2b95cc0c0489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cc520d90-421d-4800-ae96-849e7929f01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 4,
                "x": 150,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cc435833-0fa3-4ff5-8475-103da0949f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 8,
                "x": 175,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c6b6a642-a8b0-475d-ba93-620d6b188c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 200,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dad66cfc-48ca-49d9-bdaf-1033deb0d829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0d362de3-d7dd-459d-a154-5cc2ade107a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 0,
                "y": 123
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c3ae3abc-d0c3-4b30-a954-0f6ad05b38fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 123
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8d31faa1-1a1e-4be7-9499-0c8a3afb90d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 123
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "84990f4e-002e-423c-acb1-ea5b585457fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 123
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b31a4c0e-1b5e-4b05-a7a9-b3c3ca010ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 123
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ebb21bf0-1e69-4bd9-b7bc-daa89f0affad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 123
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "016ff6fc-274c-4fe0-8aab-0a46f8dcf68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 123
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "84343df9-76c1-44c6-8d45-061d9a90288f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 123
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7f8815c1-9e29-4fc1-b743-0c834a77b91a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 123
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "776b0294-c67c-44ad-8e5b-3884e321a35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 123
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d21cb93b-09f1-4a42-b157-1b5bf7772fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 164
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fbc1dbad-fd99-4dd8-b2db-c26a2a7fd95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 25,
                "y": 164
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3d5db3fe-9820-4388-b187-11e1befb5361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 164
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9ca53603-c11d-457d-97b1-8bc092b9bcb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 164
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cf657cf1-ad2a-4884-8991-352311e6397b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 164
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2d3bc3f8-287b-4711-8a7a-4e7399e133de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 164
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bdf34bf2-27ce-4112-9f04-64888a1001d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 164
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "353039a3-6b03-42da-9d5b-9efa3223e4a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 164
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8e0289a4-fd2b-44dd-a836-30f9eda76b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 164
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "126bcc09-6eb4-4c01-b319-00e05b48133d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 164
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1fe3b1d5-3313-4898-ab7f-ff6bb4fdb62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 205
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "86b6fd1f-4001-4795-bf00-fe7fada1d815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 205
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "18eeeff0-127f-423c-85ea-f023c4ddb375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 205
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c39cf3a3-2b15-465d-b558-ccbcc12acf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 205
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e39e0766-c24b-470d-89ea-425d81d75135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 205
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "40fd6e84-b524-487e-8616-cc0618ffd7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 205
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c227160a-86f6-4ebd-adc2-5a588dc757c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 205
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "02b09f9c-10fa-4042-863d-2b04dbb3ef82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 205
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "96798b31-b88b-4ace-a1ea-4044d54edf58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 205
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d1f4e85b-c4b5-4c6c-b007-f5c7cee8f5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 41,
                "offset": 8,
                "shift": 23,
                "w": 12,
                "x": 225,
                "y": 205
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a9949cf7-49da-49af-bd30-1e1c52ba7ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 246
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "485ba30a-5eb3-4ccc-bacb-a8798d9872c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 12,
                "x": 25,
                "y": 246
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4e87004b-da1e-42c7-8e37-b594cc00ae4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 246
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "66cd3047-d550-417d-b79f-5ead0a8286ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 75,
                "y": 246
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d3ac50ef-25d4-4041-8b7c-48e6bb91f5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 10,
                "x": 100,
                "y": 246
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "79b600be-9145-4eb6-a1fb-e4473aa3f23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 246
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ead57435-17cb-42b0-ad43-0023575c1bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 246
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff09f910-48ad-4e0e-8b15-2946da9e85d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 246
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "732df1b2-7e49-40ca-9ada-3ee8147b6a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 246
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "18a0b664-3535-4f48-8746-0cb323cc63fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 246
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "efb49f71-bc1f-4653-a239-914b68c0f7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 41,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 0,
                "y": 287
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "197ab40e-0000-4044-9308-e9f8b11dda47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 287
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6f35fec3-0380-4dd8-aa28-6308e77e46c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 287
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "09c99560-0f3e-49b3-af7c-5fc604d8ee91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 75,
                "y": 287
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4048f889-accc-436b-aa9e-c60104a75264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 100,
                "y": 287
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "146e5bf1-f735-4ec9-97be-34ae12549a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 287
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "54bd99c8-1913-4906-be92-3871e697a8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 41,
                "offset": 10,
                "shift": 23,
                "w": 4,
                "x": 150,
                "y": 287
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d5022d23-2802-4792-b9bd-004ee153b890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 287
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "827683ed-8cbd-4ba5-a2ca-a40c21a7d405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 287
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1991b72e-1112-442a-95e2-7a72d9783c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 287
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fca87cf5-c509-4e16-8c20-2b601c661be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 328
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ee7fee8b-294b-467c-ad8d-738f939451dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 328
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d0b52713-f51f-4dc5-8257-608f720004ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 50,
                "y": 328
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4cef7045-3f07-416d-acc7-b84f1c4d6394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 75,
                "y": 328
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "be5ad4a3-39c5-457f-ae15-947836eb0fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 100,
                "y": 328
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7b296425-00ed-4f6f-a1dd-4417f67ee154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 125,
                "y": 328
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a6586909-fa07-4add-b55d-8a89124d5663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 150,
                "y": 328
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7dd0af3b-1740-48c4-9e64-43f5285991a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 175,
                "y": 328
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "28a976f6-4a57-4807-bfb5-cc2c3be2f580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 200,
                "y": 328
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d64ddb66-3816-431a-9062-7f4525447545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 225,
                "y": 328
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4617306f-661c-4dd0-8b70-7b72e1611728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 0,
                "y": 369
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "69752262-38d7-4c8b-a464-56a5ef2e005b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 25,
                "y": 369
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e1cd5786-123c-4730-9f7d-75578bd150a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 41,
                "offset": 10,
                "shift": 23,
                "w": 4,
                "x": 50,
                "y": 369
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "593322db-7ccf-4952-beda-8cdddb62c874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 41,
                "offset": 6,
                "shift": 23,
                "w": 12,
                "x": 75,
                "y": 369
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0a96f728-b883-4cf7-9e9e-209b89123bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 100,
                "y": 369
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}