{
    "id": "5fc94e8b-4f67-4a1c-adc2-1dcf9731dd21",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main_gui_placename",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8a6b1d98-a8b8-4cc6-b807-dd0ad6e9b8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 21,
                "y": 68
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "50f4f369-c87e-4b77-8195-06deb8c6c854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 167,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d2c77b4d-56ea-4ea2-815a-d32316e784f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 201,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c6d36192-c5b7-4810-bfc1-162a8e9d999a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "79f739f2-29d6-4807-b168-46663e9ba752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "410a672b-10f1-46c4-abde-735b176d0b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4185146a-511c-427c-b1f9-2ffe424ed5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3ada012d-562b-4027-ab34-aecbc5365b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 229,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "271c61a0-9e1e-475a-8383-eccea9008017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 83,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "40de12a3-4ab5-48b4-9650-1c5d18efaf4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 76,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1b88d30e-4e77-4d88-9f28-51a54ac5517f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 159,
                "y": 68
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "70fbc0f6-97dc-468a-8958-a3919f444109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 221,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "05826e8e-5320-42ba-89af-5a30f40b5fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 133,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6291e157-febd-4761-aa86-45fd538dc2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 177,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a8bf886a-e5cd-483b-9c25-a8c2c32a62b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 212,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "113964d5-34ca-45e0-b55c-df077767d2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5ba5d628-1364-4e7a-a7f5-783f8237e10c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 215,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d798270e-c79f-418d-9d92-b7909ef5c225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 114,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6a62d332-7186-4098-b407-bea4194ae5fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 71,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c08f7b9f-7bcb-4b98-8468-29cf812e3913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 81,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "247950ff-2327-4846-afe8-938e40b18933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 91,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "82db4b4f-706b-4225-a1cd-cbb2d00fdb3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 111,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "533ed994-7761-4f01-84eb-080a0c7c6536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 226,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "24b631f6-3817-485c-9baa-0a6ccbbdd438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2b7d6f88-e8d0-488b-8cbf-580c7ed11b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 191,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c5696d1d-ba88-4b54-acfe-598fb0ba2af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 24
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ecd5345d-96d5-4e2c-876c-5085da480552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 208,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "822bbecf-0754-4581-9340-81d506a0e42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 138,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ab27c554-c0d5-4c2e-bdc5-a3aadb9cee92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 106,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "645f43b1-9969-464d-bb9a-9aca46c5454f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 97,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a8b0ba38-b661-48d0-8e28-672e1eb036cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 28,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3410c3fb-1d25-4f36-ba70-0a7c94fc9ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 37,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6892537c-2819-43b2-8760-3ddefa730140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "01f9ec12-9f62-4995-8df4-49bd4f0c434e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e5c3a3fc-5382-44b0-887b-70f42784b4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 171,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7b2d374a-067d-44a0-ae0b-cfcb42f6bbd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b5276831-666c-402d-b43a-3fbbc9fb25db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b1700b36-a8c6-47c8-a4d9-c6009831c41d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 61,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "01bc06b9-1919-430e-9d24-6e368a7b70c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fdada21e-4a4f-47a9-9cf1-9adeb1d20f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 24
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f9af4720-30dc-4e01-822d-9b135f0e3d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "74541dd1-85c6-48fa-a6b5-6e5373ff688c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 172,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5662f659-dcd0-4b99-a548-7d4eb415a7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 181,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "98e66571-6ee9-4b0c-bc0b-33f61c9a4b21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1a52d711-dce2-4656-86bb-759455b92684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 211,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "af52b829-16e0-4019-a43f-2c933fc90ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a744aa1b-6aaf-4d1f-8b25-1e5bc08455e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 127,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1f56c3e2-f427-4f11-9bfe-9fb0e6f0b99a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "94413592-2256-4f6b-9796-78635719d653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "83dfdd26-cb1e-4471-9162-c42592b8ef0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd62e62c-03c4-4dd3-9876-1ee63de1182d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "34935398-3f8a-451b-8c36-80f7edefad92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 151,
                "y": 46
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "80b80b84-5881-466e-a8b4-34e4231cad7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 94,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c0719a16-593f-4241-96aa-7f0087cebc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d59f0dad-09fa-4469-8ccc-e6a51e617b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5a435b21-377e-4352-a489-95e8465b5750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2ff4c101-71b4-43e9-8798-52905bd89b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b2aa3f5d-82a9-4d00-bf03-90b623dbafab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2c791afd-fd56-4703-bdc0-806d177f325d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f661daee-6ae0-4b6e-bb0c-cc612cc400b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 90,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "77f72b14-fd1f-4a72-b0a7-71cf2bf7fb88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 239,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6b0d6750-8a1a-4f90-8ec5-b729b03ac0fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 69,
                "y": 68
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ca79511f-8ab3-494d-bb07-d4bf8a9ffe34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 189,
                "y": 68
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "30388c3f-fa10-4945-abdf-49dfaacb6efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "54287572-c499-471b-837a-e546b85fce36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 6,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 224,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d28fcc71-66ac-4ac6-aaa3-e91622fed2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0403f9af-3fbd-4db5-bca9-71f78b2550d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 193,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c66d4850-9477-494b-9c0b-fdbf06fce599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 131,
                "y": 46
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "08b9286a-a54c-4d4e-bb7b-1f9f16b7702e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5f4150bf-64ae-4e1a-b446-e302efba9f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e75a18b7-beba-4cc3-badd-d8cc84bc1c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ed19d191-566d-4a07-bd7d-3e07dc296552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "af4af7f9-827a-4235-bfc3-decf1a49b45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d83f5b9f-4fc5-45fa-9141-ef089a8ae434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 127,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0fef29ba-72cb-44be-a2bd-b32919b76924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 231,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1a3e83fe-27c3-4991-abf1-3d365fb690c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 116,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "aec962b2-7a59-4705-8a3b-5b1bcfd70f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 121,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c5d5d56d-345e-4f82-be6d-e397ac650e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a6eebfec-423d-4908-b45a-cad820e0c087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 138,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "292f4150-2c98-40f3-930e-d6b3fbe57303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2bcb0217-2ee1-4cff-ac2c-7a4f6e2f3647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7c92911a-2e3d-497c-9bd2-142bf019f29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "648a3c9b-4333-4d26-956e-ccb13da03d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 53,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1538aaaa-0805-461f-bbf6-62b9d603cb3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 45,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "75b892de-ff10-4e7a-a6cb-014e56996e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 61,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d3f8d036-1f75-4299-b56e-0c32d21ac978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d806c700-1984-40f7-b05c-d3e7eb0d59d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d2c9ece0-8bbb-441a-9ae3-767b0038c880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1068e61a-f016-4bd2-b229-44ed1b6ca784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 171,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6bba8669-0766-4b19-b5e2-fed52cde4434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7f8e263f-a85f-4d32-8f1e-df0c400c7c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7d960d33-1b74-4170-a4f4-9838bc6b3056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b54c5578-b44d-4484-a9c7-0cdecb7ac57c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 197,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "58006988-0f20-4423-b217-af6a75cdd4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "92d6dae6-12a7-4b79-b3df-9359a1a52316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 11,
                "y": 68
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "45d03887-5616-4a51-8d24-0cf850720bdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 68
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "0e0a6fa1-9bef-4a48-bf06-468136532529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 216,
                "y": 68
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "c3d08ed8-6fdd-4b3d-9697-b941a4312696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 9,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 220,
                "y": 68
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "baca7345-2022-46a1-acf8-f5e0ad0bdabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 9,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 151,
                "y": 68
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "c526c913-1469-45de-9b16-49036d530c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 9,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "e1533288-cb43-42fb-8e1c-fa97a457ed66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 24
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "5b9e2015-fd1f-4cfb-9981-f52b47b33277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "4be888f5-3591-4968-be15-3581da217089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "ef1349cd-3f87-4373-be4f-ebb5a439b156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "e815d180-1770-407e-b726-5889b11bcc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "b68175c3-02f2-43cc-8bcf-d3ed290e2b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "496dfc96-9097-4160-8eef-52970926da45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "e1a3c9ed-84e5-4f43-8a61-5a532b4bd95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "3e93ae85-5724-44ec-9371-cbf611b8c5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "ca7af3f2-8cc3-4b37-b908-b2f9ab4fabae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "a049130d-f7bf-47d6-8380-d94af8678eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "d19a1a86-6b31-4a14-87de-39beffdf724b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "c0e2278b-622c-4bb3-a267-4c90deeed7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "435c0bb4-ed53-44e5-a154-a8a96cfb54c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "c14d9d69-42ab-485b-a4ae-6b78aef26d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "9730f2dc-945d-44cb-83d7-1743bf05ad8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "d1a93882-e6b3-4930-8672-a64b58c92185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "dc65b236-6380-45a5-b1a0-2641add49444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e2801617-eec4-484d-984c-06efc812fdbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "62bd49b9-ed6b-4452-b175-14f13acd4bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "b448c578-a802-45ea-a3ac-ac6ac782d86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d4bd2ee7-c57b-46af-8956-829e9202d69e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "ab6fcb5e-3af3-4d5e-a37c-b393799a37a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "b170c21d-16a9-4ce9-a4ab-c70c1cf4293e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "72f49c3b-6883-45c8-96d2-01fc24320733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "81a2088c-2bc8-4830-98a2-6b05fc5a14a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "de919753-4cf5-45f3-a068-db2d39555c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "eab610fe-8326-444d-9b27-c70f677a1000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "fe01b8c2-0b78-48ec-bcb8-2bd3ab4ee26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "b9089559-3502-4e8b-a117-de518d94aa6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "02553c00-8492-4f5e-b409-69588bd57c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "7facdd2c-45a4-4afa-8e67-06712380243c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "95c68821-7bfc-404f-8513-cab267a1ae25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "c4d49eb7-5c1e-40dc-989f-37b299121287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "46b5cf40-425c-476c-b154-fd901122e1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "70afdc66-1029-4fce-8c34-02329b4e28ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "e84fbcc1-401b-4aa8-9fdc-c6f43c3a380f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "996f658b-fa3a-45ba-b2bb-f7b5efb792f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "c59065af-8b76-41f1-a74b-2f10673854dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "3a481b04-070b-46d0-8730-17719f2a2f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "caf87ffe-e97f-43b2-b63f-b8d7b40a48bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "377bbaff-95ce-4aab-8ec9-e757d39dbd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "ef2cbe97-6768-46ce-a6e4-56a1a104feab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "31eb130c-faf4-4daa-94fe-bbdcc04e9943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "e13f2a32-2c54-4523-9ed4-a0210593a769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "4528a29f-eebe-4f17-b87c-162135483304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "8d615b62-159d-43e6-b86f-f30e5ba1f433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "e39cbd3a-0380-45db-b06b-9e277d97a00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "51aab596-122f-4851-91e6-dec75c804c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "196f5eff-bcea-4e90-b58a-72b3171b14aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "54946a59-082a-46b5-810a-c7e6f331fe21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "942bbc7a-880f-4d94-9d39-6f844c547ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "38662078-ee7f-4b21-a79c-b9e4b6d8eb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "39896d94-182f-489f-b581-15aa0a2c8a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "46f05444-ae46-43c3-b4e9-69d59315df93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "0273ed39-e018-455e-946a-0808723d0a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "719d27ba-fe9a-4b4f-87de-fbdd6f741120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "f93670c6-2911-4185-b0cc-41b68edd9bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "99ce706c-10ff-4ae4-9b94-d52941a782dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "26624277-f42b-473e-87f8-54d156b4c6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "043aca3b-c2f1-4644-ba55-48686494e728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "a74b00f4-7276-45c0-818a-bc89da1ca429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "20d8924c-d422-4ced-81ae-3ece1089b156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "3f4962fd-9e65-48ff-a5f3-97b4db3e4fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "496abf79-80a7-4726-b5ee-41809dfdf32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "d0af7a2a-d535-46c2-af21-6389dbe7265f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "05a41149-307e-43c2-8a0e-a9a225b0c4c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ae680229-bba4-447f-85f6-1ca37a75f4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "e04f6075-eec6-423d-85d0-eecb9c96c828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "af2bdf8f-0e34-40e5-b452-dba253333e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "9aaea0a8-f911-4e11-a1fe-780725c1227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "7e58493d-b67b-4515-8c6b-61b79e65cafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "6c3f68b4-3a6c-4df5-baaa-5da439bf7045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "246cf868-c1bc-4687-aa92-abd50d2eef1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "d583e794-e4b6-4ef0-a4f6-abab8cb9ecdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "d5899265-f340-4972-88b4-4431791726c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "dce733cd-48f6-455c-8088-911e9720abf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "abdd4084-1921-4ee1-b452-683de6d6d14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "27306034-8990-465b-878a-de6656036cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "1047dbd5-902d-4373-9fe0-c202a00aaa9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "8eef92fb-aca3-4356-bc28-1d8d2b79e510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 115
        },
        {
            "id": "6453e25d-2daf-4a0b-8ad2-67e5cef81830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 13,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}