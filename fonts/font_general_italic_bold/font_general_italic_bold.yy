{
    "id": "e6d90d02-7ab5-47a1-a9f9-6d9453cb62d9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_italic_bold",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a2029e6a-3631-4899-a1f9-16e5749e35af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "975a5925-6a6d-4fca-b475-b9dfb1736342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 22,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f13520a2-f324-4e02-b038-af6d1b2d40b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "db779a46-51b5-4050-8b4c-e619cee55029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 66,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e130e601-61ff-422e-8269-7fa9e5d8e6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 88,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "141d5b03-dfe0-48ef-af3f-e134da148158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ab5c8a15-92a3-491c-85c7-230516c253ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 132,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "bd5c24a2-918f-45d7-a437-50924228b159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 4,
                "shift": 7,
                "w": 3,
                "x": 154,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2089eace-1f93-405f-97fe-757f59b77054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 176,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f3cadba4-4093-4135-91e9-15e1da6f9b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 198,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7a71c42f-eaea-4152-8dc0-2ca647916aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 220,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9ad111a7-0b19-4bde-b592-18b567ff1a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 0,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "59972009-b39e-41c6-ba15-1f5a96a3bb73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 22,
                "y": 30
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d2ef27fc-2ecf-4536-9ebe-fec4afceaaba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 44,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cac230bd-99eb-46c1-ab81-e2c5abb167b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 66,
                "y": 30
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "14578c7a-992d-4dc7-bbad-6bd1ca183a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ed2082f7-64fa-4c90-b4bb-9877e71dadfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 110,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "43204b4f-29dc-4466-bf8f-78e561c03166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 132,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "96e35d32-df46-48ff-83f1-dbff33a23f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 154,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b9375f22-71c3-4ae4-8b93-7d6b0e0ab593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 176,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1eaaa61d-90d4-4ef6-a86d-6cb7b2083ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 198,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "993d956f-882b-4eb7-9b90-9267614d9f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 220,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "29e2eebc-7371-44de-b7ed-5aa400d5da6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 60
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c14e4c60-8fec-41a3-ae03-4b73d7bde7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 22,
                "y": 60
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ccd47927-ab50-4454-a737-12fb9eb4bffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 60
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a414339b-4d33-44e8-9b36-7b08f25be334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 66,
                "y": 60
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7e258b7a-ef41-4385-af18-429280f78ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 88,
                "y": 60
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b3877ebf-11a1-4481-8028-98285db5fa55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 110,
                "y": 60
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6f202c60-f4bd-498e-8536-bea8f073cbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 60
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fa2c7705-ff40-472c-bd55-a77f51930159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 154,
                "y": 60
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fa01c48b-6d31-4987-ba75-da71f9290130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 176,
                "y": 60
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4513e16e-72f1-4f60-896a-425b6e2c316d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 198,
                "y": 60
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "438d22f5-85e9-4cda-82a2-822d5a400c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 220,
                "y": 60
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "162d887b-9907-438f-9da9-fdc57e5ca37a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 0,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b3998fe7-a51a-4d71-aea2-163eeb242935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 22,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9c110d15-ff77-40f0-bff0-de1a263bedd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 44,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0039cf2a-2b19-4e18-8d01-9e254db3fe09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3c2aa3ef-4d9a-4157-a8ed-ddf5dfa31c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 88,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "470f4c12-b911-4e5a-868f-9970bd12ca4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 110,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4910e935-4225-4791-a580-90c60c40b4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8ad9b0b2-3cf6-4e45-b6b3-42d88276f0a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 154,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6ebfcc41-388d-4273-9166-d4ed5c7d4a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 176,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "70992b72-a2f1-4d51-bbe0-18fecf88cf6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 198,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0998a16b-bd65-4a6e-9d14-25d954bbed8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 220,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fb4615ef-d908-4009-9c6f-db931b44d0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 0,
                "y": 120
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4a4baab1-d365-4b17-b1fb-8eda21eb559b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 22,
                "y": 120
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dac8f1fa-81e7-4759-bce1-004346043997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 44,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "94241eb4-8e50-4821-a2fb-4bff7e100486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 66,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "633b9155-c46c-46fa-9f8f-1b1b2f29cede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 88,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "469486ba-f16e-486d-82e2-d6bba160189b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 110,
                "y": 120
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "643c492d-aac9-47b7-b811-6fa1490944b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 132,
                "y": 120
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d0eddf9b-c2f0-44e1-8eb5-fb17d0ac5d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 154,
                "y": 120
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ca028fd7-e54a-4ef4-9741-e407236945f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 16,
                "x": 176,
                "y": 120
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "087dfbe9-a22f-496c-a132-ccce1b599ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 198,
                "y": 120
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d8d465dc-58ed-43e2-a85d-e0072fb6e54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 220,
                "y": 120
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5f2ec9ff-74c0-4ca1-9a17-74db8e1415cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 3,
                "shift": 22,
                "w": 20,
                "x": 0,
                "y": 150
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ac8f3f88-8ac9-491d-852e-100011cc2a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 22,
                "y": 150
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "39136d69-a893-4d84-a673-bd213bc663a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 3,
                "shift": 16,
                "w": 14,
                "x": 44,
                "y": 150
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "66b2c44d-2287-4113-8cb0-856dc0fcd9d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 150
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3fa3c705-663b-452f-ae13-e348070edf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 150
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a5bc54bd-4cd0-4e44-910b-5a72555c057b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 110,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1d5c87ae-efa9-4a07-b495-404d8906efef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 150
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "68b09005-b670-4705-8b30-45e67ebee1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 154,
                "y": 150
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c2dc8dce-c09b-4dcb-a67f-b6e1638adb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 176,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "927bbb34-4b79-4184-a76a-3ff084b05205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 198,
                "y": 150
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6a7c181f-ed94-4319-81e8-067a41d50909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6369ecbd-c808-420f-8ba1-3f0e6e7c17b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 0,
                "y": 180
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "30f96519-0f66-4764-9ca1-eaf53a74eca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 22,
                "y": 180
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9e2e7f70-fe9e-4b50-b760-75eaa105e8e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 44,
                "y": 180
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "26756480-c76c-486d-a8a3-0cb2d8cf71c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 66,
                "y": 180
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bf08b7ab-6ca7-424d-80d6-b90e9f26f750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": -1,
                "shift": 10,
                "w": 13,
                "x": 88,
                "y": 180
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "776ca2b4-4879-4d04-954b-116e247861ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 110,
                "y": 180
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "05cfa130-3ee3-4306-a728-690bef2d993a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 132,
                "y": 180
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3bc98bd7-4448-43e4-8cb2-bba6b943a3f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 154,
                "y": 180
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "87240be0-096f-4bfc-84e1-19d4597551e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -3,
                "shift": 9,
                "w": 12,
                "x": 176,
                "y": 180
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e53033f4-52c0-4ea3-b46a-fd6198748e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 180
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d306f5f5-3768-4d04-8619-cc1d862448c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 220,
                "y": 180
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dab9a52d-76b8-4474-99b9-55518c048f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 0,
                "y": 210
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3280e4e6-0cdc-4e35-abc7-ea49046a2131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 22,
                "y": 210
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f302bbe5-0ffc-426a-887f-603a2d377d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 210
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8e8b4364-c89c-4ee5-bc22-e73d9407676c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 66,
                "y": 210
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "59f14c74-bac6-4b17-b2f8-367ae6d6524c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 88,
                "y": 210
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "cc26fab2-7a2d-4cb0-8191-6087062b8c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 210
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "aef6aab1-f50c-46a9-bbbf-ec23c529575f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 210
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4a1f4b76-e6b7-45f8-a5d5-ebc93b0bb42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 154,
                "y": 210
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7ff9386c-8ae7-4035-813a-97aeb4ca7b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 176,
                "y": 210
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "910e5858-ee64-4eb3-8107-bc49670805b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 198,
                "y": 210
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ee92c0b7-f5a3-4025-bcbc-fb5811136659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 220,
                "y": 210
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "941e5d00-e053-4d79-99f6-2c1dd54015d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a3ab8277-dfea-43c0-8a0c-3a79f996fce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 22,
                "y": 240
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "06957406-e34d-4872-b2ea-49dc57453f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 44,
                "y": 240
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "231be635-3771-44ed-8eed-7eb64f8dd353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 66,
                "y": 240
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c2857c98-591f-408b-9638-818ba2a64198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 88,
                "y": 240
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6cf87940-30af-41f1-9300-3625e096ea33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 110,
                "y": 240
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c9529b93-a9c3-49f2-9e92-18c02241faf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 240
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "6eb200cd-5db6-4bb9-b580-548d77dfa199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 176,
                "y": 240
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "07b020bb-795f-4411-b265-9474d1eb631d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 198,
                "y": 240
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "df02104c-f075-4e3c-a2d3-794226aebcf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 30,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 220,
                "y": 240
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "f4169811-53a1-4fd3-a1a9-fb69895cb829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 0,
                "y": 270
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "19eb5307-9552-454b-bbfd-84e12a588e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 22,
                "y": 270
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "7057c0dd-3345-4523-9ee1-6e9b58340a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 44,
                "y": 270
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "779fe7f4-c9bf-48c0-a3be-11a850010be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "572f26d7-22fe-4486-8f77-c1b870061bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "81b2580c-801e-455d-a7a7-f6f439eab85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "c9a8576a-df94-4612-b996-faa0d84a1cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "77a996df-68f2-47ba-bd36-7fd8a067a398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "e3bdf7b1-ff5e-43f2-9ca9-2147675219bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "688f1f8a-dab3-474f-a861-4f0269962997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "54b9db10-c50e-4032-9020-3d306c46dbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "d9d72a15-86e6-4292-9b88-a7860f67df13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "33fb61f2-64a9-4557-934f-684fb9705ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "35866753-f51b-4178-8bed-c9957f905b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "6157e154-9789-4bfd-a4b9-6a29c7e176bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "f154e0af-899e-4ec3-aaac-f12728b3cc91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "8ba16299-bbf7-4cce-a201-7710d417ebc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "3b55544a-1bf0-4469-ae00-2470b2df702c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "684cb045-8c28-43d5-bf4e-314567e947b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "1b51666d-55d6-4147-a32b-0f47a27b6179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "777ec44d-27c7-48f4-b258-cf1a5b3907e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "c4afea5f-48c0-46e7-b87f-31baa7baaf96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "f3f9b2c1-6797-46e9-999f-048a24db85a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "b651aa42-6249-4f4a-91de-858ef760c7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "c551dad8-5dbb-474f-81e6-c06d59ec3962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "c04a4ec5-f4a8-4926-96e7-9a8f192c5399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "0cbac010-be12-4bae-8948-c708595b5541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "066da93b-449d-4803-8a4b-9ab9c6992a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "9f922fb6-8251-48f8-b401-96a56b31b466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "1ea1a17d-d64d-4dc4-8d8f-84ae7fc9d051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "ef30e330-8828-4297-8bde-7c442efa68b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "a0fceda7-6574-4eec-a22d-6d03e684ba21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "afaa3d52-821b-4dfb-b246-b317355fb497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "1583593f-8479-4218-ab3d-60e6317e3d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "d27d348c-ed5c-4a7f-a597-947634bb5712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "f6d8c60f-ea99-4511-a8f2-48422ce0229b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "3ecb9271-d42b-4610-8713-16edb861b78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "80ab8e08-b1c4-4563-a39d-251b7be1e8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "2715122f-688d-41c2-ac27-aa265e07ef59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "91004047-8ed4-482c-b005-2584df73efb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "54a357fc-c92d-4112-8cc1-ae8150d22b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "7e67c9b5-3f37-49fe-b758-612dfb075378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "2ba97826-ee64-456a-a0a9-df081c344c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "e5c8e42b-56be-4392-a8a6-4f96516a9dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "98e89f6e-e06e-453b-9314-3a9d8305fd78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "b65fb8de-1f94-47c9-a55f-e4a3acae4012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "1d403af4-4b47-4473-ad38-89ffadd10f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "1fe6b6e7-c3bd-425e-8be3-52bb9f2e7252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "f82b3dbe-0275-4b42-b15e-96da9bddb481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "4ca82749-b741-4d92-9181-8b92db0bbb6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "a5929d4b-a6aa-4c22-858c-7af37f906ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "81170e3c-2d4a-4a8d-ae7c-f35c0c4cbf1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "ddb14a7c-f39a-467a-b29b-e85caa5be064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "5f4d5aeb-00b8-416c-90f8-1d26d8a427d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "08ae8f4b-ca96-4223-bdda-96836af3de33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7560da16-04b0-4db8-8b88-7e93f3d80fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "379edf50-9565-474f-b794-ff013a614087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "e0babd10-899f-4d92-9d9e-215cf6d51a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "9719f157-26dc-45cb-b11b-17776e7e5e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "9f4ed2f4-77ac-4c5f-8511-9e72be54519f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "9b47cb4f-6043-4452-88f6-42bb2077dba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "ece2c95b-70e1-4043-a334-ee72b82d0a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "e3608de2-d1a2-435b-bab4-3c004205d85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "6f9634f9-bcdb-4897-b9c3-31c2cfb4972d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "2dc89ba2-1b58-47b2-9965-3000774af83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "256518ff-9753-4eab-a60e-d425a1943f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "22b5fc58-2bbb-4555-9882-f8c542faad7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "24de0766-6b0d-4220-9469-485d9838c59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "16974044-43f1-4d03-b582-2d9367a60954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "03bf2e5b-6e3e-443b-9db4-e9afb5c48a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "24f9e0da-1e21-4fcf-b3c8-1996206aca62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8208
        },
        {
            "id": "9bd70eac-3cb2-4a50-ab79-bc6d6c6d14d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "f1ad52c7-6e61-44ff-b7a7-fc0f192133d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "326bc649-af3b-4dce-a5ae-6e6260d5f291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "a6b204dd-ac7f-4401-aaa8-dc798351379b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "81aac323-8f7a-41f1-af02-2d7a24085cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "622cf142-3bb4-4b6a-842b-ecc3b6c2d07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "00da0ded-2060-4a62-82eb-6593e0fcf6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "d45b2839-84c1-4865-9313-146a5daa2a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "dad99337-6c0a-4ae6-8280-d970c30d007f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "a6241906-1db9-49d3-a1fc-1330008fc53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "a1a8b103-d30c-4dee-955e-bc31e23a2104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "251a64fe-f9d4-432e-ab70-07561c37f417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "52c708a7-d862-4ea7-b98d-6d2212824727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "8b1adad9-610f-49fa-a52b-4b46cb3e5cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "cab328d0-071a-46e6-b425-89e9750cfeb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8208
        },
        {
            "id": "6e8592bc-c657-4065-af17-426dee41247d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "3ed8d5bf-1c71-46de-9584-41c5606e57ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "74cbe968-082f-4504-9f6c-49a54d19803c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "39783151-b061-479b-b8d3-ab0238d3e9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "4d6a0c12-5a4b-4a50-8067-686b1cfc735f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "b19e780a-b017-4336-bf85-7db9955a3736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "6e6d20f3-f4fe-49c2-ba0d-cee02ab85d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "1ea19eb1-436d-44ce-95a6-789e9c2fc690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "e5aac45e-66c5-41db-b8cd-81fa5ee141dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "79f20c83-e814-443d-952f-0463aade55b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "22784b05-a9b1-4c6c-a648-8c62dc81f558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "0621e173-3432-430f-a31a-c93bc6402c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "dbcd82ba-595f-46f8-9151-be4caf571c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 116
        },
        {
            "id": "0dc81a69-e0eb-43b2-beca-3cbca381f15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 18,
    "styleName": "Bold Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}