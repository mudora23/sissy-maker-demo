{
    "id": "4f4609a0-dace-4c09-9459-560f236ece8f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_50b",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "89744ec8-5b78-4e4a-8b51-ef750dc94042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 80,
                "offset": 0,
                "shift": 20,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bdd6cabf-6566-4ace-89bb-269dd07bc5d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 80,
                "offset": 7,
                "shift": 25,
                "w": 11,
                "x": 62,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "693dbde4-8b5e-43bb-bb19-98101d09f3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 80,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 124,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3ebf071d-5dcd-4f41-885b-f9e9e8360c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 80,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 186,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "00c418d1-bb1a-4589-9077-91b0ae76e483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 80,
                "offset": 5,
                "shift": 39,
                "w": 29,
                "x": 248,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "be88a0e8-2156-45cf-8af7-ec96d4d2ad37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 80,
                "offset": 1,
                "shift": 46,
                "w": 43,
                "x": 310,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fdc3a83e-4e2a-41c2-a19f-1f23e7e0546a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 80,
                "offset": 4,
                "shift": 47,
                "w": 41,
                "x": 372,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0c3e09a4-bfe9-47bf-a07f-dbaf23fc7e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 80,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 434,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b2faa1e5-960f-4a3b-a4fe-fee6a48d6d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 80,
                "offset": 7,
                "shift": 25,
                "w": 15,
                "x": 496,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e4d089dc-8c09-41c4-b7f2-f648f4956d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 80,
                "offset": 5,
                "shift": 25,
                "w": 15,
                "x": 558,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f3d7e780-c66d-4e4b-a7bf-bb6ffdd89d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 80,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 620,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e48fbabc-777f-4278-82fd-9ed714ddcd46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 29,
                "x": 0,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fbb2cf64-7d5d-4be5-a339-b9dd5d0495df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 80,
                "offset": 5,
                "shift": 25,
                "w": 13,
                "x": 62,
                "y": 80
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8e94ba75-3424-41d7-b001-b2a21d273438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 80,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 124,
                "y": 80
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0f9fade7-62db-402b-9b8c-2fa4e0594944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 80,
                "offset": 6,
                "shift": 25,
                "w": 12,
                "x": 186,
                "y": 80
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c18b4c2a-adae-4401-b07d-dfdafa1c5c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 80,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 248,
                "y": 80
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "74354137-7d11-4018-aa40-248ea022a80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 80,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 310,
                "y": 80
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0ff93149-52f8-4315-8fbc-a3a491b58fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 80,
                "offset": 8,
                "shift": 39,
                "w": 19,
                "x": 372,
                "y": 80
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "eac877be-e184-4d94-8711-2bd6b43a4128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 434,
                "y": 80
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5a87bdd1-009c-409f-929a-b10254ff9cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 30,
                "x": 496,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d8984640-1368-4002-8968-467a5ab4368f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 80,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 558,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b7e3a5ac-2bb6-43bd-913d-4c55f219e0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 29,
                "x": 620,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bb302c2f-ef57-49e3-9125-af7a79456192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 0,
                "y": 160
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1036199b-24d3-4cf7-a573-57c17c025fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 33,
                "x": 62,
                "y": 160
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1f036aa5-fd8e-4520-91cb-3ec6e13d1b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 124,
                "y": 160
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "960916e0-94cd-4797-b07f-09158e7b586d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 80,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 186,
                "y": 160
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d9216b8c-a585-4cc7-a7c7-28ca4b7c349e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 80,
                "offset": 6,
                "shift": 25,
                "w": 12,
                "x": 248,
                "y": 160
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b59c61a6-63e8-4a49-9f08-71894ef0e434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 80,
                "offset": 5,
                "shift": 25,
                "w": 13,
                "x": 310,
                "y": 160
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "765eb2f3-ea9f-4236-9e01-319b1bc18eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 26,
                "x": 372,
                "y": 160
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4994915b-0e4c-4737-b5ae-bce3b60c7220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 29,
                "x": 434,
                "y": 160
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0aa334d7-2c8f-43a4-8a84-3214b4fe235f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 80,
                "offset": 9,
                "shift": 39,
                "w": 26,
                "x": 496,
                "y": 160
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7ddc1dba-fb41-4de9-83e4-49f20dc08662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 80,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 558,
                "y": 160
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b3567296-a959-461c-8376-e2a9206304d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 80,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 620,
                "y": 160
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e02377a6-43fd-47cc-bf36-070f2de60307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 80,
                "offset": 0,
                "shift": 42,
                "w": 42,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2f7a4852-b998-409f-8ae9-0c3378bb89d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 80,
                "offset": 5,
                "shift": 40,
                "w": 33,
                "x": 62,
                "y": 240
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2f4649dc-7fbf-420a-b80a-c7cf3d6d67c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 80,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 124,
                "y": 240
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1604dc4f-15a5-49f4-bd27-2cbf5dd9759c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 80,
                "offset": 5,
                "shift": 43,
                "w": 36,
                "x": 186,
                "y": 240
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "97239c8c-7508-4d5d-8460-c3c77681eaa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 80,
                "offset": 5,
                "shift": 38,
                "w": 30,
                "x": 248,
                "y": 240
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "767d7179-a325-4b70-b801-4ee4d818bbd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 80,
                "offset": 5,
                "shift": 39,
                "w": 32,
                "x": 310,
                "y": 240
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f5d902c9-e300-4bda-9a4c-6bdf3360757c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 80,
                "offset": 3,
                "shift": 45,
                "w": 40,
                "x": 372,
                "y": 240
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0af15d16-ffd5-4927-b502-d58e0a58d076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 80,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 434,
                "y": 240
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4181e462-e668-4288-944d-067541251ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 80,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 496,
                "y": 240
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b5f7994a-54d6-4a61-b605-995c6032f916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 80,
                "offset": 0,
                "shift": 36,
                "w": 32,
                "x": 558,
                "y": 240
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f645cc52-cc91-442c-aaef-340a3033e6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 80,
                "offset": 5,
                "shift": 41,
                "w": 38,
                "x": 620,
                "y": 240
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b229caab-65c2-4a71-89b6-8730c250c69a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 80,
                "offset": 5,
                "shift": 37,
                "w": 31,
                "x": 0,
                "y": 320
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3d632e91-96cb-44ae-b72f-e41c97f2bc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 80,
                "offset": 1,
                "shift": 50,
                "w": 49,
                "x": 62,
                "y": 320
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4e807e27-6ac2-4fd9-a7ec-24621d7e05cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 80,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 124,
                "y": 320
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bfdf6043-7437-4035-91bf-f6eed2c244a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 80,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 186,
                "y": 320
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0ebb8b1d-a30e-43ff-98fd-fca3830fcef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 80,
                "offset": 5,
                "shift": 39,
                "w": 32,
                "x": 248,
                "y": 320
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2d31bc70-46a0-4a0e-b858-012d13964b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 80,
                "offset": 3,
                "shift": 48,
                "w": 48,
                "x": 310,
                "y": 320
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "65afc0f9-6764-461d-8f0d-c57b3bfd22c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 80,
                "offset": 5,
                "shift": 41,
                "w": 37,
                "x": 372,
                "y": 320
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fe779298-e9d8-4acd-b4bd-80b663ef68e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 434,
                "y": 320
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fba79f1d-ab26-44ba-8ac1-6abd00a604bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 80,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 496,
                "y": 320
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6f557d6f-dfd2-4ec9-87b2-c2771e271d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 80,
                "offset": 5,
                "shift": 45,
                "w": 36,
                "x": 558,
                "y": 320
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0d7371f3-beb9-465f-a574-1dff91b3d9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 80,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 620,
                "y": 320
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e1cb9ad6-2041-4322-82fe-3ba9744e0c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 80,
                "offset": 0,
                "shift": 59,
                "w": 60,
                "x": 0,
                "y": 400
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ac235d32-ab57-43ef-afbd-c402abba1740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 80,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 62,
                "y": 400
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "376e9e69-d91c-4f47-9a79-eeaa82eb1e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 80,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 124,
                "y": 400
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9f92596e-dc66-4579-922e-fd34ed649f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 80,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 186,
                "y": 400
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "739ed05d-89e2-4b17-9796-7b05780e385f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 80,
                "offset": 4,
                "shift": 27,
                "w": 18,
                "x": 248,
                "y": 400
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "43be4b2f-bbe0-4821-a9ac-582705ba9ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 80,
                "offset": -1,
                "shift": 24,
                "w": 25,
                "x": 310,
                "y": 400
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f5775993-7159-45e3-a397-87617c3fcaa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 80,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 372,
                "y": 400
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ec7ee588-4186-4de6-83b9-2fb561af73ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 28,
                "x": 434,
                "y": 400
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8e10b335-ac56-48ed-95b1-7bd15249c75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 80,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 496,
                "y": 400
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a7a681ae-a41b-4da0-b299-e9737fa4f0e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 80,
                "offset": 11,
                "shift": 39,
                "w": 15,
                "x": 558,
                "y": 400
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6d5ac3ae-a1c2-49d0-a948-282803c3b9cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 620,
                "y": 400
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "91727761-532d-45bc-9ba0-b07e38b151fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 0,
                "y": 480
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b4fccedc-54aa-4f57-b308-5870006862cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 80,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 62,
                "y": 480
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a49a89b9-926c-48d9-ab59-772bbbe99e37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 80,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 124,
                "y": 480
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d65ee208-f87b-42aa-9c63-24a1c201f4af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 80,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 186,
                "y": 480
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2edb43fb-075e-4230-bcf4-f6952698002a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 80,
                "offset": 1,
                "shift": 25,
                "w": 25,
                "x": 248,
                "y": 480
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "49225d4f-74a0-4f80-84ed-1b91187212f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 80,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 310,
                "y": 480
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f7abfad7-5e0a-4a25-b08d-fcb7b534dd86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 80,
                "offset": 4,
                "shift": 40,
                "w": 31,
                "x": 372,
                "y": 480
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5b7d7e43-3b28-4752-b3c8-2a59bc60e9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 80,
                "offset": 3,
                "shift": 20,
                "w": 13,
                "x": 434,
                "y": 480
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4baa79de-f162-4577-8dfc-4ddf000cf2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 80,
                "offset": 0,
                "shift": 25,
                "w": 19,
                "x": 496,
                "y": 480
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8ed5a3e4-9d34-4866-b481-482fdce7f4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 80,
                "offset": 4,
                "shift": 37,
                "w": 32,
                "x": 558,
                "y": 480
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ca89d1e1-b753-4b70-8869-28bbfe6cd3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 80,
                "offset": 6,
                "shift": 20,
                "w": 12,
                "x": 620,
                "y": 480
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d836e66c-053d-4883-92fc-dcd468f1543f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 80,
                "offset": 4,
                "shift": 58,
                "w": 48,
                "x": 0,
                "y": 560
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3cf416e1-95df-48d5-b430-e9016e9279bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 80,
                "offset": 4,
                "shift": 40,
                "w": 31,
                "x": 62,
                "y": 560
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "89e1825f-3343-4264-b289-1edbb0098f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 80,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 124,
                "y": 560
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dd70931a-3369-433b-be64-7c90fc0fd098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 80,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 186,
                "y": 560
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "39b8d0ab-77aa-4ba6-af0e-27e3f7cfc391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 80,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 248,
                "y": 560
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "adde6bee-21b6-42ce-9506-fae7f75c7968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 80,
                "offset": 4,
                "shift": 29,
                "w": 24,
                "x": 310,
                "y": 560
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4690a73e-6ba7-4e38-ab5f-9ed4fb222b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 80,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 372,
                "y": 560
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "51374d00-fcee-4f68-8403-eba26b439ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 80,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 434,
                "y": 560
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "28c4897c-c435-498b-94b3-839128074e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 80,
                "offset": 4,
                "shift": 40,
                "w": 31,
                "x": 496,
                "y": 560
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d1d8ee23-84a2-479b-9f56-f5c394838725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 80,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 558,
                "y": 560
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9f284450-5d1c-4417-8efd-b271997a50b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 80,
                "offset": 1,
                "shift": 53,
                "w": 51,
                "x": 620,
                "y": 560
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "102a68c7-55cd-4691-9446-42a4de5e03f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 80,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 0,
                "y": 640
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c540101a-ee17-4580-a033-49f044fa2c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 80,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 62,
                "y": 640
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8344d7e6-e395-4de9-9129-d23267b04f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 80,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 124,
                "y": 640
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "714e0270-9546-41e3-a96c-b68d0195f3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 80,
                "offset": 2,
                "shift": 29,
                "w": 24,
                "x": 186,
                "y": 640
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1d8a8a0c-78e3-4161-82d1-7b75fc4a8b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 80,
                "offset": 17,
                "shift": 39,
                "w": 7,
                "x": 248,
                "y": 640
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9adc4b6e-2011-44dd-8144-0b1f6386aee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 80,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 310,
                "y": 640
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "279d1576-a9cf-47d9-94f6-ad9b88703aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 26,
                "x": 372,
                "y": 640
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "573e8dab-f0ba-4814-8de8-9a716af498c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 80,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 496,
                "y": 640
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "b06e8a12-388c-4f52-b745-ffcb3e9294f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 80,
                "offset": 7,
                "shift": 25,
                "w": 12,
                "x": 558,
                "y": 640
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "d1e5e0d0-73da-4ec6-8204-121e35b52195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 80,
                "offset": 6,
                "shift": 25,
                "w": 12,
                "x": 620,
                "y": 640
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "8089ea69-f80f-4c15-a859-40521897f264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 80,
                "offset": 7,
                "shift": 39,
                "w": 27,
                "x": 0,
                "y": 720
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "6ea7c621-d47a-4a70-a5b3-842e74ed3f17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 80,
                "offset": 6,
                "shift": 39,
                "w": 27,
                "x": 62,
                "y": 720
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "e5741a69-5402-4cf7-ba67-8703578f57f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 80,
                "offset": 3,
                "shift": 49,
                "w": 42,
                "x": 124,
                "y": 720
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "109f6860-7268-4a97-bb20-069b6c672ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "505f6bc1-50eb-4ead-afd8-081abb83dabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "91cba674-8c36-4b3c-ab38-7eefffd294ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "d84db899-ad93-4bbc-892d-34b9eca4bea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "f00587bf-2bcd-4c86-8efe-cdbd31d7415f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 84
        },
        {
            "id": "ed5fa8d3-093c-4ef7-94e4-242b33aa9094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 86
        },
        {
            "id": "abe73c4d-c0cc-4234-bc92-a7cd9b35bc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "09c48b95-ffaa-4068-a75c-6269297e9fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 89
        },
        {
            "id": "a69c162c-e500-4d4c-9b71-f2de3b5619b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "bf485b27-c71e-4d24-9a4d-fbc4a098b804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "718841b1-8e16-47ab-a0a5-9c86e12c4690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "d86e1b34-eca2-4ef6-9b66-1636a425e232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9506f971-36db-49c4-9cb7-dfe7d4b2caaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "6f7edab6-a3eb-4098-8aa8-84fb142182fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "31529662-3adc-4fec-bb67-75d853d43d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "c69ec528-2682-4029-a976-87d2734f5f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 97
        },
        {
            "id": "0909ff64-ea3f-4e2f-85a7-38ab1741fa34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 101
        },
        {
            "id": "6330f075-2672-4260-92ab-13c155a9fade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 111
        },
        {
            "id": "781120f5-7fc6-40ad-ac6b-075cff7a8fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 117
        },
        {
            "id": "3ba59ddd-0ea6-4510-b7dd-30bb95dfca75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "153fe9f7-40f3-4ad0-b2bd-b5cfc4905d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "df3097ce-fc29-4b17-b89c-50979a05539f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "32e09137-ec75-441c-bde7-18e70963f0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 87
        },
        {
            "id": "f2b829e2-dd6f-47cf-807b-f50e0a2496ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "15113ce8-a740-46f4-a6a1-4ecfd8ff4f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "965c748b-4bb2-4162-bf21-6ef31b5f068a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "67c96e27-81d7-4ca5-bf87-0a173057fe29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "20e7c553-e2b2-4ffb-b608-71bf6cf257d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 44
        },
        {
            "id": "bfb2387c-79d6-4ae5-bc36-6655c714def9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 46
        },
        {
            "id": "457bc521-8320-4935-8416-ae270ac621ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 65
        },
        {
            "id": "e7b507ae-1b0e-4e66-be28-7b8b94f535d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 97
        },
        {
            "id": "34043d28-0d02-463e-bb9b-3aa9d5d707fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 101
        },
        {
            "id": "e900553c-e71e-4bd1-a85c-5ed762c85e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 111
        },
        {
            "id": "0b9b9c25-e2d3-47a4-8103-6081ffe328e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "110e5d22-ae11-4275-b9db-0e4e9ad15373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "fc90399c-090f-4cc4-94db-ca98ae26f1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "4917861b-c7fd-4b52-bd71-3673499826c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "4f4d1029-6c38-4e98-9d22-7b6dee543c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "8e9f4526-3b79-484e-b847-36d485e0e436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "b08634a6-ecdb-441b-b0a1-d77e56453d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 45
        },
        {
            "id": "0341d882-be91-4d20-aeef-b22afbe2b640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "5128081a-9245-4aa1-b78f-949419d334af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "b26065be-994d-4d18-b561-d3f5cc32dfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "233e34d5-c45b-483f-8761-0c30e567ccec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 65
        },
        {
            "id": "512b207b-ab53-403b-8d05-658f003a81cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 79
        },
        {
            "id": "0fca364c-b55c-4f22-8edc-ac660b28773d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "8321bae9-ec57-464e-a379-9ee3081e9876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "2109bd0c-5539-48f1-b1b6-f5e38581be94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "6f285832-5862-42cb-b3d4-16077edb2512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "2f0b7d72-d847-4758-be1d-bda8cf6e7663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 111
        },
        {
            "id": "1a041a07-3490-4a4a-a0f8-79d14bf369ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 114
        },
        {
            "id": "10112b69-3600-469d-9830-c822a17d861b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "6f9e08c2-8c03-4d1f-9679-5dc0ae12afcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 117
        },
        {
            "id": "fafcc7ca-4e0a-422e-9546-0e002afcad4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 119
        },
        {
            "id": "cc3c9b9f-c741-46eb-9661-8976aad26144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 121
        },
        {
            "id": "b0904154-67c2-487d-a156-3f892263dbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8208
        },
        {
            "id": "5baad542-d841-49c2-ba1b-bade971a248e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 44
        },
        {
            "id": "9af80800-e0b8-42b9-8bf7-9e80df5b9b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "9357847f-f42e-4b06-8d9c-b2ae4d05119c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "b8420fad-9640-4914-ba98-562022d98bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "623cc1ac-f560-450d-aac2-f8ee48bd6db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "09d079e1-377c-470d-8780-3b45bda51ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 65
        },
        {
            "id": "99fbbc93-75f5-4c71-b437-29f3e7123b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 97
        },
        {
            "id": "8dbbfc51-a0a6-435d-ab77-4d5b38a15682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "c4d22acc-66e8-469e-92ed-9d9f181c9a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "e114da93-f358-4021-9916-c9384d256170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 111
        },
        {
            "id": "dba99cc8-025b-4525-bd74-e002ecb19b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "5f80f7f6-8ed4-4dc6-b1f3-dc57f4cc9bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 117
        },
        {
            "id": "6247186e-18f0-4962-adb1-efd345f657a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "843ded38-ff64-4890-a0af-460cd59ae5b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 8208
        },
        {
            "id": "1c7ff830-cd95-4bd1-81bd-5edca2035c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 44
        },
        {
            "id": "42ef6f10-1dcf-43f6-a5fb-ca72745baae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "895a2bdc-2e81-4949-9458-c64cbb31ea00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 46
        },
        {
            "id": "f78acbc4-34c8-44af-8966-f8cbe612bd42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "256f8369-d750-46f9-bef9-0f5b56b65a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "600a27bb-1ac9-43b2-ba10-b604db304f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "3c972914-ffe6-4072-b475-2cf21122ba68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "19ec94bb-65f0-4da5-b280-3d56665f18d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "95db08e8-0684-4cfa-9ef9-dad300b09f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "0defd675-8784-4c4b-88ae-dca950c596ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "244fd580-2c33-4e01-afa6-cbaf26e9193f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "ad9c0d5a-b058-45d9-954e-9d27f059e28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "c56637b9-a972-4d05-9c6c-49bf459edd84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8208
        },
        {
            "id": "67ac1e2c-7d5a-43a7-aaaf-860ec3d4e122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "4b6d5044-7706-4d96-a68e-d56b5ba4dfe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 44
        },
        {
            "id": "0599be9d-ff9e-45c9-9252-8a8da0c71f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "10c86f8c-6eeb-4e94-8ab9-387058bbc486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 46
        },
        {
            "id": "3f18898e-06d1-48d2-9184-97d91f6ee5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "eb8017cc-792f-45ba-8767-6b00d4e2c533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "39bd2031-a8a3-4028-8c30-b76ba5359785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 65
        },
        {
            "id": "9a649fff-535f-4582-90fc-ccf66deb1fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 97
        },
        {
            "id": "b52b2ccd-03c8-478d-ad27-9d13385309d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "15d62ef3-7911-4b77-bee3-76cbbabec261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "506214d0-0e4a-466e-a65a-03e5b4236a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "26d8dfa5-0275-462b-ac7a-d92d6102ba9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 112
        },
        {
            "id": "8c4ef5cc-8c45-41b1-a396-439c7d071282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "70df2885-5505-4e0a-9a5c-d2e68d29856c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "06cb86e2-6743-40e4-bf34-d0984ad45f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "ede07d3e-4b31-40c8-bf1a-31736a50713d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 8208
        },
        {
            "id": "5e31ab7e-a375-4256-8b98-4f0f60a6f7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "c0514bd5-e1b8-4620-8b45-e984ea82f88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 97
        },
        {
            "id": "05ece2e7-4db2-4fb7-9c9f-3c72259972de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 44
        },
        {
            "id": "0ac4af73-0bb2-47fa-bfc2-80e0ea1e960f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 46
        },
        {
            "id": "ef89cfc1-7078-4b6a-9073-505405f4f6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 97
        },
        {
            "id": "22781ed6-c32e-4ca8-aed5-2029e5c5539f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 44
        },
        {
            "id": "6b21fd2e-9664-4956-b6e1-ebd241da93a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 46
        },
        {
            "id": "e73ba214-a853-4b10-b595-e1ea5af6188e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 105
        },
        {
            "id": "35a64c53-cdb2-4fae-99d7-21586d2ebd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 44
        },
        {
            "id": "9ca37a69-129c-40b7-8b64-d58344489402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 46
        },
        {
            "id": "74cdd320-4c8e-42a2-82aa-54ee3694e4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 121,
            "second": 44
        },
        {
            "id": "37f97ccb-f067-4157-8723-08107d92619f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 121,
            "second": 46
        },
        {
            "id": "00fa77cc-f20f-46f3-8e71-806b49eae33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "0b956902-58a0-4f5f-b6b9-6f165ce9f5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 32
        },
        {
            "id": "865e3f61-e36b-4d06-ac0f-2b0c8358e8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 8217,
            "second": 115
        },
        {
            "id": "7506fe6e-ba4e-4ced-af8a-434e48b21f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 50,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}