{
    "id": "20dba6c0-83f4-4b5b-9bd8-c8dccf143047",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "23cc5db5-53c5-4bd4-bd9c-2e7dbfeadcab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 137,
                "y": 92
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6d20570d-b2f0-4a94-ba82-565cf4b0d20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 130,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b7b739df-a494-4b7c-9fbe-f73393bd7a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 150,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b27e1746-b84d-4215-8200-2afcfbe13ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 32
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e6f9a3c4-6954-4767-bdd4-c3c14cdb28ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 119,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "55dfa0db-f3d8-41f8-a67d-9235826d61dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c648dee7-6d3c-418d-891e-92ed1cc1a423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "bd212263-cdd2-4cdc-8385-684848ad2dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 184,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "79ec4ff8-aaa8-4dcb-a736-c00e08b13d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 79,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6e21ee09-cf1a-4b77-935e-0d2d3d68e69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2fd11d7d-d627-4147-ac67-cc954214eab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 122
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "df0a2508-164c-4b35-986e-d27d89253085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 125,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "93170838-6221-4eb0-9ccb-711c430eaf05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 35,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6ca83d1d-b92a-4158-91e9-b20659f645d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 136,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "02c00a87-f0f9-4001-8739-c7bb79fa91ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f5fc09e6-8b81-418f-954e-75a9bdb3bf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 70,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a13872c5-c9b0-40f0-9aba-b9753d3473ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "650e6320-fff2-4666-8f1a-dfaa1694f3ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 6,
                "x": 27,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "107e87ff-ae2d-4054-bb14-0de92e392dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 238,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d642d889-0850-44f8-9697-6d6ca6767239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "18c54489-8724-4567-8899-bd082c236b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "afae1698-bd60-4c19-9bd3-d9e60b1316d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 202,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bab025a0-16e2-4ac1-95e5-76112e23c7e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 214,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7926e487-5b59-4e8f-9de7-153989d4f8e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 226,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "238f770e-b244-495d-a38a-9b6d63200514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f57422eb-065d-4e10-ab6f-c246d0fe3968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 142,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bd620ea5-af27-4394-ab7b-5b0bb5d66baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f97853cc-f5a8-4b90-a451-29d8146f7ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 42,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d76eb92a-4008-496c-a32b-dc6e9a456df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 176,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e8dddc6b-f0ba-4810-8adb-a917492037fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 187,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5805721f-d6f7-4bb8-80a4-37c46da259a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 199,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4c5c5e28-0960-4966-ab9d-8d938b7e8525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "52330ba5-9c0f-4f34-9d5a-e5c912d4eb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cd53988a-8650-47f0-9042-9c6dcd46192f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c92c6b86-47d1-462e-a81a-e1af9f0c52d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 212,
                "y": 32
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e2e53bfe-ee4f-4980-9322-9776faed3fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "37b371bd-1558-4e4c-9422-3ebbdc3f845f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 100,
                "y": 32
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "19ecc2c2-9b40-4e9b-8e66-dadbce92269a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "35129446-0594-4588-97b5-05e8e0db13dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 178,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "243eed74-575f-4fe6-b967-0fc541581146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 41,
                "y": 32
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b52a2cd4-6bc9-497e-ac89-8a7c56df50b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d4e561f6-27d7-4b92-9964-ed83714dc95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 174,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6dd82a54-6f13-4bf2-acf6-935bfbca99c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 236,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9cc918f6-5e95-4d52-b0e9-389965b2d6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 41,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1da43c19-d6c6-4c35-b555-10ead6ee4494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 114,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b02598bf-a243-4fdf-a92a-2e9027e19c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "67aaf1f9-fd66-400a-8c36-24ace8e9ab13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6ccc7724-5a94-491e-b96c-4030d6337647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5bcf21e6-6b37-43a9-a438-3bf705d21d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 103,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d384e48e-913b-478d-aace-2bf1e98896e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7af2f6ae-1c7b-46da-bb90-4a1d94b5fd18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "89043f7a-020b-41ee-8dbb-38c8ab489046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 81,
                "y": 92
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "40485343-314f-48b0-a411-93aef247cac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "736617d0-e24c-4e6e-a228-dbc69f29ffa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "79fe32e5-48d9-43fc-8a5f-0059060d0b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 56,
                "y": 32
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7dd8779c-49db-4d3a-a915-8cdf94fa0c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b74daf58-bf6f-47ff-a8a7-5609550c7545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 32
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ad1faaad-405e-4cd9-bdb3-f1423be69dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6dd8c879-5a30-4112-8da0-958b17e3382a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d19ccb26-4655-4803-b4ab-43a7fe71c3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 20,
                "y": 122
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ab651e0c-fa2a-4531-a1cd-366d3658fb59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3875aa35-ee73-45fb-927d-e19f2b9fc129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 13,
                "y": 122
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "42dc47f6-3373-4fef-bcc7-6d3ff32daf45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 85,
                "y": 122
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7eb2c432-b0f9-4220-b25c-009816b3ebf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4b3d9ecd-1ab1-408a-a2bc-09a1af02c1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 178,
                "y": 122
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "24d9bae8-ab8e-488e-b0e3-da74c6ac28da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 67,
                "y": 62
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "58c6fc6e-dbc9-481d-a3fb-937fec659593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "010c93de-aabb-4ba5-8343-5f25c8dd65c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 130,
                "y": 62
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3057246a-b61e-4ddf-8fa4-90e78a209fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 62
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0726fc85-0373-4d7b-80b7-a36b169cf413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 62
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "787ff918-d97e-4b9d-82ee-21d0ca847397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 166,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0c489060-f238-49a7-84dd-14d261ea507c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b650cca6-d2c6-48e8-9b7b-12de5e7ca9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4a9d382c-0c0a-4803-9161-69a9a85e8eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 124,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1b7a7dc2-9b3f-49d2-94a6-ab0578ac0b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "84bbf632-b267-45fd-aeab-2220237ec474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7e3026e0-dbc5-41f3-8f66-c47128cf8f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 106,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "135d1ef9-ccdb-4790-bc56-a916c40bab17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "89198d5b-60d5-4308-8df8-49faf774ac63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 26,
                "y": 92
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c398b909-c8e7-4e95-bf21-c4ad42a29f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7544bf42-6bf8-47cc-b259-e2b778c8e981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bdc98995-196b-4aa8-bf47-cce17b79f7a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "77fccf8c-9271-4654-9475-aeb522721467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 227,
                "y": 92
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2c6883cc-5f48-468d-b13b-0dc8f3173996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 156,
                "y": 92
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "002fbdcd-e07a-4708-ab6c-4947ad522156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 218,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "29836ffb-66c7-4023-814f-8d0bf9f53ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "66ac5347-e5b7-4c69-9a53-4370e0800188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bdc08edc-6491-4298-bec7-3ab8ad395377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "20eb1b56-3224-4377-b946-2f8fa2fd49ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "48f12bf2-a64d-4bd6-9078-b3a7d2ea9f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9a817b85-d49b-46d8-8beb-a4d53ba330e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "13f16452-ba50-40eb-bd49-c35a62358bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "256ea5eb-d4a5-4945-95ba-4a27e007292d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 5,
                "shift": 13,
                "w": 2,
                "x": 170,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a8e0f659-ddff-4dc3-a515-545e478e9f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 60,
                "y": 92
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "aca0b947-ec54-4e45-acdb-f5e9dc834001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "d394a428-4888-47d6-8594-2d71683d2984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 143,
                "y": 122
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "d9d45ff2-ac20-4d21-ae84-7154e1709451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 158,
                "y": 122
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "21adcfc2-9955-4e4f-ac83-d083d1672162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 164,
                "y": 122
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "e0ba163f-36ca-4e97-9b3d-cf0ca0aa9c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 13,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 49,
                "y": 122
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "8a8e9039-662c-42db-a59b-767b28b447e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 13,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 61,
                "y": 122
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "d236fb1a-daf0-4e8a-94cc-e9f10d74116b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 23,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 2
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "e7257714-d83d-447f-99a5-7ef03f675b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "a3aaf420-f3b7-49f8-814c-302f9453b2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "5178bcf5-fc30-4010-ad2c-c0d244c7519b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "9814dd09-c6d9-43dc-8183-1d11f932dc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "16680e82-d1f7-4f3c-ba02-ae629ccb3af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "47802139-bf34-4cb9-b761-c8a1947f5285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "5e7c3aaf-4031-45ad-b114-69d564dbda02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f24fb3e0-ef09-461f-ba1c-b02e252b5e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "583d7abf-76e3-44b3-b4b3-3413e6b9b4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "972aa6d1-e216-4fa0-87ef-1fe808bee1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "792d1405-c75a-4fe0-9860-b14cba5e1b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "13e318ab-5a61-42c8-ac0f-5d6218a6819d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "7876e4f7-f6f4-4a69-831c-d1fdb5c687f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "79214316-d665-4680-84cf-6bcacef8d1df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "63dd5dfd-9ec2-4914-952c-cf1342f2f007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "c991fdeb-5ee4-44a7-a6e0-fd6bf8f74182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "97613073-826a-4a44-8700-376812117796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "4fc29ff8-e809-4a11-9568-8bad432a1406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "da1a2196-8c91-406b-9600-e6aeeef35ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "6cb7a2b4-57c4-42da-9f7f-b29605add5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "32d9e7db-2762-41c4-949f-726bb9282a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "a8ef455b-980b-4d83-9a79-fef1b09d243b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "6ca7eaed-67e9-43cb-8e92-5dfeca8c67d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "f9f1b0ca-ec05-4163-8fd8-81bc8055ff7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "9f8d140a-2669-4548-aac2-f59dbf6b2791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "5f383023-3b69-4ef9-ba41-cc8fdaa559fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "273ff846-f02e-4f9a-b4b5-000851054ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "91a74611-a2d1-4590-ba50-ece54bd031da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "13c68959-5d25-4ab4-acb1-e7a93d35a60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "18f73ddf-13ce-4d3d-a264-d0498bfb866a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "bd6311ac-bf56-4ae5-adf3-f9390b58fe7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "bee6df0a-e862-442d-80bf-8f97b9ac8b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 104
        },
        {
            "id": "dfcaab17-24e8-4486-9b59-2777b0c56cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "80c8dedc-16ca-47db-9be1-8ff5505f2b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "6795e5b9-016d-4a70-8c2a-780f208c3372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "7e1756ba-f1ea-495a-9529-f539bb70974d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "91ef6cfe-cc79-43f9-baf2-61239e09cc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "8dce70ca-576c-4c04-8d0a-7e39d9bca7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "10df158f-25c0-4f85-8250-f555347ce294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "50b88960-3b35-4fe8-b3cf-18fbb4bfe3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "30a4bca0-7525-456a-86fc-8ecc8fe46da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "64a0726a-d722-4084-b8bb-cbbf302b91f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "a414e12a-508f-4a1c-8124-ba4d78e4e2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "0141eb17-e417-48f3-bb7c-f397605e7452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "d7dc9a56-e497-4f52-b50c-dacea083c9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "2268e58f-d827-475d-8a73-b2bac882bdc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "01a66b12-889a-40db-a559-f38d1cb36743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "536e9224-065e-4d7a-9436-7abab7f19027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "095ddc1f-935f-4b35-8544-4bf7e3d46186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "5962cee3-aad6-4cd6-96c4-bf74a06a17bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "87df3c73-5db1-41b1-9fdd-a0cca1324aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "f52bedfa-b39f-488d-a59b-5371e4d7fffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "8c0514cb-dac4-4634-8a1d-f3229139eae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "07c72c3c-423a-46db-8ca4-f354413aa4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "7a4727bc-0452-491a-a14b-33e0a46ebd07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "f68694eb-0f64-4b32-8617-11e7dac2f1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "0f44ab4d-71ad-4ba8-adda-96fae1916373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "62e3e72d-b088-4af8-a4c7-5a846b67e88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "294c9f4a-a1d3-41b3-85f3-62bb2438e6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "8a19a0ee-92b1-4ec7-9bf7-446dce8f6c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "5f75dccb-ed01-48c6-bf2c-632c97059d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "08b84246-0866-4e85-bc18-c36b4fa58c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "b7aa2a21-d2e0-4a7d-a06d-ab5fbec18d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "26773d0d-dbbf-40d1-9d36-de7c37d82f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "378b0851-9ca0-4d9a-aa45-0777a1feb59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "f2d6841c-951b-4fc9-87ea-3592a7d76d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "cb0f98ff-89fd-4fb8-a8da-1f76e286821e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "bb1f8694-db7e-4915-9969-0ac2080b9401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "ffcccf8c-955e-4f36-979d-829346d54fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "4b61ef97-d94d-419a-8c08-54fc916b8858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "d2c11c7c-ac6e-4fde-b7ed-50ff1fe5f3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "01bba260-4a86-461f-b20e-6da98c4b4173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "16ecffc0-996b-46cf-ad42-8338a73b6005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8208
        },
        {
            "id": "e1f8868c-646a-4ebc-a69c-0499bb62a5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "01e838e4-6d34-4591-af96-07c00842f6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "dd3e59a2-2226-4129-95bc-2a1627aa3e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "c1584c2a-3b95-4941-8f56-60661b0db988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "049afb1a-b0f9-44c3-9f61-88ee94463199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "6499beaa-f8be-4cef-823f-dd599dd47766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "559ee244-6190-40cd-af77-307d385d7ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "ea097877-0f84-4251-9577-579a7f0d8027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "a1695abf-0770-4381-8497-0d08627a35a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "49a861c8-ac23-49e5-b02e-ca1c0a888577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8208
        },
        {
            "id": "8f245da2-c83a-4cc7-a0a9-30cc6d1bcb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "c76cd625-bd9a-41bf-9b9a-66378c4fafbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "8b62ee22-1894-4b7c-809a-60a373674d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "20f7e3f2-45c3-45fd-8e48-c36fa8999cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "07dc3f45-bce8-4651-85cb-18747ea38ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "ac353d64-9a48-4742-bef8-d7e3dc6311ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "81d5e5e6-321c-4c34-91cf-7c8a7be2b2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "25e694b5-1760-47c3-86e9-a8e82a2ae9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "e1cda4be-3e2c-4822-a567-4320b23479f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "62d36578-bf6f-4fb2-8c31-030065af2964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "8055a22d-c19c-4a18-b551-297b9f6fb58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "250d9d28-7fc3-452f-80a7-e6b115bc1b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "05f69a25-6fc8-47b8-8385-41e981d8ddcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "19ffa50f-c2f5-4ecf-9098-006ab3590fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "db3c743d-ce51-4d7e-af1d-bbfb9d0d2f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8208
        },
        {
            "id": "dae4a0c6-876b-4561-a7a1-ad37652e9890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "dcd6ca9a-1afb-4764-b96c-f3bfcf447d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "f78785b9-7cbf-43a1-b9f6-b77e761422fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "48ca602c-a812-43f4-a9bd-dce1ca9ab1c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "59cedc91-fc94-4a43-8589-6f19a2870ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "bb7af296-7d7b-4671-977d-a62416a540f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "222dd0f8-ce05-4cb2-afec-4808b3434bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "71c9d4fb-1eb0-462b-b9a0-572dfb5f70da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "9ea75a86-7c18-4032-8797-3d5926ae45e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "d045b42e-2b84-41ee-bd22-151a0614b56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "fa394e8c-b3ee-41bc-b3f1-3fc654308df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "88df7c54-e552-47ab-967f-58c5f802d4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 115
        },
        {
            "id": "1b579c4c-3929-4545-9cc9-974bee8a29ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}