{
    "id": "782e2030-2863-4980-84e5-3007e57e001d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_general_30",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Trebuchet MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "278704d5-b6ca-4914-b632-e2b0a3e971cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 12,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "55aa2c55-1487-4948-86d3-771e26832716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 5,
                "shift": 15,
                "w": 6,
                "x": 35,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4069d567-cb6a-44a1-b764-589f0d8e8b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 70,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "baea8541-5ddf-43e9-8c35-0be664226635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 105,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0a952a2d-2cd6-4b9b-850b-0e3ec51296ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 140,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1ef4c444-a456-48ba-b6d2-9f6047d08dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 175,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "25144e88-1da6-45b0-ae6d-3974ccca8437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 210,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dc4e2850-d91f-4c28-9c30-74181447c280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 245,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4b6b41d6-75b8-44f1-8576-bf4034314f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 280,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1ffb3047-d2f5-41c6-9679-7814fbdf749b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 315,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1bb01a47-6a31-4bd9-878e-49b5fec575e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 350,
                "y": 0
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "93092693-2e27-4c88-b806-fbccbc7bc354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 0,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dc528943-8656-4f50-83be-b25c166c3b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 7,
                "x": 35,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2152acff-12f0-4c63-b4d1-2966009b5d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 70,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c8a2ae60-87f9-4bf4-bf7e-d4908da3594c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 6,
                "x": 105,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "262ef2ab-6999-4e32-b0cb-77a4013f846e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 4,
                "shift": 21,
                "w": 15,
                "x": 140,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "079d18cf-ef1b-4387-841c-88e74c201f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 175,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8f02d851-e265-4a0f-a1cd-8afa1a3796a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 4,
                "shift": 21,
                "w": 10,
                "x": 210,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2e21179e-48af-4911-b88a-cf666820f2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 245,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9ef202a7-84e4-47e6-8335-97aff6a0c53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 280,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b91d775e-3942-4ed6-8280-feb92c52a48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 315,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a2796204-a6a5-4173-a57d-cbceeca4ac11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 350,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a20ff041-6f49-4e31-97c9-94a2ac3bca1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 0,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "61a07edd-9019-4100-aa44-85c24246ffa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 35,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "763134ed-fc40-4642-a2ab-9f44af5eb466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 70,
                "y": 96
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7baf8ff-9bd8-494c-8c75-3068c50e5881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 105,
                "y": 96
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cf7cfca7-a341-4eee-acc0-637e0ae2c826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 6,
                "x": 140,
                "y": 96
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "273f5f55-b1c0-4a07-9b28-8e1f1f47ddb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 7,
                "x": 175,
                "y": 96
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1f75744e-f9b0-4be8-a3da-9377e9fd4cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 14,
                "x": 210,
                "y": 96
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "df053c12-4654-4df0-bc46-5b3de3f08e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 245,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e40b4f1c-fb59-4b09-ab4d-d21c8232f8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 14,
                "x": 280,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "18d46f85-089c-4f0b-b95f-ca18e8523645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 315,
                "y": 96
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cf3d553c-eb9f-4a6b-9d76-ca1d4ba34c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 350,
                "y": 96
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "72efc711-8f90-4065-aa71-f8eccca6f7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 0,
                "y": 144
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6c0d6d1f-5f09-4323-bf70-056f456c5453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 35,
                "y": 144
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "50ba0e0f-8589-4858-8247-f4b095138266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 70,
                "y": 144
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4d4df202-5191-4909-9493-c493e4d8817a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 105,
                "y": 144
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "792c40c4-329c-4dfe-a81e-3ec6eff64b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 140,
                "y": 144
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1f770431-a517-46fb-bb57-e5c42b94aa74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 175,
                "y": 144
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "004ee220-c007-4aeb-9dcd-8dc25ccb23a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 210,
                "y": 144
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7f82706b-707a-4742-836c-f7f087100596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 245,
                "y": 144
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "39f7f825-9672-4750-adcd-2995ab54f38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 280,
                "y": 144
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2d6d149a-de86-490a-9288-229bae844625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 315,
                "y": 144
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3f9ea669-2677-4318-a889-3d9472aa617d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 350,
                "y": 144
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2450fbc5-4b6f-4d8c-973c-283a59f3082c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 0,
                "y": 192
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e688b74f-74ca-491f-8ec5-8a7f5fd07263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 35,
                "y": 192
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a5de0c0d-a645-401d-be8e-e079560737db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 70,
                "y": 192
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "13d6f4e3-ebc4-464a-83d7-386313237c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 105,
                "y": 192
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "508c47ed-2ce8-4692-bd4b-d7c950c5cfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 140,
                "y": 192
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9b555c1c-9433-4551-812b-1fd3e2c7646a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 26,
                "x": 175,
                "y": 192
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "614285bd-5c4e-43bd-9993-051331e1cdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 210,
                "y": 192
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c8be7554-d031-42ed-b1c4-7bab03a3e3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 245,
                "y": 192
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "284f57ff-616f-4021-ab2c-72156555fb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 280,
                "y": 192
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c1edcbe7-539a-49ac-827d-6bf19e542af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 315,
                "y": 192
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ce4c92f6-27fe-40d3-87d9-ed27937a6234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 350,
                "y": 192
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "57f423ba-4fee-4ff2-87c2-7bf39b510dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a9495aa0-8a51-402a-9782-c9bc9d65f9f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 35,
                "y": 240
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "12ca6a51-0110-460f-90d9-d52ef4913f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 70,
                "y": 240
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "71f660e3-5e2e-44a3-a055-2e1bef934502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 105,
                "y": 240
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "160774ab-7e05-49ee-8a36-b961f2d45a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 9,
                "x": 140,
                "y": 240
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e4f4fc00-2fba-4886-94f6-e74805202cce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 175,
                "y": 240
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "866fd358-19fd-408d-903e-cd5045f9cd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 210,
                "y": 240
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "dc276093-f083-4b24-98e4-a8f1ea6bd753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 245,
                "y": 240
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5ea0f8c4-b9f4-4cbf-9659-9d38d166af79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 280,
                "y": 240
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e46a57ad-8344-4be1-92ef-3cefe80e8af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 7,
                "shift": 21,
                "w": 7,
                "x": 315,
                "y": 240
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f382865f-8abe-42e4-874f-c6d13e8c0b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 350,
                "y": 240
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "98333f1e-ae04-4186-878d-45a60388df81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 0,
                "y": 288
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a8324aa0-1295-45c4-ab80-e9119aaf9b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 35,
                "y": 288
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ae7bf0ef-5e39-44c3-b2f9-1441e80baea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 70,
                "y": 288
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bc37612d-22dd-4518-976e-f10ec0b9860c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 105,
                "y": 288
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6b362316-17e8-45ea-932f-7fe7ab256408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 140,
                "y": 288
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c1ae7503-0505-4e23-a0ae-fae3b7897056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 175,
                "y": 288
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "398494f4-f367-4c15-b1f1-a14611f71314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 210,
                "y": 288
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "de614429-bb1b-49ef-a36e-39f4ef0af8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 245,
                "y": 288
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "db39aa9a-bab7-48a6-8501-52197464fd67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 280,
                "y": 288
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5d239df6-7b31-4bca-9a88-be4399970ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 3,
                "shift": 20,
                "w": 18,
                "x": 315,
                "y": 288
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ba672bfc-0c76-4b78-b202-0b7b834b833f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 350,
                "y": 288
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6e5c5324-858b-4f26-b888-caa15715080f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 0,
                "y": 336
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cd14d56d-5c54-488d-8a3f-8a78310d400e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 35,
                "y": 336
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "be8c3ec7-d4fd-4357-b5ec-bafb4a61ec86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 70,
                "y": 336
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7cda5ed2-d2f0-4566-9539-9d2e8bac4577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 105,
                "y": 336
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ad7f05ea-a72f-4092-96b8-e2a0987043a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 140,
                "y": 336
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8aa557b4-daa9-4f31-acda-e410ef8ea7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 3,
                "shift": 16,
                "w": 13,
                "x": 175,
                "y": 336
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "625fd470-2ed0-4471-9fb9-88c64a6a1438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 336
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5bee3db-5d8a-4cf3-bae8-f377276f05a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 245,
                "y": 336
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e3614bdd-bc2f-43f8-81d5-eee54ce19a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 280,
                "y": 336
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9c185671-5d0c-4d6b-b4df-9aff66a0eded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 315,
                "y": 336
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "47db6703-e4df-45ab-affa-aa3dcf1b5204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 350,
                "y": 336
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8cb58d22-db7c-476a-b948-55c233c417fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 0,
                "y": 384
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0a079ceb-26af-4674-941e-f92ef4e2511a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 35,
                "y": 384
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9c9444cd-6c70-4fea-9329-4171a731a875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 70,
                "y": 384
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "02806a65-3a09-4490-92ae-9fbc5227d52d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 105,
                "y": 384
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "caf3b28c-26b3-4e93-85be-73c6c328d1e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 9,
                "shift": 21,
                "w": 4,
                "x": 140,
                "y": 384
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "778eb060-c85b-48c6-bbc9-edfe026eb64d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 175,
                "y": 384
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bda9c9df-f957-40f4-b339-7f5464083ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 14,
                "x": 210,
                "y": 384
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "e8a4973d-8442-4791-9560-9d946d1fee81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 48,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 280,
                "y": 384
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "99cd9f12-3cd6-4470-b759-d820cdce1d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 315,
                "y": 384
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "cfa9ac44-d5cc-4880-a908-ee1d2c6059c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 48,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 350,
                "y": 384
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "a562b189-6c5c-4e64-a113-ea2397b3d984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 0,
                "y": 432
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "ada429ad-e941-45d0-baf2-75a0ea1065ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 35,
                "y": 432
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "b928bf63-73ea-4a6a-916f-778cbf9ccdd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 48,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 70,
                "y": 432
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "137d3f65-aa30-4eec-81a1-f42867df5978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "257ed059-798a-4e42-83b2-227ba15e38f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "f8a8b4dc-02b6-4a1e-82c5-98bc27abc217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "3529100d-f376-4e66-aad2-4da6214df1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "ba163a91-8d36-44dd-bc37-0a69c2eacd0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "fc8372d8-8e72-4ab0-bba5-1541eaf4124c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "8b6e7f2f-3767-4202-a1ad-676ddd35bce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "7da339ca-1bc6-4119-8404-c969040509f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "a78d80f2-47e6-423f-a991-54f15354867c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "8597a44a-3338-4739-b142-7c91a4807e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "d148072e-1fa1-4a1f-a24f-d5cdba061a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "fa132885-4a61-4874-907b-9c34ef6340a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c99aa59e-e4c5-4c9f-96cc-bc3dbab79507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "5b975ae8-ddfc-4fc5-99ff-a528d7489324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "e8827934-d3cd-4533-8bd4-5f3223e7ee09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "6827f264-d4c1-4fab-8958-f6d76929125b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "aa9e1943-0d75-4ceb-9705-068f37d27e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "7c2e68ee-a8a1-4fd6-b0fd-1987aa3dd100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "8e3b427b-f978-4cde-b709-a239c08afdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "9bdff13c-64ea-41fe-9ea2-3fa40b658ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "2b15f4ee-a65d-48d0-bdb9-464a22612b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "81aac460-ac95-47eb-977b-bf006beb6a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "7889b76a-b4c5-47a2-ab56-7e5f31dfe91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "38deb8a4-0fef-49da-be9d-cd16401496f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 86
        },
        {
            "id": "171c7fa0-a444-4707-8663-6dc956414ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 87
        },
        {
            "id": "b3b4634a-9d29-4039-8312-82e0c02bc0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "d8a0c199-c862-405a-bc2a-24bfa926ec75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 121
        },
        {
            "id": "067f8a6b-b056-40e4-886e-37c731fa8fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "8af2514c-1667-4f8c-ac27-873f51c2b7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "d4cafe5e-291b-4d09-afec-6e82b59060ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "5630d072-de4e-4cb0-b8a1-32de5c42eef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "f682ba7a-f683-4d53-90ed-df42546377e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "99e61f89-bd01-4b7d-bd2d-f482d77b590f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "15e780cb-1461-434d-8cd4-f0783a31929a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "de7c3ef8-009b-435c-8402-1a44378265d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 104
        },
        {
            "id": "d2d72f65-9eb4-479c-a69d-d21a6b0da806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 105
        },
        {
            "id": "eef6dc15-c4da-4934-9e5b-2fadf54b1d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "7e940963-e686-4e52-947d-c99c413a9746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 114
        },
        {
            "id": "2ff8be4f-4734-4be5-a67c-91afcba2214e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "3793573a-d450-4618-8f56-be14e32f1638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "2cafbf81-b8cd-4905-83e0-b53d94060126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "5712556a-9709-4e6e-a1ed-33935f2683a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "c556309e-080d-49e4-9119-97ba8fee4907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 101
        },
        {
            "id": "2b66c19b-73d3-4b6e-bc11-940c731b860a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 111
        },
        {
            "id": "9db42366-0ccd-4733-a60b-51bde93e124d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "4846d274-76ac-4f7e-b40a-50202ce6cda7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "bfe8b82e-2533-4451-a383-4f759b594687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "1ddef2e7-43ba-456b-855c-01c4ac8869ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "4f8670ff-18e6-43a8-996d-68c1c377e233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "4247755d-293e-46ef-8b2d-a08065d9da17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "0c071e92-b5a1-4ef3-ba7c-59892d4d23e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "4808edc4-ced0-4d8e-8f25-989fb6a8f49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "fbf4cc3c-69b7-4cf8-a565-5d43a3504ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "13595f5e-d5e2-4320-ac94-40dafd8460e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 97
        },
        {
            "id": "16896a05-b39c-43d6-8c49-d9d01fdd38c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 99
        },
        {
            "id": "01d63e1f-8f1b-47d9-b491-f3447b197c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 101
        },
        {
            "id": "859821f5-4a15-402b-a2b0-d0a28dea80f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "412b0ddd-cf1b-4549-b838-158a9a0639a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 111
        },
        {
            "id": "61d6623b-7fcc-4823-9cc9-30458d147b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 114
        },
        {
            "id": "012dec34-8407-47ca-8922-2faa8073de45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 115
        },
        {
            "id": "59fa60dc-49b9-405b-af26-91e6fa500b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 117
        },
        {
            "id": "12d758e6-e9f0-464c-b43f-2cbc87218d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 119
        },
        {
            "id": "b72d036b-ba68-48c7-a6c1-b365c98d7155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 121
        },
        {
            "id": "78023278-c266-4b4e-9a2c-96f565969d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8208
        },
        {
            "id": "26f8126f-c5ef-462f-8610-53c8fdf047f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "a43d58eb-ad7c-4a75-b083-5c2dad92d0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "71fa0cc1-440a-4a7b-a3a5-be4d6af441ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "b68aae5e-77d1-42a3-9eb4-63fe0065722b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "f298de59-3f2e-4412-b830-ff9dde1d8393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "9ea2555a-dd26-4d5d-8b33-940f893e4c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "b99b323c-7914-4642-bf1a-7f77ed3ccc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "1d7a2cb8-9d63-49b9-b6e6-2f6a5234be31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "0ab8c375-091a-40f8-8281-b695a7f09126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "c9f89708-07f7-4c08-ad99-d5099113dc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "3cb88dbe-2f6e-477f-a8e8-7dea809527ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "6e878d2f-606f-45d9-922e-9f67fd9e5667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 117
        },
        {
            "id": "39cd4fab-d144-4aa5-8e2e-01fb54864930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "fc8797a3-c0d5-4022-b4f1-10ce8db3b3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8208
        },
        {
            "id": "7f7099cb-e022-45fe-bdbe-c67edef49ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "05a917fc-ec39-4d61-9fa4-75f394450f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 45
        },
        {
            "id": "389c6142-5ab2-4d36-8059-a606de2197fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "a1e1a008-583c-4ac3-a563-f0a703411f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "e675c720-2c15-49a5-9805-2c9d193d5bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "282ab1a6-d454-4be6-85b7-c68b73b88ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "058c25db-99a9-41ab-9c71-0ef3c51e73af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "911363c2-9249-4ce0-80fd-b913f27d6041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "745342f8-b96c-4476-9968-36656ca47690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "017e110e-6cab-4a2b-a0b0-fc4162458fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "1781cf8d-3813-4a17-a66b-e16612944831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 114
        },
        {
            "id": "9cc74fca-cd05-4abe-8057-86013871c1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 117
        },
        {
            "id": "f6e08a7d-a3cc-420a-8322-237c93e5bed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "8b67a6f2-8e74-4611-9925-5d30fd661cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8208
        },
        {
            "id": "98c98abb-d78d-4bfb-a69e-107bef41db83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "f304e089-3c44-4784-aed7-2d383599f05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 44
        },
        {
            "id": "0d92045d-4897-4e04-a550-936e0e8ce3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 45
        },
        {
            "id": "fbcc1b80-53fe-4b1f-b366-94c4646d3da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 46
        },
        {
            "id": "c1bace39-7372-4ee0-ad51-c767724b09ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "23896333-f3be-4747-bd5a-a09b9610b7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "119be248-7de5-4c01-bca8-c13f6f9a28ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "fe782bc7-a550-4a76-b284-c9eb86ec11ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "83643eb8-9f89-4407-8c56-1e970a556e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "b8c1d2b4-ae5d-45fa-9614-65163b0f1de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "c74b19e2-269d-4532-ae00-fa319bd1d46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 111
        },
        {
            "id": "3aed0240-3803-45cc-8155-d7c307eec4d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "4da1e68b-6001-42ba-ae72-ceffe7d3267b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 113
        },
        {
            "id": "17d07acf-73dc-4fa5-86ad-7194c5e0dcea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "a7c6607d-0f92-495f-b832-9a93620e6628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "9b30ff0e-5906-45ff-91b2-26384b0b3ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 8208
        },
        {
            "id": "e89ebc3b-d75f-41d0-8009-8a5c5f674062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "ab6dea08-e6ce-484f-9a2f-bf408fb9d349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 114,
            "second": 44
        },
        {
            "id": "36e98cd5-4ca8-42ed-af66-8e22fe191510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 46
        },
        {
            "id": "650dc02f-1a8c-4e64-a2a4-c1853a9e98d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "c889ca29-e88d-417e-856c-3207f6fa9614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "66936cf0-d060-4c53-9811-2398a978491e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "ffb6d4c2-6232-475f-9b20-c883d8a18b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "98798139-661b-449e-9d55-9f6f2da812da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "cea3685b-2aef-4172-9044-a57b09ed950d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "22b26477-4b0a-4d9f-95b3-9f81e5a02b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 46
        },
        {
            "id": "f418cdd2-ddcc-4aa9-ae68-02d4ea09c14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 8216,
            "second": 8216
        },
        {
            "id": "881ffc9e-f026-4ba5-8d18-0bb7e374e640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 8217,
            "second": 32
        },
        {
            "id": "0b7efd98-7d9b-46d6-a853-dfb1d24fde65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 8217,
            "second": 115
        },
        {
            "id": "c4f44e11-08f0-4ed3-992a-4c2b29b691fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 8217,
            "second": 8217
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 173,
            "y": 173
        },
        {
            "x": 8216,
            "y": 8217
        },
        {
            "x": 8220,
            "y": 8221
        },
        {
            "x": 8230,
            "y": 8230
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}